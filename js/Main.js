define(function(require){
	main.constants={
		featureTypeCode:'featureType6y7a'
	};
	main.tools=[];
	main.flags={
		adding:false,
		drawing:false,
		movingFeature:null
	};
	main.animations={
		defaultDuration:100,
		defaultTransition:'linear'
	};
	main.dimensions={
		phoneWidth:400
	};
	main.colors={
		colors:[[255, 74, 0],[255, 221, 0],[163, 255, 0],[0, 255, 198],[0, 176, 255],[255, 0, 247],[0, 244, 255],[255, 0, 0],[0, 255, 13],[255, 172, 0]],
		colorIdx:0
	};
	main.colorGrds={
		options:{
			'Blue-Red':{
				name:'Blue-Red',
				inverted:'Red-Blue'
			},
			'Red-Blue':{
				name:'Red-Blue',
				inverted:'Blue-Red'
			},
			'Green-Red':{
				name:'Green-Red',
				inverted:'Red-Green'
			},
			'Red-Green':{
				name:'Red-Green',
				inverted:'Green-Red'
			}
		}
	};
	main.shapes={
		square:{
			alias:"Square",
			name:"square",
			points:4,
			angle:Math.PI/4
		},
		triangle:{
			alias:"Triangle",
			name:"triangle",
			points:3,
			rotation:Math.PI/4,
			angle:0
		},
		star:{
			alias:"Star",
			name:"star",
			points:5,
			radiusMult:2/5,
			angle:0
		},
		cross:{
			alias:"Cross",
			name:"cross",
			points:4,
			radiusMult:0,
			angle:0
		},
		x:{
			alias:"X",
			name:"x",
			points:4,
			radiusMult:0,
			angle:Math.PI/4
		},
		circle:{
			alias:"Circle",
			name:"circle"
		}
	};
	main.defaultStyle={
		zIndex:100,
		fillColor:'rgba(255, 255, 255, 0.2)',
		strokeColor:'#ffcc33',
		strokeWidth:2,
		pointRadius:7,
		pointFill:'#ffcc33',
	};
	main.addedLayers=[];
	main.activeStyle={
		strokeColor:'rgb(93, 214, 255)',
		strokeWidth:3,
		zIndex:130
	};
	main.showGroupStyle={
		zIndex:125
	};
	main.dataTables={};
	main.misc={
		blockUpdateData:false,
		blockUpdateData2:false,
		numberDTs:['integer','real','double','double precision','numeric'],
		textDTs:['text','character varying','text_array'],
		excludeFileDTs:['geometry','geometry_array','file'],
		excludeFileITs:['file'],
		maxZIndex:999999,
		defaultZoomTo:18,
		seasons:{
			'spring':{
				name:'spring',
				alias:'Spring',
				months:[[2,4]]
			},
			'summer':{
				name:'summer',
				alias:'Summer',
				months:[[5,7]]
			},
			'fall':{
				name:'fall',
				alias:'Fall',
				months:[[8,10]]
			},
			'winter':{
				name:'winter',
				alias:'Winter',
				months:[[11,11],[0,1]]
				
			}
		}
	};
	main.classes={
		About:require('classes/About'),
		Account:require('classes/Account'),
		AdvancedFilter:require('classes/AdvancedFilter'),
		Attachments:require('classes/Attachments'),
		Canopy:require('classes/Canopy'),
		Dashboard:require('classes/Dashboard'),
		Data:require('classes/Data'),
		DataUpdater:require('classes/DataUpdater'),
		DataUploader1:require('classes/DataUploader1'),
		Draw:require('classes/Draw'),
		EcoBenefits:require('classes/EcoBenefits'),
		Exporter:require('classes/Exporter'),
		Forms:require('classes/Forms'),
		FilterLegend:require('classes/FilterLegend'),
		GlobalSettings:require('classes/GlobalSettings'),
		ImageView:require('classes/ImageView'),
		Inventory:require('classes/Inventory'),
		InventoryEditor:require('classes/InventoryEditor'),
		Invents:require('classes/Invents'),
		Labels:require('classes/Labels'),
		Layer:require('classes/Layer'),
		// LayerLabels:require('classes/LayerLabels'),
		Layers:require('classes/Layers'),
		LayOut:require('classes/LayOut'),
		Loader:require('classes/Loader'),
		LookUpEditor:require('classes/LookUpEditor'),
		Map:require('classes/Map'),
		MapItems:require('classes/MapItems'),
		MapLayers:require('classes/MapLayers'),
		MapPrint:require('classes/MapPrint'),
		MassDelete:require('classes/MassDelete'),
		MassUpdater:require('classes/MassUpdater'),
		AutoIncrement:require('classes/AutoIncrement'),
		Measure:require('classes/Measure'),
		Print:require('classes/Print'),
		Settings:require('classes/Settings'),
		Reports:require('classes/Reports'),
		Server:require('classes/Server'),
		TBGeocode:require('classes/TBGeocode'),
		ToolBox:require('classes/ToolBox'),
		Tour:require('classes/Tour'),
		UploaderTool:require('classes/UploaderTool'),
		Utilities:require('classes/Utilities'),
		Welcome:require('classes/Welcome'),
		Wizard:require('classes/Wizard'),
		WOM:require('classes/WOM')
	};
		
	//Modes
	main.modes={
		editing:false,
		filter_legend:false
	};
	
	//Reports
	main.reports={};
	
	//utilities
	main.utilities=new main.classes.Utilities();
	// main.utilities.getBrowserParams();
	
	//settings
	main.personal_settings=main.utilities.cloneObj(main.data.personal_settings);
	main.globalSettings=main.utilities.cloneObj(main.data.std_settings);
	
	//Data Editor
	main.dataEditor=new main.classes.Data();
	
	//init layout
	main.layout=new main.classes.LayOut();
	main.layout.initMainLayout();
	
	//Load
	main.loader=new main.classes.Loader({tables:main.map_layers_sorted_keys});
	
	//init toolBox
	main.toolBox=new main.classes.ToolBox();
	
	//settings
	main.settings=new main.classes.Settings();
	
	//main image viewer
	main.imageView=new main.classes.ImageView();
	main.attachments=new main.classes.Attachments();
	
	//map
	main.map=new main.classes.Map();
	main.map.mapPrint=new main.classes.MapPrint();
	// if(main.browser.isMobile){main.map.changeToGoogleMap(main.map.tileOptions['google'].default_map_type);}
	
	main.layers=new main.classes.Layers(main.map);
	main.labels=new main.classes.Labels(main.map);
	if(main.withs.with_draw.value){
		main.draw=new main.classes.Draw(main.map);
	}
	if(main.withs.with_about.value){main.about=new main.classes.About();}
	if(main.withs.with_measure.value){
		main.measure=new main.classes.Measure(main.map);
	}
	if(main.withs.with_print.value){
		main.print=new main.classes.Print(main.map);
	}
	//Account
	if(main.withs.with_account.value){
		main.account=new main.classes.Account();
	}
	else{
		main.account=new Object();
		main.account.loggedInUser=new Object();
		main.account.loggedInUser.user_type="public_1";
	}
	
	//Forms
	main.formsMod=new main.classes.Forms();
	
	// Advanced Filter Page
	if(main.withs.with_adv_filter.value && main.inventories/*  && main.account.loggedInUser.username=='user1' */){
		main.advFilter=new main.classes.AdvancedFilter();
	}
	
	//MapItems
	// if(main.withs.with_inventories.value){
		main.mapItems=new main.classes.MapItems();
	// }
	
	//Inventory
	if(main.withs.with_inventories.value){
		main.invents=new main.classes.Invents({map:main.map});
	}
	
	//Inventory
	// if(main.withs.with_inventories.value){
		main.mapLayers=new main.classes.MapLayers();
	// }
	
	//Dashboard
	if(main.withs.with_dashboard.value){
		main.dashboard=new main.classes.Dashboard();
	}
	
	//Filter Legend
	if(main.withs.with_filter_legend.value && main.inventories){
		main.filter_legend=new main.classes.FilterLegend();
	}
	
	//Canopy
	if(main.withs.with_canopy.value){
		main.canopy=new main.classes.Canopy();
	}
	
	main.tools.push(['with_massdelete','massdelete',main.classes.MassDelete]);
	if(main.withs.with_massdelete && main.withs.with_massdelete.value && main.withs.with_massdelete[main.account.loggedInUser.user_type+'_with']){
		main.massdelete=new main.classes.MassDelete();
	}
	main.tools.push(['with_uploadertool','uploadertool',main.classes.UploaderTool]);
	if(main.withs.with_uploadertool && main.withs.with_uploadertool.value && main.withs.with_uploadertool[main.account.loggedInUser.user_type+'_with']){
		main.uploadertool=new main.classes.UploaderTool();
	}
	
	//Geocoder
	if(main.withs.with_geocoder.value){
		main.geocoder=new main.classes.TBGeocode();
	}
	
	//Exporter
	main.tools.push(['with_exporter','exporter',main.classes.Exporter]);
	if(main.withs.with_exporter && main.withs.with_exporter.value && main.withs.with_exporter[main.account.loggedInUser.user_type+'_with']){
		main.exporter=new main.classes.Exporter();
	}
	
	//Uploader
	// main.tools.push(['with_uploader','uploader',main.classes.DataUploader1]);
	// if(main.withs.with_uploader && main.withs.with_uploader.value && main.withs.with_uploader[main.account.loggedInUser.user_type+'_with']){
		// main.uploader=new main.classes.DataUploader1();
	// }
	
	//WOM
	// main.tools.push(['with_wom','wom',main.classes.WOM]);
	// if(main.withs.with_wom && main.withs.with_wom.value && main.withs.with_wom[main.account.loggedInUser.user_type+'_with']){
		// main.wom=new main.classes.WOM();
	// }
	
	//Look Up Table Editor
	main.tools.push(['with_lookup_editor','luEditor',main.classes.LookUpEditor]);
	if(main.withs.with_lookup_editor && main.withs.with_lookup_editor.value && main.withs.with_lookup_editor[main.account.loggedInUser.user_type+'_with']){
		main.luEditor=new main.classes.LookUpEditor();
	}
	
	//Inventory Editor
	main.tools.push(['with_inventory_editor','invEditor',main.classes.InventoryEditor]);
	if(main.withs.with_inventory_editor && main.withs.with_inventory_editor.value && main.withs.with_inventory_editor[main.account.loggedInUser.user_type+'_with']){
		main.invEditor=new main.classes.InventoryEditor();
	}
	
	//Mass Updater
	main.tools.push(['with_mass_updater','massUpdater',main.classes.MassUpdater]);
	if(main.invents && main.withs.with_mass_updater && main.withs.with_mass_updater.value && main.withs.with_mass_updater[main.account.loggedInUser.user_type+'_with']){
		main.massUpdater=new main.classes.MassUpdater();
	}
	
	//Auto Increment
	main.tools.push(['with_auto_increment','autoIncrement',main.classes.AutoIncrement]);
	if(main.invents && main.withs.with_auto_increment && main.withs.with_auto_increment.value && main.withs.with_auto_increment[main.account.loggedInUser.user_type+'_with']){
		main.autoIncrement=new main.classes.AutoIncrement();
	}
	
	//Time Compare
	// main.tools.push(['with_time_compare','timeCompare',main.classes.TimeCompare]);
	// if(main.withs.with_time_compare && main.withs.with_time_compare.value && main.withs.with_time_compare[main.account.loggedInUser.user_type+'_with']){
		// main.timeCompare=new main.classes.TimeCompare();
	// }
	
	main.tools.push(['with_updater','updater',main.classes.DataUpdater]);
	//Data Updater
	if(main.withs.with_updater && main.withs.with_updater.value && main.withs.with_updater[main.account.loggedInUser.user_type+'_with']){
		main.updater=new main.classes.DataUpdater();
	}
	
	// Tour
	if(main.withs.with_tour && main.withs.with_tour.value){
		main.tour=new main.classes.Tour();
	}
	
	// Welcome Page
	if(main.withs.with_welcome && main.withs.with_welcome.value){
		main.welcome=new main.classes.Welcome();
	}
	
	if(main.withs.with_wizard && main.withs.with_wizard.value){
		main.wizard=new main.classes.Wizard();
	}
	
	/* // Eco Benefits
	if(main.withs.with_ecobens.value && main.account.loggedInUser.username=='user1'){
		main.ecoBens=new main.classes.EcoBenefits();
	} */
	
	//custom classes
	main.classes.Custom=require('classes/Custom');
	main.custom=new main.classes.Custom();
	// main.custom=new require('../'+main.insDir+'classes/Custom');
	
	if(main.toolBox){main.toolBox.setGroups();};
});