define(function(require){
	return function pg(args){
		var self=this;
		var MapLayer=require('./MapLayer');
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.mapLayers={};
		(this.addInventoriyForms=function(){
			self.layers=main.data.map_layers;
			self.map_layers=main.map_layers;
			for(var key in self.layers){
				if(main.data.map_layers[key].active){
					self.mapLayers[key]=new MapLayer({
						layerName:key,
						mapLayers:self,
						groupOrganizer:self,
						map_layer:main.map_layers[key],
						properties:main.data.map_layers[key],
						map:main.map,
						fields:main.map_layers[key].fields,
						layer:main.map_layers[key]
					});
					if(main.tables[key]){
						main.tables[key].mapLayerMod=self.mapLayers[key];
						main.tables[key].coordModule=self.mapLayers[key];
					}
				}
			}
		})();
	};
});