define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Panel=require('./Panel');
		//var Utilities = require('./Utilities');
		this.setToApp=true;
		this.alias='Welcome';
		this.addAccShelf=true;
		this.items={};
		this.welcome=main.data.welcome;
		this.inSettings=true;
		this.minOnOutClick=true;
		this.id=main.utilities.generateUniqueID();
		
		title=main.data.welcome.title;
		text=main.data.welcome.text;
		var buttons=main.data.welcome.buttons;
		var logos=main.data.welcome.logos;
		var bottomLeftLink=main.data.welcome.bottom_left_link;
		var bottomRightLink=main.data.welcome.bottom_right_link;
		var mainImage=main.data.welcome.main_image;
		var imageType = main.data.welcome.main_image_type;
		var labelOnTop = main.data.welcome.button_label_ontop;
		if(imageType=="counter"){
		var counterAlias= main.data.welcome.counter_alias;	
		var counterTable = main.data.welcome.counter_table;
		var counterField = main.data.welcome.counter_field["field"];
		var counterFieldTable = main.data.welcome.counter_field["table"];
		}
		
/* 		if(main.startClass="PHS"){
		title="Welcome to the PHS Urban Forest Cloud!";
		text="This is an online map and data management tool for managing trees in the Philadelphia region planted by the Pennsylvania Horticultural Society. <br> Apply for a tree or login to begin managing your project.";
		var buttons={"Apply":{"label":"Online Application <br>for PHS TreeVitalize Programs","button":{"button1":{"Text":"Apply for a Tree","onclick":"function() {window.open('http://www.pennhort.net/','_blank');}"}}},
				 "Manage":{"label":"TreeVitalize Philadelphia Street Trees <br> TreeVitalize Watersheds <br> TreeVitalize Municipalities<br> Admin","button":{"button1":{"Text":"Manage Your Project","onclick":"function() {window.open('http://www.pennhort.net/','_blank');}"}}},
				 "Learn":{"label":"Overview and Intro","button":{"button1":{"Text":"Learn More","onclick":"function() {window.open('http://www.pennhort.net/','_blank');}"}}}};
		var logos={"PHS":{"name":"logo-phs.png"},"PlantOneMillion":{"name":"plantOneMillion.png"},"USFS":{"name":"USFS.png"},"TreeVitalize":{"name":"TreeVitalize.jpg"}};
		var bottomLeftLink={"label":"","link":""};
		var bottomRightLink={"label":"Help and Tutorials","link":"http://www.pennhort.net/"};
		var mainImage="";
		} */
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.createShelfContent=function(){
			self.showWelcomeButton=new Button('std','showWelcome showButton','Show');
			self.hideWelcomeButton=new Button('std','hideWelcome hideButton','Hide');
			var table=$('<table>')
				.append($('<tbody>')
					.append($('<tr>')
						.append($('<th>')
							.append($('<td>')
								.append($('<div>').addClass('tdLiner')
									.append(self.showWelcomeButton.html)
									.append(self.hideWelcomeButton.html)
								)
							)
						)
					)
				);
			return table
		};
		this.show=function(){
			self.panel.open();
			// self.updateButtonsShow();
		};
		this.hide=function(){
			self.panel.minimizePanel();
			// self.updateButtonsHide();
		};
		if(self.addAccShelf){
			this.updateButtonsShow=function(){
				self.showWelcomeButton.html.removeClass('inlineBlock');
				self.hideWelcomeButton.html.removeClass('none');
			};
			this.updateButtonsHide=function(){
				self.showWelcomeButton.html.addClass('inlineBlock');
				self.hideWelcomeButton.html.addClass('none');
			}; 
		};
		evalLink=function(link){
			window.open(link,'_blank');
		};
		evalOnlySidePanel=function(sidePanel){
			main.layout.sidePanels[sidePanel].open();
			self.hide();
		};
		evalFilterLegend=function(layerName){
			main.filter_legend.changeLayer(layerName);
			self.hide();
		};
		evalSidebar=function(key,sidePanel){
			main.utilities.openForm(key,sidePanel,true);
			self.hide();
		};
		evalPanel=function(panelName){
			main[panelName].show();
			self.hide();
		}
		self.refreshCount=function(){
			if(imageType=="counter"){
					$('#countValue').html("Loading");
					var params={
							table : counterTable,
							field : counterField,
							fieldTable: counterFieldTable,
							folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]
						}
				$.post('../main/server/db.php',{
								action : 'getAllTableCount',
								params : params
							}).done(function(response){
								if(response){
									
									
										$('#countValue').html(main.utilities.addCommas(response));										
								}
							});
			}
		};
		(this.createHTML=function(){
			if(!self.inSettings){
				//shelf
				if(self.addAccShelf){
					self.launchButton=new Button('toolBox','',self.alias);
					self.shelfContent=self.createShelfContent();
					self.shelf=main.layout.toolBox.createShelf({
						name:'welcome',
						button:self.launchButton.html,
						content:self.shelfContent,
						alias:self.alias
					});
				}
			}else{
				var settingsStuff=main.settings.addSetting('Welcome Window');
				self.showWelcomeButton=settingsStuff.showButton;
				self.hideWelcomeButton=settingsStuff.hideButton;
			}
 			self.titleHTML=$('<div>').addClass('welcomeTitle welcomeRibbon');
				$('<h1>').addClass('subHeader center').html(title).appendTo(self.titleHTML)	;		
			self.welcomeTopRow=$('<div>').addClass('row welcomeTopRow cf');
			if (imageType=="mainImage"){
				if(mainImage!=null){
					$('<div>').addClass('rowEle welcomeTopRowEle')
						.append($('<div>').addClass('imgwrap')
							.append($('<img align="left">').addClass('welcomeMainImg containerImg').attr('src',main.localImgs+mainImage)))
								.appendTo(self.welcomeTopRow);
					}
			}
			else if(imageType=="counter"){
				var countValue="Loading";
/* 				var params={
							table : counterTable,
							field : counterField,
							fieldTable: counterFieldTable,
							userID:main.userID
						}
				$.post('../main/server/db.php',{
								action : 'getAllTableCount',
								params : params
							}).done(function(response){
								if(response){
									
									
										$('#countValue').html(response);										
								}
							}); */

				var counterhtml =$('<div>').addClass('imgwrap').attr('id','counterwrap');
				self.countRow=$('<div>').addClass('welcomeCountRow cf');
				if(mainImage!=null){
					self.countRow.append('<h3 id= countTitle><span>'+counterAlias+'</span><span id= countValue></span></h3>');
					
					//hidden for spacing
					$('<div>').addClass('rowEle welcomeTopRowEle')
						.append($('<div>').addClass('imgwrap')
							.append($('<img align="left">').addClass('welcomeMainImg containerImg').attr('src',main.localImgs+mainImage)))
								.appendTo(self.welcomeTopRow);
					// counterhtml.append($('<img id=hiddenCounterImage>').addClass('containerImg welcomeMainImg').attr('src',main.localImgs+mainImage));
					// counterhtml[0].style.backgroundImage="url('"+main.localImgs+mainImage+"')";
					
					}
				else{
					self.countRow.append('<h3 id= countTitle><span>'+counterAlias+'</span><span id= countValue></span></h3>');
					
				}	
				// $('<div>').addClass('rowEle welcomeTopRowEle')
						// .append(counterhtml)
						// .appendTo(self.welcomeTopRow);
				self.refreshCount();	
			}
			else{}
/* 			if(mainImage!=null){
				$('<div>').addClass('rowEle welcomeTopRowEle')
					.append($('<div>').addClass('imgwrap')
						.append($('<img>').addClass('containerImg').attr('src',main.localImgs+mainImage)))
							.appendTo(self.welcomeTopRow);
				} */
			$('<div>').addClass('rowEle welcomeTopRowEle').append($('<div class="welcomeContentDesc">').html(text)).appendTo(self.welcomeTopRow);
			self.welcomeMoreDetailButton=$('<div>').addClass('moreDetail cf');
			self.welcomeMoreDetailLabel=$('<div>').addClass('moreDetail welcomeLabel cf');
			self.welcomeMoreDetailLogo=$('<div>').addClass('moreDetail cf');
				for(var key in buttons){
					temp1=$('<div data-button="'+key+'">').addClass('moreDetailButtonColumn rowEle center');
					temp2=$('<div data-button="'+key+'">').addClass('moreDetailButtonColumn rowEle center');
					for (var key2 in buttons[key].button){
						if(buttons[key].button[key2].type=="link"){
							$('<button>').addClass('center welcomButton centermargin').data('key',key).data('key2',key2).html(buttons[key].button[key2].Text).on('click',function(){
								var key=$(this).data('key');
								var key2=$(this).data('key2');
								evalLink(buttons[key].button[key2].onclick.link)}).appendTo(temp1);
						}else if(buttons[key].button[key2].type=='onlySidePanel'){
							$('<button>').addClass('center welcomButton centermargin').data('key',key).data('key2',key2).html(buttons[key].button[key2].Text).click(function(){
								var key=$(this).data('key');
								var key2=$(this).data('key2');
								evalOnlySidePanel(buttons[key].button[key2].onclick.sidePanel);
							}).appendTo(temp1);
						}else if(buttons[key].button[key2].type=='filterLegendLayer'){
							$('<button>').addClass('center welcomButton centermargin').data('key',key).data('key2',key2).html(buttons[key].button[key2].Text).click(function(){
								var key=$(this).data('key');
								var key2=$(this).data('key2');
								evalFilterLegend(buttons[key].button[key2].onclick.key);
							}).appendTo(temp1);
						}
						else if(buttons[key].button[key2].type=='panel'){
							$('<button>').addClass('center welcomButton centermargin').data('key',key).data('key2',key2).html(buttons[key].button[key2].Text).click(function(){
								var key=$(this).data('key');
								var key2=$(this).data('key2');
								evalPanel(buttons[key].button[key2].onclick.key);
							}).appendTo(temp1);
						}
						else{
							$('<button>').addClass('center welcomButton centermargin').data('key',key).data('key2',key2).html(buttons[key].button[key2].Text).click(function(){
								var key=$(this).data('key'),key2=$(this).data('key2'),mapLayers;
								evalSidebar(buttons[key].button[key2].onclick.key,buttons[key].button[key2].onclick.sidePanel)
								if(buttons[key].button[key2].onclick.mapLayers && main.filter_legend && main.filter_legend.otherLayerItems){
									mapLayers=buttons[key].button[key2].onclick.mapLayers;
									for(var n=0;n<mapLayers.length;n++){
										if(main.filter_legend.otherLayerItems[mapLayers[n]]){
											main.filter_legend.otherLayerItems[mapLayers[n]].checkbox.checkbox.prop('checked',true).change();
										}
									}
								}
							}).appendTo(temp1);
						}
					}
					$('<p>').html(buttons[key].label).appendTo(temp2);
					temp1.appendTo(self.welcomeMoreDetailButton); 
					temp2.appendTo(self.welcomeMoreDetailLabel); 
				};
			self.welcomeLogos=$('<div>').addClass('welcomeLogos cf');
				var leftInner = $('<div>').addClass('welcomeLinkBoxInner'),link;
				Left = $('<div>').addClass('welcomeLinkLeft welcomeLinkBox welcomeLogosEle').append(leftInner);
				self.links=[];
				for(var i=0;i<bottomLeftLink.length;i++){
					if(bottomLeftLink[i].to_open){
						link=$('<a href="#" data-shelf="'+bottomLeftLink[i].to_open.shelf+'" data-accordian="'+bottomLeftLink[i].to_open.accordian+'">').addClass('welcomelink').html(bottomLeftLink[i].label)
						link.on('click',function(e){
							e.preventDefault();
							main.utilities.openForm($(this).data('shelf'),$(this).data('accordian'),true);
							self.panel.minimizePanel();
						});
					}else{
						link=$('<a>').addClass('welcomelink').attr('href',bottomLeftLink[i].link).attr('target',"_blank").html(bottomLeftLink[i].label);
					}
					leftInner.append(link);
					self.links.push(link);
				}
				Left.appendTo(self.welcomeLogos);
				self.welcomeLogosMiddle=$('<div>').addClass('welcomeLogosMiddle welcomeLogosEle');
				self.welcomeLogos.append(self.welcomeLogosMiddle);
				for(var logo in logos){
					temp=($('<img>').addClass('containerImg welcomeLogo center centermargin').attr('src',main.localImgs+logos[logo].name))
					temp.appendTo(self.welcomeLogosMiddle); 
				};
				self.welcomeLogos.appendTo(self.welcomeMoreDetailLogo);
				var rightInner = $('<div>').addClass('welcomeLinkBoxInner');
				Right = $('<div>').addClass('welcomeLinkRight welcomeLinkBox welcomeLogosEle').append(rightInner);
				for(var i=0;i<bottomRightLink.length;i++){
					if(bottomRightLink[i].to_open){
						link=$('<a href="#" data-shelf="'+bottomRightLink[i].to_open.shelf+'" data-accordian="'+bottomRightLink[i].to_open.accordian+'">').addClass('welcomelink').html(bottomRightLink[i].label)
						link.on('click',function(e){
							e.preventDefault();
							main.utilities.openForm($(this).data('shelf'),$(this).data('accordian'),true);
							self.panel.minimizePanel();
						});
					}else{
						link=$('<a>').addClass('welcomelink').attr('href',bottomRightLink[i].link).attr('target',"_blank").html(bottomRightLink[i].label);
					}
					rightInner.append(link);
					self.links.push(link);
				}
				Right.appendTo(self.welcomeLogos);
			
				$('<div class="attributes">').attr('id','planitgeo').append($('<p>').html('For the best experience, use Chrome as your web browser. This application is based on Urban Forest Cloud© software by <a target="_blank" href="http://planitgeo.com">Plan-It Geo</a>')).appendTo(self.welcomeMoreDetailLogo);
			
			self.content=$('<div>').addClass('welcomeContent')
 				.append(self.titleHTML);
			if(imageType=="counter"){
				self.content.append(self.countRow)
			}
			self.smallScreenNotice=$('<div>').addClass('welcomeSmallScreenNotice').html(
					'<h4>Tree Plotter is in small screen mode</h4><p>This means Tree Plotter is operating in limited tool mode. In this mode, you will not be able to use the following tools:</p><ul><li>Uploader</li><li>Measure and Graphics</li><li>Take a Tour</li><li>Expanded Data Table</li><li>Dashboard</li></ul><p>In order to use these tools, enlarge your screen or switch to a device with a larger screen.</p>'
				);
			
			if(labelOnTop){
				self.content.append(self.welcomeTopRow)
					.append(self.smallScreenNotice)
					.append(self.welcomeMoreDetailLabel)
					.append(self.welcomeMoreDetailButton)
					.append(self.welcomeMoreDetailLogo);
			}
			else{				
			self.content.append(self.welcomeTopRow)
				.append(self.smallScreenNotice)
				.append(self.welcomeMoreDetailButton)
				.append(self.welcomeMoreDetailLabel)
				.append(self.welcomeMoreDetailLogo);
			}
			self.panel=new Panel({
				content:self.content,
				title:'Welcome',
				classes:'welcomeWindow',
				position:'mMiddle',
				matchHeight:true,
				relativeTo:main.layout.mapContainerWrap,
				minOnOutClick:self.minOnOutClick,
				hideOnOutClickExceptions:['.headerContEle.icon','.headerContEle.text'],
				onClose:self.updateButtonsHide,
				onMin:self.updateButtonsHide,
				onShow:self.updateButtonsShow,
				withOutClose:true,
				stayProportionalOnResize:true
			});
		})();
		(this.initEvents=function(){
			self.panel.html.on('change','.WelcomeLayerCheckWrap',function(){
				var WelcomeID=$(this).data('Welcomeid');
				var visible=$(this).prop('checked');
			});
			if(self.addAccShelf){
				self.hideWelcomeButton.html.on('click',function(){
					self.hide();
				});
				self.showWelcomeButton.html.on('click',function(){
					self.show();
					self.refreshCount();
				});
			}
			self.panel.html.find('img').one('load',function(){
				self.panel.positionPanel();
			}).each(function(){
				if(this.complete)$(this).load();
			});
		})();
		if(this.setToApp){this.show();}else{this.updateButtonsHide()}
	};
});