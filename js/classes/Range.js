define(function(require){
	return function pg(args){
		var self=this;
		var MoreInfo=require('./MoreInfo');
		var Tabs=require('./Tabs');
		var Time=require('./Time');
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.id=this.id || main.utilities.generateUniqueID();
		this.key=this.key || '';
		this.classes=this.classes || '';
		this.min=(this.min || this.min===0)?this.min:Number.POSITIVE_INFINITY;
		this.max=(this.max || this.max===0)?this.max:Number.NEGATIVE_INFINITY;
		this.step=this.step || 1;
		this.prepend_to=this.prepend_to || '';
		this.append_to=this.append_to || '';
		this.handlesLength=this.handlesLength || 2;
		if(!this.to_fixed && this.to_fixed!==0){this.to_fixed=2};
		this.sticky=this.sticky || false;
		this.stickyStep=this.stickyStep || 1;
		this.withAvg=this.withAvg || false;
		this.maxScaleMarks=this.maxScaleMarks || 5;
		this.handleWidth=12;
		this.handlePaddingWidth=0;
		this.handleBorderWidth=1;
		this.halfHandleWidth=(this.handleWidth+this.handleBorderWidth*2+this.handlePaddingWidth*2)/2;
		this.handleMouseDown=null;
		this.currentHandle=null;
		this.hasSpecifics=null;
		this.retScalePoint=function(value,left){
			return $('<div style="left:'+left+'%">').addClass('scalePoint')
				.append($('<div>').addClass('scalePointer'))
				.append($('<div>').addClass('scalePointLabel').text(value));
		};
		this.updateAll=function(currentHandle,val){
			self.updateValues();
			self.updateOutputs();
			self.updateHandles(currentHandle,val);
		};
		this.setScaleInfo=function(e){
			// if(!self.relativeValues){
				self.scaleStep=self.step;
				self.points=(self.max-self.min)/self.scaleStep;
				if(self.points>self.maxScaleMarks){
					self.points=self.maxScaleMarks;
					self.scaleStep=(self.max-self.min)/self.points;
				}
			// }
		};
		this.createScale=function(){
			var rangeScale='';
			var bottomLine=$('<div class="bottomLine">');
			var notches=$('<div class="notches">');
			var notchLabels=$('<div class="notchLabels">');
			var discreteScale='';
			if(self.discreteScale){discreteScale=" discreteScale";}
			var rangeScale=$('<div class="rangeScale'+discreteScale+'">');
			rangeScale.append(bottomLine);
			rangeScale.append(notches);
			rangeScale.append(notchLabels);
			self.setScaleInfo();
			var range=self.max-self.min,left,notch,label,value;
			for(var i=0;i<self.points+1;i++){
				value=self.min+self.scaleStep*i;
				left=self.scaleStep*i*100/range;
			/* for(var i=self.min;i<=self.max;i+=self.step){
				left=(i-self.min)*100/range; */
				notch=$('<div class="scaleNotch" style="left:calc('+left+'% - 1px)">');
				label=value;
				if(self.to_fixed || self.to_fixed==0){label=label.toFixed(self.to_fixed);}
				if(self.displayFunction){
					var label=self.displayFunction(value);
					if(typeof label=='object'){label=label.html}
				}
				if(i===0){
					if(self.prepend_to){label=self.prepend_to+label;}
					if(self.append_to){label=label+self.append_to;}
				}
				notchLabel=$('<div class="notchLabel" style="left:calc('+left+'% - 23px)">'+label+'</div>');
				notches.append(notch);
				notchLabels.append(notchLabel);
			}
			return rangeScale;
		};
		(this.createHTML=function(){
			if(self.min==Number.POSITIVE_INFINITY || self.max==Number.NEGATIVE_INFINITY){return false;}
			var key=self.key;
			if(self.withScale){
				self.rangeScale=self.createScale();
			}else{self.rangeScale='';}
			self.currentLow=self.min;
			self.currentHi=self.max;
			self.input1=$('<input class="'+key+' range range1" data-key="'+key+'" type="number" name="'+key+'_range1" min="'+self.min+'" max="'+self.max+'" value="'+self.currentLow+'" step="'+self.step+'"/>');
			self.input2=$('<input class="'+key+' range range2" data-key="'+key+'" type="number" name="'+key+'_range2" min="'+self.min+'" max="'+self.max+'" value="'+self.currentHi+'" step="'+self.step+'"/>');
			self.rangeInputWrap=$('<div class="rangeInputWrap cf">')
				.append(self.input1)
				.append(self.input2);
			self.handle1=$('<div class="'+key+' rangeHandle handle1 topHandle" data-for="range1" data-rangenum="1" data-output="rangeOutput1">');
			self.handle2=$('<div class="'+key+' rangeHandle handle2" data-for="range2" data-rangenum="2" data-output="rangeOutput2">');
			self.output1=$('<div>').addClass('rangeValue rangeLabelMin rangeWrapEle')
				.append($('<output>').addClass(key+' rangeOutput rangeOutput1').data('key',key).data('range','range1'));
			self.output2=$('<div>').addClass('rangeValue rangeLabelMax rangeWrapEle')
				.append($('<output>').addClass(key+' rangeOutput rangeOutput2').data('key',key).data('range','range2'));
			self.rangeTrack=$('<div class="rangeTrack">');
			if(self.withAvg){
				var withAvgLbl=self.withAvgLbl || '';
				self.avgIndicator=$('<div class="avgIndicator" title="Average">');
				self.avgLabelAvg=$('<span class="avgLabelAvg">');
				self.avgLabelContent=$('<div class="avgLabelContent">')
					.append(withAvgLbl+': ')
					.append(self.avgLabelAvg);
				self.avgLabel=$('<div class="avgLabel" title="'+withAvgLbl+'.">')
					.append(self.avgLabelContent);
				self.rangeTrack.append(self.avgIndicator)
			}
			if(self.showInputs){
				var toSee1;
				var toSee2;
				if(self.isTime){
					self.timeInput1=new Time({
						value:self.currentLow,
						name:key+'_showInput1',
						classes:key+' showInput1 showInputs dateShowInput',
						wrapClasses:'showInputsWrap showInputsWrap1 showInputRange_'+key,
						data:[['key',key],['key',key]]
					});
					self.showInput1=self.timeInput1.valInput;
					self.timeInput2=new Time({
						value:self.currentHi,
						name:key+'_showInput2',
						classes:key+' showInput2 showInputs dateShowInput',
						wrapClasses:'showInputsWrap showInputsWrap2 showInputRange_'+key,
						data:[['key',key]]
					});
					self.showInput2=self.timeInput2.valInput;
					self.showInput1.on('change',function(){self.showInputChange('min',$(this).val());});
					self.showInput2.on('change',function(){self.showInputChange('max',$(this).val());});
					toSee1=self.timeInput1.html;
					toSee2=self.timeInput2.html;
				}else if(self.isDate){
					var low=main.utilities.formatDate(Number(self.currentLow));
					var hi=main.utilities.formatDate(Number(self.currentHi));
					self.showInput1=$('<input class="'+key+' showInput1 showInputs dateShowInput" data-key="'+key+'" type="text" name="'+key+'_showInput1" value="'+self.currentLow+'"/>');
					self.showInput2=$('<input class="'+key+' showInput2 showInputs dateShowInput" data-key="'+key+'" type="text" name="'+key+'_showInput2" value="'+self.currentHi+'"/>');
					self.showInput1.datepicker();
					self.showInput1.datepicker('setDate',low);
					self.showInput2.datepicker();
					self.showInput2.datepicker('setDate',hi);
					self.showInput1.on('change',function(){self.showInputChange('min',main.utilities.getTimeStamp($(this).val()));$(this).blur();});
					self.showInput2.on('change',function(){self.showInputChange('max',main.utilities.getTimeStamp($(this).val()));$(this).blur();});
					toSee1=self.showInput1;
					toSee2=self.showInput2;
				}else{
					self.showInput1=$('<input class="'+key+' showInput1 showInputs" data-key="'+key+'" type="number" name="'+key+'_showInput1" min="'+self.min+'" max="'+self.max+'" value="'+self.currentLow+'" step="'+self.step+'"/>');
					self.showInput2=$('<input class="'+key+' showInput2 showInputs" data-key="'+key+'" type="number" name="'+key+'_showInput2" min="'+self.min+'" max="'+self.max+'" value="'+self.currentHi+'" step="'+self.step+'"/>');
					self.showInput1.on('change',function(){self.showInputChange('min',$(this).val());});
					self.showInput2.on('change',function(){self.showInputChange('max',$(this).val());});
					toSee1=self.showInput1;
					toSee2=self.showInput2;
				}
				self.rangeInputWrap
					.append(toSee1)
					.append(toSee2);
				self.output1.addClass('none');
				self.output2.addClass('none');
			}
			self.rangeWrap=$('<div class="rangeWrap">').data('key',key);
			self.rangePkgTop=$('<div class="rangePkgTop cf">');
			if(self.alias){
				self.label=$('<div>').addClass('rangeLabelWrap rowLabel rangeWrapEle rangeTopEle')
					.append($('<div>').addClass('rangeLabel rowEle').html(self.alias))
			}else{self.label='';}
			self.rangePkgTop.append(self.label);
			if(self.moreInfo){
				self.moreInfo=new MoreInfo({
					content:self.moreInfoContent,
					title:self.moreInfoTitle
				});
				self.rangePkgTop.append(self.moreInfo.html);
			}
			self.rangeWrapWrap=$('<div>').addClass('rangeWrapWrap');
			self.htmlContent=$('<div>').attr('id',self.id).addClass('rangeHTMLWrap rangeOuterWrap noSelect '+self.classes)
				.append($('<div class="rangeTop cf">')
					.append(self.rangePkgTop)
					.append($('<div>').addClass('rangeOutputs rangeTopEle cf')
						.append(self.output1)
						.append(self.output2)
					)
				)
				.append($('<div>').addClass('rangeBottom')
					.append(self.rangeWrapWrap
						.append(self.rangeWrap
							.append(self.handle1)
							.append(self.handle2)
							.append(self.rangeTrack)
						)
						.append(self.rangeScale)
						.append(self.avgLabel)
						.append(self.rangeInputWrap)
					)
				)
			if(self.withExact){
				if(self.isTime){
					var low=Number(self.currentLow);
					self.timeExact=new Time({
						value:low,
						name:'exactRange_'+key,
						classes:'exactRange exactRange_'+key,
						data:[['key',key],['key',key]]
					});
					self.exactInput=self.timeExact.valInput;
					self.exactInput.on('change',function(){self.exactInputChange($(this).val());});
				}else if(self.isDate){
					var low=main.utilities.formatDate(Number(self.currentLow));
					self.exactInput=$('<input class="exactRange exactRange_'+key+' dateExactRange" data-key="'+key+'" type="text" name="exactRange_'+key+'" value="'+low+'"/>');
					self.exactInput.datepicker();
					self.exactInput.datepicker('setDate',low);
					self.exactInput.on('change',function(){self.exactInputChange(main.utilities.getTimeStamp($(this).val()));$(this).blur();});
				}else{
					self.exactInput=$('<input class="exactRange exactRange_'+key+'" data-key="'+key+'" type="text" name="exactRange_'+key+'" min="'+self.min+'" max="'+self.max+'" value="'+self.currentLow+'" step="'+self.step+'"/>');
					self.exactInput.on('change',function(){self.exactInputChange($(this).val());});
				}
				self.exactInputWrap=$('<div class="exactInputWrap">')
					.append($('<div class="exactInputWrapRow cf">')
						.append($('<div class="exactInputWrapLabel exactInputWrapRowEle">').html('Exact Value <span class="small">(Separate multiple values with a comma)</span>:'))
						.append($('<div class="exactInputWrapLabel exactInputWrapRowEle">').html(self.exactInput))
					);
				self.tabs=new Tabs();
				self.rangeTab=self.tabs.addTab('Range',self.htmlContent,'rangeTab');
				self.exactTab=self.tabs.addTab('Exact',self.exactInputWrap,'rangeTab');
				self.tabs.setActiveTab(self.rangeTab.tabid);
				self.html=self.tabs.html;
			}else{self.html=self.htmlContent;}
		})();
		this.exactInputChange=function(val){
			if(val.indexOf(',')==-1 && val){
				val=Number(val);
				if(val<self.min){val=self.min;}
				if(val>self.max){val=self.max;}
				self.currentLow=val;
				self.currentHi=val;
				//correct to show all day
				if(self.isDate){
					self.currentHi=val+(1000*60*60*24-1);
					if(self.currentHi>self.max){self.currentHi=self.max;}
				}
				self.updateAll();
			}else{self.hasSpecifics=val.trim();}
		}
		this.showInputChange=function(which,val){
			var val=Number(val);
			if(val>self.max){val=self.max;}
			if(val<self.min){val=self.min;}
			self.html.find('.topHandle').removeClass('topHandle');
			if(which=='min'){
				self.setValue([val,self.currentHi]);
				var currentHandle=self.handle1;
			}else if(which=='max'){
				//correct to show all day
				if(self.isDate){
					val=val+(1000*60*60*24-1);
					if(val>self.max){val=self.max;}
				}
				self.setValue([self.currentLow,val]);
				var currentHandle=self.handle2;
			}
			self.updateAll(currentHandle.addClass('topHandle'),val);
		};
		this.updateExtremes=function(max,min,reset,extraParams){
			self.min=min;
			self.max=max;
			self.input1.add(self.input2).attr('min',min).attr('max',max);
			if(self.currentHi>self.max){self.currentHi=self.max;}
			if(self.currentLow<self.min){self.currentLow=self.min;}
			self.updateAll();
			if(self.withScale){
				var rangeScale=self.createScale();
			}else{var rangeScale='';}
			if(self.rangeScale){
				self.rangeScale.replaceWith(rangeScale);
			}
			self.rangeScale=rangeScale;
			if(reset){self.reset(null,extraParams);}
		};
		/* this.setValue=function(value){
			self.currentLow=value;
			var percent=value*100/(self.max-self.min);
			if(value.length==1){
				self.handle1.css('left','calc('+percent+'% - '+self.halfHandleWidth+'px');
			}else{
				
			}
			self.updateValues();
			self.updateOutputs();
		} */
		this.correctOtherHandle=function(rangeNum,val,dontUpdateOthers){
			var movingHandle=self.rangeWrap.find('.handle'+rangeNum);
			var otherHandle=self.rangeWrap.find('.rangeHandle').not(movingHandle);
			var corrected=false;
			if(rangeNum==1 && val>self.currentHi){
				self.currentHi=val;
				corrected=true;
			}else if(rangeNum==2 && val<self.currentLow){
				self.currentLow=val;
				corrected=true;
			}
			if(corrected){
				otherHandle.css('left',movingHandle[0].style.left);
				if(!dontUpdateOthers){
					self.updateValues();
					self.updateOutputs();
				}
			}
		};
		this.moveHandle=function(e){
			var currentHandle=self.currentHandle;
			var otherHandle=self.rangeWrap.find('.rangeHandle').not(currentHandle);
			var rangeWrapWidth=self.rangeWrap.width();
			var otherHandlePos=parseInt(otherHandle.css('left'));
			if(!main.browser.isMobile){var pageX=e.pageX;
			}else{var pageX=e.originalEvent.touches[0].pageX;}
			var leftValue=pageX-self.rangeWrap.offset().left;
			if(leftValue<0)leftValue=0;
			if(leftValue>rangeWrapWidth){leftValue=rangeWrapWidth;}
			var otherHandleValue=otherHandlePos+self.halfHandleWidth;
			var value=(leftValue*(self.max-self.min)/rangeWrapWidth)+self.min;
			if(self.round){ 
				self.setScaleInfo();
				var val,nextVal;
				for(var i=0;i<self.points*(1/self.round)+1;i++){ //  1/self.round to invert and make sure the loop runs enough for all the new values 
					val=self.min+((self.scaleStep*self.round)*i);
					nextVal=self.min+((self.scaleStep*self.round)*(i+1));
					if(val==value){
						value=val;
						break;
					}
					if(val==nextVal){
						value=nextVal;
						break;
					}
					if(value>val && value<nextVal){
						valDif=value-val;
						nextValDif=nextVal-value;
						if(valDif<=nextValDif){value=val;
						}else{value=nextVal;}
						break;
					}
				}
				leftValue=(value-self.min)*rangeWrapWidth/(self.max-self.min);
			}
			var currentLeft=leftValue*100/rangeWrapWidth;
			if(currentHandle.hasClass('handle1')){self.currentLow=value;
			}else{self.currentHi=value;}
			currentHandle.css('left','calc('+currentLeft+'% - '+self.halfHandleWidth+'px');
			self.correctOtherHandle(currentHandle.data('rangenum'),value,true);
			// if((currentHandle.hasClass('handle1') && leftValue>otherHandleValue) || (currentHandle.hasClass('handle2') && leftValue<otherHandleValue)){
				// if(currentHandle.hasClass('handle1')){self.currentHi=value;
				// }else{self.currentLow=value;}
				// otherHandle.css('left','calc('+currentLeft+'% - '+self.halfHandleWidth+'px');
			// }
			self.updateValues();
			self.updateOutputs();
		};
		(this.updateHandles=function(currentHandle,val){
			var left1=(self.currentLow-self.min)*100/(self.max-self.min);
			var left2=(self.currentHi-self.min)*100/(self.max-self.min);
			self.handle1.css('left','calc('+left1+'% - '+self.halfHandleWidth+'px');
			self.handle2.css('left','calc('+left2+'% - '+self.halfHandleWidth+'px');
			if(currentHandle && (val || val==0)){self.correctOtherHandle(currentHandle.data('rangenum'),val);}
		})();
		this.getRelativeOutput=function(val){
			var range=self.max-self.min;
			var location=(val-self.min)*100/range;
			var relativeValuesLength=self.relativeValues.length;
			var relativeValuesStep=100/relativeValuesLength;
			for(var i=0;i<relativeValuesLength;i++){
				if(i!==relativeValuesLength-1){
					if(location>=relativeValuesStep*i && location<relativeValuesStep*(i+1)){
						return self.relativeValues[i];
					}
				}else{
					if(location>=relativeValuesStep*i && location<=relativeValuesStep*(i+1)){
						return self.relativeValues[i];
					}
				}
			}
		};
		(this.updateOutputs=function(e){
			if(self.relativeValues){
				self.output1.html(self.getRelativeOutput(self.currentLow));
				self.output2.html(self.getRelativeOutput(self.currentHi));
			}else{
				if(self.displayFunction){
					var lowDisp=self.displayFunction(self.currentLow);
					if(typeof lowDisp=='object'){lowDisp=lowDisp.html}
					self.output1.html(lowDisp);
					var hiDisp=self.displayFunction(self.currentHi);
					if(typeof hiDisp=='object'){hiDisp=hiDisp.html}
					self.output2.html(hiDisp);
				}else{
					if(self.currentLow!==undefined && self.currentLow!==undefined){
						self.output1.html(self.prepend_to+self.currentLow.toFixed(self.to_fixed)+self.append_to);
						self.output2.html(self.prepend_to+self.currentHi.toFixed(self.to_fixed)+self.append_to);
					}
				}
			}
		})();
		this.reset=function(noOnChange,extraParams){
			if(self.value){
				self.setValue(self.value);
			}else{
				self.currentLow=self.min;
				self.currentHi=self.max;
			}
			self.updateAll();
			if(self.onChange && !noOnChange){
				extraParams=extraParams || null;
				self.onChange(self.getRangePkg(),extraParams);
			}
		};
		this.updateValues=function(){
			var low=self.currentLow.toFixed(self.to_fixed);
			var hi=self.currentHi.toFixed(self.to_fixed);
			self.input1.val(low);
			self.input2.val(hi);
			if(self.showInput1){
				if(self.isTime){
					self.showInput1.val(Number(self.currentLow));
					self.showInput2.val(Number(self.currentHi));
					self.timeInput1.updateShowInputs();
					self.timeInput2.updateShowInputs();
				}else if(self.isDate){
					self.showInput1.datepicker('setDate',main.utilities.formatDate(Number(self.currentLow)));
					self.showInput2.datepicker('setDate',main.utilities.formatDate(Number(self.currentHi)));
				}else{
					self.showInput1.val(low);
					self.showInput2.val(hi);
				}
			}
		};
		this.getRangePkg=function(){
			// var isFull=false;
			// if(self.currentHi===self.max && self.currentLow===self.min){isFull=true}
			return {
				currentHi:self.currentHi,
				currentLow:self.currentLow,
				min:self.min,
				max:self.max,
				key:self.key,
				hasSpecifics:self.hasSpecifics,
				html:self.html
			}
		};
		(this.updateForHandlesLength=function(){
			if(self.handlesLength===1){
				self.handle2.addClass('none');
				self.input2.addClass('none');
				self.label.addClass('labelForOne');
				self.output1.addClass('onlyOutput');
				self.output2.addClass('none');
				self.label.addClass('bringDownLabel');
			}
			if(self.noOutputs){
				self.output1.addClass('none');
				self.output2.addClass('none');
			}
		})();
		this.setAvg=function(avg){
			var left=(avg-self.currentLow)*100/(self.currentHi-self.currentLow)
			self.avgIndicator.css('left','calc('+left+'% - 1px)');
			var label=avg;
			if(self.to_fixed || self.to_fixed==0){label=label.toFixed(self.to_fixed)}
			if(self.prepend_to){label=self.prepend_to+label;}
			if(self.append_to){label=label+self.append_to;}
			self.avgLabelAvg.html(label);
		};
		this.mouseMoving=function(e){
			self.moveHandle(e);
			if(self.onMove){self.onMove(self.getRangePkg(),e);}
		};
		this.mouseUp=function(){
			self.handleMouseDown=false;
			// self.html.removeClass('noSelect');
			self.html.removeClass('pointerCursor');
			self.currentHandle=null;
			if(self.onChange){self.onChange(self.getRangePkg());}
			$('html').off(main.controls.touchMove,self.mouseMoving);
			$('html').off(main.controls.touchEnd,self.mouseUp);
		};
		(this.initEvents=function(){
			self.html.on(main.controls.touchBegin,'.rangeHandle',function(e){
				// self.html.addClass('noSelect');
				self.html.addClass('pointerCursor');
				self.handleMouseDown=true;
				self.html.find('.topHandle').removeClass('topHandle');
				self.currentHandle=$(this).addClass('topHandle');
				$('html').on(main.controls.touchMove,self.mouseMoving);
				$('html').on(main.controls.touchEnd,self.mouseUp);
			});
			self.rangeTrack.on('click',function(e){
				var handle1=self.rangeWrap.find('.handle1');
				var handle2=self.rangeWrap.find('.handle2');
				handle1Delta=Math.abs(e.pageX-(handle1.offset().left+self.halfHandleWidth));
				handle2Delta=Math.abs(e.pageX-(handle2.offset().left+self.halfHandleWidth));
				if(handle1Delta<handle2Delta){self.currentHandle=handle1.addClass('topHandle')
				}else{self.currentHandle=handle2.addClass('topHandle')}
				self.moveHandle(e);
				if(self.onChange){self.onChange(self.getRangePkg(),e);}
			});
		})();
		this.setValue=function(values){
			if(values.length==1){
				self.currentLow=values[0];
				if(self.handlesLength>1){
				}
			}else if(values.length==2){
				if(values[0] || values[0]==0){self.currentLow=values[0];
				}else{self.currentLow=self.min;}
				if(values[1] || values[1]==0){self.currentHi=values[1];
				}else{self.currentHi=self.max;}
			}else{
				return;
			}
		};
		if(self.value){
			self.setValue(self.value);
			self.updateAll();
		}
	}
});