define(function(require){
	return function pg(args){
		var self=this;
		var Layer2=require('./Layer2');
		var PopUp=require('./PopUp');
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.popupClosed=function(args){
			if(args.pid==main.mapItems.hasActive && main.mapItems.hasActiveLayer==self.context.layerName){
				self.context.hasPopUp=null;
				self.context.hasPopUpOtherFeatures=null;
				self.context.hasPopUpCurrentIndex=null;
				self.context.popUpZ=null;
			}
		};
		this.showPopup=function(pid,feature,otherFeatures,isNew,dontMakeActive,popUpZ,currentIdx){
			if(self.context.onePopUpAtTime){
				main.layout.removeAllPopUps();
			}
			if(self.context.map_layer.features[pid].popup){
				self.context.map_layer.features[pid].popup.container.remove();
				delete self.context.map_layer.features[pid].popup;
			}
			if(!self.context.map_layer.features[pid].popup){
				// var geo=feature.getGeometry().getCoordinates();
				var geo=main.utilities.getCoords(feature);
				if(!self.context.map_layer.features[pid].feature){
				self.context.map_layer.features[pid].feature=feature;}
				var properties=feature.getProperties();
				self.createFeatureHTML(properties);
				if(properties.category_unique_id){
					var topLabelContent=self.context.singular_alias+' ID '+properties.category_unique_id;
				}
				else{
					var topLabelContent=self.context.singular_alias+' ID '+pid;
				}
				if(self.context.properties.internal_field_map && self.context.properties.internal_field_map.species_common){
					var val=main.utilities.getLookUpTableHTML('species',properties[self.context.properties.internal_field_map.species_common],self.context.properties.internal_field_map.species_common,self.context.layerName,self.context.fields[self.context.properties.internal_field_map.species_common]);
					if(val){topLabelContent+=' - '+val;}
				}
				// var editForm=null;
				// console.log(self.context.map_layer.features[pid].editForm);
				// if(self.context.map_layer.features[pid].editForm){editForm=self.context.map_layer.features[pid].editForm;}
				self.context.map_layer.features[pid].popup=new PopUp({
					position:geo,
					properties:properties,
					html:self.context.map_layer.features[pid].editTable.table,
					removeMarkerOnPopUpClose:false,
					map:self.context.map,
					topLabelContent:topLabelContent,
					layer:self.context.layer.layer,
					context:self.context,
					classes:'inventoryPopUp',
					otherFeatures:otherFeatures,
					// editForm:editForm,
					currentIdx:currentIdx,
					isNew:isNew,
					dontMakeActive:dontMakeActive,
					popUpZ:popUpZ,
					onClickClose:self.popupClosed
				});
				main.layout.popups.push(self.context.map_layer.features[pid].popup);
				if(self.context.inventory){self.context.map_layer.features[pid].inventory=self.context;}
				self.context.map_layer.features[pid].editTable.tbody.on('change','.formInput',function(e){
					self.context.inputChanged($(this),e);
				});
			}else{
				self.context.map_layer.features[pid].popup.showPopup(null,dontMakeActive,popUpZ/* ,otherFeatures */);
			}
			self.context.hasPopUp=properties.pid;
			self.context.hasPopUpOtherFeatures=otherFeatures;
			self.context.hasPopUpCurrentIndex=self.context.map_layer.features[pid].popup.currentIdx;
			self.context.popUpZ=main.misc.maxZIndex;
		};
		this.createFeatureHTML=function(properties){
			var fields=self.context.fields,field,value,key;
			var pid=properties.pid;
			self.context.map_layer.features[pid].fields={};
			self.context.map_layer.features[pid].editTable={};
			self.context.map_layer.features[pid].editTable.tbody=$('<tbody>');
			if(!self.context.sortedFieldKeys){self.context.sortedFieldKeys=main.utilities.sortFields(fields,'place_in_order');}
			for(var i=0;i<self.context.sortedFieldKeys.length;i++){
				key=self.context.sortedFieldKeys[i];
				field=fields[key];
				if(field.active){
					value=properties[key];
					if(field.data_type=="geometry"){value=self.context.map_layer.features[pid].properties[key];}
					self.context.map_layer.features[pid].fields[key]={
						current_val:value,
						html:value,
						field:field
					};
					main.utilities.getRowThings(self.context.map_layer.features[pid],field,pid,{callback:self.context.updateLookUpTableFields,params:{pid:pid,fieldName:key,value:value}},{callback:self.context.newSignature},null,null,self.context.layerName,true,null,null,true);
					if(field.in_popup && field.input_type!='hidden' && field.input_type!='file'){
						self.context.map_layer.features[pid].editTable.tbody.append($('<tr class="popUpRow '+key+'" data-field="'+key+'">')
							.append($('<td>')
								.append($('<div class="tdLiner popupRowLabel">').text(field.alias))
							)
							.append($('<td class="outerEditingW notEditing">')
								.append($('<div class="tdLiner">')
									.append(
										$('<div data-field="'+key+'">').addClass('tdLiner popUpEditPkg editPkgWrap '+key)
											.append($('<div>').addClass('dataTableCurrent '+key).data('field',key).html(self.context.map_layer.features[pid].fields[key].html))
											.append((self.context.map_layer.features[pid].fields[key].editPackage || ''))
									)
								)
							)
						)
					}
					if(field.input_type=='file'){
						// main.tables[self.layerName].atchs.items[pid].fields[field.name].puCurrentPhotos.setRemoveCallBack(self.photoRemoved);
						// main.tables[self.layerName].atchs.items[pid].fields[field.name].puCurrentPhotos.setOnClick(main.utilities.photoELClicked);
						/* // self.inventory.map_layer.features[pid].fields[field.name].input.currentPhotos.setRemoveCallBack(self.photoRemoved);
						// self.inventory.map_layer.features[pid].fields[field.name].input.currentPhotos.setOnClick(main.utilities.photoELClicked); */
					}
				}
			}
			self.context.map_layer.features[pid].editTable.table=$('<table class="popupTable dataUnit" data-pid="'+pid+'">')
				.append($('<thead>')
					.append($('<tr>')
						.append($('<th colspan="2">')
							.append($('<div class="tdLiner">').text(self.context.singular_alias))
						)
					)
				)
				.append(self.context.map_layer.features[pid].editTable.tbody);
			if(self.context.popupsUneditable){main.utilities.tableToUneditable(self.context.map_layer.features[pid].editTable.table);}
		};
		this['add_map_layer_'+self.context.layerName]=function(layerName){
			// if(!main.browser.isMobile){self.context.dataTableAllocate();}
			main.map_layers[self.context.layerName].firstLoad=true;
			if(self.context.layer.properties.default_point_icon){
				self.context.iconSource=main.localImgs+self.context.layer.properties.default_point_icon;
				self.context.anchor=[0.5,0.5];
				if(self.context.layer.properties.point_icon_offset){self.context.anchor=self.context.layer.properties.point_icon_offset};
			}
			if(self.context.layer.layerObj){
				var options={features: (new ol.format.GeoJSON()).readFeatures(self.context.layer.layerObj)};
			}else{var options={};}
			var firstPostRender=null;
			if(main.map.mainLegend){firstPostRender=main.map.mainLegend.addItem;}
			self.context.defaultStyles={};
			self.context.defaultStyles.fill_color=self.context.properties.fill_color;
			self.context.defaultStyles.stroke_color=self.context.properties.stroke_color;
			self.context.defaultStyles.point_radius=self.context.properties.point_radius;
			self.context.defaultStyles.stroke_width=self.context.properties.stroke_width;
			self.context.defaultStyles.z_index=self.context.properties.z_index;
			var visible=true;
			if(self.context.delayVis){visible=false;}
			// if(main.filter_legend && (main.filter_legend.layerName==layerName || self.context.inventory.overRideDependentOn)){visible=false;}
			if(main.filter_legend && main.filter_legend.layerName!=layerName){visible=false;}
			self.context.layer.layer=new Layer2({
				id:layerName,
				source:new ol.source.Vector(options),
				iconSrc:self.context.iconSource,
				anchor:self.context.anchor,
				visible:visible,
				markerSource:new ol.source.Vector(),
				map:main.map,
				doneDragCB:self.context.moved,
				opacity:0.9,
				firstPostRender:firstPostRender,
				properties:self.context.layer.properties,
				defaultStyles:self.context.defaultStyles
			});
			self.context.layer.layer.olLayer.setVisible(visible);
			main.map.registerLayer(self.context.layer.layer);
			// main.map.map.on('click',self.onMapClick);
			self.context.layerLoaded=true;
					// if(main.filter_legend && main.filter_legend.withOtherLayers){main.filter_legend.addNewOtherLayer(main.map_layers[layerName].layer);}
			if(main.filter_legend){
				if(main.filter_legend.layerName==layerName || (self.context.inventory && self.context.inventory.overRideDependentOn)){
					main.invents.currentInvent=self.context.layerName;
					main.filter_legend.layerLoaded(self.context.layerName,self.context.updateFilterLegend);
					if(self.context.updateFilterLegend){
						// self.context.layer.layer.olLayer.setVisible(false);
						main.filter_legend.changeLayer(self.context.layerName);
						self.context.updateFilterLegend=false;
					}else{
						self.context.layer.layer.olLayer.setVisible(true);
					}
					main.loader.loadMoreMapPoints(layerName,self.context.delayVis);
				}
				// if(main.filter_legend.active && main.filter_legend.layerName==layerName){
					// main.loader.loadMoreMapPoints(layerName,null,true);
				// }
				if(main.data.map_layers[layerName] && self.context.layer.properties.max_res && Number(self.context.layer.properties.max_res)>main.map.map.getView().getResolution()){
					main.loader.loadMoreMapPoints(layerName,self.context.delayVis);
				}
			}
			if(main.dashboard && main.dashboard.inventories && main.dashboard.inventories.indexOf(layerName)>-1){
				main.dashboard.layerLoaded(layerName);
			}
			if(main.labels && self.context.properties.start_with_labels){
				main.labels.mainLabelLayerSelect.val(self.context.layerName).change();
				main.labels.mainLabelFieldSelect.val(self.context.properties.default_labels_field).change();
				main.labels.turnOn();
			}
		};
	};
});