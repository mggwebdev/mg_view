define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var ToolTip=require('./ToolTip');
		this.id=main.utilities.generateUniqueID();
		this.layerAdded=false;
		this.selecting=false;
		this.type='Point';
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		// this.iconFeatures={};
		// this.values={};
		// this.iconStyle=new ol.style.Style({
			// image:new ol.style.Icon(({
				// anchor:[0.5,0.5],
				// anchorXUnits:'fraction',
				// anchorYUnits:'fraction',
				// opacity:0.75,
				// src:main.localImgs+main.personal_settings.map.default_map_point_icon
			// }))
		// });
		self.tooltipContent='';
		if(self.type=='Point'){
			self.tooltipContent='Click to select a point.'
		}else if(self.type=='Polygon'){
			self.tooltipContent='To finish, double click or click the first point. To freehand draw, hold down shift.'
		}
		(this.create=function(){
			/* var origVal=this.default_val;
			value='';
			if(origVal){
				var pointData=main.utilities.getGeometryData(value);
				
				if(typeof origVal=='object'){
					var pointData=main.utilities.getGeometryData(value);
					if(pointData){
						for(var i=0;i<pointData.coords.length;i++){
							self.addFeature(pointData.coords[i],false,false);
						}
						value=pointData.valStr;
					}
				}else if(typeof origVal=="string"){
					var valSplit=origVal.split(','),coordSplit;
					for(var i=0;i<valSplit.length;i++){
						coordSplit=valSplit[i].split(' ');
						self.addFeature(ol.proj.transform([Number(coordSplit[0]),Number(coordSplit[1])], 'EPSG:4326', 'EPSG:3857'),false,false);
					}
					value=origVal;
				}
			} */
			// var pointData=main.utilities.getGeometryData(value);
			self.startButton=new Button('std','mapPointStart mapPointButton fullWidth','Select From Map',null,null,null,'Select a point from the map. To delete a point, hold down shift and click the corresponding icon.');
			self.stopButton=new Button('std','mapPointStop mapPointButton fullWidth','Stop Selecting',null,null,null,'Stop Selecting');
			self.showSelectedButton=new Button('std','showMapPoints mapPointButton fullWidth','Show',null,null,null,'Show');
			self.removeSelectedButton=new Button('std','removeMapPoints mapPointButton fullWidth','Hide',null,null,null,'Hide');
			self.input=$('<input name="'+self.id+'" type="hidden">').addClass('mapPointInput formInput '+self.id+' '+self.key).data('name',self.id);
			self.container=$('<div>').addClass('mapPoint')
				.append(self.input)
				.append($('<div>').addClass('mapPointButtons cf')
					.append(self.startButton.html)
					.append(self.stopButton.html)
					.append(self.showSelectedButton.html)
					.append(self.removeSelectedButton.html)
				);
			self.toolTip=new ToolTip({
				content:self.tooltipContent
			});
			self.startButton.html.on('click',function(e){
				self.startSelecting();
			});
			self.stopButton.html.on('click',function(e){
				self.stopSelecting();
			});
			self.showSelectedButton.html.on('click',function(e){
				self.showFeatures();
			});
			self.removeSelectedButton.html.on('click',function(e){
				self.hideFeatures();
			});
			self.source = new ol.source.Vector({wrapX: false});
			self.geojson=new ol.format.GeoJSON();
			if(self.origValue){
				self.source.addFeatures((self.geojson).readFeatures(self.origValue));
				// self.source.addFeatures((self.geojson).readFeatures(self.origValue.obj));
			}
			self.features=self.source.getFeatures();
			self.vector = new ol.layer.Vector({
				source: self.source,
				style: new ol.style.Style({
					fill: new ol.style.Fill({
						color: 'rgba(255, 255, 255, 0.2)'
					}),
					stroke: new ol.style.Stroke({
						color: '#ffcc33',
						width: 2
					}),
					image: new ol.style.Circle({
						radius: 7,
						fill: new ol.style.Fill({
							color: '#ffcc33'
						})
					})
				})
			});
			self.draw=new ol.interaction.Draw({
				source: self.source,
				type: self.type
			});
			self.source.on('addfeature',function(e){
				if(self.type=='Polygon'){
					if(main.utilities.isIntersecting(e.feature)){
						alert('Polygons can not intersect themselves. Please redraw with no intersecting line segments.');
						self.source.removeFeature(e.feature);
						return;
					}
				}
				self.features=self.source.getFeatures();
				self.updateInputVal();
			});
		})();
		this.clear=function(){
			self.stopSelecting();
			self.removeAllFeatures(true);
			self.updateInputVal();
			self.removeSelectedButton.html.removeClass('block');
			self.showSelectedButton.html.removeClass('none');
		};
		this.updateInputVal=function(){
			self.featureGJson=self.geojson.writeFeatures(self.features);
			self.input.val(self.featureGJson).change();
		};
		this.startSelecting=function(){
			self.map.clearOtherPActions();
			self.startButton.html.addClass('none');
			self.stopButton.html.addClass('inlineBlock');
			self.selecting=true;
			self.map.selecting=true;
			self.toolTip.activate();
			self.addDraw();
		};
		this.stopSelecting=function(){
			self.stopButton.html.removeClass('inlineBlock');
			self.startButton.html.removeClass('none');
			self.selecting=false;
			self.map.selecting=false;
			self.toolTip.deActivate();
			self.removeDraw();
		};
		this.showFeatures=function(){
			if(!self.layerAdded){self.map.map.addLayer(self.vector);}
			self.layerAdded=true;
			self.showSelectedButton.html.addClass('none');
			self.removeSelectedButton.html.addClass('block');
		};
		this.hideFeatures=function(){
			if(self.layerAdded){self.map.map.removeLayer(self.vector);}
			self.layerAdded=false;
			self.showSelectedButton.html.removeClass('none');
			self.removeSelectedButton.html.removeClass('block');
		};
		this.addDraw=function(){
			if(!self.layerAdded){self.showFeatures();}
			if(!self.drawAdded){self.map.map.addInteraction(self.draw);}
			self.drawAdded=true;
		};
		this.removeDraw=function(){
			// if(!self.layerAdded){self.hideFeatures();}
			if(self.drawAdded){self.map.map.removeInteraction(self.draw);}
			self.drawAdded=false;
		};
		this.removeFeature=function(feature){
			self.source.removeFeature(feature);
			self.features=self.source.getFeatures();
			self.updateInputVal();
		};
		
		this.removeAllFeatures=function(deleteToo){
			for(var key in self.features){
				if(self.features[key]){
					self.source.removeFeature(self.features[key]);
				}
			}
			self.features=self.source.getFeatures();
		};
		
		
		/* this.mapClickedForSelect=function(e){
			e.stopImmediatePropagation();
			self.addFeature(self.map.map.getEventCoordinate(e.originalEvent),true,true);
			self.updateInputVal();
			if(self.changeCallback){self.changeCallback(self);}
		};
		this.startSelecting=function(){
			self.map.clearOtherPActions();
			self.startButton.html.addClass('none');
			self.stopButton.html.addClass('inlineBlock');
			self.selecting=true;
			self.map.selecting=true;
			self.toolTip.activate();
			$(self.map.map.getTargetElement()).on('click',self.mapClickedForSelect);
		};
		this.stopSelecting=function(){
			self.stopButton.html.removeClass('inlineBlock');
			self.startButton.html.removeClass('none');
			self.selecting=false;
			self.map.selecting=false;
			self.toolTip.deActivate();
			$(self.map.map.getTargetElement()).off('click',self.mapClickedForSelect);
		};
		this.removeAllFeatures=function(deleteToo){
			for(var key in self.values){
				self.layerSource.removeFeature(self.values[key].feature);
				self.values[key].iconOnMap=false;
				if(deleteToo){delete self.values[key];}
			}
			self.removeSelectedButton.html.removeClass('inlineBlock');
			self.showSelectedButton.html.removeClass('none');
		};
		this.removeFeature=function(key,deleteToo){
			self.layerSource.removeFeature(self.values[key].feature);
			if(deleteToo){delete self.values[key];}
		};
		this.updateInputVal=function(){
			var value='',coods,latlng;
			for(var key in self.values){
				coods=self.values[key].feature.getGeometry().getCoordinates();
				latlng=ol.proj.transform(coods,'EPSG:3857','EPSG:4326');
				value+=latlng[0]+' '+latlng[1]+',';
			}
			value=value.replace(/,([^,]*)$/,'');
			self.input.val(value).change();
		};
		this.showSelected=function(){
			if(Object.keys(self.values).length>0){
				for(var key in self.values){
					if(!self.values[key].iconOnMap){
						self.layerSource.addFeature(self.values[key].feature);
						self.values[key].iconOnMap=true;
					}
				}
			}
		};
		this.doneDragging=function(){
			self.updateInputVal();
		};
		this.addFeature=function(coordinate,addIconToMap,stopAfterSelect){
			var geo=new ol.geom.Point(coordinate);
			var iconFeature=new ol.Feature({
				geometry: geo,
				draggable:true,
				doneDragCB:self.doneDragging
			});
			var id='icon_'+main.utilities.generateUniqueID();
			iconFeature.setProperties({'type':'select_location','id':id,'mapPoint':self.uniqueID});
			iconFeature.setStyle(self.iconStyle);
			// iconFeature.setStyle(new ol.style.Style({
				// fill:new ol.style.Fill({
					// color:'rgba(0, 255, 219,0.5);'
				// }),
				// stroke:new ol.style.Stroke({
					// color:'rgba(255,255,255,0.5)',
					// width:1
				// }),
				// image:new ol.style.Circle({
					// radius:5,
					// fill:new ol.style.Fill({
						// color:'rgba(0, 255, 219,0.5);'
					// })
				// })
			// }));
			if(addIconToMap){
				self.layerSource.addFeature(iconFeature);
			}
			self.values[id]={
				feature:iconFeature,
				iconOnMap:!!addIconToMap,
				id:id,
				type:'select_location',
				mapPoint:self.uniqueID
			}
			if(stopAfterSelect){self.stopSelecting();}
		},*/
		this.initEvents=function(){
			self.map.map.on('click',function(e){
				if(self.map.shiftDown){
					var cood=self.map.map.getEventCoordinate(e.originalEvent);
					var closestFeature=self.source.getClosestFeatureToCoordinate(cood);
					var props,matchedFeature;
					var pixel=main.utilities.getPixel(e);
					if(!pixel){return;}
					var features=[];
					self.map.map.forEachFeatureAtPixel(pixel,function(feature,layer){
						if(layer){
							if(closestFeature==feature){
								matchedFeature=feature;
								return false;
							}
						}
					});
					if(matchedFeature){
						e.originalEvent.stopImmediatePropagation();
						self.removeFeature(matchedFeature);
					}
				}
				/* if(self.map.shiftDown){
					var pixel=self.map.map.getEventPixel(e.originalEvent);
					var feature=self.map.map.forEachFeatureAtPixel(pixel,function(feature,layer){
						return feature;
					});
					if(feature){
						var props=feature.getProperties();
						if(props.type=='select_location'){
							e.originalEvent.stopImmediatePropagation();
							self.removeFeature(props.id,true);
							self.updateInputVal();
							// if(self.mapPoints[key].changeCallback){self.mapPoints[key].changeCallback(self.mapPoints[key]);}
						}
					}
				} */
			});
		};
		this.initEvents();
		self.map.mapPoints[this.uniqueID]=this;
	};
});