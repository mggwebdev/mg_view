define(function(require){
	return function pg(args){
		var self=this;
		this.defaultWidth=800;
		this.defaultHeight=800;
		this.width=this.defaultWidth;
		this.height=this.defaultHeight;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.layers=[];
		this.boundMap=function(params){
			var bounds={
				max_x:-Infinity,
				max_y:-Infinity,
				min_x:Infinity,
				min_y:Infinity
			},array,coords,features;
			for(var i=0;i<self.layers.length;i++){
				features=self.layers[i].getSource().getFeatures();
				if(features.length){
					main.v=features[0];
					for(var b=0;b<features.length;b++){
						coords=features[b].getGeometry().getCoordinates();
						if(coords[0].constructor===Array){array=coords;
						}else{array=[coords];}
						if(array[0][0].constructor===Array){array=array[0];}
						for(var c=0;c<array.length;c++){
							if(array[c][0]>bounds.max_x){bounds.max_x=array[c][0];}
							if(array[c][1]>bounds.max_y){bounds.max_y=array[c][1];}
							if(array[c][0]<bounds.min_x){bounds.min_x=array[c][0];}
							if(array[c][1]<bounds.min_y){bounds.min_y=array[c][1];}
						}
					}
				}
			}
			main.utilities.boundTo(bounds,self.map,self.boundToMapMove,params);
		}
		this.boundToMapMove=function(initialCenter,newCenter,initialZoom,newZoom,params){
			if(newZoom){
				if(main.map.google.showing){
					if(newZoom>main.map.maxGZoom){newZoom=main.map.maxGZoom;}
					if(newZoom<main.map.minGZoom){newZoom=main.map.minGZoom;}
				}else{
					if(newZoom>main.map.maxZoom){newZoom=main.map.maxZoom;}
					if(newZoom<main.map.minZoom){newZoom=main.map.minZoom;}
				}
				self.map.getView().setZoom(newZoom);
			}
			self.map.getView().setCenter(newCenter);
			setTimeout(function(){main.print.print(self.map,'server',self.printFileSaved,params)},1500);
		}
		this.printFileSaved=function(response,params){
			if(response.pathWName){params.mapPath=response.pathWName}
			if(params.cb){params.cb(params);}
		};
		this.removeAllLayers=function(){
			for(var i=0;i<self.layers.length;i++){
				self.map.removeLayer(self.layers[i]);
			}
		}
		this.printMap=function(layers,params){
			if(self.layers.length){self.removeAllLayers();}
			self.layers=layers;
			for(var i=0;i<layers.length;i++){
				self.map.addLayer(layers[i]);
			}
			if(self.baseTiles){
				self.map.removeLayer(self.baseTiles);
			}
			self.map.getLayers().insertAt(0,main.map.baseTiles);
			self.boundMap(params);
		};
		(this.createHTML=function(){
			self.container=$('<div id="mapPrintContainer">').width(self.width).height(self.height);
			$('body').append(self.container);
			self.view=new ol.View({
				center: [0,0],
				zoom: 7
			});
			var layers=[];
			self.baseTiles=null;
			if(main.map.baseTiles){
				self.baseTiles=main.map.baseTiles;
				layers.push(self.baseTiles);
			}
			// var layer=[main.map.getOSMTiles(main.globalSettings.tile_source.osm.default_map_type)];
			self.map = new ol.Map({
				target: self.container.attr('id'),
				layers:layers,
				view: self.view
			});
			self.container.addClass('none');
		})();
	};
});