define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		this.panelMarginX=10;
		this.panelMarginY=10;
		this.dragging=false;
		this.minimized=false;
		this.minHeight=100;
		this.minWidth=100;
		this.topContentWrapMargin=23;
		this.opened=false;
		this.scrollTopLimit=100;
		this.resize={
			resizing:false,
			resizingDir:null,
			coordinates:{}
		};
		this.coordinates={};
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.matchHeight=this.matchHeight || false;
		this.content=this.content || '';
		this.classes=this.classes || '';
		this.label=this.label || '';
		this.title=this.title || '';
		this.position=this.position || 'mMiddle';
		this.id=this.id || main.utilities.generateUniqueID();
		this.getParams=function(){
			return {
				panelMarginX:self.panelMarginX,
				panelMarginY:self.panelMarginY,
				minHeight:self.minHeight,
				minWidth:self.minWidth
			}
		};
		this.setPanelParams=function(){
			// self.setPanelBodyWTopMargin();
			self.setBodyHeight();
		};
		this.setBodyHeight=function(){
			if(self.topContentWrap){
				var mt=self.topContentWrap.outerHeight(true)+self.topContentWrapMargin;
				if(self.html[0].style.height!='auto' && self.html[0].style.height!=''){
					var height=self.html.height()-mt;
				}else{
					var reference=$('html');
					if(self.relativeTo){reference=self.relativeTo;}
					var height=reference.height()-mt-parseInt(self.html.css('padding-top'))-parseInt(self.html.css('padding-bottom'))-parseInt(self.html.css('border-top'))-parseInt(self.html.css('border-bottom'))-2*self.panelMarginY;
				}
				if(height>0){self.body.css('max-height',height+'px');}
			}
		};
		// this.setPanelBodyWTopMargin=function(){
			// if(self.topContentWrap){
				// var mt=self.topContentWrap.outerHeight(true)+self.topContentWrapMargin;
				// self.body.css('margin-top',mt);
			// }
		// };
		this.createHTML=function(){
			this.panelContent=$('<div>').addClass('panelContent');
			this.top=$('<div>').addClass('panelTop noSelect').html(this.createPanelTop());
			this.body=$('<div>').addClass('panelBody')
				.append($('<div>').addClass('panelLabel').text(this.label))
				.append(this.panelContent.append(this.content));
			this.panelBodyWrap=$('<div>').addClass('panelBodyWrap').append(this.body);
			if(self.scrollToTop){
				this.body.on('scroll',function(){
					self.isAtTopTest();
				});
			}
			this.html=$('<div>').addClass('panel '+this.id+' '+this.classes)
				.append(this.top);
			if(this.topContent){
				this.topContentWrap=$('<div class="topContentWrap">');
				this.panelBodyWrap.prepend(this.topContentWrap.append(this.topContent));
				this.body.addClass('panelBodyWTop');
				this.html.addClass('panelWTop');
			}
			if(this.scrollToTop){
				this.scrollToTopButton=$('<div class="scrollToTop" title="Scroll To Top">').html('&uArr;');
				this.html.append(this.scrollToTopButton);
				this.scrollToTopButton.on('click',function(){
					self.panelBodyWrap.scrollTop(0);
					self.body.scrollTop(0);
				});
			}
			this.html.append(this.panelBodyWrap);
			if(!self.withoutResize){
				this.handleW=$('<div>').addClass('resizeHandle hW').data('dir','W');
				this.handleE=$('<div>').addClass('resizeHandle hE').data('dir','E');
				this.handleN=$('<div>').addClass('resizeHandle hN').data('dir','N');
				this.handleS=$('<div>').addClass('resizeHandle hS').data('dir','S');
				this.handleNW=$('<div>').addClass('resizeHandle hNW cornerHandle').data('dir','NW');
				this.handleNE=$('<div>').addClass('resizeHandle hNE cornerHandle').data('dir','NE');
				this.handleSW=$('<div>').addClass('resizeHandle hSW cornerHandle').data('dir','SW');
				this.handleSE=$('<div>').addClass('resizeHandle hSE cornerHandle').data('dir','SE');
				this.html.append($('<div>').addClass('resizeHandles')
					.append(this.handleW).append(this.handleE).append(this.handleN).append(this.handleS)
					.append(this.handleNW).append(this.handleNE).append(this.handleSW).append(this.handleSE)
				)
			}
			main.layout.panels[this.id]=this;
		};
		this.createPanelTop=function(){
			this.maximizePanelButton=$('<div>').addClass('maximizePanel panelHiding panelButtonsEle').attr('title','Maximize')
				.append($('<div>').addClass('maximizePanelInner innerButton'))
			this.minimizePanelButton=$('<div>').addClass('minimizePanel panelHiding panelButtonsEle').attr('title','Minimize')
				.append($('<div>').addClass('minimizePanelInner innerButton'))
			this.closePanelButton=$('<div>').addClass('closePanel panelButtonsEle').attr('title','Close').text('X')
			this.panelbuttons=$('<div>').addClass('panelbuttons panelTopEle cf');
			this.paneltitle=$('<div>').addClass('paneltitle panelTopEle').text(this.title);
			this.panelTitleCondensed=$('<div>').addClass('panelTitleCondensed paneltitle panelTopEle').text(this.title.substring(0,13));
			if(!self.withOutClose){
				this.panelbuttons.append(this.closePanelButton);
			}
			if(!self.withOutMin){
				this.panelbuttons.append(this.minimizePanelButton);
				this.panelbuttons.append(this.maximizePanelButton);
			}
			if(self.withExport){
				this.exportButton=$('<div>').addClass('exportPT panelHiding panelButtonsEle').attr('title','Export');
				this.panelbuttons.append(this.exportButton);
				if(self.exportCB){
					this.exportButton.on('click',function(){
						self.exportCB();
					});
				}
			}
			return $('<div>').addClass('panelControls cf')
				.append(this.paneltitle)
				.append(this.panelTitleCondensed)
				.append(this.panelbuttons)
		};
		this.hideOnOutClickTest=function(e){
			if(!self.resize.resizing){
				var exceptions='';
				if(self.hideOnOutClickExceptions){
					for(var i=0;i<self.hideOnOutClickExceptions.length;i++){
						exceptions+=','+self.hideOnOutClickExceptions[i];
					}
				}
				if($(e.target).closest('.'+self.id+'.panel,.showButton,.hideButton'+exceptions).length==0 && !self.minimized){
					self.close();
				}
			}
		};
		this.minOnOutClickTest=function(e){
			if(!self.resize.resizing){
				var exceptions='';
				if(self.hideOnOutClickExceptions){
					for(var i=0;i<self.hideOnOutClickExceptions.length;i++){
						exceptions+=','+self.hideOnOutClickExceptions[i];
					}
				}
				if($(e.target).closest('.'+self.id+'.panel,.showButton,.hideButton'+exceptions).length==0 && !self.minimized){
					self.minimizePanel();
				}
			}
		};
		this.orientationChange=function(){
			self.positionPanel();
		};
		// this.orientationChangeParams=function(){
			// self.positionPanel();
		// };
		this.setContent=function(content){
			self.content=content;
			self.panelContent.html(content);
		};
		this.toggleMin=function(){
			if(self.opened && !self.minimized ){
				self.minimizePanel();
			}else{self.open();}
		};
		this.toggle=function(){
			if(self.opened && !self.minimized ){
				self.close();
			}else{self.open();}
		};
		this.open=function(dontSetToActive){
			self.opened=true;
			if(self.minimized){self.maximizePanel();}else{
				if(self.html.closest('html').length===0){main.layout.panelsContainer.append(self.html);}
				self.html.addClass('block').css('height','auto').css('width','auto');
				self.positionPanel();
				if(self.hideOnOutClick){$('html').on(main.controls.touchBegin,'body',self.hideOnOutClickTest);}
				if(self.minOnOutClick){$('html').on(main.controls.touchBegin,'body',self.minOnOutClickTest);}
				if(self.onShow){self.onShow();}
				if(self.scrollToTop){self.scrollToTopTest();}
				if(!dontSetToActive){self.setToActive();}
				self.setPanelParams();
				// self.html.height(self.html.height());
			}
		};
		this.close=function(omitOnClose){
			self.opened=false;
			self.maximizePanel();
			self.html.removeClass('block');
			if(self.onClose && !omitOnClose){self.onClose()}
			if(self.hideOnOutClick){$('html').off(main.controls.touchBegin,'body',self.hideOnOutClickTest);}
			if(self.minOnOutClick){$('html').off(main.controls.touchBegin,'body',self.minOnOutClickTest);}
			self.html.detach();
		};
		this.remove=function(omitOnClose){
			self.close(omitOnClose);
			self.html.remove();
		};
		this.maximizePanel=function(){
			self.panelBodyWrap.removeClass('none');
			self.minimized=false;
			self.maximizePanelButton.removeClass('block');
			self.minimizePanelButton.removeClass('none');
			self.paneltitle.removeClass('none');
			self.panelTitleCondensed.removeClass('block');
			if(self.topContentWrap){self.topContentWrap.removeClass('none');}
			self.html.removeClass('minimized');
			if(self.hideOnOutClick){$('html').on(main.controls.touchBegin,'body',self.hideOnOutClickTest);}
			if(self.minOnOutClick){$('html').on(main.controls.touchBegin,'body',self.minOnOutClickTest);}
			if(self.onShow){self.onShow();}
			if(self.scrollToTop){self.scrollToTopTest();}
			self.setToActive();
		};
		this.minimizePanel=function(){
			self.panelBodyWrap.addClass('none');
			self.minimized=true;
			self.maximizePanelButton.addClass('block');
			self.minimizePanelButton.addClass('none');
			self.paneltitle.addClass('none');
			self.panelTitleCondensed.addClass('block');
			if(self.topContentWrap){self.topContentWrap.addClass('none');}
			self.addToMinPanels();
			if(self.hideOnOutClick){$('html').off(main.controls.touchBegin,'body',self.hideOnOutClick);}
			if(self.minOnOutClick){$('html').off(main.controls.touchBegin,'body',self.minOnOutClickTest);}
			if(self.onMin){self.onMin()}
			else if(self.onClose){self.onClose()}
		};
		this.addToMinPanels=function(){
			main.layout.panelsContainer.prepend(self.html);
			self.html.addClass('minimized');
		};
		this.linkDragBlock=function(){
			return false;
		};
		this.positionPanel=function(omitXChange,pos){
			var prevLeft;
			if(omitXChange){
				prevLeft=parseInt(self.html.css('left'));
			}
			var height='auto';
			var width='auto';
			var top='';
			var left='';
			var bottom='';
			var right='';
			var edgeOffset=7;
			pos=pos || this.position;
			var reference=$('html');
			if(self.relativeTo){reference=self.relativeTo}
			self.html.css('top','0px').css('left','0px').css('height',height).css('width',width);
			if(pos=='mMiddle'){
				var top=(reference.height()-self.html.outerHeight(true))/2;
				var left=(reference.width()-self.html.outerWidth(true))/2;
			}else if(pos=='tRight'){
				var top=self.panelMarginY;
				var right=self.panelMarginX;
			}else if(pos=='mRight'){
				var top=(reference.height()-self.html.outerHeight(true))/2;
				var right=self.panelMarginX;
			}else if(pos=='bRight'){
				var bottom=self.panelMarginY;
				var right=self.panelMarginX;
			}else if(pos=='tLeft'){
				var top=self.panelMarginY;
				var left=self.panelMarginX;
			}else if(pos=='mLeft'){
				var top=(reference.height()-self.html.outerHeight(true))/2;
				var left=self.panelMarginX;
			}else if(pos=='bLeft'){
				var bottom=self.panelMarginY;
				var left=self.panelMarginX;
			}else if(pos=='tMiddle'){
				var top=self.panelMarginY;
				var left=(reference.width()-self.html.outerWidth(true))/2;
			}else if(pos=='bMiddle'){
				var bottom=self.panelMarginY;
				var left=(reference.width()-self.html.outerWidth(true))/2;
			}
			self.html.css('top','').css('left','');
			if((top || top===0) && top<=0){
				top=0+edgeOffset;
				height=reference.height()-2*edgeOffset;
				if(self.positionTop){height=height-self.positionTop;}
			}
			if(self.positionTop){top=self.positionTop;}
			if((left || left===0) && left<=0){
				left=0+edgeOffset;
				width=reference.width()-2*edgeOffset;
			}
			if(self.relativeTo){
				var offset=self.relativeTo.offset();
				if(left || left===0){left+=offset.left}
				if(top || top===0){top+=offset.top}
				if(right || right===0){offset.left+self.relativeTo.outerWidth(true)-right}
				if(bottom || bottom===0){offset.top+self.relativeTo.outerHeight(true)-bottom}
			}
			if(prevLeft){left=prevLeft;}
			self.html.css('top',top).css('bottom',bottom).css('left',left).css('right',right).css('width',width).css('height',height);
			self.setCurrentParamRations();
		};
		// this.centerPanel=function(){
			// var reference=$(document);
			// if(self.relativeTo){reference=self.relativeTo}
			// self.html.css('top','0px').css('left','0px');
		// };
		this.addContractButton=function(callback){
			if(!this.contractButton){
				this.closePanelButton.remove();
				this.contractButton=$('<div>').addClass('contract panelButtonsEle').append('<img src="'+main.mainPath+'images/contract_white.png" class="topButtonImg"/>').attr('title','Contract');
				this.panelbuttons.prepend(this.contractButton);
				this.contractButton.on('click',function(){
					callback();
				});
			}
		};
		this.isAtTop=function(){
			if(self.panelBodyWrap.scrollTop()>self.scrollTopLimit || self.body.scrollTop()>self.scrollTopLimit){return false;
			}else{return true;}
		};
		this.isScrollable=function(){
			if(self.panelContent.outerHeight(true)>self.panelBodyWrap.height() || self.panelContent.outerHeight(true)>self.body.height()){return true;
			}else{return false;}
		};
		this.isAtTopTest=function(){
			if(!self.isAtTop()){
				self.scrollToTopButton.removeClass('none');
			}else{self.scrollToTopButton.addClass('none');}
		};
		this.scrollToTopTest=function(){
			if(self.isScrollable() && !self.isAtTop()){
				self.scrollToTopButton.removeClass('none');
			}else{self.scrollToTopButton.addClass('none');}
		};
		this.setToActive=function(){
			// $('.activePanel').removeClass('activePanel');
			// self.html.addClass('activePanel');
			self.html.css('z-index',++main.misc.maxZIndex);
		};
		this.setCurrentParamRations=function(){
			var ref=$('html');
			if(self.relativeTo){var ref=self.relativeTo;}
			self.curRatioX=(parseInt(self.html.css('left'))-ref.offset().left)/ref.width();
			self.curRatioY=(parseInt(self.html.css('top'))-ref.offset().top)/ref.height();
		};
		this.htmlMouseUpDrag=function(e){
			$("html").removeClass("noSelect");
			self.dragging=false;
			$('html').off(main.controls.touchEnd,'body',self.htmlMouseUpDrag);
			$('html').off(main.controls.touchMove,'body',self.htmlMouseMoveDragging);
		};
		this.htmlMouseUpResize=function(e){
			$('body').off(main.controls.touchBegin,'a',self.linkDragBlock);
			$("html").removeClass("noSelect");
			self.resize.resizing=false;
			$('html').off(main.controls.touchEnd,'body',self.htmlMouseUpResize);
			$('html').off(main.controls.touchMove,'body',self.htmlMouseMoveResizing);
		};
		this.htmlMouseMoveDragging=function(e){
			if(e.type=='mousemove'){
				var deltaX=e.pageX-self.coordinates.mouseStartX;
				var deltaY=e.pageY-self.coordinates.mouseStartY;
			}else if(e.type=='touchmove'){
				var deltaX=e.originalEvent.touches[0].pageX-self.coordinates.mouseStartX;
				var deltaY=e.originalEvent.touches[0].pageY-self.coordinates.mouseStartY;
			}else{
				return;
			}
			var newX=self.coordinates.htmlStartX+deltaX;
			var newY=self.coordinates.htmlStartY+deltaY;
			var panelHeight=self.html.outerHeight(true);
			var topHeight=self.top.outerHeight(true);
			var panelWidth=self.html.outerWidth(true);
			var fracOfPanel=2;
			var offset=5;
			if(newX<-panelWidth/fracOfPanel)newX=-panelWidth/fracOfPanel;
			var rLimit=panelWidth/fracOfPanel+$('html').width()-panelWidth;
			if(newX>rLimit)newX=rLimit;
			// if(newY<-panelHeight/fracOfPanel)newY=-panelHeight/fracOfPanel;
			var tLimit=0+offset;
			if(newY<tLimit)newY=tLimit;
			// var bLimit=panelHeight/fracOfPanel+$(document).height()-panelHeight;
			var bLimit=$('html').height()-topHeight-offset;
			if(newY>bLimit)newY=bLimit;
			self.html.css('left',newX).css('top',newY).css('bottom','').css('right','');
			if(self.stayProportionalOnResize){self.setCurrentParamRations();}
		};
		this.htmlMouseMoveResizing=function(e){
			if(e.type=='mousemove'){
				var pageX=e.pageX;
				var pageY=e.pageY;
			}else if(e.type=='touchmove'){
				var pageX=e.originalEvent.touches[0].pageX
				var pageY=e.originalEvent.touches[0].pageY
			}else{
				return;
			}
			if(self.resize.resizingDir=='W'){
				var deltaX=pageX-self.resize.coordinates.mouseStartX;
				var newWidth=self.resize.coordinates.htmlStartWidth-deltaX;
				var newX=self.resize.coordinates.htmlStartX+deltaX;
				if(newWidth>=self.minWidth){self.html.css('left',newX).css('width',newWidth)}
			}else if(self.resize.resizingDir=='E'){
				var deltaX=pageX-self.resize.coordinates.mouseStartX;
				var newWidth=self.resize.coordinates.htmlStartWidth+deltaX;
				if(newWidth>=self.minWidth){self.html.css('width',newWidth)}
			}else if(self.resize.resizingDir=='N'){
				var deltaY=pageY-self.resize.coordinates.mouseStartY;
				var newHeight=self.resize.coordinates.htmlStartHeight-deltaY;
				var newY=self.resize.coordinates.htmlStartY+deltaY;
				if(newHeight>=self.minHeight){self.html.css('top',newY).css('height',newHeight)}
			}else if(self.resize.resizingDir=='S'){
				var deltaY=pageY-self.resize.coordinates.mouseStartY;
				var newHeight=self.resize.coordinates.htmlStartHeight+deltaY;
				if(newHeight>=self.minHeight){self.html.css('height',newHeight)}
			}else{
				var deltaY=pageY-self.resize.coordinates.mouseStartY;
				var deltaX=pageX-self.resize.coordinates.mouseStartX;
				if(self.resize.resizingDir=='NW'){
					var newHeight=self.resize.coordinates.htmlStartHeight-deltaY;
					var newWidth=self.resize.coordinates.htmlStartWidth-deltaX;
					var newY=self.resize.coordinates.htmlStartY+deltaY;
					var newX=self.resize.coordinates.htmlStartX+deltaX;
					if(newHeight>=self.minHeight){self.html.css('top',newY).css('height',newHeight)}
					if(newWidth>=self.minWidth){self.html.css('left',newX).css('width',newWidth)}
				}else if(self.resize.resizingDir=='NE'){
					var newHeight=self.resize.coordinates.htmlStartHeight-deltaY;
					var newWidth=self.resize.coordinates.htmlStartWidth+deltaX;
					var newY=self.resize.coordinates.htmlStartY+deltaY;
					if(newHeight>=self.minHeight){self.html.css('top',newY).css('height',newHeight)}
					if(newWidth>=self.minWidth){self.html.css('width',newWidth)}
				}else if(self.resize.resizingDir=='SW'){
					var newHeight=self.resize.coordinates.htmlStartHeight+deltaY;
					var newWidth=self.resize.coordinates.htmlStartWidth-deltaX;
					var newX=self.resize.coordinates.htmlStartX+deltaX;
					if(newHeight>=self.minHeight){self.html.css('height',newHeight)}
					if(newWidth>=self.minWidth){self.html.css('left',newX).css('width',newWidth)}
				}else if(self.resize.resizingDir=='SE'){
					var newHeight=self.resize.coordinates.htmlStartHeight+deltaY;
					var newWidth=self.resize.coordinates.htmlStartWidth+deltaX;
					if(newHeight>=self.minHeight){self.html.css('height',newHeight)}
					if(newWidth>=self.minWidth){self.html.css('width',newWidth)}
				}
			}
			self.setPanelParams();
			if(self.resizeCB){self.resizeCB();}
		};
		this.initPanelHeightMatch=function(){
			self.html.height('');
			var relativeTo=self.relativeTo || document;
			var wrapHeight=$(relativeTo).height();
			var selfHeight=self.html.outerHeight(true);
			if(selfHeight+2*self.panelMarginY>wrapHeight){
				self.html.height(wrapHeight-2*self.panelMarginY-parseInt(self.html.css('padding-top'))-parseInt(self.html.css('padding-bottom')));
			}
		};
		this.setPosition=function(coords){
			self.html.css('left',coords[0]);
			self.html.css('top',coords[1]);
		};
		this.setZIndex=function(zIndex){
			self.html.css('z-index',zIndex);
		};
		this.mouseDownForMove=function(e){
			// if(!main.browser.isMobile){
				if($(e.target).closest('.resizeHandle,.panelButtonsEle').length===0 && !self.dragging && !self.minimized){
					$('html').addClass("noSelect");
					self.dragging=true;
					if(e.type=='mousedown'){
						self.coordinates.mouseStartX=e.pageX;
						self.coordinates.mouseStartY=e.pageY;
					}else if(e.type=='touchstart'){
						self.coordinates.mouseStartX=e.originalEvent.touches[0].pageX;
						self.coordinates.mouseStartY=e.originalEvent.touches[0].pageY;
					}else{
						return;
					}
					self.coordinates.htmlStartX=$(this).offset().left;
					self.coordinates.htmlStartY=$(this).offset().top;
					$('html').on(main.controls.touchEnd,'body',self.htmlMouseUpDrag);
					$('html').on(main.controls.touchMove,'body',self.htmlMouseMoveDragging);
				}
			// }
		};
		this.initEvents=function(){
			if(!self.withoutResize){
				this.html.on(main.controls.touchBegin,'.resizeHandle',function(e){
					if(!main.browser.isMobile){
						e.stopPropagation();
						$('body').on(main.controls.touchBegin,'a',self.linkDragBlock);
						if(!self.dragging && !self.minimized && !self.resize.resizing){
							$('html').addClass("noSelect");
							self.resize.resizing=true;
							self.resize.resizingDir=$(this).data('dir');
							if(e.type=='mousedown'){
								self.resize.coordinates.mouseStartX=e.pageX;
								self.resize.coordinates.mouseStartY=e.pageY;
							}else if(e.type=='touchstart'){
								self.resize.coordinates.mouseStartX=e.originalEvent.touches[0].pageX;
								self.resize.coordinates.mouseStartY=e.originalEvent.touches[0].pageY;
							}else{
								return;
							}
							self.resize.coordinates.htmlStartX=self.html.offset().left;
							self.resize.coordinates.htmlStartY=self.html.offset().top;
							self.resize.coordinates.htmlStartWidth=self.html.outerWidth();
							self.resize.coordinates.htmlStartHeight=self.html.outerHeight();
							$('html').on(main.controls.touchEnd,'body',self.htmlMouseUpResize);
							$('html').on(main.controls.touchMove,'body',self.htmlMouseMoveResizing);
						}
					}
				});
			}
			this.maximizePanelButton.on('click',function(){
				self.maximizePanel();
			});
			this.minimizePanelButton.on('click',function(){
				self.minimizePanel();
			});
			this.closePanelButton.on('click',function(){
				self.close();
			});
			$(window).resize(function(){
				if(self.matchHeight){
					self.initPanelHeightMatch();
				}else{
					/* var panelHeight=self.html.outerHeight(true);
					var panelWidth=self.html.outerWidth(true);
					if(panelHeight>$(document).height()){
						var height=$(document).height()-parseInt(panel.css('margin-top'))-parseInt(panel.css('margin-bottom'))-parseInt(panel.css('padding-top'))-parseInt(panel.css('padding-bottom'));	
						self.html.height(height);
					}else{self.html.css('height','auto');}
					if(panelWidth>$(document).width()){
						var width=$(document).width()-parseInt(panel.css('margin-left'))-parseInt(panel.css('margin-right'))-parseInt(panel.css('padding-left'))-parseInt(panel.css('padding-right'));	
						self.html.width(width);
					}else{self.html.css('width','auto');} */
				}
				if(self.topContentWrap){self.setPanelParams();}
			});
			this.html.on(main.controls.touchBegin,function(e){
				if($(e.target).closest('.closePanel,.minimizePanel').length===0){
					self.setToActive();
				}
			});
			if(self.centerOnOrientationChange){window.addEventListener('orientationchange',self.orientationChange);}
			// window.addEventListener('orientationchange',self.orientationChangeParams);
			// this.html.on(main.controls.touchBegin,'body',function(e){
				// if($(e.target).closest('.closePanel,.minimizePanel').length===0){
					// self.setToActive();
				// }
			// });
			// this.html.filter('.mapTools').on(main.controls.touchBegin,self.mouseDownForMove);
			this.html.on(main.controls.touchBegin,'.panelTop',self.mouseDownForMove);
			if(self.stayProportionalOnResize){
				self.setCurrentParamRations();
				$(window).on('resize',function(){
					var newX=$(this).width()*self.curRatioX;
					var newY=$(this).height()*self.curRatioY;
					self.html.css('left',newX).css('top',newY);
				});
			}
		};
		this.createHTML();
		// if(this.topContent){self.setPanelBodyWTopMargin();}
		this.initEvents();
	};
});