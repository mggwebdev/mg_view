define(function(require){
	return function pg(default_val,id,newSigCallback){
		var self=this;
		this.signedText='Signed';
		this.notSignedText='Not Signed';
		this.id=id;
		this.newSigCallback=newSigCallback;
		this.writing=false;
		this.default_val=default_val || '';
		this.pointRadius=1;
		this.pointColor='#000000';
		this.lineWidth=this.pointRadius*2;
		this.circleLength=2*Math.PI;
		this.signed=false;
		this.changed=false;
		this.notSignedVal='';
		if(this.default_val){
			this.currentFileName=this.default_val;
			this.signed=true;
		}
		this.img=null;
		this.createSignature=function(){
			this.canvas=$('<canvas width="300" height="70" style="background:white;">').addClass('sigCanvas');
			this.input=$('<input name="'+this.id+'" type="hidden" value="'+this.default_val+'">').addClass('sigInput formInput '+this.id).data('name',this.id)
			this.ctx=this.canvas.get(0).getContext('2d');
			this.clearCanvasButton=$('<div>').addClass('clearSigCanvas sigExtra noSelect')
				.append($('<span>').text('Clear'))
			this.signedStatus=$('<div>').addClass('sigSignedStatus notSigned sigExtra noSelect')
				.append($('<span>').text('Not Signed'));
			this.container=$('<div>').addClass('signature')
				.append(this.input)
				.append($('<div>').addClass('signatureCanvasWrap')
					.append(this.canvas)
				).append($('<div>').addClass('sigExtras')
					.append(this.signedStatus)
					.append(this.clearCanvasButton)
				);
		};
		this.clearCanvas=function(){
			self.ctx.clearRect(0,0,self.canvas.width(),self.canvas.height());
			self.clearLasts();
			self.changeSignedStatus(false);
		};
		this.clearLasts=function(){
			self.lastX=null;
			self.lastY=null;
		};
		this.interpolateLine=function(x1,y1,x2,y2){
			self.ctx.moveTo(x1,y1);
			self.ctx.lineTo(x2,y2);
			self.ctx.lineWidth=this.lineWidth;
			self.ctx.stroke();
		};
		this.newPoint=function(e){
			var canvasWidth=self.canvas.width();
			var canvasHeight=self.canvas.height();
			var mouseCanvasX=e.pageX-self.canvas.offset().left;
			var mouseCanvasY=e.pageY-self.canvas.offset().top;
			self.ctx.beginPath();
			if(self.lastX){
				self.interpolateLine(self.lastX,self.lastY,mouseCanvasX,mouseCanvasY);
			}
			self.ctx.arc(mouseCanvasX,mouseCanvasY,self.pointRadius,0,self.circleLength,false);
			self.ctx.fillStyle=self.pointColor;
			self.ctx.fill();
			self.lastX=mouseCanvasX;
			self.lastY=mouseCanvasY;
		};
		this.changeSignedStatus=function(signed){
			self.signed=signed;
			self.changed=true;
			if(signed){
				self.img=self.canvas.get(0).toDataURL('image/png');
				self.input.val('true').change();
				self.signedStatus.removeClass('notSigned');
				self.signedStatus.addClass('signed');
				self.signedStatus.find('span').html(self.signedText);
			}else{
				self.img=null;
				self.input.val(self.notSignedVal).change();
				self.signedStatus.removeClass('signed');
				self.signedStatus.addClass('notSigned');
				self.signedStatus.find('span').html(self.notSignedText);
			}
		};
		this.initEvents=function(){
			this.canvas.on('mousemove',function(e){
				if(self.writing){
					self.newPoint(e);
				}
			});
			this.canvas.on('mousedown',function(e){
				if(e.which===1){
					$('html').addClass('noSelect');
					self.writing=true;
					self.newPoint(e);
				}
			});
			$('html').on('mouseup',function(e){
				if(self.writing){
					self.changeSignedStatus(true);
					self.writing=false;
					self.clearLasts();
					$('html').removeClass('noSelect');
					if(self.newSigCallback){self.newSigCallback(self);}
				}
			});
			this.clearCanvasButton.on('click',function(e){
				self.clearCanvas();
			});
		};
		this.createSignature();
		this.initEvents();
	};
});