define(function(require){
	return function pg(map){
		var self=this;
		var Button=require('./Button');
		var Input=require('./Input');
		var ViewMore=require('./ViewMore');
		this.map=map;
		this.keys=main.utilities.cloneObj(main.data.std_settings.draw.sortedKeys);
		this.defaultType='Point';
		this.currentType=null;
		this.alias='Draw';
		this.withItem='with_draw';
		this.drawing=false,
		this.stopDrawingAlias='Stop Drawing';
		this.removeFeaturesHTML='Remove';
		this.formID='drawForm';
		this.setParams=function(){
			this.params=main.utilities.cloneObj(main.data.std_settings.draw);
		};
		this.getStyle=function(){
			return new ol.style.Style({
				fill:new ol.style.Fill({
					color:(self.params.fill_color.current_val)?self.params.fill_color.current_val:self.params.fill_color.default_val
				}),
				stroke:new ol.style.Stroke({
					color:(self.params.stroke_color.current_val)?self.params.stroke_color.current_val:self.params.stroke_color.default_val,
					width:(self.params.stroke_width.current_val)?self.params.stroke_width.current_val:self.params.stroke_width.default_val
				}),
				image:new ol.style.Circle({
					radius:(self.params.point_radius.current_val)?self.params.point_radius.current_val:self.params.point_radius.default_val,
					fill:new ol.style.Fill({
						color:(self.params.point_color.current_val)?self.params.point_color.current_val:self.params.point_color.default_val
					})
				})
			});
		};
		this.startDrawing=function(type){
			main.measure.stopMeasuringFunctions();
			if(this.deleting) this.doneDeletingFeatures();
			if(this.modifying) this.stopModifying();
			this.drawing=true;
			main.flags.drawing=true;
			this.addDraw(type);
			this.startDrawingButton.addClass('none');
			this.stopDrawingButton.removeClass('none');
		};
		this.stopDrawing=function(){
			this.drawing=false;
			main.flags.drawing=false;
			this.map.map.removeInteraction(this.draw);
			this.startDrawingButton.removeClass('none');
			this.stopDrawingButton.addClass('none');
		};
		this.startModifying=function(){
			main.measure.stopMeasuringFunctions();
			if(this.drawing) this.stopDrawing();
			if(this.deleting) this.doneDeletingFeatures();
			this.modifying=true;
			this.map.map.addInteraction(this.select);
			this.map.map.addInteraction(this.modify);
			this.startModifyingButton.addClass('none');
			this.stopModifyingButton.removeClass('none');
		};
		this.stopModifying=function(){
			this.modifying=false;
			this.select.getFeatures().clear();
			this.map.map.removeInteraction(this.select);
			this.map.map.removeInteraction(this.modify);
			this.startModifyingButton.removeClass('none');
			this.stopModifyingButton.addClass('none');
		};
		this.beginDeletingFeatures=function(){
			main.measure.stopMeasuringFunctions();
			if(this.drawing) this.stopDrawing();
			if(this.modifying) this.stopModifying();
			this.deleting=true;
			this.map.map.addInteraction(this.dragBox);
			this.deleteFeaturesButton.addClass('none');
			this.doneDeletingFeaturesButton.removeClass('none');
		};
		this.doneDeletingFeatures=function(){
			this.deleting=false;
			this.map.map.removeInteraction(this.dragBox);
			this.doneDeletingFeaturesButton.addClass('none');
			if(this.drawSource.getFeatures().length===0){
				this.deleteFeaturesButton.addClass('none');
				this.deleteAllButton.addClass('none');
			}else{this.deleteFeaturesButton.removeClass('none');}
		};
		this.stopDrawingFunctions=function(){
			if(this.drawing) this.stopDrawing();
			if(this.modifying) this.stopModifying();
			if(this.deleting) this.doneDeletingFeatures();
		};
		this.addDraw=function(type){
			this.draw=new ol.interaction.Draw({
				source:this.drawSource,
				type:type
			});
			this.draw.on('drawend',function(e){
				e.feature.set(main.constants.featureTypeCode,'draw');
				self.deleteFeaturesButton.removeClass('none');
				self.deleteAllButton.removeClass('none');
				self.startModifyingButton.removeClass('none');
			});
			this.map.map.addInteraction(this.draw);
		};
		this.createHTML=function(){
			this.addShelf();
			main.utilities.initSpectrumPickers(this.params,this.drawingInputChange,true,true);
		};
		this.addShelf=function(){
			this.launchButton=new Button('toolBox','',this.alias);
			this.form=this.createDrawForm();
			this.shelf=main.layout.toolBox.createShelf({
				name:'draw',
				button:this.launchButton.html,
				content:this.form,
				alias:this.alias,
				groups:main.withs[self.withItem].toolbox_groups
			});
		};
		this.createDrawForm=function(){
			var form=$('<form>').attr('id',this.formID);
			// this.drawings=new EditableList();
			this.startDrawingButton=$('<div>').addClass('button startDrawing').text('Start Drawing');
			this.stopDrawingButton=$('<div>').addClass('button stopDrawing none').text('Stop Drawing');
			this.startModifyingButton=$('<div>').addClass('button startModifying none').text('Modify Drawings');
			this.stopModifyingButton=$('<div>').addClass('button stopModifying none').text('Stop Modifying');
			this.deleteFeaturesButton=$('<div>').addClass('button deleteDrawFeatures none').text('Delete Drawings');
			this.doneDeletingFeaturesButton=$('<div>').addClass('button doneDeletingFeatures none').text('Done Deleting');
			this.deleteAllButton=$('<div>').addClass('button deleteAllButton none').text('Delete All Drawings');
			self.viewMore=new ViewMore({
				prev:'Use this tool to add graphics and text to the map area',
				content:'<ul><li>Step 1: Select a draw type.</li><li>Step 2: Change the parameters.</li><li>Step 3: Click "Start Drawing".</li><li>To delete individual drawings, click "Delete Drawings" and click the graphics you want to delete.</li><li>To modify individual drawings, click "Modify Drawings", then click a graphic. Click and drag the point or the outline of a line or polygon.</li></ul>'
			});
			var table=$('<table>').addClass('shelfForm')
				.append($('<tr>')
					.append($('<td colspan="2">').append($('<div>').addClass('tdLiner toolBoxDesc').html(self.viewMore.html)))
				).append($('<tr>')
					.append($('<th colspan="2">')
						.append($('<div>').addClass('tdLiner center').text('Draw Properties'))
					)
				);
			var toolFormMakeItems=main.utilities.toolFormMake(this.keys,this.params,this.formID,table);
			this.advancedViewMore=toolFormMakeItems.advancedViewMore;
			this.advancedOptions=toolFormMakeItems.advancedOptions;
			this.typeSelect=toolFormMakeItems.typeSelect;
			var key;
			table/* .append($('<tr>')
					.append($('<td>').append($('<div>').addClass('tdLiner').text('Drawings')))
					.append($('<td>').append($('<div>').addClass('tdLiner').append(this.drawings)))
				) */.append($('<tr>')
					.append($('<td>').append($('<div>').addClass('tdLiner').append(this.startDrawingButton)))
					.append($('<td>').append($('<div>').addClass('tdLiner').append(this.stopDrawingButton)))
				).append($('<tr>')
					.append($('<td>').append($('<div>').addClass('tdLiner').append(this.startModifyingButton)))
					.append($('<td>').append($('<div>').addClass('tdLiner').append(this.stopModifyingButton)))
				).append($('<tr>')
					.append($('<td>').append($('<div>').addClass('tdLiner').append(this.deleteFeaturesButton)))
					.append($('<td>').append($('<div>').addClass('tdLiner').append(this.doneDeletingFeaturesButton)))
				).append($('<tr>')
					.append($('<td colspan="2">').append($('<div>').addClass('tdLiner').append(this.deleteAllButton)))
				);
			return form.append(table);
		};
		this.drawingInputChange=function(){
			self.updateDrawingParams();
		};
		this.updateDrawingParams=function(){
			main.utilities.setCurrentVals(self.form,self.params);
			self.drawVector.setStyle(self.getStyle(self));
		};
		this.initEvents=function(){
			$(main.map.map.getViewport()).on('click',function(e){
				var pixel=main.utilities.getPixel(e);
				if(!pixel){return;}
				var feature=main.map.map.forEachFeatureAtPixel(pixel,function(feature,layer){
					return feature;
				});
				if(feature){
					if(self.deleting && feature.get(main.constants.featureTypeCode)=='draw'){
						self.drawSource.removeFeature(feature);
						if(self.drawSource.getFeatures().length===0){
							self.doneDeletingFeatures();
							self.startModifyingButton.addClass('none');
						}
					}
				}
			});
			this.dragBox.on('boxend',function(e){
				var extent=self.dragBox.getGeometry().getExtent();
				self.drawSource.forEachFeatureIntersectingExtent(extent,function(feature){
					self.drawSource.removeFeature(feature);
					if(self.drawSource.getFeatures().length===0){
						self.doneDeletingFeatures();
						self.startModifyingButton.addClass('none');
					}
				});
			});
			this.typeSelect.on('change',function(){
				self.currentType=$(this).val();
				self.stopDrawing();
				self.startDrawing(self.params.type.options[self.currentType].geomType);
			});
			this.startDrawingButton.on('click',function(){
				self.startDrawing((!self.currentType)?self.params.type.options[self.defaultType].geomType:self.params.type.options[self.currentType].geomType);
			});
			this.stopDrawingButton.on('click',function(){
				self.stopDrawing();
			});
			this.startModifyingButton.on('click',function(){
				self.startModifying();
			});
			this.stopModifyingButton.on('click',function(){
				self.stopModifying();
			});
			this.deleteFeaturesButton.on('click',function(){
				self.beginDeletingFeatures();
			});
			this.doneDeletingFeaturesButton.on('click',function(){
				self.doneDeletingFeatures();
			});
;			this.deleteAllButton.on('click',function(){
				self.drawSource.clear();
				if(self.deleting){self.doneDeletingFeatures();
				}else{
					self.deleteFeaturesButton.addClass('none');
					self.doneDeletingFeaturesButton.addClass('none');
					self.deleteAllButton.addClass('none');
				}
				if(self.modifying){ self.stopModifying();}
				self.startModifyingButton.addClass('none');
			});
			this.form.on('change','.formInput',function(e){
				e.preventDefault();
				self.drawingInputChange();
			});
		};
		this.setParams();
		this.drawSource=new ol.source.Vector();
		this.drawVector=new ol.layer.Vector({
			source:this.drawSource,
			style:this.getStyle(this)
		});
		this.map.map.addLayer(this.drawVector);
		this.select=new ol.interaction.Select({});
		this.modify=new ol.interaction.Modify({
			features:this.select.getFeatures(),
			deleteCondition:function(event){
				return ol.events.condition.shiftKeyOnly(event) && ol.events.condition.singleClick(event);
			}
		});
		this.dragBox=new ol.interaction.DragBox({
			condition:ol.events.condition.shiftKeyOnly,
			style:this.getStyle(this)
		});
		this.createHTML();
		main.utilities.setCurrentVals(this.form,this.params);
		this.initEvents();
	};
});