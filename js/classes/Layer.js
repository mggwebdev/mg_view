define(function(require){
	return function pg(params){
		var self=this;
		var Style=require('./Style');
		this.url=params.url;
		if(params.map){this.map=params.map}
		else{this.map=main.map}
		if(params.alias){this.alias=params.alias}
		else{
			var urlSplit=this.url.split('/');
			this.alias=urlSplit[urlSplit.length-1];
		}
		this.keys=main.utilities.cloneObj(main.data.std_settings.layer.sortedKeys);
		this.setParams=function(){
			this.params=main.utilities.cloneObj(main.data.std_settings.layer);
		};
		this.initEvents=function(){}
		this.setParams();
		this.id=main.utilities.generateUniqueID();
		this.source=new ol.source.GeoJSON({
			extractStyles: false,
			projection: 'EPSG:3857',
			url: this.url
		});
		this.layer=new ol.layer.Vector({
			source:this.source,
			style:new ol.style.Style({
				fill: new ol.style.Fill({
					color: this.params.fill_color.default_val
				}),
				stroke: new ol.style.Stroke({
					color: this.params.stroke_color.default_val,
					width: this.params.stroke_width.default_val
				}),
				zIndex:main.layers.maxZindex++
			})
		});
		this.style=new Style(this);
		this.labels=new main.classes.LayerLabels(this);
		this.initEvents();
	};
});