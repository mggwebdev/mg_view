define(function(require){
	return function pg(args){
		var self=this;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.tabs={};
		this.data=this.data || [];
		this.createHTML=function(){
			this.tabsWrap=$('<div>').addClass('tabsWrap');
			this.content=$('<div>').addClass('tabsContentWrap');
			this.html=$('<div>').addClass('tabsContsWrap')
				.append(this.tabsWrap)
				.append(this.content)
			if(self.classes){self.html.addClass(self.classes)}
			if(self.data){
				for(var i=0;i<self.data.length;i++){
					self.html.data(self.data[i][0],self.data[i][1])
				}
			}
		};
		this.setFullTabsWidth=function(){
			var tabs=self.tabsWrap.find('.tab');
			var length=tabs.length;
			var eachWidth=1*100/length;
			tabs.css('width',eachWidth+'%');
		};
		this.setTabContent=function(content,id){
			if(this.tabs[id]){
				this.tabs[id].rawContent=content;
				this.tabs[id].content.html(content);
			}
		};
		this.addTab=function(label,rawContent,name,clickCallBack,params,userID,classes,tabclasses){
			var id=main.utilities.generateUniqueID();
			var tab=self.createTab(label,id,name,tabclasses);
			var content=self.createContent(rawContent,id,name,userID,classes);
			this.tabsWrap.append(tab);
			this.content.append(content);
			this.tabs[id]={
				tab:tab,
				content:content,
				rawContent:rawContent,
				tabid:id,
				userID:userID,
				context:self,
				name:name,
				params:params,
				classes:classes,
				clickCallBack:clickCallBack
			}
			this.tabs[id].tab.on('click',function(){
				if(self.activeTab.id!=$(this).data('tabid')){
					var prevTab=self.activeTab;
					var id=$(this).data('tabid');
					self.setActiveTab(id);
					if(self.tabs[id].clickCallBack){self.tabs[id].clickCallBack(self.activeTab,prevTab);}
				}
			});
			this.setActiveTab(id);
			if(self.tabsMatchWidth){
				self.setFullTabsWidth();
			}
			return this.tabs[id];
		};
		this.createTab=function(label,id,name,tabclasses){
			if(!tabclasses){tabclasses='';}
			if(!label){label='';}
			return $('<div>').addClass('tab noSelect '+id+' '+tabclasses).html(label).data('tabid',id).data('name',name);
		};
		this.createContent=function(content,id,name,userID,classes){
			if(!content){content='';}
			if(!classes){classes='';}
			return $('<div>').addClass('tabContent '+id+' '+classes).attr('id',userID).html(content).data('tabid',id).data('name',name);
		};
		this.getActiveTab=function(){
			return self.activeTab;
		};
		this.setActiveTabByName=function(name){
			for(var key in self.tabs){
				if(self.tabs[key].name==name){
					self.setActiveTab(self.tabs[key].tabid);
				}
			}
		};
		this.setActiveTab=function(tabid){
			for(var key in self.tabs){
				self.tabs[key].tab.removeClass('activeTab');
				self.tabs[key].content.removeClass('block');
			}
			self.tabs[tabid].tab.addClass('activeTab');
			self.tabs[tabid].content.addClass('block');
			self.activeTab=self.tabs[tabid];
		};
		this.createHTML();
	};
});