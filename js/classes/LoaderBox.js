define(function(require){
	return function pg(args){
		var self=this;
		this.message='Loading';
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		(this.createHTML=function(context,exceptions){
			// self.loaderImg=$('<img src="'+main.mainPath+'images/ajax-loader.gif">').addClass('loaderImg');
			self.loaderMsg=$('<div>').addClass('loaderMsg').text(self.message);
			self.loaderAnim=$('<div>').addClass('loaderAnim')
				.append('<span>.</span><span>.</span><span>.</span><span>.</span><span>.</span><span>.</span><span>.</span><span>.</span><span>.</span><span>.</span><span>.</span>');
			self.loaderWrap=$('<div>').addClass('loaderWrap')
				// .append(self.loaderImg)
				.append(self.loaderMsg)
				.append(self.loaderAnim);
			self.html=$('<div>').addClass('loader')
				.append(self.loaderWrap);
		})();
		this.loaderOn=function(){
			// self.loaderWrap.css('left','').css('top','');
			// if(pos=='bl'){self.loaderWrap.css('left',self.stdOffset).css('bottom',self.stdOffset);}
			self.loaderIsOn=true;
			self.html.addClass('block');
			self.startAnim();
		};
		this.loaderOff=function(){
			self.html.removeClass('block');
			self.stopAnim();
			self.loaderIsOn=false;
		};
		this.nextAnimIt=function(){
			var active=self.loaderAnim.find('.loaderActive').removeClass('loaderActive');
			if(active.length===0){active=self.loaderAnim.find('*:first-child');}
			var next=active.next();
			if(next.length===0){next=self.loaderAnim.find('*:first-child');}
			next.addClass('loaderActive');
		};
		this.startAnim=function(){
			if(self.loaderAnimInterval){clearInterval(self.loaderAnimInterval);}
			self.loaderAnimInterval=setInterval(self.nextAnimIt,100);
		};
		this.stopAnim=function(){
			clearInterval(self.loaderAnimInterval);
		};
	};
});