define(function(require){
	return function pg(args){
		var self=this;
		this.active=false;
		this.offsetX=18;
		this.offsetY=18;
		this.frozen=false;
		this.visible=false;
		this.lastMoveEvent=null;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.content=this.content || '';
		this.map=this.map || main.map;
		this.label=this.label || '';
		this.setContent=function(content){
			self.content=content;
			self.infoTipContent.html(self.content);
		};
		this.activate=function(){
			self.active=true;
			self.onEvents();
		};
		this.deActivate=function(){
			self.active=false;
			self.visible=false;
			self.container.removeClass('block');
			self.offEvents();
		};
		this.updateCurrentLayer=function(layer){
			self.currentLayer=layer;
		};
		this.triggerPointerMove=function(){
			if(self.lastMoveEvent){
				self.mouseMoveFunction(self.lastMoveEvent,true);
			}
		};
		this.onEvents=function(){
			self.pointerMoveEvent=self.map.map.on('pointermove',self.mouseMoveFunction);
			self.mapClickedEvent=self.map.map.on('click',self.mapClicked);
			self.htmlPointerMove=$('html').on(main.controls.touchMove,self.isInMap);
		};
		this.offEvents=function(){
			//else use .un()
			self.map.map.unByKey(self.pointerMoveEvent);
			self.map.map.unByKey(self.mapClickedEvent);
		};
		this.update=function(originalEvent,feature){
			var x=originalEvent.pageX+self.offsetX;
			var y=originalEvent.pageY+self.offsetY;
			self.visible=true;
			self.container.addClass('block').css('left',x).css('top',y);
			if(!feature){
				var features=self.getFeatures(originalEvent);
				if(features && features.length>0){
					feature=features[0][0];
				}
			}
			if(self.onMouseOverFeature && feature){self.onMouseOverFeature(feature.getProperties())}
		};
		this.getFeatures=function(e){
			var pixel=main.utilities.getPixel(e);
			if(!pixel){return;}
			var features=[];
			self.map.map.forEachFeatureAtPixel(pixel,function(feature,layer){
				if(layer && layer.get('id')==self.currentLayer.properties.name){
					if(main.canopy_layers && main.canopy_layers[self.currentLayer.properties.name] && main.canopy.filterStyleFunction(feature)[0].getFill().getColor()!='rgba(0,0,0,0)'){
						features.push([feature,layer]);
					}
				}
			});
			return features;
		};
		this.mapClicked=function(e){
			var features=self.getFeatures(e);
			if(features && features.length>0){
				self.freeze(e.originalEvent,features[0][0]);
			}else{
				self.frozen=false;
				self.makeUnTouchable();
			}
		};
		this.freeze=function(originalEvent,feature){
			self.update(originalEvent,feature);
			self.frozen=true;
			self.makeTouchable();
		};
		this.unFreeze=function(){
			self.visible=false;
			self.container.removeClass('block');
			self.frozen=false;
			self.makeUnTouchable();
		};
		this.makeTouchable=function(){
			self.container.removeClass('noPointerEvents');
		};
		this.makeUnTouchable=function(){
			self.container.addClass('noPointerEvents');
		};
		this.isInMap=function(e,forceUpdate){
			if((!$(e.target).closest('.mapWrap').length && !$(e.target).closest('.infoTip').length) || $(e.target).closest('.panel,.accountBox').length){
				self.unFreeze();
			}
		};
		this.mouseMoveFunction=function(e,forceUpdate){
			self.lastMoveEvent=e;
			if(self.active && (!self.frozen || forceUpdate)){
				var features=self.getFeatures(e);
				if(features && features.length>0){
					self.update(e.originalEvent,features[0][0]);
				}else{
					self.unFreeze();
				}
			}
		};
		(this.create=function(){
			self.infoTipLabel=$('<div>').addClass('infoTipLabel').html(self.label);
			self.infoTipContent=$('<div>').addClass('infoTipContent').html(self.content);
			self.container=$('<div>').addClass('infoTip noPointerEvents')
				.append(self.infoTipLabel)
				.append(self.infoTipContent);
			main.layout.infoTipsContainer.append(self.container);
			self.container.on('click',self.unFreeze);
		})();
		// (this.initEvents=function(){
		// })();
	};
});