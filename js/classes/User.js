define(function() {

	var _userName = 'Guest';
	var _userRole = 'Guest Role';
	var _userLoggedIn = false;
	var _userOrg = 'Guest Org';
	
	function _makeLoginWindow(){
		var form = $('<form>')
			.attr('id','loginForm')
			.addClass('loginForm');
		var inputName = $('<input>')
			.attr('name','name')
			.attr('id','loginForm')
			.attr('placeholder','name');
		var inputPass = $('<input>')			
			.attr('type','password')
			.attr('name','password')
			.attr('placeholder','password');
		var btnSubmit = $('<button>')			
			.attr('id','loginSubmit')
			.html('Login');

		form.append(inputName).append(inputPass).append(btnSubmit);

		var loginModal = $('<div>').addClass('loginModal');
		var loginWin = $('<div>').addClass('loginWindow');
		var closeWinBtn = $('<div>').addClass('closeButton').text('(close)');
		$('body').append(loginModal);
		$('.loginModal').html(loginWin);
		$('.loginWindow').html(form);
		$('.loginWindow').append(closeWinBtn);
		$('.loginWindow').show();
		closeWinBtn.on('click', function(e) {
			$(this).parent().parent().remove();
		});
		btnSubmit.on('click', function(e) {
			e.preventDefault();

                var rptContents = 'no data';
                
                var postData = {};
                postData.params = {};
                postData.params.name = $('#loginForm input[name=name]').val();
                postData.params.password = $('#loginForm input[name=password]').val();
                postData.action = 'login';

                //receives 'success' indicator, and user vars
                $.post( "server/login.php", postData, function(data) {
                        var jData = JSON.parse(data);
                        if( jData.success ) {
							main.user.name = jData.username;
							main.user.role = jData.rolename;
							main.user.org = jData.orgname;
							main.user.isLoggedIn = true;
							_updateUserInfoWindow(
								main.user.name,
								main.user.role,
								main.user.org,
								main.user.isLoggedIn);
                        } else {
                        	alert('Incorrect username or password');
                        }
                   }).fail(function(){alert('error logging in');console.log(response);});

		});
	}

	function _makeUserIcon(){
		//shortcut in header
		// this does not parse unless main.layout... defined - which it is not until modules
		//  are loaded.  It appears.
		if(main.layout && main.layout.shortcuts) {
			main.layout.shortcuts['userShortcut']=main.layout.header.createHeaderButton('userIcon','',[['button','programs']]);
			main.layout.header['headerRight']
				.append(main.layout.shortcuts['userShortcut'].html)
				.append('<div id="userInfoWindow">&nbsp;</div>');
			main.layout.shortcuts['userShortcut'].html.click(function(){
			});
			$('#userInfoWindow').html('<a id="userLogin">Sign In</a>');			
			$('#userLogin').click(function(){_makeLoginWindow()});	
			/*$('.userIcon').hover(function(e){
					$('#userInfoWindow').show();
					$('#userInfoWindow').css('display','block');
					$('#userInfoWindow').css('top','100px');
					$('#userInfoWindow').css('left','50px');				
				}, function() {
					$('#userInfoWindow').hide();
			});*/

/*			main.layout.shortcuts['userShortcut'].html.mouseover(function(){
				$('#userInfoWindow').offset( { 
									top:$(this).offset().top+40, left:$(this).offset().left-200 
								} );
				$('#userInfoWindow').show();
			});*/
		}

	}

	function _updateUserInfoWindow(name, role, org, isLoggedIn) {
		if( isLoggedIn ) {
			$('#userInfoWindow').html(name+'<br>'+role+'<br>'+org);
			$('#userInfoWindow').append('<br><br><a id="userLogOut">Sign Out</a>');
			$('#userLogOut').click(function(){alert('logout');});
		} else {
			$('#userInfoWindow').html('<a id="userLogin">Sign In</a>');
			$('#userLogin').click(function(){_makeLoginWindow()});	
		}
	}

	var user = {
		name:	_userName,
Add a comment to this line
		role:	_userRole,
		org:	_userOrg,
		isLoggedIn:	_userLoggedIn,
		permissions: {
			canEdit:	true,
			canExport:	false
		},
		setUserName:	function(n){ _userName = n },
		getUserName:	function(){ return _userName },

		showLogin:		_makeLoginWindow,
		makeUserIcon:   _makeUserIcon,
		//creates floating div for sign-in

	}
	return user; 
});