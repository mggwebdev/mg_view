define(function(require){
	return function pg(account){
		var self=this;
		var Accordian=require('./Accordian');
		var Button=require('./Button');
		this.account=account;
		this.users_settings=main.personal_settings.account;
		this.alias='Log In/Out';
		this.devLoginB=false;
		this.devLoginEmail='aaa';
		this.devLoginPwd='bbb';
		this.with_SC=true;
		this.recentLogOut=false;
		this.createHTML=function(){
			this.addShelf();
			if(this.users_settings.with_login_sc){
				self.loginSC=new Button('roundedSquare','iconButton headerButton loginSC','Log In',null,null,main.mainPath+'images/gear.png','Log In');
				main.layout.header.headerRight.append(self.loginSC.html);
				self.loginSCMobile=main.layout.header.createMobileMenuItem('loginSC','Log In',[['button','loginSC']],main.mainPath+'images/gear.png','Log In');
				main.layout.header.mobileMenu.append(self.loginSCMobile.html);
				self.loginSC.html.add(self.loginSCMobile.html).on('click',function(){
					if(main.layout.header.mobileMenu.is(':visible')){main.layout.header.mobileMenu.slideUp(200);}
					main.utilities.openForm('logIn','account',true);
				});
				self.logoutSC=new Button('roundedSquare','iconButton headerButton logoutSC none','Log Out',null,null,main.mainPath+'images/gear.png','Log Out');
				main.layout.header.headerRight.append(self.logoutSC.html);
				self.logoutSCMobile=main.layout.header.createMobileMenuItem('logoutSC','Log Out',[['button','logoutSC']],main.mainPath+'images/gear.png','Log Out');
				main.layout.header.mobileMenu.append(self.logoutSCMobile.html);
				self.logoutSC.html.add(self.logoutSCMobile.html).on('click',function(){
					if(main.layout.header.mobileMenu.is(':visible')){main.layout.header.mobileMenu.slideUp(200);}
					self.logOut();
				});
			}
		};
		this.createForm=function(){
			this.username=$('<input type="text" name="username" class="loginInput formInput username">');
			this.pword=$('<input type="password" name="pword" class="loginInput formInput pword">');
			this.username.add(this.pword).on('keyup',function(e){
				if(e.which==13){self.logIn();}
			});
			this.alreadySignedIn=$('<div>').addClass('alreadySignedIn').html('You are already signed In.');
			this.logoutButton=new Button('std','logoutButton formConcButton','Log Out');
			this.loginButton=new Button('std','loginButton formConcButton','Log In');
			// this.loginButton=$('<input type="submit" value="Log In" class="formConcButton button stdButton">');
			this.logOutWrap=$('<tr class="logOutWrap">').append($('<td colspan="2">')
				.append($('<div>').addClass('tdLiner center formConcButtons')
					.append(this.logoutButton.html)
			));
			if(self.account.loggedInUserType==self.account.accountSettings.default_base_user){this.logOutWrap.addClass('none');}
			this.table=$('<table>').addClass('loginTable')
				// .append($('<thead>')
					// .append($('<tr>')
						// .append($('<th colspan="2">').append($('<div>').addClass('tdLiner').text('Log In')))
					// )
				// )
				.append($('<tbody>')
					.append($('<tr>')
						.append($('<td>').append($('<div>').addClass('tdLiner').text('Username')))
						.append($('<td>')
							.append($('<div>').addClass('tdLiner')
								.append(this.username)
							)
						)
					)
					.append($('<tr>')
						.append($('<td>').append($('<div>').addClass('tdLiner').text('Password')))
						.append($('<td>')
							.append($('<div>').addClass('tdLiner')
								.append(this.pword)
							)
						)
					)
					.append($('<tr>')
						.append($('<td colspan="2">')
							.append($('<div>').addClass('tdLiner center formConcButtons')
								.append(this.loginButton.html)
						))
					)
					.append(this.logOutWrap)
				)
			this.devLogin='';
			this.devLogout='';
			if(this.devLoginB){
				// this.devLogin=$('<div style="cursor:pointer;border:1px solid black;display:inline-block">').addClass('devLogin').text('Dev Login');
				// this.devLogout=$('<div style="cursor:pointer;border:1px solid black;display:inline-block">').addClass('devLogout').text('Dev Logout');
			}
			var subDetails=this.users_settings.login_notes || '';
			this.html=$('<form>').addClass('loginWrap')
				.append($('<div>').addClass('subDetails').html(subDetails))
				.append(this.alreadySignedIn)
				.append(this.table)
				.append(this.devLogin)
				.append(this.devLogout);
			return this.html;
		};
		this.addShelf=function(){
			this.launchButton=new Button('toolBox','',this.alias);
			this.form=this.createForm();
			this.shelf=self.account.toolBoxAccordian.createShelf({
				name:'logIn',
				button:this.launchButton.html,
				content:this.form,
				alias:this.alias
			});
		};
		this.updateLoggedInUser=function(params){
			self.account.loggedInUser={};
			self.account.loggedInUserType=params.user_type;
			for(var key in params){
				self.account.loggedInUser[key]=params[key];
			}
			self.account.loggedInUser.user_type_data=params.user_type_data;
			self.account.loggedInUser.org_data=params.org_data;
		};
		this.justLoggedOut=function(params){
			main.layout.closeAllEditForms();
			self.updateLoggedInUser(params.user_data);
			self.account.hideAccountBox();
			if(main.withs.with_forms.value){self.updateForms(params);}
			if(main.account){
				main.account.removeShelves();
				main.account.addShelves();
			}
			//check for tools
			for(var i=0;i<main.tools.length;i++){
				if(main[main.tools[i][1]] && (!main.withs[main.tools[i][0]] || !main.withs[main.tools[i][0]].value || !main.withs[main.tools[i][0]][main.account.loggedInUser.user_type+'_with'])){
					if(main.toolBox && main.toolBox.accordian.shelves[main[main.tools[i][1]].name]){main.toolBox.accordian.removeShelf(main.toolBox.accordian.shelves[main[main.tools[i][1]].name]);}
					delete main[main.tools[i][1]];
					if(main.dataTables){
						var dts=main.dataTables;
						for(var key in dts){
							if(dts[key]['remove_'+main.tools[i][1]]){dts[key]['remove_'+main.tools[i][1]]();}
						}
					}
				}
			}
			if(main.toolBox){main.toolBox.recreate();}
			if(main.invents){self.updateInventories();}
			
			if(main.layout.accordians){
				main.layout.checkAccordianWidths();
			}
			self.updateLogInVisibility();
			self.logOutWrap.addClass('none');
			if(self.logoutSC){self.logoutSC.html.add(self.logoutSCMobile.html).addClass('none');}
			if(self.loginSC){self.loginSC.html.add(self.loginSCMobile.html).removeClass('none');}
			if(main.account.loggedInUser.dont_filter){
				if(main.filter_legend && main.filter_legend.active){main.filter_legend.refresh();}
				if(main.dashboard && main.dashboard.active){main.dashboard.refresh();}
			}else{
				if(main.filter_legend){main.filter_legend.changeLayer(main.filter_legend.primaryLayer.layer_name,true);}
			}
			if(main.personal_settings.map.center){main.map.panTo(ol.proj.transform(main.personal_settings.map.center, 'EPSG:4326', 'EPSG:3857'));}
			if(main.personal_settings.map.zoom){main.map.animateZoom(main.personal_settings.map.zoom);}
			if(main.filter_legend && main.filter_legend.active){main.filter_legend.checkLoginChange();}
			if(main.canopy){main.canopy.updateForLogIn();}
            if(main.dashboard){main.dashboard.refresh();}
		};
		this.justLoggedIn=function(params){
			self.updateLoggedInUser(params);
			self.account.initAccountBox();
			self.recentLogOut=false;
			self.account.initSessionCheck();
			main.layout.sidePanels[self.html.closest('.sidePanel').data('button')].close();
			main.map.adjustMapSize();
			if(main.withs.with_forms.value){self.updateForms(params);}
			if(main.account){
				main.account.removeShelves();
				main.account.addShelves();
			}
			//check for tools
			for(var i=0;i<main.tools.length;i++){
				if(!main[main.tools[i][1]] && main.withs[main.tools[i][0]] && main.withs[main.tools[i][0]].value && main.withs[main.tools[i][0]][main.account.loggedInUser.user_type+'_with']){
					main[main.tools[i][1]]= new main.tools[i][2]();
				}
				if(main.dataTables){
					var dts=main.dataTables;
					for(var key in dts){
						if(dts[key]['remove_'+main.tools[i][1]]){dts[key]['remove_'+main.tools[i][1]]();}
					}
				}
			}
			if(main.toolBox){main.toolBox.recreate();}
			if(main.invents){self.updateInventories();}
			self.updateLogInVisibility();
			self.logOutWrap.removeClass('none');
			if(self.logoutSC){self.logoutSC.html.add(self.logoutSCMobile.html).removeClass('none');}
			if(self.loginSC){self.loginSC.html.add(self.loginSCMobile.html).addClass('none');}
			
			if(main.invents && main.invents.depened_upons.length && self.account.accountSettings[main.account.loggedInUser.user_type+'_zoom_to_data_on_login'] && self.account.loggedInUser.org_data && self.account.loggedInUser.org_data.orgId){
				main.invents.inventories[main.invents.depened_upons[0]].loadFeatures({pid:Number(self.account.loggedInUser.org_data.orgId),zoomTo:true,refresh:true});
			}else{
				if(main.filter_legend && main.filter_legend.active){main.filter_legend.refresh();}
				if(main.dashboard && main.dashboard.active){main.dashboard.refresh();}
				main.layout.loader.loaderOff();
			}
			if(main.filter_legend && main.filter_legend.active){main.filter_legend.checkLoginChange();}
			if(main.canopy){main.canopy.updateForLogIn();}
		};
		this.updateInventories=function(){
			for(var key in main.invents.inventories){
				if(main.invents.inventories[key] && main.invents.inventories[key].shelf){
					if(!main.invents.inventories[key].properties.is_wom){
						var accordian=main.invents.inventories[key].inventories.accordian;
					}else{
						var accordian=main.invents.inventories[key].inventories.womAccordian;
					}
					if(main.invents.inventories[key].properties[main.account.loggedInUser.user_type+'_editable']){
						main.invents.inventories[key].createHTML();
						accordian.setContent(main.invents.inventories[key].shelf.data('name'),main.invents.inventories[key].html);
						main.invents.inventories[key].launchButton.html.off('click',main.invents.inventories[key].launchWithoutPermission);
					}else{
						accordian.setContent(main.invents.inventories[key].shelf.data('name'),'');
						main.invents.inventories[key].launchButton.html.off('click',main.invents.inventories[key].launchWithoutPermission);
						main.invents.inventories[key].launchButton.html.on('click',main.invents.inventories[key].launchWithoutPermission);
						main.invents.inventories[key].removeEditingSCButs();
						main.invents.inventories[key].clearControlledContent();
					}
					/* if(!main.browser.isMobile){ */main.invents.inventories[key].dataTableAllocate();/* } */
				}
			}
		};
		this.updateForms=function(params){
			//remove forms
			
			for(var key in main.formsMod.forms0){
				if(main.formsMod.forms0[key].properties.active){
					main.utilities.removeForm(key);
				}
			}
			for(var key in params.forms.forms){
				main.forms[key]={
					properties:params.forms.forms[key],
					fields:params.fields[key]
				};
				main.tables[key]=main.forms[key];
			}
			if(main.formsMod){
				main.formsMod.addForms();
			}
			/* 
			var keys=Object.keys(main.forms);
			if(Object.keys(main.forms).length>0){
				//add forms
				for(var key in main.forms){
					form=main.forms[key];
					if(form && (!main.account || form.properties[main.account.loggedInUser.user_type+'_editable'])){
						var accordian=main.formsMod.accordian;
						if(form.properties.groups){
							groups=form.properties.groups;
							thisGroup=main.formsMod.groups['mainGenForms'];
							for(var i=0;i<groups.length;i++){
								groupName=groups[i].name;
								if(!thisGroup.shelves[groupName]){
									var launchButton=new Button('toolBox','',groups[i].alias);
									accordian=new Accordian(groupName);
									var shelf=thisGroup.accordian.createShelf({
										name:groupName,
										button:launchButton.html,
										content:accordian.container,
										alias:groups[i].alias
									});
									thisGroup.shelves[groupName]={
										launchButton:launchButton,
										accordian:accordian,
										shelf:shelf,
										shelves:{}
									}
								}else{
									accordian=thisGroup.shelves[groupName].accordian;
								}
								thisGroup=thisGroup.shelves[groupName];
							}
						}
						form.accordian=accordian;
						form.sidePanel=main.formsMod.sidePanel;
						main.utilities.addForm(form);
					}
				}
			} */
			/* for(var key in main.layout.forms){
				//add new authorized data tables
				if(main.layout.forms[key][self.account.loggedInUser.user_type+'_data_mng'] && main.layout.forms[key].accordian && !main.layout.forms[key].accordian.shelves[key].dataTablePresent){
					main.utilities.addDataTable(main.layout.forms[key]);
				}
				//remove unauthorized new data tables
				if(!main.layout.forms[key][self.account.loggedInUser.user_type+'_data_mng'] && main.layout.forms[key].accordian && main.layout.forms[key].accordian.shelves[key].dataTablePresent){
					main.layout.forms[key].dataLaunch.remove();
					delete main.layout.forms[key].dataLaunch;
					main.layout.forms[key].dataContentWrap.remove();
					delete main.layout.forms[key].dataTable;
					delete main.layout.forms[key].dataContentWrap;
					main.layout.forms[key].accordian.shelves[key].dataTablePresent=false;
				}
			} */
		};
		this.logIn=function(){
			var username=self.username.val().trim();
			if(username==main.account.loggedInUser.username){
				alert(username+' is already logged in.');
				return false;
			}
			if(!Number(this.account.loggedInUser.base_user)){
				self.logOut(self.logInCall);
			}else{
				self.logInCall();
			}
		};
		this.logInCall=function(){
			main.layout.loader.loaderOn();
			var username=self.username.val().trim();
			var pword=self.pword.val().trim();
			if(self.devLoginB){
				var username=self.devLoginEmail;
				var pword=self.devLoginPwd;
			}
			if(!username){
				alert('Please enter an username.');
				return false;
			}
			if(!pword){
				alert('Please enter a password.');
				return false;
			}
			// var withOrgs=false;
			// if(main.tables['organization'] || main.data.lookUpTables['organization']){withOrgs=true;}
			var withOrgs=true;
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						withOrgs:withOrgs,
						username:username,
						pword:pword
					},
					action:'login'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							self.justLoggedIn(response.results);
							self.clearForm();
						}else{
							alert('Error: '+response.message);
							console.log(response);
							main.layout.loader.loaderOff();
						}
					}
				}
			});
		};
		this.clearForm=function(){
			self.html.find('input').val('').blur();
		};
		this.logOut=function(callback){
			self.recentLogOut=true;
			main.account.offSessionCheck();
			if(self.account.loggedInUserType==self.account.default_user_type && !self.loggingOut){/* alert('Already Logged Out'); */return;}
			main.layout.loader.loaderOn();
			self.loggingOut=true;
			main.formsMod.clearForms();
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2]
					},
					action:'logout'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							self.justLoggedOut(response.results);
							if(callback){callback();}
							self.loggingOut=false;
							main.layout.loader.loaderOff();
						}else{
							alert('Error: '+response.message);
							console.log(response);
						}
					}
				}
			});
		};
		this.updateLogInVisibility=function(){
			if(!Number(this.account.loggedInUser.base_user)){
				// this.alreadySignedIn.addClass('block');
				// this.table.addClass('none');
				this.logOutWrap.removeClass('none');
			}else{
				// this.alreadySignedIn.removeClass('block');
				// this.table.removeClass('none');
				this.logOutWrap.addClass('none');
			}
		};
		this.initEvents=function(){
			this.loginButton.html.on('click',function(){
				self.logIn();
			});
			this.logoutButton.html.on('click',function(){
				self.logOut();
			});
			if(this.devLogin){
				this.devLogin.on('click',function(e){
					self.logIn();
				});
			}
			if(this.devLogout){
				this.devLogout.on('click',function(e){
					self.logOut();
				});
			}
		};
		var accountUpdated=false;
		if(main.temp && main.temp.account && main.temp.account.loggedInUser && !this.account.loggedInUser){
			this.account.loggedInUser=main.temp.account.loggedInUser;
			this.account.loggedInUserType=main.temp.account.loggedInUser.user_type;
			this.account.loggedInUser.user_type_data=main.temp.account.loggedInUser.user_type_data;
			this.account.loggedInUser.org_data=main.temp.account.loggedInUser.org_data;
			accountUpdated=true;
		}
		this.createHTML();
		this.initEvents();
		if(accountUpdated){
			if(!Number(this.account.loggedInUser.base_user)){
				if(this.loginSC){this.loginSC.html.add(self.loginSCMobile.html).addClass('none');}
				if(this.logoutSC){this.logoutSC.html.add(self.logoutSCMobile.html).removeClass('none');}
				this.account.initAccountBox();
				this.recentLogOut=false;
				this.account.initSessionCheck();
			}
			this.updateLogInVisibility();
		}
	};
});
