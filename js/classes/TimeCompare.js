define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Panel=require('./Panel');
		var Tabs=require('./Tabs');
		this.withItem='with_time_compare';
		this.alias='Time Comparison';
		this.name='time_compare';
		this.defaultSortOrder='ASC';
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.addShelf=function(){
			self.launchButton=new Button('toolBox','',self.alias);
			self.shelf=main.layout.toolBox.createShelf({
				name:self.name,
				button:self.launchButton.html,
				content:self.html,
				context:self,
				alias:self.alias,
				dontAppendTo:true,
				groups:main.withs[self.withItem].toolbox_groups
			});
			main.layout.toolBox.shelfInAppTest(main.layout.toolBox.shelves[self.name]);
		};
		(this.createHTML=function(){
			self.timeCompareTabs=new Tabs();
			self.panelHTML=$('<div class="timeTrackerWrap">')
				.append(self.timeCompareTabs.html);
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
						listName:self.listName,
						layerName:self.layerName
					},
					action:'getTimeCompareData'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							var results=response.results;
							self.tables={};
							var b=0,tab,activeTab;
							for(var key in results){
								if(main.tables[results[key].layer] && main.tables[results[key].layer].fields[results[key].field_name]){
									self.tables[key]={};
									self.tables[key].thead=$('<thead>')
										.append($('<tr>')
											.append($('<th>').html('Date'))
											.append($('<th>').html('Value')))
									self.tables[key].tbody=$('<tbody>');
									self.tables[key].table=$('<table class="timeCompareTable">')
										.append(self.tables[key].thead)
										.append(self.tables[key].tbody);
									self.tables[key].content=$('<div class="timeCompareTableWrap">').append(self.tables[key].table);
									var lookUpTableMap=main.tables[results[key].layer].properties.lookup_table_map;
									for(var t=0;t<results[key].records.length;t++){
										self.tables[key].tbody
											.append($('<tr>')
												.append($('<td>')
													.append($('<div class="tdLiner">').html(main.utilities.formatDate(results[key].records[t].date)))
												).append($('<td>')
													.append($('<div class="tdLiner">').html(main.utilities.getHTMLFromVal(results[key].records[t].value,main.tables[results[key].layer].fields[results[key].field_name],lookUpTableMap)))
												)
											)
									}
									tab=self.timeCompareTabs.addTab(main.tables[results[key].layer].fields[results[key].field_name].alias+'-'+main.tables[results[key].layer].properties.alias,self.tables[key].content);
									if(b==0){var activeTab=tab;}
									b++;
								}
							}
							if(activeTab){self.timeCompareTabs.setActiveTab(activeTab.tabid);}
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
			
			self.panelOpenButton=new Button('toolBox','','Open');
			self.panelOpenButton.html.on('click',function(){
				self.panel.open();
			});
			self.panel=new Panel({
				content:self.panelHTML,
				title:self.alias,
				classes:'timeComparePanel',
				hideOnOutClick:true
			});
			self.html=$('<div class="timeCompShelfWrap">').append(self.panelOpenButton.html);
			self.addShelf();
		})();
	};
});