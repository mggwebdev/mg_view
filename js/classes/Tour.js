define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Panel=require('./Panel');
		//var Utilities = require('./Utilities');
		this.setToApp=false;
		this.alias='Take a Tour';
		this.addAccShelf=true;
		this.items={};
		this.tour=main.data.tour;
		this.inSettings=false;
		this.minOnOutClick=true;
		this.id=main.utilities.generateUniqueID();
		this.currentSlide = 1;
		this.maxSlide =Object.keys(main.data.tour).length;
		this.withItem='with_tour';



		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		
		this.createShelfContent=function(){
			self.showTourButton=new Button('std','showTour showButton','Show');
			self.hideTourButton=new Button('std','hideTour hideButton','Hide');
			var table=$('<table>')
				.append($('<tbody>')
					.append($('<tr>')
						.append($('<th>')
							.append($('<td>')
								.append($('<div>').addClass('tdLiner')
									.append(self.showTourButton.html)
									.append(self.hideTourButton.html)
								)
							)
						)
					)
				);
			return table
		};
		this.show=function(){
			self.panel.open();
			// self.updateButtonsShow();
		};
		this.hide=function(){
			self.panel.close();
			// self.updateButtonsHide();
		};
		changeSlide=function(direction){
			if (direction == "next"){
				if (self.currentSlide < self.maxSlide)
					self.currentSlide++;
				else 
					self.currentSlide =1; 
			}
			else if(direction =="prev"){
				if (self.currentSlide>1)
					self.currentSlide--;
				else
					self.currentSlide=self.maxSlide;
			}
			$('#tour_image').attr('src',main.localImgs+self.tour[self.currentSlide].image);
			$('#tourText').html(self.tour[self.currentSlide].text);
			$('#slideCounter').html("Slide "+self.currentSlide+" of "+self.maxSlide+".");
		};
		if(self.addAccShelf){
			this.updateButtonsShow=function(){
				self.showTourButton.html.removeClass('inlineBlock');
				self.hideTourButton.html.removeClass('none');
			};
			this.updateButtonsHide=function(){
				self.showTourButton.html.addClass('inlineBlock');
				self.hideTourButton.html.addClass('none');
			}; 
		};
		evalLink=function(link){
			window.open(link,'_blank');
		};
		evalSidebar=function(key,sidePanel){
			main.utilities.openForm(key,sidePanel);
			self.hide();
		};
		
		(this.createHTML=function(){
			if(!self.inSettings){
				//shelf
				if(self.addAccShelf){
					self.launchButton=new Button('toolBox','',self.alias);
					self.shelfContent=self.createShelfContent();
					self.shelf=main.layout.toolBox.createShelf({
						name:'tour',
						button:self.launchButton.html,
						content:self.shelfContent,
						alias:self.alias,
						groups:main.withs[self.withItem].toolbox_groups
					});
				}
			}else{
				var settingsStuff=main.settings.addSetting('Tour Window');
				self.showTourButton=settingsStuff.showButton;
				self.hideTourButton=settingsStuff.hideButton;
			}
			self.titleHTML=$('<div>').addClass('welcomeTitle welcomeRibbon');
				$('<h1>').addClass('subHeader center').html('Getting Started Tour').appendTo(self.titleHTML);		
			self.tourTopRow=$('<div>').addClass('row tourTopRow cf');
			$('<div>').addClass('rowEle tourTopRowEle')
				.append($('<div>').addClass('imgwrap')
					.append($('<img id=tour_image>').addClass('tourMainImg containerImg').attr('src',main.localImgs+self.tour[1].image)))
						.appendTo(self.tourTopRow);
			
		 	self.tourBottomRow=$('<div>').addClass('row tourBottomRow cf');
			$('<div>').addClass('rowEle tourBottomRowEle')
				//.append($('<div>').addClass('imgwrap')
					.append($('<input id=tourPrevButton type="image"/>').addClass('prevButton').attr('src',main.mainPath+'images/pointer_left_med.png').click(function(){
								changeSlide('prev');
							})//)
					)
						.appendTo(self.tourBottomRow); 
			$('<div>').addClass('rowEle tourBottomRowEle')
						.append($('<p id=tourText>').html(self.tour[1].text))
						.appendTo(self.tourBottomRow);	
			$('<div>').addClass('rowEle tourBottomRowEle')
				//.append($('<div>').addClass('imgwrap')
					.append($('<input id=tournextButton type="image"/>').addClass('nextButton').attr('src',main.mainPath+'images/pointer_right_med.png').click(function(){
								changeSlide('next');
							}))//)
						.appendTo(self.tourBottomRow); 
						
			self.tourslideCounterRow=$('<div>').addClass('row slideCounterRow cf');			
			$('<div>').addClass('rowEle tourslideCounterRowEle')
						.append($('<p id=slideCounter>').html("Slide "+1+" of "+self.maxSlide+"."))
						.appendTo(self.tourslideCounterRow);				
						
			
			self.content=$('<div>').addClass('tourContent')
 				.append(self.titleHTML)
				.append(self.tourTopRow)
				.append(self.tourBottomRow)
				.append(self.tourslideCounterRow);
				
				// self.panel=new Panel(self.html,'Data Management','Data Management','dataTablePanel');
			self.panel=new Panel({
				content:self.content,
				title:'Tour',
				classes:'tourWindow',
				position:'mMiddle',
				matchHeight:true,
				relativeTo:main.layout.mapContainerWrap,
				minOnOutClick:self.minOnOutClick,
				onClose:self.updateButtonsHide,
				onMin:self.updateButtonsHide,
				onShow:self.updateButtonsShow,
				withOutClose:false
			});
			
 			
			// self.panel=new Panel(self.content,'','','legend',null,'tRight',main.layout.mapContainerWrap,self.updateButtonsHide,self.updateButtonsHide,self.updateButtonsShow);
		})();
		(this.initEvents=function(){
			self.panel.html.on('change','.TourLayerCheckWrap',function(){
				var TourID=$(this).data('Tourid');
				var visible=$(this).prop('checked');
			});
			if(self.addAccShelf){
				self.hideTourButton.html.on('click',function(){
					self.hide();
				});
				self.showTourButton.html.on('click',function(){
					self.show();
					
				});
			}
			self.panel.html.find('img').one('load',function(){
				self.panel.positionPanel();
			}).each(function(){
				if(this.complete)$(this).load();
			});
		})();
		if(this.setToApp){this.show();}else{this.updateButtonsHide()}
	};
});
