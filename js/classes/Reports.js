define(function(require){
        var self;
		var Button=require('./Button');
        function pg(){

            this.count = 0;
            this.init();
            this.initEvents();
        };

        pg.prototype={
            init:   function() {
                _self = this;
                this.createHTML();
            },
            alias:'Reports',
            defaultType:'length',
            currentType:null,
            formID:'reportsForm',
            
            createHTML:function(){
                _self.addShelf();
            },
            
            privateData: null,
            setPrivateData:function(x) {
                _self.privateData = x;
            },
            getPrivateData:function() {
                return _self.privateData;
            },
            
            addShelf:function(){
                _self.launchButton=new Button('toolBox','',_self.alias);
                _self.reportsForm=_self.createReportsForm();
                _self.shelf=main.layout.reports.createShelf('Report',_self.launchButton.html,_self.reportsForm,_self.alias);
                main.layout.reports.container.append(_self.shelf);
            },
            initEvents:function(){
                _self.launchButton.html.on('click', function(e) {

                });
                _self.reports01Button.on('click', function(e) {
                    _self.openReport(e.target.attributes.reportname);
                    
                });
                _self.reports02Button.on('click', function(e) {
                });
            },
            createReportsForm:function(){
                var reportsForm=$('<form>').attr('id',_self.formID);
                
                var table=$('<table>').addClass('shelfForm')
                    .append($('<tr>')
                        .append($('<th colspan="2">')
                            .append($('<div>').addClass('tdLiner center').text('Reports'))
                        )
                    );
                    
                _self.reports01Button=$('<div>').addClass('button reports01').text('Report 01').attr('reportname','rpt');
                _self.reports02Button=$('<div>').addClass('button reports02').text('Report 02');

                
                table.append($('<tr>')
                        .append($('<td colspan="2">').append($('<div>').addClass('tdLiner').append(_self.reports01Button)))
                    );
                table.append($('<tr>')
                    .append($('<td colspan="2">').append($('<div>').addClass('tdLiner').append(_self.reports02Button)))
                );
                return reportsForm.append(table);
            },
            openReport:function(reportName){
                var rptContents = 'no data';
                var params = {'action':'rptTreeRequestYardV2'};
                
                $.post( "server/reporting.php", params, function(data) {
                        _self.makeReportWindow(data);
                    }).fail(function(){alert('error retrieving report');console.log(response);});
                
                
            },
            //creates floating div for report; expects html formatted data
            makeReportWindow:function(reportContents){
                var reportWin = $('<div>').addClass('reportWindow');
                _self.closeWinBtn = $('<div>').addClass('closeButton').text('(close)');
                $('body').append(reportWin);
                $('.reportWindow').html(reportContents);
                $('.reportWindow').append(_self.closeWinBtn);
                $('.reportWindow').show();
                _self.closeWinBtn.on('click', function(e) {
                    $(this).parent().remove();
                });
            },
 
        };
    return pg;
});