define(function(require){
	return function pg(args){
		var Button=require('./Button');
		var self=this;
		this.alias='About';
		this.withItem='with_about';
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.createHTML=function(){
			self.content=main.personal_settings.app_settings.about_html || '';
			self.html=$('<div class="aboutTB">')
				.append($('<div class="aboutTBContent">')
					.html(self.content)
				);
		};
		this.addShelf=function(){
			self.launchButton=new Button('toolBox','',self.alias);
			self.shelf=main.layout.toolBox.createShelf({
				name:'about',
				button:self.launchButton.html,
				content:self.html,
				alias:self.alias,
				extended:true,
				groups:main.withs[self.withItem].toolbox_groups
			});
			// self.shelf.addClass('extended');
		};
		(this.init=function(){
			self.createHTML();
			self.addShelf();
		})();
	};
});