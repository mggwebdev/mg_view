define(function(){
	return function (args){
		ol.interaction.Pointer.call(this, {
			handleDownEvent: this.handleDownEvent,
			handleDragEvent: this.handleDragEvent,
			handleMoveEvent: this.handleMoveEvent,
			handleUpEvent: this.handleUpEvent
		  });

		  /**
		   * @type {ol.Pixel}
		   * @private
		   */
		  this.coordinate_ = null;

		  /**
		   * @type {string|undefined}
		   * @private
		   */
		  this.cursor_ = 'pointer';

		  /**
		   * @type {ol.Feature}
		   * @private
		   */
		  this.feature_ = null;

		  /**
		   * @type {string|undefined}
		   * @private
		   */
		  this.previousCursor_ = undefined;
		  
		  ol.inherits(this, ol.interaction.Pointer);
		  
		  this.prototype.handleDownEvent = function(evt) {
		  var map = evt.map;

		  var feature = map.forEachFeatureAtPixel(evt.pixel,
			  function(feature, layer) {
				return feature;
			  });

		  if (feature) {
			this.coordinate_ = evt.coordinate;
			this.feature_ = feature;
		  }

		  return !!feature;
		};
		this.prototype.handleDragEvent = function(evt) {
		  var map = evt.map;

		  var feature = map.forEachFeatureAtPixel(evt.pixel,
			  function(feature, layer) {
				return feature;
			  });

		  var deltaX = evt.coordinate[0] - this.coordinate_[0];
		  var deltaY = evt.coordinate[1] - this.coordinate_[1];

		  var geometry = /** @type {ol.geom.SimpleGeometry} */
			  (this.feature_.getGeometry());
		  geometry.translate(deltaX, deltaY);

		  this.coordinate_[0] = evt.coordinate[0];
		  this.coordinate_[1] = evt.coordinate[1];
		};
		this.prototype.handleMoveEvent = function(evt) {
		  if (this.cursor_) {
			var map = evt.map;
			var feature = map.forEachFeatureAtPixel(evt.pixel,
				function(feature, layer) {
				  return feature;
				});
			var element = evt.map.getTargetElement();
			if (feature) {
			  if (element.style.cursor != this.cursor_ && (!main.invents || !main.invents.adding)) {
				this.previousCursor_ = element.style.cursor;
				element.style.cursor = this.cursor_;
			  }
			} else if (this.previousCursor_ !== undefined) {
			  element.style.cursor = this.previousCursor_;
			  this.previousCursor_ = undefined;
			}
		  }
		};
		this.prototype.handleUpEvent = function(evt) {
		  this.coordinate_ = null;
		  this.feature_ = null;
		  return false;
		};
	};
});