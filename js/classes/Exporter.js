define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Checkbox=require('./Checkbox');
		var Panel=require('./Panel');
		var Tabs=require('./Tabs');
		var ViewMore=require('./ViewMore');
		this.alias='Exporter';
		this.name='exporter';
		this.withItem='with_exporter';
		this.currentLayer=null;
		this.exporting=false;
		this.currentFieldStats=null;
		this.currentTemplate=null;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.addShelf=function(){
			self.launchButton=new Button('toolBox','',self.alias);
			self.accordian=main.layout.toolBox;
			self.shelf=main.layout.toolBox.createShelf({
				name:self.name,
				button:self.launchButton.html,
				content:self.html,
				context:self,
				alias:self.alias,
				dontAppendTo:true,
				groups:main.withs[self.withItem].toolbox_groups
			});
			main.layout.toolBox.shelfInAppTest(main.layout.toolBox.shelves[self.name]);
		};
		this.contExport=function(){
			if(self.exporting){return;}
			self.exporting=true;
			var pids=[];
			var limit=null;
			var firstIndex=null;
			var ecobens=null;
			var exportForUpload=null;
			var fileName='Export';
			var exportType='csv';
			if(self.fileNameInput.val()){
				fileName=self.fileNameInput.val();
				fileName=fileName.replace(/\W/g,'_');
			}
			if(self.exporterWithEcobensInput && self.exporterWithEcobensInput.checkbox.prop('checked')){	
				ecobens=main.globalSettings.ecobens;
				
			}
			if(self.exportTypeSelect.val()){exportType=self.exportTypeSelect.val();}
			if(self.limitInput.val()){
				limit=self.limitInput.val();
				if(limit<1){limit=null;}
			}
			if(self.exportForUploadInput && self.exportForUploadInput.checkbox.prop('checked')){exportForUpload=true;}
			
			var fields={},place_in_order,minimized,iSGeoField;
			var inFields=self.currentLayer.fields;
			var internal_field_map=self.currentLayer.properties.internal_field_map;
			for(var key in inFields){
				iSGeoField=null;
				if(internal_field_map && internal_field_map.geometry_field && key==internal_field_map.geometry_field){iSGeoField=true;}
				if(self.currentFieldStats && self.currentFieldStats.order[key]){place_in_order=self.currentFieldStats.order[key].place_in_order}else{
					place_in_order=inFields[key].place_in_order;}
				if(self.currentFieldStats && self.currentFieldStats.order[key]){minimized=self.currentFieldStats.order[key].min}else{
					minimized=false;}
				if(self.currentFieldStats && self.currentFieldStats.order[key]){omit=self.currentFieldStats.order[key].omit}else{
					omit=false;}
				if(self.currentFieldStats && self.currentFieldStats.order[key]){protect=self.currentFieldStats.order[key].protect}else{
					protect=false;}
				if(!minimized){minimized=null;}
				if(!omit){omit=null;}
				if(!protect){protect=null;}
				fields[key]={
					alias:inFields[key].alias,
					lookup_table:inFields[key].lookup_table,
					options:inFields[key].options,
					input_type:inFields[key].input_type,
					atc_protected:inFields[key].atc_protected,
					place_in_order:place_in_order,
					minimized:minimized,
					omit:omit,
					iSGeoField:iSGeoField,
					protect:protect,
					referencer:inFields[key].referencer,
					data_type:inFields[key].data_type
				}
			}
			
			var where='';
			var props=self.currentLayer.properties;
			if(props && props.dependent_upon && props.internal_field_map && props.internal_field_map.dependent_upon_field && !main.account.loggedInUser.dont_filter){
				if(main.account.loggedInUser && main.account.loggedInUser.org_data && !props[main.account.loggedInUser.user_type+'_download_all']){
					var filterPID=main.account.loggedInUser.org_data.orgId; 
				}else if(main.invents.currentFilterPID){
					var filterPID=main.invents.currentFilterPID;
				}
				if(filterPID){
					var dependent_upon_field=props.internal_field_map.dependent_upon_field;
					if(where){where+=' AND ';}
					where=dependent_upon_field+"='"+filterPID+"'";
				}
			}
		
			//adv filter
			if(main.advFilter && main.advFilter.appliesTo['export'].apply && main.advFilter.currentLayer && main.advFilter.currentLayer.properties.layer_name==self.currentLayer.properties.name){
				if(main.advFilter.where){
					if(where){where+=' AND ';}
					where+='('+main.advFilter.where+')';
				}
				if(main.advFilter.geoFilter && main.advFilter.geoFilter.length){
					if(pids.length){
						for(var i=0;i<pids.length;i++){
							if(main.advFilter.geoFilter.indexOf(Number(pids[i]))==-1){
								pids.splice(i,1);
							}
						}
					}else{pids=main.advFilter.geoFilter;}
				}
			}
			if(main.dataTableFilter && main.dataTableFilter.length){
				pids=main.dataTableFilter;
				main.dataTableFilter=null;
			}
			if(pids.length==0){pids=null;}
			if(pids){pids=pids.join(',');}
			if(main.filter_legend && main.filter_legend.active && !main.filter_legend.hideOnly && main.filter_legend.filterWhere && main.filter_legend.layerName==self.currentLayer.properties.name){
				if(where){where+=' AND ';}
				where+='('+main.filter_legend.filterWhere+')';
			}
			if(exportForUpload){exportType='excel';}
			main.layout.loader.loaderOn();
			var withEcobens=null;
			if(main.withs.with_ecobens.value){withEcobens=true;}
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						layerName:self.currentLayer.properties.name,
						firstIndex:firstIndex,
						sortBy:(self.currentFieldStats)?self.currentFieldStats.sort['sortBy']:null,
						sortBy2:(self.currentFieldStats)?self.currentFieldStats.sort['sortBy2']:null,
						sortBy3:(self.currentFieldStats)?self.currentFieldStats.sort['sortBy3']:null,
						sortBy4:(self.currentFieldStats)?self.currentFieldStats.sort['sortBy4']:null,
						sortOrder:(self.currentFieldStats)?self.currentFieldStats.sort['sortOrder']:null,
						sortOrder2:(self.currentFieldStats)?self.currentFieldStats.sort['sortOrder2']:null,
						sortOrder3:(self.currentFieldStats)?self.currentFieldStats.sort['sortOrder3']:null,
						sortOrder4:(self.currentFieldStats)?self.currentFieldStats.sort['sortOrder4']:null,
						limit:limit,
						ecobens:ecobens,
						exportForUpload:exportForUpload,
						where:where,
						pids:pids,
						exportType:exportType,
						fileName:fileName,
						pathname:window.location.pathname,
						fields:JSON.stringify(fields),
						internal_field_map:JSON.stringify(self.currentLayer.properties.internal_field_map),
						lookup_table_map:JSON.stringify(self.currentLayer.properties.lookup_table_map),
					},
					action:'exportBegin'
				},
				success:function(response){
					self.exporting=false;
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							window.open(response.absPath);
							if(self.optionsInPanel && self.optionsInPanel.opened){self.optionsInPanel.close()}
							// main.utilities.deleteFile(response.filesToDelete);
						}else{
							alert('Error');
							console.log(response);
						}
						main.layout.loader.loaderOff();
					}
				}
			});
		};
		this.exportFromDT=function(layer){
			self.layerSelect.val(layer).change();
			self.convertToPanel();
		};
		this.beginExport=function(){
			if(self.currentLayer){
				self.contExport();
			}else{
				alert('Please select a layer.');
			}
		};
		this.clearSheetPrepPanel=function(){
			self.sheetPrepForm.find('.formInput.protectedCheck,.formInput.omitCheck').prop('checked',false);
		};
		this.layerSelectChange=function(){
			if(self.layerSelect.val()){
				if(main.invents && main.invents.inventories[self.layerSelect.val()]){
					self.currentLayer=main.invents.inventories[self.layerSelect.val()];
					self.currentSet='inventory';
				}else if(main.canopy && main.canopy.layers[self.layerSelect.val()]){
					self.currentLayer=main.canopy.layers[self.layerSelect.val()];
					self.currentSet='canopy';
				}
				self.defaultField=null;
				if(self.currentLayer.properties.internal_field_map.last_modified){
					self.defaultField=self.currentLayer.properties.internal_field_map.last_modified;
				}
				self.updateFieldMngr();
			}else{
				self.currentLayer=null;
			}
		};
		this.layerSelectUpdate=function(){
			self.layerSelect.html('');
			self.layerSelect.append('<option value=""></option>');
			if(main.invents){
				for(var key in main.invents.inventories){					
					if(main.invents.inventories[key] && main.invents.inventories[key].properties.exportable){
						self.layerSelect.append('<option value="'+key+'">'+main.invents.inventories[key].properties.alias+'</option>');
					}
				}
			}
			if(main.canopy){
				for(var key in main.canopy.layers){					
					if(main.canopy.layers[key].properties.exportable){
						self.layerSelect.append('<option value="'+key+'">'+main.canopy.layers[key].properties.alias+'</option>');
					}
				}
			}
		};
		this.sortByFieldSelectUpdateDefault=function(){
			var fields=self.currentLayer.fields,selected;
			self.sortBySelect.html('');
			self.sortBySelect2.html('');
			self.sortBySelect3.html('');
			self.sortBySelect4.html('');
			self.sortBySelect.append('<option value=""></option>');
			self.sortBySelect2.append('<option value=""></option>');
			self.sortBySelect3.append('<option value=""></option>');
			self.sortBySelect4.append('<option value=""></option>');
			for(var key in fields){
				selected='';
				if(self.defaultField && self.defaultField==key){
					selected=' selected="selected"';
				}
				self.sortBySelect.append('<option value="'+key+'"'+selected+'>'+fields[key].alias+'</option>');
				self.sortBySelect2.append('<option value="'+key+'">'+fields[key].alias+'</option>');
				self.sortBySelect3.append('<option value="'+key+'">'+fields[key].alias+'</option>');
				self.sortBySelect4.append('<option value="'+key+'">'+fields[key].alias+'</option>');
			}
			main.utilities.sortOptions(self.sortBySelect);
			main.utilities.sortOptions(self.sortBySelect2);
			main.utilities.sortOptions(self.sortBySelect3);
			main.utilities.sortOptions(self.sortBySelect4);
		};
		this.sortByFieldSelectUpdate=function(){
			self.sortByFieldSelectUpdateDefault();
			if(self.currentFieldStatsTemp && self.currentFieldStatsTemp.sort){
				if(self.currentFieldStatsTemp.sort.sortBy){self.sortBySelect.val(self.currentFieldStatsTemp.sort.sortBy);}
				if(self.currentFieldStatsTemp.sort.sortBy2){self.sortBySelect2.val(self.currentFieldStatsTemp.sort.sortBy2);}
				if(self.currentFieldStatsTemp.sort.sortBy3){self.sortBySelect3.val(self.currentFieldStatsTemp.sort.sortBy3);}
				if(self.currentFieldStatsTemp.sort.sortBy4){self.sortBySelect4.val(self.currentFieldStatsTemp.sort.sortBy4);}
				if(self.currentFieldStatsTemp.sort.sortOrder){self.sortOrderSelect.val(self.currentFieldStatsTemp.sort.sortOrder);}
				if(self.currentFieldStatsTemp.sort.sortOrder2){self.sortOrderSelect2.val(self.currentFieldStatsTemp.sort.sortOrder2);}
				if(self.currentFieldStatsTemp.sort.sortOrder3){self.sortOrderSelect3.val(self.currentFieldStatsTemp.sort.sortOrder3);}
				if(self.currentFieldStatsTemp.sort.sortOrder4){self.sortOrderSelect4.val(self.currentFieldStatsTemp.sort.sortOrder4);}
			}
		};
		this.saveCurrentFieldStats=function(){
			self.currentFieldStats={
				order:{},
				sort:{}
			};
			self.fieldMngTBody.find('.expFieldMngTR').each(function(){
				self.currentFieldStats.order[$(this).data('fieldname')]={fieldName:$(this).data('fieldname'),place_in_order:$(this).find('.exportFieldMngPIO').val(),min:$(this).find('.exportFieldMngMinChkBx').prop('checked'),omit:$(this).find('.exportFieldMngOmitChkBx').prop('checked'),protect:$(this).find('.exportFieldMngProtChkBx').prop('checked')};
			});
			self.currentFieldStats.sort['sortBy']=self.sortBySelect.val();
			self.currentFieldStats.sort['sortBy2']=self.sortBySelect2.val();
			self.currentFieldStats.sort['sortBy3']=self.sortBySelect3.val();
			self.currentFieldStats.sort['sortBy4']=self.sortBySelect4.val();
			self.currentFieldStats.sort['sortOrder']=self.sortOrderSelect.val();
			self.currentFieldStats.sort['sortOrder2']=self.sortOrderSelect2.val();
			self.currentFieldStats.sort['sortOrder3']=self.sortOrderSelect3.val();
			self.currentFieldStats.sort['sortOrder4']=self.sortOrderSelect4.val();
		};
		this.saveFieldOptions=function(template){
			self.saveCurrentFieldStats();
			if(main.account && main.account.loggedInUser && !main.account.loggedInUser.base_user && main.account.loggedInUser.pid){
				if(!main.account.loggedInUser.export_field_settings){main.account.loggedInUser.export_field_settings={templates:{}};}
				main.account.loggedInUser.export_field_settings.templates[template]=self.currentFieldStats;
				$.ajax({
					type:'POST',
					url:main.mainPath+'server/db.php',
					data:{
						params:{
							folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
							pid:main.account.loggedInUser.pid,
							export_field_settings:main.account.loggedInUser.export_field_settings
						},
						action:'updateExportFieldSettings'
					},
					success:function(response){
						if(response){
							response=JSON.parse(response);
							if(response.status=="OK"){
								self.setTempButtonsForLoad();
								self.setTempButtonsForSave();
							}else{
								alert('Error');
								console.log(response);
							}
						}
					}
				});
			}
		};
		this.resetFieldMngr=function(){
			self.currentFieldStats={
				order:{},
				sort:{}
			};
			self.currentFieldStatsTemp={
				order:{},
				sort:{}
			};
			self.setMngFieldsPanelDefault();
			self.sortByFieldSelectUpdateDefault();
			self.saveCurrentFieldStats();
		};
		this.clearMngFieldsPanel=function(){
			self.fieldMngTBody.find('.expFieldMngTR').each(function(){
				$(this).find('.exportFieldMngPIO').val('');
				$(this).find('.exportFieldMngMinChkBx').prop('checked',false);
				$(this).find('.exportFieldMngOmitChkBx').prop('checked',false);
				$(this).find('.exportFieldMngProtChkBx').prop('checked',false);
			});
			self.mngFieldOptions2.find('select').val('');
		};
		this.setMngFieldsPanelDefault=function(){
			var t=0;
			self.fieldMngTBody.find('.expFieldMngTR').each(function(){
				$(this).find('.exportFieldMngPIO').val(t);
				$(this).find('.exportFieldMngMinChkBx').prop('checked',false);
				$(this).find('.exportFieldMngOmitChkBx').prop('checked',false);
				$(this).find('.exportFieldMngProtChkBx').prop('checked',false);
				t++;
			});
		};
		this.setMngFieldsPanel=function(){
			if(self.currentFieldStatsTemp && Object.keys(self.currentFieldStatsTemp.order).length){
				var fieldName;
				self.fieldMngTBody.find('.expFieldMngTR').each(function(){
					fieldName=$(this).data('fieldname');
					if(self.currentFieldStatsTemp.order[fieldName]){
						minChecked=false;
						omitChecked=false;
						protectChecked=false;
						if(main.utilities.eleToBool(self.currentFieldStatsTemp.order[fieldName].min,true)){minChecked=true;}
						if(main.utilities.eleToBool(self.currentFieldStatsTemp.order[fieldName].omit,true)){omitChecked=true;}
						if(main.utilities.eleToBool(self.currentFieldStatsTemp.order[fieldName].protect,true)){protectChecked=true;}
						$(this).find('.exportFieldMngPIO').val(self.currentFieldStatsTemp.order[fieldName].place_in_order);
						$(this).find('.exportFieldMngMinChkBx').prop('checked',minChecked);
						$(this).find('.exportFieldMngOmitChkBx').prop('checked',omitChecked);
						$(this).find('.exportFieldMngProtChkBx').prop('checked',protectChecked);
					}else{
						self.fieldMngTBody.find('.expFieldMngTR').each(function(){
							$(this).find('.exportFieldMngPIO').val(99999);
							$(this).find('.exportFieldMngMinChkBx').prop('checked',false);
							$(this).find('.exportFieldMngOmitChkBx').prop('checked',false);
							$(this).find('.exportFieldMngProtChkBx').prop('checked',false);
						});
					}
				});
			}else{
				var t=0;
				self.fieldMngTBody.find('.expFieldMngTR').each(function(){
					$(this).find('.exportFieldMngPIO').val(t);
					$(this).find('.exportFieldMngMinChkBx').prop('checked',false);
					$(this).find('.exportFieldMngOmitChkBx').prop('checked',false);
					$(this).find('.exportFieldMngProtChkBx').prop('checked',false);
					t++;
				});
			}
		};
		this.setCurrentFieldStatsWithSaved=function(template){
			self.currentFieldStatsTemp={
				order:{},
				sort:{}
			};
			for(var key in main.account.loggedInUser.export_field_settings.templates[template].order){
				self.currentFieldStatsTemp.order[key]=main.account.loggedInUser.export_field_settings.templates[template].order[key];
				self.currentFieldStatsTemp.order[key].min=main.utilities.eleToBool(main.account.loggedInUser.export_field_settings.templates[template].order[key].min,true);
				self.currentFieldStatsTemp.order[key].omit=main.utilities.eleToBool(main.account.loggedInUser.export_field_settings.templates[template].order[key].omit,true);
				self.currentFieldStatsTemp.order[key].protect=main.utilities.eleToBool(main.account.loggedInUser.export_field_settings.templates[template].order[key].protect,true);
			}
			self.currentFieldStatsTemp.sort.sortBy=main.account.loggedInUser.export_field_settings.templates[template].sort.sortBy;
			self.currentFieldStatsTemp.sort.sortBy2=main.account.loggedInUser.export_field_settings.templates[template].sort.sortBy2;
			self.currentFieldStatsTemp.sort.sortBy3=main.account.loggedInUser.export_field_settings.templates[template].sort.sortBy3;
			self.currentFieldStatsTemp.sort.sortBy4=main.account.loggedInUser.export_field_settings.templates[template].sort.sortBy4;
			self.currentFieldStatsTemp.sort.sortOrder=main.account.loggedInUser.export_field_settings.templates[template].sort.sortOrder;
			self.currentFieldStatsTemp.sort.sortOrder2=main.account.loggedInUser.export_field_settings.templates[template].sort.sortOrder2;
			self.currentFieldStatsTemp.sort.sortOrder3=main.account.loggedInUser.export_field_settings.templates[template].sort.sortOrder3;
			self.currentFieldStatsTemp.sort.sortOrder4=main.account.loggedInUser.export_field_settings.templates[template].sort.sortOrder4;
		};
		this.loadSavedCurrentFieldStats=function(template){
			self.setCurrentFieldStatsWithSaved(template);
			self.clearMngFieldsPanel();
			self.setMngFieldsPanel();
			self.sortByFieldSelectUpdate();
			self.saveCurrentFieldStats();
		};
		this.updateOptions1=function(){
			self.fieldMngTBody.html('');
			var t=1;
			var fields=self.currentLayer.fields;
			var keys=main.utilities.sortFields(fields,'abc');
			for(var i=0;i<keys.length;i++,t++){
				key=keys[i];
				field=fields[key];
				if(field.active){
					val=t;
					minChecked='';
					omitChecked='';
					protChecked='';
					minDisabled='';
					omitDisabled='';
					protDisabled='';
					if(field.lookup_table || field.atc_protected){
						protChecked=' checked="checked"';
						protDisabled=' disabled="disabled"';
					}
					if(field.atc_unomittable){
						omitChecked='';
						omitDisabled=' disabled="disabled"';
					}
					self.fieldMngTBody.append($('<tr class="expFieldMngTR efMngTR_'+field.name+'"" data-fieldname="'+field.name+'">')
						.append($('<td>').html(field.alias))
						.append($('<td class="center">').html('<input type="number" class="exportFieldMngPIO" value="'+val+'"/>'))
						.append($('<td class="center">').html('<input type="checkbox" class="exportFieldMngMinChkBx"'+minChecked+minDisabled+'/>'))
						.append($('<td class="center">').html('<input type="checkbox" class="exportFieldMngOmitChkBx"'+omitChecked+omitDisabled+'/>'))
						.append($('<td class="center">').html('<input type="checkbox" class="exportFieldMngProtChkBx"'+protChecked+protDisabled+'/>')));
				}
			}
		};
		this.updateFieldMngr=function(){
			if(self.currentLayer){
				self.updateOptions1();
				self.sortByFieldSelectUpdateDefault();
			}
		};
		this.newTab=function(){
			self.mngFieldsPanel.positionPanel();
		};
		this.createMngFieldsPanel=function(){
			self.exportFieldMngWrap=$('<div class="exportFieldMngWrap">');
			self.minChAll=$('<input type="checkbox" class="expMngChAll"/>');
			self.minChAll.on('change',function(){
				self.fieldMngTBody.find('.exportFieldMngMinChkBx').prop('checked',$(this).prop('checked'));
			});
			self.omitChAll=$('<input type="checkbox" class="expMngChAll"/>');
			self.omitChAll.on('change',function(){
				self.fieldMngTBody.find('.exportFieldMngOmitChkBx').prop('checked',$(this).prop('checked'));
			});
			self.protectChAll=$('<input type="checkbox" class="expMngChAll"/>');
			self.protectChAll.on('change',function(){
				self.fieldMngTBody.find('.exportFieldMngProtChkBx:not(:disabled)').prop('checked',$(this).prop('checked'));
			});
			self.fieldMngTBody=$('<tbody>');
			self.fieldMngTable=$('<table class="fieldMngTable">')
				.append($('<thead>').append($('<tr>')
					.append($('<th class="center">').html('Field Name<br/>&nbsp;'))
					.append($('<th class="center">').html('Order<br/>&nbsp;'))
					.append($('<th class="center">').html('Minimize<br/>').append(self.minChAll))
					.append($('<th class="center">').append('Omit<br/>').append(self.omitChAll))
					.append($('<th class="center">').html('Protect<br/>').append(self.protectChAll))))
				.append(self.fieldMngTBody);
			self.sortBySelect=$('<select class="exporterSortBySelect">');
			self.sortBySelect.append('<option value="">Select a Layer First</option>');
			self.sortBySelect2=$('<select class="exporterSortBySelect">');
			self.sortBySelect2.append('<option value="">Select a Layer First</option>');
			self.sortBySelect3=$('<select class="exporterSortBySelect">');
			self.sortBySelect3.append('<option value="">Select a Layer First</option>');
			self.sortBySelect4=$('<select class="exporterSortBySelect">');
			self.sortBySelect4.append('<option value="">Select a Layer First</option>');
			self.sortOrderSelect=$('<select class="exporterSortOrderSelect">');
			self.sortOrderSelect.append('<option value="DESC" selected>Descending</option>');
			self.sortOrderSelect.append('<option value="ASC">Ascending</option>');
			self.sortOrderSelect2=$('<select class="exporterSortOrderSelect">');
			self.sortOrderSelect2.append('<option value="DESC" selected>Descending</option>');
			self.sortOrderSelect2.append('<option value="ASC">Ascending</option>');
			self.sortOrderSelect3=$('<select class="exporterSortOrderSelect">');
			self.sortOrderSelect3.append('<option value="DESC" selected>Descending</option>');
			self.sortOrderSelect3.append('<option value="ASC">Ascending</option>');
			self.sortOrderSelect4=$('<select class="exporterSortOrderSelect">');
			self.sortOrderSelect4.append('<option value="DESC" selected>Descending</option>');
			self.sortOrderSelect4.append('<option value="ASC">Ascending</option>');
			self.mngFieldOptions2=$('<div class="mngFieldOpions2">')
				.append($('<table>')
					.append($('<tr>')
						.append($('<td>').append($('<div class="tdLiner">').text('Sort By')))
						.append($('<td>').append($('<div class="tdLiner">').append(self.sortBySelect)))
					)
					.append($('<tr>')
						.append($('<td>').append($('<div class="tdLiner">').text('Sort Order')))
						.append($('<td>').append($('<div class="tdLiner">').append(self.sortOrderSelect)))
					)
					.append($('<tr>')
						.append($('<td>').append($('<div class="tdLiner">').text('Sort By 2')))
						.append($('<td>').append($('<div class="tdLiner">').append(self.sortBySelect2)))
					)
					.append($('<tr>')
						.append($('<td>').append($('<div class="tdLiner">').text('Sort Order 2')))
						.append($('<td>').append($('<div class="tdLiner">').append(self.sortOrderSelect2)))
					)
					.append($('<tr>')
						.append($('<td>').append($('<div class="tdLiner">').text('Sort By 3')))
						.append($('<td>').append($('<div class="tdLiner">').append(self.sortBySelect3)))
					)
					.append($('<tr>')
						.append($('<td>').append($('<div class="tdLiner">').text('Sort Order 3')))
						.append($('<td>').append($('<div class="tdLiner">').append(self.sortOrderSelect3)))
					)
					.append($('<tr>')
						.append($('<td>').append($('<div class="tdLiner">').text('Sort By 4')))
						.append($('<td>').append($('<div class="tdLiner">').append(self.sortBySelect4)))
					)
					.append($('<tr>')
						.append($('<td>').append($('<div class="tdLiner">').text('Sort Order 4')))
						.append($('<td>').append($('<div class="tdLiner">').append(self.sortOrderSelect4)))
					)
				)
			self.mngFieldTabs=new Tabs();
			var activeTab=self.mngFieldTabs.addTab('Order',self.fieldMngTable,null,self.newTab);
			self.mngFieldTabs.addTab('Sort',self.mngFieldOptions2,null,self.newTab);
			self.mngFieldTabs.setActiveTab(activeTab.tabid);
			self.exportFieldMngWrap.append(self.mngFieldTabs.html);
			self.resetButton=new Button('std','exporterResetFieldMngButton','Reset');
			self.resetButton.html.on('click',function(){
				self.resetFieldMngr();
			});
			self.loadSavedButton=new Button('std','exportMngFieldLoad','Load Last');
			self.loadSavedButton.html.on('click',function(){
				self.openLoadLastTemplatePanel();
			});
			self.saveFieldOptionsButton=new Button('std','saveFieldOptionsButton','Save & Close');
			self.saveFieldOptionsButton.html.on('click',function(){
				self.openSaveTemplatePanel();
			});
			self.fieldMngTopContent=$('<div>');
			if(main.account.loggedInUser.export_field_settings){
				self.fieldMngTopContent.append(self.loadSavedButton.html);
			}
			self.fieldMngTopContent
				.append(self.saveFieldOptionsButton.html)
				.append(self.resetButton.html);
			self.mngFieldsPanel=new Panel({
				content:self.exportFieldMngWrap,
				topContent:self.fieldMngTopContent,
				title:'Manage Fields',
				classes:'exportMngFields',
				hideOnOutClickExceptions:['.exportTempPicker'],
				hideOnOutClick:true,
				scrollToTop:true,
				onClose:self.panelClose
			});
		};
		this.openLoadLastTemplatePanel=function(){
			if(!self.loadLastTempPanel){
				var hasTemp1=''; 
				var hasTemp2=''; 
				var hasTemp3=''; 
				var hasTemp4=''; 
				if(main.account.loggedInUser.export_field_settings){
					if(main.account.loggedInUser.export_field_settings.templates['1']){hasTemp1=' fieldMngHasTmp'}
					if(main.account.loggedInUser.export_field_settings.templates['2']){hasTemp2=' fieldMngHasTmp'}
					if(main.account.loggedInUser.export_field_settings.templates['3']){hasTemp3=' fieldMngHasTmp'}
					if(main.account.loggedInUser.export_field_settings.templates['4']){hasTemp4=' fieldMngHasTmp'}
				}
				self.loadLastTempWrap=$('<div class="saveTempWrap">')
					.append(new Button('std','saveTempButton expempField_1','Template 1',[['temp','1']]).html)
					.append(new Button('std','saveTempButton expempField_2','Template 2',[['temp','2']]).html)
					.append(new Button('std','saveTempButton expempField_3','Template 3',[['temp','3']]).html)
					.append(new Button('std','saveTempButton expempField_4','Template 4',[['temp','4']]).html)
				self.loadLastTempPanel=new Panel({
					content:self.loadLastTempWrap,
					title:'Select Template',
					classes:'exportTempPicker',
					hideOnOutClickExceptions:['.luEditor'],
					hideOnOutClick:true,
				});
				self.loadLastTempPanel.html.on('click','.saveTempButton',function(){
					self.currentTemplate=$(this).data('temp');
					if(main.account.loggedInUser.export_field_settings && main.account.loggedInUser.export_field_settings.templates[self.currentTemplate]){
						self.loadSavedCurrentFieldStats(self.currentTemplate);
						self.loadLastTempPanel.close();
					}else{
						alert('This template is not set.');
					}
				});
				self.setTempButtonsForLoad();
			}
			self.loadLastTempPanel.open();
		};
		this.setTempButtonsForLoad=function(){
			if(main.account.loggedInUser.export_field_settings){
				if(!main.account.loggedInUser.export_field_settings.templates['1']){self.loadLastTempPanel.html.find('.expempField_1').addClass('none');
				}else{self.loadLastTempPanel.html.find('.expempField_1').removeClass('none');}
				if(!main.account.loggedInUser.export_field_settings.templates['2']){self.loadLastTempPanel.html.find('.expempField_2').addClass('none');
				}else{self.loadLastTempPanel.html.find('.expempField_2').removeClass('none');}
				if(!main.account.loggedInUser.export_field_settings.templates['3']){self.loadLastTempPanel.html.find('.expempField_3').addClass('none');
				}else{self.loadLastTempPanel.html.find('.expempField_3').removeClass('none');}
				if(!main.account.loggedInUser.export_field_settings.templates['4']){self.loadLastTempPanel.html.find('.expempField_4').addClass('none');
				}else{self.loadLastTempPanel.html.find('.expempField_4').removeClass('none');}
			}
		};
		this.setTempButtonsForSave=function(){
			if(main.account.loggedInUser.export_field_settings){
				if(main.account.loggedInUser.export_field_settings.templates['1']){self.saveTempPanel.html.find('.expempField_1').addClass('fieldMngHasTmp').attr('title','Template Set');
				}else{self.saveTempPanel.html.find('.expempField_1').removeClass('fieldMngHasTmp').removeAttr('title');}
				if(main.account.loggedInUser.export_field_settings.templates['2']){self.saveTempPanel.html.find('.expempField_2').addClass('fieldMngHasTmp').attr('title','Template Set');
				}else{self.saveTempPanel.html.find('.expempField_2').removeClass('fieldMngHasTmp').removeAttr('title');}
				if(main.account.loggedInUser.export_field_settings.templates['3']){self.saveTempPanel.html.find('.expempField_3').addClass('fieldMngHasTmp').attr('title','Template Set');
				}else{self.saveTempPanel.html.find('.expempField_3').removeClass('fieldMngHasTmp').removeAttr('title');}
				if(main.account.loggedInUser.export_field_settings.templates['4']){self.saveTempPanel.html.find('.expempField_4').addClass('fieldMngHasTmp').attr('title','Template Set');
				}else{self.saveTempPanel.html.find('.expempField_4').removeClass('fieldMngHasTmp').removeAttr('title');}
			}
		};
		this.openSaveTemplatePanel=function(){
			if(!self.saveTempPanel){
				self.saveTempWrap=$('<div class="saveTempWrap">')
					.append(new Button('std','saveTempButton expempField_1','Template 1',[['temp','1']]).html)
					.append(new Button('std','saveTempButton expempField_2','Template 2',[['temp','2']]).html)
					.append(new Button('std','saveTempButton expempField_3','Template 3',[['temp','3']]).html)
					.append(new Button('std','saveTempButton expempField_4','Template 4',[['temp','4']]).html)
				self.saveTempPanel=new Panel({
					content:self.saveTempWrap,
					title:'Select Template',
					classes:'exportTempPicker',
					hideOnOutClickExceptions:['.luEditor'],
					hideOnOutClick:true,
				});
				self.saveTempPanel.html.on('click','.saveTempButton',function(){
					self.currentTemplate=$(this).data('temp');
					self.saveFieldOptions(self.currentTemplate);
					self.saveTempPanel.close();
					self.mngFieldsPanel.close();
				});
				self.setTempButtonsForSave();
			}
			self.saveTempPanel.open();
		};
		this.manageFields=function(){
			if(!self.currentLayer){
				alert('Please select a layer.');
				return;
			}
			if(self.mngFieldsPanel){
				self.mngFieldsPanel.open();
			}else{
				self.createMngFieldsPanel();
			}
		};
		this.convertFromPanel=function(){
			self.accordian.setContent(self.shelf.data('name'),self.html);
		};
		this.optionsInPanelClosed=function(){
			self.convertFromPanel();
		};
		this.convertToPanel=function(title){
			title=title || '';
			self.optionsInPanel=new Panel({
				content:self.html,
				title:title,
				classes:'optionsInPanel',
				onClose:self.optionsInPanelClosed
			});
			self.optionsInPanel.open();
		};
		this.checkForOneLayer=function(){
			if(self.layerSelect.find("option[value!='']").length==1){
				self.layerSelect.val(self.layerSelect.find("option[value!='']").val()).change();
			}
		};
		(this.createHTML=function(){
			self.layerSelect=$('<select class="exporterLayerSelect">');
			self.layerSelect.on('change',self.layerSelectChange);
			self.layerSelectUpdate();
			self.limitInput=$('<input type="number" min="1" class="exporterLimitInput">');
			self.exportTypeSelect=$('<select class="exportTypeSelect">');
			self.exportTypeSelect.append('<option value="csv" selected="selected">CSV</option>');
			self.exportTypeSelect.append('<option value="excel">Excel</option>');
			self.exportTypeSelect.append('<option value="shp">Shapefile</option>');
			self.fileNameInput=$('<input type="text" class="exporterFileNameInput">');
			self.exportButton=new Button('std','exporterExportButton','Export');
			self.exportButton.html.on('click',function(){
				self.beginExport();
			});
			self.fieldMngButton=new Button('std','fieldMngButton','Manage Fields');
			self.fieldMngButton.html.on('click',function(){
				self.manageFields();
			});
			self.createMngFieldsPanel();
			self.infoViewMore=new ViewMore({
				prev:'Use this tool to export data with or with out a filter.',
				content:'<ul><li>Step 1: Select an export type.</li><li>Step 2: Select a layer to export.</li><li>Step 3: Optionally, select a file name.</li><li>Step 4: Filter data by applying a filter, both filters by properties and geography apply.</li><li>Step 5: Optionally, choose advanced options, like a field and sort order to sort by and/or a limit to restrict the amount of records that are exported.</li><li>*Be sure your pop up blocker is turned off when exporting a shapefile.</li></ul>'
			});
			self.advTable=$('<table>')
				.append($('<tr>')
					.append($('<td>').append($('<div class="tdLiner">').text('Limit')))
					.append($('<td>').append($('<div class="tdLiner">').append(self.limitInput)))
				);
			if(main.withs.with_ecobens && main.withs.with_ecobens.value){
				self.exporterWithEcobensInput=new Checkbox({
					name:'exporterWithEcobensInput',
					classes:'exporterWithEcobensInput'
				});
				self.advTable.append($('<tr>')
					.append($('<td>').append($('<div class="tdLiner">').text('Export Eco-Benefits')))
					.append($('<td>').append($('<div class="tdLiner">').append(self.exporterWithEcobensInput.html)))
				)
			}
			if(main.withs.with_updater && main.withs.with_updater.value && main.withs.with_updater[main.account.loggedInUser.user_type+'_with']){
				self.exportForUploadInput=new Checkbox({
					name:'exporterLimitInput',
					classes:'exporterLimitInput'
				});
				self.exportForUploadInput.checkbox.on('change',function(){
					if($(this).prop('checked')){
						self.exportTypeSelect.val('excel').attr('disabled',true);
					}else{
						self.exportTypeSelect.attr('disabled',false);
					}
				});
				self.advTable.append($('<tr>')
					.append($('<td>').append($('<div class="tdLiner">').text('Export for Upload')))
					.append($('<td>').append($('<div class="tdLiner">').append(self.exportForUploadInput.html)))
				)
			}
			self.advTable.append($('<tr>')
					.append($('<td>').append($('<div class="tdLiner">').html('&nbsp;')))
					.append($('<td>').append($('<div class="tdLiner">').append(self.fieldMngButton.html)))
				);
			self.advancedViewMore=new ViewMore({
				openHTML:'Advanced',
				content:self.advTable
			});
			self.table=$('<table>')
				.append($('<tr>')
					.append($('<td>').append($('<div class="tdLiner">').text('Export Type*')))
					.append($('<td>').append($('<div class="tdLiner">').append(self.exportTypeSelect)))
				)
				.append($('<tr>')
					.append($('<td>').append($('<div class="tdLiner">').text('Select Layer*')))
					.append($('<td>').append($('<div class="tdLiner">').append(self.layerSelect)))
				)
				.append($('<tr>')
					.append($('<td>').append($('<div class="tdLiner">').text('File Name (No Extension)')))
					.append($('<td>').append($('<div class="tdLiner">').append(self.fileNameInput)))
				)
				.append($('<tr>')
					.append($('<td colspan="2">').append($('<div class="tdLiner">').html(self.advancedViewMore.html)))
				)
				.append($('<tr>')
					.append($('<td colspan="2">').append($('<div class="tdLiner center">').append(self.exportButton.html)))
				)
				.append($('<tr>')
					.append($('<td colspan="2">').append($('<div class="tdLiner postScript">').html('*Required')))
				)
			self.html=$('<div class="exporterWrap">');
			self.html.append(self.infoViewMore.html);
			self.html.append(self.table);
			self.addShelf();
			self.checkForOneLayer();
		})();
	}
});
