define(function(require){
	return function pg(args){
		var Button=require('./Button');
		var Input=require('./Input');
		var Panel=require('./Panel');
		var Tabs=require('./Tabs');
		var Table=require('./Table');
		var self=this;
		this.defaultSortOrder='DESC';
		this.currentSortOrder=this.defaultSortOrder;
		this.defaultSortBy='pid';
		this.defaultField='primary_maintenance';
		this.currentSortBy=this.defaultSortBy;
		this.id=main.utilities.generateUniqueID();
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.layer=main.tables[self.layerName];
		this.inventory=this.layer.inventory;
		this.currentFields=[];
		this.fieldChange=function(val){
			if(val){
				self.fieldsSelect.val(val);
				self.currentFields=[val];
				self.updateItemsTable();
			}else{
				self.fieldsSelect.val('');
				self.currentFields=[];
			}
		};
		this.updateItemsTable=function(){
			var fieldsPkg={};
			for(var i=0;i<self.currentFields.length;i++){
				field=self.inventory.fields[self.currentFields[i]];
				fieldsPkg[field.name]={
					alias:field.alias,
					lookup_table:field.lookup_table,
					options:field.options,
					input_type:field.input_type,
					referencer:field.referencer
				}
			}
			var lookup_table_map='';
			if(self.layer.inventory){
				lookup_table_map=self.layer.inventory.properties.lookup_table_map;
			}
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
						fields:fieldsPkg,
						lookup_table_map:lookup_table_map,
						layer:self.layerName,
						pids:self.pids.join(','),
					},
					action:'getMassUpdateItems'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							var results=response.data;
							self.itemsTHead.html('');
							self.itemsTBody.html('');
							var fields=self.inventory.fields;
							for(var i=0;i<results.length;i++){
								if(i==0){
									var tr=$('<tr>')
										.append('<th>Zoom</th>')
										.append('<th class="tableSortTH tableSortTH_pid" data-sortorder="'+self.currentSortOrder+'" data-field="pid">'+fields.pid.alias+'</th>');
									for(var t=0;t<self.currentFields.length;t++){
										tr.append('<th class="tableSortTH tableSortTH_'+self.currentFields[t]+'" data-sortorder="'+self.currentSortOrder+'" data-field="'+self.currentFields[t]+'">'+fields[self.currentFields[t]].alias+'</th>');
									}
									self.itemsTHead.append(tr);
								}
								self.itemsTBody.append(self.addItemsRow(results[i]));
							}
							self.tableMod=new Table({
								thead:self.itemsTHead,
								tbody:self.itemsTBody
							});
							//update change inputs
							self.newValInput=new Input({
								field:fields[self.currentFields[0]],
								layerName:self.layerName,
								sortBy:'abc'
							});
							self.newValWrap.html(self.newValInput.container);
							if(self.massUpdateMiddle.hasClass('none')){
								self.massUpdateMiddleRightWrap.removeClass('none');
								self.itemsTable.removeClass('none');
								self.massUpdateMiddle.removeClass('none');
								self.panel.positionPanel();
							}
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.updateFilterLegend=function(inv){
			if(main.filter_legend && main.filter_legend.layers[inv]){
				if(main.filter_legend.layerName!=inv){
					main.filter_legend.changeLayer(inv);
				}else{
					main.filter_legend.refreshPoints();
				}
			}
		};
		this.addItemsRow=function(row){
			var tr=$('<tr class="massUpdaterItemRow massUpdaterItemRow_'+row.pid+'" data-pid="'+row.pid+'">')
				.append($('<td>').append($('<div class="tdLiner center">').html('<img src="'+main.mainPath+'images/crosshairs.png" class="massUpdateZoomTo" title="Zoom To"/>')))
				.append($('<td>').append($('<div class="tdLiner massUpdateItemVal sortableCell_pid" data-field="pid" data-val="'+row.pid+'" data-sortval="'+row.pid+'">').html(row.pid)));
			var sortVal;
			for(var i=0;i<self.currentFields.length;i++){
				sortVal=row[self.currentFields[i]+'_alias0'];
				if(typeof sortVal=='string'){sortVal=sortVal.toLowerCase();}
				tr.append($('<td>').append($('<div class="tdLiner massUpdateItemVal sortableCell_'+self.currentFields[i]+'" data-field="'+self.currentFields[i]+'" data-val="'+row[self.currentFields[i]+'_rawval0']+'" data-sortval="'+row[self.currentFields[i]+'_alias0']+'">').html(row[self.currentFields[i]+'_alias0'])));
			}
			return tr;
		};
		this.update=function(){
			var fields=self.inventory.fields;
			var changeFields=[];
			// for(var i=0;i<self.currentFields.length;i++){
				var field=fields[self.currentFields[0]];
				var thiss=self.newValInput.container.find('.formInput').andSelf().filter('.formInput').eq(0);
				var val=main.utilities.getValFromInput(field,thiss);
				changeFields.push({field:field,val:val});
				if(field.lookup_table){
					changeFields=changeFields.concat(main.utilities.addLookUpValsToChangedFields(val,field,fields,self.inventory.properties.lookup_table_map));
				}
			// }
			var count=self.pids.length;
			if(count>1){alias=self.inventory.properties.plural_alias;
			}else{alias=self.inventory.properties.singular_alias;}
			var confirmStr='The following update will be applied to '+count+' '+alias.toLowerCase()+':\n\n';
			for(var i=0;i<changeFields.length;i++){
				confirmStr+='- '+changeFields[i].field.alias+' will be changed to '+main.utilities.getHTMLFromVal(changeFields[i].val,changeFields[i].field,self.inventory.properties.lookup_table_map)+'.\n';
			}
			confirmStr+='\nThis update can not be undone, are you sure you wish to continue?';
			if(confirm(confirmStr)){
				main.dataEditor.updateData({
					pids:self.pids,
					reference:thiss,
					context:self.inventory,
					changeFields:changeFields,
					callback:self.dataUpdated,
					callbackParams:{changeFields:changeFields}
				});
			}
		};
		this.dataUpdated=function(args){
			// main.layout.closeAllEditForms();
			main.layout.refreshPopups();
			self.updateItemsTable();
			var changeFields=args.changeFields;
			if(main.filter_legend && main.filter_legend.active && args.inventory && args.inventory.layerName==main.filter_legend.layerName){main.filter_legend.refresh();}
			if(self.inventory.dataTable){
				for(var i=0;i<self.pids.length;i++){
					for(var t=0;t<changeFields.length;t++){
						self.inventory.dataTable.updateDataTable(self.pids[i],changeFields[t].field.name,args.reference,false,true,true,changeFields[t].val);
					}
				}
			}
		};
		this.updateButtonPressed=function(){
			self.update();
		};
		this.geoFound=function(response){
			main.layout.removeAllPopUps();
			main.map.panTo(response.mainCoords);
		};
		(this.createHTML=function(){
			self.fieldsSelect=$('<select class="massUpdateFieldSelect massUpdateTopEle">')
				.append('<option value=""></option>');
			var fields=self.inventory.fields;
			for(var key in fields){
				if(fields[key].active && fields[key].in_mass_update){
					self.fieldsSelect.append('<option value="'+key+'">'+fields[key].alias+'</option>');
				}
			}
			main.utilities.sortOptions(self.fieldsSelect);
			self.fieldsSelect.on('change',function(){
				self.fieldChange($(this).val());
			});
			self.itemsTBody=$('<tbody>');
			self.itemsTHead=$('<thead>');
			self.itemsTable=$('<table class="massUpdateItemsTable none" border="1">')
				.append(self.itemsTHead)
				.append(self.itemsTBody);
			self.itemsTBody.on('click','.massUpdateZoomTo',function(){
				var pid=$(this).closest('.massUpdaterItemRow').data('pid');
				main.mapItems.unSetActives();
				if(self.inventory){
					main.mapItems.hasActive=Number(pid);
					main.mapItems.hasActiveLayer=self.inventory.layerName;
				}
				self.updateFilterLegend(self.inventory.layerName);
				if(self.layer.properties.internal_field_map && self.layer.properties.internal_field_map.geometry_field){
					main.utilities.getGeo(self.inventory.layerName,self.layer.properties.internal_field_map.geometry_field,pid,self.geoFound);
				}
			});
			self.fieldSelectLabel=$('<div class="massUpdateFieldSelectLabel massUpdateTopEle">').html('Select Field');
			self.massUpdateTopTop=$('<div class="massUpdateTopTop massUpdateEle cf">').html('Mass Update '+self.inventory.properties.plural_alias);
			self.massUpdateTop=$('<div class="massUpdateTop massUpdateEle cf">')
				.append(self.fieldSelectLabel)
				.append(self.fieldsSelect);
			self.massUpdateMiddleLeft=$('<div class="massUpdateMiddleLeft massUpdateMiddleEle">')
				.append(self.itemsTable);
			self.updateButton=new Button('std','massUpdateUpdateButton','Update');
			self.updateButton.html.on('click',function(){
				self.updateButtonPressed();
			});
			self.newValButtons=$('<div class="massUpdatenNwValButtons massUpdateMiddleRightEle">').append(self.updateButton.html);
			self.newValWrap=$('<div class="massUpdateNewValWrap massUpdateMiddleRightEle">');
			self.newValLabel=$('<div class="massUpdateNewValLabel massUpdateMiddleRightEle">').html('Select New Value');
			self.massUpdateMiddleRightWrap=$('<div class="massUpdateMiddleRightWrap none">');
			self.massUpdateMiddleRight=$('<div class="massUpdateMiddleRight massUpdateMiddleEle">')
				.append(self.massUpdateMiddleRightWrap
					.append(self.newValLabel)
					.append(self.newValWrap)
					.append(self.newValButtons)
				);
			self.massUpdateMiddle=$('<div class="massUpdateMiddle none massUpdateEle cf">')
				.append(self.massUpdateMiddleLeft)
				.append(self.massUpdateMiddleRight);
			self.html=$('<div class="massUpdateWrap">')
				.append(self.massUpdateTopTop)
				.append(self.massUpdateTop)
				.append(self.massUpdateMiddle);
			self.panel=new Panel({
				content:self.html,
				title:'Mass Update',
				matchHeight:true,
				classes:'massUpdatePanel',
				centerOnOrientationChange:true,
				// onClose:self.printFormOopened
				// onShow:self.printFormOopened
			});
			if(self.defaultField){
				self.fieldChange(self.defaultField);
			}
		})();
		this.remove=function(){
			self.panel.remove(true);
		};
	};
});