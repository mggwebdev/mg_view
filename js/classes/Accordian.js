define(function(require){
	return function pg(name,sort){
		var self=this;
		var Checkbox=require('./Checkbox');
		if(sort){ this.sort=sort;}
		if(name){ this.id=name;
		}else{this.id=main.utilities.generateUniqueID();}
		this.shelves={};
		this.createContainer=function(){
			return $('<div data-name="'+this.id+'">').addClass('accordian '+this.id);
		};
		this.setContent=function(name,content){
			this.shelves[name].shelfContentWrap.html(content);
			this.shelves[name].content=content;
		};
		this.setOnOpen=function(onOpen){
			self.onOpen=onOpen;
		};
		this.detachShelf=function(shelf){
			shelf.shelf.detach();
		};
		this.removeShelf=function(shelf){
			shelf.shelf.remove();
			delete self.shelves[shelf.name];
		}
		this.removeAllShelves=function(){
			for(var key in self.shelves){
				self.removeShelf(self.shelves[key]);
			}
		};
		this.shelfInAppTest=function(shelf){
			if(shelf.context && shelf.context.withItem){
				if(main.withs[shelf.context.withItem][main.account.loggedInUser.user_type+'_with']){
					shelf.shelf.removeClass('none');
				}else{
					shelf.shelf.addClass('none');
				}
			}
		};
		this.receiveShelf=function(shelf){
			self.shelves[shelf.name]=shelf;
			self.container.append(shelf.shelf)
		};
		this.createShelf=function(args){
			var id=main.utilities.generateUniqueID();
			var classes=args.classes || '';
			var shelfButtonWrap=$('<div class="shelfButtonWrap">').data('name',args.name).append(args.button);
			var shelf=$('<div class="shelf closed '+args.name+' '+classes+'">').data('name',args.name).data('id',id)
				.append(shelfButtonWrap)
				.append(shelfContentWrap);
			if(!args.withoutContent){
				var shelfContentWrap=$('<div class="shelfContentWrap '+args.name+'">').html(args.content);
				shelf.append(shelfContentWrap);
			}else{
				var shelfContentWrap=null;
			}
			var actCheck=null;
			if(args.activateCheck){
				var classes="shelfActCheck";
				if(args.activateCheckClasses){
					classes+=' '+args.activateCheckClasses;
				}
				actCheckCheck=new Checkbox({
					value:'true',
					classes:classes,
					title:'Check To Activate'
				});
				actCheckCheck.checkbox.on('change',function(){
					var name=$(this).closest('.shelf').data('name');
					if($(this).prop('checked')){
						if((!self.shelves[name].opened && !self.shelves[name].opening) || self.shelves[name].closing){
							self.open(name);}
					}else{
						if((self.shelves[name].opened || self.shelves[name].opening) && !self.shelves[name].closing){
						self.close(name);}
					}
				});
				actCheck=$('<div class="actCheckWrap">').append(actCheckCheck.html);
				shelf.prepend(actCheck);
				shelf.addClass('actChkShelf cf');
			}
			if(args.extended){shelf.addClass('extended');}
			var preShelfContent=$('<div class="preShelfContent">');
			if(args.description){
				preShelfContent.html(args.description);
				shelf.prepend(preShelfContent);
			}
			this.shelves[args.name]={
				name:args.name,
				id:id,
				accordian:self,
				shelf:shelf,
				alias:args.alias,
				place_in_order:args.place_in_order,
				context:args.context,
				content:args.content,
				groups:args.groups,
				withoutContent:args.withoutContent,
				dontAppendTo:args.dontAppendTo,
				classes:classes,
				preShelfContent:preShelfContent,
				description:args.description,
				shelfButtonWrap:shelfButtonWrap,
				shelfContentWrap:shelfContentWrap,
				actCheck:actCheck,
				dataTablePresent:false,
				isGroup:args.isGroup,
				onOpen:args.onOpen,
				onClose:args.onClose,
				opened:false,
				opening:false,
				closing:false
			};
			main.layout.shelves[id]=this.shelves[args.name];
			shelfButtonWrap.on('click',function(e){
				var name=$(this).data('name');
				if($(e.target).closest('.buttonExtra').length===0 && self.shelves[name].content && (typeof self.shelves[name].content!='string' || self.shelves[name].content.trim())){
					if(self.shelves[name].uploaderContentWrap && self.shelves[name].uploaderContentWrap.is(':visible')){
						self.close(name,self.shelves[name].uploaderContentWrap);
					}
					if(!self.shelves[name].withoutContent){
						if(self.shelves[name].dataContentWrap && self.shelves[name].dataContentWrap.is(':visible')){
							self.close(name,self.shelves[name].dataContentWrap);
						}
						self.toggle(name,self.shelves[name].shelfContentWrap);
					}else{
						if(self.shelves[name].dataContentWrap){
							self.toggleDataShelf(name);
						}else{
							alert('This requires a log in. Please log in.');
							// main.utilities.openForm('logIn','account',true);
						}
					}
				}
			});
			if(!args.dontAppendTo){
				if(args.appendTo){args.appendTo.append(shelf)}
				else{self.container.append(shelf);}
			}
			if(self.sort){
				self.sortShelves(self.sort)
			}
			return shelf;
		};
		this.sortShelves=function(sortBy){
			var keys=null,itemA,itemB;
			if(sortBy=='abc'){
				keys=Object.keys(self.shelves);
				keys.sort(function(a,b){
					itemA=self.shelves[a].alias;
					itemB=self.shelves[b].alias;
					if(itemA){itemA=itemA.toLowerCase();}
					if(itemB){itemB=itemB.toLowerCase();}
					if(itemA>itemB){return 1;}
					if(itemA<itemB){return -1;}
					return 0;
				});
			}else if(sortBy=='place_in_order'){
				keys=Object.keys(self.shelves),itemA,itemB;
				keys.sort(function(a,b){
					if(self.shelves[a].place_in_order>self.shelves[b].place_in_order){return 1;}
					if(self.shelves[a].place_in_order<self.shelves[b].place_in_order){return -1;}
					return 0;
				});
			}
			if(keys){
				for(var i=0;i<keys.length;i++){
					self.shelves[keys[i]].shelf.appendTo(self.shelves[keys[i]].shelf.closest('.accordian'));
				}
			}
		};
		this.toggleDataShelf=function(name){
			if(self.shelves[name].shelfContentWrap && self.shelves[name].shelfContentWrap.is(':visible')){
				self.close(name,self.shelves[name].shelfContentWrap);
			}
			if(self.shelves[name].uploaderContentWrap && self.shelves[name].uploaderContentWrap.is(':visible')){
				self.close(name,self.shelves[name].uploaderContentWrap);
			}
			var opened=self.toggle(name,self.shelves[name].dataContentWrap);
			// dataTable.updateRanges();
			main.dataTables[name].updateHeaderSizes();
			if(opened){
				if(opened && (!main.dataTables[name].initiallyLoaded  || main.dataTables[name].needsRefresh) && !main.dataTables[name].dontLoadOnOpen){main.dataTables[name].sideReset({scrollToBOnLoad:true});}
				main.dataTables[name].opened();
			}else{
				main.dataTables[name].closed();
			}
		};
		this.createDataShelf=function(dataTable){
			var name=dataTable.table;
			self.shelves[name].dataContentWrap=$('<div data-table="'+name+'">').addClass('dataContentWrap '+name).html(dataTable.html);
			self.shelves[name].shelf.append(self.shelves[name].dataContentWrap);
			dataTable.launchButton.on('click',function(){
				var name=$(this).closest('.shelf').data('name');
				self.toggleDataShelf(name);
			});
			self.shelves[name].dataTablePresent=true;
			dataTable.dataContentWrap=self.shelves[name].dataContentWrap;
			return self.shelves[name].dataContentWrap;
		};
		this.removeDataShelf=function(dataTable){
			var name=dataTable.table;
			self.shelves[name].dataContentWrap.remove();
			dataTable.launchButton.remove();
			self.shelves[name].dataTablePresent=false;
		};
		this.createUploaderShelf=function(uploader){
			var name=uploader.table;
			self.shelves[name].uploaderContentWrap=$('<div>').addClass('uploaderContentWrap '+name).html(uploader.html);
			self.shelves[name].shelf.append(self.shelves[name].uploaderContentWrap);
			uploader.launchButton.on('click',function(){
				var name=$(this).closest('.shelf').data('name');
				if(self.shelves[name].shelfContentWrap && self.shelves[name].shelfContentWrap.is(':visible')){
					self.close(name,self.shelves[name].shelfContentWrap);
				}
				if(self.shelves[name].dataContentWrap && self.shelves[name].dataContentWrap.is(':visible')){
					self.close(name,self.shelves[name].dataContentWrap);
				}
				self.toggle(name,self.shelves[name].uploaderContentWrap);
			});
			self.shelves[name].uploaderPresent=true;
			return self.shelves[name].shelfContentWrap;
		};
		this.closeAllShelves=function(name,target){
			for(var key in self.shelves){
				self.close(key);
			}
		};
		this.toggle=function(name,target){
			if(target.is(':hidden')){
				self.open(name,target);
				return true;
			}else{
				self.close(name,target);
				return false;
			}
		};
		this.close=function(name,target){
			self.shelves[name].closing=true;
			self.shelves[name].shelf.addClass('closing');
			self.shelves[name].opening=false;
			self.shelves[name].shelf.removeClass('opening');
			if(!self.shelves[name].withoutContent){
				if(!target){target=self.shelves[name].shelfContentWrap;}
			}else{
				if(!target){target=self.shelves[name].dataContentWrap;}
			}
			if(target.hasClass('dataContentWrap')){target.removeClass('dtOpen');}
			target.slideUp(main.animations.defaultDuration,function(){
				self.shelves[name].opened=false;
				self.shelves[name].shelf.removeClass('opened');
				self.shelves[name].shelf.addClass('closed');
				self.shelves[name].closing=false;
				self.shelves[name].shelf.removeClass('closing');
				self.extSidePanelTest2();
				if(self.shelves[name].onClose){self.shelves[name].onClose(self.shelves[name]);}
			});
		};
		this.open=function(name,target){
			self.shelves[name].opening=true;
			self.shelves[name].shelf.addClass('opening');
			self.shelves[name].closing=false;
			self.shelves[name].shelf.removeClass('closing');
			if(!self.shelves[name].withoutContent){
				if(!target){target=self.shelves[name].shelfContentWrap;}
			}else{
				if(!target){target=self.shelves[name].dataContentWrap;}
			}
			if(target.hasClass('dataContentWrap')){target.addClass('dtOpen');}
			target.slideDown(main.animations.defaultDuration,function(){
				self.shelves[name].opened=true;
				self.shelves[name].shelf.addClass('opened');
				self.shelves[name].shelf.removeClass('closed');
				self.shelves[name].opening=false;
				self.shelves[name].shelf.removeClass('opening');
				self.extSidePanelTest2();
				if(self.shelves[name].onOpen){self.shelves[name].onOpen(self.shelves[name]);}
			});
		};
		this.extSidePanelTest2=function(){
			var openExtended=false,shelf;
			var sidePanel=self.container.closest('.sidePanel');
			sidePanel.find('.shelf').each(function(){
				shelf=main.layout.shelves[$(this).data('id')];
				if(shelf){
					if((!shelf.opened || shelf.closing) && !shelf.opening){return;}
					if($(this).parents('.shelf.closed:not(.opening),.shelf.closing').length===0){
						if($(this).hasClass('extended') || ($(this).hasClass('dataExtended') && $(this).find('.dataContentWrap').hasClass('dtOpen'))){
							openExtended=true;
							return false;
						}
					}
				}
			});
			if(openExtended){self.container.closest('.sidePanel').addClass('extSidePanel');
			}else{self.container.closest('.sidePanel').removeClass('extSidePanel');}
			main.map.adjustMapSize();
		};
		this.extSidePanelTest=function(target){
			var openExtended=false,accordian,shelf;
			var currentParent=null;
			if(target.parents('.shelf').length==0){
				var topElement=target;
			}else{
				var topElement=target.parents('.shelf').last();
			}
			topElement.find('.shelf').andSelf().each(function(){
				accordian=main.layout.accordians[$(this).closest('.accordian').data('name')];
				shelf=accordian.shelves[$(this).data('name')];
				if(shelf){
					if((!shelf.opened || shelf.closing) && !shelf.opening){return;}
					if($(this).parents('.shelf.closed:not(.opening),.shelf.closing').length===0){
						if($(this).hasClass('extended') || ($(this).hasClass('dataExtended') && $(this).find('.dataContentWrap').hasClass('dtOpen'))){
							openExtended=true;
							return false;
						}
					}
				}
			});
			if(openExtended){
				target.closest('.sidePanel').addClass('extSidePanel');
			}else{
				target.closest('.sidePanel').removeClass('extSidePanel');
			}
			main.map.adjustMapSize();
		};
		this.openAllParents=function(target){
			var t0=target,dt=false;
			if(t0.hasClass('dataContentWrap') || t0.closest('dataContentWrap').length!=0){dt=true;}
			var tgts=target.parents('.shelf').andSelf().filter('.shelf'),length=tgts.length,i=1,name,accordianName,shelfName,targetSCW,name;
			tgts.each(function(){
				accordianName=$(this).closest('.accordian').data('name');
				shelfName=$(this).data('name');
				targetSCW=$(this).find('.shelfContentWrap:first');
				if(dt && i==length){targetSCW=$(this).find('.dataContentWrap:first');}
				if(main.layout.accordians[accordianName].shelves[shelfName]){
					main.layout.accordians[accordianName].open(shelfName,targetSCW);
				}else if(main.layout.accordians['toolBox'] && main.layout.accordians['toolBox'].shelves[shelfName]){
					main.layout.accordians['toolBox'].open(shelfName,targetSCW);
				}
				i++;
			});
		};
		this.container=this.createContainer();
		main.layout.accordians[name]=this;
	};
});