define(function(require){
	return function pg(args){
		var self=this;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.name=this.name || '';
		this.value=this.value || '';
		this.checked=this.checked || false;
		this.label=this.label || '';
		this.classes=this.classes || '';
		this.orientation=this.orientation || 'checkLeft';
		this.id=this.id || '';
		this.data=this.data || [];
		this.disabled=this.disabled || '';
		this.readonly=this.readonly || '';
		this.wrapClasses=this.wrapClasses || '';
		
		this.create=function(){
			this.html=$('<div>').addClass('checkboxWrap cf')
			if(this.wrapClasses){this.html.addClass(this.wrapClasses);}
			this.checkbox=$('<input type="checkbox" name="'+this.name+'" value="'+this.value+'">').addClass('checkbox checkboxEle '+this.classes);
			if(this.checked)this.checkbox.prop('checked',true);
			this.label=$('<div>').addClass('checkboxLabel checkboxEle').html(this.label);
			if(this.orientation=='checkLeft'){
				this.html.append(this.checkbox).append(this.label)
			}else if(this.orientation=='checkRight'){
				this.html.append(this.label).append(this.checkbox)
			}
			if(this.data.length>0){
				for(var i=0;i<this.data.length;i++){
					this.html.data(this.data[i][0],this.data[i][1])
					this.checkbox.data(this.data[i][0],this.data[i][1])
				}
			}
			if(this.disabled){this.checkbox.prop('disabled',true);}
			if(this.readonly){this.checkbox.prop('readonly',true);}
			if(this.title){this.checkbox.attr('title',this.title);}
		};
		this.initEvents=function(){
			this.label.on('click',function(e){
				self.checkbox.click();
			});
		};
		this.create();
		this.initEvents();
	};
});