define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		this.alias='Uploader';
		this.name='uploadertool';
		this.withItem='with_uploadertool';
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.addShelf=function(){
			self.launchButton=new Button('toolBox','',self.alias);
			self.shelf=main.layout.toolBox.createShelf({
				name:self.name,
				button:self.launchButton.html,
				content:self.html,
				context:self,
				alias:self.alias,
				extended:true,
				groups:main.withs[self.withItem].toolbox_groups
			});
			main.layout.toolBox.shelfInAppTest(main.layout.toolBox.shelves[self.name]);
		};
        this.launchUploader = function() {

		    self.clientFolder=window.location.pathname.split("/")[window.location.pathname.split("/").length-2];
            window.open('../uploader/uploader.php?client='+self.clientFolder, 'uploaderWindow');
        };
		(this.createHTML=function(){
		    self.clientFolder=window.location.pathname.split("/")[window.location.pathname.split("/").length-2];
			self.html=$('<div class="uploaderTool">')
			self.openUploaderButton = $('<div>').addClass('button').text('Open Uploader');
			self.openUploaderButton.on('click',function(){
				self.launchUploader();
			});
			self.instructions =$('<div id="uploadertoolinstr">');
			self.html.append(self.instructions);
			self.html.append(self.openUploaderButton);
			self.addShelf();

		})();
	}
});

