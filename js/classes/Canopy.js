define(function(require){
	return function pg(args){
		var self=this;
		var Accordian=require('./Accordian');
		var Button=require('./Button');
		var Checkbox=require('./Checkbox');
 		var ExistingCanopy=require('./ExistingCanopy');
		var InfoTip=require('./InfoTip');
		var Layer2=require('./Layer2');
		var LoaderBox=require('./LoaderBox');
		var MoreInfo=require('./MoreInfo');
		var Panel=require('./Panel');
		var Radio=require('./Radio');
		var Range=require('./Range');
		var Rasters=require('./Rasters');
		var Switch=require('./Switch');
		var Tabs=require('./Tabs');
		// var ToolTip=require('./ToolTip');
		var ViewMore=require('./ViewMore');
		var Wait=require('./Wait');
		var WaitLoad=require('./WaitLoad');
		this.alias='Canopy';
		this.name='canopy';
		this.weightLabelAlias='Site Suitability';
		this.view={fields:{}};
		this.plan={fields:{}};
		this.grow={fields:{}};
		this.planLoaded=false;
		this.growLoaded=false;
		this.defaultOpacity=0.8;
		this.defaultBorderColor=[255,255,255,0.5];
		this.defaultBorderWidth=1;
		this.defaultStdDev=3;
		this.defaultWeightToFixed=3;
		this.defaultRangeMin=0;
		this.defaultRangeMax=50;
		this.defaultWeight=6.66;
		this.defaultWeightMin=0;
		this.defaultWeightMax=9.99;
		this.defaultWeightStep=3.33;
		this.defaultWeightStdDev=3;
		this.defaultWeightColorGrad='Green-Red';
		this.relativeValues=['Low Priority','Medium Priority','High Priority'];
		this.treeSizeRelativeValues=['Small','Medium','Large'];
		this.currentWeightColorGrad=main.colorGrds.options[this.defaultWeightColorGrad];
		this.maxToFixed=0;
		this.toolType=null;
		this.grow.defaultSimType='individual';
		this.grow.simType=this.grow.defaultSimType;
		this.grow.defaultGrowOutType='target';
		this.grow.growOutType=this.grow.defaultGrowOutType;
		this.grow.defaultTargetCanopyPerc=30;
		this.grow.targetCanopyPerc=this.grow.defaultTargetCanopyPerc;
		this.grow.defaultTargetCanopyMax=60;
		this.grow.defaultTargetCanopyMin=10;
		this.grow.defaultIncreaseCanopyPerc=10;
		this.grow.increaseCanopyPerc=this.grow.defaultIncreaseCanopyPerc;
		this.grow.defaultAvgTreeSize=30;
		this.grow.avgTreeSize=this.grow.defaultAvgTreeSize;
		this.grow.defaultAvgTreeSizeMax=40;
		this.grow.defaultAvgTreeSizeMin=20;
		this.grow.defaultIncreaseCanopyMax=60;
		this.grow.defaultIncreaseCanopyMin=0;
		this.grow.defaultGrowOpacity=0.9;
		this.grow.currentReports={};
		this.growing=false;
		this.grow.defaultGrowBorderColor=[0,0,0,0.7];
		this.grow.defaultGrowBorderWidth=3;
		this.grow.reportTitleLimit=16;
		this.ecobenTypes=["Current","Modeled","Net"];
		this.withEcobens=main.personal_settings.app_settings.with_grow_ecobens;
		this.grow.totals={
			existingCanopy:null,
			area_total:null,
			modeledCanopy:null,
			noTreesToReachGoal:null,
			avgTreeSize:null,
			annualEcoBens:null,
			ecoBenefits:{}
		};
		this.grow.currentGrowOut={
			features:{},
			targetCanopy:null,
			increaseBy:null,
			avgTreeSize:null,
			layer:null,
			reportID:null
		};
		this.canopyEcobensCategories={
			'overall':{
				name:'overall',
				alias:'Overall',
				place_in_order:1,
				img:'carbon_storaged.png'
			},
			'air_quality':{
				name:'air_quality',
				alias:'Air Quality',
				place_in_order:5,
				img:'air_quality.png'
			},
			'carbon':{
				name:'carbon',
				alias:'Carbon',
				place_in_order:6,
				img:'carbon_sequestered.png'
			}
		};
		this.canopyEcobensKeys=Object.keys(this.canopyEcobensCategories).sort(function(a,b){
			if(self.canopyEcobensCategories[a].place_in_order>self.canopyEcobensCategories[b].place_in_order){return 1;}
			if(self.canopyEcobensCategories[a].place_in_order>self.canopyEcobensCategories[b].place_in_order){return -1;}
			return 0;
		});
		//g
		this.grow.growKey=71;
		//r
		this.grow.removeKey=82;
		this.omitSPClose=['toolBox'];
		this.active=false;
		this.startedWeighting=false;
		this.loadOnOpenIfNoInvents=true;
		this.initiallyOpened=false;
		this.waitingFor=null;
		this.map=main.map;
		this.layers=main.canopy_layers;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		(function(args){
			self.view.layers={};
			self.plan.layers={};
			self.grow.layers={};
			for(var key in self.layers){
				if(self.layers[key].properties.in_view_tools){self.view.layers[key]=self.layers[key];}
				if(self.layers[key].properties.in_plan_tools){self.plan.layers[key]=self.layers[key];}
				if(self.layers[key].properties.in_grow_tools){self.grow.layers[key]=self.layers[key];}
			}
		})(args);
		this.turnFilterLegendItemOff=function(){
			if(main.filter_legend.otherLayerItems && main.filter_legend.otherLayerItems[self.currentLayer.properties.layer_name]){
				main.filter_legend.otherLayerItems[self.currentLayer.properties.layer_name].radio.radio.prop('checked',false);
			}
		};
		this.turnFilterLegendItemOn=function(){
			if(main.filter_legend.otherLayerItems && main.filter_legend.otherLayerItems[self.currentLayer.properties.layer_name]){
				main.filter_legend.turnOffAllLayers();
				main.filter_legend.otherLayerItems[self.currentLayer.properties.layer_name].radio.radio.prop('checked',true);
			}
		};
		this.activate=function(){
			self.loader.loaderOff();
			if(!self.active){
				if(!self.currentLayer.map_layer || !self.currentLayer.map_layer.layer){
					self.loader.loaderOn();
					self.waitToOpen=new Wait(self.checkForCurrentLayer,self.activate);
					return;
				}
				self.setInitialPrimaries();
				self.update(self.currentLayer.properties.name);
				if(main.filter_legend && main.filter_legend.active){
					self.turnFilterLegendItemOn();
					if(!main.personal_settings.app_settings.canopy_with_invents){
						main.filter_legend.turnOff();
					}else{
						main.filter_legend.convertToWithCanopy();
					}
				}
				self.currentLayer.map_layer.layer.setVisible(true);
				if(main.invents && !main.personal_settings.app_settings.canopy_with_invents){
					for(var key in main.invents.inventories){
						if(main.invents.inventories[key] && main.invents.inventories[key].layer.layer){
							main.invents.inventories[key].layer.layer.olLayer.setVisible(false);
						}
					}
				}
				main.layout.removeAllPopUps();
				main.layout.closeAllEditForms();
				self.initLegend();
				if(self.panel){self.panel.open(true);}
				self.active=true;
				if(self.planLoaded && self.toolType=='plan'){self.tabClick({name:'plan'});
				}else if(self.growLoaded && self.toolType=='grow'){self.tabClick({name:'grow'});}
			}
		};
		this.deActivate=function(args){
			if(self.initialStart){self.tabClick({name:self.initialStart});}
			self.currentLayer.map_layer.layer.setVisible(false);
			if(self.panel){self.panel.close();}
			if(main.filter_legend && !main.filter_legend.active){
				main.filter_legend.turnOn();
			}
			self.turnFilterLegendItemOff();
			if(main.personal_settings.app_settings.canopy_with_invents){
				main.filter_legend.convertFromWithCanopy();
			}
			if(main.invents){
				for(var key in main.invents.inventories){
					if(main.invents.inventories[key] && main.invents.inventories[key].layer.layer){
						main.invents.inventories[key].layer.layer.olLayer.setVisible(true);
					}
				}
			}
			self.active=false;
			self.currentLayer.map_layer.layer.setDefaultStyle();
		};
		this.canopySPOpened=function(args){
			self.initiallyOpened=true;
			self.activate();
		};
		this.canopySPClosed=function(args){
			if((!args.prevContext || self.omitSPClose.indexOf(args.prevContext.name)===-1) && !main.canopy.blockDeactivate){
				self.deActivate();
			}
		};
		(this.preHTMLAdd=function(){
			var buttonAlias=main.withs.with_canopy.alias || self.alias;
			if(main.withs.with_account.icon_path){var buttonImg='images/'+main.withs.with_account.icon_path}
			else{var buttonImg=main.mainPath+'images/canopy.png';}
			main.utilities.addLayoutItems(self.name,buttonAlias,self,buttonImg,true,self.canopySPOpened,false,self.canopySPClosed);
		})();
		this.updateRangePkgs=function(layerName){
			if(self.currentLayer.properties.layer_name==layerName){
				for(var key in self.view.fields){
					if(self.view.fields[key] && self.view.fields[key].range && self.view.fields[key].range.withAvg){
						self.view.fields[key].range.setAvg(self.currentLayer.canopy.stats.fields[key].avg);
					}
				}
			}
		};
		this.createRangePkgHTML=function(range,activateButton){
			var activateButton=activateButton || '',fieldPkgLeft='',noFieldPkgLeft=' noFieldPkgLeft';
			if(activateButton){
				fieldPkgLeft=$('<div class="fieldPkgLeft fieldPkgEle cf">').append(activateButton);
				noFieldPkgLeft='';
			}
			return $('<div class="fieldPkg canopy cf">')
				.append(fieldPkgLeft)
				.append($('<div class="fieldPkgRight fieldPkgEle cf'+noFieldPkgLeft+'">')
					// .append($('<div class="fieldPkgLabel">').html(field.alias))
					// .append(self[tools].fields[fieldName].slider.html)
					.append(range.html)
				);
		};
		this.createRangePkg=function(field,onRangeChange,onRangeMove){
			if(field.input_type=='date'){
				var isTime=false;
				var isDate=true;
				if(field.sub_data_type=='time'){
					var displayFunction=main.utilities.msToTime;
					isDate=false;
					isTime=true;
				}else{var displayFunction=main.utilities.formatDate}
			}else{var displayFunction=null;}
			// var fixed=Math.min(self.maxToFixed,field.to_fixed);
			var fixed=self.maxToFixed;
			var moreInfo=false;
			var moreInfoContent=null;
			var moreInfoTitle=null;
			if(field.more_info_content){
				moreInfo=true;
				moreInfoContent=field.more_info_content;
				if(field.more_info_title){
					moreInfoTitle=field.more_info_title;
				}
			}
			self.view.fields[field.name].range=new Range({
				alias:field.alias,
				classes:'canopyRange fieldPkgRightEle',
				key:field.name,
				to_fixed:fixed,
				prepend_to:field.prepend_to,
				append_to:field.append_to,
				step:field.step,
				min:self.defaultRangeMin,
				max:self.defaultRangeMax,
				withScale:true,
				onChange:onRangeChange,
				onMove:onRangeMove,
				withAvg:true,
				withAvgLbl:'Average',
				displayFunction:displayFunction,
				moreInfo:moreInfo,
				moreInfoContent:moreInfoContent,
				moreInfoTitle:moreInfoTitle,
				isDate:isDate,
				isTime:isTime
			});
			var checked='';
			if(self.currentField.name==field.name){checked=' checked="checked"';}
			self.view.fields[field.name].activateButton=$('<input type="radio" class="canopyActivate '+field.name+'" data-field="'+field.name+'"'+checked+'/>');
			return self.view.fields[field.name].fieldPkg=self.createRangePkgHTML(self.view.fields[field.name].range,self.view.fields[field.name].activateButton);
		};
		this.weightDisplay=function(val){
			var html='';
			if(val==0){html='None';
			}else if(val==3.33){html=self.relativeValues[0];
			}else if(val==6.66){html=self.relativeValues[1];
			}else if(val==9.99){html=self.relativeValues[2];}
			return html;
		};
		this.createSliderPkg=function(field,onRangeChange,onRangeMove){
			var moreInfo=false;
			var moreInfoContent=null;
			var moreInfoTitle=null;
			if(field.more_info_content){
				moreInfo=true;
				moreInfoContent=field.more_info_content;
				if(field.more_info_title){
					moreInfoTitle=field.more_info_title;
				}
			}
			self.plan.fields[field.name].slider=new Range({
				alias:field.alias,
				classes:'canopyRange fieldPkgRightEle weightSlider',
				key:field.name,
				to_fixed:0,
				value:[self.defaultWeight],
				step:self.defaultWeightStep,
				max:self.defaultWeightMax,
				min:self.defaultWeightMin,
				displayFunction:self.weightDisplay,
				onChange:onRangeChange,
				withScale:true,
				discreteScale:true,
				onMove:onRangeMove,
				handlesLength:1,
				round:1,
				moreInfo:moreInfo,
				moreInfoContent:moreInfoContent,
				moreInfoTitle:moreInfoTitle
			});
			// var checked='';
			// if(self.currentField.name==field.name){checked=' checked="checked"';}
			// self[tools].fields[field.name].activateButton=$('<input type="radio" class="canopyActivate '+field.name+'" data-field="'+field.name+'"'+checked+'/>');
			return self.plan.fields[field.name].fieldPkg=$('<div class="fieldPkg canopy cf">')
				/* .append($('<div class="fieldPkgLeft fieldPkgEle cf">')
					.append(self[tools].fields[field.name].activateButton)
				) */.append($('<div class="fieldPkgEle cf">')
					// .append($('<div class="fieldPkgLabel">').html(field.alias))
					.append(self.plan.fields[field.name].slider.html)
					// .append(self[tools].fields[field.name].range.html)
				);
		};
		this.view.rangeChange=function(args,extraParams){
			if(self.currentLayer.map_layer.layer){
				self.currentLayer.canopy.stats.fields[args.key].currentHi=args.currentHi;
				self.currentLayer.canopy.stats.fields[args.key].currentLow=args.currentLow;
				if(!extraParams || !extraParams.dontRefreshMap){
					self.refreshFeatures();
				}
			}
		};
		this.plan.rangeChange=function(args){
			if(self.currentLayer.canopy.plan.fields[args.key]){
				self.currentLayer.canopy.plan.fields[args.key].currentWeight=args.currentLow;
				self.updateWeights();
				self.currentLayer.canopy.plan.weightedScore.weightFilter.currentHi=self.currentLayer.canopy.plan.weightedScore.maxWeightedScore;
				self.currentLayer.canopy.plan.weightedScore.weightFilter.currentLow=self.currentLayer.canopy.plan.weightedScore.minWeightedScore;
				self.plan.weightFilterPkgRange.updateExtremes(self.currentLayer.canopy.plan.weightedScore.weightFilter.currentHi,self.currentLayer.canopy.plan.weightedScore.weightFilter.currentLow,true);
			}
		};
		this.plan.weightFilterChange=function(args){
			self.currentLayer.canopy.plan.weightedScore.weightFilter.currentHi=args.currentHi;
			self.currentLayer.canopy.plan.weightedScore.weightFilter.currentLow=args.currentLow;
			self.refreshFeatures();
		};
		this.grow.targetCanopyRangeChange=function(args){
			self.grow.targetCanopyPerc=args.currentLow;
		};
		this.grow.increaseCanopyRangeChange=function(args){
			self.grow.increaseCanopyPerc=args.currentLow;
		};
		this.grow.avgTreeSizeRangeChange=function(args){
			self.grow.avgTreeSize=args.currentLow;
		};
		this.getWeightedScore = function(props,fields){
			var statsFields=self.currentLayer.canopy.stats.fields,weightedScore=0,value,val0;
			for(var key2 in fields){
				if(self.currentLayer.fields[key2].in_canopy_tools && self.currentLayer.fields[key2].in_plan_tools){
					value=Number(props[key2]);
					if(!self.currentLayer.fields[key2].inverted_canopy_colors){
						val0=value-statsFields[key2].min;
					}else{
						val0=statsFields[key2].max-value;
					}
					weightedScore+=val0*(fields[key2].currentWeight/self.defaultWeightMax)/(statsFields[key2].max-statsFields[key2].min);
				}
			}
			return weightedScore;
		};
		this.filterStyleFunction = function(feature, resolution){
			var visible=true,value,props=feature.getProperties(),currentHi,currentLow;
			if(self.toolType!='plan' || !self.currentLayer.canopy.plan.weightedScore.weightFilter.currentHi==undefined){
				var stats=self.currentLayer.canopy.stats;
				var statsFields=stats.fields;
				for(var key in statsFields){
					value=props[key];
					currentHi=statsFields[key].currentHi,currentLow=statsFields[key].currentLow
					if(value>currentHi || value<currentLow){
						visible=false;
						break;
					}
				}
			}
			if(visible){
				if(self.toolType=='plan'){
					var plan=self.currentLayer.canopy.plan;
					var planFields=plan.fields;
					var cushion=self.defaultWeightStdDev*plan.weightedScore.weightedScoreStdDev;
					var max=Math.min(plan.weightedScore.weightedScoreAvg+cushion,plan.weightedScore.maxWeightedScore),min=Math.max(plan.weightedScore.weightedScoreAvg-cushion,plan.weightedScore.minWeightedScore);
					var weightedScore=self.getWeightedScore(props,planFields);
					if(weightedScore>max){weightedScore=max;}
					if(weightedScore<min){weightedScore=min;}
					if(self.currentLayer.canopy.plan.weightedScore.weightFilter.currentHi!=undefined && (weightedScore>self.currentLayer.canopy.plan.weightedScore.weightFilter.currentHi || weightedScore<self.currentLayer.canopy.plan.weightedScore.weightFilter.currentLow)){
						return [self.getOlStyle('rgba(0,0,0,0)','rgba(0,0,0,0)',0)];
					}
					var color=main.utilities.getColor(max,min,weightedScore,self.currentWeightColorGrad.name);
				}else{
					if(self.toolType=='grow' && self.grow.currentGrowOut.features[props.pid]){
						var styleForCanopy=true;
					}
					var primFieldName=self.currentField.name;
					var field=statsFields[primFieldName];
					var cushion=self.defaultStdDev*field.stdDev;
					var max=Math.min(field.max,field.avg+cushion),min=Math.max(field.min,field.avg-cushion);
					if(styleForCanopy){
						var colorValue=self.grow.currentGrowOut.features[props.pid].hypCanopy;
					}else{
						var colorValue=props[primFieldName];
					}
					if(colorValue>max){colorValue=max;}
					if(colorValue<min){colorValue=min;}
					if(!self.currentField.color_grad){
						var type=self.currentWeightColorGrad.name;
						if(self.currentField.inverted_canopy_colors){type=self.currentWeightColorGrad.inverted;}
						var color=main.utilities.getColor(max,min,colorValue,type);
					}else{
						var color=main.utilities.getColorFromGradient(max,min,colorValue,self.currentField.color_grad);
					}
				}
				if(styleForCanopy){
					color[3]=self.grow.defaultGrowOpacity;
					var borderColor=self.grow.defaultGrowBorderColor;
					var borderWidth=self.grow.defaultGrowBorderWidth;
				}else{
					color[3]=self.defaultOpacity;
					var borderColor=self.defaultBorderColor;
					var borderWidth=self.defaultBorderWidth;
				}
				if(self.currentLayer.map_layer.layer.labels.withLabels){
					var labels=self.currentLayer.map_layer.layer.labels;
					var text=labels.createTextStyle(feature,resolution);
					return [self.getOlStyle(color,'rgba('+borderColor[0]+','+borderColor[1]+','+borderColor[2]+','+borderColor[3]+')',borderWidth,text)];
				}
				else{
					return [self.getOlStyle(color,'rgba('+borderColor[0]+','+borderColor[1]+','+borderColor[2]+','+borderColor[3]+')',borderWidth)];
				}
			}else{
				return [self.getOlStyle('rgba(0,0,0,0)','rgba(0,0,0,0)',0)];
			}
		};
		this.getOlStyle=function(color, strokeColor, strokeWidth,text) {
			if(text){
				return new ol.style.Style({
					fill: new ol.style.Fill({
						color: color
					}),
					stroke: new ol.style.Stroke({
						color: strokeColor,
						width: strokeWidth
					}),
					text:text
				});
			}
			else{
				return new ol.style.Style({
					fill: new ol.style.Fill({
						color: color
					}),
					stroke: new ol.style.Stroke({
						color: strokeColor,
						width: strokeWidth
					})
				});
			}
		};
		this.turnLayerOff=function(){
			if(self.currentLayer.map_layer.layer){self.currentLayer.map_layer.layer.setVisible(false);}
			if(main.filter_legend){
				self.turnFilterLegendItemOff();
			}
		};
		this.turnLayerOn=function(){
			if(main.filter_legend){
				self.turnFilterLegendItemOn()
			}
			self.currentLayer.map_layer.layer.setVisible(true);
		};
		this.goLayer=function(layerName){
			self.currentLayer.map_layer.layer.setDefaultStyle();
			self.turnLayerOn();
			if(!main.withs.with_canopy_view || main.withs.with_canopy_view.value){self.updateViewTools();}
			if(!main.withs.with_canopy_plan || main.withs.with_canopy_plan.value){self.updatePlanTools();}
			self.update(layerName);
			if(self.toolType=='view'){
			}else if(self.toolType=='plan'){
				if(!self.startedWeighting){self.startWeighting();}
			}else if(self.toolType=='grow'){
				self.initGrow();
			}
			self.initLegend();
		};
		this.setAllSelectLayers=function(val){
			if(self.view.selectLayer){self.view.selectLayer.val(val);}
			if(self.plan.selectLayer){self.plan.selectLayer.val(val);}
			if(self.grow.selectLayer){self.grow.selectLayer.val(val);}
		};
		this.newLayerGo=function(val){
			self.setAllSelectLayers(val);
			self.turnLayerOff();
			self.setCurrentLayer(self.layers[val]);
			if(self.currentLayer.map_layer.layer){
				self.goLayer(val);
			}else{
				alert('This layer is still loading, please wait while the layer loads.');
				self.waitingFor=self.currentLayer.properties.layer_name;
				return false;
			}
			return true;
		};
		this.changeCurrentLayer=function(){
			self.newLayerGo($(this).val());
		};
		this.resetView=function(){
			self.view.tools.find('.canopyActivate').prop('checked',false);
			self.setCurrentField(self.primaryField.name);
			self.view.fields[self.primaryField.name].fieldPkg.find('.canopyActivate').prop('checked',true);
			self.updateSymbology();
			for(var key in self.view.fields){
				self.view.fields[key].range.reset();
			}
		};
		this.resetPlan=function(){
			for(var key in self.plan.fields){
				if(self.plan.fields[key] && self.plan.fields[key].slider){self.plan.fields[key].slider.reset();}
			}
			if(self.plan.weightFilterPkgRange){self.plan.weightFilterPkgRange.reset();}
			self.updateSymbology();
		};
		this.createViewHTML=function(){
			self.view.viewMore=new ViewMore({
				prev:'Use the slider bars below to make maps of existing Urban Tree Canopy and/or Possible Planting Areas.',
				content:'<ul><li>Step 1: Select the geographic scale for your map.</li><li>Step 2: Use the sliders bar to filter values.</li><li>Step 3: Use the tool menus to change color, add labels and print or export your map.</li></ul><p>Tip! - - check both slider bars to filter areas by tree canopy AND planting area.</p>'
			});
			self.view.resetButton=new Button('std','canopyViewResetButton','Reset');
			self.view.resetButton.html.on('click',function(){
				self.resetView();
			});
			self.view.desc=$('<div class="viewDesc canopyDesc canopyElesWrap">')
				.append($('<div class="viewDescTop viewDescEle canopyDescEle cf">')
					.append($('<div class="viewDescTopEle viewDescLegend canopyDescLegend canopyDescTopEle">')
						.append(self.view.viewMore.html)
					)
				)
				.append($('<div class="canopyDescButtons">')
					.append(self.view.resetButton.html)
				);
			self.view.selectLayer=$('<select class="viewSelectLayer canopySelectLayer">');
			var selected;
			if(self.layers){
				for(var key in self.layers){
					if(self.layers[key].properties.in_view_tools){
						selected="";
						if(self.layers[key].properties.primary_layer){
							selected=' selected="selected"';
						}
						self.view.selectLayer.append('<option value="'+key+'"'+selected+'>'+self.layers[key].properties.alias+'</option>');
					}
				}
			}
			self.view.selectLayer.on('change',self.changeCurrentLayer);
			self.view.toolsWrap=$('<div class="viewToolsWrap toolsWrap">');
			self.view.tools=$('<div class="viewTools canopyTools">');
			self.updateViewTools();
			self.view.hidecheckBox=new Checkbox({
				label:'Disable Info Tip',
				classes:'disableInfoTipCheck'
			});
			self.view.hidecheckBoxWrap=$('<div class="viewToolsOptions canopyOption">')
				.append(self.view.hidecheckBox.html);
			self.view.hidecheckBox.checkbox.on('change',function(){
				self.hideInfoTipChange($(this));
			});
			self.view.canopyOptionsTop=$('<div class="canopyOptionsTop">').html('Options');
			self.view.canopyOptions=$('<div class="viewToolsOptions canopyOptions">')
				.append(self.view.canopyOptionsTop)
				.append(self.view.hidecheckBoxWrap);
			return self.view.html=$('<div class="viewWrap canopyTabContent">')
				.append(self.view.desc)
				.append(self.view.selectLayer)
				.append(self.view.toolsWrap
					// .append(self.view.weightFieldPkg)
					.append(self.view.tools)
				)
				.append(self.view.canopyOptions);
		};
		this.weightFilterPkgDisplay=function(val){
			return val;
		};
		this.hideInfoTipChange=function(thiss){
			if(!main.withs.with_canopy_plan || main.withs.with_canopy_plan.value){
				self.plan.hidecheckBox.checkbox.prop('checked',thiss.prop('checked'));
			}
		
			self.view.hidecheckBox.checkbox.prop('checked',thiss.prop('checked'));
		
			if(!thiss.prop('checked')){
				self.infoTip.activate();
			}else{self.infoTip.deActivate();}
		};
		this.createPlanHTML=function(){
			self.plan.viewMore=new ViewMore({
				prev:'Use the slider bars below to weight your priorities.',
				content:'<ul><li>Step 1: Select the geographic scale for your map.</li><li>Step 2: Use the sliders bars to set weights for selected criteria.</li><li>Step 3: Filter map to show priority areas determined by '+self.weightLabelAlias.toLowerCase()+'.</li><li>Step 4: Change the colors of the areas in your map and print or export the map data.</li></ul>'
			});
			self.plan.resetButton=new Button('std','canopyPlanResetButton','Reset');
			self.plan.resetButton.html.on('click',function(){
				self.resetPlan();
			});
			self.plan.desc=$('<div class="planDesc canopyDesc canopyElesWrap">')
				.append($('<div class="planDescTop planDescEle canopyDescEle cf">')
					.append($('<div class="planDescTopEle planDescLegend canopyDescLegend canopyDescTopEle">')
						.append(self.plan.viewMore.html)
					)
				)
				.append($('<div class="canopyDescButtons">')
					.append(self.plan.resetButton.html)
				);
			self.plan.weightFilterWrap=$('<div class="weightFilterWrap">');
			self.plan.selectLayer=$('<select class="planSelectLayer canopySelectLayer">');
			var selected;
			if(self.layers){
				for(var key in self.layers){
					if(self.layers[key].properties.in_plan_tools){
						selected="";
						if(self.layers[key].properties.primary_layer){
							selected=' selected="selected"';
						}
						self.plan.selectLayer.append('<option value="'+key+'"'+selected+'>'+self.layers[key].properties.alias+'</option>');
					}
				}
			}
			self.plan.selectLayer.on('change',self.changeCurrentLayer);
			self.plan.toolsWrap=$('<div class="planToolsWrap toolsWrap">');
			// self.view.weightActivateButton=$('<input type="radio" class="canopyWeightActivate weightActivateButton" data-type="weighting"/>');
			// self.view.weightFieldPkg=$('<div class="fieldPkg canopy weightActivateWrap cf">')
				// .append($('<div class="fieldPkgLeft fieldPkgEle cf">')
					// .append(self.view.weightActivateButton)
				// ).append($('<div class="fieldPkgRight fieldPkgEle cf">')
					// .append($('<div class="fieldPkgLabel">').html('Weighting'))
				// );
			self.plan.tools=$('<div class="planTools canopyTools">');
			self.updatePlanTools();
			self.plan.hidecheckBox=new Checkbox({
				label:'Disable Info Tip',
				classes:'disableInfoTipCheck'
			});
			self.plan.hidecheckBoxWrap=$('<div class="planToolsOptions canopyOption">')
				.append(self.plan.hidecheckBox.html);
			self.plan.hidecheckBox.checkbox.on('change',function(){
				self.hideInfoTipChange($(this));
			});
			self.plan.canopyOptionsTop=$('<div class="canopyOptionsTop">').html('Options');
			self.plan.canopyOptions=$('<div class="planToolsOptions canopyOptions">')
				.append(self.plan.canopyOptionsTop)
				.append(self.plan.hidecheckBoxWrap);
			// self.view.weightActivateButton.on('change',function(){
				// self.startWeighting();
			// });
			// self.view.tools.find('.canopyActivate').on('change',function(){
				// self.view.tools.find('.canopyActivate').not($(this)).add(self.view.weightActivateButton).prop('checked',false);
				// self.currentField=self.currentLayer.fields[$(this).data('field')];
				// self.stopWeighting();
				// self.updateSymbology();
			// });
			return self.plan.html=$('<div class="planWrap canopyTabContent">')
				.append(self.plan.desc)
				.append(self.plan.selectLayer)
				.append(self.plan.toolsWrap
					// .append(self.plan.weightFieldPkg)
					.append(self.plan.tools)
				)
				.append(self.plan.weightFilterWrap)
				.append(self.plan.canopyOptions);
		};
		this.turnGrowOn=function(){
			self.initIndividualGrowOut();
		};
		this.turnGrowOff=function(){
			self.deInitIndividualGrowOut();
		};
		this.getTableRow=function(row){
			var area_name_or_id=row.area_name_or_id || '';
			return $('<tr class="growReportTR growReportTR_'+row.pid+'" data-pid="'+row.pid+'">')
				.append($('<td>').append($('<div class="tdLiner">').html('<input type="checkbox" class="growReportCheckBox"/>')))
				.append($('<td>').append($('<div class="tdLiner growReportCell area_name_or_id">').html(area_name_or_id)))
				.append($('<td>').append($('<div class="tdLiner growReportCell existing_canopy_perc">').html(main.utilities.addCommas(row.existing_canopy_perc.toFixed(0))+'%')))
				.append($('<td>').append($('<div class="tdLiner growReportCell modeled_canopy_perc">').html(main.utilities.addCommas(row.modeled_canopy_perc.toFixed(0))+'%')))
				.append($('<td>').append($('<div class="tdLiner growReportCell no_trees_to_reach_goal">').html(main.utilities.addCommas(Math.round(row.no_trees_to_reach_goal)))))
				.append($('<td>').append($('<div class="tdLiner growReportCell tree_size_used">').html(main.utilities.addCommas((row.tree_size_used/0.3048).toFixed(0)))))
				.append($('<td>').append($('<div class="tdLiner growReportCell annual_eco_benefits">').html(main.utilities.addCommas(row.annual_eco_benefits.toFixed(0)))))
				.append($('<td>').append($('<div class="tdLiner">').html(new Button('std','canopyScenRestoreButton','Restore').html)))
		};
		this.addGrowReportRow=function(row){
			var row0={};
			row0.annual_eco_benefits=Number(row.annual_eco_benefits);
			row0.area_total=Number(row.area_total);
			row0.existing_canopy_perc=Number(row.existing_canopy_perc);
			row0.modeled_canopy_perc=Number(row.modeled_canopy_perc);
			row0.no_trees_to_reach_goal=Number(row.no_trees_to_reach_goal);
			row0.pid=Number(row.pid);
			row0.tree_size_used=Number(row.tree_size_used);
			row0.area_name_or_id=row.area_name_or_id;
			row0.features=row.features || null;
			row0.layer=row.layer;
			self.grow.detailedGrowReport.reports[row.pid]=row0;
			return self.getTableRow(row0);
		};
		this.updateGrowCharts=function(){
			
			if(main.canopy.grow.totals.existingCanopy){
				var existingPercent=main.canopy.grow.totals.existingCanopy;
				self.grow.growCharts.Info.addClass('none');
			}
			else{
				var existingPercent=0;
			}
			if(main.canopy.grow.totals.modeledCanopy){
				var modeledPercent=main.canopy.grow.totals.modeledCanopy;
			}
			else{
				var modeledPercent=0;
			}
			var existingAcre=main.canopy.grow.totals.area_total*existingPercent/100;
			var modeledAcre=main.canopy.grow.totals.area_total*modeledPercent/100;
			
			updateGrowChart(existingPercent,modeledPercent,"growChartPercentChart",true);
			updateGrowChart(existingAcre,modeledAcre,"growChartAcreChart",false);
			updateGrowChartTrees();
		};
		updateGrowChartTrees=function(){
			var inverse=false;
			var color="#004100";
			if(main.canopy.grow.totals.noTreesToReachGoal){
				if(main.canopy.grow.totals.noTreesToReachGoal>=0){
					notrees=main.canopy.grow.totals.noTreesToReachGoal;
				}
				else{
					notrees=main.canopy.grow.totals.noTreesToReachGoal* -1;
					inverse=true;
				}
			}
			else{
				notrees=0;
			}
			if(inverse){
				color="#FF0000";
			}
			var name='growChartTreesChart';
			 if(notrees!=0){
				 $("#"+name).html('');
				$.jqplot(name,[[notrees]],{
						   
						   seriesDefaults: {
							   renderer: $.jqplot.MeterGaugeRenderer,
							   rendererOptions: {
								   label: 'Trees',
								   ringColor:color,
								   tickColor:color
							   }
						   }
					   }).replot();
			}
			else{
				$("#"+name).html('Choose a model to see the number of trees');
			}
			$("#growChartTreesChartNum").html(main.utilities.addCommas(notrees.toFixed(0))+" Trees");
					
		};
		
		updateGrowChart=function(existing,modeled,name,percent){
			var existingChart=existing;
			newCanopy=modeled-existing;
			var max=100;
			
			var existingColor="#418045";
			var newColor="#004100";
			var inverse=false;
			if(newCanopy<0){newCanopy=newCanopy*-1;existingChart=existing-newCanopy;var inverse=true;}
			if(inverse){newColor="#FF0000";}
			if(!percent){
				if(inverse){max=existing+(existing*.1);}
				else{max=modeled+(modeled*.1);}
			}
			$.jqplot(name, [[[existingChart,0]],[[newCanopy,0]]],{
				// The "seriesDefaults" option is an options object that will
				// be applied to all series in the chart.
				stackSeries: true,
				seriesColors: [existingColor ,newColor ],
				seriesDefaults:{
					renderer:$.jqplot.BarRenderer,
					shadowAngle: 135,
					rendererOptions: {
						barDirection: 'horizontal',
						highlightMouseDown: true   
					},
					//pointLabels: {show: true, formatString: '%d'}
				},
				legend: {
					show: false,
					location: 's',
					placement: 'outside'
				},
				axes: {
					yaxis: {
						renderer: $.jqplot.CategoryAxisRenderer,
						showTicks:false
					},
                     xaxis: {
						//renderer: $.jqplot.CategoryAxisRenderer
						showTicks:false,
						max:max
					} 
				}
				}).replot();
				$("#"+name+"Current").html("Current: "+main.utilities.addCommas(existing.toFixed(0)));
				$("#"+name+"Model").html("Modeled: "+main.utilities.addCommas(modeled.toFixed(0)));
				
		};
		this.updateEcoBens=function(){
			
			var ecobens=main.data.std_settings.ecobens;
			var ecobensKeys=Object.keys(ecobens).sort(function(a,b){
				if(ecobens[a].place_in_order>ecobens[b].place_in_order){return 1;}
				if(ecobens[a].place_in_order<ecobens[b].place_in_order){return -1;}
				return 0;
			});
			
			for(var i=0; i<ecobensKeys.length;i++){
				var key = ecobensKeys[i];
				if(main.data.std_settings.ecobens[key].canopy_ben){
					for (var j=0;j<self.ecobenTypes.length;j++){
						var type=self.ecobenTypes[j];
						if(self.ecobensItems[key]){
							var perAcreVal=0;
							if(self.grow &&self.grow.totals&&self.grow.totals.ecoBenefits&&self.grow.totals.ecoBenefits.OverallBenefits){
								self.grow.growBens.Info.addClass('none');
								switch(key){
									case "ecoben_overall_bens_dol": perAcreVal=self.grow.totals.ecoBenefits.OverallBenefits.DollarValue; break;
									case "ecoben_aq_lbs": perAcreVal=self.grow.totals.ecoBenefits.AirQualityBenefits.TotalLbs; break;
									case "ecoben_aq_dol": perAcreVal=self.grow.totals.ecoBenefits.AirQualityBenefits.TotalDollarValue; break;
									//case "ecoben_carb_avoid": perAcreVal=self.grow.totals.ecoBenefits.CO2Benefits.Avoided; break;
									case "ecoben_carb_dols": perAcreVal=self.grow.totals.ecoBenefits.CO2Benefits.DollarValue; break;
									case "ecoben_carb_sequ": perAcreVal=self.grow.totals.ecoBenefits.CO2Benefits.Sequestered; break;
									//case "ecoben_carb_stored": perAcreVal=self.grow.totals.ecoBenefits.CO2Benefits.TotalCO2; break;
								}
							}
							perAcreVal=Number(perAcreVal);
							var numAcres=0;
							var netNegative=false;
							if(self.grow &&self.grow.totals&&self.grow.totals.ecoBenefits&&self.grow.totals.ecoBenefits.OverallBenefits){
								switch(type){
									case "Current":numAcres=self.grow.totals.existingCanopy/100 * self.grow.totals.area_total;break;
									case "Modeled":numAcres=self.grow.totals.modeledCanopy/100 * self.grow.totals.area_total;break;
									case "Net":numAcres=(self.grow.totals.modeledCanopy/100 * self.grow.totals.area_total)-(self.grow.totals.existingCanopy/100 * self.grow.totals.area_total);
												if(numAcres<0){numAcres=numAcres*-1;netNegative=true;}break;
								}
							}
							prepend_to='',append_to='';
							if(netNegative){
								prepend_to="-";
							}
							if(ecobens[key].prepend_to){prepend_to+=ecobens[key].prepend_to;}
							if(ecobens[key].append_to){append_to=ecobens[key].append_to;}
							val=numAcres*perAcreVal;
							if(val<5){val=val.toFixed(2);
							}else{val=val.toFixed(0);}
							self.ecobensItems[key][type].subContentContent.html('');
							self.ecobensItems[key][type].subContentContent.html(prepend_to+main.utilities.addCommas(val)+append_to);
						}
					}
				}
			}
		};
		this.updateGrowTable=function(){
			self.grow.detailedGrowReport.reports={};
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]
					}, 
					action:'getGrowTableData'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							self.grow.detailedGrowReport.reportTBody.html('');
							var results=response.results;
							self.grow.currentReports={};
							for(var i=0;i<results.length;i++){
								self.grow.currentReports[results[i].pid]=results[i];
								self.grow.detailedGrowReport.reportTBody.append(self.addGrowReportRow(results[i]));
							}
							self.testDGRLength();
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.grow.removeGrowReport=function(pid){
			self.grow.detailedGrowReport.reportTBody.find('.growReportTR[data-pid="'+pid+'"]').remove();
			delete self.grow.detailedGrowReport.reports[pid];
			self.testDGRLength();
		};
		this.deleteGrowReports=function(){
			var pids=[];
			self.grow.detailedGrowReport.reportTBody.find('.growReportCheckBox:checked').each(function(){
				pids.push($(this).closest('.growReportTR').data('pid'));
			});
			if(pids.length){
				if(!confirm("Are you sure you want to delete?")){return;}
				$.ajax({
					type:'POST',
					url:main.mainPath+'server/db.php',
					data:{
						params:{
							folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
							pids:pids.join(',')
						},
						action:'deleteGrowReports'
					},
					success:function(response){
						if(response){
							response=JSON.parse(response);
							if(response.status=="OK"){
								var pids=response.params.pids.split(',');
								for(var t=0;t<pids.length;t++){
									self.grow.removeGrowReport(pids[t]);
									if(self.grow.currentGrowOut.reportID==pids[t]){
										self.grow.currentGrowOut.reportID=null;
									}
								}
							}else{
								alert('Error');
								console.log(response);
							}
						}
					}
				});
			}else{alert('Please select scenarios to delete.');}
		};
		this.createScenReportRow=function(feature){
			var netChange=(Number(feature.hypCanopy)-Number(feature.properties[self.grow.currentCanopyField.name])).toFixed(0);
			var ecobens=(self.grow.modelEcoVals.OverallBenefits.DollarValue*Number(feature.treesNeeded)).toFixed(0);
			if(self.currentLayer.properties.internal_field_map.alias){
				var aliasField=self.currentLayer.properties.internal_field_map.alias;
			}
			else{
				var aliasField="pid";
			}
			return $('<tr class="scenReportRow" data-pid="'+feature.pid+'">')
				.append( $('<td>').append($('<div class="tdLiner">').html(feature.properties[aliasField])))
				.append($('<td>').append($('<div class="tdLiner">').html(main.utilities.addCommas(feature.properties[self.grow.currentCanopyField.name].toFixed(0)))))
				.append($('<td>').append($('<div class="tdLiner">').html(main.utilities.addCommas(feature.hypCanopy.toFixed(0)))))
				.append($('<td>').append($('<div class="tdLiner">').html(main.utilities.addCommas(netChange))))
				.append($('<td>').append($('<div class="tdLiner">').html(main.utilities.addCommas(feature.treesNeeded.toFixed(0)))))
				.append($('<td>').append($('<div class="tdLiner">').html(main.utilities.addCommas((feature.avgTreeSizeMeters/0.3048).toFixed(0)))))
				.append($('<td>').append($('<div class="tdLiner">').html(main.utilities.addCommas(ecobens))))
		};
		this.updateReportScenTBody=function(){
			self.grow.reportScenTBody.html('');
			var currentFeatures=self.grow.currentGrowOut.features;
			for(var key in currentFeatures){
				self.grow.reportScenTBody.append(self.createScenReportRow(currentFeatures[key]));
			}
			if(self.grow.reportScenTBody.find('.scenReportRow').length){self.grow.reportScenWrap.removeClass('none');
			}else{self.grow.reportScenWrap.addClass('none');}
		};
		this.resetGrow=function(){
			self.clearScenario();
			self.updateGrowTable();
		};
		this.clearScenario=function(){
			self.resetGrowOutScenario();
			self.grow.reportScenTBody.html('');
			self.grow.reportScenWrap.addClass('none');
			self.grow.reportScenUpdateButton.html.addClass('none');
			self.grow.reportScenTableTitle.val('');
		};
		this.restoreScenario=function(response,params){
			var pid=params.pid;
			self.resetGrowOutScenario();
			var scenario=self.grow.currentReports[pid];
			var scenFeatures=scenario.features;
			self.grow.currentGrowOut.reportID=pid;
			if(scenFeatures){
				self.newLayerGo(scenario.layer);
				var features=self.currentLayer.map_layer.layer.olLayer.getSource().getFeatures();
				var scenFeature;
				for(var i=0;i<features.length;i++){
					if(scenFeatures[features[i].get('pid')]){
						scenFeature=scenFeatures[features[i].get('pid')];
						
						var props=features[i].getProperties();
						var area=props.area_acre;
						self.grow.currentGrowOut.features[props.pid]={
							hypCanopy:scenFeature.hypCanopy,
							treesNeeded:self.getTreesNeeded(area,scenFeature.hypCanopy,props[self.grow.currentCanopyField.name],scenFeature.avgTreeSize),
							area:area,
							avgTreeSizeMeters:scenFeature.avgTreeSize*0.3048,
							pid:props.pid,
							properties:props,
							feature:features[i]
						};
						scenFeatures[features[i].get('pid')].properties=features[i].getProperties();
					}
				}
				self.updateReportScenTBody();
				self.updateTotals(self.setUpdateButton);
				if(scenario.area_name_or_id){self.grow.reportScenTableTitle.val(scenario.area_name_or_id);}
			}
			main.layout.loader.loaderOff();
		};
		this.setUpdateButton=function(){
			if(self.grow.currentGrowOut.reportID){
				self.grow.reportScenUpdateButton.html.removeClass('none');
			}
		};
		this.printCurrentScenario=function(){
				var features=self.grow.currentGrowOut.features;
				var featurePkg=[],feature,netChange,ecobens;
				if(self.currentLayer.properties.internal_field_map.alias){
					var aliasField=self.currentLayer.properties.internal_field_map.alias;
				}
				else{
					var aliasField="pid";
				}
				for(var key in features){
					feature=features[key];
					netChange=(Number(feature.hypCanopy)-Number(feature.properties[self.grow.currentCanopyField.name])).toFixed(0);
					ecobens=(self.grow.modelEcoVals.OverallBenefits.DollarValue*Number(feature.treesNeeded)).toFixed(0);
					featurePkg.push({
						id:feature.properties[aliasField],
						currentCanopy:feature.properties[self.grow.currentCanopyField.name].toFixed(0),
						hypCanopy:feature.hypCanopy,
						netChange:netChange,
						noTreesToReachGoal:main.utilities.addCommas(feature.treesNeeded.toFixed(0)),
						avgTreeSize:main.utilities.addCommas(feature.avgTreeSizeMeters/0.3048),
						ecobens:main.utilities.addCommas(ecobens)
					});
				}
				var form=$('<form>',{
					action:main.mainPath+'server/printCanopyScenario.php',
					method:'POST',
					target:'_blank'
				});
				var netChange=self.grow.totals.modeledCanopy-self.grow.totals.existingCanopy;
				if(netChange==0){netChange=0;}
				if(self.grow.reportScenTableTitle.val().trim()){
					form.append($('<input type="hidden" name="title"/>').val(self.grow.reportScenTableTitle.val().trim()));
				}
				form.append($('<input type="hidden" name="folder"/>').val(window.location.pathname.split("/")[window.location.pathname.split("/").length-2]));
				form.append($('<input type="hidden" name="features"/>').val(JSON.stringify(featurePkg)));
				form.append($('<input type="hidden" name="layerAlias"/>').val(self.currentLayer.properties.alias));
				form.append($('<input type="hidden" name="modeledCanopy"/>').val(self.grow.totals.modeledCanopy.toFixed(0)));
				form.append($('<input type="hidden" name="noTreesToReachGoal"/>').val(main.utilities.addCommas(self.grow.totals.noTreesToReachGoal.toFixed(0))));
				form.append($('<input type="hidden" name="avgTreeSize"/>').val((self.grow.totals.avgTreeSize/0.3048).toFixed(0)));
				form.append($('<input type="hidden" name="netChangeCanopy"/>').val(netChange.toFixed(0)));
				form.append($('<input type="hidden" name="existingCanopy"/>').val(main.utilities.addCommas(self.grow.totals.existingCanopy.toFixed(0))));
				form.append($('<input type="hidden" name="area_total"/>').val(main.utilities.addCommas((self.grow.totals.area_total*0.000000386102159).toFixed(0))));
				form.append($('<input type="hidden" name="annualEcoBens"/>').val(main.utilities.addCommas(self.grow.totals.annualEcoBens.toFixed(0))));
				form.append('<input type="hidden" name="action" value="printCanopyScenario"/>');
				// $('body').append(form);
				form.get(0).submit();
				// form.remove();
		};
		this.testDGRLength=function(){
			if(self.grow.detailedGrowReport.reportTBody.find('.growReportTR').length){
				self.grow.detailedGrowReport.reportTableWrapWrap.removeClass('none');
			}else{self.grow.detailedGrowReport.reportTableWrapWrap.addClass('none');}
		};
		this.createGrowHTML=function(){
			self.grow.resetButton=new Button('std','canopyGrowResetButton','Reset');
			self.grow.resetButton.html.on('click',function(){
				self.resetGrowOutScenario();
			});
			self.grow.viewMore=new ViewMore({
				prev:'<p>The <b>Grow</b> tool is designed to help evaluate potential urban tree canopy goals and the number of trees to reach the goal. With just two inputs - canopy % and average tree size at maturity - the tool outputs how many trees are needed, what the impact is on canopy cover regionally, and the impact on urban forest ecosystem services. <b>Grow</b> can be used incrementally, e.g. add 10% canopy to one or more areas using a medium size stature tree, and then add 5% canopy to other areas using a large stature average tree size, then evaluate the results.</p><p><b>Grow</b> is meant to be a simple tool to assist communities in developing tree planting and canopy goals. To keep it simple, factors such as mortality, natural regeneration, and the impacts from land development have not been taken into account.</p>',
				content:'<p>Instructions for the <b>Grow</b> tool:</p><ol><li>Choose a geographic layer from the drop-down</li><li>Select a <b>Grow</b>-out method from the radio dials (a target or a % to increase by)</li><li>Set your assumptions for the % canopy and average size tree crown to model</li><li>Turn “on” the paint canopy goals tool</li><li>Hold down the “g” key (for <b><u>G</u></b>row) then click polygons in the map</li><li>Review your Modeled Tree Canopy, Modeled Ecosystem Benefits, and Detailed Grow Report </li><li>Save your current scenario within the Detailed Grow Report</li><li>Reset, load a saved scenario, or change assumptions to iteratively <b>Grow</b> more areas</li></ol>',
				hidePrevOnShow:true,
				viewPrevHTML:'Open Grow Introduction'
			});
			self.grow.desc=$('<div class="growDesc canopyDesc canopyElesWrap">')
				.append($('<div class="growDescTop growDescEle canopyDescEle cf">')
					// .append($('<div class="growDescTopEle growDescTopLabel canopyDescLabel canopyDescTopEle">')
						// .append($('<div class="canopyDescLabelText">').text('Display'))
					// )
					.append($('<div class="growDescTopEle growDescLegend canopyDescLegend canopyDescTopEle">')
						.append($('<div class="growDescBottom growDescEle canopyDescEle">').html(self.grow.viewMore.html))
						// .append($('<div class="growDescLegendEle canopyLegendEle cf">')
							// .append($('<div class="growDescLegendEleItem canopyDescLegendEleItem canopyLegendEleEle">').html('<input type="radio" class="canopyLegendItemEle" checked="checked"/>'))
							// .append($('<div class="growDescLegendEleLabel canopyDescLegendEleLabel canopyLegendEleEle">').text('Activate Symbology'))
						// )
						// .append($('<div class="growDescLegendEle canopyLegendEle cf">')
							// .append($('<div class="growDescLegendEleItem canopyDescLegendEleItem canopyLegendEleEle">').html('<div class="canopyLegendItem canopyLegendItemEle weight"></div>'))
							// .append($('<div class="growDescLegendEleLabel canopyDescLegendEleLabel canopyLegendEleEle">').text('Weight'))
						// )
					)
				).append($('<div class="canopyDescButtons">')
					.append(self.grow.resetButton.html)
				);
			// self.grow.growSimTypeSelect=$('<select class="growSimTypeSelect growSettingsSelect">')
				// .append('<option value="individual">Individual</option>')
				// .append('<option value="polygon">Polygon</option>')
				// .append('<option value="addByAttribute">Add By Attribute</option>');
			// self.grow.growSimTypeSelect.find('*[value="'+self.grow.simType+'"]').attr('selected','selected');
			if(self.grow.growOutType=='target'){
				var targetSelected=true;
				var increaseSelected=false;
			}else if(self.grow.growOutType=='increase'){
				var targetSelected=false;
				var increaseSelected=true;
			}
			self.grow.growOutTypeRadioGroup=$('<div class="growOutTypeRadioGroup">')
				.append(new Radio({
					value:'target',
					label:'Target Canopy %',
					checked:targetSelected,
					name:'legendLayer',
					classes:'growOutTypeRadio',
					wrapClasses:'growOutTypeRadioWrap'
				}).html)
				.append(new Radio({
					value:'increase',
					label:'Increase Canopy By %',
					checked:increaseSelected,
					name:'legendLayer',
					classes:'growOutTypeRadio',
					wrapClasses:'growOutTypeRadioWrap'
				}).html)
			// self.grow.growOutTypeSelect=$('<select class="growOutTypeSelect growSettingsSelect">')
				// .append('<option value="target">Target Canopy %</option>')
				// .append('<option value="increase">Increase Canopy By %</option>');
			// self.grow.growOutTypeSelect.find('*[value="'+self.grow.growOutType+'"]').attr('selected','selected');
			self.grow.targetCanopySlider=new Range({
				alias:'Target Canopy %',
				classes:'canopyRange',
				key:'targetCanopySlider',
				to_fixed:0,
				value:[self.grow.targetCanopyPerc],
				step:10,
				max:self.grow.defaultTargetCanopyMax,
				min:self.grow.defaultTargetCanopyMin,
				//maxScaleMarks:10,
				append_to:'%',
				onChange:self.grow.targetCanopyRangeChange,
				handlesLength:1,
				withScale:true,
				round:.5
			});
			self.grow.increaseCanopySlider=new Range({
				alias:'Increase Canopy By %',
				classes:'canopyRange',
				key:'increaseCanopySlider',
				to_fixed:0,
				value:[self.grow.increaseCanopyPerc],
				step:1,
				max:self.grow.defaultIncreaseCanopyMax,
				min:self.grow.defaultIncreaseCanopyMin,
				append_to:'%',
				onChange:self.grow.increaseCanopyRangeChange,
				handlesLength:1,
				withScale:true,
				
				round:1
			});
			self.grow.avgTreeSizeSlider=new Range({
				alias:'Average Tree Crown Diameter',
				classes:'canopyRange',
				key:'avgTreeSizeSlider',
				to_fixed:0,
				value:[self.grow.avgTreeSize],
				step:10,
				relativeValues:self.treeSizeRelativeValues,
				relativeOutPuts:true,
				max:self.grow.defaultAvgTreeSizeMax,
				min:self.grow.defaultAvgTreeSizeMin,
				withScale:true,
				append_to:' ft.',
				onChange:self.grow.avgTreeSizeRangeChange,
				handlesLength:1,
				round:1
			});
			self.grow.targetCanopyFieldPkg=$('<div class="fieldPkg targetCanopy growOutFieldPkg canopy cf">')
				.append($('<div class="fieldPkgEle cf">')
					.append(self.grow.targetCanopySlider.html)
				);
			self.grow.increaseCanopyFieldPkg=$('<div class="fieldPkg increaseCanopy growOutFieldPkg canopy cf">')
				.append($('<div class="fieldPkgEle cf">')
					.append(self.grow.increaseCanopySlider.html)
				);
			self.grow.avgTreeSizeFieldPkg=$('<div class="fieldPkg avgTreeSize canopy cf">')
				.append($('<div class="fieldPkgEle cf">')
					.append(self.grow.avgTreeSizeSlider.html)
				);
			self.grow.growOnSwitch=new Switch(self.turnGrowOn,self.turnGrowOff);
			self.grow.reportScenClearButtonTop=new Button ('std','reportScenClearButtonTop','Clear Scenario');
			self.grow.reportScenClearButtonTop.html.on('click',function(){
				self.clearScenario();
			});
			self.grow.settingsTable=$('<table class="growSettingsTable canopyTable">')
				.append($('<tr>')
					.append($('<td>')
						.append($('<div class="tdLiner paintGrowLabel">').html('Paint Canopy Goals'))
					)
					.append($('<td>')
						.append($('<div class="tdLiner">').html(self.grow.growOnSwitch.container))
					)
				)
				.append(/* $('<tr>')
					.append($('<td>')
						.append($('<div class="tdLiner">').text('Simulation Type'))
					).append($('<td>')
						.append($('<div class="tdLiner">').html(self.grow.growSimTypeSelect))
					) */
				)
				.append($('<tr>')
					// .append($('<td>')
						// .append($('<div class="tdLiner">').text('Growout Type'))
					// )
					.append($('<td colspan="2">')
						.append($('<div class="tdLiner">').html(self.grow.growOutTypeRadioGroup))
						// .append($('<div class="tdLiner">').html(self.grow.growOutTypeSelect))
					)
				)
				.append($('<tr>')
					.append($('<td colspan="2">')
						.append($('<div class="tdLiner">')
							.append(self.grow.targetCanopyFieldPkg)
							.append(self.grow.increaseCanopyFieldPkg)
						)
					)
				)
				.append($('<tr>')
					.append($('<td colspan="2">')
						.append($('<div class="tdLiner">')
							.append(self.grow.avgTreeSizeFieldPkg)
						)
					)
				)
				.append($('<tr>')
					.append($('<td colspan="2">')
						.append($('<div class="tdLiner paintCanopyButtons">')
							.append(self.grow.reportScenClearButtonTop.html)
						)
					)
				);
			self.grow.selectLayer=$('<select class="growSelectLayer canopySelectLayer">')
			if(self.layers){
				var selected;
				for(var key in self.layers){
					if(self.layers[key].properties.in_grow_tools){
						selected="";
						if(self.layers[key].properties.primary_layer){
							selected=' selected="selected"';
						}
						self.grow.selectLayer.append('<option value="'+key+'"'+selected+'>'+self.layers[key].properties.alias+'</option>');
					}
				}
				main.utilities.sortOptions(self.grow.selectLayer);
			}
			self.grow.selectLayer.on('change',self.changeCurrentLayer);
			self.grow.settings=$('<div class="growSettings canopyElesWrap">')
				.append(self.grow.selectLayer)
				.append(self.grow.settingsTable);
				
			self.grow.growCharts={};	
			self.grow.growCharts.content=$('<div id=growChartWrapWrap>');
			self.grow.growCharts.Info=$('<h3>').html('Run <b>Grow</b> to see chart results.');
			self.grow.growCharts.content.append(self.grow.growCharts.Info);
			
			self.grow.growCharts.overallPercentWrap=$('<div class=growChartWrap>');
			self.grow.growCharts.overallAcreWrap=$('<div class=growChartWrap>');
			self.grow.growCharts.numTreesWrap=$('<div class=growChartWrap>');
			
			self.grow.growCharts.overallPercentTitle=$('<p id=overallGrowPercentTitle class=growChartTitle>').html('Overall Urban Tree Canopy (%)');
			self.grow.growCharts.overallAcreTitle=$('<p id=overallAcreTitle class=growChartTitle>').html('Overall Urban Tree Canopy (Acres)');
			self.grow.growCharts.numTreesTitle=$('<p id=numTreesTitle class=growChartTitle>').html('Number of Trees to Plant');
			
			self.grow.growCharts.overallPercentChart=$('<div id=growChartPercentChart class=growChartChart>');
			self.grow.growCharts.overallAcreChart=$('<div id=growChartAcreChart class=growChartChart>');
			self.grow.growCharts.numTreesChart=$('<div id=growChartTreesChart>');	
			
			self.grow.growCharts.overallPercentCurrent=$('<div id=growChartPercentChartCurrent class=growChartNumCurrent>');
			self.grow.growCharts.overallPercentModel=$('<div id=growChartPercentChartModel class=growChartNumModel>');
			self.grow.growCharts.overallAcreCurrent=$('<div id=growChartAcreChartCurrent class=growChartNumCurrent>');
			self.grow.growCharts.overallAcreModel=$('<div id=growChartAcreChartModel class=growChartNumModel>');
			self.grow.growCharts.numTreesNum=$("<div id=growChartTreesChartNum class=growChartNumTree>");
			
			self.grow.growCharts.overallPercentWrap
				.append(self.grow.growCharts.overallPercentTitle)
				.append(self.grow.growCharts.overallPercentChart)
				.append(self.grow.growCharts.overallPercentCurrent)
				.append(self.grow.growCharts.overallPercentModel);
			self.grow.growCharts.overallAcreWrap
				.append(self.grow.growCharts.overallAcreTitle)
				.append(self.grow.growCharts.overallAcreChart)
				.append(self.grow.growCharts.overallAcreCurrent)
				.append(self.grow.growCharts.overallAcreModel);
			self.grow.growCharts.numTreesWrap
				.append(self.grow.growCharts.numTreesTitle)
				.append(self.grow.growCharts.numTreesChart)
				.append(self.grow.growCharts.numTreesNum);
			
			self.grow.growCharts.content.append(self.grow.growCharts.overallPercentWrap).append(self.grow.growCharts.overallAcreWrap).append(self.grow.growCharts.numTreesWrap);
			
			
			//EcoBens
			self.grow.growBens={};
			self.grow.growBens.accordian=new Accordian('canopyEcoBenAccordian','place_in_order');
			self.grow.growBens.Info=$('<h3>').html('Run <b>Grow</b> to see ecosystem benefits.');
			self.grow.growBens.accordian.container.append(self.grow.growBens.Info);
			self.grow.growBens.shelves={};
			//overallContent=$('<div class="EcoBens">');
			
			//Create Ecobens
			var key,content,subContent,ecoben;
			var ecobens=main.data.std_settings.ecobens;
			
			var ecobensKeys=Object.keys(ecobens).sort(function(a,b){
				if(ecobens[a].place_in_order>ecobens[b].place_in_order){return 1;}
				if(ecobens[a].place_in_order<ecobens[b].place_in_order){return -1;}
				return 0;
			});
			self.ecobensItems={};
			for(var i=0;i<self.canopyEcobensKeys.length;i++){
				//small loop becuase this Item kept getting overwritten but the overall order is weird 
				key=self.canopyEcobensKeys[i];
				for(var t=0;t<ecobensKeys.length;t++){
						key0=ecobensKeys[t];
						if(ecobens[key0].category==key){
							self.ecobensItems[key0]={};
						}
				}
				contentWrap=$('<div class="EcoBens cf">');
				
				for(var j=0;j<self.ecobenTypes.length;j++){
					var type=self.ecobenTypes[j];
					
					
					img=self.canopyEcobensCategories[key].img || '';
					self.canopyEcobensCategories[key][type]={};
					self.canopyEcobensCategories[key][type].items={};
					content=$('<div class="EcoBensWrap cf">').html('<h3>'+type+'</h3>');
					
					content.append($('<img>').addClass('containerImg').attr('src',main.mainPath+'images/'+img));
				
					self.grow.growBens.shelves[key]={alias:self.canopyEcobensCategories[key].alias};
					self.grow.growBens.shelves[key].launchButton=new Button('toolBox','',self.grow.growBens.shelves[key].alias+"\t");
					for(var t=0;t<ecobensKeys.length;t++){
						key0=ecobensKeys[t];
						if(ecobens[key0].category==key){
							//self.ecobensItems[key0]={};
							if(ecobens[key0].canopy_ben){
								var ecoben=ecobens[key0];
								self.canopyEcobensCategories[key][type].items[key0]={};
								self.canopyEcobensCategories[key][type].items[key0].subContentContent=$('<div class="ecoSubGroupWrapContent">');
								self.canopyEcobensCategories[key][type].items[key0].subContent=$('<div class="growEcoSubGroupWrap '+key+'">')
									.append($('<div class="ecoSubGroupWrapLabel">').html(ecoben.alias))
									.append(self.canopyEcobensCategories[key][type].items[key0].subContentContent)
								content.append(self.canopyEcobensCategories[key][type].items[key0].subContent);
								contentWrap.append(content);
								self.ecobensItems[key0][type]=self.canopyEcobensCategories[key][type].items[key0];
							}
						}
					}
				}
				   self.grow.growBens.shelves[key].shelf=self.grow.growBens.accordian.createShelf({
						name:key,
						button:self.grow.growBens.shelves[key].launchButton.html,
						content:contentWrap,
						alias:self.grow.growBens.shelves[key].alias,
						extended:true
					});
					//overallContent.append(self.grow.growBens.shelves[key].shelf);	
			}
                
			
			
			self.grow.detailedGrowReport={};
			self.grow.detailedGrowReport.reports={};
			self.grow.detailedGrowReport.growReportCheckAll=$('<input type="checkbox" class="growReportCheckAll"/>');
			self.grow.detailedGrowReport.growReportCheckAll.on('change',function(){
				self.grow.detailedGrowReport.reportTBody.find('.growReportCheckBox').prop('checked',$(this).prop('checked'));
			});
			self.grow.detailedGrowReport.reportTHead=$('<thead>')
				.append($('<tr>')
					.append($('<th>').append(self.grow.detailedGrowReport.growReportCheckAll))
					.append($('<th>Scenario Name</th>'))
					.append($('<th>Existing Canopy (%)</th>'))
					.append($('<th>Modeled Canopy (%)</th>'))
					.append($('<th>No. Trees to Reach Goal</th>'))
					.append($('<th>Avg Tree Size (ft.)</th>'))
					.append($('<th>Annual Added Eco-Benefits ($)</th>'))
					.append($('<th>Restore</th>')));
			self.grow.detailedGrowReport.reportTBody=$('<tbody>');
			// self.grow.detailedGrowReport.reportTBody.on('keyup','.savedScenIDInput',function(){
				// var val=$(this).val();
				// if(val.length>self.grow.reportTitleLimit){
					// $(this).val(val.substring(0,self.grow.reportTitleLimit));
				// }
			// });
			// self.grow.detailedGrowReport.reportTBody.on('change','.savedScenIDInput',function(){
				// var val=$(this).val();
				// var pid=$(this).closest('.growReportTR').data('pid');
				// if(val.length>self.grow.reportTitleLimit){
					// val=val.substring(0,self.grow.reportTitleLimit);
					// $(this).val(val);
				// }
				// self.grow.currentReports[pid].area_name_or_id=val;
				// self.updateGrowReport(pid);
				// if(self.grow.currentGrowOut.reportID==pid){self.grow.reportScenTableTitle.val(val);}
			// });
			self.grow.detailedGrowReport.reportTable=$('<table class="canopyReportsTable" border="1">')
				.append(self.grow.detailedGrowReport.reportTHead)
				.append(self.grow.detailedGrowReport.reportTBody);
			self.grow.detailedGrowReport.reportTableWrapWrap=$('<div class="canopyReportsTableWrap none">').append(self.grow.detailedGrowReport.reportTable);
			self.grow.detailedGrowReport.reportTable.on('click','.canopyScenRestoreButton',function(){
				main.layout.loader.loaderOn();
				self.updateTotals(self.restoreScenario,{pid:$(this).closest('.growReportTR').data('pid')});
			});
			self.grow.detailedGrowReport.saveButton=new Button('std','growReportSave','Save Scenario');
			self.grow.detailedGrowReport.deleteButton=new Button('std','growReportDelete','Delete Scenario');
			self.grow.detailedGrowReport.buttons=$('<div class="detailedGrowReportButtons">')
				.append(self.grow.detailedGrowReport.saveButton.html)
				.append(self.grow.detailedGrowReport.deleteButton.html);
			self.grow.detailedGrowReport.deleteButton.html.on('click',function(){
				self.deleteGrowReports();
			});
			self.grow.detailedGrowReport.saveButton.html.on('click',function(){
				self.saveCurrentGrowOut();
			});
			self.grow.detailedGrowReport.label=$('<div class="detailedGrowReportLabel growDetailsLabel">').html('Overall Summary: Saved Scenarios');
			self.grow.detailedGrowReport.reportTableWrap=$('<div class="detailedGrowReportTableWrap scenarioDetailsWrap">')
				.append(self.grow.detailedGrowReport.label)
				.append(self.grow.detailedGrowReport.reportTableWrapWrap)
				.append(self.grow.detailedGrowReport.buttons);
				
			self.grow.reportScenPrintButton=new Button ('std','reportScenPrintButton','Print Scenario');
			self.grow.reportScenPrintButton.html.on('click',function(){
				self.updateTotals(self.printCurrentScenario);
			});
			self.grow.reportScenUpdateButton=new Button ('std','reportScenUpdateButton none','Update Scenario');
			self.grow.reportScenUpdateButton.html.on('click',function(){
				self.updateTotals(self.updateReportFromCurrentGrowOut);
			});
			self.grow.reportScenClearButton=new Button ('std','reportScenClearButton','Clear Scenario');
			self.grow.reportScenClearButton.html.on('click',function(){
				self.clearScenario();
			});
			self.grow.reportScenButtons=$('<div class="reportScenButtons">')
				.append(self.grow.reportScenPrintButton.html)
				.append(self.grow.reportScenClearButton.html)
				.append(self.grow.reportScenUpdateButton.html);
			self.grow.reportScenTBody=$('<tbody class="reportScenTBody">');
			self.grow.reportScenTHead=$('<thead class="reportScenTHead">')
				.append($('<tr>')
					.append($('<th>Area Name or ID</th>'))
					.append($('<th>Existing Canopy (%)</th>'))
					.append($('<th>Modeled Canopy (%)</th>'))
					.append($('<th>Net Change Canopy (%)</th>'))
					.append($('<th>No. Trees to Reach Goal</th>'))
					.append($('<th>Avg Tree Size (ft.)</th>'))
					.append($('<th>Annual Added Eco-Benefits ($)</th>')));
			self.grow.reportScenTable=$('<table border="1" class="reportScenTable">')
				.append(self.grow.reportScenTHead)
				.append(self.grow.reportScenTBody);
			//get tr from current table
			self.grow.reportScenTableWrap=$('<div class="reportScenTableWrap">').append(self.grow.reportScenTable);
			self.grow.reportScenTableLabel=$('<div class="reportScenTableLabel growDetailsLabel">').html('Scenario Details');
			self.grow.reportScenTableTitleLabel=$('<div class="reportScenTableTitleLabel">').html('Scenario Title:');
			self.grow.reportScenTableTitle=$('<input type="text" class="reportScenTableTitle">');
			self.grow.reportScenTableTitleVal=null;
			self.grow.reportScenTableTitle.on('change',function(){
				var val=$(this).val();
				if(val.length>self.grow.reportTitleLimit){
					val=val.substring(0,self.grow.reportTitleLimit);
					$(this).val(val);
				}
				self.grow.reportScenTableTitleVal=val;
				if(self.grow.currentGrowOut.reportID){
					self.grow.detailedGrowReport.reportTBody.find('.growReportTR_'+self.grow.currentGrowOut.reportID+' .growReportCell.area_name_or_id').html(val);
					self.grow.currentReports[self.grow.currentGrowOut.reportID].area_name_or_id=val;
					self.updateGrowReport(self.grow.currentGrowOut.reportID);
				}
			});
			self.grow.reportScenTableTitleWrap=$('<div class="reportScenTableTitleWrap cf">')
				.append(self.grow.reportScenTableTitleLabel)
				.append(self.grow.reportScenTableTitle);
			self.grow.reportScenTableDetailsWrap=$('<div class="reportScenTableDetailsWrap">')
				.append(self.grow.reportScenTableTitleWrap);
			self.grow.reportScenWrap=$('<div class="reportScenWrap scenarioDetailsWrap none">')
				.append(self.grow.reportScenTableLabel)
				.append(self.grow.reportScenTableDetailsWrap)
				.append(self.grow.reportScenTableWrap)
				.append(self.grow.reportScenButtons)
			
			self.grow.detailedGrowReport.content=$('<div class="detailedGrowReportWrap">')
				.append(self.grow.reportScenWrap)
				.append(self.grow.detailedGrowReport.reportTableWrap);
			
			self.grow.accordian=new Accordian('canopyGrowAccordian','place_in_order');
			self.grow.accordian.createShelf({
				name:'assumptions',
				button:new Button('toolBox','','Assumptions').html,
				content:self.grow.settings,
				alias:'Assumptions',
				place_in_order:1
			});
			
			self.grow.accordian.createShelf({
				name:'modeled_tree_canopy',
				button:new Button('toolBox','','Modeled Tree Canopy').html,
				content:self.grow.growCharts.content,
				alias:'Modeled Tree Canopy',
				place_in_order:2,
				onOpen:self.updateGrowCharts,
				extended:true
			});
			if(self.withEcobens){
				self.grow.accordian.createShelf({
					name:'modeled_ecosystem_benefits',
					button:new Button('toolBox','','Modeled Ecosystem Benefits').html,
					content:self.grow.growBens.accordian.container,
					alias:'Modeled Ecosystem Benefits',
					place_in_order:3,
					onOpen:self.updateEcoBens,
					extended:true
				});
			}
			self.grow.accordian.createShelf({
				name:'detailed_grow_report',
				button:new Button('toolBox','','Detailed Grow Report').html,
				content:self.grow.detailedGrowReport.content,
				alias:'Detailed Grow Report',
				extended:true,
				place_in_order:4
			});
			self.updateGrowTable();
			// self.grow.growSimTypeSelect.on('change',function(){
				// var val=$(this).val();
				// self.grow.simType=val;
				// if(val=='individual'){
					// self.initIndividualGrowOut();
				// }else if(val=='polygon'){
					// self.deInitIndividualGrowOut();
				// }else if(val=='addByAttribute'){
					// self.deInitIndividualGrowOut();
				// }
				// $(this).blur();
			// });
			self.grow.growOutTypeRadioGroup.on('change','.growOutTypeRadio',function(){
			// self.grow.growOutTypeSelect.on('change',function(){
				var val=$(this).val();
				self.grow.growOutType=val;
				if(val=='target'){
					self.grow.targetCanopyFieldPkg.removeClass('none');
					self.grow.increaseCanopyFieldPkg.removeClass('block');
				}else if(val=='increase'){
					self.grow.targetCanopyFieldPkg.addClass('none');
					self.grow.increaseCanopyFieldPkg.addClass('block');
				}
				$(this).blur();
			});
			self.grow.tools=$('<div class="growTools canopyTools">')
			self.updateGrowTools(self.currentLayer,self.grow.rangeChange);
			// if(self.grow.simType=='individual'){
				// self.initIndividualGrowOut();
			// }
			return self.grow.html=$('<div class="growWrap canopyTabContent">')
				.append(self.grow.desc)
				.append(self.grow.accordian.container);
				// .append(self.grow.tools)
		};
		this.updateGrowReport=function(pid){
			var curReport=self.grow.currentReports[pid];
			var pkg={};
			pkg.pid=curReport.pid;
			pkg.area_name_or_id=curReport.area_name_or_id;
			pkg.existing_canopy_perc=curReport.existing_canopy_perc;
			pkg.modeled_canopy_perc=curReport.modeled_canopy_perc;
			pkg.no_trees_to_reach_goal=curReport.no_trees_to_reach_goal;
			pkg.tree_size_used=curReport.tree_size_used;
			pkg.annual_eco_benefits=curReport.annual_eco_benefits;
			pkg.layer=curReport.layer;
			pkg.area_total=curReport.area_total;
			var features=curReport.features;
			var featuresPpkg={};
			for(var key in features){
				pid=features[key].pid;
				featuresPpkg[pid]={};
				featuresPpkg[pid].pid=pid;
				featuresPpkg[pid].area=features[key].area;
				featuresPpkg[pid].avgTreeSize=features[key].avgTreeSize;
				featuresPpkg[pid].hypCanopy=features[key].hypCanopy;
				featuresPpkg[pid].treesNeeded=features[key].treesNeeded;
			}
			pkg.features=JSON.stringify(featuresPpkg);
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						pkg:pkg
					},
					action:'updateGrowReport'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							if(response.results.length){
								for(var i=0;i<response.results.length;i++){
									self.grow.currentReports[response.results[i].pid]=response.results[i];
									self.grow.detailedGrowReport.reportTBody.find('.growReportTR_'+response.results[i].pid).replaceWith(self.addGrowReportRow(response.results[i]));
								}
							}
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.getFeaturesPkg=function(){
			var pkg={},pid;
			var features=self.grow.currentGrowOut.features;
			for(var key in features){
				pid=features[key].pid;
				pkg[pid]={};
				pkg[pid].pid=pid;
				pkg[pid].area=features[key].area;
				pkg[pid].avgTreeSize=features[key].avgTreeSizeMeters/0.3048;
				pkg[pid].hypCanopy=features[key].hypCanopy;
				pkg[pid].treesNeeded=features[key].treesNeeded;
			}
			return pkg;
		};
		this.updateReportFromCurrentGrowOut=function(){
			if(self.grow.currentGrowOut.reportID){
				self.grow.currentReports[self.grow.currentGrowOut.reportID].features=self.getFeaturesPkg();
				self.grow.currentReports[self.grow.currentGrowOut.reportID].annual_eco_benefits=self.grow.totals.annualEcoBens;
				self.grow.currentReports[self.grow.currentGrowOut.reportID].area_total=self.grow.totals.area_total;
				self.grow.currentReports[self.grow.currentGrowOut.reportID].existing_canopy_perc=self.grow.totals.existingCanopy;
				self.grow.currentReports[self.grow.currentGrowOut.reportID].modeled_canopy_perc=self.grow.totals.modeledCanopy;
				self.grow.currentReports[self.grow.currentGrowOut.reportID].no_trees_to_reach_goal=self.grow.totals.noTreesToReachGoal;
				self.grow.currentReports[self.grow.currentGrowOut.reportID].tree_size_used=self.grow.totals.avgTreeSize;
				self.updateGrowReport(self.grow.currentGrowOut.reportID);
			}
		};
		this.saveCurrentGrowOut=function(){
			if(Object.keys(self.grow.currentGrowOut.features).length){
				var pkg=self.getFeaturesPkg();
				var name=null;
				if(self.grow.reportScenTableTitle.val()){name=self.grow.reportScenTableTitle.val();}
				$.ajax({
					type:'POST',
					url:main.mainPath+'server/db.php',
					data:{
						params:{
							folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
							features:JSON.stringify(pkg),
							layer:self.currentLayer.properties.name,
							modeledCanopy:self.grow.totals.modeledCanopy,
							noTreesToReachGoal:self.grow.totals.noTreesToReachGoal,
							avgTreeSize:self.grow.totals.avgTreeSize,
							existingCanopy:self.grow.totals.existingCanopy,
							area_total:self.grow.totals.area_total,
							annualEcoBens:self.grow.totals.annualEcoBens,
							name:name
						},
						action:'saveGrowReports'
					},
					success:function(response){
						if(response){
							response=JSON.parse(response);
							if(response.status=="OK"){
								self.grow.currentGrowOut.reportID=response.result.pid;
								self.grow.currentReports[response.result.pid]=response.result;
								self.grow.detailedGrowReport.reportTBody.append(self.addGrowReportRow(response.result));
								self.testDGRLength();
							}else{
								alert('Error');
								console.log(response);
							}
						}
					}
				});
			}else{
				alert('There is no current grow out.');
			}
		};
		this.setGrowOutEvents=function(){
			$(document).on('keydown',self.indGrowOutKeyDown);
			$(document).on('keyup',self.indGrowOutKeyUp);
			$(self.map.map.getViewport()).on(main.controls.touchMove,self.indGrowOutMouseMove);
			main.layout.loader.loaderOff();
		};
		this.initIndividualGrowOut=function(){
			main.layout.loader.loaderOn();
			self.growing=true;
			self.updateTotals(self.setGrowOutEvents);
		};
		this.deInitIndividualGrowOut=function(){
			self.growing=false;
			$(document).off('keydown',self.indGrowOutKeyDown);
			$(document).off('keyup',self.indGrowOutKeyUp);
			self.grow.growOnSwitch.turnSwitchOff(true);
			// $(self.map.map.getViewport()).off(main.controls.touchMove,self.indGrowOutMouseMove);
		};
		this.indGrowOutKeyDown=function(e){
			if(self.toolType=='grow' && self.grow.simType=='individual' && (e.which==self.grow.growKey || e.which==self.grow.removeKey)){
				if(e.which==self.grow.growKey){
					self.grow.painting=true;
				}else if(e.which==self.grow.removeKey){
					self.grow.removing=true;
				}
				if(self.lastMoveEvent){
					self.indGrowOutMouseMove(self.lastMoveEvent);
				}
				self.infoTip.triggerPointerMove();
			}
		};
		this.indGrowOutKeyUp=function(e){
			if(e.which==self.grow.growKey || e.which==self.grow.removeKey){
				self.grow.painting=false;
				self.grow.removing=false;
				self.updateTotals(self.updateReportScenTBody);
				if(self.grow.currentGrowOut.reportID){
					self.grow.reportScenUpdateButton.html.removeClass('none');
				}
			}
		};
		this.resetCurrentGrowOut=function(){
			self.grow.currentGrowOut={
				features:{},
				targetCanopy:null,
				increaseBy:null,
				avgTreeSize:null,
				layer:null,
				reportID:null
			};
		};
		this.resetGrowOutScenario=function(){
			self.resetCurrentGrowOut();
			self.updateSymbology();
		};
		this.getTreeDiamMeters=function(treeDiamFt){
			return Math.pow((treeDiamFt*0.3048)/2,2)*Math.PI
		};
		this.convertAcresToMeters=function(area){
			return area*4046.86;
		};
		this.getTreesNeeded=function(area,newCanopy,currentCanopy,treeDiamFt){
			return (self.convertAcresToMeters(area)/(100*(self.getTreeDiamMeters(treeDiamFt))))*(newCanopy-currentCanopy);
		};
		this.indGrowOutMouseMove=function(e){
			self.lastMoveEvent=e;
			if(self.grow.painting){
				var features=self.currentLayer.map_layer.layer.source.getFeaturesAtCoordinate(self.map.map.getEventCoordinate(e.originalEvent));
				if(features.length>0){
					var feature=features[0];
					var props=feature.getProperties();
					if(self.grow.currentCanopyField){
						var growOutType=self.grow.growOutType;
						var area=props.area_acre;
						var avgTreeSizeMeters=self.grow.avgTreeSize*0.3048;
						if(self.grow.growOutType=='target'){
							var newCanPct=self.grow.targetCanopyPerc;
						}else if(self.grow.growOutType=='increase'){
							var newCanPct=props[self.grow.currentCanopyField.name]+self.grow.increaseCanopyPerc;
						}
						if(newCanPct>100){newCanPct=100;}
						var treesNeeded=self.getTreesNeeded(area,newCanPct,props[self.grow.currentCanopyField.name],self.grow.avgTreeSize);
						self.grow.currentGrowOut.features[props.pid]={
							hypCanopy:newCanPct,
							treesNeeded:treesNeeded,
							pid:props.pid,
							area:area,
							avgTreeSizeMeters:avgTreeSizeMeters,
							properties:props,
							feature:feature
						};
						// feature.setProperties({hypCanopy:curCanPct});
						// self.currentLayer.layer.olLayer.setStyle(self.currentLayer.layer.olLayer.getStyle);
						self.refreshFeatures();
					}
				}
			}else if(self.grow.removing){
				var features=self.currentLayer.map_layer.layer.source.getFeaturesAtCoordinate(self.map.map.getEventCoordinate(e.originalEvent));
				if(features.length>0){
					var props=features[0].getProperties();
					if(self.grow.currentGrowOut.features[props.pid]){
						delete self.grow.currentGrowOut.features[props.pid];
						self.refreshFeatures();
					}
				}
			}
		};
		this.refreshFeatures=function(){
			self.currentLayer.map_layer.layer.setStyle(self.filterStyleFunction);
		};
		this.updateForLogIn=function(){
			self.updateGrowTable();
			self.grow.reportScenUpdateButton.html.addClass('none');
			self.grow.currentGrowOut.reportID=null;
			// self.updateAllGrowCharts();
		};
		this.updateAllGrowCharts=function(){
			self.updateGrowCharts();
			if(self.withEcobens){
				self.updateEcoBens();
			}
		};
		this.updateTotals=function(cb,cbParams){
			if(self.grow.currentGrowOut && Object.keys(self.grow.currentGrowOut.features).length){
				var features=self.grow.currentGrowOut.features;
				var avgTreeSizeSum=0;
				for(var key in features){
					avgTreeSizeSum+=features[key].avgTreeSizeMeters;
				}
				self.grow.totals.avgTreeSize=avgTreeSizeSum/Object.keys(self.grow.currentGrowOut.features).length;
			}
			//http://libinfo.uark.edu/aas/issues/2005v59/v59a14.pdf
			var avgTreeDBH=(0.25439+(10.97732*(self.grow.avgTreeSize/2)*0.3048))*0.393701;
			var pkg={},pid;
			var features=self.grow.currentGrowOut.features;
			for(var key in features){
				pid=features[key].properties.pid;
				pkg[pid]={};
				pkg[pid].area=features[key].area;
				pkg[pid].avgTreeSize=features[key].avgTreeSizeMeters/0.3048;
				pkg[pid].hypCanopy=features[key].hypCanopy;
				pkg[pid].treesNeeded=features[key].treesNeeded;
			}
			//Update modeled canopy for labeling	
			var feat=main.map_layers[self.currentLayer.properties.name].layerObj.features;
			var temp_vals={};
			for (var i=0;i<feat.length;i++){
				var temp_pid=feat[i].properties.pid;
				
				if(pkg[temp_pid]){
					feat[i].properties.last_grow_canopy_model=pkg[temp_pid].hypCanopy;
					temp_vals[temp_pid]=pkg[temp_pid].hypCanopy;
				}
				else{
					feat[i].properties.last_grow_canopy_model=feat[i].properties[self.grow.currentCanopyField.name];
					temp_vals[temp_pid]=feat[i].properties[self.grow.currentCanopyField.name];
				}
			}
			main.map.updateLayerSource(self.currentLayer.properties.name,false);
			 $.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						keys:temp_vals,
						layer:self.currentLayer.properties.name
					}, 
					action:'updateModelCanopy',
					success:function(response){
					if(response){
						//nothing to do
					}}
			}}); 
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						layer:self.currentLayer.properties.name,
						avgTreeDBH:avgTreeDBH,
						features:JSON.stringify(pkg),
						current_canopy_field:self.grow.currentCanopyField.name
					}, 
					action:'getGrowTableTotals'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							if(response.current_canopy && response.current_canopy>-1){
								self.grow.totals.existingCanopy=Number(response.current_canopy);
								self.grow.totals.area_total=Number(response.area_total);
								self.grow.totals.modeledCanopy=Number(response.modeledCanopy);
								self.grow.totals.noTreesToReachGoal=Number(response.noTreesToReachGoal);
								// var hypCanopy=0;
								// var noTreesToReachGoal=0;
								// var features=self.grow.currentGrowOut.features;
								// for(var key in features){
									// hypCanopy+=features[key].hypCanopy*(features[key].area/self.grow.totals.area_total);
									// noTreesToReachGoal+=features[key].treesNeeded;
								// }
								// self.grow.totals.modeledCanopy=hypCanopy*100;
								// self.grow.totals.noTreesToReachGoal=noTreesToReachGoal;
								self.grow.totals.ecoBenefits=response.ecoResults;
								//only using some of the possible benefits
								var airDollars=Number(self.grow.totals.ecoBenefits.AirQualityBenefits.TotalDollarValue);
								var carbonDollars=Number(self.grow.totals.ecoBenefits.CO2Benefits.DollarValue);
								self.grow.totals.ecoBenefits.OverallBenefits.DollarValue=airDollars+carbonDollars;
								
								self.grow.modelEcoVals=response.ecoResults;
								self.grow.totals.annualEcoBens=response.ecoResults.OverallBenefits.DollarValue*self.grow.totals.noTreesToReachGoal;
								self.grow.totals.ecoStats=response.ecoResults;
								
								self.updateGrowCharts();
								if(self.withEcobens){
									self.updateEcoBens();
								}
							}
							if(cb){
								cb(response,cbParams);
							}
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.initGrow=function(){
			self.grow.accordian.open('assumptions');
			self.setCurrentCanopyField();
		};
		this.setCurrentCanopyField=function(){
			self.grow.currentCanopyField=null;
			for(var key in self.currentLayer.fields){
				if(self.currentLayer.fields[key].current_canopy_pct){
					self.grow.currentCanopyField=self.currentLayer.fields[key];
				}
			}
		};
		this.tabClick=function(args){
			if(!self.tabs.tabsWrap.find('.canopyTabs_'+args.name).hasClass('activeTab')){
				self.tabs.setActiveTab(self.tabs.tabsWrap.find('.canopyTabs_'+args.name).data('tabid'));
			}
			self.infoTip.unFreeze();
			if(self.toolType!=args.name){
				if(self.toolType=='view'){self.resetView();
				}else if(self.toolType=='plan'){self.resetPlan();
				}else if(self.toolType=='grow'){self.turnGrowOff();}
				self.toolType=args.name;
			}
			var loaded=false;
			if(self.grow.accordian){self.grow.accordian.closeAllShelves();}
			if(self.toolType=='view'){
				if(!self.currentLayer.properties.in_view_tools){
					loaded=self.newLayerGo(self.layers[Object.keys(self.view.layers)[0]].properties.layer_name);
				}else{loaded=true;}
				if(loaded){
					self.view.tools.find('.canopyActivate.'+self.currentField.name).prop('checked',true);
					self.updateSymbology();
				}
			}else if(self.toolType=='plan'){
				if(!self.currentLayer.properties.in_plan_tools){
					loaded=self.newLayerGo(self.layers[Object.keys(self.plan.layers)[0]].properties.layer_name);
				}else{loaded=true;}
				if(loaded){
					self.startWeighting();
				}
			}else if(self.toolType=='grow'){
				if(!self.currentLayer.properties.in_grow_tools){
					loaded=self.newLayerGo(self.layers[Object.keys(self.grow.layers)[0]].properties.layer_name);
				}else{loaded=true;}
				if(loaded){
					self.initGrow();
					self.setCurrentField(self.grow.currentCanopyField.name);
					if(self.view.tools){self.view.tools.find('.canopyActivate').prop('checked',false);}
					self.updateSymbology();
				}
			}
			if(loaded){self.initLegend();}
		};
		this.updateWeightFilterPkg=function(){
			var moreInfoContent="Based on the weights applied to each selected criteria above, filter the results from low to high scoring.";
			self.plan.weightFilterPkgRange=new Range({
				alias:'Filter by '+self.weightLabelAlias,
				classes:'canopyRange fieldPkgRightEle',
				key:'weightedScoreRange',
				to_fixed:0,
				min:self.currentLayer.canopy.plan.weightedScore.minWeightedScore,
				max:self.currentLayer.canopy.plan.weightedScore.maxWeightedScore,
				// withScale:true,
				
				
				
				onChange:self.plan.weightFilterChange,
				onMove:self.plan.weightFilterChange,
				displayFunction:self.weightFilterPkgDisplay,
				moreInfo:true,
				moreInfoContent:moreInfoContent,
				handlesLength:1,
				noOutputs:true

			});
			self.currentLayer.canopy.plan.weightedScore.weightFilter.currentHi=self.currentLayer.canopy.plan.weightedScore.maxWeightedScore;
			self.currentLayer.canopy.plan.weightedScore.weightFilter.currentLow=self.currentLayer.canopy.plan.weightedScore.minWeightedScore;
			self.plan.weightFilterPkg=self.createRangePkgHTML(self.plan.weightFilterPkgRange);
			self.plan.weightFilterWrap.html(self.plan.weightFilterPkg);
		};
		this.startWeighting=function(){
			self.startedWeighting=true;
			if(self.view.tools){self.view.tools.find('.canopyActivate').prop('checked',false);}
			self.updateWeights();
			self.updateWeightFilterPkg();
		};
		this.updateWeights=function(){
			//get Weight Extremes
			var features=self.currentLayer.map_layer.features,weightedScore;
			var fields=self.currentLayer.canopy.plan.fields, maxWeightedScore=-Infinity,minWeightedScore=Infinity,numbers=[];
			var keys={};
			for(var key in features){
				weightedScore=self.getWeightedScore(features[key].properties,fields);
				numbers.push(weightedScore);
				if(weightedScore>maxWeightedScore){maxWeightedScore=weightedScore;}
				if(weightedScore<minWeightedScore){minWeightedScore=weightedScore;}
				keys[features[key].properties.pid]={pid:features[key].properties.pid,weightedScore:weightedScore};
			}
			var keys2=Object.keys(keys);
			var sortedKeys=keys2.sort(function(a,b){
				if(keys[a].weightedScore<keys[b].weightedScore){return 1;}
				if(keys[a].weightedScore>keys[b].weightedScore){return -1;}
				return 0;
			});
			self.currentLayer.canopy.plan.weightedScore.sortedKeys=sortedKeys;
			
			var feat=main.map_layers[self.currentLayer.properties.name].layerObj.features;
			for (var i=0;i<feat.length;i++){
				var temp_pid=feat[i].properties.pid;
				feat[i].properties.last_plan_rank=sortedKeys.indexOf(temp_pid.toString())+1;
			}
			main.map.updateLayerSource(self.currentLayer.properties.name,false);
			 $.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						keys:sortedKeys,
						layer:self.currentLayer.properties.name
					}, 
					action:'updatePlanRanks',
					success:function(response){
					if(response){
						//nothing to do
					}}
			}}); 
			var stats=main.utilities.getStdDev(numbers);
			self.currentLayer.canopy.plan.weightedScore.weightedScoreStdDev=stats.stdDev;
			self.currentLayer.canopy.plan.weightedScore.weightedScoreAvg=stats.mean;
			self.currentLayer.canopy.plan.weightedScore.maxWeightedScore=maxWeightedScore;
			self.currentLayer.canopy.plan.weightedScore.minWeightedScore=minWeightedScore;
			self.refreshFeatures();
		};
		this.updateSymbology=function(){
			self.refreshFeatures();
		};
		this.initLayerObjects=function(){
			for(var key in self.layers){
				self.layers[key].canopy={
					view:{fields:{}},
					plan:{weightedScore:{weightFilter:{}},fields:{}},
					grow:{fields:{}},
					stats:{fields:{}}
				};
			}
		};
		this.createLegend=function(ctx,canvas){
			var gradientWidth=26;
			var gradientHeight=180;
			ctx.save();
			ctx.beginPath();
			var grd = ctx.createLinearGradient(0, 0, 0, gradientHeight);
			if(self.toolType=='plan' || !self.currentField.color_grad){
				grd=main.utilities.createGradient(grd,self.currentWeightColorGrad.name);
			}else{
				grd.addColorStop(1,self.currentField.color_grad[0]);
				grd.addColorStop(0,self.currentField.color_grad[1]);
			}
			ctx.fillStyle = grd;
			ctx.fillRect(0, 0, gradientWidth, gradientHeight);
			return [gradientWidth,gradientHeight];
		}
		this.initLegend=function(){
			var canvasWidth=26, canvasHeight=180;
			var canvas=document.createElement('canvas');
			canvas.width=canvasWidth;
			canvas.height=canvasHeight;
			canvas.id='canopyLegendGradient';
			var ctx=canvas.getContext('2d');
			grdParams=self.createLegend(ctx,canvas);
			var max,min,middle,onEnd,fieldTitle,toFixed;
			if(self.toolType=='plan'){
				max=self.relativeValues[2],min=self.relativeValues[0],middle=self.relativeValues[1],onEnd='',fieldTitle=self.weightLabelAlias,toFixed=self.defaultWeightToFixed;
			}else{
				var stats=self.currentLayer.canopy.stats.fields[self.currentField.name];
				var toFixed=Math.min(self.maxToFixed,self.currentField.to_fixed);
				onEnd=self.currentField.append_to,fieldTitle=self.currentField.alias;
				if(self.toolType=='grow'){
					max=(Math.max(stats.max/* ,main.canopy.canopyMax */));
					min=(Math.min(stats.min/* ,main.canopy.canopyMin */));
					fieldTitle='Percent Urban Tree Canopy';
				}else{
					max=stats.max;
					min=stats.min;
				}
				middle=((max+min)/2).toFixed(toFixed)+onEnd;
				max=max.toFixed(toFixed)+onEnd;
				min=min.toFixed(toFixed)+onEnd;
			}
			var labelContainer=$('<div>').addClass('canopyLegendScaleWrap legend1').height(grdParams[1])
					.append($('<div>').addClass('canopyLegendMax canopyScaleEle').html(max))
					.append($('<div>').addClass('canopyLegendMiddle canopyScaleEle').html(middle))
					.append($('<div>').addClass('canopyLegendMin canopyScaleEle').html(min))
					// .append($('<div>').addClass('legendFieldLabel label1').append($('<div>').addClass('legendFieldLabelText').html(fieldTitle)))
			self.legend=$('<div class="cf canopyLegendContent">')
				.append($('<div>').addClass('canopyLegendTitleWrap')
					.append($('<div>').addClass('canopyLegendTitle').html(fieldTitle))
					// .append($('<div>').addClass('legendTitleCity subTitleEle').html(main.cities[main.closestCity].alias))
				)
				.append($('<div>').addClass('canopyLegendGraphicWrapWrap')
					.append($('<div>').addClass('canopyLegendGraphicWrap cf')
						.append(canvas)
						.append(labelContainer)
					)
				);
			var offset=parseInt(self.legend.find('.canopyLegendMiddle').css('font-size'))/2;
			var middleTop=grdParams[1]/2-offset;
			self.legend.find('.canopyLegendMax').css('top',-offset);
			self.legend.find('.canopyLegendMiddle').css('top',middleTop);
			self.legend.find('.canopyLegendMin').css('bottom',-offset);
			// var label1=self.legend.find('.label1 .legendFieldLabelText');
			// label1.css('display','inline-block');
			// var label1Top=(grdParams[1])/2+label1.width()/2-parseInt(label1.css('font-size'))/2;
			// label1.css('display','block');
			// $('#mainLegend .label1').css('top',label1Top);
			//hide label if don't want side label
			// label1.hide();
			if(!main.personal_settings.app_settings.canopy_with_invents){
				if(!self.panel){
					self.panel=new Panel({
						content:self.legend,
						title:'Canopy',
						classes:'canopyLegend',
						position:'tRight',
						relativeTo:main.layout.mapContainerWrap,
						onMin:self.panelMin,
						onShow:self.panelShow,
						withOutClose:true
					});
				}else{
					self.panel.setContent(self.legend);
				}
			}else if(main.filter_legend && main.filter_legend.flCanopyWrap){
				main.filter_legend.flCanopyWrap.html(self.legend);
			}
		};
		this.panelMin=function(){
		};
		this.panelShow=function(){
		};
		this.setStats=function(layer,dontUpdateExtremes){
			if(Object.keys(layer.fields).length>0){
				var fields=layer.fields;
				var features=layer.map_layer.features;
				var value;
				for(var key in fields){
					if(fields[key].in_canopy_tools){
						layer.canopy.stats.fields[key]={
							max:-Infinity,
							min:Infinity,
							currentHi:-Infinity,
							currentLow:Infinity
						};
						layer.canopy.plan.fields[key]={
							currentWeight:self.defaultWeight
						};
						var numbers=[];
						for(var key2 in features){
							value=Number(features[key2].properties[fields[key].name]);
							numbers.push(value)
							if(value>layer.canopy.stats.fields[key].max){
								layer.canopy.stats.fields[key].max=value;
								layer.canopy.stats.fields[key].currentHi=value;
							}
							if(value<layer.canopy.stats.fields[key].min){
								layer.canopy.stats.fields[key].min=value;
								layer.canopy.stats.fields[key].currentLow=value;
							}
						}
						var stats=main.utilities.getStdDev(numbers);
						layer.canopy.stats.fields[key].avg=stats.mean;
						layer.canopy.stats.fields[key].stdDev=stats.stdDev;
						if(self.view.fields[key] && !dontUpdateExtremes){
							self.view.fields[key].range.updateExtremes(layer.canopy.stats.fields[key].max,layer.canopy.stats.fields[key].min,true,{dontRefreshMap:true});
						}
					}
				}
			}
		};
		this.setPrimaryLayer=function(){
			if(self.layers){
				for(var key in self.layers){
					if(self.layers[key].properties.primary_layer){
						self.primaryLayer=self.layers[key];
					}
				}
			}
			if(!self.primaryLayer){self.primaryLayer=self.layers[Object.keys(self.layers)[0]];}
		};
		this.setPrimaryField=function(){
			var fields=self.currentLayer.fields;
			for(var key in fields){
				if(fields[key].primary_field){
					self.primaryField=fields[key];
				}
			}
			if(!self.primaryField){
				self.setCurrentCanopyField();
				if(self.grow.currentCanopyField){
					self.primaryField=self.grow.currentCanopyField;
				}
				if(!self.primaryField){
					self.primaryField=self.currentLayer.fields[Object.keys(self.currentLayer.fields)[0]];
				}
			}
		};
		this.setCurrentLayer=function(layer){
			self.currentLayerSortedFieldKeys=main.utilities.sortTabFields(Object.keys(layer.fields),layer.fields);
			self.currentLayer=layer;
		};
		this.setCurrentField=function(fieldName){
			self.currentField=self.currentLayer.fields[fieldName];
		};
		this.updateViewTools=function(){
			self.view.tools.html('');
			self.view.groups={};
			if(self.currentLayer.fields){
				var field;
				for(var t=0;t<self.currentLayerSortedFieldKeys.length;t++){
					key=self.currentLayerSortedFieldKeys[t];
					if(self.currentLayer.fields[key].in_canopy_tools && self.currentLayer.fields[key].in_view_tools){
						self.view.fields[key]=main.utilities.cloneObj(self.currentLayer.fields[key]);
						var a=self.createRangePkg(self.currentLayer.fields[key],self.view.rangeChange,self.view.rangeChange);
						if(self.currentLayer.fields[key].groups){
							if(!self.view.groups[self.currentLayer.fields[key].groups]){
								self.view.groups[self.currentLayer.fields[key].groups]={fields:{}};
							}
							self.view.groups[self.currentLayer.fields[key].groups].fields[key]={html:a.addClass('fieldPkgOfGroup')};
							if(self.currentLayer.fields[key].group_details){
								if(self.currentLayer.fields[key].group_details.desc){
									self.view.groups[self.currentLayer.fields[key].groups].desc=self.currentLayer.fields[key].group_details.desc;
								}
								if(self.currentLayer.fields[key].group_details.title){
									self.view.groups[self.currentLayer.fields[key].groups].title=self.currentLayer.fields[key].group_details.title;
								}
							}
						}else{
							self.view.tools.append(a);
						}
					}
				}
				if(Object.keys(self.view.groups).length>0){
					var group,wrap,primaryField,selected;
					for(var key in self.view.groups){
						self.view.groups[key].groupSelect=$('<select class="rangePkgGroupSelect" data-key="'+key+'">');
						self.view.groups[key].groupSelect.on('change',function(){
							self.view.groups[$(this).data('key')].wrap.find('.fieldPkgOfGroup').removeClass('block');
							self.view.groups[$(this).data('key')].fields[$(this).val()].html.addClass('block');
							self.view.tools.find('.canopyActivate').prop('checked',false);
							self.view.groups[$(this).data('key')].fields[$(this).val()].html.find('.canopyActivate').prop('checked',true);
							self.newFieldChecked(self.view.groups[$(this).data('key')].fields[$(this).val()].html.find('.canopyActivate').data('field'));
						});
						self.view.groups[key].groupSelectLabel=$('<div class="rangePkgGroupSelectLabel">').text('Select Type of Planting Area');
						self.view.groups[key].groupsTop=$('<div class="rangePkgGroupTop cf">')
							.append(self.view.groups[key].groupSelectLabel);
						self.view.groups[key].groupSelectWrap=$('<div class="rangePkgGroupSelectWrap">');
						self.view.groups[key].groupSelectWrap
							.append(self.view.groups[key].groupsTop)
							.append(self.view.groups[key].groupSelect);
						if(self.view.groups[key].desc){
							self.view.groups[key].moreInfo=new MoreInfo({
								content:self.view.groups[key].desc,
								title:self.view.groups[key].title
							});
							self.view.groups[key].groupsTop.append(self.view.groups[key].moreInfo.html);
						}
						self.view.groups[key].pkgsWrap=$('<div class="rangePkgGroupPkgsWrap">');
						group=self.view.groups[key];
						primaryField=Object.keys(group.fields)[0];
						for(var key2 in group.fields){
							selected='';
							if(primaryField==key2){
								selected=' selected="selected"';
								group.fields[key2].html.addClass('block');
							}
							self.view.groups[key].groupSelect.append('<option value="'+key2+'"'+selected+'>'+self.currentLayer.fields[key2].alias+'</option>');
							self.view.groups[key].pkgsWrap.append(group.fields[key2].html);
						}
						self.view.groups[key].wrap=$('<div class="rangePkgGroupWrap">')
							.append(self.view.groups[key].groupSelectWrap)
							.append(self.view.groups[key].pkgsWrap);
						self.view.tools.append(self.view.groups[key].wrap);
					}
				}
			}
			self.view.tools.find('.canopyActivate').on('change',function(){
				self.view.tools.find('.canopyActivate').not($(this)).prop('checked',false);
				self.newFieldChecked($(this).data('field'));
			});
		};
		this.newFieldChecked=function(field){
			self.setCurrentField(field);
			// self.stopWeighting();
			self.updateSymbology();
			self.initLegend();
		};
		this.updatePlanTools=function(){
			self.plan.tools.html('');
			if(self.currentLayer.fields){
				for(var t=0;t<self.currentLayerSortedFieldKeys.length;t++){
					key=self.currentLayerSortedFieldKeys[t];
					if(self.currentLayer.fields[key].in_canopy_tools && self.currentLayer.fields[key].in_plan_tools){
						self.plan.fields[key]=main.utilities.cloneObj(self.currentLayer.fields[key]);
						self.plan.tools.append(self.createSliderPkg(self.currentLayer.fields[key],self.plan.rangeChange,self.plan.rangeChange));
					}
				}
			}
		};
		this.updateGrowTools=function(layer,onRangeChange){
			/* self.plan.tools.html('');
			if(layer.fields){
				for(var key in layer.fields){
					if(layer.fields[key].in_canopy_tools && layer.fields[key].in_grow_tools){
						self.plan.fields[key]=main.utilities.cloneObj(layer.fields[key]);
						self.plan.tools.append(self.createSliderPkg(layer.fields[key],onRangeChange));
					}
				}
			} */
		};
		this.updateInfoTip=function(props){
			var fields=self.currentLayer.fields,field,color,label,fixed,colorValue,value,fieldStats=self.currentLayer.canopy.stats.fields;
			self.infoTipTbody.html('');
			if(self.toolType=='plan'){
				var rank=self.currentLayer.canopy.plan.weightedScore.sortedKeys.indexOf(props.pid.toString())+1;
				var outOf=self.currentLayer.canopy.plan.weightedScore.sortedKeys.length;
				var html=rank+' out of '+outOf;
				self.infoTipTbody.append($('<tr class="infoTipTR">')
					/* .append($('<td>')
						.append($('<div class="tdLiner">').html('Rank')) */
					).append($('<td colspan="2">')
						.append($('<div class="tdLiner infoTipVal center curVal">').html(html))
					).append($('<td colspan="2">')
						.append($('<div class="tdLiner rankTitleWrap">').html('Rank'))
					)
				self.infoTip.container.addClass('planIT');
				self.canopyValueIT.addClass('none')/* .html('Rank').attr('colspan',2) */;
				self.canopyCornerITCell.attr('colspan',2);
				var ifMap=self.currentLayer.properties.internal_field_map,alias="";
				if(ifMap && ifMap.alias && props[ifMap.alias]){
					alias=props[ifMap.alias];
				}
				self.canopyCornerITCell.html(alias);
			}else{
				var currentCanopyStats={};
				var colorGrad=self.currentWeightColorGrad.name;
				for(var t=0;t<self.currentLayerSortedFieldKeys.length;t++){
					key=self.currentLayerSortedFieldKeys[t];
					field=fields[key];
					if(field.in_canopy_tools && field.in_view_tools){
						if(field.inverted_canopy_colors){colorGrad=main.utilities.getInvertedColorType(colorGrad);}
						var cushion=self.defaultStdDev*fieldStats[key].stdDev;
						var max=Math.min(fieldStats[key].max,fieldStats[key].avg+cushion),min=Math.max(fieldStats[key].min,fieldStats[key].avg-cushion);
						colorValue=props[key];
						if(colorValue>max){colorValue=max;}
						if(colorValue<min){colorValue=min;}
						color=main.utilities.getColor(max,min,colorValue,colorGrad);
						label=field.alias;
						if(label.length>35){label=label.substring(0,35);}
						var fixed=Math.min(self.maxToFixed,field.to_fixed);
						value=main.utilities.addCommas(props[key].toFixed(fixed));
						valueAvg=main.utilities.addCommas(fieldStats[key].avg.toFixed(fixed));
						if(field.append_to){
							value=value+field.append_to;
							valueAvg=valueAvg+field.append_to;
						}
						if(field.prepend_to){
							value=field.prepend_to+value.prepend_to;
							valueAvg=field.prepend_to+valueAvg.prepend_to;
						}
						if(self.grow.currentCanopyField && self.grow.currentCanopyField.name==field.name){
							currentCanopyStats.max=max;
							currentCanopyStats.min=min;
							currentCanopyStats.colorGrad=colorGrad;
						}
						self.infoTipTbody.append($('<tr class="infoTipTR '+key+'" data-field="'+key+'">')
							.append($('<td>')
								.append($('<div class="tdLiner">').html(label))
							).append($('<td>')
								.append($('<div class="tdLiner infoTipVal curVal" '+/* style="background-color:rgb('+color[0]+','+color[1]+','+color[2]+');" */'>').html(value))
							)/* .append($('<td>')
								.append($('<div class="tdLiner infoTipVal avgVal">').html(valueAvg))) */
						)
					}
				}
				self.infoTip.container.removeClass('planIT');
				self.canopyValueIT.removeClass('none')/* .html('Value').attr('colspan','') */;
				self.canopyCornerITCell.attr('colspan','');
				var ifMap=self.currentLayer.properties.internal_field_map,alias="";
				if(ifMap && ifMap.alias && props[ifMap.alias]){
					alias=self.currentLayer.fields[ifMap.alias].alias+': '+props[ifMap.alias];
				}
				self.canopyCornerITCell.html(alias);
			}
			//Hypothetical Canopy
			var feature=self.grow.currentGrowOut.features[props.pid];
			if(feature){
				if(currentCanopyStats){
					var hypCanopyColorVal;
					if(hypCanopyColorVal>max){hypCanopyColorVal=currentCanopyStats.max;}
					if(hypCanopyColorVal<min){hypCanopyColorVal=currentCanopyStats.min;}
					var color=main.utilities.getColor(currentCanopyStats.max,currentCanopyStats.min,hypCanopyColorVal,currentCanopyStats.colorGrad);
				}else{
					var color=[0,0,0];
				}
				self.infoTipTbody.append($('<tr class="infoTipTR hypCanopy">')
					.append($('<td>')
						.append($('<div class="tdLiner">').html('Hypothetical Canopy'))
					).append($('<td>')
						.append($('<div class="tdLiner infoTipVal curVal" style="background-color:rgb('+color[0]+','+color[1]+','+color[2]+');">').html((feature.hypCanopy.toFixed(0)+'%')))
					)/* .append($('<td>')
						.append($('<div class="tdLiner infoTipVal avgVal">'))) */
				);
				//trees needed
				var treesNeeded=feature.treesNeeded;
				if(treesNeeded!==null){
					treesNeeded=main.utilities.addCommas(Math.ceil(treesNeeded));
				}else{
					treesNeeded='N/A';
				}
				self.infoTipTbody.append($('<tr class="infoTipTR hypCanopy">')
					.append($('<td>')
						.append($('<div class="tdLiner">').html('Trees needed'))
					).append($('<td>')
						.append($('<div class="tdLiner infoTipVal curVal" style="background-color:rgb('+color[0]+','+color[1]+','+color[2]+');">').html(treesNeeded))
					)
						// .append($('<td>').append($('<div class="tdLiner infoTipVal avgVal">')))
				)
			}
			self.itTitle.html(self.currentLayer.properties.alias);
			self.zoomToBlock.data('block',props.pid);
		};
		this.initInfoTip=function(){
			self.canopyCornerITCell=$('<th class="itBlockLabel itSubLabel">');
			self.canopyValueIT=$('<th class="canopyValueIT itSubLabel">').text('Value');
			self.infoTipTbody=$('<tbody>').addClass('infoTipTbody');
			self.zoomToBlock=$('<div class="itExtra zoomToBlock">').text('Zoom To');
			self.zoomToBlock.on('click',function(){
				var pid=$(this).data('block'),features=self.currentLayer.map_layer.layer.olLayer.getSource().getFeatures(),feature;
				for(var t=0;t<features.length;t++){
					if(features[t].get('pid')==pid){
						feature=features[t];
						break;
					}
				}
				if(feature){
					var extent=feature.getGeometry().getExtent();
					var center=[(extent[0]+extent[2])/2,(extent[1]+extent[3])/2];
					self.infoTip.unFreeze();
					self.map.panTo(center);
					self.map.animateZoom(15);
				}
			});
			self.itExtrasWrap=$('<div class="itExtrasWrap">').append(self.zoomToBlock);
			self.infoTipExtrasTbody=$('<tbody>').addClass('infoTipExtrasTbody');
			self.itTitle=$('<th class="itTitle" colspan="2">').text('Block Values');
			self.itTitleTR=$('<tr>').append(self.itTitle);
			self.infoTipContent=$('<table border="1">').addClass('canopyITContent')
				.append($('<thead>').addClass('canopyITContent')
					.append(self.itTitleTR)
					.append($('<tr class="itHeadLabelRow">')
						.append(self.canopyCornerITCell)
						.append(self.canopyValueIT)
						// .append($('<th>').text('Avg.'))
					)
				)
				.append(self.infoTipTbody)
				.append(self.infoTipExtrasTbody
					.append($('<tr>')
						.append($('<td colspan="2">')
							.append($('<div class="tdLiner">')
								.append($('<div class="tdLiner">')
									.append(self.itExtrasWrap)
								)
							)
						)
					)
				);
			self.infoTip=new InfoTip({
				content:self.infoTipContent,
				map:self.map,
				currentLayer:self.currentLayer,
				onMouseOverFeature:self.updateInfoTip
			});
			self.infoTip.activate();
			// self.updateInfoTip();
		};
		this.update=function(layerName){
			self.setStats(self.layers[layerName]);
			if(!main.withs.with_canopy_plan || main.withs.with_canopy_plan.value){self.updateWeights();}
			self.updateSymbology();
			if(!main.withs.with_canopy_view || main.withs.with_canopy_view.value){self.updateRangePkgs(layerName);}
			self.infoTip.updateCurrentLayer(self.layers[layerName]);
		};
		(this.setLayerReadies=function(){
			for(var key in self.layers){
				self['add_map_layer_'+key]=function(layerName){
					var layer=main.canopy_layers[layerName];
					main.map_layers[layerName].layer=new Layer2({
						id:layerName,
						source:new ol.source.Vector({
							features: (new ol.format.GeoJSON()).readFeatures(layer.map_layer.layerObj)
						}),
						visible:layer.properties.visible || false,
						markerSource:new ol.source.Vector(),
						map:self.map,
						// firstPostRender:self.map.mainLegend.addItem,
						properties:layer.properties,
						olLayerProps:{is_canopy:true}
					});
					// self.map.mainLegend.addItem(main.map_layers[layerName].layer);
					// if(main.filter_legend && main.filter_legend.withOtherLayers){main.filter_legend.addNewOtherLayer(main.map_layers[layerName].layer);}
					self.map.registerLayer(main.map_layers[layerName].layer);
					// self.calcAvgs(layer);
					if(Object.keys(self.grow.layers)[0]==layerName && self.initialStart==self.toolType && self.initialStart=='grow'){
						self.tabClick({name:'grow'});
						self.growLoaded=true;
					}else if(Object.keys(self.plan.layers)[0]==layerName && self.initialStart==self.toolType && self.initialStart=='plan'){
						self.tabClick({name:'plan'});
						self.planLoaded=true;
					}else{
						if(layerName==self.waitingFor){
							self.waitingFor=null;
							self.goLayer(layerName);
						}
					}
				};
			}
			
		})();
		this.checkForCurrentLayer=function(){
			if(self.currentLayer.map_layer.layer){return true;}
		};
		this.currentLayerLoaded=function(){
			var count=0;
			if(main.invents){
				for(var key in main.invents.inventories){
					if(main.invents.inventories[key]){count++;}
				}
			}
			if(!count){self.headerButton.html.click();}
		};
		this.setInitialPrimaries=function(){
			self.setCurrentLayer(self.primaryLayer);
			self.setPrimaryField();
			self.setCurrentField(self.primaryField.name);
			self.setStats(self.primaryLayer);
		};
		(this.start=function(){
			self.tabs=new Tabs({
				tabsMatchWidth:true
			});
			self.initLayerObjects();
			self.setPrimaryLayer();
			self.setInitialPrimaries();
			self.initInfoTip();
			var activeTab,tab;
			self.initialStart=null;
			if(!main.withs.with_canopy_view || main.withs.with_canopy_view.value){
				activeTab=self.tabs.addTab('View',self.createViewHTML(),'view',self.tabClick,'','','','viewTab canopyTabs_view');
				self.toolType='view';
				self.initialStart=self.toolType;
			}
			if(!main.withs.with_canopy_plan || main.withs.with_canopy_plan.value){
				tab=self.tabs.addTab('Plan',self.createPlanHTML(),'plan',self.tabClick,'','','','planTab canopyTabs_plan');
				if(!activeTab){
					activeTab=tab;
					self.toolType='plan';
					self.initialStart=self.toolType;
				}
			}
			if(!main.withs.with_canopy_grow || main.withs.with_canopy_grow.value){
				tab=self.tabs.addTab('Grow',self.createGrowHTML(),'grow',self.tabClick,'','','','growTab canopyTabs_grow');
				if(!activeTab){
					activeTab=tab;
					self.toolType='grow';
					self.initialStart=self.toolType;
				}
			}
			if(activeTab){self.tabs.setActiveTab(activeTab.tabid);}
			
			self.sidePanel.setContent(self.tabs.html);
			if(self.loadOnOpenIfNoInvents && !self.initiallyOpened/*  && Object.keys(main.invents.inventories).length==0 */){
				self.waitToOpen=new Wait(self.checkForCurrentLayer,self.currentLayerLoaded);
			}
			self.loader=new LoaderBox({message:'Loading'});
			self.loader.html.addClass('flLoader');
			self.sidePanel.container.append(self.loader.html);
			// self.toolTip=new ToolTip({});
			// self.rasters=new Rasters(); 	
			// self.existingCanopy=new ExistingCanopy(); 
		})();
		this.unFreezeCheck=function(){
			if(self.infoTip.frozen){self.infoTip.unFreeze();}
		};
		(this.initEvents=function(){
			self.map.map.on('pointerdrag',self.unFreezeCheck);
			self.map.map.on('moveend',self.unFreezeCheck);
		})();
		var layers={};
		for(var key in self.layers){
			layers[key]=self.layers[key].map_layer;
			
		}
		this.waitLoad=new WaitLoad({
			layers:layers,
			context:self
		});
		// setTimeout(function(){self.tabClick({name:'plan'});},5000);
		// self.tabClick({name:'plan'});
	};
});