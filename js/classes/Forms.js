define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Form=require('./Form');
		this.alias='Forms';
		this.name='forms';
		this.groups={};
		this.forms=main.forms;
		this.forms0={};
		this.auto_load_last_fields={};
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.clearForms=function(){
			for(var key in self.forms0){
				if(self.forms0[key]){
					self.forms0[key].clearForm();
				}
			}
		};
		this.preHTMLAdd=function(){
			var buttonAlias=main.withs.with_forms.alias || self.alias;
			var buttonImg=main.mainPath+'images/forms.png';
			if(main.withs.with_forms.icon_path){buttonImg='images/'+main.withs.with_forms.icon_path}
			main.utilities.addLayoutItems(self.name,buttonAlias,self,buttonImg);
		};
		this.addForm=function(form){
			self.forms0[form.properties.name]=new Form({
				fields:form.fields,
				properties:form.properties,
				forms:self
			});
		};
		this.addForms=function(){
			//add forms to forms accordian
			var form,currentGroup,groups,thisGroup,accordian,shelf,alias,setGroup,tooltip;
			self.accordian.removeAllShelves();
			self.groups={};
			self.groups['mainGenForms']={
				accordian:self.accordian,
				shelves:{}
			}
			for(var key in self.forms){
				form=self.forms[key];
				if(form && form.properties.active && (!main.account || form.properties[main.account.loggedInUser.user_type+'_visible']) && form.properties.add_on_start){
					self.addForm(form);
				}
			}
			/* if(setGroups){
				var keys=Object.keys(setGroups);
				keys=keys.sort(function(a,b){
					if(setGroups[a].place_in_order>setGroups[b].place_in_order){return 1;}
					if(setGroups[a].place_in_order<setGroups[b].place_in_order){return -1;}
					return 0;
				});
				var shelf,acordian;
				for(var i=0;i<keys.length;i++){
					shelf=self.accordian.container.find('.shelf.'+keys[i]);
					acordian=shelf.closest('.accordian');
					acordian.append(shelf);
				}
			} */
		};
		if(main.withs.with_forms.value){
			self.preHTMLAdd();
			self.addForms();
		}
	};
});