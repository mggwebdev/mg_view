define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Accordian=require('./Accordian');
		var Checkbox=require('./Checkbox');
		var Tabs=require('./Tabs');
		var Panel=require('./Panel');
		var ViewMore=require('./ViewMore');

		this.alias='Stats';
		this.name='dashboard';

		this.defaultInventory='trees';
		this.inventories=Object.keys(main.inventories);
		this.layersLoaded=[];
		this.defaultColorGrad='Green-Red';
		this.characterSortType='abc';
		this.default_number_breaks=5;
		this.default_to_fixed=2;
		this.canvasSize=160;
		this.nothingAlias='Not Specified';
		this.defaultChartTypes=['pie','bar'];
		this.active=false;
		this.largeDataSet=main.personal_settings.app_settings.large_dataset;
		this.timeStamp=main.personal_settings.app_settings.dashboard_last_refresh;
		this.lookups={};
		var tables=[];
			
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);

		this.ecobensCategories={
			'overall':{
				name:'overall',
				alias:'Overall',
				place_in_order:1,
				img:'carbon_storaged.png'
			},
			'stormwater':{
				name:'stormwater',
				alias:'Stormwater Management',
				place_in_order:2,
				img:'stormwater.png'
			},
			'property':{
				name:'property',
				alias:'Property Value',
				place_in_order:3,
				img:'property_value.png'
			},
			'energy':{
				name:'energy',
				alias:'Energy Conservation',
				place_in_order:4,
				img:'energy.png'
			},
			'air_quality':{
				name:'air_quality',
				alias:'Air Quality',
				place_in_order:5,
				img:'air_quality.png'
			},
			'carbon':{
				name:'carbon',
				alias:'Carbon',
				place_in_order:6,
				img:'carbon_sequestered.png'
			}
		};
		this.ecobensKeys=Object.keys(this.ecobensCategories).sort(function(a,b){
			if(self.ecobensCategories[a].place_in_order>self.ecobensCategories[b].place_in_order){return 1;}
			if(self.ecobensCategories[a].place_in_order>self.ecobensCategories[b].place_in_order){return -1;}
			return 0;
		});
		
		this.refresh=function(){
				self.refreshAllCharts();
				self.refreshByNum();
                if( main.account.loggedInUserType != 'admin_1') { 
                    $('#openCreateCustomPanel').hide();
                } else {
                    $('#openCreateCustomPanel').show();
                }
			if(main.withs.with_ecobens.value){
				self.updateEcoBen();
			}
		}
		this.refreshResize=function(){//only update charts when the sidepanel is resized
				self.refreshAllCharts();
		}
		
		this.setCharts=function(){
			var charts=main.personal_settings.charts;
			if(!charts){return;}
			var chartKeys=Object.keys(charts);
			
			var sortedKeys=chartKeys.sort(function(a,b){
				if(charts[a].place_in_order>charts[b].place_in_order){return 1;}
				if(charts[a].place_in_order<charts[b].place_in_order){return -1;}
				return 0;
			});
			self.summaries=[];
			self.countNums=[];
			self.charts=[];
			self.chartObs={};
			self.summariesGroups=[];
			self.countNumsGruops=[];
			self.chartsGroups=[];
			for(var i=0;i<sortedKeys.length;i++){
				if(charts[sortedKeys[i]].category=='summary'){
					if(charts[sortedKeys[i]].theme_group & $.inArray(charts[sortedKeys[i]].theme_group,self.summariesGroups)<0){ // if using groupby and catagory not in array
						self.summariesGroups.push(charts[sortedKeys[i]].theme_group);
					}
					self.summaries.push(charts[sortedKeys[i]]);
				}else if(charts[sortedKeys[i]].category=='count_num'){
					if(charts[sortedKeys[i]].theme_group & $.inArray(charts[sortedKeys[i]].theme_group,self.countNumsGroups)<0){ // if using groupby and catagory not in array
						self.countNumsGroups.push(charts[sortedKeys[i]].theme_group);
					}
					self.countNums.push(charts[sortedKeys[i]]);
				}else{
					temp=$.inArray(charts[sortedKeys[i]].theme_group,self.chartsGroups);
					if( $.inArray(charts[sortedKeys[i]].theme_group,self.chartsGroups)<0){ // if using groupby and catagory not in array
						self.chartsGroups.push(charts[sortedKeys[i]].theme_group);
					}
					self.charts.push(charts[sortedKeys[i]]);
                    self.chartObs[charts[sortedKeys[i]].name] = charts[sortedKeys[i]];
				}
			}
		};
        //theme groups divs 
		this.createChartGroups=function(){
			self.chartsInfo={items:{}};
            self.dashChartsAccordian=new Accordian('afFields','abc');
			self.chartsLabel=$('<div id="chartTitle" class="summaryPanelLabel">').text(main.personal_settings.app_settings.default_dashboard_title);
			self.chartsInfo.dbChartsChartsWrap=$('<div class="dbChartsChartsWrap">')
				.append(self.chartsLabel)
				.append(self.dashChartsAccordian.container);
			for (var group in self.chartsGroups){
 				var groupDivId = self.chartsGroups[group].replace(/\s/g, "");//remove all white space
				groupDiv = $('<div id="'+groupDivId+'" class="dbChartGroup">');
			
                self.shelves[groupDivId]={alias:self.chartsGroups[group]};
                self.shelves[groupDivId].launchButton=new Button('toolBox','',self.shelves[groupDivId].alias);

                self.shelves[groupDivId].shelfContent=groupDiv;
                self.shelves[groupDivId].shelf=self.dashChartsAccordian.createShelf({
                    name:groupDivId,
                    button:self.shelves[groupDivId].launchButton.html,
                    content:self.shelves[groupDivId].shelfContent,
                    alias:self.shelves[groupDivId].alias,
                    extended:true,
                    onOpen:self.chartsAndGraphsOpened
                });
                // self.chartsInfo.dbChartsChartsWrap.append(self.shelves[groupDivId].shelf);
			}
			return self.chartsInfo.dbChartsChartsWrap;
		};
        this.clearThemeGroups = function() {	
			for (var group in self.chartsGroups){
 				var groupDivId = self.chartsGroups[group].replace(/\s/g, "");//remove all white space
 				$('#'+groupDivId).empty(); 
            }
        };
		this.hideSettingsOnOutClick=function(e){
			if($(e.target).closest('.dbChartsSettingsWrap').length==0){
				$('#'+e.data+'Settings').removeClass('block');
				$('html').off('click',self.hideSettingsOnOutClick);
			}
		};
		this.createCharts=function(){
            //empty all groups so the charts chn all be rebuilt
            this.clearThemeGroups();
			for (var chart in self.charts){
				if(!main.inventories[self.charts[chart].inventory]){continue;}
				if(self.charts[chart].chart_types==null){
					self.charts[chart].chart_types=self.defaultChartTypes;
				}
				chartUnit=$('<div id="'+self.charts[chart].name+'_unit" class="dbChartUnit">');
				chartTitle=$('<div id="'+self.charts[chart].name+'_title" class="dbChartsTitle">').html('<b>'+self.charts[chart].alias+'</b>');
				
				tabContsWrap=$('<div class="tabsContsWrap dbTabs">');
				
				tabWrap=$('<div class="tabsWrap">');
				if($.inArray("pie",self.charts[chart].chart_types)>-1){
					var pietab=$('<div class="tab noSelect activeTab">')
                            .attr('id',"pie"+self.charts[chart].name)
                            .html("Pie")
                            .on('click',function(){
                                self.refreshChart(self.chartObs[this.id.substring(3)],"pie");
                                $('#'+this.id).addClass("activeTab");
                                if($('#bar'+this.id.substring(3)).length){$('#bar'+this.id.substring(3)).removeClass("activeTab")};
                                if($('#tabular'+this.id.substring(3)).length){$('#tabular'+this.id.substring(3)).removeClass("activeTab")};
                                if($('#stacked'+this.id.substring(3)).length){$('#stacked'+this.id.substring(3)).removeClass("activeTab")};
                            });
                    pietab.appendTo(tabWrap);
				} 
				if($.inArray("bar",self.charts[chart].chart_types)>-1){
				    var bartab=$('<div class="tab noSelect activeTab">')
                        .attr('id',"bar"+self.charts[chart].name)
                        .html("Bar")
                        .on('click',function(){
                                self.refreshChart(self.chartObs[this.id.substring(3)],"bar");
                                $('#'+this.id).addClass("activeTab");
                                if($('#pie'+this.id.substring(3)).length){$('#pie'+this.id.substring(3)).removeClass("activeTab")};
                                if($('#tabular'+this.id.substring(3)).length){$('#tabular'+this.id.substring(3)).removeClass("activeTab")};
                                if($('#stacked'+this.id.substring(3)).length){$('#stacked'+this.id.substring(3)).removeClass("activeTab")};
					    });
					if($.inArray("pie",self.charts[chart].chart_types)>-1){
                        bartab.removeClass('activeTab');   
					}
					
					bartab.appendTo(tabWrap);
				}
                if($.inArray("stacked",self.charts[chart].chart_types)>-1){
					var stackedtab=$('<div class="tab noSelect activeTab">')
                            .attr('id',"stacked"+self.charts[chart].name)
                            .html("Stacked")
                            .on('click',function(){
                                self.refreshChart(self.chartObs[this.id.substring(7)],"stacked");
                                $('#'+this.id).addClass("activeTab");
                                if($('#tabular'+this.id.substring(7)).length){$('#tabular'+this.id.substring(7)).removeClass("activeTab")};
                                if($('#pie'+this.id.substring(7)).length){$('#pie'+this.id.substring(7)).removeClass("activeTab")};
                                if($('#bar'+this.id.substring(7)).length){$('#bar'+this.id.substring(7)).removeClass("activeTab")};
                            });
						if( $.inArray("pie",self.charts[chart].chart_types)>-1 || 
                            $.inArray("bar",self.charts[chart].chart_types)>-1){
                                stackedtab.removeClass("activeTab");
					    }
						stackedtab.appendTo(tabWrap);
				}
                if($.inArray("tabular",self.charts[chart].chart_types)>-1){
					var tabulartab=$('<div class="tab noSelect activeTab">')
                            .attr('id',"tabular"+self.charts[chart].name)
                            .html("Tabular")
                            .on('click',function(){
                                self.refreshChart(self.chartObs[this.id.substring(7)],"tabular");
                                $('#'+this.id).addClass("activeTab");
                                if($('#bar'+this.id.substring(7)).length){$('#bar'+this.id.substring(7)).removeClass("activeTab")};
                                if($('#stacked'+this.id.substring(7)).length){$('#stacked'+this.id.substring(7)).removeClass("activeTab")};
                                if($('#pie'+this.id.substring(7)).length){$('#pie'+this.id.substring(7)).removeClass("activeTab")};
                            });
                    if( $.inArray("pie",self.charts[chart].chart_types)>-1 || 
                        $.inArray("bar",self.charts[chart].chart_types)>-1 ){
                            tabulartab.removeClass("activeTab");
                    }
                    tabulartab.appendTo(tabWrap);
				}
				tabContentWrap=$('<div class="tabsContentWrap ">');
				tabContent=$('<div  class="tabContent block">');
				chartObj=$('<div id="'+self.charts[chart].name+'" class="dbTab">').appendTo(tabContent);
				
				dbChartsSettingsButton=new Button('icon','dbChartsSettingsButton '+self.charts[chart].name,'',[['name',self.charts[chart].name]],null,main.mainPath+'images/wrench.png','Options');
				dbChartsSettingsButton.html.on('click',function(){
					$('#'+this.classList[this.classList.length-1]+'Settings').addClass('block');
					$('html').on('click',null,this.classList[this.classList.length-1],self.hideSettingsOnOutClick);
				});
				dbChartsSettingsList=$('<ul class="dbChartsSettingsList">');
				dbChartsSettings=$('<div id="'+self.charts[chart].name+'Settings" class="dbChartsSettings" >')
				.append(dbChartsSettingsList);
				dbChartsSettingsWrap=$('<div class="dbChartsSettingsWrap">')
					.append(dbChartsSettingsButton.html)
					.append(dbChartsSettings);
				
			
				tabWrap.appendTo(tabContsWrap);
				tabContent.appendTo(tabContentWrap);
				tabContentWrap.appendTo(tabContsWrap);
				chartTitle.appendTo(chartUnit);
				tabContsWrap.appendTo(chartUnit);
				dbChartsSettingsWrap.appendTo(chartUnit);

                //append the chart to the theme group div
                var id = self.charts[chart].theme_group.replace(/\s/g, "");
				chartUnit.appendTo($("#"+id));
                    
				dbChartSave=$('<li class="dbChartsSettingsListLI dbChartSave" data-chartname="'+self.charts[chart].name+'" data-chartpid="'+self.charts[chart].pid+'" data-chartidx="'+chart+'">').text("Quick Save");
				dbChartSave.on('click',function(e){
			
                    var name = $(e.target).data('chartname');
                    var chartIdx = $(e.target).data('chartidx');
                    var chartObject = self.charts[chartIdx];
                    
                    var chartAliasTitle = chartObject.alias;
                    var typeToPrint = $('#'+name+'_unit').find('.activeTab').text();
    
                    if(typeToPrint == 'Tabular') {

                        var $html = $('#'+name+'_unit').find('.dashTabular').parent().clone();
    
                        $title = $('<strong>').text(chartAliasTitle);
                        $titleDiv = $('<div>').append($title);
                        $titleDiv.css({'text-align':'center'});
                        $html.prepend($titleDiv);

                        var html = $html.get(0);
                        html = html.outerHTML;

                        var win = window.open('', 'quickSaveWindow');
                        win.document.write('<html><head><title>Quick Save</title><link rel="stylesheet" type="text/css" href="../main/css/main.css"></head>');
                        win.document.write('<body>');
                        win.document.write('</body>');
			            $(win.document.body).html(html);

                    } else {
 
                        var imgData = $('#'+name).jqplotToImageStr({});

                        //self.chartsInfo.items[$(this).data('chartname')][activeTab.params.type].canvas;
                        var fileTypeName='PNG';
                        var fileType=main.print.params.file_type.options[fileTypeName];
                        var fileName='chart';
                        main.print.printChartImg(fileType,imgData,fileName); 
                        $('#'+name+'Settings').removeClass('block');
                        $('html').off('click',self.hideSettingsOnOutClick);

                    }
				});
                dbChartsSettingsList.append(dbChartSave);


                //add Print button to chart 
                var dbChartPrint=$('<li class="dbChartsSettingsListLI dbChartPrint" data-chartname="'+self.charts[chart].name+'" data-chartnumber="'+chart+'">').text('Print');
				dbChartsSettingsList.append(dbChartPrint);
				dbChartPrint.on('click',function(e) {	
                    self.printSingleChart( e );
                });

                if( self.charts[chart].is_added_from_app &&
                    self.charts[chart][main.account.loggedInUserType+'_visible']){
                    var dbCustomChartDelete=$('<li class="dbChartsSettingsListLI dbCustomChartDelete" data-chartname="'+self.charts[chart].name+'" data-chartpid="'+self.charts[chart].pid+'" data-chartidx="'+chart+'">').text('Delete');
                    dbChartsSettingsList.append(dbCustomChartDelete);
                    dbCustomChartDelete.on('click',function(e) {	
                        self.deleteCustomChart( e );
                    });
                }
			}
			
		};
        // expects button event that contains chart data
        this.printSingleChart = function(e){
            var name = $(e.target).data('chartname');
            var chartNum = $(e.target).data('chartnumber');
            var chartObject = self.charts[chartNum];
            
            var chartAliasTitle = chartObject.alias;
        
            var stackedField2 = main.tables[chartObject.inventory].map_layer.fields[chartObject.group_by['field2']] || null;

            var notReporting = $('#'+name+'_title').data('notreporting');
            chartObject.notReporting = notReporting;
                        
            //send data to print page, in new window
            //  pass current advFilter, and chart info
            // - open window, give a name
            window.open('', 'reportWindow');

            var chartTypes = chartObject.chart_types || self.defaultChartTypes; 

            // TODO - factor out 
            var additionalwhere='';
            var advFilter = {};
            advFilter.filteredByInternalMap = ''; 
            advFilter.filteredByAlias = '';
            advFilter.filtererIvtry = '';
            advFilter.advWhere = '';
            advFilter.advWhereAlias = '';
            advFilter.advLayer = '';
            advFilter.advDepend = false;
            advFilter.advDependLayer = '';
            advFilter.advDependField = '';
            advFilter.advDependAlias = '';
            advFilter.advGeo = '';

            if(main.invents.currentFilterPID && main.invents.currentFiltered && !main.account.loggedInUser.dont_filter){
                if (main.invents.currentFiltered==chartObject.inventory){
                    advFilter.filtererIvtry=main.invents.inventories[main.invents.currentFilterLayer];
                    advFilter.filteredByInternalMap=advFilter.filtererIvtry.inventory.properties.internal_field_map;
                    advFilter.filteredByAlias=advFilter.filtererIvtry.inventory.map_layer.features[main.invents.currentFilterPID].properties[advFilter.filteredByInternalMap.alias_field];
                    additionalwhere= main.invents.currentFilterLayer+"='"+advFilter.filteredByAlias+"'";	
                }	
            }
            if(main.advFilter.where || main.advFilter.geoFilter){
                if(main.advFilter.where){advFilter.advWhere=main.advFilter.where;}
                if(main.advFilter.whereAlias){advFilter.advWhereAlias=main.advFilter.whereAlias;}
                advFilter.advLayer=main.advFilter.currentLayer.properties.name;
                if(main.advFilter.geoFilter){advFilter.advGeo=main.advFilter.geoFilter.join(',');}
                if(chartObject.inventory!=advFilter.advLayer){// Chart layer not currently filtered
                    if(main.inventories[chartObject.inventory].properties.dependent_upon){// chart layer has a dependency
                        if(main.inventories[chartObject.inventory].properties.dependent_upon==advFilter.advLayer){//chart layer is dependent on filtered layer
                            advFilter.advDepend=true;
                            advFilter.advDependField=Object.keys(main.inventories[chartObject.inventory].properties.field_map[advFilter.advLayer].fields)[0];
                            advFilter.advDependLayer=chartObject.inventory;
                        }
                        else if(main.inventories[advFilter.advLayer].properties.dependent_upon==chartObject.inventory){//filtered layer is dependent on chart layer
                            advFilter.advDepend=true;	
                            advFilter.advDependField=Object.keys(main.inventories[advFilter.advLayer].properties.field_map[advFilter.advLayer].fields)[0];
                            advFilter.advDependLayer=advFilter.advLayer;
                        }
                        else{
                            //do nothing
                        }
                    }
                }
            }

            var field1Alias, field2Alias;
            field1Alias = main.data.fields[chartObject.inventory][chartObject.group_by['field1']].alias;
            if( chartObject.group_by['field2'] ) {
                field2Alias = main.data.fields[chartObject.inventory][chartObject.group_by['field2']].alias;
            }
            chartObject.field1Alias =    field1Alias;
            chartObject.field2Alias =    field2Alias;

            var params={
                chartName:      name,
                chartAliasTitle:      chartAliasTitle,
                groupby:        chartObject.group_by,
                table:          chartObject.alias_table,
                sort:           chartObject.sort_by,
                sortdir:        chartObject.sort_dir,
                limit:          chartObject.val_limit,
                chartwhere:     chartObject.limit_where,
                additionalwhere: additionalwhere,
                advWhere:       advFilter.advWhere,
                advWhereAlias:  advFilter.advWhereAlias,
                advLayer:       advFilter.advLayer,
                advDepend:      advFilter.advDepend,
                advDependLayer: advFilter.advDependLayer,
                advDependField: advFilter.advDependField,
                advGeo:         advFilter.advGeo,
                inventory:      chartObject.inventory,
                stackedField2:  stackedField2,
                folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]
            };

            var form = $('<form/>').attr({
                id: 'formDbReport',
                method: 'post',
                action: '../main/server/dashReport.php',
                target: 'reportWindow',
            });
            var x = $.param(params);
        
            var currentFilterDisplayWhere = (main.advFilter && main.advFilter.displayWhere) 
                    ? main.advFilter.displayWhere : '';
            var currentGeoFilter = (main.advFilter.geoFilter ) 
                    ? main.advFilter.geoFilter.length : '';

            form.append($('<input/>').attr({name:'currentGeoFilter',value: currentGeoFilter}));                   
            form.append($('<input/>').attr({name:'currentFilterDisplayWhere',value: currentFilterDisplayWhere}));                   
            form.append($('<input/>').attr({name:'params',value: $.param(params) }));                   
            form.append($('<input/>').attr({name:'chartTypes',value: chartTypes }));                   
            form.append($('<input/>').attr({name:'chartData',value: $.param(chartObject) }));                   
            form.append($('<input/>').attr({name:'notReporting',value: notReporting }));                   

            $('body').append(form);
            form.submit();
            $('#formDbReport').remove();

        }		
		this.refreshAllCharts=function(){
			if(self.accordian.shelves[self.shelves['chartsAndGraphs'].shelf.data('name')].opened && self.charts && self.charts.length){
				for (var group in self.chartsGroups){
					$('.'+self.chartsGroups[group].replace(/\s/g, "")).removeClass('none');
				}
				$('#chartTitle').html('');
				if(main.invents.currentFiltered && main.invents.currentFilterPID){
					var filtererIvtry=main.invents.inventories[main.invents.currentFilterLayer];
					var filteredByInternalMap=filtererIvtry.inventory.properties.internal_field_map;
					var filteredByAlias=filtererIvtry.inventory.map_layer.features[main.invents.currentFilterPID].properties[filteredByInternalMap.alias_field];
					
					$('#chartTitle').html('Charts Filtered by: '+filteredByAlias);
				}
				else{
					$('#chartTitle').html(main.personal_settings.app_settings.default_dashboard_title);	
				}	
				
				for(var i = 0; i < self.charts.length; i++ ){
					if(!main.inventories[self.charts[i].inventory]){continue;}
			        if(!self.charts[i][main.account.loggedInUserType+'_visible']){
					    $('#'+self.charts[i]['name']+'_unit').addClass('none');
                    }
                    if( self.charts[i][main.account.loggedInUserType+'_visible'] ) {

                        //get active chart and pass to refresh
                        var themeGroup = self.charts[i].theme_group;
                        //console.log(themeGroup);
                        if(self.dashChartsAccordian.shelves[self.shelves[self.charts[i].theme_group.replace(/\s/g, "")].shelf.data('name')].opened){
                            if($('#'+"pie"+self.charts[i].name).length && $('#'+"pie"+self.charts[i].name).hasClass("activeTab") && ($('#pie'+self.charts[i].name).offset().top > 0) ) {
                                self.refreshChart(self.charts[i],"pie");
                                
                            }
                            else if($('#'+"bar"+self.charts[i].name).length && $('#'+"bar"+self.charts[i].name).hasClass("activeTab") && ($('#bar'+self.charts[i].name).offset().top > 0) ){
                                self.refreshChart(self.charts[i],"bar");
                            }
                            else if($('#'+"tabular"+self.charts[i].name).length && $('#'+"tabular"+self.charts[i].name).hasClass("activeTab") && ($('#tabular'+self.charts[i].name).offset().top > 0) ){

                                self.refreshChart(self.charts[i],"tabular");
                            }
                            else{
                                self.refreshChart(self.charts[i],"stacked");
                            }
                        }
                    }
				}
				/*for (var group in self.chartsGroups){
					hide=true;
					for(var i = 0; i < self.charts.length && hide; i++ ){
						if(self.charts[i].theme_group==self.chartsGroups[group]){
							if( self.charts[i][main.account.loggedInUserType+'_visible']){hide=false;}
						}
					}
					if(hide){
						$('.'+self.chartsGroups[group].replace(/\s/g, "")).addClass('none');
					}
				}*/
			}
		};

		this.refreshByNum=function(){
			if(self.accordian.shelves[self.shelves['byNumbers'].shelf.data('name')].opened){
				$('#summaryTitle').html('');
				if(main.invents.currentFiltered && main.invents.currentFilterPID){
					var filtererIvtry=main.invents.inventories[main.invents.currentFilterLayer];
					var filteredByInternalMap=filtererIvtry.inventory.properties.internal_field_map;
					var filteredByAlias=filtererIvtry.inventory.map_layer.features[main.invents.currentFilterPID].properties[filteredByInternalMap.alias_field];
					
					$('#summaryTitle').html('Summaries Filtered by: '+filteredByAlias);
				}
				else{
					$('#summaryTitle').html(main.personal_settings.app_settings.default_dashboard_title);	
				}
				//self.summaryPLargeSums.html('');
				//for(var i = 0; i < self.summaries.length; i++ ){
					//get active chart and pass to refresh
					self.refreshSum(self.summaries[i]);
				//}
				self.summaryCountNums.html('');
				for(var i = 0; i < self.countNums.length; i++ ){
					//get active chart and pass to refresh
					self.refreshCountNum(self.countNums[i]);
				}
			}
		};
		this.getCountNum=function(countNum, val){
			additionalwhere='';
				if(main.invents.currentFiltered && main.invents.currentFilterPID && !main.account.loggedInUser.dont_filter){
					if (main.invents.currentFiltered==countNum.inventory){
						var filtererIvtry=main.invents.inventories[main.invents.currentFilterLayer];
						var filteredByInternalMap=filtererIvtry.inventory.properties.internal_field_map;
						var filteredByAlias=filtererIvtry.inventory.map_layer.features[main.invents.currentFilterPID].properties[filteredByInternalMap.alias_field];
						additionalwhere= Object.keys(main.inventories[main.invents.currentFiltered].properties.lookup_table_map[main.invents.currentFilterLayer])[0]+"='"+filteredByAlias+"'";
					}
				}
				var advWhere='';
				var advLayer='';
				var advWhereAlias='';
				var advDepend=false;
				var advDependField='';
				var advDependLayer='';
				var advGeo='';
				if(main.advFilter.where || main.advFilter.geoFilter){
					if(main.advFilter.where){advWhere=main.advFilter.where;}
					if(main.advFilter.whereAlias){advWhereAlias=main.advFilter.whereAlias;}
					advLayer=main.advFilter.currentLayer.properties.name;
					if(main.advFilter.geoFilter){advGeo=main.advFilter.geoFilter.join(',');}
					if(countNum.inventory!=advLayer){// countNum layer not currently filtered
						if(main.inventories[countNum.inventory].properties.dependent_upon){// countNum layer has a dependency
							if(main.inventories[countNum.inventory].properties.dependent_upon==advLayer){//countNum layer is dependent on filtered layer
								advDepend=true;
								advDependField=Object.keys(main.inventories[countNum.inventory].properties.field_map[advLayer].fields)[0];
								advDependLayer=countNum.inventory;
							}
							else if(main.inventories[advLayer].properties.dependent_upon==countNum.inventory){//filtered layer is dependent on countNum layer
								advDepend=true;	
								advDependField=Object.keys(main.inventories[advLayer].properties.field_map[advLayer].fields)[0];
								advDependLayer=advLayer;
							}
							else{
								//do nothing
							}
						}
					}
				}
				field=main.tables[countNum.inventory].map_layer.fields[countNum.group_by['field1']];
				var fieldPkg={
					data_type:field.data_type
				};
			var params={
								groupby :countNum.group_by,
								inventory :countNum.inventory,
								table:countNum.alias_table,
								val:val,
								fieldPkg:fieldPkg,
								additionalwhere: additionalwhere,
								advWhere:advWhere,
								advWhereAlias:advWhereAlias,
								advLayer:advLayer,
								advDepend:advDepend,
								advDependLayer:advDependLayer,
								advDependField:advDependField,
								advGeo:advGeo,
								folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]
							}

							$.post('../main/server/db.php',{
									action : 'getCountNum',
									params : params
							
							}).done(function(data) { 
							var jData = JSON.parse(data);

						if (val=="Choose Value"){
								$('#'+countNum.name+ 'Total').html('');
								$('#'+countNum.name+ 'Perc').html('');
							}
							else{
								$('#'+countNum.name+ 'Total').html(main.utilities.addCommas(jData[0]['count']));
								$('#'+countNum.name+ 'Perc').html(main.utilities.addCommas('('+jData[0]['percent']+'%)'));
							}
			});
			
		};
		this.refreshCountNum=function(countNum){
			if(!countNum[main.account.loggedInUserType+'_visible']){
					//do nothing 
				 }
				else{
					
			valSelect=$('<select id='+countNum.name+ ' class="countNumValSelect">');
			countNumTotal=$('<div id='+countNum.name+ 'Total class="countNumTotal countNumEle">');
			countNumTotalPerc=$('<div id='+countNum.name+ 'Perc class="countNumTotalPerc countNumEle">');
			valSelect.on('change',function(){
							if($(this).val()){
								val=$(this).val();
								self.getCountNum(countNum,val);
							}
						});
								
			valSelect.append('<option value="">Choose Value</option>');
			valSelect.append('<option value="_not_specified">Not Specified</option>');	
			
			field=main.tables[countNum.inventory].map_layer.fields[countNum.group_by['field1']];			
			if (field.options){//options	
				for(var i in field.options){
					valSelect.append('<option value="'+field.options[i]['alias']+'">'+field.options[i]['alias']+'</option>');
				}
			}
			else if (field.lookup_table){ //	lookup	
			var values =self.lookups[countNum.group_by['field1']].values;
				for(var i in values){
					valSelect.append('<option value="'+values[i]+'">'+values[i]+'</option>');
				}			
			}
			else{
				//Do Nothing 
			}
			unit=$('<div class="countNumUnit">')
				.append($('<div class="countNumInner cf">')
				.append($('<div class="countNumLabel countNumEle">').html(countNum.alias))
				.append($('<div class="countNumValSelectWrap countNumEle">').html(valSelect))
				.append(countNumTotal)
				.append(countNumTotalPerc)
			);
			self.summaryCountNums.append(unit);
		}
		};
		this.refreshSum = function() {
			var sum, pkg = {};
			for (var i = 0; i < self.summaries.length; i++) {
				sum = self.summaries[i]
				if (!sum[main.account.loggedInUserType + '_visible']) {
					continue;
				} else {

					additionalwhere = '';
					if (main.invents.currentFiltered && main.invents.currentFilterPID && !main.account.loggedInUser.dont_filter) {
						if (main.invents.currentFiltered == sum.inventory) {
							var filtererIvtry = main.invents.inventories[main.invents.currentFilterLayer];
							var filteredByInternalMap = filtererIvtry.inventory.properties.internal_field_map;
							var filteredByAlias = filtererIvtry.inventory.map_layer.features[main.invents.currentFilterPID].properties[filteredByInternalMap.alias_field];
							additionalwhere = Object.keys(main.inventories[main.invents.currentFiltered].properties.lookup_table_map[main.invents.currentFilterLayer])[0] + "='" + filteredByAlias + "'";
						}
					}
					var advWhere = '';
					var advLayer = '';
					var advWhereAlias = '';
					var advDepend = false;
					var advDependField = '';
					var advDependLayer = '';
					var advGeo = '';
					if (main.advFilter.where || main.advFilter.geoFilter) {
						if (main.advFilter.where) {
							advWhere = main.advFilter.where;
						}
						if (main.advFilter.whereAlias) {
							advWhereAlias = main.advFilter.whereAlias;
						}
						advLayer = main.advFilter.currentLayer.properties.name;
						if (main.advFilter.geoFilter) {
							advGeo = main.advFilter.geoFilter.join(',');
						}
						if (sum.inventory != advLayer) { // sum layer not currently filtered
							if (main.inventories[sum.inventory].properties.dependent_upon) { // sum layer has a dependency
								if (main.inventories[sum.inventory].properties.dependent_upon == advLayer) { //sum layer is dependent on filtered layer
									advDepend = true;
									advDependField = Object.keys(main.inventories[sum.inventory].properties.field_map[advLayer].fields)[0];
									advDependLayer = sum.inventory;
								} else if (main.inventories[advLayer].properties.dependent_upon == sum.inventory) { //filtered layer is dependent on sum layer
									advDepend = true;
									advDependField = Object.keys(main.inventories[advLayer].properties.field_map[advLayer].fields)[0];
									advDependLayer = advLayer;
								} else {
									//do nothing
								}
							}
						}
					}
					if (main.filter_legend && main.filter_legend.active && !main.filter_legend.hideOnly && main.filter_legend.filterWhere && main.filter_legend.layerName == sum.inventory) {
						if (advWhere) {
							advWhere += ' AND ';
						}
						advWhere += '(' + main.filter_legend.filterWhere + ')';
					}
					//test for bulk counting
					var bulkCountLayer = null;
					var bulkCountField = null;
					if (main.invents && main.invents.bulkCounts && main.invents.bulkCounts.length) {
						for (var y = 0; y < main.invents.bulkCounts.length; y++) {
							if (main.invents.bulkCounts[y].countedLayer == sum.inventory) {
								bulkCountLayer = main.invents.bulkCounts[y].countLayer;
								bulkCountField = main.invents.bulkCounts[y].countField;
							}
						}
					}
					pkg[sum.name] = {
						name: sum.name,
						groupby: sum.group_by,
						table: sum.alias_table,
						bulkCountLayer: bulkCountLayer,
						bulkCountField: bulkCountField,
						chartwhere: sum.limit_where,
						additionalwhere: additionalwhere,
						type: sum.type,
						summaryIndex: 1,
						advWhere: advWhere,
						advWhereAlias: advWhereAlias,
						advLayer: advLayer,
						advDepend: advDepend,
						advDependLayer: advDependLayer,
						advDependField: advDependField,
						advGeo: advGeo,
						folder: window.location.pathname.split("/")[window.location.pathname.split("/").length - 2]

					}
				}
			}
			$.post('../main/server/db.php', {
				action: 'getsum',
				params: {
					pkg: pkg
				}
			}).done(function(data) {
				var results = JSON.parse(data),
					jData;
				self.summaryPLargeSums.html('');
				for (var b = 0; b < self.summaries.length; b++) {
					sum = self.summaries[b];
					if (!sum[main.account.loggedInUserType + '_visible']) {
						continue;
					} else {
						jData = results[sum.name];
						// if(!jData[0]['total_sum']){var dataVal=main.utilities.addCommas(jData[0]['count']);
						// }else{var dataVal=main.utilities.addCommas(jData[0]['total_sum']);}
						var i = jData[1]; //value of i when the ajax call was made
						aliasSplit = sum.alias.split(' ');
						splitAt = Math.ceil(aliasSplit.length / 2) - 1;
						topAlias = '', bottomAlias = '';
						for (var t = 0; t < aliasSplit.length; t++) {
							if (t <= splitAt) {
								topAlias += aliasSplit[t] + ' ';
							}
							if (t > splitAt) {
								bottomAlias += aliasSplit[t] + ' ';
							}
						}
						topAlias = topAlias.replace(/ ([^ ]*)$/, '$1');
						bottomAlias = bottomAlias.replace(/ ([^ ]*)$/, '$1');
						self.summaryInfo[sum.name] = {
							properties: sum
						};
						if (!jData[0]['total_sum']) {
							self.summaryInfo[sum.name].html = $('<div class="dbSummaryUnit noSelect">')
								.append($('<div class="dbSummaryInner">')
									.append($('<div class="dbSummaryAlias dbSummaryAliasTop">').text(topAlias))
									.append($('<div class="dbSummaryData">').html(main.utilities.addCommas(jData[0]['count'])))
									.append($('<div class="dbSummaryAlias dbSummaryAliasBottom">').text(bottomAlias))
								);
							self.summaryPLargeSums.append(self.summaryInfo[sum.name].html);
						} else {
							self.summaryInfo[sum.name].html_1 = $('<div class="dbSummaryUnit noSelect">')
								.append($('<div class="dbSummaryInner">')
									.append($('<div class="dbSummaryAlias dbSummaryAliasTop">').text(topAlias))
									.append($('<div class="dbSummaryData">').html(main.utilities.addCommas(jData[0]['total_sum'])))
									.append($('<div class="dbSummaryAlias dbSummaryAliasBottom">').text(bottomAlias))
								);
							self.summaryPLargeSums.append(self.summaryInfo[sum.name].html_1);
							self.summaryInfo[sum.name].html_2 = $('<div class="dbSummaryUnit noSelect">')
								.append($('<div class="dbSummaryInner">')
									.append($('<div class="dbSummaryAlias dbSummaryAliasTop">').text('Individual'))
									.append($('<div class="dbSummaryData">').html(main.utilities.addCommas(jData[0]['count'])))
									.append($('<div class="dbSummaryAlias dbSummaryAliasBottom">').text(bottomAlias))
								);
							self.summaryPLargeSums.append(self.summaryInfo[sum.name].html_2);
							self.summaryInfo[sum.name].html_3 = $('<div class="dbSummaryUnit noSelect">')
								.append($('<div class="dbSummaryInner">')
									.append($('<div class="dbSummaryAlias dbSummaryAliasTop">').text('Bulk'))
									.append($('<div class="dbSummaryData">').html(main.utilities.addCommas(jData[0]['bulk_count'])))
									.append($('<div class="dbSummaryAlias dbSummaryAliasBottom">').text(bottomAlias))
								);
							self.summaryPLargeSums.append(self.summaryInfo[sum.name].html_3);
						}
					}
				}
			});

		};
		//Tooltip creator for bar charts
		function tooltipContentEditorBar(str, seriesIndex, pointIndex, plot) {
			//$('#info').html(JSON.Strignify(plot));
			return plot.axes.xaxis.ticks[pointIndex] + "<hr>Count: " + plot.data[seriesIndex][pointIndex];
			
		};
		//Tooltip creator for stacked charts
		function tooltipContentEditorStacked(str, seriesIndex, pointIndex, plot) {
			//$('#info').html(JSON.Strignify(plot));
			return plot.axes.xaxis.ticks[pointIndex] +"<br>"+plot.series[seriesIndex].label+ "<hr>Count: " + plot.data[seriesIndex][pointIndex];
			
		};
        /*this.printChart = function(chart) {
			for (var chart in self.charts){
                if(self.charts[chart].chart_types==null){
					self.charts[chart].chart_types=self.defaultChartTypes;
				}
            }
        };*/
        //built content for Print Charts shelf  - to select reports
        this.createPrintChartsShelfContent = function() {
            
            $('#printChartsDiv').remove();
            $('#groupReorder').remove();
        
            $('#printChartsAndGraphs').remove();
            
			var content = document.createElement('div');
            content.setAttribute('id','printChartsDiv');
            var divNode, node, text;
            divNode = document.createElement('div');
            divNode.appendChild(document.createElement('br'));
            text = document.createTextNode('Use this tool to select which charts, graphs and tables of your tree inventory to include a printed report. Optionally, use the Advanced Filter tool in Tree Plotter to produce a report based on specific trees.');
            divNode.appendChild(text);
            divNode.appendChild(document.createElement('br'));
            divNode.appendChild(document.createElement('br'));
            text = document.createTextNode('Enter a report title: ');
            divNode.appendChild(text);
            node = document.createElement('input');
            node.setAttribute('id','multiReportTitle');
            node.setAttribute('type', 'text');
            divNode.appendChild(node);
            divNode.appendChild(document.createElement('br'));
            divNode.appendChild(document.createElement('br'));
            text = document.createTextNode('Add a description of your report (optional):');
            divNode.appendChild(text);
            divNode.appendChild(document.createElement('br'));
            node = document.createElement('textarea');
            node.setAttribute('id','multiReportDescription');
            node.setAttribute('rows', 5);
            divNode.appendChild(node);
            divNode.appendChild(document.createElement('br'));
            content.appendChild(divNode);
            node = document.createElement('div');
            node.appendChild(document.createElement('br'));
            input = document.createElement('input');
            input.setAttribute('type','checkbox');
            input.setAttribute('value','chart');
            input.setAttribute('id','multiReportShowDate');
            input.setAttribute('checked','checked');
            node.appendChild(input);
            text = document.createTextNode("Include today's date on the cover page");
            node.appendChild(text);
            content.appendChild(node);
            node = document.createElement('br');
            content.appendChild(node);
            node = document.createElement('div');
            text = document.createTextNode('Select the summaries to include in this report below.  You can drag the buttons to re-order the charts.');
            node.appendChild(text);
            node.appendChild(document.createElement('br'));
            node.appendChild(document.createElement('br'));
            content.appendChild(node);

            //append top
            self.printChartsAccordian.container.prepend(content); 
           
            self.chartOrder = self.chartOrder || {};
            var chartCount = 1; 
            var groupOrder = [];
            $.each(self.chartsGroups, function(idx,val) { 
                //input checks for this group
                var currentCharts = $.grep(self.charts, function(valInner, idxInner) {
                    return (valInner.theme_group == val)
                        && valInner[main.account.loggedInUserType+'_visible'];
                });	

			    var ul = document.createElement('ul');
                ul.setAttribute('class', 'chartReorder');
                $.each(currentCharts, function(idxInner,valInner) {
                    var subContent = document.createElement('li');
                    subContent.setAttribute('class','button');
                    subContent.setAttribute('style','text-align:left');
                    self.chartOrder[valInner.pid] = chartCount++;
                    valInner.place_in_order = chartCount++;
                    node = document.createElement('input');
                    node.setAttribute('type','checkbox');
                    node.setAttribute('value',valInner.pid);
                    node.setAttribute('class','chartMultiprint');
                    node.setAttribute('checked','checked');
                    subContent.appendChild(node);
                    node = document.createTextNode(' '+valInner.alias);
                    subContent.appendChild(node);
                    node = document.createElement('br');
                    subContent.appendChild(node);
                    ul.appendChild(subContent);
                });
 
                var shelf = self.printChartsAccordian.createShelf({
                    name:val,
                    button:new Button('toolBox','',val).html,
                    content:ul,
                    alias:val,
                    onOpen:function(){},
                    onClose:function(){}
                });

			    node = document.createElement('input');
                node.setAttribute('type','checkbox');
                node.setAttribute('value',val);
                node.setAttribute('class','chartMultiprintGroup');
                node.setAttribute('checked','checked');
                shelf.find('.buttonEle').prepend(' ');
                shelf.find('.buttonEle').prepend(node);
            });
        
            //reset order based on initial pass
            self.charts.sort(function(a, b) {
                return a.place_in_order - b.place_in_order;
            });
                
            //bottom part - button to print
			content = document.createElement('div');
            node = document.createElement('br');
            content.appendChild(node);
			node = document.createElement('button');
            node.setAttribute('class','button');
            node.setAttribute('id','printChartsAndGraphs');
            node.innerHTML = 'Print Preview this Report';
            content.appendChild(node);
            
            self.printChartsAccordian.container.append(content);

        };
		this.refreshChart=function(chart,type){
			if(!chart[main.account.loggedInUserType+'_visible']){
					$('#'+chart['name']+'_unit').addClass('none');
                    return; 
				 }
				else{
					 $('#'+chart['name']+'_unit').removeClass('none'); 
				additionalwhere='';
				if(main.invents.currentFiltered && main.invents.currentFilterPID && !main.account.loggedInUser.dont_filter){
					if (main.invents.currentFiltered==chart.inventory){
					var filtererIvtry=main.invents.inventories[main.invents.currentFilterLayer];
				var filteredByInternalMap=filtererIvtry.inventory.properties.internal_field_map;
				var filteredByAlias=filtererIvtry.inventory.map_layer.features[main.invents.currentFilterPID].properties[filteredByInternalMap.alias_field];
						additionalwhere= Object.keys(main.inventories[main.invents.currentFiltered].properties.lookup_table_map[main.invents.currentFilterLayer])[0]+"='"+filteredByAlias+"'";	
					}	
				}
				var advWhere='';
				var advLayer='';
				var advWhereAlias='';
				var advDepend=false;
				var advDependField='';
				var advDependLayer='';
				var advGeo='';
				if(main.advFilter.where || main.advFilter.geoFilter){
					if(main.advFilter.where){advWhere=main.advFilter.where;}
					if(main.advFilter.whereAlias){advWhereAlias=main.advFilter.whereAlias;}
					advLayer=main.advFilter.currentLayer.properties.name;
					if(main.advFilter.geoFilter){advGeo=main.advFilter.geoFilter.join(',');}
					if(chart.inventory!=advLayer){// Chart layer not currently filtered
						if(main.inventories[chart.inventory].properties.dependent_upon){// chart layer has a dependency
							if(main.inventories[chart.inventory].properties.dependent_upon==advLayer){//chart layer is dependent on filtered layer
								advDepend=true;
								advDependField=Object.keys(main.inventories[chart.inventory].properties.field_map[advLayer].fields)[0];
								advDependLayer=chart.inventory;
							}
							else if(main.inventories[advLayer].properties.dependent_upon==chart.inventory){//filtered layer is dependent on chart layer
								advDepend=true;	
								advDependField=Object.keys(main.inventories[advLayer].properties.field_map[advLayer].fields)[0];
								advDependLayer=advLayer;
							}
							else{
								//do nothing
							}
						}
					}
				}
				var params={
								groupby :chart.group_by ,
								table:chart.alias_table,
								sort:chart.sort_by,
								sortdir:chart.sort_dir,
								limit:chart.val_limit,
								chartwhere: chart.limit_where,
								additionalwhere: additionalwhere,
								type:type,
								advWhere:advWhere,
								advWhereAlias:advWhereAlias,
								advLayer:advLayer,
								advDepend:advDepend,
								advDependLayer:advDependLayer,
								advDependField:advDependField,
								folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
								advGeo:advGeo
							}

					$.post('../main/server/db.php',{
							    action : 'getChartNotReport',
								params : params
							
							}).done(function(data) {
								var jData = JSON.parse(data);
								$('#'+chart.name+'_title').html('');
								$('#'+chart.name+'_title').html('<b>'+chart.alias+'</b></br>'+jData[0]['percent']+'% of Data for the Field is "Not Specified" and Is Not Reflected in the Chart');
								$('#'+chart.name+'_title').data('notreporting',jData[0]['percent']);
                            });
				
					var params={
								groupby :chart.group_by ,
								table:chart.alias_table,
								sort:chart.sort_by,
								sortdir:chart.sort_dir,
								limit:chart.val_limit,
								chartwhere: chart.limit_where,
								additionalwhere: additionalwhere,
								type:type,
								advWhere:advWhere,
								advWhereAlias:advWhereAlias,
								advLayer:advLayer,
								advDepend:advDepend,
								advDependLayer:advDependLayer,
								advDependField:advDependField,
								advGeo:advGeo,
								folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
								inventory:chart.inventory
								
							}

					$.post('../main/server/db.php',{
									action : 'getchart',
									params : params
						}).done(function(data) { 
                            
                            for (var c in self.charts){
                                if(self.charts[c].chart_types==null){
                                    self.charts[c].chart_types=self.defaultChartTypes;
                                }
                            }
								 
                            var field1Alias, field2Alias;
                            field1Alias = main.data.fields[chart.inventory][chart.group_by['field1']].alias;
                            if( chart.group_by['field2'] ) {
                                field2Alias = main.data.fields[chart.inventory][chart.group_by['field2']].alias;
							}
                            temp2=chart.group_by['field2'];

                            var jData;
                            //if invalid data, continue
                            try {
							    jData = JSON.parse(data);
                                if(type=="pie"){
                                    var storedData=[];
                                    for(var j = 0; j < jData.length; j++){
                                        tempArray=[];
                                        temp=chart.group_by['field1'];
                                        tempArray.push(jData[j][temp]);
                                        tempArray.push(jData[j]['count']);
                                        tempArray.push(jData[j]['percent']);
                                        storedData.push(tempArray);
                                    }	

								    var temp = JSON.stringify([storedData]);
                                    if(storedData.length>0) {
                                         $.jqplot(chart.name, [storedData],{
                                            // The "seriesDefaults" option is an options object that will
                                            // be applied to all series in the chart.
                                            seriesDefaults:{
                                                renderer:$.jqplot.DonutRenderer, rendererOptions:{
                                                showDataLabels: true,
                                                dataLabels: 'percent'
                                                }
                                            },
                                            legend:{ show: true,location: 'e'}, 
                                            highlighter: {
                                                show: true,
                                                formatString: "%s<hr>Count: %d<br>%% of Data: %s ",
                                                useAxesFormatters:false
                                              }
                                        }).replot();
                                    }
                                }
                                else if (type=="bar"){//bar
                                    var storedData=[];
                                    var ticks =[];
                                    for(var j = 0; j < jData.length; j++){
                                        tempArray=[];
                                        temp=chart.group_by['field1'];
                                        ticks.push([jData[j][temp]]);
                                        storedData.push(jData[j]['count']);
                                    }	 
                                    
                                    if(storedData.length>0) {
                                        $.jqplot(chart.name, [storedData],{
                                            // The "seriesDefaults" option is an options object that will
                                            // be applied to all series in the chart.
                                            axesDefaults: {
                                                tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
                                                tickOptions: {
                                                  angle: -30,
                                                  fontSize: '10pt'
                                                }
                                            },
                                            seriesDefaults:{
                                                renderer:$.jqplot.BarRenderer, rendererOptions:{
                                                    showDataLabels: true,
                                                    barWidth: 20,
                                                    varyBarColor:true
                                                }
                                            },
                                             
                                            highlighter: {
                                                show: true,
                                                //tooltipAxes:'both',
                                                tooltipContentEditor: tooltipContentEditorBar,
                                                useAxesFormatters:false
                                              },
                                              axes: {
                                                // Use a category axis on the x axis and use our custom ticks.
                                                xaxis: {
                                                    
                                                    renderer: $.jqplot.CategoryAxisRenderer,
                                                    ticks: ticks
                                              },yaxis : {
                                                    tickOptions: {formatString: "%'i" },
                                                    
                                                },
                                              }
                                        }).replot();
                                    }
                                } 
                                else if (type=="stacked"){
                                    field=main.tables[chart.inventory].map_layer.fields[chart.group_by['field2']];			
                                    var ticks=[];
                                    var ticksAliases=[];
                                    var lables=[];
                                    var storedData=[];
                                    if(field.options){
                                        for(var i in field.options){
                                            ticks.push(field.options[i].alias);
                                            ticksAliases.push({label: field.options[i].alias});
                                            storedData.push([]);
                                        }
                                    }
                                    else if (field.lookup_table){ //	lookup	
                                        var values =self.lookups[chart.group_by['field2']].values;
                                        
                                            for(var i in values){
                                                ticks.push(values[i]);
                                            ticksAliases.push({label: values[i]});
                                            storedData.push([]);
                                            
                                            }			
                                        }
                                    
                                     temp=chart.group_by['field1']; // ie species_common
                                     temp2=chart.group_by['field2'];// ie dbh
                                     currentFieldVal=jData[0][temp];
                                     var tickCount=0;
                                     if(jData[0]==0){storedData[0].push(0);}
                                     else{
                                         for(var j = 0; j < jData.length; j++){
                                                
                                                if($.inArray(jData[j][temp],lables)<0){
                                                    lables.push(jData[j][temp]);
                                                }

                                                if(jData[j][temp]==currentFieldVal && tickCount<ticks.length){
                                                    if(jData[j][temp2]==ticks[tickCount]){
                                                        storedData[tickCount].push(jData[j]['count']);
                                                        tickCount++;
                                                    }
                                                    else{
                                                        storedData[tickCount].push(0);//didn't have a value for this option, value is 0
                                                        tickCount++;
                                                        j--; //get the next option and try again
                                                    }
                                                }
                                                else{
                                                    currentFieldVal=jData[j][temp];
                                                    if(tickCount<ticks.length){
                                                        for(;tickCount<ticks.length;tickCount++){
                                                            storedData[tickCount].push(0);
                                                        }
                                                    }
                                                    tickCount=0;//just guaranteed that we are at the end to ticks
                                                    if(jData[j][temp2]==ticks[tickCount]){
                                                        storedData[tickCount].push(jData[j]['count']);
                                                        tickCount++;
                                                    }
                                                    else{
                                                        storedData[tickCount].push(0);//didn't have a value for this option, value is 0
                                                        tickCount++;
                                                        j--; //get the next option and try again
                                                    }
                                                }
                                         }
                                     }
                                     var maxLength=1;
                                    for (var i=0; i< storedData.length;i++){
                                             if(storedData[i].length>maxLength){maxLength=storedData[i].length;}
                                    } 
                                    for (var i=0; i< storedData.length;i++){	 
                                        for (var j=0; j< maxLength-storedData[i].length;j++){
                                                 storedData[i].push(0);
                                        } 
                                     }
                                     
                                    if(storedData.length>0) {
                                         $.jqplot(chart.name, storedData,{
                                             stackSeries: true,
                                            // The "seriesDefaults" option is an options object that will
                                            // be applied to all series in the chart.
                                            axesDefaults: {
                                                tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
                                                tickOptions: {
                                                  angle: -30,
                                                  fontSize: '10pt'
                                                }
                                            },
                                            seriesDefaults:{
                                                renderer:$.jqplot.BarRenderer, rendererOptions:{
                                                showDataLabels: true,
                                                barWidth: 20,
                                                
                                                }
                                            },
                                            legend:{ show: true,location: 'e',placement :"outsideGrid"}, 
                                            series: ticksAliases,
                                            highlighter: {
                                                show: true,
                                                tooltipContentEditor:tooltipContentEditorStacked,
                                                useAxesFormatters:false
                                              },
                                              axes: {
                                                // Use a category axis on the x axis and use our custom ticks.
                                                xaxis: {
                                                    
                                                    renderer: $.jqplot.CategoryAxisRenderer,
                                                    ticks: lables
                                              },yaxis : {
                                                    tickOptions: {formatString: "%'i" },
                                                    
                                                }}
                                        }).replot();
                                    }
                                }//end stacked
                                else if (type=="tabular"){
                                     var storedData=[];
                                     var ticks =[];
                                     for(var j = 0; j < jData.length; j++){
                                        tempArray=[];
                                        temp=chart.group_by['field1'];
                                        ticks.push([jData[j][temp]]);
                                        storedData.push(jData[j]['count']);
                                     }	
                                    
                                    if(storedData.length == 0) {
                                        return; 
                                    }
                                    var cols = [];
                                    var table = '<div class="dashTabularWrap"><table class="dashTabular"><tr>';
                                    
                                    table += '<tr><th>' + field1Alias + '</th>';
                                    if(field2Alias) {
                                        table += '<th>' + field2Alias + '</th>';
                                    }
                                    table += '<th>Count</th>';
                                    table += '<th>Percent</th>';
                                    table += '</tr>';
                                    
                                    jData.forEach(function(row){
                                        table += '<tr>';
                                        for(var item in row){
                                            table += '<td>' + row[item] + '</td>';
                                        }
                                        table += '</tr>';
                                    }); 
                                    table += '</table></div>';
                                    
                                    $('#'+chart.name).html(table);
                                    $('#tabluar'+chart.name).click();
                                }

                            } catch(e) {
                                console.log(e.message);
                            }
								
	                    });
			}
		};
		//create initial summary wrapper
		this.createDBSummary=function(){
			self.treesByTheNumbers=$('<div class="treesByTheNumbers">');
			self.summaryPanel=$('<div class="dbSummaryPanel">');
			self.summaryPanelLabel=$('<div id ="summaryTitle" class="summaryPanelLabel">').text(main.personal_settings.app_settings.default_dashboard_title);
			self.summaryPLargeSums=$('<div class="summaryPLargeSums">');
			self.summaryPLargeSumsWrap=$('<div class="summaryPLargeSumsWrap">')
				.append(self.summaryPanelLabel)
				.append(self.summaryPLargeSums);
			self.countNumInfo={};
			self.summaryCountNums=$('<div class="summaryCountNums">');
			self.summaryPanelContent=$('<div class="summaryPanelContent">')
				.append(self.summaryPLargeSumsWrap)
				.append(self.summaryCountNums);
			self.summaryPanel.append(self.summaryPanelContent);
			self.summaryInfo={};
			self.dbCountsWrap=$('<div class="dbCountsWrap">');
			self.treesByTheNumbers
				.append(self.summaryPanel)
				.append(self.dbCountsWrap);
			return self.treesByTheNumbers;
		};
		//refresh charts if charts and graphs opened
		this.chartsAndGraphsOpened=function(){
			self.refreshAllCharts();
		};
		//refresh summaries and countNums if bynums opened
		this.byNumbersOpened=function(){
			self.refreshByNum();
		};
		//opened print charts
        this.printChartsOpened=function(){
			console.log('print charts opened');
		};
		//called by other components if loaded layer changes, used to refresh dashboard
		this.layerLoaded=function(layer){
			//self.refresh();
		}; 
		this.refreshLookups=function(){//get the lookups that have values currently being used in the app 
			numReturned=0;
			if (Object.keys(self.lookups).length==0){
				self.refresh();
				main.layout.loader.loaderOff();
				$(".byNumbers").css("pointer-events", "auto");
				$(".chartsAndGraphs").css("pointer-events", "auto");
				$(".ecoBens").css("pointer-events", "auto");
				}
				
			for (var i in self.lookups){
			 var params={
								field:i,
								table:self.lookups[i].table,
								folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]
	
							}
					$.post('../main/server/db.php',{
									action : 'getlookupTable',
									params : params
							
							}).done(function(data) { 
							var jData = JSON.parse(data);
								var values =[];
								for(var i in jData[1]){
									values.push(jData[1][i][jData[0]]);
								}
								self.lookups[jData[0]].values=values;
								numReturned++;
								if(numReturned==Object.keys(self.lookups).length){
									self.refresh();
									main.layout.loader.loaderOff();
									 $(".byNumbers").css("pointer-events", "auto");
									 $(".chartsAndGraphs").css("pointer-events", "auto");
									 $(".ecoBens").css("pointer-events", "auto");
									}
							}); 
			}
			
		};
		//refreshes all database views used by the dashboard, used to get the most recent data
		this.refreshAliasViews=function(){
			main.layout.loader.loaderOn();
			for(var i = 0; i < tables.length; i++ ){
				var params={
								table:tables[i],
								folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]
							}

					$.post('../main/server/db.php',{
									action : 'updateAliasViews',
									params : params
							
							}).done(function(data) { 
								response=JSON.parse(data);
								main.layout.loader.loaderOff();
							});
							
			}
			if(self.largeDataSet){
			var params={folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]};

					$.post('../main/server/db.php',{
									action : 'getTimeStamp',
									params : params
							
							}).done(function(data) { 
								response=JSON.parse(data);
							$('#timestamp').html('Data Last Updated: '+response[0]['dashboard_last_refresh']);
								
			});}
			
		}; 
		//if stats clicked on 
		this.panelOpened=function(){
			main.layout.loader.loaderOn();
			 $(".byNumbers").css("pointer-events", "none");
			 $(".chartsAndGraphs").css("pointer-events", "none");
			 $(".ecoBens").css("pointer-events", "none");
			 if(!self.largeDataSet){
				self.refreshAliasViews();
			 }
			self.refreshLookups();
			self.active=true;
		};
		//if stats clicked on again
		this.panelClosed=function(){
			self.active=false;
		};
		this.panelCustomChart={
            buildContent: function() {
                
                var allContent = $('<div>').addClass('');
                var fLFLWrap = $('<div>').addClass('fLFLWrap');
                var formContent = $('<form>')
                    .attr('id','frmCustomChart')
                    .addClass('fLFWrap');

                /*formContent.css({
                    'padding':'5px',
                    'background-color':'white',
                    'border':'1px solid black'
                });*/
                
                var fLOptionsWrap = $('<div>').addClass('filterLOptionsWrap');
               
                var selectTitleLabel= $('<label>').attr('for','inputTitle').text('Chart Title');
                var selectTitle = $('<input>').attr('id','inputTitle'); 
                selectTitleLabel.append(selectTitle);

                var selectChartTypeLabel = $('<label>').attr('for','inputGroupChartType')
                                            .text('Chart Type')
                                            .append('<br>');
                var inputGroupChartType = $('<fieldset>')
                        .attr('style','height:auto') 
                        .attr('id','inputGroupChartType'); 
                var types = ['pie','bar','stacked','tabular'];
                $.each(types, function( idx,val ) {
                    var label = $('<label>').text(val);
                    var input = $('<input type="checkbox" value='+val+'>');
                    label.append(input);
                    inputGroupChartType
                        .append(label)
                        .append('<br>');
                });
                selectChartTypeLabel.append(inputGroupChartType);
                
                var selectCustomChartInventoryLabel = $('<label>').attr('for','selectCustomChartInventory').text('Inventory');
                var selectCustomChartInventory = $('<select>').attr('id','selectCustomChartInventory'); 
                
                var sortedInvs = Object.keys(main.inventories);
                var idx = sortedInvs.indexOf('trees');
                var treesItem;
                if( idx != -1 && idx != 0 ) {
                    treesItem = sortedInvs.splice(idx,1);
                }            
                sortedInvs.unshift(treesItem);

                $.each(sortedInvs, function( idx,val ) {
                    var opt = $('<option>').text(val);
                    selectCustomChartInventory.append(opt);
                });
                selectCustomChartInventoryLabel.append(selectCustomChartInventory);
               
                //var optTrees = selectCustomChartInventory.find('option[value="trees"]');
                //selectCustomChartInventory.find('option[value="trees"]').remove();
                //selectCustomChartInventory.find('option:eq(0)').before(optTrees);
 
                var selectChartFieldLabel = $('<label>').attr('for','selectCustomChartField').text('Primary field');
                var selectChartField = $('<select>').attr('id','selectCustomChartField'); 
                selectChartFieldLabel.append(selectChartField);
                
                var selectChartField2Label = $('<label>').attr('for','selectCustomChartField2').text('Group by field');
                var selectChartField2 = $('<select>').attr('id','selectCustomChartField2'); 
                selectChartField2Label.append(selectChartField2);
             
                var inputPerms1Label = $('<label>').attr('for','inputPerms1').text('Admin 2');
                var inputPerms1 = $('<input>')
                        .attr('type','checkbox') 
                        .attr('value','admin_2_visible') 
                        .attr('checked',true) 
                        .attr('id','inputPerms1'); 
                inputPerms1Label.append(inputPerms1);
                
                var inputPerms2Label = $('<label>').attr('for','inputPerms2').text('Admin 3');
                var inputPerms2 = $('<input>')
                        .attr('type','checkbox') 
                        .attr('value','admin_3_visible') 
                        .attr('checked',true) 
                        .attr('id','inputPerms2'); 
                inputPerms2Label.append(inputPerms2);

                var inputPerms3Label = $('<label>').attr('for','inputPerms3').text('Public 1');
                var inputPerms3 = $('<input>')
                        .attr('type','checkbox') 
                        .attr('value','public_1_visible') 
                        .attr('checked',true) 
                        .attr('id','inputPerms3'); 
                inputPerms3Label.append(inputPerms3);
 
                var selectLimitLabel = $('<label>').attr('for','selectCustomChartLimitLabel').text('Limit');
                var selectLimit= $('<select>').attr('id','selectCustomChartLimit');
                var limits = {'Top 5':5,'Top 10':10,'Top 20':20,'None':0};
                $.each(limits, function( idx,val ) {
                    var opt = $('<option>').text(idx).attr('value',val);
                    selectLimit.append(opt);
                });
                selectLimitLabel.append(selectLimit);

                var selectThemeLabel = $('<label>').attr('for','selectThemeLabel').text('Category');
                var selectTheme = $('<select>').attr('id','selectTheme'); 
                $.each(self.chartsGroups, function( idx,val ) {
                    var opt = $('<option>').text(val).attr('value',val);
                    selectTheme.append(opt);
                });
                selectThemeLabel.append(selectTheme);
                
                var selectSortByLabel = $('<label>').attr('for','selectSortBy').text('Sort By');
                var selectSortBy = $('<select>').attr('id','selectSortBy'); 
                var sortBys = {'count(*)':'count(*)'};
                $.each(sortBys, function( idx,val ) {
                    var opt = $('<option>').text(idx).attr('value',val);
                    selectSortBy.append(opt);
                });
                selectSortByLabel.append(selectSortBy);

                var selectSortDirLabel = $('<label>').attr('for','selectSortDir').text('Sort By');
                var selectSortDir = $('<select>').attr('id','selectSortDir'); 
                selectSortDir.append( $('<option>').text('Descending').attr('value','desc') ); 
                selectSortDir.append( $('<option>').text('Ascending').attr('value','asc') );
                selectSortDirLabel.append(selectSortDir);

                var btnCreateCustomChart = $('<button>')
                        .attr('type','submit')
                        .attr('form','frmCustomChart')
                        .attr('id','btnCreateCustomChart').text('Create Chart');

                fLOptionsWrap = $('<div>').addClass('filterLOptionsWrap');
                
                formContent.append(fLOptionsWrap.append(selectThemeLabel).append('<br><br>'));
                fLOptionsWrap = $('<div>').addClass('filterLOptionsWrap');
                formContent.append(fLOptionsWrap.append(selectTitleLabel).append('<br><br>'));
                fLOptionsWrap = $('<div>').addClass('filterLOptionsWrap');
                formContent.append(fLOptionsWrap.append(selectChartTypeLabel).append('<br>'));
                fLOptionsWrap = $('<div>').addClass('filterLOptionsWrap');
                formContent.append(fLOptionsWrap.append(selectCustomChartInventoryLabel).append('<br><br>'));
                fLOptionsWrap = $('<div>').addClass('filterLOptionsWrap');
                formContent.append(fLOptionsWrap.append(selectChartFieldLabel).append('<br><br>'));
                fLOptionsWrap = $('<div>').addClass('filterLOptionsWrap');
                formContent.append(fLOptionsWrap.append(selectChartField2Label).append('<br><br>'));
                fLOptionsWrap = $('<div>').addClass('filterLOptionsWrap');
                formContent.append(fLOptionsWrap.append(selectLimitLabel).append('<br><br>'));
                formContent.append();
                fLOptionsWrap = $('<div>').addClass('filterLOptionsWrap');
                formContent.append(fLOptionsWrap
                                    .append('<p>Who should see this chart?</p>')
                                    .append(inputPerms1Label)
                                    .append('<br><br>')
                                    .append(inputPerms2Label)
                                    .append('<br><br>')
                                    .append(inputPerms3Label)
                                    .append('<br><br>')
                );
                //formContent.append(selectSortByLabel);
                //formContent.append('<br>');
                fLOptionsWrap = $('<div>').addClass('filterLOptionsWrap');
                formContent.append(fLOptionsWrap.append(selectSortDirLabel).append('<br><br>'));

                formContent.append(btnCreateCustomChart);

                var viewMore=new ViewMore({
                    prev:'Use this tool to create a custom chart.',
                    content:'<ul><li>Select a chart category, enter a title, select chart types.</li></ul>'
                            +'<ul><li>Choose fields - two for stacked, one for pie or bar.</li></ul>'
                            +'<ul><li>Limit your results to the first 5, 10 or 20 results.</li></ul>'
                            +'<ul><li>Choose who should see the chart.</li></ul>'
                });
                $(viewMore.html).css({
                    'background-color':'white',
                    'border':'1px solid black',
                    'margin-bottom':'5px',
                    'padding':'5px'
                });

                fLFLWrap.append(viewMore.html);
                fLFLWrap.append(formContent);
                allContent.append(fLFLWrap);
 
                selectCustomChartInventory.on('change', function(ev) {
                    //build fields list
                    var fields = main.inventories[ev.target.value].fields;
                    selectChartField.empty();
                    selectChartField2.empty();
                    $.each(fields, function(idx,val) {
                        var opt = $('<option>').text(val.alias).val(val.name);
                        selectChartField.append(opt);
                            opt2 = $('<option>').text(val.alias).val(val.name);
                            selectChartField2.append(opt2);
                    });
                });
                /*selectChartType.on('change', function(ev) {
                    });*/
                inputGroupChartType.on('change', function(e) {
                    var selected = [];
                    $('#inputGroupChartType input:checked')
                        .each(function(){ selected.push($(this).val()); });
                    selectCustomChartInventory.change();
                    
                    if(selected.indexOf('stacked') == -1) {
                        $('#selectCustomChartField2').prop('disabled',true).text('');
                        $('#selectCustomChartField2').parent().addClass('disabledLabel');
                    } else {
                        $('#selectCustomChartField2').prop('disabled',false);
                        $('#selectCustomChartField2').parent().removeClass('disabledLabel');
                    }
                });

                selectCustomChartInventory.change();
                
                btnCreateCustomChart.on('click', function(e){
                    createCustomChart(e);
                });
                
                function createCustomChart(e) {
                    e.preventDefault();
                    var ch = {};
                    ch.is_added_from_app = true;
                    ch.alias = $('#inputTitle').val();
                    ch.name = ch.alias.replace(/[^A-Z0-9]/ig, '_');
                    var currentChartnames = $.map( self.charts, function(val, idx) {
                        return val.name;
                    });
                    if( currentChartnames.indexOf(ch.name)>-1 || 
                        ch.name == '' ) {
                            alert('Another chart has the same name - please type another');
                            return;
                    }
                    ch.chart_types = [];
                    $('#inputGroupChartType input:checked')
                        .each(function(){ ch.chart_types.push($(this).val()); });
                    ch.inventory = $('#selectCustomChartInventory').val();
                    //ch.alias= $('#selectCustomChartField :selected').text();
                    ch.val_limit = $('#selectCustomChartLimit').val();

                    ch.admin_1_visible = true;
                    ch.admin_2_visible = $('#inputPerms1')[0].checked;
                    ch.admin_3_visible = $('#inputPerms2')[0].checked;
                    ch.public_1_visible = $('#inputPerms3')[0].checked;

                    ch.alias_table= "v_trees_dashboard";
                    
                    ch.category= "graph";
                    ch.type= "count";
                    ch.colorgrad= "Random";
                    
                    var field = $('#selectCustomChartField').val();
                    if(ch.chart_types.indexOf('stacked')!=-1) {
                        var field2 = $('#selectCustomChartField2').val();
                        ch.group_by= {'field1': field, 'field2': field2};
                    } else {
                        ch.group_by= {'field1': field};
                    }
    
                    ch.limit_where= '';// eg "status='Removed'";
                    var pids = $.map( self.charts, function(val, idx) {
                        return val.pid;
                    });
                    ch.pid = (Math.max.apply(Math, pids) + 1).toString();
                    var placeRanks = $.map( self.charts, function(val, idx) {
                        return val.place_in_order;
                    });
                    ch.place_in_order = Math.max.apply(Math, placeRanks) + 1;
                    ch.sort_by= "count(*)";
                    ch.sort_dir= "desc";
                    ch.theme_group = $('#selectTheme').val();

                    self.chartObs[ch.name] = ch;
                    self.charts.push(ch);
                    self.createCharts();
                    self.createPrintChartsShelfContent();
                    self.refreshAllCharts();

                    self.initPrintCharts();

                    //save the chart
                    ch.group_by = JSON.stringify(ch.group_by);
                    self.saveCustomChart(ch);
                    ch.group_by = JSON.parse(ch.group_by);

                    self.customChartPanel.close();
                }
	
                return allContent;
            }
        };
        this.saveCustomChart = function(chart) {
            
            $.ajax({
                type:'POST',
                url:main.mainPath+'server/db.php',
                data:{
                    params:{
                        chart: chart,
                        folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]
                    },
                    action:'saveCustomChart'
                },
                success:function(response){
                    if(response){
                        response=JSON.parse(response);
                    }
                }
            });
        };
        this.deleteCustomChart = function(e) {
            var chartName;
            var chartIdx; 
            var chartPid = $(e.target).data('chartpid');
            var ch = $.grep(self.charts, function(v,i) {
                if( chartPid == v['pid'] )  {
                    chartName = v['name'];
                    chartIdx = i; 
                    return v['pid'] == $(e.target).data('chartpid');
                }
            });
            
            $.ajax({
                type:'POST',
                url:main.mainPath+'server/db.php',
                data:{
                    params:{
                        chartPid: chartPid,
                        folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]
                    },
                    action:'deleteCustomChart'
                },
                success:function(response){
                    if(response){
                        response=JSON.parse(response);
                        if(response.success){
                            $('#'+chartName+'_unit').remove(); 
                            $('.chartMultiprint[value="'+chartPid+'"]').parent().remove();
                            self.charts.splice(chartIdx, 1);
                        }
                    }
                }
            });
        };
        this.sortChartsAndGroups = function() {
            var newGroupOrder = [];
            $('#groupReorder .buttonEle').each(function(idx,val) {
                newGroupOrder.push($(val).text().trim());
            });
            self.chartsGroups = newGroupOrder;
            
            $('#groupReorder').find('.chartReorder input').each(function(idx,val) {
                var ch = $.grep(self.charts, function(v,i) {
                    return v['pid'] == $(val).val();
                });
                ch[0].place_in_order = idx;
            });

            //now, sort the charts themselves based on the new order
            self.charts.sort(function(a, b) {
                return a.place_in_order - b.place_in_order;
            });
        };
        this.initPrintCharts = function() {
            var chartGroupDiv = document.createElement('div');
            chartGroupDiv.setAttribute('id', 'groupReorder');
            $('.printCharts .shelf').wrapAll(chartGroupDiv); 
            
            $('#groupReorder').sortable();   
            $('.chartReorder').sortable({});   

            //write over the chart groups array with array in new order
            $('#groupReorder').on('sortupdate', function(e,ui) {
                self.sortChartsAndGroups();
            });   
            $('.chartReorder').on('sortupdate', function(e,ui) {
                self.sortChartsAndGroups();
            });

            $('.chartMultiprintGroup').click(function(e){
                if( $(this).prop('checked') ) {
                    $(this).parent().parent().parent().parent().find('input.chartMultiprint').prop('checked',true);
                } else {
                    $(this).parent().parent().parent().parent().find('input.chartMultiprint').prop('checked',false);
                }

                e.stopPropagation();
            });

            // chart groups inputs select descending inputs
            $('.chartMultiprint').click(function(e){
                if( !$(this).prop('checked') ) {
                    $(this).parent().parent().parent().find('.chartMultiprintGroup').prop('checked', false);
                }
            });
       
           
            //handler for print all charts button, create in shelfContent for Print Charts And Graphs 
            $('#printChartsAndGraphs').on('click',function(e){
                self.printChartsAndGraphs(e);
             });  
        };
        this.printChartsAndGraphs = function(e) {
                self.charts.sort(function(a, b) {
                    return a.place_in_order - b.place_in_order;
                });
                var allChartsToPrint = [];
            
                //gather all checked charts, send to multi-report printer
                var selectedPids = $('.chartMultiprint:checked').map(function(){
                    return $(this).attr('value');
                }).get();
                    
                // - open window, give a name
                var newWin = window.open('', 'reportWindow');

                for (var chart in self.charts){
                    if( selectedPids.indexOf(self.charts[chart].pid) == -1 ){
                       continue;
                    }
                    
                    var chartNum = chart;
                    var name = self.charts[chartNum].name; 
                    var chartObject = self.charts[chartNum];
                    
                    var chartAliasTitle = chartObject.alias;
                
                    var stackedField2 = main.tables[chartObject.inventory].map_layer.fields[chartObject.group_by['field2']] || null;
                    chartObject.stackedField2 = stackedField2;
                                
                    //send data to print page, in new window
                    //  pass current advFilter, and chart info

                    var chartTypes = chartObject.chart_types || self.defaultChartTypes; 
                    var additionalwhere='';
                    var advFilter = {};
                    advFilter.filteredByInternalMap = ''; 
                    advFilter.filteredByAlias = '';
                    advFilter.filtererIvtry = '';
                    advFilter.advWhere = '';
                    advFilter.advWhereAlias = '';
                    advFilter.advLayer = '';
                    advFilter.advDepend = false;
                    advFilter.advDependLayer = '';
                    advFilter.advDependField = '';
                    advFilter.advDependAlias = '';
                    advFilter.advGeo = '';

                    // NOW we want to create the advFilter data for each chart, and params

                    if(main.invents.currentFiltered && !main.account.loggedInUser.dont_filter){
                        if (main.invents.currentFiltered==chartObject.inventory){
                            advFilter.filtererIvtry=main.invents.inventories[main.invents.currentFilterLayer];
                            advFilter.filteredByInternalMap=advFilter.filtererIvtry.inventory.properties.internal_field_map;
                            advFilter.filteredByAlias=advFilter.filtererIvtry.inventory.map_layer.features[main.invents.currentFilterPID].properties[advFilter.filteredByInternalMap.alias_field];
                            additionalwhere= main.invents.currentFilterLayer+"='"+advFilter.filteredByAlias+"'";	
                        }	
                    }
                    if(main.advFilter.where || main.advFilter.geoFilter){
                        if(main.advFilter.where){advFilter.advWhere=main.advFilter.where;}
                        if(main.advFilter.whereAlias){advFilter.advWhereAlias=main.advFilter.whereAlias;}
                        advFilter.advLayer=main.advFilter.currentLayer.properties.name;
                        if(main.advFilter.geoFilter){advFilter.advGeo=main.advFilter.geoFilter.join(',');}
                        if(chartObject.inventory!=advFilter.advLayer){// Chart layer not currently filtered
                            if(main.inventories[chartObject.inventory].properties.dependent_upon){// chart layer has a dependency
                                if(main.inventories[chartObject.inventory].properties.dependent_upon==advFilter.advLayer){//chart layer is dependent on filtered layer
                                    advFilter.advDepend=true;
                                    advFilter.advDependField=Object.keys(main.inventories[chartObject.inventory].properties.field_map[advFilter.advLayer].fields)[0];
                                    advFilter.advDependLayer=chartObject.inventory;
                                }
                                else if(main.inventories[advFilter.advLayer].properties.dependent_upon==chartObject.inventory){//filtered layer is dependent on chart layer
                                    advFilter.advDepend=true;	
                                    advFilter.advDependField=Object.keys(main.inventories[advFilter.advLayer].properties.field_map[advFilter.advLayer].fields)[0];
                                    advFilter.advDependLayer=advFilter.advLayer;
                                }
                                else{
                                    //do nothing
                                }
                            }
                        }
                    }

                    var field1Alias, field2Alias;
                    field1Alias = main.data.fields[chartObject.inventory][chartObject.group_by['field1']].alias;
                    if( chartObject.group_by['field2'] ) {
                        field2Alias = main.data.fields[chartObject.inventory][chartObject.group_by['field2']].alias;
                    }
                    chartObject.field1Alias =    field1Alias;
                    chartObject.field2Alias =    field2Alias;
                    
                    var params={
                        chartName:      name,
                        chartAliasTitle:chartAliasTitle,
                        groupby:        chartObject.group_by,
                        table:          chartObject.alias_table,
                        sort:           chartObject.sort_by,
                        sortdir:        chartObject.sort_dir,
                        limit:          chartObject.val_limit,
                        chartwhere:     chartObject.limit_where,
                        additionalwhere: additionalwhere,
                        advWhere:       advFilter.advWhere,
                        advWhereAlias:  advFilter.advWhereAlias,
                        advLayer:       advFilter.advLayer,
                        advDepend:      advFilter.advDepend,
                        advDependLayer: advFilter.advDependLayer,
                        advDependField: advFilter.advDependField,
                        userID:         main.userID,
                        advGeo:         advFilter.advGeo,
                        inventory:      chartObject.inventory,
                        stackedField2:  stackedField2,
                        folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]
                    };

                    //chartObj and params - use $.param()
                    allChartsToPrint.push({
                        chartObject:    chartObject,
                        params:         params,
                        chartTypes:     chartTypes  
                    });


                };

                var form = $('<form/>').attr({
                    method: 'post',
                    action: '../main/server/dashReportMulti.php',
                    target: 'reportWindow',
                });
       
                var multiReportDescription = $('#multiReportDescription').val();
                var multiReportTitle= $('#multiReportTitle').val();
                var currentFilterDisplayWhere = (main.advFilter && main.advFilter.displayWhere) 
                            ? main.advFilter.displayWhere : '';
                var currentGeoFilter = (main.advFilter.geoFilter ) 
                            ? main.advFilter.geoFilter.length : '';
                var showDate = $('#multiReportShowDate').prop('checked');

                form.append($('<input/>').attr({name:'showDate',value: showDate }));                   
                form.append($('<input/>').attr({name:'currentGeoFilter',value: currentGeoFilter}));                   
                form.append($('<input/>').attr({name:'currentFilterDisplayWhere',value: currentFilterDisplayWhere}));                   
                form.append($('<input/>').attr({name:'multiReportDescription',value: multiReportDescription }));                   
                form.append($('<input/>').attr({name:'multiReportTitle',value: multiReportTitle }));                   
                form.append($('<input/>').attr({name:'allChartsToPrint',value: JSON.stringify(allChartsToPrint) }));                   
                $('body').append(form);
                
                form.submit();
                form.remove(); 
        };
   
        this.nextEcoIt=function(){
            var min=self.ecobensCurrentIt*self.ecobensMaxUpdatesPerIt;
            if(min>self.ecobensPIDs.length){return;}
            var max=min+self.ecobensMaxUpdatesPerIt;
            var slice=self.ecobensPIDs.slice(min,max);
            var dbhField=main.invents.inventories.trees.properties.internal_field_map.dbh;
            var speciesField=main.invents.inventories.trees.properties.internal_field_map.main_species_field;
            $.ajax({
                type:'POST',
                url:main.mainPath+'server/db.php',
                data:{
                    params:{
                        folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
                        slice:slice,
                        dbhField:dbhField,
                        speciesField:speciesField
                    },
                    action:'updateTreesForEcoBens'
                },
                success:function(response){
                    if(response){
                        response=JSON.parse(response);
                        if(response.status=="OK"){
                            self.ecobensCurrentIt++;
                            self.nextEcoIt();
                        }
                    }
                }
            });
        };
            
        this.updateEcoBen=function(){
            if(!main.personal_settings.app_settings.use_custom_ecobens){
                if(main.invents.currentFiltered && main.invents.currentFilterPID){
                    var filtererIvtry=main.invents.inventories[main.invents.currentFilterLayer];
                    var filteredByInternalMap=filtererIvtry.inventory.properties.internal_field_map;
                    var filteredByAlias=filtererIvtry.inventory.map_layer.features[main.invents.currentFilterPID].properties[filteredByInternalMap.alias_field];
                    
                    $('#ecoLabel').html('Benefits Filtered by: '+filteredByAlias);
                }
                else{
                    
                    $('#ecoLabel').html(main.personal_settings.app_settings.default_ecobens_title);
                }
                if(main.account.loggedInUser.username=='user1'){
                    $('body').on('click','.header',function(e){
                        return false;
                        e.stopImmediatePropagation();
                        var dbhField=main.invents.inventories.trees.properties.internal_field_map.dbh;
                        var speciesField=main.invents.inventories.trees.properties.internal_field_map.main_species_field;
                        if(dbhField && speciesField){
                            var where=null;
                            $.ajax({
                                type:'POST',
                                url:main.mainPath+'server/db.php',
                                data:{
                                    params:{
                                        folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
                                        table:'trees',
                                        where:where
                                    },
                                    action:'getPIDs'
                                },
                                success:function(response){
                                    if(response){
                                        response=JSON.parse(response);
                                        if(response.status=="OK"){
                                            self.ecobensCurrentIt=0;
                                            self.ecobensMaxUpdatesPerIt=100;
                                            self.ecobensPIDs=response.pids;
                                            self.nextEcoIt();
                                        }
                                    }
                                }
                        });
                        }
                    });
                }
                (function(){
                    var additionalwhere =''; 
                    var advWhere='';
                    var advLayer='';
                    var advWhereAlias='';
                    var advDepend=false;
                    var advDependField='';
                    var advDependLayer='';
                    var advGeo='';
                    if(main.invents.currentFiltered && main.invents.currentFilterPID && !main.account.loggedInUser.dont_filter){
                        if (main.invents.currentFiltered=='trees'){
                            var filtererIvtry=main.invents.inventories[main.invents.currentFilterLayer];
                            var filteredByInternalMap=filtererIvtry.inventory.properties.internal_field_map;
                            var filteredByAlias=filtererIvtry.inventory.map_layer.features[main.invents.currentFilterPID].properties[filteredByInternalMap.alias_field];
                            additionalwhere= Object.keys(main.inventories['trees'].properties.field_map[main.invents.currentFilterLayer].fields)[0]+"='"+main.invents.currentFilterPID+"'";	
                        }	
                    }
                    if(main.advFilter.where || main.advFilter.geoFilter){
                        if(main.advFilter.where){advWhere=main.advFilter.where;}
                        if(main.advFilter.whereAlias){advWhereAlias=main.advFilter.whereAlias;}
                        advLayer=main.advFilter.currentLayer.properties.name;
                        if(main.advFilter.geoFilter){advGeo=main.advFilter.geoFilter.join(',');}
                        if('trees'!=advLayer){// Chart layer not currently filtered
                        if(main.inventories['trees'].properties.dependent_upon){// chart layer has a dependency
                            if(main.inventories['trees'].properties.dependent_upon==advLayer){//chart layer is dependent on filtered layer
                                advDepend=true;
                                advDependField=Object.keys(main.inventories['trees'].properties.field_map[advLayer].fields)[0];
                                advDependLayer='trees';
                            }
                            else if(main.inventories[advLayer].properties.dependent_upon=='trees'){//filtered layer is dependent on chart layer
                                advDepend=true;	
                                advDependField=Object.keys(main.inventories[advLayer].properties.field_map[advLayer].fields)[0];
                                advDependLayer=advLayer;
                            }
                            else{
                                //do nothing
                            }
                        }
                    }
                    }
                    $.ajax({
                        type:'POST',
                        url:main.mainPath+'server/db.php',
                        data:{
                            params:{
                                additionalwhere: additionalwhere,
                                table:'trees',
                                advWhere:advWhere,
                                advWhereAlias:advWhereAlias,
                                advLayer:advLayer,
                                advDepend:advDepend,
                                advDependLayer:advDependLayer,
                                advDependField:advDependField,
                                advGeo:advGeo,
                                folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
                                table:'trees'
                            },
                            action:'getCurrentEcoTotals'
                        },
                        success:function(response){
                            if(response){
                                response=JSON.parse(response);
                                if(response.status=="OK"){
                                    var totals=response.totals,val,prepend_to,append_to;
                                    var ecobens=main.data.std_settings.ecobens;
                                    for(var key in totals){
                                        if(self.ecobensItems[key]){
                                            prepend_to='',append_to='';
                                            if(ecobens[key].prepend_to){prepend_to=ecobens[key].prepend_to;}
                                            if(ecobens[key].append_to){append_to=ecobens[key].append_to;}
                                            val=totals[key];
                                            if(val<5){val=val.toFixed(2);
                                            }else{val=val.toFixed(0);}
                                            self.ecobensItems[key].subContentContent.html(prepend_to+main.utilities.addCommas(val)+append_to);
                                        }
                                    }
                                }
                            }
                        }
                    });
                })();
            }else{
            
                if(main.invents.currentFiltered && main.invents.currentFilterPID){
                    var filtererIvtry=main.invents.inventories[main.invents.currentFilterLayer];
                    var filteredByInternalMap=filtererIvtry.inventory.properties.internal_field_map;
                    var filteredByAlias=filtererIvtry.inventory.map_layer.features[main.invents.currentFilterPID].properties[filteredByInternalMap.alias_field];
                    
                    $('#ecoLabel').html('Benefits Filtered by: '+filteredByAlias);
                }
                else{
                    
                    $('#ecoLabel').html(main.personal_settings.app_settings.default_ecobens_title);
                }
                var ids="where trees.species_code<>'' ";
                if(self.layersLoaded.indexOf("trees")>-1){
                        var features=main.tables["trees"].map_layer.features;
                        ids+='and ('
                        for (feature in features){
                            ids+=' trees.pid= '+features[feature].properties.pid+' or';
                        }
                        ids=ids.slice(0, -3);
                        ids+=')'
                }
                //ajax 
                
                 var params={
                                ids : ids,
                                folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]
                                
                            }
                    $.post('../main/server/db.php',{
                                    action : 'getBens',
                                    params : params
                            
                        }).done(function(data) { 
                             var jData = JSON.parse(data);
                             if (jData[0].e_dollar){
                                $('#energyBenVal').html(jData[0].e_dollar.slice(0, - 3)+'<br>'+jData[0].e_unit+" megawatts");
                                $('#waterBenVal').html(jData[0].w_dollar.slice(0, - 3)+'<br>'+jData[0].w_unit+" gallons");
                                $('#airBenVal').html(jData[0].air_dollar.slice(0, - 3)+'<br>'+jData[0].air_unit+"  lbs");
                                $('#overallBenVal').html(jData[0].t_dollar.slice(0, - 3));
                            }else{
                                $('#energyBenVal').html('no qualifying trees');
                                $('#waterBenVal').html('no qualifying trees');
                                $('#airBenVal').html('no qualifying trees');
                                $('#overallBenVal').html('no qualifying trees');  
                             }
                        });
            }
        };
                

		//setup very inital html
		(this.preHTMLAdd=function(){
			if(main.withs.with_dashboard.icon_path){var buttonImg='images/'+main.withs.with_dashboard.icon_path}
			else{var buttonImg=main.mainPath+'images/bar_graph.png';}
			

			
			var buttonAlias=main.withs.with_dashboard.alias || self.alias;
			main.utilities.addLayoutItems(self.name,buttonAlias,self,buttonImg,'');
			self.sidePanel.setOnOpen(self.panelOpened);
			self.sidePanel.setOnClose(self.panelClosed);
			if(self.largeDataSet){			
			refresh =$('<div>');
			refresh.html('<p id=refreshInstruction>Click refresh to see latest updates (may take up to 60sec)</p>');
			refresh.append('<p id=timestamp> Last Update: '+self.timeStamp+'</p>');
			refreshbutton= new Button('std','right','Refresh');
			refresh.append(refreshbutton.html.on('click',self.refreshAliasViews));
			self.accordian.container.append(refresh);
			}
			
			self.shelves={};
			self.shelves['byNumbers']={alias:/* main.invents.inventories[self.defaultInventory].plural_alias+ */'By the Numbers'};
			self.shelves['byNumbers'].launchButton=new Button('toolBox','',self.shelves['byNumbers'].alias);
			self.setCharts();
			//summary
			self.shelves['byNumbers'].shelfContent=self.createDBSummary();
			self.shelves['byNumbers'].shelf=self.accordian.createShelf({
				name:'byNumbers',
				button:self.shelves['byNumbers'].launchButton.html,
				content:self.shelves['byNumbers'].shelfContent,
				alias:self.shelves['byNumbers'].alias,
				extended:true,
				onOpen:self.byNumbersOpened
			});
			for(var i in main.personal_settings.charts ){
				if(!main.inventories[main.personal_settings.charts[i].inventory]){continue}
				if($.inArray(main.personal_settings.charts[i].alias_table,tables)<0){
					tables.push(main.personal_settings.charts[i].alias_table);
				}
				if(main.personal_settings.charts[i].group_by){
					if(main.personal_settings.charts[i].group_by['field1']){
					var test = main.personal_settings.charts[i].group_by['field1'];
					var field=main.tables[main.personal_settings.charts[i].inventory].map_layer.fields[main.personal_settings.charts[i].group_by['field1']];
					if(!(self.lookups[test])){
						if(field.lookup_table){
						self.lookups[test]={"table":main.personal_settings.charts[i].alias_table}
						}
					}}
					if(main.personal_settings.charts[i].group_by['field2']){
					var test2 = main.personal_settings.charts[i].group_by['field2'];
					var field2=main.tables[main.personal_settings.charts[i].inventory].map_layer.fields[main.personal_settings.charts[i].group_by['field2']];
					if(!(self.lookups[test2])){
						if(field2.lookup_table){
						self.lookups[test2]={"table":main.personal_settings.charts[i].alias_table}
						}
					}}
				}

			}
			//charts
			self.shelves['chartsAndGraphs']={alias:'Charts and Graphs'};
			self.shelves['chartsAndGraphs'].launchButton=new Button('toolBox','',self.shelves['chartsAndGraphs'].alias);
			self.shelves['chartsAndGraphs'].dbChartsChartsWrapWrap=$('<div class="dbChartsChartsWrapWrap">');
			self.shelves['chartsAndGraphs'].dbChartsChartsDetails=$('<div class="dbChartsChartsDetails">').append(self.shelves['chartsAndGraphs']);
			self.shelves['chartsAndGraphs'].dbChartsChartsWrapWrap
				.append(self.shelves['chartsAndGraphs'].dbChartsChartsDetails)
				.append(self.createChartGroups());
				
			self.shelves['chartsAndGraphs'].shelfContent=self.shelves['chartsAndGraphs'].dbChartsChartsWrapWrap;
			self.shelves['chartsAndGraphs'].shelf=self.accordian.createShelf({
				name:'chartsAndGraphs',
				button:self.shelves['chartsAndGraphs'].launchButton.html,
				content:self.shelves['chartsAndGraphs'].shelfContent,
				alias:self.shelves['chartsAndGraphs'].alias,
				extended:true,
				onOpen:self.chartsAndGraphsOpened
			});

            // CHART PRINTING 
            if(main.withs.with_dashchartprint && main.withs.with_dashchartprint.value){

                self.shelves['printCharts']={alias:'Print Report'};
                self.shelves['printCharts'].launchButton=new Button('toolBox','',self.shelves['printCharts'].alias);
                self.printChartsAccordian=new Accordian('afFields','abc');
                self.shelves['printCharts'].shelfContent=self.printChartsAccordian.container;
                //self.shelves['printCharts'].shelfContent = self.createPrintChartsShelfContent();
                self.createPrintChartsShelfContent();
                self.shelves['printCharts'].shelf=self.accordian.createShelf({
                    name:'printCharts',
                    button:self.shelves['printCharts'].launchButton.html,
                    content: self.shelves['printCharts'].shelfContent,
                    alias:self.shelves['printCharts'].alias,
                    extended:true,
                    onOpen:self.printChartsOpened
                });

			    self.createCharts();  
                self.initPrintCharts();

                //initial group order
                self.newOrder = [];
                $('#groupReorder .buttonEle').each( function(idx,val) {
                     self.newOrder.push($(val).text().trim());
                });
                self.chartsGroups = self.newOrder;
            
                //initial chart order
                var initialChartOrder = 0;
               	$.each(self.chartsGroups, function(i,group) {
					$.each(self.charts, function(i2,chart) {
                        if( chart.theme_group == group ) {
						    chart.place_in_order = initialChartOrder++;
					    }
                    });
                });
			}


            var client  = window.location.pathname.split("/")[window.location.pathname.split("/").length-2];

            if( client == 'IEC' || client == 'Idaho') { // && main.account.loggedInUserType == 'admin_1') {
                //create custom chart interface
                self.customChartPanel=new Panel({
                    content:        self.panelCustomChart.buildContent(),
                    title:          'Create custom chart',
                    classes:        'tourWindow',
                    position:       'mMiddle',
                    matchHeight:    true,
                    relativeTo:     main.layout.mapContainerWrap,
                });


                var btn = $('<div>').prop('id','openCreateCustomPanel')
                        .addClass('button','buttonExtra','dataExtra')
                        .css({
                            'display':'inline-block',
                            'margin-top':'.5em',
                        })
                        .text('Create a Custom Chart');

                
                $('.shelfContentWrap.chartsAndGraphs .accordian').append(btn);
                $('.shelfContentWrap.chartsAndGraphs .accordian').append(btn);
                if( main.account.loggedInUserType != 'admin_1') { 
                    btn.hide();
                }
                 
                //$('.buttonContent.buttonEle:contains("Charts and Graphs")').parent().append(btn);

                //self.shelves['chartsAndGraphs'].dbChartsChartsWrapWrap.append(btn);

                $('#openCreateCustomPanel').click(function(e){
                    e.stopPropagation();
                    self.customChartPanel.open(); 
                });   
            }

            //ecobens
			if(main.withs.with_ecobens.value){
                ecoContent=$('<div class="dbSummaryPanel">');
                self.EcoPanelLabel=$('<div class="summaryPanelLabel" id="ecoLabel">').html(main.personal_settings.app_settings.default_ecobens_title);
                
                ecoContent.append(self.EcoPanelLabel);
                
                discalimer=$('<p>');
                discalimer.html('Benefits are only calculated for trees with defined species, DBH, and land use.');
                ecoContent.append(discalimer);
                
                overallBen=$('<p id="overallBenVal">');
                ecoContent.append(overallBen);
                self.ecoBensAccordian=new Accordian('dashEcoBens','abc');
                overallContent=$('<div class="EcoBens">')
					.append(self.ecoBensAccordian.container);
                if(!main.personal_settings.app_settings.use_custom_ecobens){
                    var key,content,subContent,ecoben;
                    var ecobens=main.data.std_settings.ecobens;
                    var ecobensKeys=Object.keys(ecobens).sort(function(a,b){
                        if(ecobens[a].place_in_order>ecobens[b].place_in_order){return 1;}
                        if(ecobens[a].place_in_order<ecobens[b].place_in_order){return -1;}
                        return 0;
                    });
                    self.ecobensItems={};
                    for(var i=0;i<self.ecobensKeys.length;i++){
                        key=self.ecobensKeys[i];
                        content=$('<div class="EcoBens cf">');
                        img=self.ecobensCategories[key].img || '';
                        self.ecobensCategories[key].items={};
                        content.append($('<img>').addClass('containerImg').attr('src',main.mainPath+'images/'+img));
                        self.shelves[key]={alias:self.ecobensCategories[key].alias};
                        self.shelves[key].launchButton=new Button('toolBox','',self.shelves[key].alias+"\t");
                        for(var t=0;t<ecobensKeys.length;t++){
                            key0=ecobensKeys[t];
                            if(ecobens[key0].category==key){
                                var ecoben=ecobens[key0];
                                self.ecobensCategories[key].items[key0]={};
                                self.ecobensCategories[key].items[key0].subContentContent=$('<div class="ecoSubGroupWrapContent">');
                                self.ecobensCategories[key].items[key0].subContent=$('<div class="ecoSubGroupWrap '+key+'">')
                                    .append($('<div class="ecoSubGroupWrapLabel">').html(ecoben.alias))
                                    .append(self.ecobensCategories[key].items[key0].subContentContent)
                                content.append(self.ecobensCategories[key].items[key0].subContent);
                                self.ecobensItems[key0]=self.ecobensCategories[key].items[key0];
                            }
                        }
                        self.shelves[key].shelf=self.ecoBensAccordian.createShelf({
                            name:key,
                            button:self.shelves[key].launchButton.html,
                            content:content,
                            alias:self.shelves[key].alias,
                            extended:true
                        });
                        // overallContent.append(self.shelves[key].shelf);	
                    }
                }else{
                    energyContent=$('<div class="EcoBens">');
                    energyContent.append($('<img>').addClass('containerImg').attr('src',main.localImgs+'energy.png'));
                    self.shelves['energy']={alias:'Energy Conservation'};
                    self.shelves['energy'].launchButton=new Button('toolBox','',self.shelves['energy'].alias+"\t");
                    energyBen=$('<p id="energyBenVal">');
                    energyContent.append(energyBen);
                    
                    self.shelves['energy'].shelf=self.ecoBensAccordian.createShelf({
                        name:'energy',
                        button:self.shelves['energy'].launchButton.html,
                        content:energyContent,
                        alias:self.shelves['energy'].alias,
                        extended:true
                    });
                    // overallContent.append(self.shelves['energy'].shelf);			
                    
                    airContent=$('<div class="EcoBens">');
                    airContent.append($('<img>').addClass('containerImg').attr('src',main.localImgs+'air_quality.png'));
                    self.shelves['airQual']={alias:'Air Quality'};
                    self.shelves['airQual'].launchButton=new Button('toolBox','',self.shelves['airQual'].alias+"\t");
                    airBen=$('<p id="airBenVal">');
                    airContent.append(airBen);
                    self.shelves['airQual'].shelf=self.ecoBensAccordian.createShelf({
                        name:'airQual',
                        button:self.shelves['airQual'].launchButton.html,
                        content:airContent,
                        alias:self.shelves['airQual'].alias,
                        extended:true
                    });
                    // overallContent.append(self.shelves['airQual'].shelf);
                    
                    waterContent=$('<div class="EcoBens">');
                    self.shelves['stormWater']={alias:'Storm Water Management'};
                    waterContent.append($('<img>').addClass('containerImg').attr('src',main.localImgs+'stormwater.png'));
                    self.shelves['stormWater'].launchButton=new Button('toolBox','',self.shelves['stormWater'].alias+"\t");
                    waterBen=$('<p id="waterBenVal">');
                    waterContent.append(waterBen);
                    
                    self.shelves['stormWater'].shelf=self.ecoBensAccordian.createShelf({
                        name:'stormWater',
                        button:self.shelves['stormWater'].launchButton.html,
                        content:waterContent,
          extended:true
                    });
                    // overallContent.append(self.shelves['stormWater'].shelf);	
                }
                
                ecoContent.append(overallContent);
                
                self.shelves['ecoBens']={alias:'Ecosystem Benefits'};
                self.shelves['ecoBens'].launchButton=new Button('toolBox','',self.shelves['ecoBens'].alias);
                self.shelves['ecoBens'].shelf=self.accordian.createShelf({
                    name:'ecoBens',
                    button:self.shelves['ecoBens'].launchButton.html,
                    content:ecoContent,
                    alias:self.shelves['ecoBens'].alias,
                    extended:true,
                    onOpen:self.updateEcoBen
                });
                self.accordian.container.append(self.shelves['ecoBens'].shelf);
                self.updateEcoBen();
            }//end ecobens
		    
            window.addEventListener('orientationchange',self.refreshResize);
		})();
	};
});
