define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var DataTable=require('./DataTable');
		var Geocoder=require('./Geocoder');
		var Layer2=require('./Layer2');
		var MapInteractions=require('./MapInteractions');
		var Panel=require('./Panel');
		var PopUp=require('./PopUp');
		var ToolTip=require('./ToolTip');
		var Wait=require('./Wait');
		var WaitLoad=require('./WaitLoad');
		this.onePopUpAtTime=true;
		this.popupsUneditable=true;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.matchedLayer=null;
		this.is_inventory=false;
		this.hasPopUp=null;
		this.hasPopUpOtherFeatures=null;
		this.hasPopUpCurrentIndex=null;
		this.popUpZ=null;
		this.blockMoveEndLoadOnce=false;
		this.currentFocused=null;
		this.lastFocused=null;
		main.map_layers[self.layerName].firstLoad=false;
		this.alias=self.properties.alias;
		this.singular_alias=self.properties.singular_alias || this.alias;
		this.plural_alias=self.properties.plural_alias || this.alias;
		this.mapInteractions=new MapInteractions({
			context:self
		});
		// this.clearControlledContent=function(){
			// self.form=null;
			// self.tableTbody=null;
			// self.templateStuff.templatesHTML=null;
			// self.templateStuff.templatePanel=null;
			// self.addSC=null;
			// self.moveSC=null;
			// self.editingSCPkg=null;
			// self.addItemButton=null;
			// self.moveItemButton=null;
			// self.addPhotoButton=null;
			// self.table=null;
		// };
		(this.init=function(){
			var layers={};
			layers[self.layerName]=self.layer;
			self.waitLoad=new WaitLoad({
				layers:layers,
				context:self
			});
		})();
	};
});