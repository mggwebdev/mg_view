define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Geocoder=require('./Geocoder');
		this.alias='Search by Address';
		this.name='geocoder';
		this.withItem='with_geocoder';
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.markers={};
		this.id=this.id || main.utilities.generateUniqueID();
		this.geocoded=function(coods,address){
			var mapCood=ol.proj.transform(coods, 'EPSG:4326', 'EPSG:3857');
			main.map.panTo(mapCood);
			main.map.animateZoom(main.misc.defaultZoomTo);
			var marker=self.geocoder.addMarker(mapCood,address,true);
			self.markers[marker.id]=marker;
		};
		this.clearResult=function(marker){
			marker.popup.remove();
			self.geocoder.removeFeature(marker.id,true);
			var id=marker.id;
			delete self.markers[id];
		}
		this.clearResults=function(){
			for(var key in self.markers){
				self.clearResult(self.markers[key]);
			}
		};
		this.clearResultsButtonClicked=function(){
			self.clearResults();
		};
		this.lookUpButtonClicked=function(){
			var val=self.input.val();
			if(val){
				self.geocoder.getCoodsFromAddress(self.geocoded,val);
			}
		};
		this.createContent=function(){
			self.lookUpButton=new Button('std','lookUpButton formConcButton','Look Up');
			self.lookUpButton.html.on('click',self.lookUpButtonClicked);
			self.clearResultsButton=new Button('std','clearResultsButton formConcButton','Clear Results');
			self.clearResultsButton.html.on('click',self.clearResultsButtonClicked);
			self.formConcButtons=$('<div class="formConcButtons">')
				.append(self.lookUpButton.html)
				.append(self.clearResultsButton.html);
			self.input=$('<input type="text" class="tbGeocoderInput tbGEoInputWrapEle" placeholder="5690 Webster Street, Arvada, CO 80002">');
			self.input.on('keyup',function(e){
				if(e.which==13){
					$(this).blur();
					self.lookUpButtonClicked();
				}
			});
			self.tbGeocoderInputLabel=$('<div class="tbGeocoderInputLabel tbGEoInputWrapEle">').html('Address:&nbsp;');
			self.tbGEoInputWrap=$('<div class="tbGEoInputWrap cf">')
				.append(self.tbGeocoderInputLabel)
				.append(self.input);
			self.html=$('<div class="tbGeocoder">')
				.append(self.tbGEoInputWrap)
				.append(self.formConcButtons);
		};
		this.addShelf=function(){
			self.launchButton=new Button('toolBox','',self.alias);
			self.createContent();
			self.shelf=main.layout.toolBox.createShelf({
				name:self.name,
				button:self.launchButton.html,
				content:self.html,
				alias:self.alias,
				groups:main.withs[self.withItem].toolbox_groups
			});
		};
		this.initGeocoder=function(){
			self.geocoder=new Geocoder({
				map:main.map,
				context:self,
				zoomTo:true
			});
		};
		(this.createHTML=function(){
			self.addShelf();
			self.initGeocoder();
		})();
	}
});