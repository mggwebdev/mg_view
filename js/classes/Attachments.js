define(function(require){
	return function pg(args){
		var self=this;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.export=function(files,path){
			if(!path){return;}
			var pkg=[];
			for(var i=0;i<files.length;i++){
				pkg.push({
					fileName:files[i].fileName,
					servFileName:files[i].servFileName,
					ext:files[i].ext
				});
			}
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
						pkg:pkg,
						attachPathName:path
					},
					action:'exportAttachments'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							window.open(response.absPath);
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
	}
});