define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var DataTable=require('./DataTable');
		var Geocoder=require('./Geocoder');
		var Layer2=require('./Layer2');
		var MapInteractions=require('./MapInteractions');
		var Panel=require('./Panel');
		var PopUp=require('./PopUp');
		var ToolTip=require('./ToolTip');
		var Wait=require('./Wait');
		var WaitLoad=require('./WaitLoad');
		this.templateStuff={
			templates:main.data.personal_settings.templates,
			with_templates:true
		};
		this.openEditFormAfterAdd=true;
		this.onePopUpAtTime=true;
		this.updateFilterLegend=false;
		// this.with_photos=true;
		this.popupsUneditable=true;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		// if(self.properties){
			// for(var key in self.properties){
				// self[key]=self.properties[key];
			// }
		// }
		this.movingFeature=null;
		this.matchedLayer=null;
		this.is_inventory=true;
		this.hasPopUp=null;
		this.hasPopUpOtherFeatures=null;
		this.hasPopUpCurrentIndex=null;
		this.popUpZ=null;
		this.addPolygonSource=null;
		this.addMultiLineStringSource=null;
		this.polygonDrawAdded=false;
		this.multiLineStringDrawAdded=false;
		this.blockMoveEndLoadOnce=false;
		this.currentFocused=null;
		this.lastFocused=null;
		main.map_layers[self.layerName].firstLoad=false;
		this.inventory=main.inventories[self.layerName];
		this.templateStuff.with_templates=self.inventory.properties.with_templates;
		this.singular_alias=self.inventory.properties.singular_alias || this.alias;
		this.plural_alias=self.inventory.properties.plural_alias || this.alias;
		this.with_add=self.inventory.properties.with_add;
		this.with_move=self.inventory.properties.with_move;
		this.addContent='Click the map to add a '+self.singular_alias;
		this.moveContent='Click and drag a '+self.singular_alias+' to move it.';
		// this.photoContent='Select a '+self.singular_alias+' to add a photo to it.';
		// this.addPhoto=function(){
		// };
		this.mapInteractions=new MapInteractions({
			context:self
		});
		if(self.inventory.properties.is_wom){
			this.addByAddress=true;
		}
		(this.testForBulkCount=function(){
			if(self.properties.special_connections && self.properties.special_connections.bulk_count){
				self.inventories.bulkCounts.push({countLayer:self.layerName,countField:self.properties.special_connections.bulk_count.to_effect,countedLayer:self.properties.special_connections.bulk_count.counting_layer});
			}
		})();
		this.clearControlledContent=function(){
			self.form=null;
			self.tableTbody=null;
			self.templateStuff.templatesHTML=null;
			self.templateStuff.templatePanel=null;
			self.addSC=null;
			self.addSCMobile=null;
			self.moveSC=null;
			self.moveSCMobile=null;
			self.editingSCPkg=null;
			self.addItemButton=null;
			self.moveItemButton=null;
			// self.addPhotoButton=null;
			self.table=null;
		};
		this.addEditingSCButs=function(){
			main.layout.header.headerRight.append(self.editingSCPkg);
		};
		this.removeEditingSCButs=function(){
			if(self.editingSCPkg){self.editingSCPkg.remove();}
		};
		this.templateStuff.templateSelected=function(template){
			self.currentTemplate=template;
		};
		this.templateStuff.promptTemplates=function(){
			self.promptingForTemplates=true;
			self.templateStuff.templatePanel.open();
			$('html').on('click',self.templateStuff.outTemplatePanelCheck);
		};
		this.templateStuff.outTemplatePanelCheck=function(e){
			/* if($(e.target).closest('.templatesPanel,.addItemButton').length==0){
				self.templateStuff.templatePanel.close();
			} */
		};
		this.addMethodPanelClose=function(){
			self.stopAdding();
		};
		this.templateStuff.templatePanelClose=function(){
			self.promptingForTemplates=false;
			if(!self.blockTemplatePanelCloseCB){
				self.addingOn();
			}else{
				self.stopAdding();
			}
			self.blockTemplatePanelCloseCB=false;
		};
		this.startAdding=function(){
			self.inventories.checkForOtherActions();
			self.addItemButton.changeIcon(main.mainPath+'images/stop.png');
			self.addItemButton.html.attr('title','Stop Adding '+self.singular_alias);
			if(self.inventory.properties.with_addsc){
				self.addSC.changeIcon(main.mainPath+'images/stop.png');
				self.addSCMobile.changeIcon(main.mainPath+'images/stop.png');
				self.addSC.html.add(self.addSCMobile.html).attr('title','Stop Adding '+self.singular_alias);
			}
			if(main.layout.header.mobileMenu.is(':visible')){main.layout.header.mobileMenu.slideUp(200);}
			self.checkEditFormsForButtons(true);
			if(self.templateStuff.with_templates){
				if(self.inventories.currentFilterPID && self.inventory.properties.template_lookup_table){
					self.currentTemplate=self.inventories.currentFilterPID;
					self.addingOn();
				}else{
					self.blockTemplatePanelCloseCB=true;
					self.templateStuff.promptTemplates();
				}
			}else{
				self.addingOn();
			}
		};
		this.addPolygonDraw=function(){
			if(!self.polygonDrawAdded){main.map.map.addInteraction(self.polygonDraw);}
			self.polygonDrawAdded=true;
			main.invents.drawing=true;
			main.flags.drawing=true;
			main.map.map.on('dblclick',main.utilities.mapDoubleClickWhenDrawing);
		};
		this.removePolygonDraw=function(){
			if(self.polygonDrawAdded){main.map.map.removeInteraction(self.polygonDraw);}
			self.polygonDrawAdded=false;
			main.invents.drawing=false;
			main.flags.drawing=false;
		};
		this.addMultiLineStringDraw=function(){
			if(!self.multiLineStringDrawAdded){main.map.map.addInteraction(self.multiLineStringDraw);}
			self.multiLineStringDrawAdded=true;
			main.invents.drawing=true;
			main.flags.drawing=true;
			main.map.map.on('dblclick',main.utilities.mapDoubleClickWhenDrawing);
		};
		this.removeMultiLineStringDraw=function(){
			if(self.multiLineStringDrawAdded){main.map.map.removeInteraction(self.multiLineStringDraw);}
			self.multiLineStringDrawAdded=false;
			main.invents.drawing=false;
			main.flags.drawing=false;
		};
		this.addingOn=function(){
			main.modes.editing=true;
			self.adding=true;
			self.inventories.adding=true;
			main.flags.adding=true;
			if(self.inventory.properties.geom_type=='Polygon' || self.inventory.properties.geom_type=='MultiPolygon'){
				if(!self.addPolygonSource){
					self.addPolygonSource = new ol.source.Vector({wrapX: false});
					self.polygonDraw=new ol.interaction.Draw({
						source: self.addPolygonSource,
						type: 'Polygon'
					});
					self.addPolygonSource.on('addfeature',function(e){
						if(main.utilities.isIntersecting(e.feature)){
							alert('Must be a non-intersecting polygon.');
							return;
						}
						main.invents.blockDoubleClick=true;
						self.removePolygonDraw();
						coords=e.feature.getGeometry().getCoordinates()[0];
						self.newItem(coords,null,null,null,null,self.inventory.properties.geom_type);
					});
				}
				self.addPolygonDraw();
			}else if(self.inventory.properties.geom_type=='MultiLineString'){
				if(!self.addMultiLineStringSource){
					self.addMultiLineStringSource = new ol.source.Vector({wrapX: false});
					self.multiLineStringDraw=new ol.interaction.Draw({
						source: self.addMultiLineStringSource,
						type: 'LineString'
					});
					self.addMultiLineStringSource.on('addfeature',function(e){
						main.invents.blockDoubleClick=true;
						self.removeMultiLineStringDraw();
						coords=e.feature.getGeometry().getCoordinates();
						self.newItem(coords,null,null,null,null,'MultiLineString');
					});
				}
				self.addMultiLineStringDraw();
			}else{
				if(!self.addByAddress){
					self.startAddByPoint();
				}else{
					if(!self.addMethodPanel){
						self.addMethodOptions=$('<div class="addMethodOptions">')
							.append($('<div class="addMethodOption" data-val="map">By Map</div>'))
							.append($('<div class="addMethodOption" data-val="address">By Address</div>'))
							.append($('<div class="addMethodOption none" data-val="fromForm">From Form</div>'));
						self.addMethodContent=$('<div class="addMethodContent">')
							.append(self.addMethodOptions);
						self.addMethodOptions.on('click','.addMethodOption',function(){
							var method=$(this).data('val');
							switch(method){
								case 'map':
									self.startAddByPoint();
									break;
								case 'address':
									self.startAddByAddress();
									break;
								case 'fromForm':
									self.startAddByForm();
									break;
							}
						});
						self.addMethodPanel=new Panel({
							content:self.addMethodContent,
							label:'Choose an Add Method',
							title:'Choose an Add Method',
							classes:'addMethodPanel',
							onClose:self.addMethodPanelClose
						});
					}
					if(main.invents.addingFromForm){self.addMethodOptions.find('.addMethodOption[data-val="fromForm"]').removeClass('none');
					}else{self.addMethodOptions.find('.addMethodOption[data-val="fromForm"]').addClass('none');}
					self.addMethodPanel.open();
				}
			}
		};
		this.startAddByPoint=function(){
			if(self.addMethodPanel){self.addMethodPanel.close(true);}
			main.map.mapContainer.addClass('adding');
			main.map.map.on('click',self.addingEvent);
			if(!main.browser.isMobile){
				self.toolTip.setContent(self.addContent);
				self.toolTip.activate();
			}
		};
		this.geocoded=function(coods,address){
			var mapCood=ol.proj.transform(coods, 'EPSG:4326', 'EPSG:3857');
			self.newItem(mapCood,null,null,null,null,null,true);
			if(self.properties.stop_adding_after_add){self.stopAdding();}
			if(self.addByAddressPanel){self.addByAddressPanel.close();}
		};
		this.addByAddressSearch=function(address){
			if(!self.geocoder){
				self.geocoder=new Geocoder({
					map:main.map,
					context:self
				});
			}
			self.geocoder.getCoodsFromAddress(self.geocoded,address);
		};
		this.geoFoundForAddFromForm=function(response){
			self.newItem(response.mainCoords);
		};
		this.startAddByForm=function(){
			if(main.invents.addingFromForm){
				main.utilities.getGeo(main.invents.addingFromForm.inventory.layerName,main.invents.addingFromForm.inventory.properties.internal_field_map.geometry_field,main.invents.addingFromForm.properties.pid,self.geoFoundForAddFromForm);
			}
		};
		this.startAddByAddress=function(){
			if(self.adding){self.stopAdding();}
			if(self.addMethodPanel){self.addMethodPanel.close(true);}
			if(!self.addByAddressPanel){
				self.addByAddressInput=$('<input type="text" class="addByAddressInput addByAddressEle"/>');
				self.addByAddressGoButton=new Button('std','addByAddressGoButton','Add');
				self.addByAddressInput.on('keyup',function(e){
					if(e.which==13){
						if(!self.addByAddressInput.val().trim()){return;}
						self.addByAddressSearch(self.addByAddressInput.val().trim());
					}
				});
				self.addByAddressGoButton.html.on('click',function(){
					if(!self.addByAddressInput.val().trim()){return;}
					self.addByAddressSearch(self.addByAddressInput.val().trim());
				});
				self.addByAddressButtons=$('<div class="addByAddressButtons addByAddressEle">').append(self.addByAddressGoButton.html);
				self.addByAddressWrap=$('<div class="addByAddressWrap">')
					.append(self.addByAddressInput)
					.append(self.addByAddressButtons);
				self.addByAddressPanel=new Panel({
					content:self.addByAddressWrap,
					label:'Add By Address',
					title:'Add By Address',
					classes:'addByAddressPanel',
					onClose:self.addByAddressPanelClose
				});
			}else{
				self.addByAddressInput.val('');
			}
			self.addByAddressPanel.open();
		};
		this.addByAddressPanelClose=function(){
			self.stopAdding();
		};
		this.editFormClosed=function(){
			if(self.inventory.properties.geom_type=='Polygon' || self.inventory.properties.geom_type=='MultiPolygon'){
				self.addPolygonDraw();
			}else if(self.inventory.properties.geom_type=='MultiLineString'){
				self.addMultiLineStringDraw();
			}else{
				main.map.mapContainer.addClass('adding');
				self.toolTip.activate();
			}
		};
		this.checkEditFormsForButtons=function(adding){
			var eFs=main.layout.editForms;
			for(var i=0;i<eFs.length;i++){
				if(eFs[i].womInvUnits){
					for(var key in eFs[i].womInvUnits){
						if(eFs[i].womInvUnits[key].invs.withAdd){
							if(adding){
								eFs[i].womInvUnits[key].addButton.html.addClass('none');
								eFs[i].womInvUnits[key].doneAddButton.html.removeClass('none');
							}else{
								eFs[i].womInvUnits[key].addButton.html.removeClass('none');
								eFs[i].womInvUnits[key].doneAddButton.html.addClass('none');
							}
							
						}
					}
				}
			}
		};
		this.closeAddPanels=function(){
			if(self.addByAddressPanel && self.addByAddressPanel.opened){self.addByAddressPanel.close(true);}
			if(self.addMethodPanel && self.addMethodPanel.opened){self.addMethodPanel.close(true);}
		};
		this.stopAdding=function(){
			self.closeAddPanels();
			if(self.addItemButton){
				self.addItemButton.changeIcon(main.mainPath+'images/pencil.png');
				self.addItemButton.html.attr('title','Add '+self.singular_alias);
			}
			if(self.addSC){
				self.addSC.changeIcon(main.mainPath+'images/pencil.png');
				self.addSCMobile.changeIcon(main.mainPath+'images/pencil.png');
				self.addSC.html.add(self.addSCMobile.html).attr('title','Add '+self.singular_alias);
			}
			main.modes.editing=false;
			self.adding=false;
			self.inventories.addingFromForm=null;
			self.checkEditFormsForButtons(false);
			self.inventories.adding=false;
			main.flags.adding=false;
			if(self.inventory.properties.geom_type=='Polygon' || self.inventory.properties.geom_type=='MultiPolygon'){
				self.removePolygonDraw();
				main.map.map.un('dblclick',main.utilities.mapDoubleClickWhenDrawing);
			}else if(self.inventory.properties.geom_type=='MultiLineString'){
				self.removeMultiLineStringDraw();
				main.map.map.un('dblclick',main.utilities.mapDoubleClickWhenDrawing);
			}else{
				main.map.mapContainer.removeClass('adding');
				main.map.map.un('click',self.addingEvent);
				if(!main.browser.isMobile){
					self.toolTip.deActivate();
				}
			}
		};
		this.startMoving=function(){
			if(self.inventory.map_layer.layer){
				self.moveItemButton.changeIcon(main.mainPath+'images/stop.png');
				self.moveItemButton.html.attr('title','Stop Moving '+self.singular_alias);
				if(self.inventory.properties.with_movesc){
					self.moveSC.changeIcon(main.mainPath+'images/stop.png');
					self.moveSCMobile.changeIcon(main.mainPath+'images/stop.png');
					self.moveSC.html.add(self.moveSCMobile.html).attr('title','Stop Moving '+self.singular_alias);
				}
				if(main.layout.header.mobileMenu.is(':visible')){main.layout.header.mobileMenu.slideUp(200);}
				self.inventories.checkForOtherActions();
				self.moving=true;
				self.inventories.moving=true;
				if(!main.browser.isMobile){
					self.toolTip.setContent(self.moveContent);
					self.toolTip.activate();
					self.inventory.map_layer.layer.setDraggable(true);
				}
				// else{main.map.map.on('click',self.movingEvent);}
			}
			// self.map.disableDragPan();
		};
		this.moved=function(e,feature,layer){
			var cood=e.coordinate;
			var changeFields=[];
			if(self.movingFeature){
				var features=self.layer.layer.olLayer.getSource().getFeatures();
				feature=null;
				for(var i=0;i<features.length;i++){
					if(features[i].get('pid')==self.movingFeature){
						feature=features[i];
						break;
					}
				}
				if(feature){
					var geo=feature.getGeometry();
					geo.setCoordinates(cood);
					feature.setGeometry(geo);
					feature.set('isActive',true);
					self.layer.layer.olLayer.getSource().removeFeature(feature);
					self.layer.layer.olLayer.getSource().addFeature(feature);
					self.layer.layer.olLayer.setSource(self.layer.layer.olLayer.getSource());
					// main.loader.loadMoreMapPoints(self.layerName);
				}
			}
			if(!feature){return false;}
			var props=feature.getProperties();
			var row=self.inventory.map_layer.features[props.pid];
			if(!row.fields){self.mapInteractions.createFeatureHTML(props);}
			if(row.popup){
				if(row.popup.visible){row.popup.setPosition(cood);}
				else{row.popup.setCood(cood);}
			}
			var latlng=ol.proj.transform(cood, 'EPSG:3857', 'EPSG:4326');
			if(row.fields.lat && row.fields.lng && props.lat!==undefined && row.properties.lat!==undefined){
				row.fields.lat.current_val=latlng[1];
				row.fields.lng.current_val=latlng[0];
				changeFields.push({field:self.fields['lat'],val:latlng[1]},{field:self.fields['lng'],val:latlng[0]});
			}
			var latlngStr=latlng[1]+','+latlng[0];
			var fieldMap=self.inventory.properties.internal_field_map;
			if(fieldMap && fieldMap.geometry_field && self.inventory.map_layer.fields[fieldMap.geometry_field] && self.inventory.map_layer.fields[fieldMap.geometry_field].sub_data_type && self.inventory.map_layer.fields[fieldMap.geometry_field].sub_data_type.toLowerCase()=='point' && row.fields[fieldMap.geometry_field].current_val){
				row.fields[fieldMap.geometry_field].current_val.coordinates[0]=cood[0];
				row.fields[fieldMap.geometry_field].current_val.coordinates[1]=cood[1];
				changeFields.push({field:self.fields[fieldMap.geometry_field],val:row.fields[fieldMap.geometry_field].current_val});
			}
			if(confirm('Do you want to update your address?')){
				var dontUpdateAddress=false;
				if(fieldMap){
					if(fieldMap.address_num_street && self.fields[fieldMap.address_num_street]){changeFields.push({field:self.fields[fieldMap.address_num_street],val:null});}
					if(fieldMap.address_number && self.fields[fieldMap.address_number]){changeFields.push({field:self.fields[fieldMap.address_number],val:null});}
					if(fieldMap.street && self.fields[fieldMap.street]){changeFields.push({field:self.fields[fieldMap.street],val:null});}
					if(fieldMap.street_tree_on && self.fields[fieldMap.street_tree_on]){changeFields.push({field:self.fields[fieldMap.street_tree_on],val:null});}
					if(fieldMap.city && self.fields[fieldMap.city]){changeFields.push({field:self.fields[fieldMap.city],val:null});}
					if(fieldMap.zip && self.fields[fieldMap.zip]){changeFields.push({field:self.fields[fieldMap.zip],val:null});}
					if(fieldMap.state && self.fields[fieldMap.state]){changeFields.push({field:self.fields[fieldMap.state],val:null});}
				}
			}else{var dontUpdateAddress=true;}
			// self.updateData([props.pid],null,null,null,true,latlngStr,[feature],dontUpdateAddress,changedFields);
			// changeFields=[{fieldName:fieldName,val:value}];
			main.dataEditor.updateData({
				pids:[props.pid],
				context:self,
				dontUpdateCharts:true,
				latlng:latlngStr,
				features:[feature],
				changeFields:changeFields
			});
			if(main.browser.isMobile){self.stopMoving();}
		};
		this.stopMoving=function(){
			self.moveItemButton.changeIcon(main.mainPath+'images/move.png');
			self.moveItemButton.html.attr('title','Move '+self.singular_alias);
			if(self.inventory.properties.with_movesc){
				self.moveSC.changeIcon(main.mainPath+'images/move.png');
				self.moveSCMobile.changeIcon(main.mainPath+'images/move.png');
				self.moveSC.html.add(self.moveSCMobile.html).attr('title','Move '+self.singular_alias);
			}
			self.moving=false;
			self.inventories.moving=false;
			if(!main.browser.isMobile){
				self.toolTip.deActivate();
			}
			if(!main.browser.isMobile){self.inventory.map_layer.layer.setDraggable(false);}
			else{
				self.movingFeature=null;
				main.flags.movingFeature=null;
				self.movingLayer=null;
				main.map.mapContainer.removeClass('adding');
				main.map.map.un('click',self.movingEvent);
			}
			// self.map.enableDragPan();
		};
		/* this.startSelectingForPhoto=function(){
			self.inventories.checkForOtherActions();
			self.selectingForPhoto=true;
			self.toolTip.setContent(self.photoContent);
			self.toolTip.activate();
		}; */
		/* this.stopSelectingForPhoto=function(){
			self.selectingForPhoto=false;
			self.toolTip.deActivate();
		}; */
		this.startMovingInit=function(){
			if(!self.moving){self.startMoving();
			}else{self.stopMoving();}
		};
		this.startAddingInit=function(){
			if(self.inventories.currentHighlightFilterLayer){
				main.invents.resetHighlightFilter();
				main.utilities.updateFilterLegend();
			}
			self.closeAddPanels();
			main.layout.closeAllSidePanels(null,['inventory']);
			main.map.adjustMapSize();
			if(!self.bulkCountCheck()){
				if(main.filter_legend && main.filter_legend.layerName!=self.layerName && main.filter_legend.layers[self.layerName]){
					main.filter_legend.changeLayer(self.layerName);
				}
				if(!self.adding){
					if(!self.promptingForTemplates){
						self.startAdding();
					}else{
						self.stopAdding();
						self.templateStuff.templatePanel.close();
					}
				}else{self.stopAdding();}
			}else{
				alert('Apologies, no adding data while in "Bulk Count Mode".');
			}
		};
		this.notLoadedForAdd=function(verb){
			var dependentUponInventory=self.inventories.inventories[self.inventory.properties.dependent_upon];
			var flOption='';
			if(main.filter_legend && main.filter_legend.active){flOption=' or select the '+dependentUponInventory.singular_alias.toLowerCase()+' from the legend';}
			alert('No '+dependentUponInventory.plural_alias.toLowerCase()+' selected yet. To '+verb+' '+self.plural_alias.toLowerCase()+', select a '+dependentUponInventory.singular_alias.toLowerCase()+' by clicking a marker on the map and selecting "Initiate"'+flOption+'.');
		};
		this.createHTML=function(){
			self.form=$('<form>').addClass('inventoryForm');
			self.tableTbody=$('<tbody>');
			//editing SCs
			if(self.templateStuff.with_templates){
				self.templateStuff.templatesHTML=$('<div class="templates">');
				self.templateStuff.templatesHTML.append(
					$('<div class="templateUnit notemplate cf" data-notemplate="1">').append(
						$('<div class="templateLabel fLeft notemplate">').html('No Template')
					)
				)
				if(self.inventory.properties.template_lookup_table){
					var lUTable=main.data.lookUpTables[self.inventory.properties.template_lookup_table];
					var lUTableInternalMap=main.tables[self.inventory.properties.template_lookup_table].properties.internal_field_map;
					var alias=lUTableInternalMap.lookUpTableAlias;
					var items=lUTable.items,label;
					for(var key in items){
						label=items[key][alias];
						if(!label){label=items[key].pid}
						self.templateStuff.templatesHTML.append(
							$('<div class="templateUnit fromLookUpTable cf" data-fromlookup="true" data-template="'+key+'">').append(
								$('<div class="templateLabel fLeft template_'+key+'">').html(label)
							)
						);
					}
				}else{
					for(var key in self.templateStuff.templates){
						self.templateStuff.templatesHTML.append(
							$('<div class="templateUnit cf" data-template="'+key+'">').append(
								$('<div class="templateLabel fLeft template_'+key+'">').html(self.templateStuff.templates[key].alias)
							)
						)
					}
				}
				self.templateStuff.templatePanel=new Panel({
					content:self.templateStuff.templatesHTML,
					label:'Choose a Type',
					title:'Templates',
					classes:'templatesPanel',
					onClose:self.templateStuff.templatePanelClose
				});
				self.templateStuff.templatesHTML.on('click','.templateUnit',function(){
					if(!$(this).data('notemplate')){
						self.promptingForTemplates=false;
						if($(this).data('fromlookup') && ($(this).data('fromlookup')=='true' || $(this).data('fromlookup')===true)){
							self.templateStuff.templateSelected($(this).data('template'));
						}else{
							self.templateStuff.templateSelected(self.templateStuff.templates[$(this).data('template')]);
						}
					}
					self.blockTemplatePanelCloseCB=false;
					self.templateStuff.templatePanel.close();
					self.addingOn();
				});
			}
			self.editingSCPkg=$('<div class="editingSCPkg templatesSCPkg cf">');
			if(self.layerName==self.inventories.default_inventory){self.addEditingSCButs();}
			if(self.with_add){
				self.addItemButton=new Button('icon','addItemButton inventEditButton','',null,null,main.mainPath+'images/pencil.png');
				self.addItemButton.html.attr('title','Add '+self.singular_alias);
				self.tableTbody.append($('<tr>')
					.append($('<td>')
						.append($('<div class="tdLiner">').html(self.addItemButton.html))
					)
					.append($('<td>')
						.append($('<div class="tdLiner">').html('Add New '+self.singular_alias+' (click in map to add point)'))
					)
				)
				self.addItemButton.html.on('click',function(){
					self.beginAdding($(this));
				});
				if(self.inventory.properties.with_addsc){
					self.addSC=new Button('roundedSquare','iconButton headerButton addSC templatesAddSC','Add',null,null,main.mainPath+'images/pencil.png','Add '+self.singular_alias);
					self.editingSCPkg.prepend(self.addSC.html);
					self.addSCMobile=main.layout.header.createMobileMenuItem('addSC','Add',[['button','addSC']],main.mainPath+'images/pencil.png','Add');
					main.layout.header.mobileMenu.append(self.addSCMobile.html);
					self.addSC.html.add(self.addSCMobile.html).on('click',function(){
						if(!self.inventory.properties.dependent_upon || (self.inventory.properties.dependent_upon && self.inventory.map_layer.layer)){self.startAddingInit();
						}else{self.notLoadedForAdd('add');}
					});
				}
			}
			if(self.with_move){
				self.moveItemButton=new Button('icon','moveItemButton inventEditButton','',null,null,main.mainPath+'images/move.png');
				self.moveItemButton.html.attr('title','Move '+self.singular_alias);
				self.tableTbody.append($('<tr>')
					.append($('<td>')
						.append($('<div class="tdLiner">').html(self.moveItemButton.html))
					)
					.append($('<td>')
						.append($('<div class="tdLiner">').html('Move '+self.singular_alias))
					)
				)
				self.moveItemButton.html.on('click',function(){
					self.beginMoving($(this));
				});
				if(self.inventory.properties.with_movesc){
					self.moveSC=new Button('roundedSquare','iconButton headerButton moveSC templatesMoveSC','Move',null,null,main.mainPath+'images/move.png','Move '+self.singular_alias);
					self.editingSCPkg.prepend(self.moveSC.html);
					self.moveSCMobile=main.layout.header.createMobileMenuItem('moveSC','Move',[['button','moveSC']],main.mainPath+'images/move.png','Move');
					main.layout.header.mobileMenu.append(self.moveSCMobile.html);
					self.moveSC.html.add(self.moveSCMobile.html).on('click',function(){
						if(self.inventory.properties[main.account.loggedInUser.user_type+'_editable']){
							if(!self.inventory.properties.dependent_upon || (self.inventory.properties.dependent_upon && self.inventory.map_layer.layer)){self.startMovingInit();
							}else{self.notLoadedForAdd('move');}
						}else{
							alert('This requires a log in. Please log in.');
							// open login
							main.utilities.openForm('logIn','account',true);
						}
					});
				}
			}
			// if(self.with_photos){
				/* self.addPhotoButton=new Button('icon','','',null,null,main.mainPath+'images/camera.png');
				self.tableTbody.append($('<tr>')
					.append($('<td>')
						.append($('<div class="tdLiner">').html(self.addPhotoButton.html))
					)
					.append($('<td>')
						.append($('<div class="tdLiner">').html('Add Photo of '+self.singular_alias))
					)
				)
				self.addPhotoButton.html.on('click',function(){
					if(!self.selectingForPhoto){self.startSelectingForPhoto();
					}else{self.stopSelectingForPhoto();}
				}); */
			// }
			self.html=$('<div>').addClass('inventoryContainer')
				.append($('<div>').addClass('inventoryContainerLabel').html(self.plural_alias));
			if(self.inventory.properties.in_editor_desc){self.html.append($('<div class="in_editor_desc">').html(self.inventory.properties.in_editor_desc));}
			self.table=$('<table>').addClass('inventoryTable')
				.append(self.tableTbody);
			self.html.append($('<div>').addClass('inventoryContainerContent').html(self.table));
			return self.html;
		};
		this.beginMoving=function(context){
			if(self.inventory.properties[main.account.loggedInUser.user_type+'_editable']){
				if(!self.inventory.properties.dependent_upon || (self.inventory.properties.dependent_upon && self.inventory.map_layer.layer)){self.startMovingInit();
				}else{self.notLoadedForAdd('move');}
			}else{
				alert('This requires a log in. Please log in.');
				// open login
				main.utilities.openForm('logIn','account',true);
			}
		};
		this.beginAdding=function(context){
			self.closeAddPanels();
			if(self.inventory.properties[main.account.loggedInUser.user_type+'_editable']){
				if(!self.inventory.properties.dependent_upon || (self.inventory.properties.dependent_upon && self.inventory.map_layer.layer)){
					if(main.browser.isMobile && context){
						main.layout.sidePanels[context.closest('.sidePanel').data('button')].close();
						main.map.adjustMapSize();
					}
					self.startAddingInit();
				}else{self.notLoadedForAdd('add');}
			}else{
				alert('This requires a log in. Please log in.');
				// open login
				main.utilities.openForm('logIn','account',true);
			}
		};
		this.dataTableAllocate=function(){
			if(self.dataTable){
				if(!self.properties.is_wom){
					self.dataTable.remove(self.inventories.accordian);
				}else if(self.inventories.womAccordian){
					self.dataTable.remove(self.inventories.womAccordian);
				}
			}
			if(self.launchButton){
				if(self.inventory.properties[main.account.loggedInUser.user_type+'_dt_visible'] && self.properties.active){
					self.dataLaunch=self.launchButton.appendExtras('data');
					self.dataTable=new DataTable({
						title:'',
						table:self.layerName,
						caller:self,
						callerProperties:self.inventory.properties,
						launchButton:self.dataLaunch
					});
					main.dataTables[self.layerName]=self.dataTable;
					if(!self.properties.is_wom){
						self.dataContentWrap=self.inventories.accordian.createDataShelf(self.dataTable);
					}else if(self.inventories.womAccordian){
						self.dataContentWrap=self.inventories.womAccordian.createDataShelf(self.dataTable);
					}
					self.dataContentWrap.closest('.shelf').addClass('dataExtended');
				}/* else{
					self.dataTable.remove();
				} */
			}
		};
		this.launchWithoutPermission=function(){
			alert('This requires a log in. Please log in.');
			main.utilities.openForm('logIn','account',true);
		};
		(this.create=function(){
			var inventoryName=self.layerName;
			// self.fields=self.inventory.fields;
			self.layer=self.inventory.map_layer;
			if(!self.inventory.properties.dont_add_to_side_panel){
				self.launchButton=new Button('toolBox','',self.plural_alias);
				if(self.inventory.properties[main.account.loggedInUser.user_type+'_editable']){
					self.content=self.createHTML();
				}else{
					self.content='';
				}
				var props0={
					name:inventoryName,
					button:self.launchButton.html,
					content:self.content,
					alias:self.plural_alias,
					description:self.inventory.properties.description,
					place_in_order:self.inventory.properties.place_in_order
				};
				if(!self.properties.is_wom){
					self.shelf=self.inventories.accordian.createShelf(props0);
				}else if(self.inventories.womAccordian && self.properties.active){
					self.shelf=self.inventories.womAccordian.createShelf(props0);
				}
				if(!self.content){
					self.launchButton.html.off('click',self.launchWithoutPermission);
					self.launchButton.html.on('click',self.launchWithoutPermission);
				}
				// self.inventories.accordian.container.append(self.shelf);
				if(!main.browser.isMobile){
					self.toolTip=new ToolTip({
						content:self.addContent
					});
				}
				// /* if(!self.inventory.properties.dependent_upon){
					self.dataTableAllocate();
				// } */
				if(self.inventory.properties.shortcut){
					var shortcut_name=self.inventory.properties.shortcut_name || '';
					var shortcut_attr_title=self.inventory.properties.shortcut_attr_title || '';
					var shortcut_image=self.inventory.properties.shortcut_image || '';
					self.shortCut=main.layout.header.createHeaderButton('iconButton shortCutButton '+self.inventory.properties.name,shortcut_name,[['button',self.inventories.sidePanel.container.data('button')],['accordian',self.inventories.accordian.id],['form',self.inventory.properties.name]],'images/'+shortcut_image,shortcut_attr_title);
					main.layout.header[self.inventory.properties.shortcut].append(self.shortCut.html);
					self.shortCut.html.click(function(){
						main.utilities.openForm($(this).data('form'),$(this).data('accordian'));
					});
				}
			}
		})();
		this.updateLookUpTableFields=function(params){
		};
		this.newSignature=function(params){
		};
		this.dbPhotoRemoved=function(response){
			// if(!updateNoOtherTables && main.tables[self.layerName].map_layer.features[pid] && main.tables[self.layerName].map_layer.features[pid].dataTable){
				// main.tables[self.layerName].map_layer.features[pid].dataTable.updateDataTable(pid,fieldName,primaryE,false,true,true);
			// }
		};
		this.photoRemoved=function(args){
			if(args.params.pid){
				main.utilities.removePhoto(args.name,args.params.field.referencer,args.params.field.name,args.params.pid,self.dbPhotoRemoved);
			}
		};
		this.countOpenEditForms=function(){
			return $('.editFormPanel:visible:not(.minimized)').length;
		};
		this.addItemFromForm=function(item,field_map,context){
			if(field_map && !self.countOpenEditForms()){
				self.newItem(/* null */null,item,field_map,context);
				if(self.adding && self.properties.stop_adding_after_add){self.stopAdding();}
				main.map.mapContainer.removeClass('adding');
				if(!main.browser.isMobile){self.toolTip.deActivate();}
			}
		};
		this.bulkCountCheck=function(){
			var a=false;
			if(self.inventory.properties.special_connections && self.inventories.currentFilterPID){
				var sc=self.inventory.properties.special_connections;
				for(var key in sc){
					if(sc[key].type=='count'){
						if(main.utilities.doSpecialConnection(null,sc[key],null,true)){
							a=true;
							break;
						}
					}
					// if(sc[key].type=='count'){
						// var features=self.inventories.inventories[sc[key].to_effect_layer].inventory.map_layer.features;
						// if(features && (features[self.inventories.currentFilterPID].properties[sc[key].dont_count_if]!==false && features[self.inventories.currentFilterPID].properties[sc[key].dont_count_if]!==null)){
							// a=true;
							// break;
						// }
					// }
				}
			}
			return a;
		};
		this.newItem=function(cood,ref,field_map,context,geom,type,fromAddress,transferMap,dontPanTo,cb){
			if(self.bulkCountCheck()){
				alert('Apologies, no adding data while in "Bulk Count Mode".');
				return;
			}
			main.layout.loader.loaderOn();
			var pkg={};
			var fields=self.fields,value;
			var defaults=null,dontLookUpAddress=false;
			
			if(self.currentTemplate){
				if(self.inventory.properties.template_lookup_table){
					if(self.inventory.properties.template_defaults){
						if(self.inventory.properties.template_defaults.defaults){
							var defaults=self.inventory.properties.template_defaults.defaults;
						}
						if(self.inventory.properties.template_defaults.correlations){
							var lUTable=main.data.lookUpTables[self.inventory.properties.template_lookup_table];
							var items=lUTable.items;
							var item=items[self.currentTemplate];
							var correlations=self.inventory.properties.template_defaults.correlations;
							for(var key in correlations){
								defaults[key]=item[correlations[key]];
							}
						}
					}
				}else{
					defaults=self.currentTemplate.defaults;
				}
			}
			for(var key in fields){
				if(fields[key].active && fields[key].in_create && fields[key].input_type!='html'){
					value=fields[key].default_val;
					if(defaults && defaults[key]!==undefined){value=defaults[key];}
					if(field_map && ref && field_map[key] && ref[field_map[key]]!==undefined){
						value=ref[field_map[key]];
					}
					if(self.inventories.currentFiltered==self.layerName && self.inventories.currentFilterPID && key==self.properties.internal_field_map.dependent_upon_field){
						// if(main.tables[self.inventories.currentFilterLayer].map_layer.features && main.tables[self.inventories.currentFilterLayer].map_layer.features[self.inventories.currentFilterPID]){
							// var feature=main.tables[self.inventories.currentFilterLayer].map_layer.features[self.inventories.currentFilterPID];
							// if(fields[key].lookup_table){value=feature.properties.pid;
							// }else{value=feature.properties[self.inventory.properties.field_map[self.inventories.currentFilterLayer].fields[key]];}
							value=self.inventories.currentFilterPID;
						// }
					}
					if(self.inventory.properties.internal_field_map){
						if(self.inventory.properties.internal_field_map.last_modified && self.inventory.properties.internal_field_map.last_modified==key){
							value=new Date().getTime();
						}
						if(self.inventory.properties.internal_field_map.date_created && self.inventory.properties.internal_field_map.date_created==key){
							value=new Date().getTime();
						}
						if(self.inventory.properties.internal_field_map.user_field && main.account && main.account.loggedInUser && self.inventory.properties.internal_field_map.user_field==key){
							if(fields[key].lookup_table){value=main.account.loggedInUser.pid;
							}else{value=main.account.loggedInUser.username;}
						}
						if(self.inventory.properties.internal_field_map.org_field && main.account && main.account.loggedInUser && self.inventory.properties.internal_field_map.org_field==key){
							if(fields[key].lookup_table){value=main.account.loggedInUser.org_data.orgId;
							}else{value=main.account.loggedInUser.org_data.organization;}
						}
					}
					pkg[key]={
						input_type:fields[key].input_type,
						value:value,
						data_type:fields[key].data_type,
						sub_data_type:fields[key].sub_data_type,
						name:key
					};
					if(fields[key].data_type=='geometry'){
						if(cood){
							if(!type || type=='Point'){
								if(!geom || geom=='Point'){
									if(fields[key].sub_data_type=='point'){
										pkg[key].value={
											type:'Point',
											coordinates:cood
										}
									}
								}else if(geom=='Polygon'){
									pkg[key].value={
										type:'Point',
										coordinates:cood
									}
								}
							}else if(type=='Polygon'){
									pkg[key].value={
										type:'Polygon',
										coordinates:[cood]
									}
							}else if(type=='MultiPolygon'){
									pkg[key].value={
										type:'MultiPolygon',
										coordinates:[[cood]]
									}
							}else if(type=='MultiLineString'){
									pkg[key].value={
										type:'MultiLineString',
										coordinates:[cood]
									}
							}
						}else{
							if(value!==null && typeof value=='object'){
								value.coordinates=ol.proj.transform(value.coordinates, 'EPSG:4326', 'EPSG:3857');
								cood=value.coordinates;
								dontLookUpAddress=true;
							}else if(value===null){
								return;
							}
						}
					}
				}
			}
			var latlngStr=null;
			var fieldMap=self.layer.properties.internal_field_map || null;
			if(cood){
				if(!type || type=='Point'){
					var latlng=ol.proj.transform(cood, 'EPSG:3857', 'EPSG:4326');
					if(pkg['lat']){
						pkg['lat'].value=latlng[1];
						pkg['lng'].value=latlng[0];
					}
					latlngStr=latlng[1]+','+latlng[0];
				}
				var limit_tos=self.inventory.properties.limit_tos || null;
				var where=null;
				if(pkg['category_unique_id'] && !main.account.loggedInUser.dont_filter && self.inventories.currentFilterPID){
					where=self.inventory.properties.dependent_upon+"='"+self.inventories.currentFilterPID+"'";
				}
				dontPanTo=dontPanTo || null;
				$.ajax({
					type:'POST',
					url:main.mainPath+'server/db.php',
					data:{
						params:{
							folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
							pkg:pkg,
							latlng:latlngStr,
							fieldMap:fieldMap,
							limit_tos:limit_tos,
							dontLookUpAddress:dontLookUpAddress,
							where:where,
							type:type,
							fromAddress:fromAddress,
							dontPanTo:dontPanTo,
							table:self.layerName
						},
						action:'addInventoryItem'
					},
					success:function(response){
						if(response){
							response=JSON.parse(response);
							if(response.status=="OK"){
								var result=response.results[0];
								if(context && context.properties.internal_field_map && context.properties.internal_field_map.inv_pid){
									context.updatePID(result.pid,ref.pid);
								}
								if(main.invents.addingFromForm && main.invents.addingFromForm.linkOppWOMOnAdd){
									main.invents.addingFromForm.addInvItems([result.pid],self.layerName);
								}
								if(self.inventory.map_layer.features){
									self.inventory.map_layer.features[result.pid]={
										properties:result,
										dataTable:self.dataTable
									}
									// self.createFeatureHTML(result);
									//stafford -8690396.894530347, 5312996.443558295
									// var cood=[-12769259.402501449, 5304408.4614361115];
									if(!response.params.type || response.params.type=='Point'){
										var iconFeature=new ol.Feature({
											geometry: new ol.geom.Point(cood),
											style:self.layer.layer.style.createNewStyle()
										});
									}else if(response.params.type=='Polygon'){
										var iconFeature=new ol.Feature({
											geometry: new ol.geom.Polygon([cood]),
											style:self.layer.layer.style.createNewStyle()
										});
									}else if(response.params.type=='MultiPolygon'){
										var iconFeature=new ol.Feature({
											geometry: new ol.geom.MultiPolygon([[cood]]),
											style:self.layer.layer.style.createNewStyle()
										});
									}else if(response.params.type=='MultiLineString'){
										var iconFeature=new ol.Feature({
											geometry: new ol.geom.MultiLineString([cood]),
											style:self.layer.layer.style.createNewStyle()
										});
									}
									iconFeature.setProperties(result);
									iconFeature.set('appProps',{});
									self.inventory.map_layer.features[result.pid].feature=iconFeature;
									self.layer.layer.olLayer.getSource().addFeature(iconFeature);
									self.layer.layer.olLayer.setSource(self.layer.layer.olLayer.getSource());
									if(!response.params.dontPanTo){self.map.panTo(main.utilities.getCoords(iconFeature));}
									main.loader.loadMoreMapPoints(self.layerName,null,true);
									// var id=main.utilities.generateUniqueID();
									// iconFeature.setProperties({'layername':self.layerName,'type':'inventory','id':id});
									// iconFeature.setId(5555);
								// main.layer=self.layer.layer.olLayer;
						// main.iconFeature=iconFeature;
						// main.source=self.source;
									self.lastFocused=self.currentFocused;
									self.currentFocused=iconFeature;
									self.mapInteractions.showPopup(result.pid,iconFeature,null,true);
									if((self.adding || response.params.fromAddress) && self.openEditFormAfterAdd){
										if(!main.invents.addingFromForm){
											main.layout.closeAllEditForms();
											var linkTo=null;
										}else{
											var linkTo=main.invents.addingFromForm;
										}
										self.inventory.map_layer.features[result.pid].popup.createEditForm(linkTo);
										self.inventory.map_layer.features[result.pid].popup.editForm.open();
										self.inventory.map_layer.features[result.pid].popup.hide();
										if(main.invents.addingFromForm){
											self.stopAdding();
										}
									}
									main.mapItems.unSetActives();
									iconFeature.set('isActive',true);
									main.mapItems.hasActive=result.pid;
									main.mapItems.hasActiveLayer=self.layerName;
								}
								if(!self.bulkCountCheck()){
									var sc=self.inventory.properties.special_connections;
									for(var key in sc){
										if(sc[key].type=='count'){
											main.utilities.doSpecialConnection(null,sc[key],null,null,self.layerName);
										}
									}
								}
								if(main.filter_legend && main.filter_legend.active && main.filter_legend.layerName){main.filter_legend.refresh();}
								if(main.dashboard && main.dashboard.active){main.dashboard.refresh();}
								
								if(self.dataTable && self.dataTable.loaded){
									self.dataTable.addData(result);
									self.dataTable.html.find('.dtRow_'+result.pid).addClass('activeRow');
								}
								if(main.data.lookUpTables[self.layerName]){
									main.data.lookUpTables[self.layerName].items[result.pid]=result;
								}
								if(cb){cb({result:result,table:self.layerName});}
							}else{
								if(response.ERR_CODE==1){main.utilities.loggedOutNotice();}
								else{alert('Error: '+response.message);}
							}
						}
						main.layout.loader.loaderOff();
					}
				});
			}
		};
		this.reIndex=function(pid,is_depended_upon_by){
			if(!confirm('This will overwite the category specific unique identifier. Are you sure you want to continue?')){return;}
			var where=self.layerName+"='"+pid+"'";
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						table:is_depended_upon_by,
						where:where
					},
					action:'reIndex'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
						}
					}
				}
			});
		};
		this.deleteAll=function(){
			main.utilities.deleteAllData(self.layerName);
		};
		this.inputChanged=function(thiss,e,updatedCB){
			// var value=thiss.val();
			// var pid=thiss.closest('.popupTable').data('pid');
			// var field=thiss.closest('.editsWrap').data('field');
			/* if(thiss.attr('type')=='checkbox'){
				value='';
				thiss.closest('.checkboxesWrap').find('.checkboxInput').each(function(){
					if($(this).prop('checked')){value+=$(this).val()+';;';}
				});
				value=value.replace(/;;([^;;]*)$/,'$1');
			} */
			self.updateTable(thiss.closest('.popupTable,.editFormTabs').data('pid'),thiss.closest('.editsWrap').data('field'),thiss,true,false,null,e,updatedCB);
		};
		this.updateTable=function(pid,fieldName,primaryE,updateDB,updateNoOtherTables,fromPopUp,e,updatedCB){
			var value,html;
			var featureItems=self.inventory.map_layer.features[pid];
			if(!featureItems.fields){
				self.mapInteractions.createFeatureHTML(self.inventory.map_layer.features[pid].properties);
			}
			var fieldsItems=featureItems.fields[fieldName];
			var field=fieldsItems.field;
			var input_type=fieldsItems.field.input_type,valSplit;
			if(featureItems.editTable){var smallE=featureItems.editTable.table.find('.popUpRow.'+fieldName+' .formInput');}
			if(featureItems.editLargeTable){var largeE=featureItems.editLargeTable.formWrap.find('.popUpRow.'+fieldName+' .formInput');}
			var primaryEVal=primaryE.val();
			if(self.inventory.properties.special_connections && self.inventory.properties.special_connections[field.name]){
				var specialConnection=self.inventory.properties.special_connections[field.name];
				var type=specialConnection.type;
				if(type=='disable'){main.utilities.doSpecialConnection(null,specialConnection,primaryE,null,self.layerName,pid);}		
			}		
			// if(fromLargeEditForm){var primaryE=largeE}else{var primaryE=smallE}
			// if(!fromLargeEditForm){
				// var smallE=featureItems.editTable.table.find('.popUpRow.'+fieldName+' .formInput');
			// }else{var largeE=featureItems.editLargeTable.table.find('.popUpRow.'+fieldName+' .formInput');}
			
			//get db value and visible html
			if(field.input_type=='checkbox'){
				var chkStuff=main.utilities.getCheckboxValue(primaryE.closest('.checkboxesWrap').find('.checkboxInput'),field);
				value=chkStuff.value;
				html=chkStuff.html;
				// html='';
				var fileChanged=null;
				if(smallE){
					if(value && typeof value=='string'){
						main.utilities.setCheckboxes(smallE.closest('.checkboxesWrap').find('.checkboxInput'),value.split(','),field);
					}else if(field.data_type=='boolean'){
						if(value){
							if(field.options['true'] && field.options['true'].alias){html=field.options['true'].alias;}else{html='True';}
							value=true;
							smallE.prop('checked',true);
						}else{
							if(field.options['true'] && field.options['true'].offAlias){html=field.options['true'].offAlias;}else{html='False';}
							value=false;
							smallE.prop('checked',false);}
					}
					// main.utilities.setCheckboxes(smallE.closest('.checkboxesWrap').find('.checkboxInput'),value.split(';;'),field);
				}
				if(largeE){
					if(value && typeof value=='string'){
						main.utilities.setCheckboxes(largeE.closest('.checkboxesWrap').find('.checkboxInput'),value.split(','),field);
					}else if(field.data_type=='boolean'){
						if(value){
							// html=field.options['true'].alias;
							largeE.prop('checked',true);
						}else{largeE.prop('checked',false);	}
					}
					// main.utilities.setCheckboxes(largeE.closest('.checkboxesWrap').find('.checkboxInput'),value.split(';;'),field);
				}
			}else if(field.input_type=='radio'){
				value=primaryEVal;
				html='';
				if(value!==null){
					if((value || value==0) && field.options[value]){html=field.options[value].alias;}
					if(smallE){smallE.filter('*[value="'+value+'"]').prop('checked',true);}
					if(largeE){largeE.filter('*[value="'+value+'"]').prop('checked',true);}
					// input.filter('*[value="'+value+'"]').prop('checked',true);
				}
			}else if(field.input_type=='date'){
				if(field.sub_data_type=='season'){
					value=primaryEVal;
					if(value && !isNaN(value)){
						var seasonStuff=main.utilities.getSeasonFromTimeStamp(Number(value));
						html=main.utilities.formatSeason(seasonStuff);
						if(smallE){
							smallE.closest('.seasonDateContainer').find('.seasonSelect').val(seasonStuff.season);
							smallE.closest('.seasonDateContainer').find('.yearSelect').val(seasonStuff.year);
						}
						if(largeE){
							largeE.closest('.seasonDateContainer').find('.seasonSelect').val(seasonStuff.season);
							largeE.closest('.seasonDateContainer').find('.yearSelect').val(seasonStuff.year);
						}
					}else{
						html='';
						if(largeE){largeE.closest('.seasonDateContainer').find('.seasonSelect').val('');}
						if(largeE){largeE.closest('.seasonDateContainer').find('.yearSelect').val('');}
					}
				}else if(field.sub_data_type=='time'){
					value=primaryEVal;
					var timePkg;
					if(timePkg=main.utilities.msToTime(value)){
						html=timePkg.html
					}else{html='';}
					if(smallE){main.utilities.setTimePkg(timePkg,smallE);}
					if(largeE){main.utilities.setTimePkg(timePkg,largeE);}
				}else{
					value=main.utilities.getTimeStamp(primaryEVal);
					html=primaryEVal;
				}
				if(smallE){smallE.val(primaryEVal);}
				if(largeE){largeE.val(primaryEVal);}
			}else if(field.input_type=='file'){
				/* var curval=featureItems.properties[field.name] || [];
				var fileObjs=main.utilities.getFileValue(primaryE);
				for(var b=0;b<fileObjs.fileObjs.length;b++){
					curval.push(fileObjs.fileObjs[b]);
				}
				value=JSON.stringify(fileObjs.fileObjs);
				html=fileObjs.filesStr; */
				if(!primaryEVal){return;}
				value={};
				html='Uploading';
				var fileChanged=field.name;
			}else{
				html=primaryEVal;
				value=primaryEVal;
				if(field.lookup_table){
					var a;
					if(a=main.utilities.getLookUpTableHTML(field.lookup_table,value,fieldName,self.layerName,field)){
						html=a;}
				}else{
					if(field.options){
						if(field.options[primaryEVal]){
							html=field.options[primaryEVal].alias;
						}
					}
					if(smallE){smallE.val(primaryEVal);}
					if(largeE){largeE.val(primaryEVal);}
				}
				
			}
			if(msg=main.utilities.validateData(value,field)){
				alert(msg);
				return;
			}
			fieldsItems.html=html || '';
			fieldsItems.current_val=value;
			if(featureItems.editTable){featureItems.editTable.table.find('.popUpRow.'+fieldName+' .dataTableCurrent').html(fieldsItems.html);}
			if(featureItems.editLargeTable){featureItems.editLargeTable.formWrap.find('.popUpRow.'+fieldName+' .dataTableCurrent').html(fieldsItems.html);}
			// if(!main.misc.blockUpdateData && !main.misc.blockUpdateData2 && updateDB && (!self.popupsUneditable || !fromPopUp)){self.updateData([pid],null,primaryE,fileChanged,null,null,null,null,[fieldName]);}
			if(!main.misc.blockUpdateData && !main.misc.blockUpdateData2 && updateDB && (!self.popupsUneditable || !fromPopUp)){
				var changeFields=[{field:self.fields[fieldName],val:value}];
				changeFields=changeFields.concat(main.utilities.addLookUpValsToChangedFields(value,field,self.fields,self.properties.lookup_table_map));
			
				var fieldMap=self.layer.properties.internal_field_map;
				var ecoVals=null;
				if(fieldMap && fieldMap.dbh && fieldMap.main_species_field){
						var landUseVal='';
						if(featureItems.fields['landuse'] && self.layer.properties.eco_fields_map && self.layer.properties.eco_fields_map[featureItems.fields['landuse'].current_val]){
							landUseVal=self.layer.properties.eco_fields_map[featureItems.fields['landuse'].current_val];
						}
					ecoVals=main.utilities.getEcoFields(fieldMap,featureItems.fields[fieldMap.dbh].current_val,featureItems.fields[fieldMap.main_species_field].current_val,landUseVal);
				}
				main.dataEditor.updateData({
					pids:[pid],
					context:self,
					reference:primaryE,
					fileChanged:fileChanged,
					ecoVals:ecoVals,
					changeFields:changeFields,
					callback:updatedCB
				});
			}
			//update other app areas
			// if(!updateNoOtherTables && main.tables[self.layerName].map_layer.features[pid] && main.tables[self.layerName].map_layer.features[pid].dataTable){
				// main.tables[self.layerName].map_layer.features[pid].dataTable.updateDataTable(pid,fieldName,primaryE,false,true,true);
			// }
			if(!updateNoOtherTables && self.dataTable){
				self.dataTable.updateDataTable(pid,fieldName,primaryE,false,true,true);
			}
			if(self.inventory.properties.special_connections){
				if(self.inventory.properties.special_connections[field.name]){
					var specialConnection=self.inventory.properties.special_connections[field.name];
					var type=specialConnection.type;
					if(type=='place_in_range'){
						main.utilities.doSpecialConnection(value,specialConnection,primaryE,null,self.layerName);
					}
				}
			}
			
			// if(featureItems.editTable.table && featureItems.editTable.table){featureItems.editTable.table.find('.popUpRow.'+fieldName+' .dataTableCurrent').html(fieldsItems.html);}
			// if(featureItems.editLargeTable && featureItems.editLargeTable.table){featureItems.editLargeTable.table.find('.popUpRow.'+fieldName+' .dataTableCurrent').html(fieldsItems.html);}
			
			
			
			
			/* if(!main.misc.blockUpdateData && updateDB && (!self.popupsUneditable || fromLargeEditForm)){self.updateData([pid]);}
			if(main.tables[self.layerName].map_layer.features[pid] && main.tables[self.layerName].map_layer.features[pid].dataTable){
				main.tables[self.layerName].map_layer.features[pid].dataTable.updateDataTable(pid,field,true);
			}
			
			
			
			
			
			var html=value;
			var featureItems=self.inventory.map_layer.features[pid];
			var fieldsItems=featureItems.fields[fieldName];
			var input_type=fieldsItems.field.input_type,valSplit;
			var hasLargeTable=false;
			if(featureItems.editLargeTable && featureItems.editLargeTable.table){hasLargeTable=true;}
			if(updateEI){
				if(input_type=='checkbox'){
					valSplit=value.split(';;');
					if(hasLargeTable){self.setCheckboxes(valSplit,featureItems.editLargeTable.table.find('.popUpRow.'+fieldName+' .formInput'));}
					self.setCheckboxes(valSplit,featureItems.editTable.table.find('.popUpRow.'+fieldName+' .formInput'));
				}else if(input_type=='radio'){
					if(value){
						featureItems.editTable.table.find('.popUpRow.'+fieldName+' .formInput').prop('checked',true);
						if(hasLargeTable){featureItems.editLargeTable.table.find('.popUpRow.'+fieldName+' .formInput').prop('checked',true);}
					}
				}else{
					if(fieldsItems.field.lookup_table){
						fieldsItems.input.lookUpTable.itemSelected(value,true);
					}else{
						featureItems.editTable.table.find('.popUpRow.'+fieldName+' .formInput').val(value);
						if(hasLargeTable){featureItems.editLargeTable.table.find('.popUpRow.'+fieldName+' .formInput').val(value);}
					}
				}
			}
			if(fieldsItems.input && fieldsItems.input.field.options){
				if(input_type=='checkbox'){
					var values=value.split(';;');
					html='';
					for(var i=0;i<values.length;i++){
						if(fieldsItems.input.field.options[values[i]]){
							html+=fieldsItems.input.field.options[values[i]].alias+', ';
						}else if(fieldsItems.field.default_off_alias){
							html+=fieldsItems.field.default_off_alias+', ';
						}
					}
					html=html.replace(/, ([^, ]*)$/,'$1');
				}else{
					if(fieldsItems.input.field.options[value]){
						html=fieldsItems.input.field.options[value].alias;
					}else if(fieldsItems.field.default_off_alias){
						html=fieldsItems.field.default_off_alias;
					}
				}
			}
			if(input_type=='date'){
				value=main.utilities.getTimeStamp(value);
			}
			if(fieldsItems.field.lookup_table){
				var a=fieldsItems.input.lookUpTable.getLookUpTableHTML(self.updateLookUpTableFields,{pid:pid,fieldName:key,value:value});
				if(a!==false){html=a;}
			}
			fieldsItems.html=html || '';
			fieldsItems.current_val=value;
			if(input_type=='map_point' && fieldsItems.html){
				fieldsItems.html=main.utilities.convertMapPointInput(value);
			}
			featureItems.editTable.table.find('.popUpRow.'+fieldName+' .dataTableCurrent').html(fieldsItems.html);
			if(featureItems.editLargeTable && featureItems.editLargeTable.table){featureItems.editLargeTable.table.find('.popUpRow.'+fieldName+' .dataTableCurrent').html(fieldsItems.html);} */
		};
		this.deleteData=function(layerName){
			if(main.invents.inventories[layerName].source){
				main.invents.inventories[layerName].layer.layer.olLayer.getSource().clear();
				main.map.map.removeLayer(main.invents.inventories[layerName].layer.layer.olLayer);
			}
			delete main.map_layers[layerName].layerObj;
			delete main.map_layers[layerName].features;
			main.map_layers[self.layerName].firstLoad=false;
			var layers={};
			layers[layerName]=main.invents.inventories[layerName].layer;
			if(self.waitLoad){
				self.waitLoad.stopWaiting();
				delete self.waitLoad;
			}
		};
		this.clearFeatures=function(depUponBy){
			if(main.map_layers[self.inventories.currentFiltered].layer){
				// main.map_layers[self.inventories.currentFiltered].layer.olLayer.getSource().clear();
				self.map.map.removeLayer(main.map_layers[self.inventories.currentFiltered].layer.olLayer);
			}
			delete main.map_layers[self.inventories.currentFiltered].features;
			delete main.map_layers[self.inventories.currentFiltered].layer;
			delete main.map_layers[self.inventories.currentFiltered].layerObj;
			if(main.invents.inventories[depUponBy].dataTable){main.invents.inventories[depUponBy].dataTable.clear();}
		};
		this.loadAfterWait=function(response,args){
			self.hasPopUp=null;
			self.hasPopUpOtherFeatures=null;
			self.hasPopUpCurrentIndex=null;
			main.loader.retrieveLayersStart(args.depUponBy,args.where,true);
		};
		this.checkForLoading=function(){
			if(main.loader.loading){return false;
			}else{return true;}
		};
		this.loadFeatures=function(args){
			if(args.showAll || (!main.account.accountSettings[main.account.loggedInUser.user_type+'_restrict_to_own_data'] || (main.account.loggedInUser.org_data.orgId && args.pid && args.pid==main.account.loggedInUser.org_data.orgId))){
				// var pid=args.pid;
				if(!args.showAll && self.inventory.map_layer.features[args.pid]){
					if(!self.inventory.map_layer.features[args.pid].fields){self.mapInteractions.createFeatureHTML(self.inventory.map_layer.features[args.pid].properties);}
					if(self.inventory.map_layer.features[args.pid].popup){self.inventory.map_layer.features[args.pid].popup.remove();}
				}
				var depUponBy=main.inventories[self.layerName].is_depended_upon_by;
				self.inventories.currentFilterLayer=self.layerName;
				self.inventories.currentFiltered=depUponBy;
				self.inventories.currentFilterPID=args.showAll?null:args.pid;
				//delete old data
				self.deleteData(depUponBy);
				var layers={};
				layers[depUponBy]=main.invents.inventories[depUponBy].layer;
				self.waitLoad=new WaitLoad({
					layers:layers,
					context:main.invents.inventories[depUponBy]
				});
				var internal_field_map=main.inventories[depUponBy].properties.internal_field_map;
				var fieldName=internal_field_map.dependent_upon_field;
				if(!args.showAll && !main.account.loggedInUser.dont_filter){var where=fieldName+"='"+args.pid+"'";}
				/* if(main.advFilter && main.advFilter.where && main.advFilter.currentLayer.properties.layer_name==self.layerName){
					if(where){where+=' AND ';}
					where+='('+main.advFilter.where+')';
				} */
				main.inventories[depUponBy].overRideDependentOn=true;
				// main.inventories[depUponBy].currentLoad=args.pid;
				if(main.filter_legend && main.filter_legend.active){
					main.invents.inventories[depUponBy].updateFilterLegend=true;
				}
				var oneGroupAtTime=true;
				if(oneGroupAtTime){self.clearFeatures(depUponBy);}
				if(!main.loader.loading){
					main.invents.inventories[depUponBy].delayVis=true;
					self.hasPopUp=null;
					self.hasPopUpOtherFeatures=null;
					self.hasPopUpCurrentIndex=null;
					main.loader.retrieveLayersStart([depUponBy],[where],args.refresh);
				}else{
					if(!self.loadWait){
						alert('There are other layers loading, this request will load after the other layers are loaded.');
						main.layout.loader.loaderOn();
						self.loadWait=new Wait(self.checkForLoading,self.loadAfterWait,null,{'depUponBy':[depUponBy],'where':[where]});
					}
				}
				if(main.dataTables[depUponBy] && main.dataTables[depUponBy].dataContentWrap && main.dataTables[depUponBy].dataContentWrap.is(':visible')){main.dataTables[depUponBy].sideReset();}
				if(main.dashboard && main.dashboard.active){main.dashboard.refresh();}
				if(args.context){args.context.remove();}
				if(!args.showAll){
					var feature0=self.layer.layer.olLayer.getSource().forEachFeature(function(feature){
						if(feature.get('pid')==args.pid){
							var zoom=14;
							var coords=main.utilities.getCoords(feature);
							self.inventories.currentFilterProperties=feature.getProperties();
							if(feature.get('default_zoom_home')){
								if(main.personal_settings.map.center){coords=ol.proj.transform(main.personal_settings.map.center, 'EPSG:4326', 'EPSG:3857');}
								if(main.personal_settings.map.zoom){zoom=main.personal_settings.map.zoom;}
							}
							self.map.panTo(coords);
							if(args.zoomTo){self.map.animateZoom(zoom);}
							main.invents.currentCoords=coords;
							main.invents.currentZoom=zoom;
							if(!feature.get('default_zoom_home')){return feature;
							}else{return true;}
						}
					});
				}
				if(!args.showAll && !feature0){
					if(self.properties.internal_field_map && self.properties.internal_field_map.geometry_field){
						main.utilities.getGeo(self.layerName,self.properties.internal_field_map.geometry_field,args.pid,self.geoFound,true);
					}
				}
			}else{
				alert('Logged in users are restricted to their own data.');
			}
		};
		this.geoFound=function(response){
			if(!response.default_zoom_home){
				var coords=response.mainCoords;
				var zoom=14;
			}else{
				var coords=ol.proj.transform(main.personal_settings.map.center, 'EPSG:4326', 'EPSG:3857');
				var zoom=main.personal_settings.map.zoom;
			}
			self.map.panTo(coords,self.updateFLShow);
			self.map.animateZoom(zoom,self.updateFLShow);
			main.invents.currentCoords=coords;
			main.invents.currentZoom=zoom;
			if(response.properties){self.inventories.currentFilterProperties=response.properties;}
		};
		this.movingEvent=function(e){
			if(self.movingFeature){self.moved(e,self.movingFeature,self.movingLayer);}
		};
		this.addingEvent=function(e){
			if(!self.countOpenEditForms() || self.inventories.addingFromForm){
				self.newItem(e.coordinate);
				if(self.adding && self.properties.stop_adding_after_add){self.stopAdding();}
				main.map.mapContainer.removeClass('adding');
				if(!main.browser.isMobile){self.toolTip.deActivate();}
			}else{
				if(self.countOpenEditForms()){
					alert('You must close your edit form before adding.');
				}
			}
		};
		this.updateFLShow=function(){
			main.loader.loadMoreMapPoints(self.layerName,null,true);
		};
		if(!self.inventory.properties.dependent_upon){
			var layers={};
			layers[self.layerName]=self.layer;
			this.waitLoad=new WaitLoad({
				layers:layers,
				context:self
			});
		}
	};
});
