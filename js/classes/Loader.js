define(function(require){
	return function pg(args){
		var self=this;
		var MapPointLoader=require('./MapPointLoader');
		this.maxFeatures=1000;
		this.maxMapLayerFeatures=1000;
        if(main.personal_settings.app_settings.max_features) {
            this.maxFeatures = main.personal_settings.app_settings.max_features; 
		}
		if(main.browser.isMobile){this.maxFeatures=700;}
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		self.mapPoints={};
		self.loaders={};
		this.loadMoreMapPoints=function(layerName,mustTurnOn,forCounts,cb,cbParams){
			// if(main.map_layers[layerName].features || forCounts){
				if(!self.loaders[layerName]){
					self.loaders[layerName]=new MapPointLoader({
						layerName:layerName,
						maxFeatures:self.maxFeatures,
						maxMapLayerFeatures:self.maxMapLayerFeatures,
					});
				}
				if(self.loaders[layerName].mapPoints.loading){self.loaders[layerName].mapPoints.stopLoading();}
				var extent=main.map.map.getView().calculateExtent(main.map.map.getSize());
				var where='';
				if(main.invents && main.invents.currentFilterLayer && main.inventories[main.invents.currentFilterLayer].is_depended_upon_by==layerName && main.invents.currentFilterPID && main.invents.inventories[layerName].properties.internal_field_map && main.invents.inventories[layerName].properties.internal_field_map.dependent_upon_field){
					if(!main.account.loggedInUser.dont_filter){
						where=main.invents.inventories[layerName].properties.internal_field_map.dependent_upon_field+"='"+main.invents.currentFilterPID+"'";
						main.invents.unOrgFilteredAndLoaded=false;
					}else{main.invents.unOrgFilteredAndLoaded=true;}
				}
				var pids=null;
				if(main.advFilter && main.advFilter.appliesTo['map'].apply && main.advFilter.currentLayer && main.advFilter.currentLayer.properties.layer_name==layerName){
					if(main.advFilter.where){
						if(where){where+=' AND ';}
						where+='('+main.advFilter.where+')';
					}
					if(main.advFilter.geoFilter){
						pids=main.advFilter.geoFilter;
					}
				}
				if(main.filter_legend && main.filter_legend.active && !main.filter_legend.hideOnly && main.filter_legend.filterWhere && main.filter_legend.layerName==layerName){
					if(where){where+=' AND ';}
					where+='('+main.filter_legend.filterWhere+')';
				}
				var geom_field=null;
				if(main.map_layers[layerName].properties.internal_field_map && main.map_layers[layerName].properties.internal_field_map.geometry_field){geom_field=main.map_layers[layerName].properties.internal_field_map.geometry_field;}
				if(forCounts){self.getShowingData(layerName,where,extent,geom_field,mustTurnOn,pids);
				}else{self.loaders[layerName].mapPoints.retrieveMapPointsStart(layerName,where,extent,geom_field,mustTurnOn,pids,cb,cbParams);}
			// }
		};
		/* this.mapPoints.stopLoading=function(){
			if(self.mapPoints.retrieveMapPointsStartAjax){self.mapPoints.retrieveMapPointsStartAjax.abort();}
			if(self.mapPoints.retrieveMapLayersAjax){self.mapPoints.retrieveMapLayersAjax.abort();}
			self.mapPoints.loading=false;
		}; */
	/* 	this.mapPoints.resetMapPointParams=function(){
			self.mapPoints.its=0;
			self.mapPoints.thisIt=0;
			self.mapPoints.resultsPerIt=self.maxFeatures;
			// if(main.browser.isMobile){self.mapPoints.resultsPerIt=300;}
			self.mapPoints.pids=[];
			self.mapPoints.currentSlice=[];
			self.mapPoints.loading=false;
			self.mapPoints.doneLoading=false;
			self.mapPoints.active=false;
			self.mapPoints.retrieveMapPointsStartAjax=null;
			self.mapPoints.retrieveMapLayersAjax=null;
		}; */
		this.resetRBDLayers=function(){
			self.its=0;
			self.thisIt=0;
			self.resultsPerIt=self.maxFeatures;
			// if(main.browser.isMobile){self.resultsPerIt=300;}
			self.layersQueue=[];
			self.layers={};
			self.pids=[];
			self.currentSlice=[];
			self.loading=false;
			self.doneLoading=false;
		};
		this.retrieveLayers=function(refresh){
			if(self.currentSlice.length>0){
				$.ajax({
					type:'POST',
					url:main.mainPath+'server/db.php',
					data:{
						params:{
							funcToCall:'retrieveLayers',
							args:{
								folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
								table:self.thisLayer.name,
								// pidst:JSON.stringify(self.currentSlice),
								pids:self.currentSlice.join(',')
							}
						},
						action:'callFunction'
					},
					success:function(response){
						if(response){
							response=JSON.parse(response);
							if(response.status=="OK"){
								var layerName=self.thisLayer.name;
								var map_layer=main.map_layers[layerName];
								if(map_layer){
									var features=response.results[layerName].features;
									var layerObj=JSON.parse(response.results[layerName].geojson);
									if(map_layer.features){
										for(var key in features){
											if(!main.map_layers[layerName].features[key]){
												main.map_layers[layerName].features[key]={
													properties:features[key]
												}
											}
										}
										var gjFeatures=layerObj.features;
										var remainingFeaturesToAdd=self.maxFeatures-main.map_layers[layerName].layerObj.features.length;
										if(remainingFeaturesToAdd>0){
											var featuresToAdd=gjFeatures.slice(0,remainingFeaturesToAdd);
											Array.prototype.push.apply(main.map_layers[layerName].layerObj.features,featuresToAdd);
											if(map_layer.layer && !self.mapPoints.active){main.map.updateLayerSource(layerName);}
											// if(remainingFeaturesToAdd<=gjFeatures.length){self.doneLoading=true;}
										}
									}else{
										main.map_layers[layerName].features={};
										for(var key in features){
											main.map_layers[layerName].features[key]={
												properties:features[key]
											}
										}
										main.map_layers[layerName].layerObj=layerObj;
									}
									self.thisIt++;
									self.nextLayerRetIt(refresh);
								}
							}else{
								alert('Error');
								console.log(response);
							}
						}
						main.layout.loader.loaderOff();
					}
				});
			}
		};
		this.nextLayerRetIt=function(refresh){
			if(self.thisIt<self.its){
				var min=self.thisIt*self.resultsPerIt;
				var max=min+self.resultsPerIt;
				self.currentSlice=self.thisLayer.pids.slice(min,max);
				self.retrieveLayers(refresh);
			}else{
				if(main.filter_legend && main.filter_legend.active && main.filter_legend.layerName==self.layersQueue[self.currentLayer]){
					main.loader.loadMoreMapPoints(main.filter_legend.layerName,null,true);
					main.filter_legend.refresh();
				}
				self.currentLayer++;
				// self.layersQueue.splice(self.layersQueue.indexOf(self.thisLayer.name),1);
				self.nextLayerRet(refresh);
			}
		};
		self.nextLayerRet=function(refresh){
			if(self.currentLayer<self.layersQueue.length){
				var currentLayer0=self.layersQueue[self.currentLayer];
				if(main.inventories && main.inventories[currentLayer0] && main.inventories[currentLayer0].properties.dependent_upon && !main.inventories[currentLayer0].overRideDependentOn){
					var dependent_upon=main.inventories[currentLayer0].properties.dependent_upon;
					main.inventories[dependent_upon].is_depended_upon_by=currentLayer0;
					if(main.invents.depened_upons.indexOf(dependent_upon)==-1){main.invents.depened_upons.push(dependent_upon);}
					if(main.filter_legend && main.filter_legend.active){main.filter_legend.checkLoginChange();}
					self.currentLayer++;
					self.nextLayerRet(refresh);
					return false;
				}
				if((!main.canopy || !main.canopy.layers[currentLayer0])/*  && (!main.map_layers[currentLayer0].properties.max_res || !Number(main.map_layers[currentLayer0].properties.max_res)<main.map.map.getView().getResolution()) */){
					main.map_layers[currentLayer0].features={};
					main.map_layers[currentLayer0].layerObj={
						features:[],
						type:'FeatureCollection'
					};
					self.currentLayer++;
					self.nextLayerRet(refresh);
					return false;
					// if(main.filter_legend && !self.mapPoints.active && layerName==main.filter_legend.layerName){
						// var length=null;
						// if(main.invents && main.map_layers[layerName].layer){
							// main.invents.inventories[layerName].updateFLShow();
						// }
					// }
				}else{
					self.thisLayer=self.layers[currentLayer0];
					if(self.thisLayer.pids.length>0){
						self.its=Math.ceil(self.thisLayer.pids.length/self.resultsPerIt);
						self.thisIt=0;
						self.nextLayerRetIt(refresh);
					}else{
						main.map_layers[self.thisLayer.name].features={};
						main.map_layers[self.thisLayer.name].layerObj={
							type:'FeatureCollection',
							features:[]
						}
						self.currentLayer++;
						self.nextLayerRet(refresh);
						return false;
					}
				}
			}else{
				main.layout.loader.loaderOff();
				self.loading=false;
				if(refresh){
					main.loader.loadMoreMapPoints(self.layersQueue[self.currentLayer-1]);
				}
			}
		};
		self.retrieveLayersStart=function(tables,where,refresh){
			main.layout.loader.loaderOn();
			self.resetRBDLayers();
			self.loading=true;
			// advWhere=advWhere || null;
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						funcToCall:'retrieveLayersStart',
						args:{
							folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
							tables:tables,
							// advWhere:advWhere,
							// maxFeatures:main.maxFeatures,
							where:where
						}
					},
					action:'callFunction'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							// var count=response.results.data[Object.keys(response.results.data)[0]].pids.length;
							var datas=response.results.data;
							for(var keyd in datas){
								var count=datas[keyd].total.count;
								if(datas[keyd].maxs){
									var maxs=datas[keyd].maxs;
									for(var key in maxs){maxs[key]=Number(maxs[key]);}
									var mins=datas[keyd].mins;
									for(var key in mins){mins[key]=Number(mins[key]);}
									if(main.invents && main.invents.inventories[keyd]){
										main.invents.inventories[keyd].maxs=maxs;
										main.invents.inventories[keyd].mins=mins;
										main.invents.inventories[keyd].totalCount=count;
									}
									if(main.tables[keyd]){
										main.tables[keyd].maxs=maxs;
										main.tables[keyd].mins=mins;
										main.tables[keyd].totalCount=count;
									}
									// if(main.canopy && main.canopy.layers[self.layersQueue[self.currentLayer]]){
										// main.canopy.layers[self.layersQueue[self.currentLayer]].featureCount=self.layers[self.layersQueue[self.currentLayer]].total.count
									// }
								}
								if(main.advFilter && main.advFilter.currentLayer && main.advFilter.currentLayer.properties.name==keyd){main.advFilter.refresh();}
								if(!main.map_layers[keyd].layerObj){
									main.map_layers[keyd].layerObj={
										type:'FeatureCollection',
										features:[]
									}
								}
								if(main.filter_legend && main.filter_legend.active && main.filter_legend.layerName==keyd){
									main.loader.loadMoreMapPoints(keyd,null,true);
								}
							}
							self.layersQueue=tables;
							self.currentLayer=0;
							// if(count && Number(count)>0){
								// self.layersQueue=Object.keys(response.results.data);
								self.layers=response.results.data;
								self.nextLayerRet(refresh);
							/* }else{
								main.layout.loader.loaderOff();
								main.map_layers[response.results.data[Object.keys(response.results.data)[0]].name].features={};
								// main.map_layers[layerName].layerObj=layerObj;
								self.loading=false;
							} */
							
							// main.data.layers=response.results;
							
							
							// var table;
							// for(var key in response.results){
								// table=response.results[key];
								// main.data.lookUpTables[key].items=table.features;
								// main.data.lookUpTables[key].properties=response.results[key].properties;
								// main.data.lookUpTables[key].waiting=[];
							// }
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.getShowingData=function(layerName,where,extent,geom_field,mustTurnOn,pids){
			// main.layout.loader.loaderOn();
			if(!geom_field){return;}
			pids=pids || null;
			if(pids){
				pids=pids.join(',');
			}
			// var currentPIDs=self.getCurrentPids();
			self.getShowingDataAjax=$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						layerName:layerName,
						extent:extent,
						mustTurnOn:mustTurnOn,
						geom_field:geom_field,
						pids:pids,
						// currentPIDs:currentPIDs
						// maxFeatures:main.maxFeatures,
						where:where
					},
					action:'getShowingData'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							if(main.tables[response.args.layerName].map_layer.layer){
								main.filter_legend.updateShowingDisplay(response.total);
							}
						}else{
							alert('Error: '+response.message);
						}
					}
					main.layout.loader.loaderOff();
				}
			});
		};
		this.retrieveLookupTables=function(){
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						args:{
							folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2]
						},
						funcToCall:'retrieveLookUpTables'
					},
					action:'callFunction'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							if(response.results){
								for(var key in response.results){
									main.data.lookUpTables[key].items=response.results[key].items;
									var waiting=main.data.lookUpTables[key].waiting;
									for(var i=0;i<waiting.length;i++){
										waiting[i][0](waiting[i][1]);
									}
									// main.data.lookUpTables[key].properties=response.results[key].properties;
									// main.data.lookUpTables[key].waiting=[];
								}
							}
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		for(var key in main.data.lookUpTables){main.data.lookUpTables[key].waiting=[];}
		self.retrieveLookupTables();
		if(self.tables){self.retrieveLayersStart(self.tables);}
	};
});
