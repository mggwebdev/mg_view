define(function(require){
	return function pg(args){
		var self=this;
		this.openHTML='Read Instructions';
		this.viewPrevHTML='Read Introduction';
		this.classes='';
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.content=this.content || '';
		(this.createHTML=function(){
			self.viewMoreInstructs=$('<span class="viewMoreInstructs viewMore">'+self.openHTML+'</span>');
			self.instructionsBotButtons=$('<div class="instructionsBotButtons viewMoreBotButtons">')
				.append(self.viewMoreInstructs);
			if(self.hidePrevOnShow){
				self.viewIntro=$('<span class="viewMoreIntroShow viewIntro none">'+self.viewPrevHTML+'</span>');
				self.instructionsBotButtons.append(self.viewIntro);
				self.viewIntro.on('click',function(){
					self.viewMoreInstructs.removeClass('none');
					self.viewIntro.addClass('none');
					self.descContentPrev.slideDown(200);
					self.descContent.slideUp(200);
				});
			}
			self.descContent=$('<div class="descContent viewMoreContent">').html(self.content);
			self.html=$('<div class="viewMoreWrap">')
				.append(self.descContent)
				.append(self.instructionsBotButtons)
			if(self.classes){
				self.html.addClass(self.classes);
			}
			if(self.prev){
				self.descContentPrev=$('<div class="descContentPrev">').html(self.prev);
				self.html.prepend(self.descContentPrev);
			}
		})();
		(initEvents=function(){
			self.viewMoreInstructs.on('click',function(){
				if(self.hidePrevOnShow){
					if(self.descContent.is('visible')){
						self.descContent.slideUp(200);
					}else{
						self.descContent.slideDown(200);
						if(self.hidePrevOnShow){
							self.viewMoreInstructs.addClass('none');
							self.viewIntro.removeClass('none');
							self.descContentPrev.slideUp(200);
						}
					}
				}else{
					self.descContent.slideToggle(200);
				}
			});
		})();
	};
});