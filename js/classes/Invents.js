define(function(require){
	return function pg(args){
		var self=this;
		var Accordian=require('./Accordian');
		var Button=require('./Button');
		var Inventory=require('./Inventory');
		this.alias='Plot';
		this.name='inventory';
		this.default_inventory='trees';
		this.womSelectInv=null;
		this.womSelectEditForm=null;
		this.currentHighlightFilterLayer=null;
		this.currentHighlightFilter=null;
		this.drawing=false;
		this.adding=false;
		this.addingFromForm=null;
		this.moving=false;
		this.hasHierarchy=false;
		this.currentFilterLayer=null;
		this.currentFiltered=null;
		this.currentFilterPID=null;
		this.currentFilterProperties=null;
		this.currentCoords=null;
		this.currentZoom=null;
		this.currentInvent=null;
		this.selectingForTransferTo=false;
		this.unOrgFilteredAndLoaded=false;
		this.blockDoubleClick=false;
		this.inventories={};
		this.bulkCounts=[];
		this.depened_upons=[];
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		(this.preHTMLAdd=function(){
			var buttonAlias=main.withs.with_inventories.alias || self.alias;
			if(main.withs.with_inventories.icon_path){var buttonImg='images/'+main.withs.with_inventories.icon_path}
			else{var buttonImg=main.mainPath+'images/pencil.png';}
			main.utilities.addLayoutItems(self.name,buttonAlias,self,buttonImg);
		})();
		this.clearCurrents=function(){
			self.currentFilterLayer=null;
			self.currentFiltered=null;
			self.currentFilterPID=null;
			self.currentFilterProperties=null;
			self.currentCoords=null;
			self.currentZoom=null;
		};
		/* this.unSetActives=function(){
			for(var key in self.inventories){
				if(self.inventories[key].inventory.map_layer.layer){
					var layerSource=self.inventories[key].inventory.map_layer.layer.olLayer.getSource();
					var sourceFeatures=layerSource.getFeatures();
					for(var t=0;t<sourceFeatures.length;t++){
						sourceFeatures[t].set('isActive',false);
					}
				}
			}
			main.mapItems.hasActive=null;
			$('.activeRow').removeClass('activeRow');
		}; */
		/* this.layerUnSetActives=function(layerName){
			if(main.tables[layerName].map_layer && main.tables[layerName].map_layer.layer){
				var layerSource=main.tables[layerName].map_layer.layer.olLayer.getSource();
				var sourceFeatures=layerSource.getFeatures();
				for(var t=0;t<sourceFeatures.length;t++){
					sourceFeatures[t].set('isActive',false);
				}
			}
			$('.activeRow').removeClass('activeRow');
		}; */
		this.checkForOtherActions=function(){
			for(var key in self.mainInventories){
				if(self.inventories[key]){
					if(self.inventories[key].adding || self.inventories[key].promptingForTemplates){
						self.inventories[key].stopAdding();
						if(self.inventories[key].promptingForTemplates){self.inventories[key].templateStuff.templatePanel.close();}
					}
					if(self.inventories[key].moving){self.inventories[key].stopMoving();}
					if(self.inventories[key].selectingForPhoto){self.inventories[key].stopSelectingForPhoto();}
				}
			}
		};
		this.resetHighlightFilter=function(){
			self.currentHighlightFilter=null;
			self.currentHighlightFilterLayer=null;
		};
		if(main.withs.with_wom && main.withs.with_wom.value/*  && main.withs.with_wom[main.account.loggedInUser.user_type+'_with'] */){
			main.wom=new main.classes.WOM();
			self.womAccordian=new Accordian('wom','abc');
			self.accordian.createShelf({
				name:'wom',
				button:new Button('toolBox','','WOM').html,
				content:self.womAccordian.container,
				alias:'WOM'
			});
		}
		(this.addInventoriyForms=function(){
			self.mainInventories=main.inventories;
			self.layers=main.inventories;
			for(var key in self.mainInventories){
				if(!self.mainInventories[key].properties.is_wom || (main.withs.with_wom && main.withs.with_wom.value/*  && main.withs.with_wom[main.account.loggedInUser.user_type+'_with'] */)){
					if(self.mainInventories[key].properties.primary_inv){self.default_inventory=key;}
					self.inventories[key]=new Inventory({
						layerName:key,
						inventories:self,
						groupOrganizer:self,
						map_layer:main.map_layers[key],
						properties:self.mainInventories[key].properties,
						fields:self.mainInventories[key].fields,
						map:self.map
						// layer:visible
					});
					if(main.tables[key]){
						main.tables[key].inventory=self.inventories[key];
						main.tables[key].coordModule=self.inventories[key];
					}
					if(self.mainInventories[key].properties.dependent_upon){self.hasHierarchy=true;}
					if(self.mainInventories[key].properties.is_orgs){self.hasOrgs=true;}
					if(self.mainInventories[key].properties.is_events){self.hasEvents=true;}
					if(self.mainInventories[key].properties.is_projects){self.hasProjects=true;}
					if(self.mainInventories[key].properties.is_wom){self.hasWom=true;}
				}
			}
		})();
		if(self.hasHierarchy){
			(this.addDescription=function(){
				if(self.hasProjects){
					self.descriptionContent=$('<div class="inventsDescContent">').html('<b>Welcome to the Plot tool</b>.<br/>Tree planting activities are stored at two different scales, the project-level and for individual trees. When logged in, registered users can first add a Project location to the map and then add individual trees to the map. Read the instructions below for each tool.');
					self.description=$('<div class="inventsDesc stdSection">').append(self.descriptionContent);
					self.accordian.container.prepend(self.description);
				}
			})();
		}
	};
});