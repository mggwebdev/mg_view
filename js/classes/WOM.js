define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		this.withItem='with_wom';
		this.alias='WOM';
		this.name='wom';
		this.serviceRequests={
			name:'service_requests',
			alias:'Service Requests'
		};
		this.workOrders={
			name:'work_orders',
			alias:'Work Orders'
		};
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.addShelf=function(){
			self.launchButton=new Button('toolBox','',self.alias);
			self.shelf=main.layout.toolBox.createShelf({
				name:self.name,
				button:self.launchButton.html,
				content:self.html,
				context:self,
				alias:self.alias,
				dontAppendTo:true,
				groups:main.withs[self.withItem].toolbox_groups
			});
			main.layout.toolBox.shelfInAppTest(main.layout.toolBox.shelves[self.name]);
		};
		this.updateWOM=function(){
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2]
					},
					action:'getWOMData'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.printItemGo=function(params){
			var form=$('<form>',{
				action:main.mainPath+'server/printWorkOrder.php',
				method:'POST',
				target:'_blank'
			});
			var layerAlias='Work Order';
			if(main.invents.inventories[params.layerName]){layerAlias=main.invents.inventories[params.layerName].properties.singular_alias;}
			form.append($('<input type="hidden" name="folder"/>').val(window.location.pathname.split("/")[window.location.pathname.split("/").length-2]));
			params.mapPath=params.mapPath || '';
			form.append($('<input type="hidden" name="pid"/>').val(params.pid));
			form.append($('<input type="hidden" name="layer"/>').val(params.layerName));
			form.append($('<input type="hidden" name="layerAlias"/>').val(layerAlias));
			form.append($('<input type="hidden" name="completeByInput"/>').val(params.completeByInput));
			form.append($('<input type="hidden" name="printComments"/>').val(params.printComments));
			form.append($('<input type="hidden" name="mapPath"/>').val(params.mapPath));
			form.append($('<input type="hidden" name="printPhotos"/>').val(params.printPhotos));
			form.append($('<input type="hidden" name="activeFields"/>').val(JSON.stringify(params.activeFields)));
			form.append('<input type="hidden" name="action" value="printWorkOrder"/>');
			$('body').append(form);
			form.get(0).submit();
			form.remove();
		};
		this.printFileSaved=function(response,params){
			if(response.pathWName){params.mapPath=response.pathWName}
			self.printItemGo(params);
		};
		this.printItem=function(params){
			// $.ajax({
				// type:'POST',
				// url:main.mainPath+'server/db.php',
				// data:{
					// params:{
						// folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						// layer:layer,
						// pid:pid
					// },
					// action:'printWorkOrder'
				// },
				// success:function(response){
					// if(response){
						// response=JSON.parse(response);
						// if(response.status=="OK"){
							// main.layout.loader.loaderOff();
							// self.addInvItems(response.results,response.params.table);
						// }
					// }
				// }
			// });
			if(!params.printMap){
				self.printItemGo(params);
			}else{
				if(main.print){
					main.print.print(main.map.map,'server',self.printFileSaved,params);
				}
				// $.ajax({
					// type:'POST',
					// url:main.mainPath+'server/db.php',
					// data:{
						// params:{
							// folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2]
						// },
						// action:'p'
					// },
					// success:function(response){
						// if(response){
							// response=JSON.parse(response);
							// if(response.status=="OK"){
							// }else{
								// alert('Error');
								// console.log(response);
							// }
						// }
					// }
				// });
			}
		};
		this.createTabContent=function(name){
			self[name].contentWrap=$('<div class="womMainContentWrap '+name+'" data-key="'+name+'">');
			self[name].label=$('<div class="womMainLabel">').html(self[name].alias);
			self[name].tableWrap=$('<div class="womMainTableWrap">');
			self[name].table=$('<t class="womMainTableWrap">');
		};
		(this.createHTML=function(){
			// self.createTabContent(self.workOrders.name);
			// self.createTabContent(self.serviceRequests.name);
			
			// self.tabs=new Tabs({classes:'womMainTabs'});
			// self.serviceRequestsTab=self.tabs.addTab('Service Requests',$('<div class="womMainTab">').append(table)),'workOrdersTab');
			// self.workOrdersTab=self.tabs.addTab('Work Orders',$('<div class="womMainTab">').append(table)),'serviceRequestsTab');
			// self.inAccordian=new Accordian('wom','abc');
			// self.serviceRequests.addButton=self.resetButton=new Button('std','serviceRequestAddButton','Add');
			// self.serviceRequests.content=$('<div class="serviceRequestsWrap">')
				// .append(self.serviceRequests.addButton.html);
			// self.workOrders.addButton=self.resetButton=new Button('std','workOrdersAddButton','Add');
			// self.workOrders.content=$('<div class="workOrdersWrap">')
				// .append(self.workOrders.addButton.html);
			// self.inAccordian.createShelf({
				// name:self.serviceRequests.name,
				// button:new Button('toolBox','',self.serviceRequests.alias).html,
				// content:self.serviceRequests.content,
				// alias:self.serviceRequests.alias
			// });
			// self.inAccordian.createShelf({
				// name:self.workOrders.name,
				// button:new Button('toolBox','',self.workOrders.alias).html,
				// content:self.workOrders.content,
				// alias:self.workOrders.alias
			// });
			// self.html=$('<div class="womWrap">').append(self.inAccordian.container);
			// self.updateWOM();
			// self.addShelf();
		})();
		(this.initEvents=function(){
		})();
	};
});