define(function(require){
	return function pg(args){
		var self=this;
		var Panel=require('./Panel');
		this.id=this.id || main.utilities.generateUniqueID();
		this.scrollMethod='clickUnit';
		this.clickUnitIncrement=200;
		this.velocity=0.5;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.alias=this.alias || 'Photos';
		this.tnImageLoaded=function(){
			self.tnsUploaded++;
			if(self.tnsUploaded==self.images.length){
				self.testForNonActives();
				// self.panel.positionPanel();
			}
		};
		this.updateThumbnails=function(){
			self.tnsUploaded=0;
			self.tnBackground.html('');
			var primaryImg;
			for(var i=0;i<self.images.length;i++){
				self.images[i].img=$('<img class="tnBGImg" onload="main.imageView.tnImageLoaded();" src="images/uploaded_images/'+self.images[i].servFileName+'" title="'+self.images[i].fileName+'" data-data_uploaded="'+self.images[i].dateUploaded+'"/>');
				self.images[i].img.on('click',function(){
					self.setMainImg($(this).attr('src'),$(this).attr('title'),$(this).data('data_uploaded'));
					self.testIfImgPanelOut($(this));
				});
				self.tnBackground.append(self.images[i].img);
				if(self.primaryImg && self.primaryImg==self.images[i].servFileName){primaryImg=self.images[i];}
			}
			var img=self.images[0].servFileName;
			var title=self.images[0].fileName;
			var dateUploaded=self.images[0].dateUploaded;
			if(primaryImg){
				img=primaryImg.servFileName;
				title=primaryImg.fileName;
				dateUploaded=primaryImg.dateUploaded;
			}
			self.setMainImg('images/uploaded_images/'+img,title,dateUploaded);
		};
		this.testIfImgPanelOut=function(img){
			var tnBGoffset=self.tnBackground.offset().left;
			var imgWholeOffset=img.offset().left;
			var offset=imgWholeOffset-tnBGoffset;
			var imgWidth=img.width();
			var imgRight=offset+imgWidth;
			var left=parseInt(self.tnBackground.css('left'));
			var tnContainerWidth=self.tnContainer.width();
			var leftTest=-left+tnContainerWidth;
			var increment=tnContainerWidth/2;
			if(imgRight>leftTest){
				var toMove=Math.ceil(left-(imgRight-leftTest));
				self.tnBackground.stop(true).animate({left:toMove},increment/self.velocity,'linear');
				self.scrollLeft.removeClass('noActive');
				var limit=tnContainerWidth-self.tnBackground.width();
				if(toMove==limit){self.scrollRight.addClass('noActive');}
			}
			var imgLeft=offset;
			if(imgLeft<-left){
				var toMove=-imgLeft;
				self.tnBackground.stop(true).animate({left:toMove},increment/self.velocity,'linear');
				self.scrollRight.removeClass('noActive');
				if(toMove==0){self.scrollLeft.addClass('noActive');}
			}
			
		};
		this.centerMainImg=function(src,title){
			var wrapHeight=self.assImagesMainImgWrap.height();
			var imgHeight=self.assImagesMainImg.height();
			if(imgHeight>0){
				var marginTop=(wrapHeight-imgHeight)/2;
				self.assImagesMainImg.css('padding-top',marginTop);
			}
		}
		this.setMainImg=function(src,title,dateUploaded){
			if(title){
				self.assImagesMainImg.attr('title',title);
				self.ivfileNameShow.html(title);
			}
			if((dateUploaded || dateUploaded==0) && !isNaN(dateUploaded)){self.ivDateShow.html(main.utilities.formatDate(Number(dateUploaded),true));}
			if(src!=self.assImagesMainImg.attr('src')){
				self.assImagesMainImg.attr('src',src).css('padding-top','');
			}
			
		};
		this.loadImages=function(images,primaryImg){
			self.images=[];
			for(var i=0;i<images.length;i++){
				self.images.push({
					dateUploaded:images[i].dateUploaded, 
					ext:images[i].ext, 
					fileName:images[i].fileName, 
					servFileName:images[i].servFileName
				});
			}
			self.primaryImg=primaryImg;
			if(self.images.length>0){self.updateThumbnails();}
		};
		this.testForNonActives=function(){
			self.scrollRight.removeClass('noActive');
			self.scrollLeft.removeClass('noActive');
			if(self.tnBackground.width()<self.tnContainer.width()){
				self.scrollRight.addClass('noActive');
				self.scrollLeft.addClass('noActive');
			}else{
				var tnBGLeft=parseInt(self.tnBackground.css('left'));
				if(tnBGLeft==0){self.scrollLeft.addClass('noActive');}
				if(self.tnContainer.width()==self.tnBackground.width()){self.scrollRight.addClass('noActive');}
			}
		};
		this.panelShow=function(){
			self.testForNonActives();
			self.centerMainImg();
			main.layout.shade.addClass('block');
		};
		this.panelClose=function(){
			main.layout.shade.removeClass('block');
		};
		this.panelMin=function(){
			main.layout.shade.removeClass('block');
		};
		this.createHTML=function(){
			self.scrollLeft=$('<div class="scroller noSelect scrollLeft assImageTNsEle">')
				.append($('<img class="scrollArrow" src="'+main.mainPath+'images/arrowLeft.png">'));
			self.scrollRight=$('<div class="scroller noSelect scrollRight assImageTNsEle">')
				.append($('<img class="scrollArrow" src="'+main.mainPath+'images/arrowRight.png">'));
			self.tnBackground=$('<div class="tnBackground cf">');
			self.tnBackgroundWrap=$('<div class="tnBackgroundWrap cf">')
				.append(self.tnBackground);
			self.tnContainer=$('<div class="tnContainer assImageTNsEle">');
			self.assImagesMainImgWrap=$('<div class="assImagesMainImgWrap">');
			self.assImagesMainImg=$('<img class="assImagesMainImg" onload="main.imageView.centerMainImg();">');
			self.assImageTNs=$('<div class="assImageTNs cf">')
			self.ivfileNameShow=$('<div class="ivfileNameShow ivOtherInfoEle">');
			self.ivDateShow=$('<div class="ivDateShow ivOtherInfoEle">');
			self.ivOtherInfo=$('<div class="ivOtherInfo cf">')
				.append(self.ivDateShow)
				.append(self.ivfileNameShow);
			self.html=$('<div class="assImageViewer">')
				.append(self.assImagesMainImgWrap.append(self.assImagesMainImg))
				.append(self.ivOtherInfo)
				.append(self.assImageTNs
					.append(self.scrollLeft)
					.append(self.tnContainer.append(self.tnBackgroundWrap))
					.append(self.scrollRight));
		};
		this.exportButtonClicked=function(){
			main.attachments.export(self.images,'images/uploaded_images/');
		};
		(this.init=function(){
			self.createHTML();
			self.panel=new Panel({
				content:self.html,
				title:self.alias,
				classes:'imgViewerPanel',
				matchHeight:true,
				onClose:self.panelClose,
				hideOnOutClick:true,
				hideOnOutClickExceptions:['.viewPhotos','.filePkgWrap'],
				onMin:self.panelMin,
				onShow:self.panelShow,
				withExport:true,
				exportCB:self.exportButtonClicked
			});
			self.html.appendTo(self.imageViewerContainer);
		})();
		this.scrollToRight=function(){
			if(self.scrollMethod='clickUnit'){
				var tnBGLeft=parseInt(self.tnBackground.css('left'));
				var tnBGWidth=self.tnBackground.width();
				var tnContainerWidth=self.tnContainer.width();
				var increment=tnContainerWidth/2;
				// var increment=self.clickUnitIncrement;
				var newLeft=tnBGLeft-increment;
				var limit=tnContainerWidth-tnBGWidth;
				if(newLeft<=limit){
					newLeft=limit;
					self.scrollRight.addClass('noActive');
				}else{
					self.scrollLeft.removeClass('noActive');
				}
				self.tnBackground.stop(true).animate({left:newLeft},increment/self.velocity,'linear');
			}
		};
		this.scrollToLeft=function(){
			if(self.scrollMethod='clickUnit'){
				var tnBGLeft=parseInt(self.tnBackground.css('left'));
				var increment=self.tnContainer.width()/2;
				// var increment=self.clickUnitIncrement;
				var newLeft=tnBGLeft+increment;
				if(newLeft>=0){
					newLeft=0;
					self.scrollLeft.addClass('noActive');
				}else{
					self.scrollRight.removeClass('noActive');
				}
				self.tnBackground.stop(true).animate({left:newLeft},increment/self.velocity,'linear');
			}
		};
		(this.initEvents=function(){
			/* $('html').on('click',function(e){
				if($(e.target).closest('.imgViewerPanel,.viewPhotos,.filePkgWrap').length==0 && !self.panel.minimized){self.panel.close();}
			}); */
			self.scrollLeft.on('click',function(){
				if(!$(this).hasClass('noActive') && self.tnBackground.width()>self.tnContainer.width()){
					self.scrollToLeft();
				}
			});
			self.scrollRight.on('click',function(){
				if(!$(this).hasClass('noActive') && self.tnBackground.width()>self.tnContainer.width()){
					self.scrollToRight();
				}
			});
		})();
	}
});