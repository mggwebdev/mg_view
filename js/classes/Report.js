define(function(require){
	return function pg(args,table){
		var self=this;
		var Button=require('./Button');
		var Checkbox=require('./Checkbox');
		var Form=require('./Form');
		var Panel=require('./Panel');
		var Radio=require('./Radio');
		var Tabs=require('./Tabs');
		this.id=main.utilities.generateUniqueID();
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.table=this.callerContext.table;
		this.headerImg=main.personal_settings.app_settings.report_logo;
		this.records=this.records || {};
		this.reports=main.utilities.cloneObj(main.data.personal_settings.reports);
		this.createHTML=function(){
			this.typeSelect=$('<select>').addClass('typeSelect');
			this.typeSelect.append($('<option>').val('').text(''));
			// this.typeSelect.append($('<option>').val('custom').text('Custom'));
			for(var key in this.reports){
				if(this.reports[key].table_name==this.table){
					this.typeSelect.append($('<option>').val(this.reports[key].name).text(this.reports[key].alias));
				}
			}
			if(self.with_export){
				self.typeSelect.append($('<option>').val('_export').text('Export'));
			}
			var typeSelectContent=$('<form>').addClass('reportTypeSelectForm')
				.append($('<table>')
					.append($('<tbody>')
						.append($('<tr>')
							.append($('<td>')
								.append($('<div>').addClass('tdLiner').text('Select Report Type'))
							)
							.append($('<td>')
								.append($('<div>').addClass('tdLiner')
									.append(this.typeSelect)
								)
							)
						)
					)
				);
			this.typeSelectPanel=new Panel({
				content:typeSelectContent,
				label:'Report Type',
				title:'Report Type',
				classes:'reportTypeSelectPanel'
			});
			this.panel=new Panel({
				label:'Inspector Report',
				title:'Inspector Report',
				classes:'insepctorReport'
			});
		};
		this.checkAll=function(checkAll){
			checkAll.closest('.reportParamTab').find('.reportParamTbody .reportParamCheck').prop('checked',checkAll.prop('checked'))
		};
		this.createSheet=function(){
			var record0=self.records[Object.keys(self.records)[0]];
			if(record0){
				self.fields=record0.fields;
				var protectedTbody=$('<tbody class="reportParamTbody">'),omitTbody=$('<tbody class="reportParamTbody">'),checked,value,key,field;
				var keys=main.utilities.sortFields(self.fields,'abc');
				for(var i=0;i<keys.length;i++){
					key=keys[i];
					field=self.fields[key];
					if(field.active){
						if(field.field.in_atc_1/*  && !field.field.atc_hidden */){
							disabled=false,checked=false;
							if(field.field.atc_protected){
								disabled=true;
								checked=true;
							}
							protectedTbody.append($('<tr>')
								.append($('<td>')
									.append($('<div>').addClass('tdLiner').append(new Checkbox({
										label:field.field.alias,
										value:key,
										checked:checked,
										name:key,
										classes:'formInput protectedCheck reportParamCheck',
										data:[['name',key]],
										disabled:disabled
									}).html))
								)
							)
							disabled=false;
							if(field.field.atc_unomittable){
								disabled=true;
							}
							omitTbody.append($('<tr>')
								.append($('<td>')
									.append($('<div>').addClass('tdLiner').append(new Checkbox({
										label:field.field.alias,
										value:key,
										checked:false,
										name:key,
										classes:'formInput omitCheck reportParamCheck',
										data:[['name',key]],
										disabled:disabled
									}).html))
								)
							)
						}
					}
				}
				self.protectChAll=new Checkbox({
					label:'Toggle All',
					value:'',
					checked:false,
					name:'',
					classes:'reportProtectChAll'
				});
				self.protectChAll.checkbox.on('change',function(){
					self.checkAll($(this));
				});
				var protectTable=$('<table class="reportParamTable">')
					.append($('<thead>')
						.append($('<tr>')
							.append($('<th>')
								.append($('<div>').addClass('tdLiner').text('Protect these columns:'))
							)
						)
					)
					.append(protectedTbody);
				self.omitChAll=new Checkbox({
					label:'Toggle All',
					value:'',
					checked:false,
					name:'',
					classes:'reportOmitChAll'
				});
				self.omitChAll.checkbox.on('change',function(){
					self.checkAll($(this));
				});
				var omitTable=$('<table class="reportParamTable">')
					.append($('<thead>')
						.append($('<tr>')
							.append($('<th>')
								.append($('<div>').addClass('tdLiner').text('Omit these columns:'))
							)
						)
					)
					.append(omitTbody);
				var tabs=new Tabs();
				var activeTab=tabs.addTab('Protect',$('<div class="reportParamTab">')
					.append($('<div class="reportParamTop">')
						.append(self.protectChAll.html)
					)
					.append(protectTable)
				);
				tabs.addTab('Omit',$('<div class="reportParamTab">')
					.append($('<div class="reportParamTop">')
						.append(self.omitChAll.html)
					)
					.append(omitTable)
				);
				tabs.setActiveTab(activeTab.tabid);
				self.sheetPrepForm=$('<form>').addClass('sheetPrepForm')
					.append(tabs.html)
					.append($('<div>').addClass('center formConcButtons').html('<input type="submit" class="button submit formConcButton" value="Create"/>'))
				self.sheetPrepPanel=new Panel({
					content:self.sheetPrepForm,
					label:'Export Sheet',
					title:'Export Sheet',
					classes:'createSheetPanel'
				});
				// self.sheetPrepForm.on('change','.formInput',function(){
					// main.utilities.setCurrentVals(self.sheetPrepForm,self.fields);
				// });
				self.sheetPrepForm.on('submit',function(e){
					e.preventDefault();
					var protecteds=[];
					self.sheetPrepForm.find('.formInput.protectedCheck:checked').each(function(){
						protecteds.push($(this).closest('.checkboxWrap').data('name'));
					})
					var omits=[];
					self.sheetPrepForm.find('.formInput.omitCheck:checked').each(function(){
						omits.push($(this).closest('.checkboxWrap').data('name'));
					})
					var form=$('<form>',{
						action:main.mainPath+'server/excel.php',
						method:'POST'
					});
					var saveMethod='saveToLocal';
					var title="Tree Tender City Inspector Sheet";
					var dataPkgs=[];
					for(var key in self.records){
						dataPkgs.push(main.utilities.createFilePKG(self.records[key].fields))
					}
					form.append('<input type="hidden" name="title" value="'+title+'"/>');
					if(saveMethod=='saveToLocal'){form.append('<input type="hidden" name="fileName" value="'+title.replace(/ /g,'_').replace(/\W+/g,"")+'.xlsx"/>');}
					if(self.headerImg){
						form.append('<input type="hidden" name="headerImg" value="'+self.headerImg+'"/>');
					}
					form.append('<input type="hidden" name="saveMethod" value="'+saveMethod+'"/>');
					form.append('<input type="hidden" name="action" value="generateInspectorSheet"/>');
					form.append($('<input type="hidden" name="protecteds"/>').val(JSON.stringify(protecteds)));
					form.append($('<input type="hidden" name="omits"/>').val(JSON.stringify(omits)));
					form.append($('<input type="hidden" name="dataPkgs"/>').val(JSON.stringify(dataPkgs)));
					$('body').append(form);
					form.get(0).submit();
					form.remove();
					self.sheetPrepPanel.close();
				});
				// main.utilities.setCurrentVals(self.sheetPrepForm,self.fields);
				self.sheetPrepPanel.open();
			}else{
				alert('Please select one or more rows to export.');
			}
		};
		this.createInspectorForm=function(formName){
			var record=this.records[Object.keys(this.records)[0]];
			if(record){
				var reference=record.fields
			}
			var form=main.utilities.cloneObj(main.data.forms[formName]);
			form.appForm=new Form({
				properties:self.forms[formName],
				reference:reference,
				fields:self.forms[formName]
			});
			main.layout.forms[formName]=form;
			self.panel.setContent(form.appForm.html);
		};
		this.startInspectorForm=function(){
			self.createInspectorForm('tree_tenders_street_tree_inspector_form');
			self.typeSelectPanel.close();
			self.panel.open();
		};
		// this.generateMultiRowReport=function(){
			// var dataTable=new DataTable({
				// title:'',
				// table:'tree_tenders_street_tree_inspector_form',
				// loadData:false
			// });
			// dataTable.createLinkedRows(self.records);
			// self.html.setContent(dataTable.html);
			// self.html.open();
			// self.html.centerPanel();
		// };
		this.createNewForms=function(formName){
			var record,form;
			var rows=[];
			for(var key in this.records){
				record=this.records[key]
				if(this.records[key]){
					rows.push(new Form({
						properties:main.utilities.cloneObj(main.data.forms[formName]),
						reference:this.records[key].fields,
						forValuesOnly:true,
						fields:this.records[key].fields
					}));
				}else{
					alert('Error: No reference.');
					break;
				}
			}
			main.utilities.submitMultipleForms(rows,formName);
		};
		this.showTypeSelect=function(){
			this.typeSelectPanel.open();
		};
		this.sendExportForm=function(action){
			var form=$('<form>',{
				action:main.mainPath+'server/excel.php',
				method:'POST'
			});
			var title="Export";
			var dataPkgs=[];
			for(var key in self.records){
				dataPkgs.push(main.utilities.createFilePKG(self.records[key].fields,false,true,self))
			}
			form.append('<input type="hidden" name="title" value="'+title+'"/>');
			form.append('<input type="hidden" name="fileName" value="'+title.replace(/ /g,'_').replace(/\W+/g,"")+'.xlsx"/>');
			// if(context.headerImg){
				// form.append('<input type="hidden" name="headerImg" value="../..'+main.insFolderName+'images/'+context.headerImg+'"/>');
			// }
			form.append('<input type="hidden" name="action" value="'+action+'"/>');
			form.append($('<input type="hidden" name="dataPkgs"/>').val(JSON.stringify(dataPkgs)));
			$('body').append(form);
			form.get(0).submit();
			form.remove();
		};
		this.exportData=function(exportType){
			/* if(self.exportAllCheck && self.exportAllCheck.checkbox.prop('checked') && self.callerContext){
				self.records=self.callerContext.rows;
			} */
			if(exportType=='xlsx'){
				self.sendExportForm('exportXLSX');
			}else if(exportType=='csv'){
				self.sendExportForm('exportCSV');
			}else if(exportType=='shp'){
				var gjson={
					'type':'FeatureCollection',
					'features':[]
				}
				var dataPkgs={},props,vals,featProps,coods,newgeom,latlng,jsonFeat;
				var geometry_field=main.tables[self.callerContext.callerProperties.name].properties.internal_field_map.geometry_field;
				for(var key in self.records){
					props=main.utilities.createFilePKG(self.records[key].fields,true,true,self);
					vals={};
					for(var key2 in props){
						vals[props[key2].key]=props[key2].value
					}
					featProps=main.map_layers[self.callerContext.callerProperties.name].features[key].properties;
					coods=featProps[geometry_field].coordinates;
					newgeom={
						type:'Point',
						coordinates:[]
					}
					if(featProps[geometry_field].type!="Point"){return;}
					/* //polygon
					for(var y=0;y<coods.length;y++){
						latlng=ol.proj.transform(coods[y], 'EPSG:3857', 'EPSG:4326');
						newgeom.coordinates.push([latlng[0],latlng[1]]);
					} */
					latlng=ol.proj.transform(coods, 'EPSG:3857', 'EPSG:4326');
					newgeom.coordinates=[latlng[0],latlng[1]];
					jsonFeat={
						type:'Feature',
						properties:vals,
						geometry:newgeom
					}
					gjson.features.push(jsonFeat);
				}
				var form=$('<form>',{
					action:'http://ogre.adc4gis.com/convertJson',
					method:'POST'
				}).addClass('dataForm');
				form.append($('<input type="hidden" name="json">').val(JSON.stringify(gjson)));
				form.append('<input type="hidden" name="outputName" value="export.zip"/>');
				$('body').append(form);
				form.get(0).submit();
				form.remove();
			}
			// if(self.callerContext && self.callerContext.clearChecked){self.callerContext.clearChecked();}
			self.exportOptionsPanel.close();
		};
		this.initExportPanel=function(){
			self.exportTypes=[
				{
					properties:{
						name:'xlsx',
						ext:'xlsx',
						alias:'Excel',
						defaultExp:true
					}
				},
				{
					properties:{
						name:'csv',
						ext:'csv',
						alias:'CSV'
					}
				},
				{
					properties:{
						name:'shp',
						alias:'Shapefile'
					}
				}
			];
			self.exportTypeRadios=$('<div class="exportTypeRadios">');
			for(var i=0;i<self.exportTypes.length;i++){
				checked=false;
				if(self.exportTypes[i].properties.defaultExp){checked=true;}
				self.exportTypes[i].radio=new Radio({
					label:self.exportTypes[i].properties.alias,
					value:self.exportTypes[i].properties.name,
					checked:checked,
					name:'exportType_'+self.id,
					classes:'radioInput formInput uiInput toolBoxInput exportType_'+self.id
				});
				self.exportTypeRadios.append(self.exportTypes[i].radio.html);
			}
			// var disabled,checked;
			// if(self.isExportAll){disabled=true;checked=true;}
			// self.exportAllCheck=self.omitChAll=new Checkbox({
				// label:'Export All',
				// checked:checked,
				// classes:'reportExportAll',
				// disabled:disabled
			// });
			self.exportButton=new Button('std','exportButton','Export');
			self.exportButton.html.on('click',function(){
				self.exportData(self.exportTypeRadios.find('.radioInput:checked').val());
			});
			self.exportButtonsWrap=$('<div class="exportButtonsWrap">');
			self.exportButtonsMiddleWrap=$('<div class="exportButtonsMiddleWrap">');
			self.exportOContent=$('<div class="exportOContent">')
				.append(self.exportTypeRadios)
				// .append(self.exportAllCheck.html)
				.append(self.exportButtonsWrap.append(self.exportButton.html));
			self.exportOptionsPanel=new Panel({
				content:self.exportOContent,
				label:'Report Type',
				title:'Report Type',
				classes:'reportTypeSelectPanel'
			});
			self.exportOptionsPanel.open();
		};
		this.reportGenerated=function(){
			// self.callerContext.html.find('.dtRCheckB:checked').prop('checked',false);
		};
		this.initEvents=function(){
			this.typeSelect.on('change',function(){
				var val=$(this).val();
				if(val=='_export'){
					self.initExportPanel();
				}else{
					self.currentReport=val;
					main.utilities[val+'_report_start'](self);
				}
				/* if(val=='inspector'){
					// self.startInspectorForm();
					self.createNewForms(self.types[val].transfer_to_table);
					// self.generateMultiRowReport();
				}
				if(val=='inspectorSheet'){
					main.utilities.inspectorSheet_report_start(self);
					// self.createSheet();
				} */
				self.typeSelectPanel.close();
			});
		};
		this.createHTML();
		this.initEvents();
		if(this.isExport){self.initExportPanel();}
	};
});