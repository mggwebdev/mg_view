define(function(){
	return function pg(args){
		var self=this;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.initialContent=this.initialContent || '';
		this.minWidth=400;
		this.resize={
			resizing:false,
			resizingDir:null,
			coordinates:{}
		};
		this.openExtendeds=[];
		this.opened=false;
		this.opening=false;
		this.closing=false;
		this.open=function(){
			if(!self.opening && !self.opened){
				self.opening=true;
				self.closing=false;
				options={};
				options[this.side]=0;
				this.container.addClass('block');
				this.container.stop().animate(options,main.animations.defaultDuration,main.animations.defaultTransition,function(){
					self.opened=true;
					self.opening=false;
					if(self.onOpen){self.onOpen(self);}
					self.container.find('.dataContentWrap ').each(function(){
						if(main.dataTables[$(this).data('table')] && main.dataTables[$(this).data('table')].needsRefresh){
							main.dataTables[$(this).data('table')].sideReset();
						}
					});
					if(main.canopy && main.canopy.active && self.name!='canopy' && main.canopy.omitSPClose.indexOf(self.name)===-1 && !main.canopy.blockDeactivate){main.canopy.deActivate();}
				});
			}
		};
		this.close=function(context){
			if(!self.closing && self.opened){
				self.closing=true;
				self.opening=false;
				options={};
				options[this.side]=-this.container.outerWidth(true);
				self.container.stop().animate(options,main.animations.defaultDuration,main.animations.defaultTransition,function(){
					$(this).removeClass('block');
					self.opened=false;
					self.closing=false;
					if(self.onClose){self.onClose({context:self,prevContext:context});}
				});
			}
		};
		this.setOnOpen=function(oo){
			self.onOpen=oo;
		};
		this.setOnClose=function(oc){
			self.onClose=oc;
		};
		this.createHTML=function(){
			var button=this.openButtons.data('button');
			var classes='sidePanel '+button;
			this.handleW=$('<div>').addClass('resizeHandle hW').data('dir','W');
			this.handleE=$('<div>').addClass('resizeHandle hE').data('dir','E');
			if(this.right){
				classes+=' right';
				this.side='right';
				this.handleE.addClass('none');
			}else{
				this.side='left';
				this.handleW.addClass('none');
			}
			if(this.defaultOpen) classes+=' defaultOpen';
			this.closeSidePanelButton=$('<div>').addClass('closeSidePanel sPTopButton').attr('title','Close').text('<').on('click',function(){
				self.close();
				main.map.adjustMapSize();
			});
			this.sPTopButtons=$('<div class="sPTopButtons cf">').append(this.closeSidePanelButton);
			this.sidePanelContent=$('<div>').addClass('sidePanelContent');
			this.sidePanelContent=$('<div>').addClass('sidePanelContent');
			this.container=$('<div>').addClass(classes).data('button',button).width(main.layout.defaultSidePanelWidth)
				.append(this.sPTopButtons)
				.append(this.sidePanelContent.html(this.initialContent))
				.append($('<div>').addClass('resizeHandles')
					.append(this.handleW)
					.append(this.handleE)
				);
			this.container.css(this.side,-main.layout.defaultSidePanelWidth);
		};
		this.setContent=function(conent){
			self.sidePanelContent.html(conent);
		};
		this.togglePanel=function(){
			if(self.opened){
				self.close();
				main.map.adjustMapSize();
			}else{
				main.layout.closeAllSidePanels(self);
				self.open();
				main.map.adjustMapSize();
			}
		};
		this.initEvents=function(){
			this.openButtons.add(this.mobileMenuButtons).on('click',function(){
				if($(this).hasClass('mobileMenuItem')){main.layout.header.mobileMenu.slideUp(200);}
				self.togglePanel();
			});
			this.container.on(main.controls.touchBegin,'.resizeHandle',function(e){
				e.stopPropagation();
				if(!self.dragging && !self.minimized && !self.resize.resizing){
					$('html').addClass("noSelect");
					self.container.addClass('customSize');
					self.resize.resizing=true;
					self.resize.resizingDir=$(this).data('dir');
					self.resize.coordinates.mouseStartX=e.pageX;
					self.resize.coordinates.htmlStartWidth=self.container.outerWidth();
				}
			});
			$('html').on(main.controls.touchMove,function(e){
				if(self.resize.resizing){
					if(self.resize.resizingDir=='W'){
						var deltaX=e.pageX-self.resize.coordinates.mouseStartX;
						var newWidth=self.resize.coordinates.htmlStartWidth-deltaX;
					}else if(self.resize.resizingDir=='E'){
						var deltaX=e.pageX-self.resize.coordinates.mouseStartX;
						var newWidth=self.resize.coordinates.htmlStartWidth+deltaX;
					}
					self.container.removeClass('extSidePanel');
					self.container.css('width',newWidth);
					if(main.map){main.map.adjustMapSize();}
				}
			});
			$('html').on(main.controls.touchEnd,function(e){
				if(self.resize.resizing){
					$("html").removeClass("noSelect");
					self.resize.resizing=false;
					main.map.adjustMapSize();
					if(main.dashboard){main.dashboard.refreshResize();}
				}
			});
		};
		this.createHTML();
		this.initEvents();
	};
});