define(function(require){
	return function pg(args){
		var Button=require('./Button');
		var self=this;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.isCheckable=false;
		if(self.context.fields[self.stem_field].input_type=='radio' || self.context.fields[self.stem_field].input_type=='checkbox'){
			this.isCheckable=true;
		}
		this.changeForStemField=function(val){
			if(!val || val==1){
				self.trWrap.detach();
				self.dataUnit.find('.formInput.'+self.dbh_exact).prop('disabled',false);
				self.toggleStemsButton.html.detach();
			}else{
				self.trWrap.css('display','none');
				self.dataUnit.find('.popUpRow.'+self.dbh_exact).after(self.trWrap);
				self.trWrap.slideDown(200);
				self.dataUnit.find('.formInput.'+self.dbh_exact).prop('disabled',true);
				if(self.isCheckable){
					self.dataUnit.find('.popUpRow.'+self.stem_field+' .checksableWrap').after(self.toggleStemsButton.html);
				}else{
					self.dataUnit.find('.formInput.'+self.stem_field).after(self.toggleStemsButton.html);
				}
			}
		};
		(this.createHTML=function(){
			self.tbody=$('<tbody class="multiDBHTBody">');
			self.thead=$('<thead class="multiDBHTHead">')
				.append('<tr><th colspan="2">Multi-Stem DBH Calculator</th></tr>');
			self.table=$('<table class="multiDBHTable" border="1">')
				.append(self.thead)
				.append(self.tbody);
			self.tableWrap=$('<div class="multiDBHTableWrap">').append(self.table);
			self.trWrapTD=$('<td class="multiDBHTRWrapTD" colspan="2">').append(self.tableWrap);
			self.trWrap=$('<tr class="multiDBHTRWrap">').append(self.trWrapTD);
			// self.html=$('<div class="multiDBHWrap">')
				// .append(self.table);
			for(var i=0;i<self.stem_fields.length;i++){
				self.tbody.append(self.dataUnit.find('.popUpRow.'+self.stem_fields[i]));
			}
			self.toggleStemsButton=new Button('std','multiDBHCalcButton','Toggle Multi-DBH Calc');
			self.toggleStemsButton.html.on('click',function(){
				if($.contains(document,self.trWrap[0])){
					self.trWrap.slideUp(200,function(){
						self.trWrap.detach();
					});
				}else{
					self.dataUnit.find('.popUpRow.'+self.dbh_exact).after(self.trWrap);
					self.trWrap.slideDown(200);
				}
			});
			self.tbody.on('change','.formInput',function(){
				var dbh=0;
				self.tbody.find('.formInput').each(function(){
					if($(this).val){
						dbh+=Math.pow($(this).val(),2);
					}
				});
				dbh=Math.round(Math.sqrt(dbh));
				self.dataUnit.find('.formInput.'+self.dbh_exact).val(dbh).change();
			});
			self.dataUnit.on('change','.formInput.'+self.stem_field,function(){
				self.changeForStemField($(this).val());
			});
			if(self.context && self.context.fields && self.context.fields[self.stem_field]){
				if(self.isCheckable){
					var checked=self.dataUnit.find('.formInput.'+self.stem_field).closest('.checksableWrap').find('.formInput:checked');
					if(checked.length){
						self.changeForStemField(checked.val());
					}
				}else{
					self.changeForStemField(self.dataUnit.find('.formInput.'+self.stem_field).val());
				}
			}else{
				self.changeForStemField(self.dataUnit.find('.formInput.'+self.stem_field).val());
			}
		})();
	};
});