define(function(require){
	return function pg(args){
		var self=this;
		this.mapLayers=main.map_layers;
		this.hasActive=false;
		this.hasActiveLayer=false;
		this.unSetActives=function(){
			if(main.map_layers){
				for(var key in main.map_layers){
					if(main.map_layers[key].layer){
						var sourceFeatures=main.map_layers[key].layer.olLayer.getSource().getFeatures();
						for(var t=0;t<sourceFeatures.length;t++){
							sourceFeatures[t].set('isActive',false);
						}
					}
				}
			}
			self.hasActive=null;
			self.hasActiveLayer=null;
			$('.activeRow').removeClass('activeRow');
		};
	};
});