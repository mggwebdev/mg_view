define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Checkbox=require('./Checkbox');
		var MassUpdate=require('./MassUpdate');
		var ViewMore=require('./ViewMore');
		this.alias='Mass Updater';
		this.name='massUpdater';
		this.withItem='with_mass_updater';
		this.defaultSelectType='polygon';
		this.currentSelectType=this.defaultSelectType;
		this.currentLayer=null;
		this.polygonDrawAdded=false;
		this.selecting=false;
		this.selectingFromPoint=false;
		this.currentPIDs=[];
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.addShelf=function(){
			self.launchButton=new Button('toolBox','',self.alias);
			// var link='<a href="'+window.location.href+'exporter/'+'">Click here to Export.</a>';
			self.accordian=main.layout.toolBox;
			self.shelf=main.layout.toolBox.createShelf({
				name:self.name,
				button:self.launchButton.html,
				content:self.html,
				context:self,
				alias:self.alias,
				dontAppendTo:true,
				groups:main.withs[self.withItem].toolbox_groups
			});
			main.layout.toolBox.shelfInAppTest(main.layout.toolBox.shelves[self.name]);
		};
		this.layerSelectUpdate=function(){
			var layers=main.invents.inventories;
			self.layerSelect.html('').append('<option value=""></option>')
			for(var key in layers){
				self.layerSelect.append('<option value="'+key+'">'+layers[key].properties.alias+'</option>')
			}
			main.utilities.sortOptions(self.layerSelect);
		};
		this.resetSelected=function(){
			self.currentPIDs=[];
			self.newCurrentPIDs();
		}
		this.layerSelectChange=function(){
			if($(this).val()){
				if(main.invents.inventories[$(this).val()].inventory.map_layer && main.invents.inventories[$(this).val()].inventory.map_layer.layer){
					self.currentLayer=main.invents.inventories[$(this).val()];
				}else{
					alert('Please load this layer.');
					self.layerSelect.val('');
					self.currentLayer=null;
				}
			}else{
				self.currentLayer=null;
			}
			self.resetSelected();
		};
		this.selectTypeSelectChange=function(){
			if($(this).val()){
				self.currentSelectType=$(this).val();
			}
			self.doneSelectingFromMap();
		};
		this.addPolygonDraw=function(){
			if(!self.polygonDrawAdded){main.map.map.addInteraction(self.draw);}
			self.polygonDrawAdded=true;
			main.invents.drawing=true;
			main.flags.drawing=true;
			main.map.map.on('dblclick',function(e){
				if(main.utilities.mapDoubleClickWhenDrawing()===false){
					e.preventDefault();
				}
			});
		};
		this.removePolygonDraw=function(){
			if(self.polygonDrawAdded){main.map.map.removeInteraction(self.draw);}
			self.polygonDrawAdded=false;
			main.invents.drawing=false;
			main.flags.drawing=false;
			main.map.map.un('dblclick',main.utilities.mapDoubleClickWhenDrawing);
		};
		this.polygonSelected=function(e){
			main.layout.loader.loaderOn();
			var polyPoints=e.feature.getGeometry().getCoordinates()[0];
			var geomField=null;
			if(self.currentLayer.properties.internal_field_map && self.currentLayer.properties.internal_field_map.geometry_field){geomField=self.currentLayer.properties.internal_field_map.geometry_field;}
			if(!geomField){return;}
			var whereItems=main.utilities.getWhere(self.currentLayer.layerName);
			var where=whereItems.where;
			var pids=whereItems.pids;
			if(pids){pids=pids.join(',');}
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
						geomField:geomField,
						polyPoints:polyPoints,
						where:where,
						pids:pids,
						table:self.currentLayer.layerName,
					},
					action:'featuresFromPolygon'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							if(response.results.length){
								for(var i=0;i<response.results.length;i++){
									if(self.currentPIDs.indexOf(response.results[i])==-1){
										self.currentPIDs.push(response.results[i]);
									}
								}
								self.newCurrentPIDs();
							}
							main.layout.loader.loaderOff();
						}
					}
				}
			});
		};
		this.addSelectedShowerListRow=function(pid){
			return $('<div class="massUSlectedShowerListTR cf" data-pid="'+pid+'">')
				.append($('<div class="massUSlectedShowerListTREle massUShowCheckWrap">').html('<input type="checkbox" class="massUShowCheck"/>'))
				.append($('<div class="massUSlectedShowerListTREle massUShowPropWrap">').html(pid))
		};
		this.updateSelectedShowerList=function(feature){
			self.selectedShowerList.html('');
			for(var i=0;i<self.currentPIDs.length;i++){
				self.selectedShowerList.append(self.addSelectedShowerListRow(self.currentPIDs[i]));
			}
			if(self.selectedShowerList.find('.massUSlectedShowerListTR').length){
				self.selectedShowTR.removeClass('none');
			}else{
				self.selectedShowTR.addClass('none');
			}
		};
		this.newCurrentPIDs=function(feature){
			self.currentPIDs.sort(function(a,b){return a-b;});
			self.toggleHighlight('on');
			self.updateSelectedShowerList();
			self.selectedShowerSelectAll.checkbox.prop('checked',false);
			if(!self.currentPIDs.length){
				main.invents.resetHighlightFilter();
			}
		};
		this.featureSelectedByPoint=function(feature){
			if(self.currentPIDs.indexOf(Number(feature.get('pid')))==-1){
				self.currentPIDs.push(feature.get('pid'));
			}
			self.newCurrentPIDs();
		};
		this.beginSelectingFromMap=function(){
			if(self.currentLayer){
				if(main.filter_legend && main.filter_legend.layerName!=self.currentLayer.layerName && main.filter_legend.layers[self.currentLayer.layerName]){
					main.filter_legend.changeLayer(self.currentLayer.layerName);
				}
				if(self.currentSelectType=='point'){
					self.selectingFromPoint=true;
				}else if(self.currentSelectType=='polygon'){
					if(!self.source){
						self.source = new ol.source.Vector({wrapX: false});
						self.draw=new ol.interaction.Draw({
							source: self.source,
							type: 'Polygon'
						});
						self.draw.on('drawend',function(e){
							if(main.utilities.isIntersecting(e.feature)){
								alert('Must be a non-intersecting polygon.');
								return;
							}
							main.invents.blockDoubleClick=true;
							self.polygonSelected(e);
						});
					}
					self.selecting=true;
					self.addPolygonDraw();
				}
				self.doneSelectFromMapButton.html.removeClass('none');
				self.selectFromMapButton.html.addClass('none');
			}else{
				alert('Please select a layer to update.');
			}
		};
		this.doneSelectingFromMap=function(){
			if(self.polygonDrawAdded){self.removePolygonDraw();
			}else if(self.selectingFromPoint){self.selectingFromPoint=false;}
			self.selecting=false;
			self.doneSelectFromMapButton.html.addClass('none');
			self.selectFromMapButton.html.removeClass('none');
		};
		this.removePIDs=function(pids){
			var index;
			for(var i=0;i<pids.length;i++){
				index=self.currentPIDs.indexOf(pids[i]);
				if(self.currentPIDs.indexOf(pids[i])!=-1){
					self.currentPIDs.splice(index,1);
				}
			}
			self.newCurrentPIDs();
		};
		this.toggleHighlight=function(turnTo){
			if(self.currentLayer){
				if((main.invents.currentHighlightFilterLayer || turnTo=='off') && turnTo!='on'){
					main.invents.resetHighlightFilter();
				}else{
					if(self.currentPIDs.length){
						main.invents.currentHighlightFilter=self.currentPIDs;
						main.invents.currentHighlightFilterLayer=self.currentLayer.layerName;
					}
				}
				main.utilities.updateFilterLegend(self.currentLayer.layerName);
			}
		};
		this.beginUpdating=function(){
			if(self.currentLayer){
				self.doneSelectingFromMap();
				if(self.currentPIDs.length){
					if(self.massUpdate){
						self.massUpdate.remove();
						delete self.massUpdate;
					}
					self.massUpdate=new MassUpdate({
						pids:self.currentPIDs,
						layerName:self.currentLayer.layerName
					});
					self.massUpdate.panel.open();
				}else{
					alert('Please select features to update.');
				}
			}else{
				alert('Please select a layer');
			}
		};
		this.addByID=function(){
			if(self.addByIDTA.val().trim() && /^[\d,]+$/.test(self.addByIDTA.val().trim())){
				var pidsLength=self.addByIDTA.val().trim().split(',').length;
				$.ajax({
					type:'POST',
					url:main.mainPath+'server/db.php',
					data:{
						params:{
							folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
							pids:self.addByIDTA.val().trim(),
							pidsLength:pidsLength,
							table:self.currentLayer.layerName,
						},
						action:'validateIDs'
					},
					success:function(response){
						if(response){
							response=JSON.parse(response);
							if(response.status=="OK"){
								if(response.results.length){
									for(var i=0;i<response.results.length;i++){
										if(self.currentPIDs.indexOf(Number(response.results[i]))==-1){
											self.currentPIDs.push(Number(response.results[i]));
										}
									}
									self.newCurrentPIDs();
									self.addByIDTA.val('');
								}
								if(Number(response.pidsLength)!=response.results.length){
									alert('One of more of your entered IDs was not found and not added.');
								}
								main.layout.loader.loaderOff();
							}
						}
					}
				});
			}else{
				alert('The format must be only numeric id\'s seperated by commas.');
			}
		};
		(this.createHTML=function(){
			self.selectTypeSelect=$('<select class="muSelectTypeSelect">')
				.append('<option value="polygon" selected>Polygon</option>')
				.append('<option value="point">Point</option>');
			self.selectTypeSelect.on('change',self.selectTypeSelectChange);
			self.layerSelect=$('<select class="massUpdaterLayerSelect">');
			self.layerSelect.on('change',self.layerSelectChange);
			self.layerSelectUpdate();
			self.addByIDTA=$('<textarea class="massUAddByIDTA">');
			self.addByIDTAButton=new Button('std','massUAddByIDTAButton','Add From IDs');
			self.addByIDTAButton.html.on('click',function(){
				self.addByID();
			});
			self.addByIDTAWrap=$('<div class="massUAddByIDTAWrap">')
				.append(self.addByIDTA)
				.append(self.addByIDTAButton.html);
			self.infoViewMore=new ViewMore({
				prev:'Use this tool to update multiple features at one time.',
				content:'<ul><li>Step 1: Select a layer to update.</li><li>Step 2: Select a selection method, either by point click or by polygon.</li><li>Step 3: Press the "Select From Map" button to begin selecting from the map.</li><li>Step 4: If selecting by polygon, click a polygon around the features of interest. Double click to end the polygon. You may select multiple times. If selecting by clicking features (point), then click the features of interest.</li><li>Step 5: Modify the selected list if necessary.</li><li>Step 6: Click "Update".</li><li>Step 7: Once the popup opens, select the field you would like to edit, select a new value, and then click "Update" in the popup.</li><li>Step 8: You may update multiple fields.</li></ul>',
				hidePrevOnShow:true
			});
			self.doneSelectFromMapButton=new Button('std','massUDoneSelectFromMapButton none','Done Selecting');
			self.doneSelectFromMapButton.html.on('click',function(){
				self.doneSelectingFromMap();
			});
			self.selectFromMapButton=new Button('std','massUSelectFromMapButton','Select From Map');
			self.selectFromMapButton.html.on('click',function(){
				self.beginSelectingFromMap();
			});
			self.toggleHighlightButton=new Button('std','massUToggleHighlight','Toggle Highlight');
			self.toggleHighlightButton.html.on('click',function(){
				self.toggleHighlight();
			});
			self.removePIDButton=new Button('std','massURemovePIDButton','Remove Checked');
			self.removePIDButton.html.on('click',function(){
				var pids=[];
				self.selectedShowerList.find('.massUShowCheck:checked').each(function(){
					pids.push($(this).closest('.massUSlectedShowerListTR').data('pid'));
				});
				self.removePIDs(pids);
			});
			self.selectedShowerListButtons=$('<div class="massUListButtons">')
				.append(self.removePIDButton.html)
				.append(self.toggleHighlightButton.html);
			self.selectedShowerSelectAll=new Checkbox({
				value:'true',
				name:'massUListToggleAll',
				checked:false,
				label:"Toggle All",
				classes:'massUListToggleAll'
			});
			self.selectedShowerSelectAll.checkbox.on('change',function(){
				self.selectedShowerList.find('.massUShowCheck').prop('checked',$(this).prop('checked'));
			});
			self.selectedShowerSelectAllWrap=$('<div class="selectedShowerSelectAllWrap">')
				.append(self.selectedShowerSelectAll.html);
			self.selectedShowerList=$('<div class="massUSelectedShowerList">');
			self.selectedShowerWrap=$('<div class="massUSelectedShowerWrap">')
				.append(self.selectedShowerSelectAllWrap)
				.append(self.selectedShowerList)
				.append(self.selectedShowerListButtons);
			self.selectedShowTR=$('<tr class="none">')
				.append($('<td>').append($('<div class="tdLiner">').html('Selected')))
				.append($('<td>').append($('<div class="tdLiner">').append(self.selectedShowerWrap)));
			self.updateButton=new Button('std','massUUpdateButton','Update');
			self.updateButton.html.on('click',function(){
				self.beginUpdating();
			});
			self.bottomButtons=$('<div class="massUBottomButtons">')
				.append(self.updateButton.html);
			self.table=$('<table border="1" class="massUpdaterTable">')
				.append($('<tr>')
					.append($('<td>').append($('<div class="tdLiner">').text('Select Layer')))
					.append($('<td>').append($('<div class="tdLiner">').append(self.layerSelect)))
				)
				.append($('<tr>')
					.append($('<td>').append($('<div class="tdLiner">').text('Select Type')))
					.append($('<td>').append($('<div class="tdLiner">').append(self.selectTypeSelect)))
				)
				.append($('<tr>')
					.append($('<td>').append($('<div class="tdLiner">').text('Add By ID (seperate by comma)')))
					.append($('<td>').append($('<div class="tdLiner">').append(self.addByIDTAWrap)))
				)
				.append($('<tr>')
					.append($('<td>').append($('<div class="tdLiner">').html('Add By Map')))
					.append($('<td>').append($('<div class="tdLiner">')
						.append(self.selectFromMapButton.html)
						.append(self.doneSelectFromMapButton.html)
					))
				)
				.append(self.selectedShowTR)
				.append($('<tr>')
					.append($('<td colspan="2">').append($('<div class="tdLiner">').html(self.bottomButtons)))
				)
			self.html=$('<div class="massUpdaterWrap">');
			self.html.append(self.infoViewMore.html);
			self.html.append(self.table);
			self.addShelf();
		})();
	}
});
