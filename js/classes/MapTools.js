define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Panel=require('./Panel');
		this.alias='Map Tools';
		this.mapTools={};
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.zoomInClicked=function(){
			self.context.animateZoom(self.map.getView().getZoom()+1);
		};
		this.zoomOutClicked=function(){
			self.context.animateZoom(self.map.getView().getZoom()-1);
		};
		this.homeExtentClicked=function(){
			if(main.invents && main.invents.currentCoords){
				if(main.invents.currentZoom){var zoom=main.invents.currentZoom;
				}else if(main.personal_settings.map.zoom){var zoom=main.personal_settings.map.zoom;}
				else{var zoom=14;}
				self.context.panTo(main.invents.currentCoords);
				self.context.animateZoom(zoom);
			}else{
				self.context.panTo(ol.proj.transform(main.personal_settings.map.center, 'EPSG:4326', 'EPSG:3857'));
				self.context.animateZoom(main.personal_settings.map.zoom);
			}
		};
		this.geolocateButtonClicked=function(){
			if(self.context.geolocating){self.context.turnGeolocationOff();
			}else{self.context.turnGeolocationOn();}
		}
		this.createMapToolItem=function(name,label,onClick,title){
			title=title || '';
			var mapToolLabel=$('<div class="mapToolLabel">').html(label)
			var button=$('<div class="mapToolItem '+name+' noSelect" data-name="'+name+'" title="'+title+'">')
				.append(mapToolLabel);
			button.on('click',function(){
				var name=$(this).data('name');
				self.mapTools[name].onClick(self,name);
			});
			this.mapTools[name]={
				name:name,
				label:label,
				title:title,
				mapToolLabel:mapToolLabel,
				button:button,
				onClick:onClick
			}
			return button;
		};
		this.addItem=function(name,html,onClick,title){
			self.mapToolsItems.append(self.createMapToolItem(name,html,onClick,title));
		};
		(this.createHTML=function(){
			self.html=$('<div class="mapToolsWrap">');
			self.mapToolsItems=$('<div class="mapToolsItems">');
			self.html
				.append(self.mapToolsItems);
			//zoom in
			self.addItem('zoomIn','+',self.zoomInClicked,'Zoom In');
			//zoom out
			self.addItem('zoomOut','-',self.zoomOutClicked,'Zoom Out');
			//home extent
			self.addItem('homeExtent','<img draggable="false" class="mapToolImg" src="'+main.mainPath+'images/house.png">',self.homeExtentClicked,'Go To Home Extent');
			//geolocation
			self.addItem('geolocateMapToolButton','<img draggable="false" class="mapToolImg" src="'+main.mainPath+'images/crosshairs_white.png">',self.geolocateButtonClicked,'Find Your Location');
			self.panel=new Panel({
				content:self.html,
				title:self.alias,
				classes:'mapTools',
				relativeTo:$(self.map.getTargetElement()),
				position:'tLeft'
			});
			
			self.map.on('change:size',function(){self.panel.positionPanel();});
			self.panel.open();
		})();
	};
});