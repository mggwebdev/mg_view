define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Checkbox=require('./Checkbox');
		var Input=require('./Input');
		var Panel=require('./Panel');
		var Range=require('./Range');
		var ViewMore=require('./ViewMore');
		this.name='ecobens';
		this.alias='Eco Benefits';
		this.withItem='with_ecobens';
		this.classes='';
		this.currentLayer=null;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.layers=main.tables;
		this.viewMorePrev='Use this tool to measure the eco benefits of selected trees.';
		this.viewMoreContent='<ul><li>Step 1: Select a layer to apply the filters to.</li><li>Step 2: Select which tools to apply the filters to.</li><li>Step 3: Change the filter parameters.</li></ul>';
		this.onPanelOpen=function(){
		};
		this.onPanelClose=function(){
		};
		this.resetFilters=function(){
			// self.html.find('.filterInput.textInput').val('');
			// self.html.find('.filterInput.filterCheck').prop('checked',true);
			// self.html.find('.showNullsOption').prop('checked',true);
			// self.html.find('.rangeOuterWrap').each(function(){
				// self.ranges[$(this).attr('id')].reset();
			// });
		};
		this.reset=function(){
			if(self.currentLayer){self.hardClearFilter();}
			self.currentLayer=null;
			self.layerSelect.val('');
			self.fieldsWrap.html('');
			self.advancedFilterWrapTop.removeClass('layerSelected');
			self.clearFilterButton.html.addClass('none');
			self.panel.positionPanel();
		};
		this.hardClearFilter=function(){
			self.clearFilter();
			self.resetFilters();
			self.filter.currentFilters={};
		};
		this.updateFields=function(){
			self.fieldsWrap.html('');
			// for(var key in fields){
			// }
			self.panel.positionPanel();
		};
		this.newLayerSelected=function(){
			self.updateFields();
			self.advancedFilterWrapTop.addClass('layerSelected');
			self.clearFilterButton.html.removeClass('none');
		};
		this.addShelf=function(){
			self.launchButton=new Button('toolBox','',self.alias);
			self.panelLaunch=new Button('std','resetButton ecoBensShelfTopB','Launch Eco Benefits');
			self.panelLaunch.html.click(function(){
				if(!self.panel.opened){self.panel.open();
				}else{self.panel.close();}
			});
			self.shelfTopBs=$('<div class="shelfTopBs">')
				.append(self.panelLaunch.html);
			self.shelfViewMore=new ViewMore({
				prev:self.viewMorePrev+' Click the launch button to start.',
				content:self.viewMoreContent
			});
			self.shelfContent=$('<div class="ecoBensWrapShelfContent">')
				.append(self.shelfViewMore.html)
				.append(self.shelfTopBs);
			self.shelf=main.layout.toolBox.createShelf({
				name:self.name,
				button:self.launchButton.html,
				content:self.shelfContent,
				alias:self.alias,
				groups:main.withs[self.withItem].toolbox_groups
			});
		};
		this.createHTML=function(){
			self.html=$('<div class="ecoBensWrap">');
			self.layerSelect=$('<select class="ecoBLayerSelect">');
				self.layerSelect.append('<option value=""></option>');
			var name;
			for(var key in self.layers){
				name=self.layers[key].properties.layer_name || self.layers[key].properties.name;
				self.layerSelect.append('<option value="'+name+'">'+self.layers[key].properties.alias+'</option>');
			}
			self.layerSelect.on('change',function(){
				if($(this).val()){
					if(self.layers[$(this).val()].map_layer){
						if(self.layers[$(this).val()].map_layer.layer){
							self.currentLayer=self.layers[$(this).val()];
							self.newLayerSelected();
							return;
						}else{
							alert('Please load this layer.');
						}
					}else if(self.layers[$(this).val()].appForm){
						self.currentLayer=self.layers[$(this).val()];
						self.newLayerSelected();
						return;
					}
				}
				self.reset();
			});
			main.utilities.sortOptions(self.layerSelect);
			self.advancedFilterWrapTop=$('<tbody class="advancedFilterWrapTop">')
				.append($('<tr class="advFTR">')
					.append($('<td>').append($('<div class="tdLiner">').append('Select Layer')))
					.append($('<td>').append($('<div class="tdLiner">').append(self.layerSelect))))
					
			self.resetButton=new Button('std','resetButton advFButtonsTopB','Reset');
			self.advFTopBs=$('<div class="advFTopBs">')
				.append(self.resetButton.html);
			self.resetButton.html.on('click',function(){
				self.reset();
			});
			self.viewMore=new ViewMore({
				prev:self.viewMorePrev,
				content:self.viewMoreContent
			});
			self.topTop=$('<div class="topTop">')
				.append(self.viewMore.html);
			self.fieldsWrap=$('<tbody class="advFFields">')
			self.table=$('<table border="1" class="advFTable">')
				.append(self.advancedFilterWrapTop)
				.append(self.fieldsWrap);
			self.html=$('<div class="ecoBensHTML">')
				.append(self.topTop)
				.append(self.advFTopBs)
				.append(self.table);
			self.panel=new Panel({
				content:self.html,
				title:self.alias,
				matchHeight:true,
				relativeTo:main.layout.mapContainerWrap,
				classes:'advFilterPanel',
				centerOnOrientationChange:true,
				onOpen:self.onPanelOpen,
				onClose:self.onPanelClose
			});
			self.addShelf();
		};
		this.initEvents=function(){
		};
		(this.init=function(){
			self.createHTML();
			self.initEvents();
			// if(main.map.mapTools){
				// self.mapToolButtonClicked=function(){
					// if(!self.panel.opened){self.panel.open();
					// }else{self.panel.close();}
				// };
				// main.map.mapTools.addItem('advFilter','<img draggable="false" class="mapToolImg" src="'+main.mainPath+'images/filter_white.png">',self.mapToolButtonClicked,self.alias);
			// }
			
		})();
	};
});