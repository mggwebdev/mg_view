define(function(require){
	return function pg(){
		var self=this;
		var Button=require('./Button');
		var Checkbox=require('./Checkbox');
		this.alias='Settings';
		this.withItem='with_settings';
		this.params={};
		this.createHTML=function(){
			this.addShelf();
		};
		this.addShelf=function(){
			this.launchButton=new Button('toolBox','',this.alias);
			this.settingsForm=this.createSettingsForm();
			this.shelf=main.layout.toolBox.createShelf({
				name:'settings',
				button:this.launchButton.html,
				content:this.settingsForm,
				alias:this.alias,
				groups:main.withs[self.withItem].toolbox_groups
			});
		};
		this.createSettingsForm=function(){
			var settingsForm=$('<form>').addClass('settingsForm');
			// var selected,options;
			
			self.tbody=$('<tbody>');
			self.table=$('<table>').addClass('shelfForm')
				.append($('<thead>')
					.append($('<tr>')
						.append($('<th colspan="2">')
							.append($('<div>').addClass('tdLiner center').text('Settings'))
						)
					)
				).append(self.tbody);
			/* for(var key in this.params){
				param=this.params[key];
				if(param.input_type=='number' || param.input_type=='text'){
					default_val=param.default_val,max='',min='',step='';
					if(!default_val && default_val!==0) default_val=''
					if(param.max || param.max===0) max=' max="'+param.max+'"';
					if(param.min || param.min===0) min=' min="'+param.min+'"';
					if(param.step || param.step===0) step=' step="'+param.step+'"';
					param.input=$('<input type="'+param.input_type+'" name="'+key+'" value="'+default_val+'"'+max+min+step+'>').addClass('textInput settingsInput formInput uiInput toolBoxInput '+key).data('name',key);
				}else if(param.input_type=='checkbox'){
					options=param.options;
					for(var key2 in options){
						selected='';
						if(options[key2].value==param.default_val) selected=' checked="checked"';
						param.input=new Checkbox({
							value:options[key2].value,
							name:key,
							checked:selected,
							classes:'checkboxInput settingsInput formInput uiInput toolBoxInput '+key,
							data:[['name',key]]
						}).html
					}
				}else if(param.input_type=='select'){
					param.input=$('<select name="'+key+'">').addClass('selectInput settingsInput formInput uiInput toolBoxInput '+key).data('name',key);
					options=param.options;
					for(var key2 in options) param.input.append('<option value="'+options[key2].name+'">'+options[key2].alias+'</option>');
				}
				table.append($('<tr>')
					.append($('<td>').append($('<div>').addClass('tdLiner').data('name',key).text(param.alias)))
					.append($('<td>').append($('<div>').addClass('tdLiner').append(param.input)))
				)
			} */
				// table.append($('<tr>')
					// .append($('<td>').append($('<div>').addClass('tdLiner').data('name',key).text(param.alias)))
					// .append($('<td>').append($('<div>').addClass('tdLiner').append(param.input)))
				// )
			return settingsForm.append(self.table);
		};
		this.updateButtonsShow=function(){
		};
		this.updateButtonsHide=function(){
		};
		this.addSetting=function(label,content,wholeRow){
			var showButton=new Button('std','showButton','Show');
			var hideButton=new Button('std','hideButton','Hide');
			var table=$('<table>')
				.append($('<tbody>')
					.append($('<tr>')
						.append($('<th>')
							.append($('<td>')
								.append($('<div>').addClass('tdLiner')
									.append(showButton.html)
									.append(hideButton.html)
								)
							)
						)
					)
				);
			if(!wholeRow){
				self.tbody.append($('<tr>')
					.append($('<td>').append($('<div class="tdLiner settingsLabel">').html(label)))
					.append($('<td>').append($('<div class="tdLiner settingsContent">').html(table)))
				)
			}else{
				self.tbody.append($('<tr>')
					.append($('<td colspan="2">').append($('<div class="settingsContent fullRowContent">').html(table)))
				)
			}
			return {
				table:table,
				showButton:showButton,
				hideButton:hideButton
			}
		}
		this.updateSettings=function(){
			main.utilities.setCurrentVals(this.settingsForm,this.params);
		};
		this.settingsInputChange=function(){
			self.updateSettings();
		};
		this.initEvents=function(){
			// this.settingsForm.on('change','.formInput',function(e){
				// e.preventDefault();
				// self.settingsInputChange();
			// });
		};
		this.createHTML();
		// main.utilities.setCurrentVals(this.settingsForm,this.params);
		this.initEvents();
	};
});