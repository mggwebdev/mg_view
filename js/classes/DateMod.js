define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		this.name='';
		this.value='';
		this.disabled='';
		this.readonly='';
		this.classes='';
		this.on_change_cb=null;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		(this.createHTML=function(){
			var disabled='',readonly='';
			if(self.disable){disabled=' disabled';}
			if(self.readonly){readonly=' readonly';}
			self.html=$('<input type="text" readonly="true" placeholeder="mm/dd/YYYY" name="'+self.name+'" value="'+self.value+'"'+disabled+readonly+'>').addClass('dateInput '+self.classes).data('name',self.name);
			self.html.datepicker({});
			self.html.datepicker('setDate',self.value);
			if(self.on_change_cb){
				self.html.data('on_change',self.on_change_cb);
				self.html.on('change',function(){
					main.utilities.callFunctionFromName($(this).data('on_change'),window,{context:self});
				});
			}
		})();
	};
});