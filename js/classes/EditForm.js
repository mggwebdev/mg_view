define(function(require){
	return function pg(args){
		var Button=require('./Button');
		var Checkbox=require('./Checkbox');
		var DateMod=require('./DateMod');
		var Input=require('./Input');
		var MassUpdate=require('./MassUpdate');
		var MultiDBHCalc=require('./MultiDBHCalc');
		var Panel=require('./Panel');
		var Table=require('./Table');
		var Tabs=require('./Tabs');
		var self=this;
		this.id=main.utilities.generateUniqueID();
		this.activeTab=null;
		this.timeTracked=null;
		this.editable=true;
		this.linkOppWOMOnAdd=true;
		this.oppositeWOM=null;
		this.openOnStart=false;
		// this.dataLastChangedField=null;
		// this.dataLastChangedThis=null;
		// this.dataLastChangedE=null;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.extraFields={};
		this.fieldInputs={};
		this.alias=self.inventory.singular_alias+' Details';
		this.convertToEditing=function(pid){
			self.inventory.map_layer.features[pid].editLargeTable.table.find('.outerEditingW').removeClass('editing').addClass('notEditing').find('.dataTableEdit').addClass('block');
			self.inventory.map_layer.features[pid].editLargeTable.table.find('.dataTableCurrent,.dataTableEditLink,.dataTableDoneEditLink').addClass('none');
		};
		this.removeEditing=function(pid){
			self.inventory.map_layer.features[pid].editLargeTable.table.find('.dataTableEditPkg').remove();
		};
		this.open=function(){
			self.panel.open();
		};
		this.pointsLoadedAfterRefresh=function(params){
			main.invents.inventories.trees.inventory.map_layer.features[params.pid].popup.editForm=main.invents.inventories.trees.inventory.map_layer.features[params.pid].editForm;
			main.invents.inventories.trees.inventory.map_layer.features[params.pid].popup.refresh();
		};
		this.loadLast=function(feature){
			// $.ajax({
				// type:'POST',
				// url:main.mainPath+'server/db.php',
				// data:{
					// params:{
						// folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						// pid:self.properties.pid,
						// lastLoaded:feature.get('pid'),
						// table:self.inventory.layerName
					// },
					// action:'loadLast'
				// },
				// success:function(response){
					// if(response){
						// response=JSON.parse(response);
						// if(response.status=="OK"){
							// if(response.pid){
								// main.loader.loadMoreMapPoints(self.inventory.layerName,null,null,self.pointsLoadedAfterRefresh,{pid:response.pid});
							// }
						// }
					// }
				// }
			// });
			// return;
			main.layout.loader.loaderOn();
			var props=feature.getProperties();
			if(self.inventory.properties.omit_load_last_fields){
				var omitLoadLasts=self.inventory.properties.omit_load_last_fields;
			}else if(self.inventory.properties.internal_field_map){
				var omitLoadLasts=[];
				var internalFieldMap=self.inventory.properties.internal_field_map;
				if(internalFieldMap.address_number){omitLoadLasts.push(internalFieldMap.address_number);}
				if(internalFieldMap.address_num_street){omitLoadLasts.push(internalFieldMap.address_num_street);}
				if(internalFieldMap.street){omitLoadLasts.push(internalFieldMap.street);}
				if(internalFieldMap.city){omitLoadLasts.push(internalFieldMap.city);}
				if(internalFieldMap.zip){omitLoadLasts.push(internalFieldMap.zip);}
				if(internalFieldMap.state){omitLoadLasts.push(internalFieldMap.state);}
				if(internalFieldMap.geometry_field){omitLoadLasts.push(internalFieldMap.geometry_field);}
				if(internalFieldMap.geometry_field){omitLoadLasts.push(internalFieldMap.geometry_field);}
				omitLoadLasts.push('pid');
			}else{
				var omitLoadLasts=[];
			}
			if(self.inventory.properties.internal_field_map && self.inventory.properties.internal_field_map.last_modified && omitLoadLasts.indexOf(self.inventory.properties.internal_field_map.last_modified)==-1){
				omitLoadLasts.push(self.inventory.properties.internal_field_map.last_modified);
			}
			var input,field,value,htmlStuff,changeFields=[];
			for(var i=0;i<self.sortedFieldKeys.length;i++){
				key=self.sortedFieldKeys[i];
				if(omitLoadLasts.indexOf(key)===-1){
					field=self.fields[key];
					if(field.active){
						value=props[key];
						changeFields.push({field:field,val:value});
						input=self.itemForm.find('.popUpRow.'+field.name+' .dataTableEdit .formInput');
						if(!self.inventory.map_layer.features[self.properties.pid].fields){
							self.inventory.mapInteractions.createFeatureHTML(self.properties);
						}
						htmlStuff=main.utilities.setUnit(field,input,value,null,null,self.fieldInputs[field.name],self.fields,self.inventory.layerName,null,true);
						html=htmlStuff.html;
						self.inventory.map_layer.features[self.properties.pid].fields[field.name].current_val=htmlStuff.val;
						if(html!==false){
							self.inventory.map_layer.features[self.properties.pid].fields[field.name].html=html;
							if(self.inventory.map_layer.features[self.properties.pid].editTable.table){self.inventory.map_layer.features[self.properties.pid].editTable.table.find('.popUpRow.'+field.name+' .dataTableCurrent').html(html);}
							if(self.inventory.map_layer.features[self.properties.pid].editLargeTable){self.inventory.map_layer.features[self.properties.pid].editLargeTable.formWrap.find('.popUpRow.'+field.name+' .dataTableCurrent').html(html);}
						}
						if(self.inventory.map_layer.features[self.properties.pid].dataTable){
							self.inventory.map_layer.features[self.properties.pid].dataTable.updateDataTable(self.properties.pid,field.name,input,false,true,true);
						}
					}
				}
			}
			if(input){
				if(!main.misc.blockUpdateData && !main.misc.blockUpdateData2){
					main.dataEditor.updateData({
						pids:[self.properties.pid],
						reference:input,
						context:self.inventory,
						changeFields:changeFields
					});
				}
			}
			main.layout.loader.loaderOff();
		};
		this.editFormClose=function(){
			if(main.invents && main.invents.currentHighlightFilter){self.showAllItems(main.invents.currentHighlightFilterLayer);}
			self.stopAddingAll();
			if(self.inventory.adding && !self.inventory.countOpenEditForms() && !main.browser.isMobile){
				self.inventory.editFormClosed();
			}
		};
		this.tabChanged=function(tab){
			self.panel.initPanelHeightMatch();
			if(self.womInvUnits){
				var key;
				tab.content.find('.tableContentTableWrap').each(function(){
					key=$(this).closest('.womInvUnit').data('key');
					self.womInvUnits[key].tableContentTableWrap.css('height',self.womInvUnits[key].height);
				});
			}
			// self.panel.positionPanel(true);
			// self.panel.initPanelHeightMatch();
		};
		this.sortByAlias=function(keys){
			var aAlias,bAlias;
			return keys.sort(function(a,b){
				aAlias=self.fields[a].alias.toLowerCase();
				bAlias=self.fields[b].alias.toLowerCase();
				if(aAlias>bAlias){return 1;}
				if(aAlias<bAlias){return -1;}
				return 0;
			});
		};
		this.addPolygonDraw=function(inv){
			if(!self.womInvUnits[inv].polygonDrawAdded){main.map.map.addInteraction(self.womInvUnits[inv].draw);}
			self.womInvUnits[inv].polygonDrawAdded=true;
			main.invents.drawing=true;
			main.flags.drawing=true;
			main.map.map.on('dblclick',function(e){
				if(main.utilities.mapDoubleClickWhenDrawing()===false){
					e.preventDefault();
				}
			});
		};
		this.removePolygonDraw=function(inv){
			if(self.womInvUnits[inv].polygonDrawAdded){main.map.map.removeInteraction(self.womInvUnits[inv].draw);}
			self.womInvUnits[inv].polygonDrawAdded=false;
			main.invents.drawing=false;
			main.flags.drawing=false;
			main.map.map.un('dblclick',main.utilities.mapDoubleClickWhenDrawing);
		};
		this.startSelectFromMap=function(inv){
			var invUnit=self.womInvUnits[inv];
			if(main.filter_legend && main.filter_legend.layerName!=inv && main.filter_legend.layers[inv]){
				main.filter_legend.changeLayer(inv);
			}
			if(invUnit.selectionMethod=='polygon'){
				if(!self.womInvUnits[inv].source){
					self.womInvUnits[inv].source = new ol.source.Vector({wrapX: false});
					self.womInvUnits[inv].draw=new ol.interaction.Draw({
						source: self.womInvUnits[inv].source,
						type: 'Polygon'
					});
					self.womInvUnits[inv].draw.set('inv',inv);
					self.womInvUnits[inv].draw.on('drawend',function(e){
						if(main.utilities.isIntersecting(e.feature)){
							alert('Must be a non-intersecting polygon.');
							return;
						}
						main.invents.blockDoubleClick=true;
						// self.removePolygonDraw(this.get('inv'));
						self.womSelectedFromMap(e,this.get('inv'));
					});
				}
				self.addPolygonDraw(inv);
			}else if(invUnit.selectionMethod=='point'){
				main.invents.womSelectInv=inv;
				main.invents.womSelectEditForm=self;
			}
			self.womInvUnits[inv].addFromMapButton.html.addClass('none');
			self.womInvUnits[inv].doneAddingFromMapButton.html.removeClass('none');
		};
		this.womOnMapClick=function(feature,inv){
			// main.invents.womSelectInv=null;
			// main.invents.womSelectEditForm=null;
			self.addInvItems([feature.get('pid')],inv);
		};
		this.womSelectedFromMap=function(e,inv){
			main.layout.loader.loaderOn();
			var polyPoints=e.feature.getGeometry().getCoordinates()[0];
			var geomField=null;
			var table=main.tables[inv];
			if(table && table.properties.internal_field_map && table.properties.internal_field_map.geometry_field){geomField=table.properties.internal_field_map.geometry_field;}
			if(!geomField){return;}
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						polyPoints:polyPoints,
						geomField:geomField,
						table:inv,
					},
					action:'featuresFromPolygon'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							main.layout.loader.loaderOff();
							self.addInvItems(response.results,response.params.table);
						}
					}
				}
			});
		};
		this.addInvItems=function(pids,inv){
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
						pid:self.properties.pid,
						pids:JSON.stringify(pids),
						extraFields:self.extraFields,
						ownLayer:self.inventory.layerName,
						assocLayer:inv,
					},
					action:'addAssocInvWOM'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							var results=response.invResults,inv=response.params.assocLayer;
							if(results.length){
								var extraFields0=response.params.extraFields || null;
								var extraFields=null;
								if(extraFields0 && extraFields0[inv]){extraFields=extraFields0[inv];}
								for(var i=0;i<results.length;i++){
									self.womInvUnits[inv].tbody.append(self.addWOMUnitRow(results[i],extraFields));
								}
							}
							if(!response.newData.length){
								alert('No new '+main.tables[inv].properties.plural_alias.toLowerCase()+' added.');
							}
							if(main.invents.currentHighlightFilter){
								self.updatecurrentHighlightFilter(inv);
							}
							self.allocateNoAssoc(inv,true);
							if(main.filter_legend){self.setDefaultButtons(main.filter_legend.layerName);}
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.allocateNoAssoc=function(inv,sort){
			if(self.womInvUnits[inv].tbody.find('.womUnitTableTR').length){
				self.womInvUnits[inv].tableMod.sortTable(self.currentSortBy,null,true);
				self.womInvUnits[inv].noAssoc.detach();
				self.womInvUnits[inv].tableContentTableWrap.append(self.womInvUnits[inv].tableTableWrap);
				self.womInvUnits[inv].removeButton.html.removeClass('none');
				if(self.womInvUnits[inv].massUpdateButton){self.womInvUnits[inv].massUpdateButton.html.removeClass('none');}
				// if(main.invents.currentHighlightFilterLayer){
					// self.womInvUnits[inv].showAssocItemsButton.html.addClass('none');
					// self.womInvUnits[inv].showAllItemsButton.html.removeClass('none');
				// }else{
					// self.womInvUnits[inv].showAssocItemsButton.html.removeClass('none');
					// self.womInvUnits[inv].showAllItemsButton.html.addClass('none');
				// }
			}else{
				self.womInvUnits[inv].checkAll.prop('checked',false);
				self.womInvUnits[inv].tableTableWrap.detach();
				self.womInvUnits[inv].tableContentTableWrap.append(self.womInvUnits[inv].noAssoc);
				self.womInvUnits[inv].removeButton.html.addClass('none');
				if(self.womInvUnits[inv].massUpdateButton){self.womInvUnits[inv].massUpdateButton.html.addClass('none');}
				// self.womInvUnits[inv].showAssocItemsButton.html.addClass('none');
				// if(self.currentHighlightFilter){
					// self.womInvUnits[inv].showAllItemsButton.html.removeClass('none');
				// }
			}
		};
		this.updateAssItems=function(inv){
			var pids=[];
			self.womInvUnits[inv].tbody.find('.womUnitCheck:checked').each(function(){
				pids.push($(this).closest('.womUnitTableTR').data('pid'));
			});
			if(!pids.length){
				alert('Please select '+main.invents.inventories[inv].properties.plural_alias.toLowerCase()+' to update.');
				return;
			}
			if(self.massUpdate){
				self.massUpdate.remove();
				delete self.massUpdate;
			}
			self.massUpdate=new MassUpdate({
				pids:pids,
				layerName:inv
			});
			self.massUpdate.panel.open();
		};
		this.removeAssItems=function(inv){
			var pids=[];
			self.womInvUnits[inv].tbody.find('.womUnitCheck:checked').each(function(){
				pids.push($(this).closest('.womUnitTableTR').data('pid'));
			});
			if(!pids.length){
				alert('Please select '+main.invents.inventories[inv].properties.plural_alias.toLowerCase()+' to unlink.');
				return;
			}
			if(!confirm("Are you sure you want to remove these?")){return;}
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
						pid:self.properties.pid,
						pids:pids,
						inv:inv,
						layer:self.inventory.layerName
					},
					action:'removeAssocInvWOM'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							var pids=response.params.pids,inv=response.params.inv;
							for(var i=0;i<pids.length;i++){
								self.womInvUnits[inv].tbody.find('.womUnitTableTR[data-pid="'+pids[i]+'"]').remove()
							}
							self.updatecurrentHighlightFilter(inv);
							self.allocateNoAssoc(inv,true);
							if(main.filter_legend){self.setDefaultButtons(main.filter_legend.layerName);}
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.geoFound=function(response){
			main.layout.removeAllPopUps();
			main.map.panTo(response.mainCoords);
			main.map.animateZoom(main.misc.defaultZoomTo);
		};
		// this.showAllOpp=function(){
			// self.showAllItems();
			// main.utilities.updateFilterLegend(self.oppositeWOM);
			// self.showAllSelfItemsButtonInv.html.removeClass('none');
			// self.showAllOppositeItemsButtonInv.html.addClass('none');
			// for(var key in self.womInvUnits){
				// self.womInvUnits[key].showAllItemsButton.html.removeClass('none');
				// self.womInvUnits[key].showAssocItemsButton.html.removeClass('none');
			// }
		// };
		this.showAllSelf=function(){
			main.invents.resetHighlightFilter();
			main.utilities.updateFilterLegend(self.inventory.layerName);
			// self.showAllSelfItemsButtonInv.html.addClass('none');
			// self.showAllOppositeItemsButtonInv.html.addClass('none');
			self.setDefaultButtons(self.inventory.layerName);
			/*for(var key in self.womInvUnits){
				self.womInvUnits[key].showAllItemsButton.html.removeClass('none');
				if(self.womInvUnits[key].tbody.find('.womUnitTableTR').length){
					// self.womInvUnits[key].showAssocItemsButton.html.removeClass('none');
				}
			}*/
		};
		/* this.showAllItemsButtonsFix=function(inv){
			for(var key in self.womInvUnits){
				if(key==inv){
					self.womInvUnits[key].showAllItemsButton.html.addClass('none');
					if(self.womInvUnits[key].tbody.find('.womUnitTableTR').length){
						self.womInvUnits[key].showAssocItemsButton.html.removeClass('none');
					}
				}else{
					self.womInvUnits[key].showAllItemsButton.html.removeClass('none');
					self.womInvUnits[key].showAssocItemsButton.html.addClass('none');
					// if(self.womInvUnits[key].tbody.find('.womUnitTableTR').length){
						// self.womInvUnits[key].showAssocItemsButton.html.removeClass('none');
					// }
				}
			}
		}; */
		this.showAllItems=function(inv){
			self.stopAddingAll(inv);
			main.invents.resetHighlightFilter();
			main.utilities.updateFilterLegend(inv);
			// self.showAllSelfItemsButtonInv.html.removeClass('none');
			// self.showAllOppositeItemsButtonInv.html.removeClass('none');
			// self.showAllItemsButtonsFix(inv);
			self.setDefaultButtons(inv);
		};
		this.showAllAssocItems=function(inv){
			self.stopAddingAll(inv);
			self.updatecurrentHighlightFilter(inv);
			main.utilities.updateFilterLegend(inv);
			// self.showAllSelfItemsButtonInv.html.removeClass('none');
			// self.showAllOppositeItemsButtonInv.html.removeClass('none');
			// self.womInvUnits[inv].showAllItemsButton.html.removeClass('none');
			// self.womInvUnits[inv].showAssocItemsButton.html.addClass('none');
			self.setDefaultButtons(inv);
			/*for(var key in self.womInvUnits){
				if(key==inv){
					self.womInvUnits[key].showAllItemsButton.html.removeClass('none');
						self.womInvUnits[key].showAssocItemsButton.html.addClass('none');
					// if(self.womInvUnits[key].tbody.find('.womUnitTableTR').length){
						// self.womInvUnits[key].showAssocItemsButton.html.addClass('none');
					// }
				}else{
					self.womInvUnits[key].showAllItemsButton.html.removeClass('none');
					self.womInvUnits[key].showAssocItemsButton.html.addClass('none');
					// if(self.womInvUnits[key].tbody.find('.womUnitTableTR').length){
						// self.womInvUnits[key].showAssocItemsButton.html.removeClass('none');
					// }
				}
			}*/
		};
		this.setDefaultButtons=function(inv){
			for(var key in self.womInvUnits){
				if(self.womInvUnits[key].tbody.find('.womUnitTableTR').length){
					if(key==inv){
						if(main.invents.currentHighlightFilterLayer){
							self.womInvUnits[key].showAllItemsButton.html.removeClass('none');
							self.womInvUnits[key].showAssocItemsButton.html.addClass('none');
						}else{
							self.womInvUnits[key].showAllItemsButton.html.addClass('none');
							self.womInvUnits[key].showAssocItemsButton.html.removeClass('none');
						}
					}else{
						self.womInvUnits[key].showAllItemsButton.html.removeClass('none');
						self.womInvUnits[key].showAssocItemsButton.html.addClass('none');
					}
				}else{
					if(key==inv){
						self.womInvUnits[key].showAllItemsButton.html.addClass('none');
						self.womInvUnits[key].showAssocItemsButton.html.addClass('none');
					}else{
						self.womInvUnits[key].showAllItemsButton.html.removeClass('none');
						self.womInvUnits[key].showAssocItemsButton.html.addClass('none');
					}
				}
			}
		};
		this.refreshPoints=function(inv){
			if(main.filter_legend && main.filter_legend.layerName==inv){
				main.filter_legend.refreshPoints();
			}
		};
		this.getAssocPIDs=function(inv){
			var pids=[];
			self.womInvUnits[inv].tbody.find('.womUnitTableTR').each(function(){
				pids.push($(this).data('pid'));
			});
			return pids;
		};
		this.updatecurrentHighlightFilter=function(inv){
			var pids=self.getAssocPIDs(inv);
			if(pids.length){
				main.invents.currentHighlightFilter=pids;
				main.invents.currentHighlightFilterLayer=inv;
			}else{main.invents.resetHighlightFilter();}
			self.refreshPoints(inv);
		};
		this.addNewItem=function(inv){
			main.invents.addingFromForm=self;
			if(main.invents && main.invents.inventories[inv]){
				main.invents.inventories[inv].beginAdding();
			}
		};
		this.doneAddingNewItem=function(inv){
			if(main.invents && main.invents.inventories[inv]){
				main.invents.inventories[inv].stopAdding();
			}
		};
		this.stopAddingAll=function(invException){
			for(var key in self.womInvUnits){
				if(!invException || invException!=key){
					self.stopAdding(key);
				}
			}
		};
		this.stopAdding=function(inv){
			if(self.womInvUnits[inv].polygonDrawAdded){
				self.removePolygonDraw(inv);
			}else if(main.invents.womSelectInv){
				main.invents.womSelectInv=null;
				main.invents.womSelectEditForm=null;
			}else if(main.invents.addingFromForm){
				self.doneAddingNewItem(inv);
			}
			self.womInvUnits[inv].addFromMapButton.html.removeClass('none');
			self.womInvUnits[inv].doneAddingFromMapButton.html.addClass('none');
		};
		this.updateSignOffInfo=function(data,val){
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
						pid:self.properties.pid,
						layer:self.inventory.layerName,
						val:val,
						data:data
					},
					action:'updateSignOffData'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.refreshSignOff=function(){
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
						pid:self.properties.pid,
						layer:self.inventory.layerName
					},
					action:'getSignOffData'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							var results=response.results,val,alias,date,who,notes,sortNotes,username,usernameSort;
							var field=self.inventory.fields[self.inventory.properties.internal_field_map.status];
							self.signOffTBody.html('');
							for(var i=0;i<results.length;i++){
								val=results[i].value;
								if(results[i].date || results[i].date==0){date=main.utilities.formatDate(results[i].date)
								}else{date='';}
								username=results[i].username || '';
								if(results[i].username){
									usernameSort=results[i].username.toLowerCase();
								}else{usernameSort='';}
								notes=results[i].notes || '';
								if(notes){sortNotes='t';}else{sortNotes='';}
								if(field.options[val].alias){alias=field.options[val].alias.toLowerCase();}else{alias='';}
								self.signOffTBody.append($('<tr class="efSignOffRow efSignOffRow_'+results[i].pid+'" data-pid="'+results[i].pid+'">')
									.append($('<td class="efSignOffValVal sortableCell_value" data-val="'+val+'" data-sortval="'+alias+'">').html(field.options[val].alias))
									.append($('<td class="efSignOffValDate sortableCell_date" data-sortval="'+results[i].date+'">').html(date))
									.append($('<td class="efSignOffValWho sortableCell_who" data-sortval="'+usernameSort+'">').data('val',who).html(username))
									.append($('<td class="efSignOffValNotes sortableCell_notes" data-sortval="'+sortNotes+'">').html('<textarea class="efSignOffNotesTA">'+notes+'</textarea>'))
								)
							}
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.initWOMContent=function(){
			if(!self.inventory.properties.is_wom){return;}
			if(!self.invAssesTab){
				// self.invAssesLabel=$('<div class="invAssesLabel">').html('Linked Inventories');
				// self.showAllSelfItemsButtonInv=new Button('std','womShowAllSelfItemsButtonInv none','Show All '+self.inventory.properties.plural_alias,null,null,null,'Show All '+self.inventory.properties.plural_alias);
				// self.showAllSelfItemsButtonInv.html.on('click',function(){
					// self.showAllSelf();
				// });
				// self.invAssesButtons=$('<div class="invAssesButtons">')
					// .append(self.showAllSelfItemsButtonInv.html);
				self.invTabContentWrap=$('<div class="invTabContentWrap">');
				self.invAssesContent=$('<div class="invAssesContent">')
					// .append(self.invAssesLabel)
					// .append(self.invAssesButtons)
					.append(self.invTabContentWrap);
				self.invAssesTab=self.tabs.addTab('Link Inventories',self.invAssesContent,'invAssesTab',self.tabChanged,null,null,null,'linkInvTab');
				if(main.invents.inventories.work_orders && main.invents.inventories.work_orders.properties.active && main.invents.inventories.service_requests && main.invents.inventories.service_requests.properties.active){
					if(self.inventory.layerName=='service_requests'){
						self.oppositeWOM='work_orders'
					}else{
						self.oppositeWOM='service_requests';
					}
				}
				// self.womAssesLabel=$('<div class="womAssesLabel">').html('Linked '+main.invents.inventories[self.oppositeWOM].properties.plural_alias);
				// self.showAllOppositeItemsButtonInv=new Button('std','womShowAllSelfItemsButtonInv none','Show All '+self.inventory.properties.plural_alias,null,null,null,'Show All '+self.inventory.properties.plural_alias);
				// self.addOppositeItemsButton=new Button('std','addOppositeItemsButton','Add '+main.tables[self.oppositeWOM].properties.singular_alias);
				// self.doneAddOppositeItemsButton=new Button('std','doneAddOppositeItemsButton none','Done Adding '+main.tables[self.oppositeWOM].properties.plural_alias);
				// self.oppWOMAssesButtons=$('<div class="oppWOMAssesButtons">')
					// .append(self.showAllOppositeItemsButtonInv.html)
					// .append(self.addOppositeItemsButton.html)
					// .append(self.doneAddOppositeItemsButton.html);
				// self.addOppositeItemsButton.html.on('click',function(){
					// self.addNewItem();
				// });
				// self.doneAddOppositeItemsButton.html.on('click',function(){
					// self.doneAddOppWOM();
				// });
				// self.showAllOppositeItemsButtonInv.html.on('click',function(){
					// self.showAllOpp();
					// self.showAllSelf();
				// });
				self.womTabContentWrap=$('<div class="womTabContentWrap">');
				self.womAssesContent=$('<div class="womAssesContent">')
					// .append(self.womAssesLabel)
					// .append(self.oppWOMAssesButtons)
					.append(self.womTabContentWrap);
				if(self.oppositeWOM){
					self.womAssesTab=self.tabs.addTab('Link '+main.invents.inventories[self.oppositeWOM].properties.plural_alias,self.womAssesContent,'womAssesTab',self.tabChanged,null,null,null,'linkWomTab');
				}
				//signoff
				if(self.inventory.properties.internal_field_map && self.inventory.properties.internal_field_map.status){
					self.signOffTBody=$('<tbody class="efSignOffTBody">');
					self.signOffTBody.on('change','.efSignOffNotesTA',function(){
						self.updateSignOffInfo([['notes',$(this).val()]],$(this).closest('.efSignOffRow').find('.efSignOffValVal').data('val'));
					});
					self.signOffTHead=$('<thead>');
					self.signOffTable=$('<table border="1" class="efSignOffTable">')
						.append(self.signOffTHead
							.append($('<tr>')
								.append($('<th class="tableSortTH tableSortTH_value" data-field="value">').html('Status'))
								.append($('<th class="tableSortTH tableSortTH_date" data-field="date">').html('Date'))
								.append($('<th class="tableSortTH tableSortTH_who" data-field="who">').html('Who'))
								.append($('<th class="tableSortTH tableSortTH_notes" data-field="notes">').html('Notes'))
							)
						)
						.append(self.signOffTBody);
					self.signOffTableMod=new Table({
						thead:self.signOffTHead,
						tbody:self.signOffTBody
					});
					self.signOffInfo=$('<div class="efSignOffInfo">').html('This tab tracks all changes in work order status over time by user and date.');
					self.signOffTableWrap=$('<div class="efSignOffTableWrap">').append(self.signOffTable);
					self.signOffWrap=$('<div class="efSignOffWrap">')
						.append(self.signOffInfo)
						.append(self.signOffTableWrap);
					self.womSignOffTab=self.tabs.addTab('WO Status Tracking',self.signOffWrap,'womSignOffTab',self.tabChanged);
					self.refreshSignOff();
				}
				self.tabs.setActiveTab(self.activeTab.tabid);
			}
			self.invTabContentWrap.html('');
			self.womTabContentWrap.html('');
			if(self.inventory.properties.wom_linked_inventories){
				self.womInvUnits={};
				var invs=self.inventory.properties.wom_linked_inventories,inv,wrap;
				
				if(self.oppositeWOM){
					invs[self.oppositeWOM]={
						name:self.oppositeWOM,
						withAdd:true
					}
				}
				for(var key in invs){
					if(main.invents.inventories[key]){
						wrap=self.setWOMContent(invs[key]);
						if(key!=self.oppositeWOM){
							self.invTabContentWrap.append(wrap);
						}else{
							self.womTabContentWrap.append(wrap);
							// self.womInvUnits[key].isWOM=true;
						}
					}
				}
				if(self.linkTo){
					var layers=[];
					var units=self.linkTo.womInvUnits;
					for(var key in units){
						if(self.womInvUnits[key]){layers.push(key);}
					}
					self.linkItemsWOM(self.linkTo.properties.pid,self.linkTo.inventory.layerName,layers);
				}else{
					self.refreshWOM();
				}
			}
		};
		this.linkItemsWOM=function(pid,table,layers){
			// if(main.invents.inventories[table].adding){main.invents.inventories[table].checkEditFormsForButtons(false);}
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
						pid:pid,
						thisPid:self.properties.pid,
						table:table,
						thisTable:self.inventory.layerName,
						layers:layers
					},
					action:'linkItemsWOM'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							self.refreshWOM();
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.getExtraFields=function(key){
			self.extraFields[key]={};
			var inventory=main.invents.inventories[key];
			field_map=inventory.properties.internal_field_map || {};
			if(!inventory.properties.is_wom){
				if(field_map.species_common && inventory.properties.lookup_table_map){
					if(inventory.properties.lookup_table_map.species){
						if(inventory.properties.lookup_table_map.species[field_map.species_common]){
							self.extraFields[key][field_map.species_common]={
								alias:inventory.fields[field_map.species_common].alias,
								lookup_table_map:inventory.properties.lookup_table_map,
								lookup_table:inventory.fields[field_map.species_common].lookup_table,
								options:inventory.fields[field_map.species_common].options,
								input_type:inventory.fields[field_map.species_common].input_type,
								referencer:inventory.fields[field_map.species_common].referencer
							}
						} 
					} 
				}
				var fields=['address_number','street','primary_maintenance'];
				for(var i=0;i<fields.length;i++){
					if(field_map[fields[i]] && inventory.fields[field_map[fields[i]]]){
						self.extraFields[key][field_map[fields[i]]]={
							alias:inventory.fields[field_map[fields[i]]].alias,
							lookup_table_map:inventory.properties.lookup_table_map,
							lookup_table:inventory.fields[field_map[fields[i]]].lookup_table,
							options:inventory.fields[field_map[fields[i]]].options,
							input_type:inventory.fields[field_map[fields[i]]].input_type,
							referencer:inventory.fields[field_map[fields[i]]].referencer
						}
					}
				}
			}else{
				var fields=['date_created','street','primary_maintenance'];
				for(var i=0;i<fields.length;i++){
					if(field_map[fields[i]] && inventory.fields[field_map[fields[i]]]){
						self.extraFields[key][field_map[fields[i]]]={
							alias:inventory.fields[field_map[fields[i]]].alias,
							lookup_table_map:inventory.properties.lookup_table_map,
							lookup_table:inventory.fields[field_map[fields[i]]].lookup_table,
							options:inventory.fields[field_map[fields[i]]].options,
							input_type:inventory.fields[field_map[fields[i]]].input_type,
							referencer:inventory.fields[field_map[fields[i]]].referencer
						}
					}
				}
			}
		};
		this.refreshWOM=function(){
			var invs=self.inventory.properties.wom_linked_inventories,inv,pkg={},features;
			for(var key in invs){
				pkg[key]={
					inv:key,
				}
			}
			if(!Object.keys(pkg).length){return;}
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
						pid:self.properties.pid,
						pkg:pkg,
						extraFields:self.extraFields,
						layer:self.inventory.layerName
					},
					action:'getWomInventoryData'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							self.womUpdateResults(response);
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.addWOMUnitRow=function(result,extraFields){
			var tr=$('<tr class="womUnitTableTR" data-pid="'+result['pid']+'">')
				.append($('<td>').append($('<div class="tdLiner">').html('<input type="checkbox" class="womUnitCheck"/>')))
				.append($('<td>').append($('<div class="tdLiner center">').html('<img src="'+main.mainPath+'images/crosshairs.png" class="womUnitLocInv" title="Zoom To"/>')))
				.append($('<td>').append($('<div class="tdLiner womItemUnitVal sortableCell_pid" data-sortval="'+result['pid']+'" data-val="'+result['pid']+'">').html(result['pid'])))
				// .append($('<td>').append($('<div class="tdLiner womItemUnitVal womItemUnitVal_last_modified" data-sortval="'+result['last_modified']+'" data-val="'+result['last_modified']+'">').html(result['last_modified_alias'])))
				if(extraFields){
					var sortVal;
					for(var key in extraFields){
						sortVal=result[key+'_alias0'];
						if(typeof sortVal=='string'){sortVal=sortVal.toLowerCase();}
						tr.append($('<td>').append($('<div class="tdLiner womItemUnitVal sortableCell_'+key+'" data-sortval="'+sortVal+'" data-val="'+result[key+'_rawval0']+'">').html(result[key+'_alias0'])))
					}
				}
			return tr;
		};
		this.womUpdateResults=function(response){
			var results=response.results,extraFields;
			var extraFields0=response.params.extraFields || null;
			for(var key in results){
				self.womInvUnits[key].tbody.html('');
				extraFields=null;
				if(extraFields0 && extraFields0[key]){extraFields=extraFields0[key];}
				if(results[key].length){
					for(var t=0;t<results[key].length;t++){
						self.womInvUnits[key].tbody.append(self.addWOMUnitRow(results[key][t],extraFields));
					}
				}
				if(main.invents.currentHighlightFilterLayer){
					self.updatecurrentHighlightFilter(key);
				}
				self.allocateNoAssoc(key,true);
			}
			if(main.filter_legend){self.setDefaultButtons(main.filter_legend.layerName);}
		};
		this.refreshTimeTracked=function(){
			if(!self.timeTracked || !self.timeTracked.length){return;}
			if(!self.timeTab){
				self.timeTabLabel=$('<div class="timeTabLabel">').html('Time Comparison Data');
				self.timeTabContentWrap=$('<div class="timeTabContentWrap">');
				self.timeTabContentOWrap=$('<div class="timeTabContentOWrap">')
					.append(self.timeTabLabel)
					.append(self.timeTabContentWrap);
				self.timeTab=self.tabs.addTab('Time',self.timeTabContentOWrap,'time_compare',self.tabChanged);
				if(self.activeTab){self.tabs.setActiveTab(self.activeTab.tabid);}
			}
			self.timeTabContentWrap.html('');
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
						timeTracked:self.timeTracked,
						layerName:self.inventory.layerName
					},
					action:'getTimeCompareSingle'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							self.timeTables={};
							var results=response.results,lookUpTableMap,layer,field_name;
							if(Object.keys(results).length){
								var keys=self.sortByAlias(Object.keys(results));
								for(var y=0;y<keys.length;y++){
									key=keys[y];
									self.timeTables[key]={};
									self.timeTables[key].label=$('<div class="timeCompareIndLabel">').html(self.fields[key].alias);
									self.timeTables[key].thead=$('<thead>')
										.append($('<tr>')
											.append($('<th>').html('Date'))
											.append($('<th>').html('Value')))
									self.timeTables[key].tbody=$('<tbody>');
									self.timeTables[key].table=$('<table border="1" class="timeCompareIndTable">')
										.append(self.timeTables[key].thead)
										.append(self.timeTables[key].tbody);
									self.timeTables[key].tableWrap=$('<div class="timeCompareIndTableTableWrap">')
										.append(self.timeTables[key].table);
									self.timeTables[key].content=$('<div class="timeCompareIndTableWrap">')
										.append(self.timeTables[key].label)
										.append(self.timeTables[key].tableWrap);
									for(var t=0;t<results[key].length;t++){
										if(t==0){
											layer=results[key][t].layer;
											field_name=results[key][t].field_name;
											lookUpTableMap=main.tables[layer].properties.lookup_table_map;
										}
										self.timeTables[key].tbody
											.append($('<tr>')
												.append($('<td>')
													.append($('<div class="tdLiner">').html(main.utilities.formatDate(results[key][t].date)))
												).append($('<td>')
													.append($('<div class="tdLiner">').html(main.utilities.getHTMLFromVal(results[key][t].value,main.tables[layer].fields[field_name],main.tables[layer].properties.lookup_table_map)))
												)
											)
									}
									self.timeTabContentWrap.append(self.timeTables[key].content);
								}
							}
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.setWOMContent=function(item){
			key=item.name;
			inv=main.invents.inventories[key];
			self.womInvUnits[key]={
				wrap:$('<div class="womInvUnit cf" data-key="'+key+'">'),
				is_WOM:false,
				// buttons:$('<div class="womInvUnitButtons">'),
				selectFromMapWrap:$('<div class="womUnitWrapEle womInvSelectFromMapWrap">'),
				label:$('<div class="womInvLabel">').html('Linked '+inv.properties.plural_alias),
				tableWrap:$('<div class="womUnitWrapEle womInvTableWrap">'),
				tableTableWrap:$('<div class="womTableWrap">'),
				noAssoc:$('<div class="womUnitNoAssocInv">').html('No Linked '+inv.properties.plural_alias),
				tableContentWrap:$('<div class="tableContentWrap">'),
				tableContentTableWrap:$('<div class="tableContentTableWrap">'),
				tableButtons:$('<div class="womInvUnitTableButtons">'),
				table:$('<table border="1" class="womInvTable">'),
				tbody:$('<tbody>'),
				thead:$('<thead>'),
				checkAll:$('<input type="checkbox" class="womInvCheckAll">'),
				removeButton:new Button('std','womRemoveAssocInvButton','Unlink',null,null,null,'Unlink Checked '+inv.properties.plural_alias),
				showAssocItemsButton:new Button('std','womShowAssocItemsButton none','Highlight',null,null,null,'Show All Linked '+inv.properties.plural_alias),
				showAllItemsButton:new Button('std','womShowAllItemsButton none','Show All'/* +inv.properties.plural_alias */,null,null,null,'Show All '+inv.properties.plural_alias),
				addFromMapWrap:$('<div class="womAddFromMapWrap womUnitAddWrap">'),
				addFromMapLabel:$('<div class="addFromMapLabel womAddingLabel">').html('Add'),
				addFromMapButton:new Button('std','womAddFromMapButton womUnitAddButton','Add'),
				doneAddingFromMapButton:new Button('std','womDoneAddingFromMapButton womUnitAddButton none','Done'),
				addFromMapButtonsWrap:$('<div class="womAddFromMapButtonsWrap">'),
				invs:item,
				addFromIDWrap:$('<div class="womAddFromIDWrap womUnitAddWrap none">'),
				addFromIDLabel:$('<div class="womAddFromIDLabel womAddingLabel">').html('Add from ID'),
				addFromIDButton:new Button('std','womAddFromIDButton womUnitAddButton','Add'),
				addButtonsWrap:$('<div class="womAddButtonsWrap">'),
				addFromIDInput:$('<input type="text" class="womAddFromIDInput">'),
				selectionMethodSelect:$('<select class="womInvUnitSelection"><option value="point" selected="selected">Point</option><option value="polygon">Polygon</option><option value="id">ID</option></select>'),
				selectionMethod:'point',
				polygonDrawAdded:false
			}
			self.womInvUnits[key].tbody.on('click','.womUnitLocInv',function(){
				var inv=$(this).closest('.womInvUnit').data('key');
				var pid=$(this).closest('.womUnitTableTR').data('pid');
				main.mapItems.unSetActives();
				if(main.invents && main.invents.inventories[inv]){
					main.mapItems.hasActive=Number(pid);
					main.mapItems.hasActiveLayer=inv;
				}
				self.showAllAssocItems(inv);
				var table=main.tables[inv];
				if(table.properties.internal_field_map && table.properties.internal_field_map.geometry_field){
					main.utilities.getGeo(inv,table.properties.internal_field_map.geometry_field,pid,self.geoFound);
				}
			});
			self.womInvUnits[key].showAssocItemsButton.html.on('click',function(){
				self.showAllAssocItems($(this).closest('.womInvUnit').data('key'));
			});
			self.womInvUnits[key].showAllItemsButton.html.on('click',function(){
				self.showAllItems($(this).closest('.womInvUnit').data('key'));
			});
			self.womInvUnits[key].removeButton.html.on('click',function(){
				self.removeAssItems($(this).closest('.womInvUnit').data('key'));
			});
			self.womInvUnits[key].tableButtons
				.append(self.womInvUnits[key].removeButton.html)
				.append(self.womInvUnits[key].showAssocItemsButton.html)
				.append(self.womInvUnits[key].showAllItemsButton.html);
			if(self.inventory.layerName=='work_orders'){
				if(main.withs.with_wom_mass_updater && main.withs.with_wom_mass_updater.value && main.withs.with_wom_mass_updater[main.account.loggedInUser.user_type+'_with']){
					self.womInvUnits[key].massUpdateButton=new Button('std','womMassUpdateButton none','Mass Update');
					self.womInvUnits[key].massUpdateButton.html.on('click',function(){
						self.updateAssItems($(this).closest('.womInvUnit').data('key'));
					});
					self.womInvUnits[key].removeButton.html.after(self.womInvUnits[key].massUpdateButton.html);
				}
			}
			self.womInvUnits[key].selectionMethodSelect.on('change',function(){
				var inv=$(this).closest('.womInvUnit').data('key');
				if($(this).val()!='id'){
					self.womInvUnits[inv].addFromIDWrap.addClass('none');
					self.womInvUnits[inv].addFromMapButton.html.removeClass('none');
					self.womInvUnits[inv].selectionMethod=$(this).val();
					self.stopAddingAll();
				}else{
					self.womInvUnits[inv].addFromIDWrap.removeClass('none');
					self.womInvUnits[inv].addFromMapButton.html.addClass('none');
				}
			});
			self.womInvUnits[key].addFromIDButton.html.on('click',function(){
				var inv=$(this).closest('.womInvUnit').data('key');
				self.stopAddingAll();
				if(self.womInvUnits[inv].addFromIDInput.val().trim()){
					self.addInvItems([self.womInvUnits[inv].addFromIDInput.val().trim()],inv);
				}else{
					alert('Please enter a value.');
				}
			});
			self.womInvUnits[key].addFromMapButton.html.on('click',function(){
				var inv=$(this).closest('.womInvUnit').data('key');
				self.stopAddingAll();
				self.startSelectFromMap(inv);
			});
			self.womInvUnits[key].doneAddingFromMapButton.html.on('click',function(){
				self.stopAdding($(this).closest('.womInvUnit').data('key'));
			});
			self.womInvUnits[key].addFromMapWrap
				.append(self.womInvUnits[key].addFromMapLabel)
				.append($('<table>')
					.append($('<tbody>')
						.append($('<tr>')
							.append($('<td>')
								.append($('<div class="tdLiner">').html('Method'))
							)
							.append($('<td>')
								.append($('<div class="tdLiner">').html(self.womInvUnits[key].selectionMethodSelect))
							)
						)
					)
				)
				.append(self.womInvUnits[key].addFromMapButtonsWrap
					.append(self.womInvUnits[key].addFromMapButton.html)
					.append(self.womInvUnits[key].doneAddingFromMapButton.html)
				)
			self.womInvUnits[key].addFromIDWrap
				.append(self.womInvUnits[key].addFromIDLabel)
				.append($('<table>')
					.append($('<tbody>')
						.append($('<tr>')
							.append($('<td colspan="2">')
								.append($('<div class="tdLiner">').append((self.womInvUnits[key].addFromIDInput)))
							)
						)
					)
				)
				.append(self.womInvUnits[key].addButtonsWrap
					.append(self.womInvUnits[key].addFromIDButton.html)
				)
			var withAddRow='';
			if(item.withAdd){
				extraText='';
				if(self.oppositeWOM){
					if(self.oppositeWOM=='work_orders'){
						extraText=' WO';
					}else if(self.oppositeWOM=='service_requests'){
						extraText=' SR';
					}
				}
				self.womInvUnits[key].addNewWrap=$('<div class="womAddNewWrap womUnitAddWrap">');
				self.womInvUnits[key].addNewLabel=$('<div class="addNewLabel womAddingLabel">').html('Create New'+extraText);
				self.womInvUnits[key].addNewButtonsWrap=$('<div class="womAddNewButtonsWrap">');
				self.womInvUnits[key].addButton=new Button('std','womAddItemsButton womUnitAddButton','Add');
				self.womInvUnits[key].doneAddButton=new Button('std','womDoneAddItemsButton womUnitAddButton none','Done');
				self.womInvUnits[key].addButton.html.on('click',function(){
					self.addNewItem($(this).closest('.womInvUnit').data('key'));
				});
				self.womInvUnits[key].doneAddButton.html.on('click',function(){
					self.doneAddingNewItem($(this).closest('.womInvUnit').data('key'));
				});
				self.womInvUnits[key].addNewWrap
					.append(self.womInvUnits[key].addNewLabel)
					.append(self.womInvUnits[key].addNewButtonsWrap
						.append(self.womInvUnits[key].addButton.html)
						.append(self.womInvUnits[key].doneAddButton.html)
					)
				withAddRow=$('<tr>')
					.append($('<td colspan="2">')
						.append($('<div class="tdLiner">').html(self.womInvUnits[key].addNewWrap))
					);
			}
			self.womInvUnits[key].selectFromMapWrap
				.append($('<table class="womSelectFromMapTable">')
					.append($('<tbody>')
						.append($('<tr>')
							.append($('<td colspan="2">')
								.append($('<div class="tdLiner">').append(self.womInvUnits[key].addFromMapWrap))
							)
						)
						.append($('<tr>')
							.append($('<td colspan="2">')
								.append($('<div class="tdLiner">').html(self.womInvUnits[key].addFromIDWrap))
							)
						)
						.append(withAddRow)
					)
				);
					
			self.womInvUnits[key].checkAll.on('change',function(){
				$(this).closest('.womInvTable').find('.womUnitCheck').prop('checked',$(this).prop('checked'));
			});
			self.getExtraFields(key);
			var tr=$('<tr>')
				.append($('<th>').append(self.womInvUnits[key].checkAll))
				.append($('<th>').html('Zoom'))
				.append($('<th class="womTableTHSortable tableSortTH womTableTHSortable_pid" data-field="pid">').html('ID'))
				// .append($('<th class="womTableTHSortable womTableTHSortable_last_modified" data-field="last_modified">').html('Last Modified'));
			if(self.extraFields && self.extraFields[key]){
				for(var key2 in self.extraFields[key]){
					 tr.append($('<th class="womTableTHSortable tableSortTH tableSortTH_'+key2+'" data-field="'+key2+'">').html(self.extraFields[key][key2].alias))
				}
			}
			// self.womInvUnits[key].wrap.find('.womUnitCheck','change',function(){
				// self.setWomInvUnitButtons();
			// });
			if(self.oppositeWOM!=key){self.womInvUnits[key].wrap.append(self.womInvUnits[key].label);}
			self.womInvUnits[key].wrap
				// .append(self.womInvUnits[key].buttons)
				.append(self.womInvUnits[key].tableWrap
					.append(self.womInvUnits[key].tableContentWrap
						.append(self.womInvUnits[key].tableButtons)
						.append(self.womInvUnits[key].tableContentTableWrap
							.append(self.womInvUnits[key].tableTableWrap
								.append(self.womInvUnits[key].table
									.append(self.womInvUnits[key].thead
										.append(tr)
									)
									.append(self.womInvUnits[key].tbody)
								)
							)
						)
					)
				)
			if(!item.noAdd){self.womInvUnits[key].wrap.append(self.womInvUnits[key].selectFromMapWrap);
			}else{self.womInvUnits[key].tableWrap.addClass('efWOMNoAddTableWrap')}
			self.womInvUnits[key].tableMod=new Table({
				thead:self.womInvUnits[key].thead,
				tbody:self.womInvUnits[key].tbody
			});
			return self.womInvUnits[key].wrap;
		};
		this.initInvWOMContent=function(){
			if(!self.invWOMTab){
				self.invWOMLabel=$('<div class="invWOMLabel">').html('Linked WOM');
				// self.showAllSelfItemsButtonInv=new Button('std','showAllSelfItemsButtonInv none','Show All '+self.inventory.properties.plural_alias,null,null,null,'Show All '+self.inventory.properties.plural_alias);
				// self.invWOMButtons=$('<div class="invWOMButtons">')
					// .append(self.showAllSelfItemsButtonInv.html);
				// self.showAllSelfItemsButtonInv.html.on('click',function(){
					// self.showAllSelf();
				// });
				self.invWOMTabContentWrap=$('<div class="invWOMTabContentWrap">');
				self.invWOMContent=$('<div class="invWOMContent">')
					.append(self.invWOMLabel)
					// .append(self.invWOMButtons)
					.append(self.invWOMTabContentWrap);
				self.invWOMTab=self.tabs.addTab('WOM',self.invWOMContent,'invWOMTab',self.tabChanged,null,null,null,'womTab');
				self.tabs.setActiveTab(self.activeTab.tabid);
			}
			self.invWOMTabContentWrap.html('');
			// if(self.inventory.properties.wom_linked_inventories){
				self.womInvUnits={};
				var invs={},inv;
				invs['service_requests']={
					name:'service_requests',
					withAdd:true,
					noAdd:false
				}
				invs['work_orders']={
					name:'work_orders',
					withAdd:true,
					noAdd:false
				}
				for(var key in invs){
					if(main.invents.inventories[key]){
						self.invWOMTabContentWrap.append(self.setWOMContent(invs[key]));
					}
				}
				self.updateInvForWOM();
			// }
		};
		this.updateInvForWOM=function(){
			var womItems=[];
			for(var key in main.inventories){
				if(main.inventories[key] && main.inventories[key].properties.is_wom){
					womItems.push(key);
				}
			}
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
						pid:self.properties.pid,
						womItems:womItems,
						extraFields:self.extraFields,
						layer:self.inventory.layerName
					},
					action:'getInvWOMData'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							self.womUpdateResults(response);
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.newTab=function(args){
			// self.printPanel.positionPanel();
		};
		this.resetPrintOptions=function(){
			self.printComments.val('');
			if(self.completeByInput){self.completeByInput.html.val('');}
			self.printMapCheck.prop('checked',false);
			self.printPhotosCheck.prop('checked',false);
			self.printFieldOptionsTBody.find('.efPrintActiveField').prop('checked',true);
		};
		this.boundMapForPrint=function(){
			var printLayers=[];
			self.printMapLayerSelectWrap.find('.efPrintMapLayerCheck:checked').each(function(){
				printLayers.push({
					layer:$(this).val(),
					pids:self.getAssocPIDs($(this).val()).join(',')
				});
			});
			if(!printLayers.length){
				alert('Select map layers to bound.');
				return;
			}else if(self.inventory.properties.internal_field_map.geometry_field){
				main.map.boundMap(printLayers,self.inventory.properties.internal_field_map.geometry_field);
			}else{
				alert('Error: No geometry field. Please contact Plan-It GEO.');
			}
		};
		this.womItemsResize=function(){
			if(self.womInvUnits){
				var height;
				for(var key in self.womInvUnits){
					if(self.womInvUnits[key].tableContentTableWrap && self.womInvUnits[key].tableWrap.is(':visible')){
						var womInvUnit=self.womInvUnits[key];
						height=self.panel.html.height()+parseInt(self.panel.html.css('padding-top'))-womInvUnit.tableContentTableWrap.position().top-parseInt(womInvUnit.tableWrap.css('padding-bottom'))-parseInt(womInvUnit.wrap.css('padding-bottom'))-parseInt(womInvUnit.wrap.closest('.tabContent').css('padding-bottom'))-parseInt(womInvUnit.wrap.closest('.tabsContentWrap').css('border-bottom-width'));
						self.womInvUnits[key].tableContentTableWrap.css('height',height);
					}
				}
				if(height){
					for(var key in self.womInvUnits){
						if(self.womInvUnits[key].tableContentTableWrap){
							self.womInvUnits[key].height=height;
						}
					}
				}
			}
		};
		this.beginPrint=function(){
			if(!self.printPanel){
				self.printComments=$('<textarea cols="40" rows="6" class="efPrintComments">');
				self.printPhotosCheck=$('<input type="checkbox" class="eFPrintPhotosCheck">');
				self.printMapCheck=$('<input type="checkbox" class="eFPrintMapCheck">');
				self.printMapCheck.on('change',function(){
					if($(this).prop('checked')){self.printMapOptionsTab.tab.removeClass('none');
					}else{self.printMapOptionsTab.tab.addClass('none');}
				});
				if(self.inventory.layerName=='work_orders'){
					self.completeByInput=new DateMod({
						name:'complete_by',
						value:self.properties['complete_by']
					});
					var complete_by_tr=$('<tr>')
						.append($('<td>').append($('<div class="tdLiner">').html('Complete By')))
						.append($('<td>').append($('<div class="tdLiner">').append(self.completeByInput.html)))
				}else{
					var complete_by_tr='';
				}
				self.printOptionsTBody=$('<tbody class="efPrintOptionsTBody">')
					.append($('<tr>')
						.append($('<td>').append($('<div class="tdLiner">').html('Comments')))
						.append($('<td>').append($('<div class="tdLiner">').html(self.printComments)))
					)
					.append(complete_by_tr
					)
					.append($('<tr>')
						.append($('<td>').append($('<div class="tdLiner">').html('Print Map')))
						.append($('<td>').append($('<div class="tdLiner">').html(self.printMapCheck)))
					)
					.append($('<tr>')
						.append($('<td>').append($('<div class="tdLiner">').html('Print Photos')))
						.append($('<td>').append($('<div class="tdLiner">').html(self.printPhotosCheck)))
					)
				self.printOptionsTable=$('<table border="1" class="efPrintOptionsTable">').append(self.printOptionsTBody);
				self.printOptionsWrap=$('<div class="efPrintOptionsWrap">').append(self.printOptionsTable);
				
				self.printMapLayerSelectWrap=$('<div>').addClass('checkboxesWrap checksableWrap');
				var invs=self.inventory.properties.wom_linked_inventories,checked;
				// invs[self.oppositeWOM]
				for(var key in invs){
					if(main.invents.inventories[key]){
					checked=false;
						if(!main.invents.inventories[key].properties.is_wom){checked=true;}
						self.printMapLayerSelectWrap.append(new Checkbox({
							label:main.invents.inventories[key].properties.plural_alias,
							value:key,
							checked:checked,
							name:'efPrintMapLayerCheck',
							classes:'checkboxInput formInput uiInput toolBoxInput efPrintMapLayerCheck'
						}).html);
					}
				}
				self.printOptionsLabelsWrap=$('<div class="efPrintOptionsLabelsWrap efOptionsSection">')
					.append($('<div class="efPrintOptionsLabel optionsLabel">').html('Labels'));
				self.printBoundMapButton=new Button('std','eFPrintBoundMapButton','Bound');
				self.printBoundMapButton.html.on('click',function(){
					self.boundMapForPrint();
				});
				self.printOptionsMapTBody=$('<tbody class="efPrintOptionsMapTBody">')
					.append($('<tr>')
						.append($('<th colspan="2">').append($('<div class="tdLiner center">').html('Bound Options')))
					)
					.append($('<tr>')
						.append($('<td>').append($('<div class="tdLiner">').html('Layers to Bound')))
						.append($('<td>').append($('<div class="tdLiner">').html(self.printMapLayerSelectWrap)))
					)
					.append($('<tr>')
						.append($('<td colspan="2">').append($('<div class="tdLiner center">').html(self.printBoundMapButton.html)))
					)
				self.printOptionsMapTable=$('<table border="1" class="efPrintOptionsTable">').append(self.printOptionsMapTBody);
				self.printOptionsMapWrap=$('<div class="efPrintOptionsMapWrap efOptionsSection">').append(self.printOptionsMapTable);
				self.printMapOptionsWrap=$('<div class="efPrintMapOptionsWrap">')
					.append(self.printOptionsMapWrap);
				
				self.printFieldOptionsTBody=$('<tbody class="efPrintOptionsTBody">');
				var keys=main.utilities.sortFields(self.fields,'abc');
				var field;
				for(var i=0;i<keys.length;i++){
					field=self.fields[keys[i]];
					if(field.active && field.input_type!='hidden' && field.input_type!='file' && field.input_type!='html'){
						self.printFieldOptionsTBody.append($('<tr>')
							.append($('<td>').append($('<div class="tdLiner">').html('<input type="checkbox" checked="checked" data-fieldname="'+keys[i]+'" class="efPrintActiveField efPrintActiveField_'+keys[i]+'"/>')))
							.append($('<td>').append($('<div class="tdLiner">').text(field.alias)))
						)
					}
				}
				self.printFieldOptionsTable=$('<table border="1" class="efPrintOptionsTable">').append(self.printFieldOptionsTBody);
				self.printFieldOptionsWrap=$('<div class="efPrintFieldOptionsWrap">').append(self.printFieldOptionsTable);
				self.printTabs=new Tabs();
				var activeTab=self.printTabs.addTab('Options',self.printOptionsWrap,null,self.newTab);
				self.printTabs.addTab('Fields',self.printFieldOptionsWrap,null,self.newTab);
				self.printMapOptionsTab=self.printTabs.addTab('Map Options',self.printMapOptionsWrap,null,self.newTab,null,null,null,'none');
				self.printTabs.setActiveTab(activeTab.tabid);
				self.printOptionsReset=new Button('std','eFPrintOptionsReset','Reset');
				self.printOptionsReset.html.on('click',function(){
					self.resetPrintOptions();
				});
				self.printOptionsSubmit=new Button('std','eFPrintOptionsSubmit','Print');
				self.printOptionsSubmit.html.on('click',function(){
					self.printPanel.close();
					var activeFields=[],field;
					self.printFieldOptionsTBody.find('.efPrintActiveField').each(function(){
						if($(this).prop('checked')){
							field=self.fields[$(this).data('fieldname')];
							activeFields.push({
								name:field.name,
								alias:field.alias
							});
						}
					});
					var printMap=null;
					var printPhotos=null;
					if(self.printMapCheck.prop('checked')){
						printMap=true;
					}
					if(self.printPhotosCheck.prop('checked')){printPhotos=true;}
					var params={
						layerName:self.inventory.layerName,
						pid:self.properties.pid,
						activeFields:activeFields,
						completeByInput:(self.completeByInput)?self.completeByInput.html.val():'',
						printComments:self.printComments.val(),
						printMap:printMap,
						printPhotos:printPhotos,
					}
					main.wom.printItem(params);
				});
				self.printTopButtons=$('<div class="efPrintTopButtons">')
					.append(self.printOptionsReset.html);
				self.printBottomButtons=$('<div class="efPrintBottomButtons">')
					.append(self.printOptionsSubmit.html);
				self.printContent=$('<div class="efPrintWrap">')
					.append(self.printTopButtons)
					.append(self.printTabs.html)
					.append(self.printBottomButtons);
				self.printPanel=new Panel({
					content:self.printContent,
					title:'Print Options',
					matchHeight:true,
					relativeTo:main.layout.mapContainerWrap,
					classes:'editFormPrintPanel',
					centerOnOrientationChange:true,
					scrollToTop:true,
					onClose:self.printFormClosed,
					onShow:self.onPrintPanelOpen,
					positionTop:30
				});
			}
			self.printPanel.open();
		};
		this.onPrintPanelOpen=function(){
			if(main.labels && main.labels.labelForm){
				self.printOptionsLabelsWrap.append(main.labels.labelForm);
				self.printMapOptionsWrap.append(self.printOptionsLabelsWrap);
				main.labels.mainLabelLayerSelect.val(self.inventory.layerName).change();
			}
		};
		this.printFormClosed=function(){
			if(main.labels && main.labels.labelForm){
				self.printOptionsLabelsWrap.detach();
				main.labels.shelf.find('.shelfContentWrap').append(main.labels.labelForm);
			}
		};
		this.makePreviewFieldUnit=function(val,fieldName){
			var field=self.inventory.fields[fieldName];
			var lookup_table_map=null;
			if(self.inventory.properties.lookup_table_map){lookup_table_map=self.inventory.properties.lookup_table_map;}
			return $('<div class="efPreviewFieldUnit cf efPreviewFieldUnit_'+fieldName+'" data-fieldname="'+fieldName+'">')
				.append($('<div class="efPreviewFieldUnitEle efPreviewFieldUnitLabel">').html(field.alias))
				.append($('<div class="efPreviewFieldUnitEle efPreviewFieldUnitVal">').html(main.utilities.getHTMLFromVal(val,field,lookup_table_map)));
		};
		this.updateFieldPreviews=function(properties){
			var lookup_table_map=null,field;
			if(self.inventory.properties.lookup_table_map){lookup_table_map=self.inventory.properties.lookup_table_map;}
			for(var c=0;c<self.previewFields.length;c++){
				field=self.inventory.fields[self.previewFields[c]];
				self.editFormFieldsPreviewContent.find('.efPreviewFieldUnit_'+self.previewFields[c]+' .efPreviewFieldUnitVal').html(main.utilities.getHTMLFromVal(properties[self.previewFields[c]],field,lookup_table_map));
			}
		};
		this.dataUpdated=function(args,response){
			if(response && response.fields){
				for(var o=0;o<response.fields.length;o++){
					if(self.inventory.fields[response.fields[o]].on_data_save_cb){
						main.utilities.callFunctionFromName(self.inventory.fields[response.fields[o]].on_data_save_cb,window,{pid:self.properties.pid});
					}
				}
			}
		};
		(this.createHTML=function(){
			self.html=$('<div class="editForm">');
			var pid=self.properties.pid;
			self.editFormContentWrap=$('<div class="editFormContentWrap dataUnit">');
			self.editFormTopButtons=$('<div class="editFormTopButtons">')
			if(self.inventory.is_inventory){
				self.closeButton=new Button('std','eFCloseButton','Save & Close');
				self.closeButton.html.on('click',function(){
					self.panel.close();
				});
				self.editFormTopButtons.append(self.closeButton.html);
			}
			self.editFormTop=$('<div class="editFormTop">')
				.append(self.editFormTopButtons);
			if(self.inventory.properties.preview_fields){
				// self.editFormFieldsPreviewLabel=$('<div class="editFormFieldsPreviewLabel">').html('Field Previews');
				self.editFormFieldsPreviewContent=$('<div class="editFormFieldsPreviewContent">');
				self.editFormFieldsPreview=$('<div class="editFormFieldsPreview">')
					// .append(self.editFormFieldsPreviewLabel)
					.append(self.editFormFieldsPreviewContent);
				self.editFormTop.append(self.editFormFieldsPreview);
				self.previewFields=self.inventory.properties.preview_fields;
				if(self.previewFields.length){
					for(var c=0;c<self.previewFields.length;c++){
						self.editFormFieldsPreviewContent.append(self.makePreviewFieldUnit(self.inventory.map_layer.features[pid].fields[self.previewFields[c]].current_val,self.previewFields[c]));
					}
				}
			}
			if(self.inventory.is_inventory){
				// if(!self.inventory.properties.is_wom){
				self.deleteButton=new Button('std','deleteItem','Delete',[['pid',pid]]);
				self.deleteButton.html.on('click',function(){
					if(confirm("Are you sure you want to delete this?")){
						main.utilities.deleteData([$(this).data('pid')],self.inventory.layerName);
					}
				});
				self.editFormTopButtons.prepend(self.deleteButton.html);
				if(self.inventory.properties.is_wom){
					self.printButton=new Button('std','printItem','Print');
					self.printButton.html.on('click',function(){
						self.beginPrint();
					});
					self.editFormTopButtons.append(self.printButton.html);
				}
			}
			if(self.inventory.lastFocused && !self.inventory.properties.is_wom/*  && self.inventory.properties.omit_load_last_fields *//*  && self.isNew */){
				self.loadLastButton=new Button('std','loadLastButton','Load Last');
				self.loadLastButton.html.attr('title','Click to Load Last Focused Feature');
				self.editFormTopButtons.append(self.loadLastButton.html);
				self.loadLastButton.html.on('click',function(){
					if(confirm('Warning, this will overwrite current data. Do you want to continue?')){
						self.loadLast(self.inventory.lastFocused);
					}
				});
			}
			if(self.inventory.properties.photo_fields){
				var photoFields=self.inventory.properties.photo_fields.split(',');
				for(var u=0;u<photoFields.length;u++){
					if(self.inventory.fields[photoFields[u]].sub_data_type=='photo'){
						self.showImages=main.utilities.getPhotosPkg(pid,photoFields[u],self.inventory.layerName);
					}else{
						main.utilities.getPhotosPkg(pid,photoFields[u],self.inventory.layerName);
					}
				}
				self.viewPhotosButton=self.showImages.viewPhotosButton;
				self.viewPhotosButton.html.on('click',main.utilities.viewPhotosClicked);
				self.editFormTopButtons.append(self.showImages.html);
			}
			self.editFormContentWrap.append(self.editFormTop);
			var fields=self.inventory.fields,field,value,key;
			self.fields=self.inventory.fields;
			self.inventory.map_layer.features[pid].fields={};
			self.inventory.map_layer.features[pid].editForm=self;
			self.inventory.map_layer.features[pid].editLargeTable={};
			self.inventory.map_layer.features[pid].editLargeTable.tbody=$('<tbody>');
			if(!self.sortedFieldKeys){self.sortedFieldKeys=main.utilities.sortFields(self.fields,'place_in_order');}
			self.timeTracked=[];
			for(var i=0;i<self.sortedFieldKeys.length;i++){
				key=self.sortedFieldKeys[i];
				field=self.fields[key];
				if(field.active){
					if(field.time_tracked){
						self.timeTracked.push(field.name);
					}
					value=self.properties[key];
					if(field.data_type=="geometry"){value=self.inventory.map_layer.features[pid].properties[key];}
					self.inventory.map_layer.features[pid].fields[key]={
						current_val:value,
						html:value,
						field:field
					};
					if(field.input_type!='html'){
						self.fieldInputs[key]=main.utilities.getRowThings(self.inventory.map_layer.features[pid],field,pid,{params:{pid:pid,fieldName:key,value:value}},{callback:self.newSignature},null,true,self.inventory.layerName,null,null,true,null,'abc');
					}
					if(field.in_editform && field.input_type!='hidden'){
						if(field.input_type!='html'){
							self.inventory.map_layer.features[pid].editLargeTable.tbody.append($('<tr class="popUpRow '+key+'" data-field="'+key+'">')
								.append($('<td>')
									.append($('<div class="tdLiner popupRowLabel">').text(field.alias))
								)
								.append($('<td class="outerEditingW notEditing">')
									.append($('<div class="tdLiner">')
										.append(
											$('<div data-field="'+key+'">').addClass('tdLiner popUpEditPkg '+key)
												.append($('<div>').addClass('dataTableCurrent '+key).data('field',key).html(self.inventory.map_layer.features[pid].fields[key].html))
												.append(self.inventory.map_layer.features[pid].fields[key].largeEditPackage)
										)
									)
								)
							)
						}else{
							self.inventory.map_layer.features[pid].editLargeTable.tbody.append($('<tr class="htmlRow popUpRow '+key+'" data-field="'+key+'">')
								.append($('<td colspan="2" class="htmlTD">')
									.append($('<div class="tdLiner">')
										.append(field.html)
									)
								)
							)
						}
					}
					if(field.input_type=='file'){
						main.tables[self.inventory.layerName].atchs.items[pid].fields[field.name].etCurrentPhotos.setRemoveCallBack(self.inventory.photoRemoved);
						main.tables[self.inventory.layerName].atchs.items[pid].fields[field.name].etCurrentPhotos.setOnClick(main.utilities.photoELClicked);
					}
				}
			}
			self.itemForm=$('<form class="itemForm dataUnit" enctype="multipart/form-data">');
			self.inventory.map_layer.features[pid].editLargeTable.table=$('<table class="popupTable dataUnit" border="1" data-pid="'+pid+'">')
				.append($('<thead>')
					.append($('<tr>')
						.append($('<th colspan="2">')
							.append($('<div class="tdLiner">').text(self.inventory.singular_alias))
						)
					)
				)
				.append(self.inventory.map_layer.features[pid].editLargeTable.tbody);
			if(!self.editable){main.utilities.tableToUneditable(self.inventory.map_layer.features[pid].editLargeTable.table)}
			if(self.inventory.properties[main.account.loggedInUser.user_type+'_editable'] && self.inventory.is_inventory){
				self.convertToEditing(pid);
			}else{
				self.removeEditing(pid);
			}
			// self.label=$('<div class="editFormLabel">').text(self.alias);
			// self.content=$('<div class="editFormContent">');
			// self.html.append(self.content.append(self.inventory.inventory.map_layer.features[pid].editLargeTable.table));
			if(self.inventory.properties.tabs){
				self.tabs=new Tabs({classes:'editFormTabs',data:[['pid',pid]]});
				self.dbTabs=self.inventory.properties.tabs;
				var dbTab,tab,i=0,table,tabFields,keys,primaryTable;
				self.activeTab=null;
				for(var key in self.dbTabs){
					dbTab=self.dbTabs[key];
					tbody=$('<tbody>');
					table=$('<table border="1" class="editFormTabTable">').append(tbody);
					if(self.inventory.properties.primary_tab==key){
						primaryTable=table;
					}
					tabFields=dbTab.fields;
					keys=main.utilities.sortTabFields(tabFields,self.fields);
					for(var t=0;t<keys.length;t++){
						self.inventory.map_layer.features[pid].editLargeTable.table.find('.popUpRow.'+keys[t]).appendTo(tbody);
					}
					tab=self.tabs.addTab(dbTab.alias,$('<div class="editFormTab">').append($('<div class="editFormContent">').append(table)),key,self.tabChanged);
					if(i==0){self.activeTab=tab;i++;}
				}
				if(self.inventory.properties.primary_tab){
					self.inventory.map_layer.features[pid].editLargeTable.table.find('.popUpRow').appendTo(primaryTable);
				}
				self.inventory.map_layer.features[pid].editLargeTable.tabs=self.tabs;
				self.inventory.map_layer.features[pid].editLargeTable.formWrap=self.tabs.html;
				// var activeTab=self.tabs.addTab('Tree',$('<div class="editFormTab">')
					// .append(self.html)
				// );
				// self.tabs.addTab('Site',$('<div class="editFormTab">'));
				self.tabs.setActiveTab(self.activeTab.tabid);
				self.panelContent=self.itemForm.append(self.tabs.html);
			}else{
				var ct=$('<div class="editFormContent">').append(self.inventory.map_layer.features[pid].editLargeTable.table);
				if(self.timeTracked.length || self.inventory.properties.is_wom || (main.withs.with_wom && main.withs.with_wom.value)){
					self.tabs=new Tabs({classes:'editFormTabs',data:[['pid',pid]]});
					self.normTab=self.tabs.addTab('Edit Details',ct,'invAssNorm',self.tabChanged);
					self.activeTab=self.normTab;
					self.inventory.map_layer.features[pid].editLargeTable.tabs=self.tabs;
					self.inventory.map_layer.features[pid].editLargeTable.formWrap=self.tabs.html;
					self.panelContent=self.itemForm.append(self.tabs.html);
				}else if(!self.inventory.properties.is_wom){
					self.panelContent=self.itemForm.append(ct);
					self.inventory.map_layer.features[pid].editLargeTable.formWrap=ct;
				}
			}
			if(main.withs.with_wom && main.withs.with_wom.value && !self.inventory.properties.is_wom){
				self.initInvWOMContent();
			}
			if(self.inventory.properties.is_wom){
				self.initWOMContent();
			}
			if(self.timeTracked.length){
				self.refreshTimeTracked();
			}
			if(self.inventory.properties.special_connections){
				var sc=self.inventory.properties.special_connections;
				for(var key in sc){
					if(sc[key].type=='disable'){
						self.inventory.map_layer.features[pid].fields[sc[key].to_effect].largeEditPackage.find('.formInput').prop('disabled',!self.inventory.map_layer.features[pid].fields[key].largeEditPackage.find('.formInput').prop('checked'));
					}
					if(sc[key].type=='multi_dbh_calc'){
						var thisSC=sc[key];
						self.multiDBHCalc=new MultiDBHCalc({
							stem_fields:thisSC.stem_fields,
							dbh_exact:thisSC.dbh_exact,
							stem_field:thisSC.stem_field,
							dataUnit:self.itemForm,
							context:self
						});
					}
				}
			}
			// self.inventory.inventory.map_layer.features[pid].editLargeTable.formWrap.on('change','.formInput',function(e){
				// var field=$(this).closest('.editsWrap').data('field');
				// if(self.dataUpdateTO){clearTimeout(self.dataUpdateTO);}
				// if(self.dataLastChangedThis && field!=self.dataLastChangedField){
					// self.inventory.inputChanged(self.dataLastChangedThis,self.dataLastChangedE);
				// }
				// self.dataLastChangedField=field;
				// self.dataLastChangedThis=$(this);
				// self.dataLastChangedE=e;
				// self.dataUpdateTO=setTimeout(function(){
					// self.dataUpdateTO=null;
					// self.inventory.inputChanged(self.dataLastChangedThis,self.dataLastChangedE);
					// /* clearTimeout(self.dataUpdateTO); */
					// self.dataLastChangedField=null;
					// self.dataLastChangedThis=null;
					// self.dataLastChangedE=null;
				// },5000);
			// });
			self.inventory.map_layer.features[pid].editLargeTable.formWrap.on('change','.formInput',function(e){
				self.inventory.inputChanged($(this),e,self.dataUpdated);
			});
			var extraClasses='';
			if(self.inventory.properties.is_wom){extraClasses=' womPanel';
			}else if(main.withs.with_wom && main.withs.with_wom.value && !self.inventory.properties.is_wom){extraClasses=' womInvPanel';}
			self.panel=new Panel({
				content:self.html.append(self.editFormContentWrap.append(self.panelContent)),
				title:self.alias,
				matchHeight:true,
				relativeTo:main.layout.mapContainerWrap,
				classes:'editFormPanel'+extraClasses,
				centerOnOrientationChange:true,
				scrollToTop:true,
				// withOutClose:true,
				onClose:self.editFormClose,
				resizeCB:self.womItemsResize
			});
			if(self.openOnStart){self.panel.open();}
		})();
	};
});