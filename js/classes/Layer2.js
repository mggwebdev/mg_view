define(function(require){
	return function pg(args){
		var self=this;
		var Checkbox=require('./Checkbox');
		var LayerLabels=require('./LayerLabels');
		var PopUp=require('./PopUp');
		var Style2=require('./Style2');
		this.createLayerOnCall=true;
		this.visible=true;
		this.layerType='vector';
		this.addToMap=true;
		this.olStyle=null;
		this.markers={};
		this.popUps=[];
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		if(!this.id){this.id=main.utilities.generateUniqueID()}
		if(!this.alias){this.alias=this.id}
		this.markerStyle=this.markerStyle || main.utilities.getMarkerStyle();
		this.setProperties=function(props){
			for(var key in props){
				self[key]=props[key];
			}
		};
		if(this.properties){this.setProperties(this.properties);}
		this.draggable=this.draggable || false;
		this.setVisible=function(visible){
			self.visible=visible;
			self.olLayer.setVisible(visible);
			if(!visible){self.hidePopUps();
			}else{self.showPopUps();}
		};
		this.setDraggable=function(draggable){
			self.draggable=draggable;
			self.olLayer.setProperties({draggable:draggable});
		};
		this.setSource=function(layerObj){
			// self.source=new ol.source.GeoJSON({
				// extractStyles:false,
				// object:layerObj
			// });
			self.source=new ol.source.Vector({
				features: (new ol.format.GeoJSON()).readFeatures(layerObj)
			});
			self.olLayer.setSource(self.source);
		};
		this.showPopUps=function(){
			for(var key in self.markers){
				if(self.markers[key].popup && !self.markers[key].popup.visible && self.markers[key].popup.wasOn){
					self.markers[key].popup.showPopup();
				}
			}
		};
		this.setStyle=function(style){
			self.olLayer.setStyle(style);
		};
		this.setDefaultStyle=function(){
			if(self.olStyle){
				self.olLayer.setStyle(self.olStyle);
			}else{
				self.style.setOriginalStyle();
				self.olLayer.setStyle(self.style.createNewStyle());
			}
		};
		this.hidePopUps=function(destroy){
			for(var key in self.markers){
				if(self.markers[key].popup && self.markers[key].popup.visible){
					self.markers[key].popup.hide(true);
				}
			}
			if(destroy){
				for(var i=0;i<self.popUps.length;i++){
					self.popUps[i].remove(true);
					self.popUps.splice(i,1);
					i--;
				}
			}
			else{for(var i=0;i<self.popUps.length;i++){self.popUps[i].hide(true);}}
		};
		this.labels=new LayerLabels(this);
		self.defaultStyles=self.defaultStyles || {};
		this.style=new Style2({vectorLayer:this,opacity:self.opacity,defaultStyles:self.defaultStyles,iconSrc:self.iconSrc,anchor:self.anchor});
		(this.createLayer=function(){
			if(!self.source && self.createLayerOnCall){return;}
			if(self.layerType=='vector'){
				
				if(self.olStyle){var style=self.olStyle;}else{var style=self.style.createNewStyle();}
				var minResolution,maxResolution;
				if(self.properties.max_res){maxResolution=self.properties.max_res;}
				if(self.properties.min_res){minResolution=self.properties.min_res;}
				self.olLayer=new ol.layer.Vector({
					source:self.source,
					style:style,
					visible:self.visible,
					maxResolution:maxResolution,
					minResolution:minResolution,
				});
				self.olLayer.set('_id',self.id);
				if(self.firstPostRender){
					self.olLayer.once('precompose',function(e){
						if(!self.visible){self.olLayer.setVisible(false);}
						self.firstPostRender(self);
					});
				}
			}
			self.olLayer.setProperties(self.properties);
			self.olLayer.setProperties({id:self.id,draggable:self.draggable,fromApp:true});
			if(self.doneDragCB){
				self.olLayer.setProperties({doneDragCB:self.doneDragCB});
			}
			if(self.olLayerProps){self.olLayer.setProperties(self.olLayerProps);}
			//add marker layer
			if(self.markerSource){
				self.markerLayer=new ol.layer.Vector({
					source:self.markerSource
				});
				self.map.map.addLayer(self.markerLayer);
				// self.popup=new PopUp(self.popupsContainer,{
					// position:cood,
					// html:formattedAddress
				// });
			}
			var mapLayers=main.map.map.getLayers();
			mapLayers.insertAt(1,self.olLayer);
			main.map.sortLayers();
			// if(self.addToMap){main.map.map.addLayer(self.olLayer);}
			// main.map.map.addLayer(self.olLayer);
			main.addedLayers.push(self.olLayer);
			// main.map.mapq8.removeLayer(self.olLayer);
		})();
		this.setMarkerStyle=function(style){
			self.markerStyle=style;
		};
		this.clearMarkers=function(deleteToo){
			for(var id in self.markers){
				self.removeFeature(id,deleteToo);
			}
		};
		this.removeFeature=function(id,deleteToo){
			self.source.removeFeature(self.markers[id].feature);
			self.markers[id].iconOnMap=false;
			if(deleteToo){
				self.markers[id].popup.remove();
				delete self.markers[id];
			}
		};
		this.registerPopUp=function(popup){
			self.popUps.push(popup);
		};
		this.addMarker=function(cood,formattedAddress,addIconToMap,removeMarkerOnPopUpClose,popupLabel){
			var iconFeature=new ol.Feature({
				geometry: new ol.geom.Point(cood)
			});
			var id=main.utilities.generateUniqueID();
			iconFeature.setProperties({'layerid':self.id,'type':'marker','id':id});
			iconFeature.setStyle(self.markerStyle);
			var popup=new PopUp({
				position:cood,
				html:formattedAddress,
				removeMarkerOnPopUpClose:removeMarkerOnPopUpClose,
				map:self.map,
				iconOffset:[26,-48],
				topLabelContent:popupLabel,
				classes:'layerPopup'
			});
			addIconToMap=true;
			if(addIconToMap){self.source.addFeature(iconFeature);}
			self.markers[id]={
				feature:iconFeature,
				id:id,
				iconOnMap:!!addIconToMap,
				formattedAddress:formattedAddress,
				type:'locate_point',
				popup:popup,
				geocoder:self.uniqueID,
				layer:self
			};
			self.markers[id].popup.setMarker(self.markers[id]);
		};
	};
});