define(function(require){
	return function pg(args){
		var self=this;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.name=this.name || '';
		this.value=this.value || '';
		this.checked=this.checked || false;
		this.label=this.label || '';
		this.classes=this.classes || '';
		this.orientation=this.orientation || 'checkLeft';
		this.id=this.id || '';
		this.data=this.data || [];
		this.disabled=this.disabled || '';
		this.readonly=this.readonly || '';
		this.wrapClasses=this.wrapClasses || '';
		this.wasOn=false;
		
		this.create=function(){
			this.html=$('<div>').addClass('checkboxWrap cf')
			if(this.wrapClasses){this.html.addClass(this.wrapClasses);}
			var checked='';
			if(this.checked)checked=' checked="checked"';
			this.radio=$('<input type="radio" name="'+this.name+'"'+checked+' value="'+this.value+'">').addClass('radio checkboxEle '+this.classes);
			if(this.checked){this.radio.attr('checked',true)}
			this.label=$('<div>').addClass('checkboxLabel checkboxEle').html(this.label);
			if(this.orientation=='checkLeft'){
				this.html.append(this.radio).append(this.label)
			}else if(this.orientation=='checkRight'){
				this.html.append(this.label).append(this.radio)
			}
			if(this.data.length>0){
				for(var i=0;i<this.data.length;i++){
					this.html.data(this.data[i][0],this.data[i][1])
					this.radio.data(this.data[i][0],this.data[i][1])
				}
			}
			if(this.disabled){this.radio.prop('disabled',true);}
			if(this.readonly){this.radio.prop('readonly',true);}
		};
		this.turnOffableMouseUp=function(){
			if(self.radio.prop('checked')){
				self.wasOn=true;
			}
		};
		this.turnOffableClick=function(){
			if(self.radio.prop('checked') && self.wasOn){
				self.wasOn=false;
				self.radio.prop('checked',false).change();
			}
		};
		this.turnOnTurnOffable=function(){
			self.radio.on('mouseup',self.turnOffableMouseUp);
			self.radio.on('click',self.turnOffableClick);
		};
		this.turnOffTurnOffable=function(){
			self.radio.off('mouseup',self.turnOffableMouseUp);
			self.radio.off('click',self.turnOffableClick);
		};
		this.initEvents=function(){
			this.label.on('click',function(e){
				self.radio.click();
			});
			if(self.turnOffable){
				self.turnOnTurnOffable();
			}
		};
		this.create();
		this.initEvents();
	};
});