define(function(require){
	return function pg(args){
		var self=this;
		var Wait=require('instToMainClasses/Wait');
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.waits={};
		this.waitFunctions={};
		this.stop=false;
		this.stopWaiting=function(){
			for(var key in self.waits){
				self.waits[key].stopIts();
			}
			self.stop=true;
		};
		(this.init=function(){
			for(var key in self.layers){
				var criteriaName=key+'_wait_criteria';
				var loadedName=key+'_loaded';
				self.waitFunctions[criteriaName]=function(params){
					if(self.layers[params.key].features){return true;}else{return false;}
				};
				self.waitFunctions[loadedName]=function(results,params){
					if(!self.stop){
						if(self.context['add_map_layer_'+params.key]){
							self.context['add_map_layer_'+params.key](params.key);
						}else if(self.context.mapInteractions && self.context.mapInteractions['add_map_layer_'+params.key]){
							self.context.mapInteractions['add_map_layer_'+params.key](params.key);
						}
					}
				};
				self.waits[key]=new Wait(self.waitFunctions[criteriaName],self.waitFunctions[loadedName],{key:key},{key:key});
			}
		})();
	};
});