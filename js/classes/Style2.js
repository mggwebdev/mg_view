define(function(){
	return function pg(args){
		var self=this;
		this.default_fill_color='rgba(0, 255, 219,0.5)';
		this.default_stroke_color='rgba(0, 199, 255, 0.9)';
		this.default_shape='circle';
		this.default_circle_radius=4;
		// this.default_circle_fill='#ffcc33';
		this.default_opacity=0.5;
		this.default_stroke_width=1;
		this.randomColor=false;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		if(self.defaultStyles && self.defaultStyles.fill_color){this.default_fill_color=self.defaultStyles.fill_color;}
		if(self.defaultStyles && self.defaultStyles.stroke_color){this.default_stroke_color=self.defaultStyles.stroke_color;}
		if(self.defaultStyles && self.defaultStyles.point_radius){this.default_circle_radius=Number(self.defaultStyles.point_radius);}
		if(self.defaultStyles && self.defaultStyles.stroke_width){this.default_stroke_width=Number(self.defaultStyles.stroke_width);}
		if(self.defaultStyles && self.defaultStyles.z_index){this.z_index=Number(self.defaultStyles.z_index);
		}else{this.z_index=main.defaultStyle.zIndex;}
		this.opacity=this.opacity || this.default_opacity;
		this.params={};
		this.styles={
			fill_color:{current_val:self.default_fill_color},
			stroke_color:{current_val:self.default_stroke_color},
			stroke_width:{current_val:self.default_stroke_width},
			point_radius:{current_val:self.default_circle_radius},
			shape:{current_val:self.default_shape},
			z_index:{current_val:self.z_index}
			// circle_fill:{current_val:self.default_fill_color}
		}
		if(self.randomColor){
			var fill_color=main.utilities.getColorNumber(null,null,null,'Vivid Random');
			this.styles.fill_color.current_val='rgba('+fill_color[0]+','+fill_color[1]+','+fill_color[2]+','+self.opacity+')';
		}else if(!self.defaultStyles || !self.defaultStyles.fill_color){
			if(!self.vectorLayer.properties.fill_color){var color=main.utilities.getNextColor();
			}else{var color=main.utilities.getColorArray(self.vectorLayer.properties.fill_color);}
			if(!color){var color=main.utilities.getNextColor();}
			this.styles.fill_color.current_val='rgba('+color[0]+','+color[1]+','+color[2]+','+self.opacity+')';
		}
		(this.setParams=function(params){
			for(var key in params){
				self.params[key]=params[key].current_val
			}
		})(self.styles);
		this.setOriginalStyle=function(){
			self.setParams(self.styles);
		}
		this.createNewStyle=function(feature,resolution,dom){
			if(feature && feature.get('isActive')){
				var strokeColor=main.activeStyle.strokeColor;
				var strokeWidth=main.activeStyle.strokeWidth;
				var zIndex=main.activeStyle.zIndex;
			}else{
				var strokeColor=self.params.stroke_color;
				var strokeWidth=self.params.stroke_width;
				var zIndex=self.z_index;
			}
			var radius=self.params.point_radius;
			if(main.browser.isMobile && radius<9){radius=9;}
			if(self.vectorLayer.iconSrc){
				var anchor=self.anchor || [0.5,0.5];
				return new ol.style.Style({
					zIndex: zIndex,
					image:new ol.style.Icon(({
						anchor:anchor,
						anchorXUnits:'fraction',
						anchorYUnits:'fraction',
						opacity:0.75,
						src:self.vectorLayer.iconSrc
					}))
				});
				/* return new ol.style.Style({
					fill: new ol.style.Fill({
						color: self.params.fill_color
					}),
					stroke: new ol.style.Stroke({
						color: self.params.stroke_color,
						width: self.params.stroke_width
					}),
					image:new ol.style.Icon(({
						anchor:[0.5,0.5],
						anchorXUnits:'fraction',
						anchorYUnits:'fraction',
						opacity:0.75,
						src:self.vectorLayer.iconSrc
					}))
				}) */
				/* 
				return new ol.style.Style({
					fill: new ol.style.Fill({
						color: self.params.fill_color
					}),
					stroke: new ol.style.Stroke({
						color: strokeColor,
						width: strokeWidth
					}),
					zIndex: zIndex,
					image:new ol.style.Circle({
						radius:radius,
						fill:new ol.style.Fill({
							color:self.params.fill_color
						}),
						stroke: new ol.style.Stroke({
							color: strokeColor,
							width: strokeWidth
						})
					})
				}); */
			}else{
				if(self.vectorLayer.olLayer){
					var g=self.vectorLayer.olLayer.getSource().getFeatures()[0];
					main.y=g;
				}
				var goShape=false;
				if(self.params.shape && self.params.shape!='circle'){
					if(self.vectorLayer.olLayer){
						var feature0=self.vectorLayer.olLayer.getSource().getFeatures()[0];
						if(feature0){
							var geometry=feature0.getGeometry();
							if(!(geometry instanceof ol.geom.Polygon) && !(geometry instanceof ol.geom.MultiPolygon)){
								goShape=true;
							}
						}
					}else{
						goShape=true;
					}
				}
				if(goShape){
					var shape=main.shapes[self.params.shape];
					var radius2=null;
					if(shape.radiusMult || shape.radiusMult===0){
						if(shape.radiusMult!==0){radius2=radius*shape.radiusMult;
						}else{radius2=0;}
					}else{
						radius2=radius;
					}
					return new ol.style.Style({
						zIndex: zIndex,
						image:new ol.style.RegularShape({
							fill:new ol.style.Fill({
								color: self.params.fill_color
							}),
							stroke:new ol.style.Stroke({
								color: strokeColor,
								width: strokeWidth
							}),
							points:shape.points,
							radius:radius,
							radius2:radius2,
							angle:shape.angle
						})
					});
				}else{
					return new ol.style.Style({
						fill: new ol.style.Fill({
							color: self.params.fill_color
						}),
						stroke: new ol.style.Stroke({
							color: strokeColor,
							width: strokeWidth
						}),
						zIndex: zIndex,
						image:new ol.style.Circle({
							radius:radius,
							fill:new ol.style.Fill({
								color:self.params.fill_color
							}),
							stroke: new ol.style.Stroke({
								color: strokeColor,
								width: strokeWidth
							})
						})
					});
				}
			}
		};
		this.createNewLabeledStyle=function(feature,resolution,dom){
			var labels=self.vectorLayer.labels;
			var text=labels.createTextStyle(feature,resolution,dom);
			if(feature && feature.get('isActive')){
				var strokeColor=main.activeStyle.strokeColor;
				var strokeWidth=main.activeStyle.strokeWidth;
				var zIndex=main.activeStyle.zIndex;
			}else{
				var strokeColor=self.params.stroke_color;
				var strokeWidth=self.params.stroke_width;
				var zIndex=self.z_index;
			}
			if(self.vectorLayer.iconSrc){
				return new ol.style.Style({
					zIndex: zIndex,
					fill: new ol.style.Fill({
						color: self.params.fill_color
					}),
					stroke: new ol.style.Stroke({
						color: strokeColor,
						width: strokeWidth
					}),
					image:new ol.style.Icon(({
						anchor:[0.5,0.5],
						anchorXUnits:'fraction',
						anchorYUnits:'fraction',
						opacity:0.75,
						src:self.vectorLayer.iconSrc
					})),
					text:text
				})
			}else{
				return new ol.style.Style({
					zIndex: zIndex,
					fill: new ol.style.Fill({
						color: self.params.fill_color
					}),
					stroke: new ol.style.Stroke({
						color: strokeColor,
						width: strokeWidth
					}),
					text:text
				})
			}
		};
		this.setStyleFunction=function(){
			if(self.vectorLayer.labels.withLabels){
				self.vectorLayer.olLayer.setStyle(self.labelStyleFunction);
			}else{
				self.vectorLayer.olLayer.setStyle(self.styleFunction);
			}
		};
		this.labelStyleFunction=function(feature,resolution,dom){
			if(main.filter_legend && main.filter_legend.active && !main.filter_legend.disabled && main.filter_legend.currentStyle && main.filter_legend.layerName==self.vectorLayer.name && !self.vectorLayer.iconSrc){
				var labels=self.vectorLayer.labels;
				var text=labels.createTextStyle(feature,resolution,dom);
				return main.filter_legend.currentStyle(feature,text);
			}else{
				return [self.vectorLayer.style.createNewLabeledStyle(feature,resolution,dom)];
			}
		};
		this.styleFunction=function(feature,resolution,dom){
			if(main.filter_legend && main.filter_legend.active && !main.filter_legend.disabled && main.filter_legend.currentStyle && main.filter_legend.layerName==self.vectorLayer.name){
				return main.filter_legend.currentStyle(feature);
			}else{
				return [self.createNewStyle(feature,resolution,dom)];
			}
		};
		this.setLayer=function(layer){
			this.vectorLayer=layer;
		};
		this.updateLayers=function(params){
			self.setParams(params);
			// main.utilities.setCurrentVals(main.layers.layersForm,this.params);
			self.setStyleFunction();
		};
	};
});