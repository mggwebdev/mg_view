define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Checkbox=require('./Checkbox');
		var Panel=require('./Panel');
		var Tabs=require('./Tabs');
		var ViewMore=require('./ViewMore');
		this.alias='Mass Delete';
		this.name='massdelete';
		this.withItem='with_massdelete';
		this.orgSelected=null;
		this.uploadIdSelected=null;
		this.orgs=null;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.addShelf=function(){
			self.launchButton=new Button('toolBox','',self.alias);
			self.shelf=main.layout.toolBox.createShelf({
				name:self.name,
				button:self.launchButton.html,
				content:self.html,
				context:self,
				alias:self.alias,
				extended:true,
				groups:main.withs[self.withItem].toolbox_groups,
				onOpen:self.massDeleteOpened
			});
			main.layout.toolBox.shelfInAppTest(main.layout.toolBox.shelves[self.name]);
		};
        this.massDeleteOpened = function() {
            self.deleteAction = null;
            self.getOrgs();
            self.getUploadIds();
        };
        this.massDelete = function() {

            var massDeleteAction = self.deleteAction;
            
            main.layout.loader.loaderOn();
            $.ajax({
                type:'POST',
                url:main.mainPath+'server/massDelete.php',
                data:{
                    params:{
                        uploadId:self.uploadId,
                        orgId:self.orgId
                    },
                    action:massDeleteAction
                },
                success:function(response){
                    if(response){
                        response=JSON.parse(response);
                        
                    }else{
                        alert('Error');
                        console.log(response);
                    }
                    main.layout.loader.loaderOff();
                }
			});
		};	
        this.getOrgs = function() {
            main.layout.loader.loaderOn();
            $.ajax({
                type:'POST',
                url:main.mainPath+'server/massDelete.php',
                data:{
                    params:{
                    },
                    action:'getOrgs'
                },
                success:function(response){
                    if(response){
                        response=JSON.parse(response);
                        self.orgs = response.orgs;						
                        $('#slctOrgId').append(
                        $('<option>')
                            .text('select an org')
                            .val('0')
                            .prop('selected',true)
                        );

                        $.each(self.orgs, function(idx,val) {
                            $('#slctOrgId').append(
                                $('<option>')
                                    .text(val.orgname)
                                    .val(val.value)
                            );
                        });

                        $('#slctOrgId').change( function () {
                            self.orgId = $( this ).find( "option:selected" ).val();
                        });                         
                                       

                    }else{
                        alert('Error');
                        console.log(response);
                    }
                    main.layout.loader.loaderOff();
                }
			});
		};	
        this.getUploadIds = function() {
            main.layout.loader.loaderOn();
            $.ajax({
                type:'POST',
                url:main.mainPath+'server/massDelete.php',
                data:{
                    params:{
                    },
                    action:'getUploadsData'
                },
                success:function(response){
                    if(response){
                        response=JSON.parse(response);
                        self.orgs = response.data;						
                        $('#slctUploadId').append(
                        $('<option>')
                            .text('select an upload')
                            .val('0')
                            .prop('selected',true)
                        );

                        $.each(self.orgs, function(idx,val) {
                            $('#slctUploadId').append(
                                $('<option>')
                                    .text(val.sourcefile + ' - ' + val.filetype + ' - ' + val.datetime )
                                    .val(val.id)
                            );
                        });

                        $('#slctUploadId').change( function () {
                            var selected = $( this ).find( "option:selected" ).val();
                            self.uploadId = selected;
                        });                         
                                       

                    }else{
                        alert('Error');
                        console.log(response);
                    }
                    main.layout.loader.loaderOff();
                }
			});
		};	
		(this.createHTML=function(){
			self.massDeleteButton =new Button('std','class1 class2','Mass Delete');
			self.orgSelect=$('<select class="" id="slctOrgId">');
			self.uploadIdSelect=$('<select class="" id="slctUploadId">');
			self.instructions =$('<div id="massDeleteInstructions">');
			self.html=$('<div class="massDelete">')
                .append('<br>Delete by organization: <input type="radio" name="mdOrgOrUpload" value="massDeleteByOrgId">')
                .append('<br>')
                .append(self.orgSelect)
                .append('<br><br>Delete by upload id: <input type="radio" name="mdOrgOrUpload" value="massDeleteByUploadId">')
                .append('<br>')
                .append(self.uploadIdSelect)
                .append('<br>')
                .append(self.instructions)
                .append('<br>')
                .append(self.massDeleteButton)
                .append('<button class="button" id="btnMassDelete">Mass Delete</button>')
            ;
			self.html.append(self.instructions);
			self.addShelf();

            $('input[name=mdOrgOrUpload]').click(function(e) {
                self.deleteAction = e.target.value;
            }); 
            $('#btnMassDelete').click(function(e) {

                if(self.deleteAction != 'massDeleteByOrgId' && self.deleteAction != 'massDeleteByUploadId') {
                    alert('Please choose to delete by orgnaization, or by upload.');
                    return;
                }

                var confirm =  window.confirm('WARNING - are you absolutely sure you want to delete features from the map?  This action cannot be undone.');
                if( confirm ) { 
                    self.massDelete() 
                };
            }); 
		})();
	}
});


