define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Input=require('./Input');
		var Panel=require('./Panel');
		this.withItem='with_inventory_editor';
		this.alias='Inventory Editor';
		this.name='invEditor';
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.addShelf=function(){
			self.launchButton=new Button('toolBox','',self.alias);
			self.shelf=main.layout.toolBox.createShelf({
				name:self.name,
				button:self.launchButton.html,
				content:self.html,
				context:self,
				alias:self.alias,
				dontAppendTo:true,
				groups:main.withs[self.withItem].toolbox_groups
			});
			main.layout.toolBox.shelfInAppTest(main.layout.toolBox.shelves[self.name]);
		};
		this.addListItem=function(pid,count,val,lookup_table,primary_field,size){
			var input=new Input({
				classes:'invEditorInput',
				lookup_table:lookup_table,
				lookup_table_field:primary_field,
				setToVal:val
			});
			var inputSizeSel=$('<select class="invEditorInputSize"><option value=""></option><option value="s">Small (under 30\')</option><option value="m">Medium (30 to 50\' - possibly taller in ideal conditions)</option><option value="l">Large (over 50\')</option><select/>');
			if(size){inputSizeSel.val(size);}
			return {
				li:$('<li class="invEditorListLI" data-pid="'+pid+'">')
					.append($('<table>')
						.append($('<tbody>')
							.append($('<tr>')
								.append($('<td>').html('Count'))
								.append($('<td>')
									.append($('<div class="invEditorListLIEle invEditorListLICount">').append('<input type="number" min="0" class="invEditorListLICountInput" value="'+count+'"/>'))
									.append($('<div class="invEditorListLIEle invEditorListLIDelete">').html($('<div class="invEditorDeleteButton button">X</div>')))))
							.append($('<tr>')
								.append($('<td>').html('Species'))
								.append($('<td>').append($('<div class="invEditorListLIEle invEditorListLIAlias">').html(input.container))))
							.append($('<tr>')
								.append($('<td>').html('Size'))
								.append($('<td>').append($('<div class="invEditorListLIEle invEditorListLISize">').html(inputSizeSel))))
					
						)
					),
				input:input
			}
		};
		this.refresh=function(){
			self.unitsWrap.html('');
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2]
					},
					action:'getInventoryEditorData'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							self.inventories={};
							if(response.results && Object.keys(response.results).length>0){
								// var lookUpTableMap=main.tables[results[key].layer].properties.lookup_table_map;
								var results=response.results;
								for(var key in results){
									self.inventories[key]={
										items:{},
										info:results[key],
										list:$('<ul class="invEditorList">'),
										addButton:new Button('std','invEditorAddButton','Add',[['inv',key]])
									};
									self.inventories[key].addButton.html.on('click',function(){
										self.addItem($(this).closest('.invEditorUnit').data('table'));
									});
									for(var i=0;i<results[key].results.length;i++){
										var liObj=self.addListItem(results[key].results[i].pid,results[key].results[i].count,results[key].results[i].value,results[key].properties.lookup_table,results[key].properties.primary_field,results[key].results[i].size_class);
										self.inventories[key].list.append(liObj.li);
										self.inventories[key].items[results[key].results[i].pid]=results[key].results[i];
									}
									self.inventories[key].unit=$('<div class="invEditorUnit" data-table="'+key+'">')
										.append($('<div class="invEditorUnitTop cf">')
											.append($('<div class="invEditorLabel">').html(results[key].properties.alias))
											.append(self.inventories[key].addButton.html)
										).append($('<div class="invEditorListWrap">')
											.append(self.inventories[key].list)
										)
									self.unitsWrap.append(self.inventories[key].unit);
								}
							}
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		(this.createHTML=function(){
			self.unitsWrap=$('<div class="invEditorUnitsWrap">');
			self.html=$('<div class="invEditorWrap">').append(self.unitsWrap);
			self.refresh();
			self.addShelf();
		})();
		this.refreshAvailSels=function(pid,inv){
			if(main.main.formsMod){
				for(var key in main.formsMod.forms0){
					if(main.formsMod.forms0[key].properties.active){
						for(var key2 in main.formsMod.forms0[key].fields){
							if(main.formsMod.forms0[key].fields[key2].input && main.formsMod.forms0[key].fields[key2].input.availabilitySelect){
								main.formsMod.forms0[key].fields[key2].input.availabilitySelect.refresh();
							}
						}
					}
				}
			}
			if(main.dataTables){
				for(var key in main.dataTables){
					for(var key2 in main.dataTables[key].rows){
						for(var key3 in main.dataTables[key].rows[key2].fields){
							if(main.dataTables[key].rows[key2].fields[key3].input && main.dataTables[key].rows[key2].fields[key3].input.availabilitySelect){
								main.dataTables[key].rows[key2].fields[key3].input.availabilitySelect.refresh();
							}
						}
					}
				}
			}
		};
		this.deleteItem=function(pid,inv){
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
						table:self.inventories[inv].info.properties.name,
						pid:pid,
						inv:inv
					},
					action:'deleteInventoryEditorItem'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							delete self.inventories[response.params.inv].items[response.params.pid];
							self.inventories[response.params.inv].list.find('.invEditorListLI[data-pid="'+response.params.pid+'"]').remove();
							self.refreshAvailSels();
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.addItem=function(inv){
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
						table:self.inventories[inv].info.properties.name,
						inv:inv
					},
					action:'addInventoryEditorItem'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							var liObj=self.addListItem(response.result.pid,response.result.count,response.result.value,self.inventories[response.params.inv].info.properties.lookup_table,self.inventories[response.params.inv].info.properties.primary_field);
							self.inventories[response.params.inv].list.append(liObj.li);
							self.inventories[response.params.inv].items[response.result.pid]=response.result;
							self.refreshAvailSels();
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.updateInvEditorList=function(pid,table,count,val,size){
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
						count:count,
						val:val,
						size:size,
						pid:pid,
						table:table
					},
					action:'updateInventoryEditorData'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							self.refreshAvailSels();
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		(this.initEvents=function(){
			self.html.on('change','.invEditorListLICountInput,.invEditorInput,.invEditorInputSize',function(){
				var invEditorListLI=$(this).closest('.invEditorListLI');
				self.updateInvEditorList(invEditorListLI.data('pid'),$(this).closest('.invEditorUnit').data('table'),invEditorListLI.find('.invEditorListLICountInput').val(),invEditorListLI.find('.invEditorInput').val(),invEditorListLI.find('.invEditorInputSize').val());
			});
			self.html.on('click','.invEditorDeleteButton',function(){
				if(confirm('Are you sure you want to delete this?')){
					self.deleteItem($(this).closest('.invEditorListLI').data('pid'),$(this).closest('.invEditorUnit').data('table'));
				}
			});
		})();
	};
});