define(function(require){
	return function pg(args){
		var self=this;
		var Accordian=require('./Accordian');
		var Button=require('./Button');
		var Checkbox=require('./Checkbox');
		var Input=require('./Input');
		var LookUp=require('./LookUp');
		var Panel=require('./Panel');
		var Range=require('./Range');
		var Tabs=require('./Tabs');
		var ViewMore=require('./ViewMore');
		this.alias='Advanced Filter';
		this.classes='';
		this.currentLayer=null;
		this.where=null;
		this.geoFilter=null;
		this.with_ecobens=false;
		this.notInInputTypes=['geometry','hidden','html'];
		this.notInInputTypesExceptions=['pid'];
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.layers={};
		var invents=Object.keys(main.inventories);
		for(var key in invents){
			this.layers[invents[key]]=main.tables[invents[key]];
		}
		this.lookUps={};
		this.ranges={};
		this.appliesTo={};
		this.currentFields=[];
		this.startOnOpen=true;
		this.defaultLayer='trees';
		this.filter={
			currentFilters:{},
			// currentRows:[]
		}
		this.onPanelOpen=function(){
			self.panel.setPanelParams();
			if(self.startOnOpen && self.defaultLayer && self.layers[self.defaultLayer].map_layer.layer && !self.currentLayer){
				// self.selectFieldWrap.addClass('none');
				self.changeLayer(self.defaultLayer);
			}
		};
		this.onPanelClose=function(){
		};
		this.filterTextChange=function(fieldName,val,html,dontUpdateFilter){
			self.filter.currentFilters[fieldName]={
				type:'text',
				fieldName:fieldName,
				value:val,
				html:html
			}
			if(html.find('.showNullsOption').prop('checked')){self.filter.currentFilters[fieldName].showNulls=true;}
			// if(!dontUpdateFilter){self.updateFilter();}
		};
		this.fileCheckChange=function(fieldName,val,html,dontUpdateFilter){
			self.filter.currentFilters[fieldName]={
				type:'withFile',
				fieldName:fieldName,
				value:val,
				html:html
			}
			// if(html.find('.showNullsOption').prop('checked')){self.filter.currentFilters[fieldName].showNulls=true;}
			// if(!dontUpdateFilter){self.updateFilter();}
		};
		this.filterLUChange=function(fieldName,luPkg,dontUpdateFilter){
			var lu=luPkg.data('lu');
			filtered=self.lookUps[lu].currentFiltered;
			self.filter.currentFilters[fieldName]={
				type:'check',
				fieldName:fieldName,
				values:filtered,
				html:luPkg
			}
			if(luPkg.find('.showNullsOption').prop('checked')){self.filter.currentFilters[fieldName].showNulls=true;}
			// if(!dontUpdateFilter){self.updateFilter();}
		};
		this.filterCheckChange=function(fieldName,chkPkg,dontUpdateFilter){
			var val,values=[];
			chkPkg.find('.filterCheck:checked').each(function(){
				val=$(this).val();
				if(val=='true'){val=true;}
				if(val=='false'){val=false;}
				values.push(val);
			});
			self.filter.currentFilters[fieldName]={
				type:'check',
				fieldName:fieldName,
				values:values,
				html:chkPkg
			}
			if(chkPkg.find('.showNullsOption').prop('checked')){self.filter.currentFilters[fieldName].showNulls=true;}
			// if(!dontUpdateFilter){self.updateFilter();}
		};
		this.closeFieldShelves=function(){
			self.fieldsAccordian.closeAllShelves();
			if(self.ecosAccordian){self.ecosAccordian.closeAllShelves();}
		};
		this.resetFilters=function(){
			self.html.find('.filterInput.textInput').val('');
			self.html.find('.filterInput.filterCheck').prop('checked',false);
			self.html.find('.showNullsOption').prop('checked',false);
			self.html.find('.luFilterPkg').each(function(){
				self.lookUps[$(this).data('id')].clearLUPkg();
			});
			self.html.find('.rangeOuterWrap').each(function(){
				self.ranges[$(this).attr('id')].reset();
			});
			self.html.find('.applyAdvFilterChk').prop('checked',false);
		};
		this.reset=function(){
			if(self.currentLayer){self.hardClearFilter();}
			self.currentLayer=null;
			self.layerSelect.val('');
			// self.fieldsWrap.html('');
			self.fieldsAccordian.removeAllShelves();
			if(self.ecosAccordian){self.ecosAccordian.removeAllShelves();}
			self.resetAppliesTo();
			self.advFTopBs.addClass('none');
			// self.panel.positionPanel();
		};
		this.setWhereThings=function(){
			self.whereShowContent.html(self.displayWhere);
			if(!self.where){self.whereShow.addClass('none');}else{self.whereShow.removeClass('none');}
			self.panel.setPanelParams();
		};
		this.hardClearFilter=function(){
			self.clearFilter();
			self.resetFilters();
			self.closeFieldShelves();
			self.filter.currentFilters={};
			self.where='';
			self.setWhereThings();
		};
		this.clearFilter=function(){
			// self.filter.currentRows=Object.keys(self.currentLayer.map_layer.features);
		};
		this.getExtremes=function(fieldName){
			var int=main.invents.inventories[self.currentLayer.properties.name];
			return {max:int.maxs[fieldName],min:int.mins[fieldName]}
		};
		this.filterRangeChange=function(filterPkg,dontUpdateFilter){
			var fieldName=filterPkg.key;
			self.filter.currentFilters[fieldName]={
				type:'range',
				fieldName:fieldName,
				max:filterPkg.currentHi,
				min:filterPkg.currentLow,
				hasSpecifics:filterPkg.hasSpecifics,
				html:filterPkg.html
			}
			if(filterPkg.html.closest('.rangeInputPkg').find('.showNullsOption').prop('checked')){self.filter.currentFilters[fieldName].showNulls=true;}
			// if(!dontUpdateFilter){self.updateFilter();}
		};
		this.updateLookUpTableFilter=function(params){
			var fields=self.currentLayer.fields;
			var fieldName=params.fieldName,values=params.values,lookUpTable=params.lookUpTable,inputPkgContent=params.inputPkgContent;
			var table=main.data.lookUpTables[lookUpTable];
			var filterChksWrap=inputPkgContent.find('.filterChksWrap');
			filterChksWrap.html('');
			var item,alias='';
			var lookUpTableMap=self.currentLayer.properties.lookup_table_map;
			var cultivarField=null;
			if(self.currentLayer.properties.internal_field_map && self.currentLayer.properties.internal_field_map.cultivar){cultivarField=self.currentLayer.properties.internal_field_map.cultivar;}
			values.sort(function(a,b){
				var aAlias=main.utilities.getSimpleLookUpAlias(a,fields[fieldName],lookUpTableMap,cultivarField);
				var bAlias=main.utilities.getSimpleLookUpAlias(b,fields[fieldName],lookUpTableMap,cultivarField);
				if(aAlias && typeof aAlias=='string'){aAlias=aAlias.toLowerCase();}
				if(bAlias && typeof bAlias=='string'){bAlias=bAlias.toLowerCase();}
				if(aAlias>bAlias){return 1;}
				if(aAlias<bAlias){return -1;}
				return 0;
			});
			for(var i=0;i<values.length;i++){
				if(values[i]){
					alias=main.utilities.getSimpleLookUpAlias(values[i],fields[fieldName],lookUpTableMap,cultivarField);
					filterChksWrap.append(new Checkbox({
						label:alias,
						value:values[i],
						name:values[i],
						classes:'filterCheck filterInput '+values[i]
					}).html);
				}
			}
		};
		/* this.getLookUpValues=function(fieldName){
			var values=[];
			var features=self.currentLayer.map_layer.features;
			for(var key in features){
				val=features[key].properties[fieldName];
				if(val && values.indexOf(val)===-1){
					values.push(val);
				}
			}
			return values;
		}; */
		/* this.updateLookUpTable=function(inputPkgContent,fieldName,lookUpTable){
			var values=self.getLookUpValues(fieldName);
			if(!main.data.lookUpTables[lookUpTable].items){
				main.data.lookUpTables[lookUpTable].waiting.push([self.updateLookUpTableFilter,{fieldName:fieldName,values:values,lookUpTable:lookUpTable,inputPkgContent:inputPkgContent}]);
			}else{
				self.updateLookUpTableFilter({fieldName:fieldName,values:values,lookUpTable:lookUpTable,inputPkgContent:inputPkgContent});
			}
		}; */
		this.getCheckNullsCheckbox=function(type){
			return new Checkbox({
				label:'Show Nulls',
				value:'showNulls',
				checked:false,
				name:'showNulls',
				classes:'showNullsOption '+type
			}).html;
		};
		this.filterMapPointChangeLat=function(filterPkg){
			var fieldName=filterPkg.key;
			var latRangePkg=self.ranges[filterPkg.html.closest('.filterPkg').find('.filterInput.lat').attr('id')].getRangePkg();
			var lngRangePkg=self.ranges[filterPkg.html.closest('.filterPkg').find('.filterInput.lng').attr('id')].getRangePkg();
			self.filter.currentFilters[fieldName]={
				type:'map_range',
				fieldName:fieldName,
				maxLat:latRangePkg.currentHi,
				minLat:latRangePkg.currentLow,
				maxLng:lngRangePkg.currentHi,
				minLng:lngRangePkg.currentLow,
				html:filterPkg.html
			}
			// self.updateFilter();
		};
		this.getMapPointExtremes=function(fieldName){
			var maxLat=Number.NEGATIVE_INFINITY,minLat=Number.POSITIVE_INFINITY,maxLng=Number.NEGATIVE_INFINITY,minLng=Number.POSITIVE_INFINITY,val,points,point;
			var features=self.currentLayer.map_layer.features;
			for(var key in features){
				val=features[key].properties[fieldName];
				if(val || val===0){
					points=val.split(',');
					for(var i=0;i<points.length;i++){
						point=points[i].split(' ');
						lat=Number(point[1]);
						lng=Number(point[0]);
						if(lat>maxLat){maxLat=lat;}
						if(lat<minLat){minLat=lat;}
						if(lng>maxLng){maxLng=lng;}
						if(lng<minLng){minLng=lng;}
					}
				}
			}
			if(maxLat==Number.NEGATIVE_INFINITY){return false;}
			return {maxLat:maxLat,minLat:minLat,maxLng:maxLng,minLng:minLng}
		};
		this.getFilter=function(field,isEco){
			var inputPkg='';
			if(field.input_type=='text' || (field.input_type=='hidden' && main.misc.textDTs.indexOf(field.data_type)!=-1) || field.input_type=='textarea'){
				var input=new Input({
					field:field,
					classes:'filterInput',
					typeOverride:'text',
					is_in_filter:true,
					layerName:self.currentLayer.properties.layer_name
				});
				input.container.prop('disabled',false);
				var inputPkg=$('<div>').addClass('inputPkg textInputPkg').data('name',field.name)
					.append($('<div>').addClass('inputPkgContent')
						.append(input.container)
						.append($('<div class="filterExtras">').append(self.getCheckNullsCheckbox('txt')))
					);
			}else if(field.input_type=='file'){
				var checkbox=new Checkbox({
					label:'With '+field.alias,
					value:'true',
					checked:false,
					name:field.name,
					classes:'filterFileCheck filterInput'
				});
				var inputPkg=$('<div>').addClass('inputPkg fileInputPkg').data('name',field.name)
					.append($('<div>').addClass('inputPkgContent')
						.append(checkbox.html)
						// .append($('<div class="filterExtras">').append(self.getCheckNullsCheckbox('txt')))
					);
			}else if((field.input_type=='select' || field.input_type=='checkbox' || field.input_type=='radio') && (!field.lookup_table || !main.data.lookUpTables[field.lookup_table] || !field.with_lu_popup)){
				var inputPkgContent=$('<div>').addClass('inputPkgContent');
				var filterExtrasTop=$('<div>').addClass('filterExtrasTop');
				var filterExtrasToggleAll=new Checkbox({
					value:'true',
					name:'filterExtrasToggleAll',
					checked:false,
					label:"Toggle All",
					classes:'filterExtrasToggleAll'
				});
				filterExtrasToggleAll.checkbox.change(function(){
					$(this).closest('.inputPkgContent').find('.filterChksWrap .filterCheck').prop('checked',$(this).prop('checked'));
				});
				filterExtrasTop.append(filterExtrasToggleAll.html);
				var filterChksWrap=$('<div>').addClass('filterChksWrap');
				inputPkgContent.append(filterExtrasTop);
				inputPkgContent.append(filterChksWrap);
				if(field.options && Object.keys(field.options).length>0){
					for(var key in field.options){
						if(field.options[key].value){
							filterChksWrap.append(new Checkbox({
								label:field.options[key].alias,
								value:field.options[key].value,
								checked:false,
								name:key,
								classes:'filterCheck filterInput '+key
							}).html);
						}
					}
				}
				var getNulls='';
				if(field.data_type!='boolean'){
					getNulls=self.getCheckNullsCheckbox('chk');
				}
				var inputPkg=$('<div>').addClass('inputPkg chkPkg').data('name',field.name)
					.append(inputPkgContent
						.append($('<div class="filterExtras">').append(getNulls))
					);
			}else if(field.lookup_table){
					// self.updateLookUpTable(inputPkgContent,field.name,field.lookup_table);
				var inputPkgContent=$('<div>').addClass('inputPkgContent');
				var lu=new LookUp({
					tableName:field.lookup_table,
					forFilter:true,
					field:field,
					layerName:self.currentLayer.properties.name
					// extraOptions:param.options
				});
				self.lookUps[lu.id]=lu;
				inputPkgContent.append(lu.luFilterPkg);
				var inputPkg=$('<div>').addClass('inputPkg afLookupPkg').data('name',field.name).data('lu',lu.id)
					.append(inputPkgContent
						.append($('<div class="filterExtras">').append(self.getCheckNullsCheckbox('chk')))
					);
			}else if(field.input_type=='number' || (field.input_type=='hidden' && main.misc.numberDTs.indexOf(field.data_type)!=-1)){
				var extremes=self.getExtremes(field.name);
				if(extremes){
					var id=main.utilities.generateUniqueID();
					var ecoClass='',withExact=false;
					if(isEco){ecoClass=' ecoFilter';}
					else{withExact=true;}
					var range=new Range({
						id:id,
						classes:'filterInput'+ecoClass,
						key:field.name,
						to_fixed:field.to_fixed,
						prepend_to:field.prepend_to,
						append_to:field.append_to,
						step:field.step,
						min:extremes.min,
						max:extremes.max,
						showInputs:true,
						withExact:withExact,
						onMove:null,
						onChange:self.filterRangeChange
					});
					self.ranges[id]=range;
					var inputPkg=$('<div>').addClass('inputPkg rangeInputPkg')
						.append($('<div>').addClass('inputPkgContent')
							.append(range.html)
							.append($('<div class="filterExtras">').append(self.getCheckNullsCheckbox('rng')))
						);
				}else{
					var inputPkg=$('<div>').addClass('inputPkg')
						.append($('<div>').addClass('inputPkgContent')
							.append(self.noFilterValuesMsg)
						);
				}
			}else if(field.input_type=='date'){
				var extremes=self.getExtremes(field.name);
				if(extremes){
					var id=main.utilities.generateUniqueID();
					var isTime=false;
					var isDate=true;
					if(field.sub_data_type=='time'){
						var displayFunction=main.utilities.msToTime;
						isDate=false;
						isTime=true;
					}else{var displayFunction=main.utilities.formatDate}
					var range=new Range({
						id:id,
						classes:'filterInput',
						key:field.name,
						to_fixed:field.to_fixed,
						prepend_to:field.prepend_to,
						append_to:field.append_to,
						step:field.step,
						min:extremes.min,
						max:extremes.max,
						onMove:null,
						onChange:self.filterRangeChange,
						sticky:false,
						showInputs:true,
						withExact:true,
						stickyStep:1000*60*60*24,
						displayFunction:displayFunction,
						isDate:isDate,
						isTime:isTime
					});
					self.ranges[id]=range;
					var inputPkg=$('<div>').addClass('inputPkg rangeInputPkg')
						.append($('<div>').addClass('inputPkgContent')
							.append(range.html)
							.append($('<div class="filterExtras">').append(self.getCheckNullsCheckbox('rng')))
						)
				}else{
					var inputPkg=$('<div>').addClass('inputPkg')
						.append($('<div>').addClass('inputPkgContent')
							.append(self.noFilterValuesMsg)
						);
				}
			}else if(field.input_type=='map_point'){
				var extremes=self.getMapPointExtremes(field.name);
				if(extremes){
					var latId=main.utilities.generateUniqueID();
					var latRange=new Range({
						alias:'Lat',
						id:latId,
						classes:'filterInput lat',
						key:field.name,
						to_fixed:field.to_fixed,
						prepend_to:field.prepend_to,
						append_to:field.append_to,
						step:field.step,
						min:extremes.minLat,
						max:extremes.maxLat,
						to_fixed:10,
						onMove:null,
						onChange:self.filterMapPointChangeLat
					});
					self.ranges[latId]=latRange;
					var lngId=main.utilities.generateUniqueID();
					var lngRange=new Range({
						alias:'Lng',
						id:lngId,
						classes:'filterInput lng',
						key:field.name,
						to_fixed:field.to_fixed,
						prepend_to:field.prepend_to,
						append_to:field.append_to,
						step:field.step,
						min:extremes.minLng,
						max:extremes.maxLng,
						to_fixed:10,
						onMove:null,
						onChange:self.filterMapPointChangeLat
					});
					self.ranges[lngId]=lngRange;
					var inputPkg=$('<div>').addClass('inputPkg rangeInputPkg')
						.append($('<div>').addClass('inputPkgContent')
							.append(latRange.html)
							.append(lngRange.html)
							.append($('<div class="filterExtras">').append(self.getCheckNullsCheckbox('rng')))
						)
				}else{
					var inputPkg=$('<div>').addClass('inputPkg')
						.append($('<div>').addClass('inputPkgContent')
							.append(self.noFilterValuesMsg)
						);
				}
			}
			return inputPkg;
		};
		this.addShelfContent=function(field,isEco){
			var ecoClass='';
			if(isEco){ecoClass=' advEcoFieldUnit';}
			return $('<div class="afFieldUnit cf '+field.name+ecoClass+'" data-fieldname="'+field.name+'">')
				// .append($('<div class="afFieldUnitEle afFieldApplyWrap">').append('<input type="checkbox" class="applyAdvFilterChk" title="Check To Activate"/>'))
				// .append($('<div class="afFieldUnitEle afFieldLabelWrap">').append(field.alias))
				.append($('<div class="afFieldUnitEle afFieldFilterWrap">').append(self.getFilter(field,isEco)))
		};
		this.lookUpShelfOpened=function(shelf){
			self.lookUps[shelf.shelf.find('.afLookupPkg').data('lu')].open();
			self.panel.setPanelParams();
		};
		this.lookUpShelfClosed=function(shelf){
			self.lookUps[shelf.shelf.find('.afLookupPkg').data('lu')].close();
			self.panel.setPanelParams();
		};
		this.addEcoField=function(ecoben){
			ecoben.input_type='number';
			ecoben.to_fixed=2;
			ecoben.prepend_to='$';
			ecoben.step=0.01;
			self.ecosAccordian.createShelf({
				name:ecoben.name,
				activateCheck:true,
				activateCheckClasses:'applyAdvFilterChk advFEcoChk',
				button:new Button('toolBox','',ecoben.alias).html,
				content:self.addShelfContent(ecoben,true),
				alias:ecoben.alias
			});
		};
		this.addField=function(field){
			var onOpen=null,onClose=null;
			if(field.lookup_table && field.with_lu_popup){
				onOpen=self.lookUpShelfOpened;
				onClose=self.lookUpShelfClosed;
			}
			self.fieldsAccordian.createShelf({
				name:field.name,
				activateCheck:true,
				activateCheckClasses:'applyAdvFilterChk',
				button:new Button('toolBox','',field.alias).html,
				content:self.addShelfContent(field),
				alias:field.alias,
				onOpen:onOpen,
				onClose:onClose
			});
			// return $('<tr class="advFTR advFFieldTR '+field.name+'" data-fieldname="'+field.name+'">')
				// .append($('<td>').append($('<div class="tdLiner">').append('<input type="checkbox" class="applyAdvFilterChk"/>')))
				// .append($('<td>').append($('<div class="tdLiner">').append(field.alias)))
				// .append($('<td>').append($('<div class="tdLiner">').append(self.getFilter(field))));
		};
		this.refresh=function(){
			self.clearFilters();
			self.updateFields();
		};
		this.updateFields=function(){
			self.fieldsAccordian.removeAllShelves();
			if(self.ecosAccordian){self.ecosAccordian.removeAllShelves();}
			// self.fieldsWrap.html('');
			self.fieldAddSelect.html('<option value=""></option>');
			var fields=self.currentLayer.fields;
			var sortedKeys=main.utilities.sortFields(fields,'abc');
			self.prevFields=self.currentFields;
			self.currentFields=[];
			for(var y=0;y<sortedKeys.length;y++){
				field=fields[sortedKeys[y]];
				if(field.active && field.in_adv_filter && (self.notInInputTypes.indexOf(field.input_type)==-1 || self.notInInputTypesExceptions.indexOf(field.name)>-1)){
					if(field.is_basic || self.prevFields.indexOf(field.name)>-1){
						// value=self.properties[key];
						// self.fieldsWrap.append(self.addField(field));						
						self.addField(field);						
						self.currentFields.push(field.name);
					}else{
						self.fieldAddSelect.append('<option value="'+field.name+'">'+field.alias+'</option>');
					}
				}
			}
			if(main.withs.with_ecobens && main.withs.with_ecobens.value && self.currentLayer.properties.layer_name=='trees'){
				var ecobens=main.data.std_settings.ecobens;
				for(var key in ecobens){
					if(ecobens[key].constructor!==Array){
						self.addEcoField(ecobens[key]);
					}
				}
			}
			// self.panel.positionPanel();
		};
		this.resetAppliesTo=function(){
			for(var key in self.appliesTo){
				self.appliesTo[key].checkbox.checkbox.prop('checked',self.appliesTo[key].defaultChecked);
			}
		};
		this.addApplyTo=function(name,label,checked,disabled){
			checked=checked || false;
			disabled=disabled || false;
			self.appliesTo[name]={
				checkbox:new Checkbox({
					label:label,
					value:name,
					checked:checked,
					disabled:disabled,
					name:'toApply',
					classes:'advFToApply'
				}),
				apply:checked,
				label:label,
				defaultChecked:checked,
				name:name
			};
			self.appliesTo[name].checkbox.checkbox.on('change',function(){
				self.appliesTo[$(this).val()].apply=$(this).prop('checked');
			});
			self.selectToApplies.append(self.appliesTo[name].checkbox.html);
		};
		this.newLayerSelected=function(){
			self.updateFields();
			self.advFTopBs.removeClass('none');
			self.addFieldAdvF.removeClass('none');
			// self.selectFieldWrap.removeClass('noDec');
			self.html.removeClass('none');
			self.selectToAppliesWrap.removeClass('none');
			self.fieldsHTML.removeClass('none');
			self.panel.topContentWrap.css('margin-bottom','');
			self.panel.body.removeClass('none');
			self.panel.setPanelParams();
			self.panel.positionPanel();
		};
		this.getStyle=function(){
			return new ol.style.Style({
				fill:new ol.style.Fill({
					color:'rgba(255, 255, 255, 0.2)'
				}),
				stroke:new ol.style.Stroke({
					color:'rgb(255, 204, 51)',
					width:2
				}),
				image:new ol.style.Circle({
					radius:4,
					fill:new ol.style.Fill({
						color:'rgb(255, 204, 51)'
					})
				})
			});
		};
		this.newPolygonForSelect=function(feature,onSelected){
			main.layout.loader.loaderOn();
			var polyPoints=feature.getGeometry().getCoordinates()[0];
			var geomField=null;
			if(self.currentLayer.properties.internal_field_map && self.currentLayer.properties.internal_field_map.geometry_field){
				geomField=self.currentLayer.properties.internal_field_map.geometry_field;}
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						polyPoints:polyPoints,
						geomField:geomField,
						table:self.currentLayer.properties.name,
					},
					action:'featuresFromPolygon'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							self.geoFilter=response.results;
							main.loader.loadMoreMapPoints(self.currentLayer.properties.name);
							if(onSelected){onSelected();}
							if(!self.panel.opened || self.panel.minimized){self.panel.open();}
							self.firstDrawDrawn();
							if(self.appliesTo['filter_legend'].apply && main.filter_legend && main.filter_legend.active){main.filter_legend.refresh();}
							if(self.appliesTo['dashboard'].apply && main.dashboard && main.dashboard.active){main.dashboard.refresh();}
						}
					}
				}
			});
		};
		this.firstDrawDrawn=function(){
			main.map.map.removeInteraction(self.draw);
		};
		this.toggleSelectPolyVis=function(){
			if(main.advFilter.drawVector.getVisible()){self.hideSelectPoly();
			}else{self.showSelectPoly();}
		};
		this.hideSelectPoly=function(){
			main.map.map.removeInteraction(self.modify);
			main.advFilter.drawVector.setVisible(false);
			main.map.map.renderSync();
		};
		this.showSelectPoly=function(){
			main.map.map.addInteraction(self.modify);
			main.advFilter.drawVector.setVisible(true);
			main.map.map.renderSync();
		};
		this.stopSelectFromMap=function(){
			self.drawSource.clear();
			main.map.map.removeInteraction(self.draw);
			main.map.map.removeInteraction(self.modify);
			self.modify=null;
			self.drawSource=null;
			self.drawVector=null;
			self.draw=null;
			self.selectingForGeo=false;
			self.hasDrawnOne=false;
			self.clearSelectFromMap.html.addClass('none');
			self.selectFromMapButton.html.removeClass('none');
			self.togglePolyVisButton.html.addClass('none');
			if(self.appliesTo['data_table'].apply && main.dataTables[self.currentLayer.properties.name] && main.dataTables[self.currentLayer.properties.name].dataContentWrap && main.dataTables[self.currentLayer.properties.name].dataContentWrap.is(':visible')){
				main.dataTables[self.currentLayer.properties.name].selectFromMapButton.removeClass('none');
				main.dataTables[self.currentLayer.properties.name].clearSelectFromMap.addClass('none');
				main.dataTables[self.currentLayer.properties.name].togglePolyVisButton.addClass('none');
			}
		};
		this.startSelectFromMap=function(layerName,onSelected){
			if(layerName){self.changeLayer(layerName);}
			self.selectingForGeo=true;
			self.drawing=true;
			main.flags.drawing=true;
			self.features=new ol.Collection();
			self.drawSource=new ol.source.Vector({features:self.features});
			self.drawVector=new ol.layer.Vector({
				source:self.drawSource,
				style:self.getStyle()
			});
			self.drawVector.setMap(main.map.map);
			// main.map.map.addLayer(self.drawVector);
			self.modify=new ol.interaction.Modify({
				features:self.features,
				deleteCondition:function(event){
					return ol.events.condition.shiftKeyOnly(event) && ol.events.condition.singleClick(event);
				}
			});
			main.map.map.addInteraction(self.modify);
			self.draw=new ol.interaction.Draw({
				source:self.drawSource,
				type:'Polygon'
			});
			self.modify.on('modifyend',function(e){
				self.newPolygonForSelect(e.features.getArray()[0],onSelected);
			});
			self.draw.on('drawend',function(e){
				self.newPolygonForSelect(e.feature,onSelected);
				self.drawing=false;
				main.flags.drawing=false;
				self.hasDrawnOne=true;
				self.clearSelectFromMap.html.removeClass('none');
				self.selectFromMapButton.html.addClass('none');
				self.togglePolyVisButton.html.removeClass('none');
				if(self.appliesTo['data_table'].apply && main.dataTables[self.currentLayer.properties.name] && main.dataTables[self.currentLayer.properties.name].dataContentWrap && main.dataTables[self.currentLayer.properties.name].dataContentWrap.is(':visible')){
					main.dataTables[self.currentLayer.properties.name].selectFromMapButton.addClass('none');
					main.dataTables[self.currentLayer.properties.name].clearSelectFromMap.removeClass('none');
					main.dataTables[self.currentLayer.properties.name].togglePolyVisButton.removeClass('none');
				}
			});
			main.map.map.addInteraction(self.draw);
		};
		this.changeLayer=function(val){
			self.layerSelect.val(val);
			// self.prevLayer=self.currentLayer;
			if(self.layers[val].map_layer){
				if(self.layers[val].map_layer.layer){
					self.currentLayer=self.layers[val];
					self.newLayerSelected();
					return;
				}else{
					alert('Please load this layer.');
				}
			}else{
				self.currentLayer=self.layers[val];
				self.newLayerSelected();
				return;
			}
			self.reset();
		};
		this.getFitlerWhere=function(currentFilters,fields,layerName){
			var where='';
			var displayWhere='';
			var whereAlias='';
			if(Object.keys(currentFilters).length>0){
				var cF,values,field,val,alias,min,max,tempWhere,specifics;
				for(var key in currentFilters){
					tempWhere="";
					cF=currentFilters[key];
					field=fields[cF.fieldName];
					if(cF.type=="check"){
						values=cF.values;
						if(field.input_type=='checkbox'){
							if(main.misc.textDTs.indexOf(field.data_type)>-1){
								if(values.length){
									tempWhere+="(";
									whereAlias+="(";
									displayWhere+="(";
									for(var y=0;y<values.length;y++){
										val=values[y];
										alias=main.utilities.getAlias(val,field,layerName);
										tempWhere+="'"+val+"'=ANY("+cF.fieldName+") AND ";
										whereAlias+="'"+val+"'=ANY("+cF.fieldName+") AND ";
										displayWhere+=field.alias+"='"+alias+"' and ";
									}
									tempWhere=tempWhere.replace(/ AND ([^ AND ]*)$/,'$1')+") AND ";
									whereAlias=whereAlias.replace(/ AND ([^ AND ]*)$/,'$1')+") AND ";
									displayWhere=displayWhere.replace(/ and ([^ and ]*)$/,'$1')+") and ";
								}
							}else if(field.data_type=='boolean'){
								var val=values[0];
								if(typeof val=='string'){val=val.toLowerCase();}
								if(val==true || val=='true' || val=='t'){
									tempWhere+=cF.fieldName+"=TRUE AND ";
									whereAlias+=cF.fieldName+"=TRUE AND ";
									displayWhere+=field.alias+"=true and ";
								}else{
									tempWhere+="("+cF.fieldName+"=FALSE OR "+cF.fieldName+"=NULL) AND ";
									whereAlias+="("+cF.fieldName+"=FALSE OR "+cF.fieldName+"=NULL) AND ";
									displayWhere+="("+field.alias+"=false or "+field.alias+"=NULL) and ";
								}
							}
						}else{
							if(values.length){
								tempWhere+="(";
								whereAlias+="(";
								displayWhere+="(";
								for(var y=0;y<values.length;y++){
									val=values[y];
									alias=main.utilities.getAlias(val,field,layerName);
									tempWhere+=cF.fieldName+"='"+val+"' OR ";
									whereAlias+=cF.fieldName+"='"+alias+"' OR ";
									displayWhere+=field.alias+"='"+alias+"' or ";
								}
								tempWhere=tempWhere.replace(/ OR ([^ OR ]*)$/,'$1')+") AND ";
								whereAlias=whereAlias.replace(/ OR ([^ OR ]*)$/,'$1')+") AND ";
								displayWhere=displayWhere.replace(/ or ([^ or ]*)$/,'$1')+") and ";
							}
						}
					}else if(cF.type=="text"){
						tempWhere+=cF.fieldName+" ILIKE '%"+cF.value+"%' AND ";
						whereAlias+=cF.fieldName+" ILIKE '%"+cF.value+"%' AND ";
						displayWhere+=field.alias+" contains '"+cF.value+"' and ";
					}else if(cF.type=="withFile"){
						if(cF.value){
							tempWhere+=cF.fieldName+" IS NOT NULL AND "+cF.fieldName+"!='' AND "+cF.fieldName+"!='[]' AND ";
							whereAlias+=cF.fieldName+" IS NOT NULL AND "+cF.fieldName+"!='' AND "+cF.fieldName+"!='[]' AND ";
							displayWhere+=field.alias+" is not empty";
						}else{
							tempWhere+="("+field.alias+" IS NULL OR "+cF.fieldName+"='' OR "+cF.fieldName+"='[]') AND ";
							whereAlias+="("+field.alias+" IS NULL OR "+cF.fieldName+"='' OR "+cF.fieldName+"='[]') AND ";
							displayWhere+=field.alias+" is empty";
						}
					}else if(cF.type=="range"){
						if(!cF.hasSpecifics){
							minAlias=cF.min;
							maxAlias=cF.max;
							if(field.to_fixed){minAlias=minAlias.toFixed(field.to_fixed);}
							if(field.to_fixed){maxAlias=maxAlias.toFixed(field.to_fixed);}
							tempWhere+="("+cF.fieldName+">="+cF.min+" AND "+cF.fieldName+"<="+cF.max+") AND ";
							whereAlias+="("+cF.fieldName+">="+cF.min+" AND "+cF.fieldName+"<="+cF.max+") AND ";
							displayWhere+="("+field.alias+">="+minAlias+" and "+field.alias+"<="+maxAlias+") and ";
						}else{
							specifics=cF.hasSpecifics.split(',');
							tempWhere+="(";
							whereAlias+="(";
							displayWhere+="(";
							for(var j=0;j<specifics.length;j++){
								if(specifics[j]){
									tempWhere+=cF.fieldName+"='"+specifics[j]+"' OR ";
									whereAlias+=cF.fieldName+"='"+specifics[j]+"' OR ";
									displayWhere+=field.alias+"='"+specifics[j]+"' or ";
								}
							}
							tempWhere=tempWhere.replace(/ OR ([^ OR ]*)$/,'$1')+") AND ";
							whereAlias=whereAlias.replace(/ OR ([^ OR ]*)$/,'$1')+") AND ";
							displayWhere=displayWhere.replace(/ or ([^ or ]*)$/,'$1')+") and ";
						}
					}else if(cF.type=="map_range"){}
					if(cF.showNulls){
						if(tempWhere){
							tempWhere=tempWhere.replace(/ AND ([^ AND ]*)$/,'$1')+" OR ";
							whereAlias=whereAlias.replace(/ AND ([^ AND ]*)$/,'$1')+" OR ";
							displayWhere=displayWhere.replace(/ and ([^ and ]*)$/,'$1')+" or ";
						}
						nullZeroOr='';
						nullBlankOr='';
						if(((field.options && !field.options['0']) || field.lookup_table) && main.misc.numberDTs.indexOf(field.data_type)!=-1 ){nullZeroOr=" OR "+cF.fieldName+"='0'";}
						if(main.misc.textDTs.indexOf(field.data_type)!=-1){nullBlankOr=" OR "+cF.fieldName+"=''";}
						tempWhere+="("+cF.fieldName+"=NULL"+nullZeroOr+nullBlankOr+") AND ";
						whereAlias+=cF.fieldName+"=NULL AND ";
						displayWhere+=field.alias+"=NULL and ";
					}
					tempWhere=tempWhere.replace(/ AND ([^ AND ]*)$/,'$1');
					where+="("+tempWhere+") AND ";
				}
				where=where.replace(/ AND ([^ AND ]*)$/,'$1');
				whereAlias=whereAlias.replace(/ AND ([^ AND ]*)$/,'$1');
				displayWhere=displayWhere.replace(/ and ([^ and ]*)$/,'$1');
			}
			return {
				where:where,
				displayWhere:displayWhere,
				whereAlias:whereAlias
			}
		};
		this.createHTML=function(){
			self.fieldAddSelect=$('<select class="advFFieldAddSelect">');
			self.layerSelect=$('<select class="advFLayerSelect">')
				.append('<option value=""></option>');
			var name;
			var inventories=Object.keys(main.inventories);
			for(var key in self.layers){
				if(inventories.indexOf(key)>-1){
					name=self.layers[key].properties.layer_name || self.layers[key].properties.name;
					self.layerSelect.append('<option value="'+name+'">'+self.layers[key].properties.alias+'</option>');
				}
			}
			self.layerSelect.on('change',function(){
				if($(this).val()){
					self.changeLayer($(this).val());
				}
			});
			self.fieldAddSelect.on('change',function(){
				if($(this).val()){
					var thisFieldName=$(this).val();
					var fields=self.currentLayer.fields;
					var field=fields[thisFieldName];
					$(this).find('option[value="'+thisFieldName+'"]').remove();
					var fieldName,thisFieldNameLC=fields[thisFieldName].alias.toLowerCase();
					var inserted=false;
					for(var t=0;t<self.currentFields.length;t++){
						if((t==0 && fields[self.currentFields[t]].alias.toLowerCase()>thisFieldNameLC) || (fields[self.currentFields[t]].alias.toLowerCase()<thisFieldNameLC && (!self.currentFields[t+1] || fields[self.currentFields[t+1]].alias.toLowerCase()>thisFieldNameLC))){
							// self.fieldsWrap.find('.advFFieldTR.'+self.currentFields[t]).after(self.addField(field));
							self.addField(field);
							self.currentFields.splice(t+1,0,thisFieldName);
							inserted=true;
							break;
						}
					}
					if(!inserted){
						self.addField(field);
						self.currentFields.push(thisFieldName);
					}
				}
			});
			main.utilities.sortOptions(self.layerSelect);
			self.selectToApplies=$('<div class="selectToAppliesWrap">');
			self.selectToAppliesButton=new Button('std','selectToAppliesButton','Modify');
			self.selectToAppliesPanel=new Panel({
				content:self.selectToApplies,
				title:'Apply Filter To',
				matchHeight:true,
				relativeTo:main.layout.mapContainerWrap,
				classes:'selectToAppliesPanel',
				hideOnOutClick:true,
				centerOnOrientationChange:true
			});
			self.selectToAppliesButton.html.on('click',function(){
				self.selectToAppliesPanel.open();
			});
			self.addApplyTo('dashboard','Dashboard',true);
			self.addApplyTo('map','Map',true);
			self.addApplyTo('data_table','Data Tables',true);
			self.addApplyTo('filter_legend','Filter Legend',true);
			self.addApplyTo('export','Exports',true);
			// self.addApplyTo('report','Reports',true);
			self.selectFieldWrap=$('<div class="advFSelectorsItemWrap cf">')/* noDec */
				.append($('<div class="advFSelectorsItemLabel advFSelectorsItemWrapEle">').append('Select Layer'))
				.append($('<div class="advFSelectorsItem advFSelectorsItemWrapEle">').append(self.layerSelect))
			self.addFieldAdvF=$('<div class="advFSelectorsItemWrap cf none">')
				.append($('<div class="advFSelectorsItemLabel advFSelectorsItemWrapEle">').append('Add Field'))
				.append($('<div class="advFSelectorsItem advFSelectorsItemWrapEle">').append(self.fieldAddSelect))
			self.selectToAppliesWrap=$('<div class="advFSelectorsItemWrap cf none">')
					.append($('<div class="advFSelectorsItemLabel advFSelectorsItemWrapEle">').append('Apply To'))
					.append($('<div class="advFSelectorsItem advFSelectorsItemWrapEle">').append(self.selectToAppliesButton.html))
			self.advFSelectors=$('<div class="advFSelectors aFTopsWrapEle">')
				.append(self.selectFieldWrap)
				.append(self.addFieldAdvF)
				// .append(self.selectToAppliesWrap);
			self.clearFilterButton=new Button('std','clearFilterButton advFButtonsTopB','Clear Filters');
			self.applyButton=new Button('std','advFApplyButton advFButtonsTopB','Apply');
			self.resetButton=new Button('std','resetButton advFButtonsTopB','Reset');
			self.selectFromMapButton=new Button('std','selectFromMapButton advFButtonsTopB','Filter By Map');
			self.togglePolyVisButton=new Button('std','togglePolyVisButton advFButtonsTopB none','Toggle Polygon Visibility');
			self.clearSelectFromMap=new Button('std','clearSelectFromMapButton advFButtonsTopB none','Clear Filter By Map');
			self.advFTopBs=$('<div class="advFTopBs aFTopsWrapEle none">')
				.append(self.applyButton.html)
				.append(self.clearFilterButton.html)
				.append(self.selectFromMapButton.html)
				.append(self.togglePolyVisButton.html)
				.append(self.clearSelectFromMap.html);
				// .append(self.resetButton.html);
			self.selectFromMapButton.html.on('click',function(){
				if(main.invents.currentInvent==self.currentLayer.properties.name){
					if(!self.selectingForGeo){
						self.startSelectFromMap();
						self.panel.minimizePanel();
					}else{
						if(self.hasDrawnOne){
							// self.toggleSelectPolyVis();
						}else{self.stopSelectFromMap();}
					}
				}else{
					alert('Change layer to '+main.invents.inventories[self.currentLayer.properties.name].plural_alias+' before you may filter.');
				}
			});
			self.togglePolyVisButton.html.on('click',function(){
				self.toggleSelectPolyVis();
			});
			self.clearSelectFromMap.html.on('click',function(){
				self.clearMapFilters();
				// self.sideReset();
			});
			self.applyButton.html.on('click',function(){
				if(main.invents.currentInvent==self.currentLayer.properties.name){
					main.layout.loader.loaderOn();
					self.filter.currentFilters={};
					// self.fieldsWrap.find('.advFFieldTR').each(function(){
					self.fieldsAccordian.container.find('.actChkShelf').each(function(){
						if($(this).find('.applyAdvFilterChk').prop('checked')){
							if($(this).find('.rangeInputPkg').length>0){
								self.filterRangeChange(self.ranges[$(this).find('.rangeOuterWrap').attr('id')].getRangePkg(),true);
							}else if($(this).find('.chkPkg').length>0){
								self.filterCheckChange($(this).find('.inputPkg').data('name'),$(this).find('.chkPkg'),true);
							}else if($(this).find('.afLookupPkg').length>0){
								self.filterLUChange($(this).find('.inputPkg').data('name'),$(this).find('.afLookupPkg'),true);
							}else if($(this).find('.textInputPkg').length>0){
								self.filterTextChange($(this).find('.inputPkg').data('name'),$(this).find('.textInput').val(),$(this).find('.inputPkg'),true);
							}else if($(this).find('.fileInputPkg').length>0){
								self.fileCheckChange($(this).find('.inputPkg').data('name'),$(this).find('.filterFileCheck').prop('checked'),$(this).find('.inputPkg'),true);
							}
						}
					});
					if(main.withs.with_ecobens && main.withs.with_ecobens.value){
						self.ecosAccordian.container.find('.actChkShelf').each(function(){
							if($(this).find('.applyAdvFilterChk').prop('checked')){
								if($(this).find('.rangeInputPkg').length>0){
									self.filterRangeChange(self.ranges[$(this).find('.rangeOuterWrap').attr('id')].getRangePkg(),true);
								}
							}
						});
					}
					var filterWheres=self.getFitlerWhere(self.filter.currentFilters,self.currentLayer.fields,self.currentLayer.properties.name);
					self.where=filterWheres.where;
					self.displayWhere=filterWheres.displayWhere;
					self.whereAlias=filterWheres.whereAlias;
					main.layout.removeAllPopUps();
					// if(self.appliesTo['map'].apply){
						main.loader.loadMoreMapPoints(self.currentLayer.properties.layer_name);
					// }else{main.layout.loader.loaderOff();}
					self.setWhereThings();
					// self.panel.minimizePanel();
					if(self.appliesTo['data_table'].apply && main.dataTables[self.currentLayer.properties.name] && main.dataTables[self.currentLayer.properties.name].dataContentWrap){
						if(main.dataTables[self.currentLayer.properties.name].dataContentWrap.is(':visible')){
							main.dataTables[self.currentLayer.properties.name].sideReset();
						}else{
							main.dataTables[self.currentLayer.properties.name].needsRefresh=true;
						}
					}
					if(self.appliesTo['filter_legend'].apply && main.filter_legend && main.filter_legend.active){main.filter_legend.refresh();}
					if(self.appliesTo['dashboard'].apply && main.dashboard && main.dashboard.active){main.dashboard.refresh();}
				}else{
					alert('Change layer to '+main.invents.inventories[self.currentLayer.properties.name].plural_alias+' before you may filter.');
				}
			});
			self.clearFilterButton.html.on('click',function(){
				self.clearFilters();
			});
			self.resetButton.html.on('click',function(){
				self.reset();
			});
			self.viewMore=new ViewMore({
				prev:'Use this tool to apply a filter to several tools in the app.',
				content:'<ul><li>Step 1: Select a layer to apply the filters to.</li><li>Step 2: Optionally, add a field.</li><li>Step 3: Change the filter parameters. Click "Apply" to apply the properties filter.</li><li>Step 4: Clear a properties filter by clicking the "Clear Filter" button.</li><li>Step 5: Apply a map filter.</li><li>Step 6: If a map filter is applied, modify the filter polygon by clicking and dragging a vertex or edge. Toggle the filter polygon visibility by clicking the "Filter By Map" button. Clear a map filter by clicking the "Clear Filter By Map" button.</li></ul>'
			});
			self.whereShowContent=$('<span class="advFWhereShowContent">');
			self.whereShowLabel=$('<span class="advFWhereShowLabel">').html('Current Filter: ');
			self.whereShow=$('<div class="advFWhereShow aFTopsWrapEle none">')
				.append(self.whereShowLabel)
				.append(self.whereShowContent);
			self.topTop=$('<div class="topTop aFTopsWrapEle">')
				.append(self.viewMore.html);
			self.topsWrap=$('<div class="aFTopsWrap">')
				.append(self.topTop)
				.append(self.advFSelectors)
				.append(self.advFTopBs)
				.append(self.whereShow)
			self.fieldsAccordian=new Accordian('afFields','abc');
			self.ecosAccordian=new Accordian('afEcos','abc');
			// self.fieldsWrap=$('<tbody class="advFFields">')
			// self.table=$('<table border="1" class="advFTable">')
				// .append(self.fieldsWrap);
			if(main.withs.with_ecobens && main.withs.with_ecobens.value && self.with_ecobens){
				self.tabs=new Tabs();
				self.fieldsTab=self.tabs.addTab('Fields',self.fieldsAccordian.container,'fieldsTab');
				self.ecoBensTab=self.tabs.addTab('Eco-Benefits',self.ecosAccordian.container,'ecoBensTab');
				self.tabs.setActiveTab(self.fieldsTab.tabid);
				self.fieldsHTML=self.tabs.html;
			}else{
				self.fieldsHTML=self.fieldsAccordian.container;
			}
			self.html=$('<div class="advancedFilterWrapHTML none">')
				// .append(self.table);
				.append(self.fieldsHTML);
			self.panel=new Panel({
				content:self.html,
				topContent:self.topsWrap,
				title:self.alias,
				matchHeight:true,
				relativeTo:main.layout.mapContainerWrap,
				classes:'advFilterPanel',
				centerOnOrientationChange:true,
				onShow:self.onPanelOpen,
				onClose:self.onPanelClose
			});
			self.panel.topContentWrap.css('margin-bottom','0px');
			self.panel.body.addClass('none');
		};
		this.checkAppliedToos=function(){
			if(self.currentLayer.properties.layer_name==main.invents.currentInvent){main.loader.loadMoreMapPoints(self.currentLayer.properties.layer_name);	}
			if(self.appliesTo['data_table'].apply && main.dataTables[self.currentLayer.properties.name] && main.dataTables[self.currentLayer.properties.name].dataContentWrap){
				if(main.dataTables[self.currentLayer.properties.name].dataContentWrap.is(':visible')){
					main.dataTables[self.currentLayer.properties.name].sideReset();
				}else{
					main.dataTables[self.currentLayer.properties.name].needsRefresh=true;
				}
			}
			if(self.appliesTo['filter_legend'].apply && main.filter_legend && main.filter_legend.active){main.filter_legend.refresh();}
			if(self.appliesTo['dashboard'].apply && main.dashboard && main.dashboard.active){main.dashboard.refresh();}
		};
		this.clearMapFilters=function(){
			main.layout.loader.loaderOn();
			if(self.selectingForGeo){self.stopSelectFromMap();}
			self.geoFilter=null;
			self.checkAppliedToos();
			self.panel.setPanelParams();
		};
		this.clearFilters=function(){
			main.layout.loader.loaderOn();
			self.hardClearFilter();
			self.checkAppliedToos();
			self.panel.setPanelParams();
		};
		(this.init=function(){
			self.createHTML();
			if(main.map.mapTools){
				self.mapToolButtonClicked=function(){
					if(!self.panel.opened){self.panel.open();
					}else if(self.panel.minimized){self.panel.maximizePanel();
					}else{self.panel.close();}
				};
				main.map.mapTools.addItem('advFilter','<img draggable="false" class="mapToolImg" src="'+main.mainPath+'images/filter_white.png">',self.mapToolButtonClicked,self.alias);
			}
		})();
	};
});
