define(function(require){
	return function pg(args){
		var self=this;
		this.hasSecs=false;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.name=this.name || '';
		this.checked=this.checked || false;
		this.label=this.label || '';
		this.classes=this.classes || '';
		this.id=this.id || '';
		this.data=this.data || [];
		this.disabled=this.disabled || '';
		this.readonly=this.readonly || '';
		this.title=this.title || '';
		this.wrapClasses=this.wrapClasses || '';
		if((this.value || this.value==0) && !isNaN(Number(this.value))){this.value=Number(this.value);
		}else{this.value=null;}
		this.dontUpdateInputs=false;
		
		this.updateShowInputs=function(){
			var timeData;
			if(timeData=main.utilities.msToTime(self.valInput.val())){
				self.hourInput.val(timeData.hours);
				self.minInput.val(main.utilities.addLeadingZero(timeData.mins));
				if(self.hasSecs){self.secInput.val(main.utilities.addLeadingZero(timeData.secs));}
				self.ampmSelect.val(timeData.ampm);
			}
		};
		(this.create=function(){
			var value=self.value;
			if(!value && value!=0){value='';}
			var hours='';
			var mins='';
			var secs='';
			var ampm='';
			if(value || value==0){
				var timeData=main.utilities.msToTime(value);
				var hours=timeData.hours;
				var mins=timeData.mins;
				var secs=timeData.secs;
				var ampm=timeData.ampm;
			}
			self.valInput=$('<input type="hidden" name="'+self.name+'" value="'+value+'">').addClass('timeValInput '+self.classes);
			self.hourInput=$('<input type="number" class="timeHourInput timeInputEle timeInputItem timeNumItem" max="12" min="0" value="'+hours+'">');
			self.minInput=$('<input type="number" class="timeMinInput timeInputEle timeInputItem timeNumItem" max="59" min="0" value="'+main.utilities.addLeadingZero(mins)+'">');
			self.ampmSelect=$('<select class="timeAMPMSelect timeInputEle timeInputItem">')
				.append('<option value=""></option selected="selected">')
				.append('<option value="AM">AM</option>')
				.append('<option value="PM">PM</option>');
			self.ampmSelect.val(ampm);
			self.html=$('<div>').addClass('timeInputWrap cf')
				.append(self.valInput)
				.append(self.hourInput)
				.append('<div class="timeInputEle timeInterval">:</div>')
				.append(self.minInput)
			if(self.hasSecs){
				self.secInput=$('<input type="number" class="timeSecInput timeInputEle timeInputItem timeNumItem" max="59" min="0" value="'+main.utilities.addLeadingZero(secs)+'">');
				self.html
					.append('<div class="timeInputEle timeInterval">:</div>')
					.append(self.secInput);
			}
			self.html
				.append('<div class="timeInputEle timeInterval">&nbsp;</div>')
				.append(self.ampmSelect);
			if(self.wrapClasses){self.html.addClass(self.wrapClasses);}
			self.valInput.on('change',function(){
				if(!self.dontUpdateInputs){self.updateShowInputs();}
			});
			self.html.on('change','.timeInputItem',function(){
				var val=$(this).val();
				if($(this).hasClass('timeHourInput')){val=main.utilities.addLeadingZero(val);}
				$(this).val(val);
				var val=self.hourInput.val()+':'+self.minInput.val();
				if(self.hasSecs){val+=':'+self.secInput.val();}
				if(self.ampmSelect.val()){val+=' '+self.ampmSelect.val();}
				self.dontUpdateInputs=true;
				self.valInput.val(main.utilities.timeToMS(val)).change();
				self.dontUpdateInputs=false;
			});
			if(self.onChange){
				self.valInput.on('change',function(){
					$(this).val(main.utilities.addLeadingZero($(this).val()));
				});
			}
			if(self.data.length>0){
				for(var i=0;i<self.data.length;i++){
					self.html.data(self.data[i][0],self.data[i][1])
					self.valInput.data(self.data[i][0],self.data[i][1])
				}
			}
			if(self.disabled){self.valInput.prop('disabled',true);}
			if(self.readonly){self.valInput.prop('readonly',true);}
			if(self.title){self.valInput.attr('title',self.title);}
		})();
	};
});