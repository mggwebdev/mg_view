define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Checkbox=require('./Checkbox');
		var Input=require('./Input');
		var MassUpdate=require('./MassUpdate');
		var Panel=require('./Panel');
		var Range=require('./Range');
		var Report=require('./Report');
		var Tabs=require('./Tabs');
		var ViewMore=require('./ViewMore');
		var Wait=require('./Wait');
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.rows={};
		this.transferToTable=null;
		this.transferToPIDs=null;
		this.imgPath='images/';
		if(this.caller.is_inventory){
			this.fields=main.inventories[this.table].fields;
		}else{
			this.fields=main.forms[this.table].fields;
			// if(this.table){this.sortedFieldKeys=main.forms[this.table].appForm.sortedFieldKeys;}
		}
		// if(!this.sortedFieldKeys){
			// this.sortedFieldKeys=main.utilities.sortFields(this.fields,'place_in_order');
		this.sortedFieldKeys=main.utilities.sortDTFields(this.fields,'place_in_order');
		// }
		if(this.callerProperties){
			if(this.callerProperties.tabs){
				self.dbTabs=this.callerProperties.tabs;
			}
			if(this.callerProperties.dt_sticky_columns){
				self.stickyColumns=this.callerProperties.dt_sticky_columns;
			}
			if(this.callerProperties.dt_row_printable){this.printable=true;}
		}
		if(main.tables[self.table].properties.is_wom){this.printable=false;}
		this.needsRefresh=false;
		this.scrolling=false;
		this.coordinates={};
		this.ranges={};
		this.sortDataKeys=[];
		this.defaultSortOrder='DESC';
		this.currentSortOrder=this.defaultSortOrder;
		this.noFilterValuesMsg='No filterable values';
		this.primaryField;
		this.loading=false;
		this.blockScrollE=false;
		this.advFilterApplied=false;
		this.initiallyLoaded=false;
		this.dontLoadOnOpen=false;
		this.loaded=false;
		this.showingAllFields=false;
		this.shortLoadFields=['pid','season_planted','tt_group_name'];
		this.maxResultsPerPage=50;
		this.limitResultsToMax=false;
		this.initialStartIndex=0;
		this.actualStartIndex=this.initialStartIndex;
		this.paginateAmt=100;
		this.withScrollLoad=true;
		this.tabColorIDX=0;
		this.activeRow=null;
		this.sortOptions={
			'DESC':{
				value:'DESC',
				alias:'Descending',
			},
			'ASC':{
				value:'ASC',
				alias:'Ascending',
			}
		};
		this.dragging={
			mousemoved:false,
			mousemoving:false,
			mousedown:false,
			initX:null,
			initY:null,
		};
		if(main.browser.isMobile){this.maxResultsPerPage=20;}
		this.signatures={}
		this.waitingForLookUpTables={};
		this.waitingForLookUpTablesFilter={};
		this.filter={
			currentFilters:{},
			currentRows:[]
		}
		this.tas={};
		this.inTableTest=function(key,shortLoad){
			if(self.fields[key] && self.fields[key].active && self.fields[key].in_data_table && !self.fields[key].in_table && self.fields[key].input_type!='html'){
				return true;
			}
			return false;
		};
		if(!this.primaryField){
			if(this.callerProperties && this.callerProperties.internal_field_map && this.callerProperties.internal_field_map.last_modified){
				this.primaryField=this.callerProperties.internal_field_map.last_modified;
			}else{
				for(var i=0;i<this.sortedFieldKeys.length;i++){
					if(this.inTableTest(this.sortedFieldKeys[i])){
						this.primaryField=this.sortedFieldKeys[i];
						break;
					}
				}
			}
		}
		if(!this.primaryField){this.primaryField='pid';}
		if(this.callerProperties && this.callerProperties.sort_by){this.currentSortBy=this.callerProperties.sort_by;
		}else{this.currentSortBy=this.primaryField;}
		this.addNewFromDT=function(){
			self.caller.startAddingInit();
		};
		this.currentPIDsCheck=function(){
			if(self.lastLoadedPossiblePids){
				return true;
			}
		};
		this.loadPrev=function(){
			if(self.lastLoadedPossiblePids){
				if(self.prevIndex || self.prevIndex==0){var nextIndex=self.prevIndex;
				}else{var nextIndex=self.actualStartIndex;}
				var paginateAmt=Number(self.limitInput.val()) || self.paginateAmt;
				var newIndex=nextIndex-paginateAmt;
				if(newIndex<0){
					newIndex=self.lastLoadedPossiblePids.length-paginateAmt;
					if(newIndex<0){newIndex=0;}
				}
				self.indexStartInput.val(newIndex+1);
				self.loadFeatures();
			}else{
				if(!self.loadPrevWait){self.loadPrevWait=new Wait(self.currentPIDsCheck,self.loadPrev);}
			}
		};
		this.loadNext=function(){
			if(self.lastLoadedPossiblePids){
				var newIndex=self.actualStartIndex;
				if(self.actualStartIndex>self.lastLoadedPossiblePids.length-1){var newIndex=0;}
				self.indexStartInput.val(newIndex+1);
				self.loadFeatures();
			}else{
				if(!self.loadNextWait){self.loadNextWait=new Wait(self.currentPIDsCheck,self.loadNext);}
			}
		};
		this.addMoreRecords=function(dir){
			if(!self.loading){
				if(!isNaN(self.limitInput.val())){var limit=Number(self.limitInput.val());
				}else{var limit=self.maxResultsPerPage;}
				var toGet=Math.ceil(limit/2);
				var pids=[];
				if(toGet){
					if(dir=='prev' && self.lastLoadedFirstItem!=0){
						var firstIdx=self.lastLoadedFirstItem-toGet;
						if(firstIdx<0){firstIdx=0;}
						var pids=self.lastLoadedPossiblePids.slice(firstIdx,self.lastLoadedFirstItem);
					}else if(dir=='post' && (self.lastLoadedLastItem+1)!=self.lastLoadedPossiblePids.length){
						var lastIdx=self.lastLoadedLastItem+toGet+1;
						if(lastIdx>self.lastLoadedPossiblePids.length){lastIdx=self.lastLoadedPossiblePids.length;}
						var pids=self.lastLoadedPossiblePids.slice(self.lastLoadedLastItem+1,lastIdx);
					}
				}
				if(pids.length){self.loadFeatures({pids:pids,dir:dir});}
			}
		};
		this.dataTableWrapScrollWheel=function(e){
			if(self.dataTableTable.outerHeight(true)<=self.dataTableWrap.height() && self.tbody.find('.tpdataTableRow').length!=0){
				if(e.originalEvent.wheelDelta>0){self.addMoreRecords('prev');
				}else{self.addMoreRecords('post');}
			}
		};
		this.dataTableWrapScroll=function(e){
			if(self.tbody.find('.tpdataTableRow').length!=0){
				if(!self.blockScrollE){
					if(self.dataTableWrap.scrollTop()==0){
						self.addMoreRecords('prev');
					}else if(self.dataTableTable.outerHeight(true)-self.dataTableWrap.height()==self.dataTableWrap.scrollTop()){
						self.addMoreRecords('post');
					}
				}else{
					self.blockScrollE=false;
				}
			}
		};
		this.sideReset=function(loadFeaturesParams){
			loadFeaturesParams=loadFeaturesParams || null;
			self.partReset();
			self.loadFeatures(loadFeaturesParams);
		};
		this.add_exporter=function(){
			if(main.withs.with_exporter && main.withs.with_exporter.value && main.withs.with_exporter[main.account.loggedInUser.user_type+'_with'] && self.caller.properties.exportable){
				self.exportCheckedButton=new Button('std','dtButton dtExportButton dtBotButton generateDTReport','Export');
				self.dtButtons.append(self.exportCheckedButton.html);
				self.exportCheckedButton.html.on('click',function(){
					var pids=self.getCheckedPIDs();
					if(pids.length>0){
						main.dataTableFilter=pids;
						main.exporter.exportFromDT(self.table);
					}else{
						alert('Please check the '+(self.callerProperties.plural_alias?self.callerProperties.plural_alias.toLowerCase():'rows')+' you want to export.');
					}
				});
			}
		};
		this.remove_exporter=function(){
			if((!main.withs.with_exporter || !main.withs.with_exporter.value || !main.withs.with_exporter[main.account.loggedInUser.user_type+'_with']) && self.exportCheckedButton){
				self.exportCheckedButton.html.remove();
				delete self.exportCheckedButton;
			}
		};
		this.sidePanelScrollToBottomOfDT=function(){
			var ref=self.html.closest('.sidePanelContent');
			var bottomHeight=parseInt(ref.css('padding-bottom'))+ref.closest('.sidePanel').find('.sPTopButtons').outerHeight(true);
			var bottomPos=self.dataTablePkgInnerWrap.offset().top+self.dataTablePkgInnerWrap.outerHeight()+bottomHeight;
			var htmlHeight=$('html').height();
			if(bottomPos>htmlHeight){
				var margin=6;
				var newBottomPos=htmlHeight-bottomHeight-margin;
				var deltaY=newBottomPos-bottomPos;
				var scrollTop=ref.scrollTop();
				var newScrollTop=scrollTop-deltaY;
				ref.scrollTop(newScrollTop);
			}
		};
		this.opened=function(){
			if(main.customReports){
				if(main.customReports[self.table] && Object.keys(main.customReports[self.table]).length){
					self.refreshCustomReportsWrap();
					self.customReportsWrap.removeClass('none');
				}else{self.customReportsWrap.addClass('none');}
			}
			// self.html.closest('.sidePanelContent').on('scroll',self.sidePanelScroll);
		};
		this.closed=function(){
			// self.html.closest('.sidePanelContent').off('scroll',self.sidePanelScroll);
		};
		this.refreshCustomReportsWrap=function(){
			self.customReportsSelect.html('<option></option>');
			for(var key in main.customReports[self.table]){
				self.customReportsSelect.append('<option value="'+main.customReports[self.table][key].val+'">'+main.customReports[self.table][key].alias+'</option>');
			}
		};
		this.checkIfAlreadyAdded=function(params){
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						checkField:params.checkField,
						pid:params.pid,
						transferToTable:params.table,
						table:self.table
					},
					action:'checkIfAdded'
				},
				success:function(response){
					if(response){
								console.log(response);
						response=JSON.parse(response);
								console.log(response);
						if(response.status=="OK"){
							if(!response.count){
								console.log(response);
								self.transferTo({context:self,pids:[response.params.pid],table:response.params.transferToTable});
							}else{
								alert('This record has already been added. See reference in the '+self.fields[response.params.checkField].alias+' column');
							}
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.createHTML=function(){
			self.stickyColumnsTbody=$('<tbody>').addClass('dtStickyColumnsTbody');
			self.stickyColumnsTable=$('<table border="1">').addClass('dtStickyCols').append(self.stickyColumnsTbody);
			self.dtStickyColsWrap=$('<div class="dtStickyColsWrap">').append(self.stickyColumnsTable);
			self.tbody=$('<tbody>').addClass('mainTbody');
			self.tableLabels=$('<tbody>').addClass('tableLabels');
			self.tableTabLabels=$('<tr>');
			self.stickyTableHeaderLabels=$('<tr>');
			self.stickyLabelsTHead=$('<thead>');
			self.stickyTableHeader=$('<table border="1">').addClass('stickyDTHeader dataTableHeader')
				.append(self.stickyLabelsTHead.append(self.stickyTableHeaderLabels));
			self.tableHeaderLabels=$('<tr>');
			self.labelsTHead=$('<thead>');
			self.tableHeader=$('<table border="1">').addClass('mainDataTableHeader dataTableHeader')
				.append(self.labelsTHead.append(self.tableHeaderLabels));
			self.dtButtons=$('<div>').addClass('dtButtons cf');
			self.add_exporter();
			self.deleteCheckedButton=new Button('std','dtButton deleteCheckedBot dtBotButton generateDTReport','Delete');
			self.dtButtons.append(self.deleteCheckedButton.html);
			/* if(self.callerProperties.with_reports){
				self.exportButton=new Button('std','dtButton dtExport dtBotButton','Export');
				// self.dtButtons.append(self.exportButton.html);
				self.exportButton.html.on('click',function(){
					var records={};
					self.html.find('.dtRCheckB:checked').each(function(i,ele){
						if(self.rows[ele.value])records[ele.value]=self.rows[ele.value];
					});
					// var isExportAll=false;
					// if(Object.keys(records).length==0){isExportAll=true;}
					if(Object.keys(records).length>0){
						var report=new Report({
							records:records,
							callerContext:self,
							with_export:self.callerProperties.with_export,
							launchReportButton:self.exportButton.html,
							isExport:true,
							// isExportAll:isExportAll
						});
					}else{
						alert('Select one or more rows to report.');
					}
				});
				// self.generateReportButton=new Button('std','dtButton generateDTReport dtBotButton','Report');
				// self.dtButtons.append(self.generateReportButton.html);
				// if(!main.data.personal_settings.reports || Object.keys(main.data.personal_settings.reports).length==0){self.generateReportButton.html.addClass('none noReports');}
				// self.generateReportButton.html.on('click',function(){
					// var records={};
					// self.html.find('.dtRCheckB:checked').each(function(i,ele){
						// if(self.rows[ele.value])records[ele.value]=self.rows[ele.value];
					// });
					// /* if(self.callerProperties.short_load){
						// if(!self.lastLoadedResults){return;}
						// self.getFullRecords(Object.keys(records));
						// for(var key in records){
							// records[key]=self.rows[key];
						// }
					// }
					// if(Object.keys(records).length>0){
						// var report=new Report({
							// records:records,
							// callerContext:self,
							// launchReportButton:self.generateReportButton.html,
							// with_export:self.callerProperties.with_export
						// });
						// report.showTypeSelect();
					// }else{
						// alert('Select one or more rows to report.');
					// }
				// });
			}  */
			self.top=$('<div>').addClass('dtTop cf')
			self.expandButton=$('<div>').addClass('expand topButton').append('<img src="'+main.mainPath+'images/expand.png" class="topButtonImg"/>');
			self.dtTopButtonsWrap=$('<div>').addClass('dtTopButtonsWrap topEle cf')
				.append(self.expandButton);
			self.dtBottomScrollBar=$('<div class="dtBottomScrollBar">');
			self.dataTableWrap=$('<div>').addClass('dataTableWrap tableEleWrap')
			self.dataTableTable=$('<table>').attr('border','1').addClass('tpdataTable');
			self.dataTablePkgInnerWrap=$('<div>').addClass('dataTablePkgInnerWrap');
			self.dataTablePkgInnerWrapWrap=$('<div>').addClass('dataTablePkgInnerWrapWrap').append(self.dataTablePkgInnerWrap);
			self.stickyHeaderWrap=$('<div>').addClass('tableHeaderWrap tableEleWrap stickyHeaderWrap');
			self.stickyColumnsWrap=$('<div>').addClass('stickyColumnsWrap tableEleWrap')
				.append(self.stickyHeaderWrap
					.append(self.stickyTableHeader)
				)
				.append(self.dtStickyColsWrap);
			self.tableHeaderWrap=$('<div>').addClass('tableHeaderWrap tableEleWrap')
					.append(self.tableHeader)
			self.dataTablePkgInnerWrap
				.append(self.tableHeaderWrap
				).append(self.stickyColumnsWrap
				).append(self.dataTableWrap
					.append(self.dataTableTable
						.append(self.tbody)
					)
				).append(self.dtBottomScrollBar);
			// self.addNewButton=$('<div>').addClass('caller')
			if(self.withScrollLoad){
				self.dataTableWrap.on('scroll',self.dataTableWrapScroll);
				self.dataTableWrap.on('wheel',self.dataTableWrapScrollWheel);
			}
			self.loadFilter=$('<div class="dtLoadFilter cf">');
			self.loadPrevSet=$('<div class="loadPrevSet changePagButton" title="Load Previous Set">');
			self.loadNextSet=$('<div class="loadNextSet changePagButton" title="Load Next Set">');
			self.pageDetailsCurrentCount=$('<span class="pageDetailsCurrentCount">0</span>');
			self.pageDetailsOutOfCount=$('<span class="pageDetailsOutOfCount">0</span>');
			self.pageFilterDetailsHTML=$('<span class="pageFilterDetailsHTML">');
			self.pageOrderedByDetailsHTML=$('<span class="pageOrderedByDetailsHTML">');
			self.advFIsActive=$('<span class="advFIsActive">');
			self.pageDetailsHTML=$('<div class="pageDetailsHTML">')
				.append('Displaying ')
				.append(self.pageDetailsCurrentCount)
				.append(' out of ')
				.append(self.pageDetailsOutOfCount)
				.append(' records.')
				.append(self.pageFilterDetailsHTML)
				.append(self.pageOrderedByDetailsHTML)
				.append(self.advFIsActive);
			self.pageDetailsWrap=$('<div class="pageDetailsWrap">').html(self.pageDetailsHTML);
			self.pagButtonsChangeWrap=$('<div class="pagButtonsChangeWrap cf">')
				.append(self.loadPrevSet)
				.append(self.loadNextSet);
			self.pagButtonsWrap=$('<div class="pagButtonsWrap dtSortWrap cf">')
				.append(self.pagButtonsChangeWrap);
			self.loadPrevSet.on('click',function(){
				self.loadPrev();
			});
			self.loadNextSet.on('click',function(){
				self.loadNext();
			});
			self.indexStartLabel=$('<div class="indexStartLabel dtSortEle dtSortLabel">First Record:&nbsp;</div>');
			self.limitLabel=$('<div class="limitLabel dtSortEle dtSortLabel">Limit:&nbsp;</div>');
			self.indexStartInput=$('<input type="number" value="'+(self.actualStartIndex+1)+'" min="1" class="indexStartInput dtSortEle dtSortSelector">');
			self.limitInput=$('<input type="number" value="'+self.maxResultsPerPage+'" min="0" class="limitInput dtSortEle dtSortSelector">');
			self.indexStartWrap=$('<div class="indexStartWrap dtSortWrap">')
				.append(self.indexStartLabel)
				.append(self.indexStartInput);
			self.indexEndWrap=$('<div class="indexEndWrap dtSortWrap">')
				.append(self.limitLabel)
				.append(self.limitInput);
			self.orderBySelector=$('<select class="orderBySelector dtSortEle dtSortSelector">');
			self.orderBySelector2=$('<select class="orderBySelector dtSortEle dtSortSelector">');
			self.orderBySelector3=$('<select class="orderBySelector dtSortEle dtSortSelector">');
			self.orderBySelector4=$('<select class="orderBySelector dtSortEle dtSortSelector">');
			var selected,descOption;
			for(var key in self.sortOptions){
				selected='';
				if(key==self.currentSortOrder){selected=' selected="selected"';}
				self.orderBySelector.append('<option value="'+key+'"'+selected+'>'+self.sortOptions[key].alias+'</option>');
				self.orderBySelector2.append('<option value="'+key+'">'+self.sortOptions[key].alias+'</option>');
				self.orderBySelector3.append('<option value="'+key+'">'+self.sortOptions[key].alias+'</option>');
				self.orderBySelector4.append('<option value="'+key+'">'+self.sortOptions[key].alias+'</option>');
			}
			self.fieldSelecter=$('<select class="dtFieldSelecter dtSortEle dtSortSelector">');
			self.fieldSelecter2=$('<select class="dtFieldSelecter dtSortEle dtSortSelector">');
			self.fieldSelecter3=$('<select class="dtFieldSelecter dtSortEle dtSortSelector">');
			self.fieldSelecter4=$('<select class="dtFieldSelecter dtSortEle dtSortSelector">');
			self.dtSortTableHead=$('<thead>')
				.append($('<tr>')
					.append($('<th colspan="5">').html('Sort Options')))
				.append($('<tr>')
					.append($('<th>').html('&nbsp;'))
					.append($('<th>').html('1'))
					.append($('<th>').html('2')))
			self.dtSortTable=$('<table class="sortOptionsTable">')
				.append(self.dtSortTableHead)
				.append($('<tbody>')
					.append($('<tr>')
						.append($('<td>').html('Field'))
						.append($('<td>').html(self.fieldSelecter))
						.append($('<td>').html(self.fieldSelecter2)))
					.append($('<tr>')
						.append($('<td>').html('Order'))
						.append($('<td>').html(self.orderBySelector))
						.append($('<td>').html(self.orderBySelector2))))
			self.dtAddSort=new Button('std','dtAddSort','Add Sort Option');
			self.dtAddSort.html.on('click',function(){
				if(self.dtSortTableHead.find('tr:last-child th').length==3){
					self.dtSortTableHead.find('tr:last-child').append($('<th>').html('3'));
					self.dtSortTable.find('tbody tr:first-child').append($('<td>').html(self.fieldSelecter3));
					self.dtSortTable.find('tbody tr:last-child').append($('<td>').html(self.orderBySelector3));
				}else if(self.dtSortTableHead.find('tr:last-child th').length==4){
					self.dtSortTableHead.find('tr:last-child').append($('<th>').html('4'));
					self.dtSortTable.find('tbody tr:first-child').append($('<td>').html(self.fieldSelecter4));
					self.dtSortTable.find('tbody tr:last-child').append($('<td>').html(self.orderBySelector4));
				}else if(self.dtSortTableHead.find('tr:last-child th').length>4){
					self.dtAddSort.html.detach();
				}
			});
			self.dtSortButtons=$('<div class="dtSortButtons">')
				.append(self.dtAddSort.html);
			self.fieldSelecter.append('<option value=""></option>');
			self.fieldSelecter2.append('<option value=""></option>');
			self.fieldSelecter3.append('<option value=""></option>');
			self.fieldSelecter4.append('<option value=""></option>');
			var key,selected,aAlias,bAlias;
			var abcKeys=[];
			for(var u=0;u<self.sortedFieldKeys.length;u++){abcKeys.push(self.sortedFieldKeys[u]);}
			var abcKeys=abcKeys.sort(function(a,b){
				aAlias=self.fields[a].alias.toLowerCase();
				bAlias=self.fields[b].alias.toLowerCase();
				if(aAlias>bAlias){return 1;}
				if(aAlias<bAlias){return -1;}
				return 0;
			});
			for(var a=0;a<abcKeys.length;a++){
				key=abcKeys[a];
				if(self.fields[key] && self.fields[key].active && self.fields[key].in_data_table && !self.fields[key].in_table && self.fields[key].input_type!='html'){
					if(key==self.currentSortBy){selected=' selected="selected"';}else{selected='';}
					self.fieldSelecter.append('<option value="'+key+'"'+selected+'>'+self.fields[key].alias+'</option>');
					self.fieldSelecter2.append('<option value="'+key+'">'+self.fields[key].alias+'</option>');
					self.fieldSelecter3.append('<option value="'+key+'">'+self.fields[key].alias+'</option>');
					self.fieldSelecter4.append('<option value="'+key+'">'+self.fields[key].alias+'</option>');
				}
			}
			self.sortOptionsWrap=$('<div class="dtSortOptionsWrap">')
				.append(self.dtSortTable)
				.append(self.dtSortButtons);
			self.optionsTabs=new Tabs();
			self.loadFilter
				.append(self.indexStartWrap)
				.append(self.indexEndWrap)
			self.customReportsSelect=$('<select class="dTCustomReportsSelect">').on('change',function(){
				if($(this).val()){
					var val=$(this).val();
					$(this).val('');
					var pids=self.getCheckedPIDs();
					if(!pids.length){
						alert('Please select '+(self.callerProperties.plural_alias?self.callerProperties.plural_alias.toLowerCase():'rows')+' to update.');
						return;
					}
					main.customReports[self.table][val].cb({pids:pids,context:self.caller});
				}
			});
			self.customReportsWrap=$('<div class="dTCustomReportsWrap cf none">')
				.append($('<div>Custom Reports: </div>'))
				.append($('<div>').html(self.customReportsSelect));
			self.loadButton=new Button('std','loadButton dtButtonsTopB','Refresh').html;
			self.clearFilterButton=new Button('std','clearFilterButton dtButtonsTopB','Clear Filter').html;
			// self.clearResultsButton=new Button('std','clearResultsButton dtButtonsTopB','Reset').html;
			self.showAllDetailsButton=new Button('std','showAllDetailsButton dtButtonsTopB','Toggle Details').html;
			self.clearFilterButton.addClass('none');
			if(self.callerProperties.dt_description){
				var desc=self.callerProperties.dt_description;
			}else{
				var prev='View and edit your data in tabular format here. Upon opening Data Table, the most previously edited features will populate the chart first. Select another field if you would like to sort by other data entries in the inventory database, then click the “Apply” button to update the table...';
				var desc='View and edit your data in tabular format here. Upon opening Data Table, the most previously edited features will populate the chart first. Select another field if you would like to sort by other data entries in the inventory database, then click the “Apply” button to update the table. You can change the order of the values within the field by choosing ascending or descending values. The table will show all data fields and can be long lengthwise, so be sure to use the slider bar near the bottom of the table view. You can also click the arrow in the upper right hand corner to expand the table to fit the screen. As you scroll down in the data table, it will automatically load more features to analyze.</br>To edit a feature within the data table, select the feature with the checkbox on the left hand side of the table, then click the pencil icon in each cell of the data. You can also delete certain points or export a selection to a csv file to be read in Microsoft excel by clicking the “Export” or “Delete” buttons on the lower-left hand side of the table.</br>The table will display parameters applied by the advanced filter, or you can view feature data by location. Just click “Filter by Map,” then when you move your cursor to the map, you can draw a polygon around a certain set of features. You can also navigate to the actual location of the feature on the map by clicking the “Zoom To” button after a feature has been selected within the table.';
			}
			self.viewMore=new ViewMore({
				prev:prev,
				content:desc,
				hidePrevOnShow:true
			});
			var activeTab=self.optionsTabs.addTab('Instructions',self.viewMore.html,null,null,null,null,'dtInstructs');
			self.optionsTabs.addTab('Sort Options',self.sortOptionsWrap);
			// self.optionsTabs.addTab('Index Options',self.loadFilter);
			self.optionsTabs.setActiveTab(activeTab.tabid);
			self.selectFromMapButton='';
			if(main.advFilter){
				self.selectFromMapButton=new Button('std','selectFromMapButton dtButtonsTopB','Filter By Map').html;
				self.clearSelectFromMap=new Button('std','clearSelectFromMapButton dtButtonsTopB none','Clear Filter By Map').html;
				self.clearSelectFromMap.on('click',function(){
					main.advFilter.clearMapFilters();
					self.clearSelectFromMap.addClass('none');
					main.advFilter.clearSelectFromMap.html.addClass('none');
					self.sideReset();
				});
				self.selectFromMapButton.on('click',function(){
					if(!main.advFilter.selectingForGeo){
						if(!main.advFilter.layers[self.table].map_layer || !main.advFilter.layers[self.table].map_layer.layer){
							alert('Please load this layer.');
							return;
						}
						var toChange=null;
						if(!main.advFilter.currentLayer || self.table!=main.advFilter.currentLayer.properties.name){toChange=self.table;}
						main.advFilter.startSelectFromMap(toChange,self.sideReset);
					}else{
						if(main.advFilter.hasDrawnOne){
							main.advFilter.toggleSelectPolyVis();
						}else{main.advFilter.stopSelectFromMap();}
					}
				});
				self.togglePolyVisButton=new Button('std','togglePolyVisButton dtButtonsTopB none','Toggle Polygon Visibility').html;
				self.togglePolyVisButton.on('click',function(){
					main.advFilter.toggleSelectPolyVis();
				});
			}
			if(self.caller.properties.transfer_to){
				for(var key in self.caller.properties.transfer_to){
					self.transferToButton=new Button('std','dtButtonsTopB transferToDTButton none',self.caller.properties.transfer_to[key].addButtonToDT,[['table',key]]).html;
					self.transferToButton.on('click',function(){
						var pids=self.getCheckedPIDs();
						if(!pids.length){
							alert('Please select '+(self.callerProperties.plural_alias?self.callerProperties.plural_alias.toLowerCase():'rows')+' to convert.');
							return;
						}
						if(self.caller.properties.transfer_to[Object.keys(self.caller.properties.transfer_to)[0]].copiesText){
							for(var i=0;i<pids.length;i++){
								if(!Number(self.dataTablePkgInnerWrapWrap.find('.dtRow_'+pids[i]+' .transferToCopiesInput').val().trim())){
									alert('One of more of your tree sets for transfer has 0 in the "# For Project" field. You must have at least 1 copy to transfer in all tree sets. No data has been transferred.');
									// pids.splice(i,1);
									return;
								}
							}
						}
						if(self.caller.properties.transfer_to[$(this).data('table')].placeOnMap){
							if(pids.length>1){
								self.html.find('.dtRCheckB:checked').not(':eq(0)').prop('checked',false);
								pids=self.getCheckedPIDs();
								alert('Only one record may be placed to the map at a time. The first checked record was kept and the others were unchecked.');
							}
							self.checkIfAlreadyAdded({pid:pids[0],table:$(this).data('table'),checkField:self.caller.properties.transfer_to[$(this).data('table')].sourcePIDField});
							return;
						}
						self.transferTo({context:self,pids:pids,table:$(this).data('table')});
					});
					break;
				}
			}
			self.dtButtonsTopBsWrap=$('<div class="dtButtonsTopBsWrap cf">')
				.append(self.loadButton)
				// .append(self.clearResultsButton)
				.append(self.selectFromMapButton)
				.append(self.togglePolyVisButton)
				.append(self.transferToButton)
				.append(self.clearSelectFromMap)
				.append(self.customReportsWrap);
			if(main.inventories && main.inventories[self.table]){
				self.zoomToButton=new Button('std','zoomToDTButton dtButtonsTopB','Zoom To').html;
				self.dtButtonsTopBsWrap.append(self.zoomToButton);
				self.zoomToButton.on('click',function(){
					if(main.tables[self.table].map_layer.layer){
						var features=main.tables[self.table].map_layer.layer.olLayer.getSource().getFeatures();
						var feature=null;
						for(var i=0;i<features.length;i++){
							if(features[i].get('pid')==self.activeRow){
								feature=features[i];
							}
						}
						if(feature){
							if(main.filter_legend && main.filter_legend.layerName!=self.table && main.filter_legend.layers[self.table]){
								main.filter_legend.changeLayer(self.table);
							}
							main.mapItems.unSetActives();
							feature.set('isActive',true);
							main.mapItems.hasActive=feature.get('pid');
							main.mapItems.hasActiveLayer=self.table;
							coods=feature.getGeometry().getCoordinates();
							main.map.panTo(coods);
							main.map.animateZoom(main.misc.defaultZoomTo);
						}else{
							if(self.callerProperties.internal_field_map && self.callerProperties.internal_field_map.geometry_field){
								main.utilities.getGeo(self.table,self.callerProperties.internal_field_map.geometry_field,self.activeRow,self.geoFound);
							}
						}
					}else{
						var extraInfo='';
						if(main.tables[self.table].properties.dependent_upon){
							var alias='';
							if(main.invents.inventories[main.tables[self.table].properties.dependent_upon].properties.singular_alias){
								alias=main.invents.inventories[main.tables[self.table].properties.dependent_upon].properties.singular_alias;
							}
							extraInfo=' Select a feature of type '+alias.toLowerCase()+' and load the data.';
						}
						alert('Please load this data first.'+extraInfo);
					}
				});
			}
			if(main.withs.with_dt_mass_updater && main.withs.with_dt_mass_updater.value && main.withs.with_dt_mass_updater[main.account.loggedInUser.user_type+'_with']){
				self.updateButton=new Button('std','womMassUpdateButton','Mass Update').html;
				self.loadButton.after(self.updateButton);
				self.updateButton.on('click',function(){
					self.updateAssItems();
				});
			}
			self.dtButtonsTop=$('<div class="dtButtonsTop cf">')
				.append(self.dtButtonsTopBsWrap)
				.append(self.pageDetailsWrap)
				// .append(self.clearFilterButton)
				// .append(self.showAllDetailsButton)
			/* if(self.caller.is_inventory){
				self.addNewButton=new Button('std','addNewButton dtButtonsTopB','Add New').html;
				self.dtButtonsTop.prepend(self.addNewButton)
				self.addNewButton.on('click',function(){
					self.addNewFromDT();
				});
			} */
			self.loadButton.on('click',function(){
				self.sideReset({scrollToBOnLoad:true});
				/* if(!main.browser.isMobile){self.loadFeatures();}
				else{alert('For data editor, view in a desktop.');} */
			});
			self.clearFilterButton.on('click',function(){
				self.hardClearFilter();
			});
			// self.clearResultsButton.on('click',function(){
				// self.clear();
			// });
			self.showAllDetailsButton.on('click',function(){
				if(!self.showingAllFields){
					self.showingAllFields=true;
					self.showDetails();
				}else{
					self.showingAllFields=false;
					self.showMinimal();
				}
			});
			self.scrollPosBar=$('<div>').addClass('dtScrollPosBar');
			self.scrollPosBar.on(main.controls.touchBegin,self.scrollPosBarTouchBegin);
			self.scrollPos=$('<div>').addClass('dtScrollPos')
				.append(self.scrollPosBar);
			self.dataTablePkgInnerWrapWrap.append(self.scrollPos);
			self.html=$('<div>').addClass('dataTablePkgWrap')
				.append(self.top
					.append(self.optionsTabs.html)
					.append(self.dtButtonsTop)
					.append(self.dtTopButtonsWrap)
				)
				.append(self.dataTablePkgInnerWrapWrap)
				.append(self.dtButtons)
			self.html.find('.dtButtonsTopB,.dtBotButton').addClass('none');
		};
		this.scrollPosBarTouchBegin=function(e){
			if(!self.scrolling){
				$('html').addClass("noSelect cursorDefault");
				self.scrolling=true;
				if(e.type=='mousedown'){
					self.coordinates.mouseStartY=e.pageY;
				}else if(e.type=='touchstart'){
					self.coordinates.mouseStartY=e.originalEvent.touches[0].pageY;
				}else{
					return;
				}
				self.coordinates.scrollStartY=parseInt(self.scrollPosBar.css('top'));
				$('html').on(main.controls.touchEnd,'body',self.scrollPosBarTouchEnd);
				$('html').on(main.controls.touchMove,'body',self.scrollPosBarTouchMove);
			}
		};
		this.scrollPosBarTouchEnd=function(){
			$("html").removeClass("noSelect cursorDefault");
			self.scrolling=false;
			$('html').off(main.controls.touchEnd,'body',self.scrollPosBarTouchEnd);
			$('html').off(main.controls.touchMove,'body',self.scrollPosBarTouchMove);
		};
		this.scrollPosBarTouchMove=function(e){
			if(e.type=='mousemove'){
				var deltaY=e.pageY-self.coordinates.mouseStartY;
			}else if(e.type=='touchmove'){
				var deltaY=e.originalEvent.touches[0].pageY-self.coordinates.mouseStartY;
			}else{
				return;
			}
			var newY=self.coordinates.scrollStartY+deltaY;
			var scrollBarHeight=self.scrollPosBar.outerHeight(true);
			var scrollTrackHeight=self.scrollPos.height();
			if(scrollTrackHeight<(scrollBarHeight+newY)){newY=scrollTrackHeight-scrollBarHeight;}
			if(newY<0){newY=0;}
			self.scrollPosBar.css('top',newY);
			var percOfBar=newY/(scrollTrackHeight-scrollBarHeight);
			var newTBodyPos=percOfBar*(self.dataTableTable.outerHeight(true)-self.dataTableWrap.height());
			self.dataTableWrap.scrollTop(newTBodyPos);
		};
		this.setScrollPosBar=function(){
			if(self.dataTableTable.height()>self.dataTableWrap.height()){
				var percFromTop=self.dataTableWrap.scrollTop()/(self.dataTableTable.height()-self.dataTableWrap.height());
				var scrollPosHeight=self.dataTablePkgInnerWrap.height()-self.tableHeader.outerHeight(true)-12;
				// var top=self.tableHeader.outerHeight(true);
				self.scrollPos/* .css('top',top+'px') */.height(scrollPosHeight).css('bottom','12px');
				if(percFromTop>1){percFromTop=1;}
				var top=(scrollPosHeight-self.scrollPosBar.outerHeight(true))*percFromTop;
				self.scrollPosBar.css('top',top+'px');
				self.scrollPos.removeClass('none');
			}else{
				self.scrollPos.addClass('none');
			}
		};
		this.showMinimal=function(pids){
			main.layout.loader.loaderOn();
			self.tableTabLabels.remove();
			self.populateLabelRow(true);
			self.tbody.find('.tpdataTableRow').each(function(){
				pid=$(this).data('pid');
				if(self.rows[pid].fullyLoaded){
					self.rows[pid].fullyLoaded=false;
					for(var a=0;a<self.sortedFieldKeys.length;a++){
						if(self.inTableTest(self.sortedFieldKeys[a],false)){
							self.convertToRow(self.sortedFieldKeys[a],self.rows[pid].info[self.sortedFieldKeys[a]],pid);
						}
					}
					self.createNewRow(pid,true);
				}
			});
			self.initData(true);
			main.layout.loader.loaderOff();
		};
		this.getCheckedPIDs=function(inv){
			var pids=[];
			self.html.find('.dtRCheckB:checked').each(function(){
				pids.push($(this).val());
			});
			return pids;
		};
		this.updateAssItems=function(inv){
			var pids=self.getCheckedPIDs();
			if(!pids.length){
				alert('Please select '+(self.callerProperties.plural_alias?self.callerProperties.plural_alias.toLowerCase():'rows')+' to update.');
				return;
			}
			if(self.massUpdate){
				self.massUpdate.remove();
				delete self.massUpdate;
			}
			self.massUpdate=new MassUpdate({
				pids:pids,
				layerName:self.table
			});
			self.massUpdate.panel.open();
		};
		this.geoFound=function(response){
			// if(self.caller.layer && self.caller.layer.layer && self.caller.layer.layer.olLayer){
				// var features=self.caller.layer.layer.olLayer.getSource().getFeatures();
				// for(var i=0;i<features.length;i++){
					// if(features[i].get('pid')==response.params.pid){
						// features[i].set('isActive',true);
					// }
				// }
			// }
			if(main.filter_legend && main.filter_legend.layerName!=self.table && main.filter_legend.layers[self.table]){
				main.filter_legend.changeLayer(self.table);
			}
			main.mapItems.hasActive=response.params.pid;
			main.mapItems.hasActiveLayer=self.table;
			main.map.panTo(response.mainCoords);
			main.map.animateZoom(main.misc.defaultZoomTo);
		};
		this.showDetails=function(pids){
			main.layout.loader.loaderOn();
			var pid;
			if(self.dbTabs){
				var primary_tab=self.callerProperties.primary_tab || Object.keys(self.dbTabs)[0];
				var dbTab,tabFields,match;
				self.fieldHolders={};
				for(var i=0;i<this.sortedFieldKeys.length;i++){
					key=this.sortedFieldKeys[i];
					match=false;
					for(var key2 in self.dbTabs){
						dbTab=self.dbTabs[key2];
						tabFields=dbTab.fields;
						if(!self.fieldHolders[key2]){
							self.fieldHolders[key2]={
								fieldHolder:[],
								colorIDX:self.tabColorIDX
							};
							self.tabColorIDX++;
						}
						if(tabFields.indexOf(key)>-1){
							match=true;
							self.fieldHolders[key2].fieldHolder.push(key);
							break;
						}
					}
					if(!match){
						if(!self.fieldHolders[primary_tab]){
							self.fieldHolders[primary_tab]={
								fieldHolder:[],
								colorIDX:self.tabColorIDX
							};
							self.tabColorIDX++;
						}
						self.fieldHolders[primary_tab].fieldHolder.push(key);
					}
				}
			}
			self.populateLabelRow(false);
			self.tbody.find('.tpdataTableRow').each(function(){
				pid=$(this).data('pid');
				if(!self.rows[pid].fullyLoaded){
					self.rows[pid].fullyLoaded=true;
					for(var a=0;a<self.sortedFieldKeys.length;a++){
						if(self.inTableTest(self.sortedFieldKeys[a],false)){
							self.convertToRow(self.sortedFieldKeys[a],self.rows[pid].info[self.sortedFieldKeys[a]],pid);
						}
					}
					self.createNewRow(pid,false);
				}
			});
			self.initData(false);
			main.layout.loader.loaderOff();
		};
		this.getFullRecords=function(pids){
			var newResults=[];
			for(var t=0;t<self.lastLoadedResults.length;t++){
				if(pids.indexOf(self.lastLoadedResults[t]['pid'])>-1){
					newResults.push(self.lastLoadedResults[t]);
				}
			}
			self.populateLabelRow(/* true */);
			self.convertResultsToObject(newResults/* ,true */);
			self.initData(/* pids *//* ,true */);
		};
		this.loadFeatures=function(args){
			self.needsRefresh=false;
			self.loading=true;
			// self.clear();
			var indexStartInputVal=Number(self.indexStartInput.val())-1;
			if(indexStartInputVal || indexStartInputVal==0){self.currentStartIndex=Number(indexStartInputVal)}
			else{self.currentStartIndex=0;}
			if(self.currentStartIndex<0){
				self.currentStartIndex=0;
				self.indexStartInput.val(self.currentStartIndex+1);
			}
			if(Number(self.limitInput.val()) || Number(self.limitInput.val())==0){self.limit=Number(self.limitInput.val())}
			else{self.limit=self.maxResultsPerPage;}
			if(self.limit>self.maxResultsPerPage && self.limitResultsToMax){
				self.limit=self.maxResultsPerPage;
				alert('Too many records requested, returning maximum of '+main.utilities.addCommas(self.limit)+' records');
				self.limitInput.val(self.limit);
			}else if(self.limit==0){
				self.limit=self.maxResultsPerPage;
				alert('Must have a limit, returning maximum of '+main.utilities.addCommas(self.limit)+' records');
				self.limitInput.val(self.limit);
			}
			self.currentSortBy=self.fieldSelecter.val() || self.primaryField;
			self.currentSortBy2=self.fieldSelecter2.val() || null;
			self.currentSortBy3=self.fieldSelecter3.val() || null;
			self.currentSortBy4=self.fieldSelecter4.val() || null;
			self.currentSortOrder=self.orderBySelector.val() || self.defaultSortOrder;
			self.currentSortOrder2=self.orderBySelector2.val() || null;
			self.currentSortOrder3=self.orderBySelector3.val() || null;
			self.currentSortOrder4=self.orderBySelector4.val() || null;
			main.layout.loader.loaderOn();
			self.populateLabelRow(false);
			self.retrieveData(args);
			self.html.find('.dtButtonsTopB,.dtBotButton').not('.noReports,.clearSelectFromMapButton,.togglePolyVisButton,.transferToDTButton').removeClass('none');
			// self.loadButton.addClass('none');
		};
		this.createNewRow=function(pid,shortLoad){
			var dataDetails=self.createDataRow(self.rows[pid],shortLoad);
			var htmlRow=dataDetails.tr;
			var stickyTR=dataDetails.stickyTR;
			if(self.rows[pid].htmlRow){
				self.rows[pid].htmlRow.replaceWith(htmlRow);
				self.rows[pid].stickyTR.replaceWith(stickyTR);
			}else{
				self.tbody.append(htmlRow);
				self.stickyColumnsTbody.append(stickyTR);
			}
			self.rows[pid].htmlRow=htmlRow;
			self.rows[pid].stickyTR=stickyTR;
		};
		this.populateTable=function(){
			// if(pids){
				// var keys=pids;
			// }else{
				var keys=self.sortDataKeys;
			// }
			for(var i=0;i<keys.length;i++){
				if(main.tables[self.table].map_layer && main.tables[self.table].map_layer.features && main.tables[self.table].map_layer.features[keys[i]]){
					main.tables[self.table].map_layer.features[keys[i]].dataTable=self;}
				self.createNewRow(keys[i]/* ,omitShortLoadConstraint */);
			}
		};
		this.updateRow=function(data,dontUpdateOthers){
			var pid=data.pid,row,field,options,fields,value,html;
			if(self.rows[pid]){
				row=self.rows[pid];
				fields=self.rows[pid].fields;
				for(var fieldName in data){
					// if(fields.field){fields=fields.field}
					if(fields[fieldName]){
						field=fields[fieldName].field;
						if(field.active){
							value=data[fieldName];
							var input=self.html.find('.tpdataTableRow.dtRow_'+pid+' .dataTableTd.dtField_'+fieldName+' .dataTableEdit .formInput');
							
							var htmlStuff=main.utilities.setUnit(field,input,value,pid,self.updateLookUpTableFields,self.rows[pid].fields[field.name].input,fields,self.table,true);
							html=htmlStuff.html;
							value=htmlStuff.val;
							if(html!==false){
								self.rows[pid].fields[fieldName].html=html || '';
								self.html.find('.tpdataTableRow.dtRow_'+pid+' .dataTableTd.dtField_'+fieldName+' .dataTableCurrent').html(self.rows[pid].fields[fieldName].html);
							}
							self.rows[pid].fields[fieldName].current_val=value;
							
							//update filter
							// if(field.input_type=='date' || (main.misc.numberDTs.indexOf(field.data_type)>-1 && !field.options)){
								// var isDate=false;
								// if(field.input_type=='date'){isDate=true;}
								// self.updateRangeFilters(fieldName,isDate);
							// }
							// if(field.lookup_table){
								// self.updateLookUpTable(self.tableHeaderLabels.find('.dtFieldHeader.'+fieldName+' .inputPkgContent'),fieldName,field.lookup_table);
							// }
							
							//update other forms
							if(!dontUpdateOthers && main.tables[self.table].map_layer && main.tables[self.table].map_layer.features[pid] && main.tables[self.table].map_layer.features[pid].popup){
								main.tables[self.table].map_layer.features[pid].inventory.updateTable(pid,fieldName,input,false,true);
							}
							
							/* value=data[key];
							field=row.fields[key];
							options=field.field.options;
							if(options && Object.keys(options).length>0){
								for(var key2 in options){
									if(options[key2].alias==value){
										value=key2;
									}else if(options[key2].offAlias==value){
										value=null;
									}
								}
							}
							if(field.field.lookup_table && main.data.lookUpTables[field.field.lookup_table] && field.field.look_up_alias){
								var table=main.data.lookUpTables[field.field.lookup_table].items;
								for(var key2 in table){
									if(table[key2][field.field.look_up_alias]==value){
										value=key2; 
										break;
									}
								}
							}
							self.updateTable(pid,key,value,true); */
						}
					}
				}
				return pid;
			}
		};
		this.clearDT=function(forMap){
			self.rows={};
			self.tbody.html('');
			self.stickyColumnsTbody.html('');
			// self.html.find('.dtButtonsTopB,.dtBotButton').addClass('none');
			self.loadButton.removeClass('none');
			if(self.selectFromMapButton && !forMap){self.selectFromMapButton.removeClass('none');}
		};
		this.setDTForSingleItem=function(pid){
			self.sideReset({searchForOne:pid,nBefore:'std',nAfter:'std'});
		};
		this.partReset=function(){
			self.blockScrollE=true;
			self.clearDT();
			// self.hardClearFilter();
			self.pageDetailsCurrentCount.html(0);
			self.pageDetailsOutOfCount.html(0);
			self.pageFilterDetailsHTML.html('');
			self.pageOrderedByDetailsHTML.html('');
			self.advFIsActive.html('');
			if(self.zoomToButton){self.zoomToButton.removeClass('inlineBlock');}
			self.activeRow=null;
		};
		this.clear=function(){
			self.blockScrollE=true;
			self.clearDT();
			// self.hardClearFilter();
			self.currentSortBy=self.primaryField;
			self.fieldSelecter.val(self.currentSortBy);
			self.fieldSelecter2.val('');
			self.fieldSelecter3.val('');
			self.fieldSelecter4.val('');
			self.currentSortOrder=self.defaultSortOrder;
			self.orderBySelector.val(self.currentSortOrder);
			self.orderBySelector2.val(self.currentSortOrder);
			self.orderBySelector3.val(self.currentSortOrder);
			self.orderBySelector4.val(self.currentSortOrder);
			self.indexStartInput.val(self.initialStartIndex+1);
			self.limitInput.val(self.maxResultsPerPage);
			self.pageDetailsCurrentCount.html(0);
			self.pageDetailsOutOfCount.html(0);
			self.pageFilterDetailsHTML.html('');
			self.pageOrderedByDetailsHTML.html('');
			self.advFIsActive.html('');
			if(self.zoomToButton){self.zoomToButton.removeClass('inlineBlock');}
			self.activeRow=null;
		};
		this.inTableTest=function(key,shortLoad){
			if(self.fields[key] && self.fields[key].active && self.fields[key].in_data_table && !self.fields[key].in_table && self.fields[key].input_type!='html'){
				return true;
			}
			return false;
		};
		this.initiateSort=function(shortLoad){
			if(self.callerProperties && self.callerProperties.sort_by){
				var field=self.callerProperties.sort_by;
			}else{
				var field=self.primaryField;
			}
			if(shortLoad && !self.fields[field].in_popup && field!='pid' && field!=='last_modified'){
				for(var i=0;i<self.sortedFieldKeys.length;i++){
					key=self.sortedFieldKeys[i];
					if(self.inTableTest(key,shortLoad) && self.fields[key].in_popup){
						var field=key;
						break;
					}
				}
			}
			// if(self.callerProperties){
				// if(self.callerProperties.sort_asc){
					// var sort_order='ASC';
				// }else{var sort_order='DESC';}
			// }else{
				var sort_order=self.currentSortOrder;
			// }
			/* if(omitShortLoadConstraint || (!self.callerProperties.short_load || self.shortLoadFields.indexOf(field)>-1)){ */
				self.sortTable(field,sort_order);
				self.setSortArrows(field,sort_order);
			/* } */
		};
		this.sortTable=function(field,sortOrder){
			self.sortDataKeys=Object.keys(self.rows);
			var field1=self.fields[field];
			var lookUpTableMap=self.callerProperties.lookup_table_map;
			self.sortDataKeys.sort(function(a,b){
				var m=self.rows[a].fields[field].current_val;
				var n=self.rows[b].fields[field].current_val;
				if(main.misc.numberDTs.indexOf(field1.data_type)>-1){m=Number(m);
				}else{
					m=main.utilities.getHTMLFromVal(m,field1,lookUpTableMap);
					if(typeof m=='string'){m=m.toLowerCase();}
				}
				if(main.misc.numberDTs.indexOf(field1.data_type)>-1){n=Number(n);
				}else{
					n=main.utilities.getHTMLFromVal(n,field1,lookUpTableMap);
					if(typeof n=='string'){n=n.toLowerCase();}
				}
				if(!m && m!==0){return 1;}
				if(!n && n!==0){return -1;}
				if(m>n) return 1;
				if(m<n) return -1;
				return 0;
			});
			if(sortOrder=='DESC'){
				self.sortDataKeys.reverse();
			}
		};
		this.setSortArrows=function(field,sortOrder){
			if(sortOrder=='ASC'){
				var sortClass='tHSAsc';
			}else{
				var sortClass='tHSDesc';
			}
			this.html.find('.tHeadSortArrow').removeClass('arrowSelected');
			this.html.find('.dataTableLabelTDLiner.'+field+' .'+sortClass).addClass('arrowSelected');
		};
		this.endEditing=function(field,sortOrder){
			var row;
			for(var pid in self.rows){
				row=self.rows[pid];
				for(var field in row.fields){
					if(row.fields[field].dataTableDoneEditLink){
						main.utilities.stopEditing(row.fields[field].dataTableDoneEditLink.closest('.dataTableTd'));
					}
				}
			}
			self.updateHeaderSizes();
		}; 
		this.sortHTMLTable=function(field,sortOrder){
			for(var i=0;i<self.sortDataKeys.length;i++){
				var row=this.html.find('.dataTableWrap .tpdataTableRow.dtRow_'+self.sortDataKeys[i]);
				self.tbody.append(row);
				// self.stickyColumnsTbody.append(row);
			}
		};
		this.updateStickyColsSizes=function(){
			//set row heights
			self.stickyColumnsTbody.find('.tpdataTableRow .dataTableTdLiner').height('');
			self.tbody.find('.tpdataTableRow').each(function(){
				var newHeight=Number($(this).closest('table').attr('border'))+$(this).find('.dataTableTd').height();
				self.stickyColumnsTbody.find('.tpdataTableRow.dtRow_'+$(this).data('pid')+' .dataTableTdLiner').height(newHeight);
			});
			
			//set header widths
			var firstVisibbleRow=self.stickyColumnsTbody.find('tr:visible:eq(0)');
			if(firstVisibbleRow.length>0){
				// var scrollPos=[
					// self.dataTablePkgInnerWrap.scrollLeft(),
					// self.dataTablePkgInnerWrap.find('.dataTableWrap').scrollTop(),
					// self.html.closest('.sidePanel').scrollTop(),
				// ];
				firstVisibbleRow.find('.dataTableTdLiner').each(function(){
					fieldName=$(this).data('field');
					if(!fieldName)fieldName=$(this).data('specialfield');
					self.stickyTableHeaderLabels.find('.dataTableLabelTDLiner.'+fieldName).width($(this).width());
				});
				
				// self.dataTablePkgInnerWrap.scrollLeft(scrollPos[0]);
				// self.dataTablePkgInnerWrap.find('.dataTableWrap').scrollTop(scrollPos[1]);
				// self.html.closest('.sidePanel').scrollTop(scrollPos[2]);
			}
			self.dataTableWrap.css('margin-left',self.stickyColumnsTable.outerWidth(true)+'px');
			self.tableHeaderWrap.css('margin-left',self.stickyColumnsTable.outerWidth(true)+'px');
		};
		this.updateHeaderSizes=function(){
			var firstVisibbleRow=self.tbody.find('tr:visible:eq(0)');
			if(firstVisibbleRow.length>0){
				var scrollPos=[
					self.dataTablePkgInnerWrap.scrollLeft(),
					self.dataTablePkgInnerWrap.find('.dataTableWrap').scrollTop(),
					self.html.closest('.sidePanel').scrollTop(),
				];
				firstVisibbleRow.find('.dataTableTdLiner').each(function(){
					fieldName=$(this).data('field');
					if(!fieldName)fieldName=$(this).data('specialfield');
					self.tableHeaderLabels.find('.dataTableLabelTDLiner.'+fieldName).width($(this).width());
				});
				
				self.dataTablePkgInnerWrap.scrollLeft(scrollPos[0]);
				self.dataTablePkgInnerWrap.find('.dataTableWrap').scrollTop(scrollPos[1]);
				self.html.closest('.sidePanel').scrollTop(scrollPos[2]);
			}
			self.updateStickyPos();
			self.setScrollPosBar();
		};
		this.tableSort=function(field,sortOrder){
			self.endEditing(field,sortOrder);
			self.sortTable(field,sortOrder);
			self.sortHTMLTable();
			self.setSortArrows(field,sortOrder);
		};
		this.populateLabelRow=function(shortLoad/* omitShortLoadConstraint */){
			var stickyDataTableLabelTDLiners=self.stickyTableHeaderLabels.find('.dataTableLabelTDLiner');
			var dataTableLabelTDLiners=self.tableHeaderLabels.find('.dataTableLabelTDLiner');
			self.tableHeaderLabels.html('');
			self.stickyTableHeaderLabels.html('');
			var label,key;
			this.checkAll=new Checkbox({
				label:'',
				value:'dtRCheckAll',
				checked:false,
				name:'dtRCheckAll',
				classes:'dtRCheckAll',
				title:'Toggle Checked'
			});
			self.stickyTableHeaderLabels.append($('<th>').addClass('dataTableLabelTD editingBoxCheckAll')
				.append($('<div data-specialfield="editingBox">').addClass('tdLiner dataTableLabelTDLiner editingBox')
					.append($('<div>').addClass('dTCheckBoxWrap')
						.append($('<div>').addClass('dTCheckAllWrap checkAllEle')
							.append(this.checkAll.html)
						)
					)
				)
			)
			var hasTable=false;
			for(var i=0;i<this.sortedFieldKeys.length;i++){
				key=this.sortedFieldKeys[i];
				if(self.inTableTest(key,shortLoad)){
					label=self.fields[key].alias;
					if(label.length>25){label=label.substring(0,25)+'...';}
					var filterButton='';
					var width='';
					if(dataTableLabelTDLiners.filter('.'+key).length){width=' style="width:'+dataTableLabelTDLiners.filter('.'+key).width()+'"';}
					if(stickyDataTableLabelTDLiners.filter('.'+key).length){width=' style="width:'+stickyDataTableLabelTDLiners.filter('.'+key).width()+'"';}
					// if(self.fields[key].input_type!='html' && self.fields[key].in_filter){filterButton=$('<div>').addClass('filterButton dtColHeadEle').attr('title','Filter').append('<img src="'+main.mainPath+'images/filter.png" class="containImg"/>')}
					if(!self.stickyColumns || self.stickyColumns.indexOf(key)==-1){
						self.tableHeaderLabels.append($('<th>').addClass('dataTableLabelTD dtLabel_'+key)
							.append($('<div data-field="'+key+'"'+width+'>').addClass('tdLiner dataTableLabelTDLiner dtFieldHeader '+key).data('fieldname',key).attr('title',self.fields[key].alias)
								.append($('<div>').addClass('dataTableLabelContent').html(label))
							)
						)
					}else{
						self.stickyTableHeaderLabels.append($('<th>').addClass('dataTableLabelTD dtLabel_'+key)
							.append($('<div data-field="'+key+'"'+width+'>').addClass('tdLiner dataTableLabelTDLiner dtFieldHeader '+key).data('fieldname',key).attr('title',self.fields[key].alias)
								.append($('<div>').addClass('dataTableLabelContent').html(label))
							)
						)
					}
				}else{
					if(self.fields[key].in_table){
						hasTable=true;
					}
				}
			}
			if(hasTable){
				self.tableHeaderLabels.append($('<th>').addClass('dataTableLabelTD dtLabel_view_table')
					.append($('<div data-field="_view_table">').addClass('tdLiner dataTableLabelTDLiner dtFieldHeader _view_table').data('fieldname','_view_table').attr('title','View Associated Table')
						.append($('<div>').addClass('dataTableLabelContent').html('&nbsp;'))
					)
				)
			}
			if(self.caller.properties.transfer_to && self.caller.properties.transfer_to[Object.keys(self.caller.properties.transfer_to)[0]].copiesText){
				var text=self.caller.properties.transfer_to[Object.keys(self.caller.properties.transfer_to)[0]].copiesText;
				self.tableHeaderLabels.append($('<th>').addClass('dataTableLabelTD dtLabel_transferCopies')
					.append($('<div data-field="_transferCopies">').addClass('tdLiner dataTableLabelTDLiner dtFieldHeader _transferCopies').data('fieldname','_transferCopies').attr('title','Transfer count')
						.append($('<div>').addClass('dataTableLabelContent').html(text))
					)
				)
			}
			if(self.printable){
				self.tableHeaderLabels.append($('<th>').addClass('dataTableLabelTD dtLabel_print')
					.append($('<div data-field="_print">').addClass('tdLiner dataTableLabelTDLiner dtFieldHeader _print').data('fieldname','_print').attr('title','Print')
						.append($('<div>').addClass('dataTableLabelContent').html('&nbsp;'))
					)
				)
			}
			if(self.dbTabs && !shortLoad && self.fieldHolders){
				self.tableTabLabels.html('<td></td>');
				var key2,n,color,lastKey;
				for(var key in self.fieldHolders){
					n=0;
					lastKey=null;
					for(var i=0;i<self.fieldHolders[key].fieldHolder.length;i++){
						key2=self.fieldHolders[key].fieldHolder[i];
						if(self.tableHeaderLabels.find('.dtLabel_'+key2).length>0){
							lastKey=key2;
							self.tableHeaderLabels.find('.dtLabel_'+key2).appendTo(self.tableHeaderLabels);
							n++;
						}
					}
					if(lastKey){self.tableHeaderLabels.find('.dtLabel_'+lastKey).addClass('dtLastOfTab');}
					color=main.colors.colors[self.fieldHolders[key].colorIDX];
					
					self.fieldHolders[key].dbTabLabel=$('<td colspan="'+n+'" class="tabLabelTd" '+/* style="background-color:rgba('+color[0]+','+color[1]+','+color[2]+',0.13)" */'><div class="tdLiner">'+self.dbTabs[key].alias+'</div></td>')
					self.tableTabLabels.append(self.fieldHolders[key].dbTabLabel);
				}
				self.labelsTHead.prepend(self.tableTabLabels);
			}else{
				self.tableTabLabels.remove();
			}
		};
		this.addFilters=function(shortLoad/* omitShortLoadConstraint */){
			var filterPkg;
			for(var i=0;i<this.sortedFieldKeys.length;i++){
				key=this.sortedFieldKeys[i];
				if(self.inTableTest(key,shortLoad) && self.fields[key].in_filter/*  && (omitShortLoadConstraint || (!self.callerProperties.short_load || self.shortLoadFields.indexOf(key)>-1)) */){
					filterPkg='';
					if(self.fields[key].in_filter){filterPkg=self.createFilterPkg(self.fields[key]);}
					self.tableHeaderLabels.find('.dtFieldHeader.'+key+' .filterContent').html(filterPkg);
				}
			}
		};
		this.getMapPointExtremes=function(fieldName){
			var maxLat=Number.NEGATIVE_INFINITY,minLat=Number.POSITIVE_INFINITY,maxLng=Number.NEGATIVE_INFINITY,minLng=Number.POSITIVE_INFINITY,val,points,point;
			for(var key in self.rows){
				val=self.rows[key].fields[fieldName].current_val;
				if(val || val===0){
					points=val.split(',');
					for(var i=0;i<points.length;i++){
						point=points[i].split(' ');
						lat=Number(point[1]);
						lng=Number(point[0]);
						if(lat>maxLat){maxLat=lat;}
						if(lat<minLat){minLat=lat;}
						if(lng>maxLng){maxLng=lng;}
						if(lng<minLng){minLng=lng;}
					}
				}
			}
			if(maxLat==Number.NEGATIVE_INFINITY){return false;}
			return {maxLat:maxLat,minLat:minLat,maxLng:maxLng,minLng:minLng}
		};
		this.getLookUpValues=function(fieldName){
			var values=[];
			for(var key in self.rows){
				if(self.rows[key].fields[fieldName]){
					val=self.rows[key].fields[fieldName].current_val;
					if(val && values.indexOf(val)===-1){
						values.push(val);
					}
				}
			}
			return values;
		};
		this.getExtremes=function(fieldName,isDate){
			var max=Number.NEGATIVE_INFINITY,min=Number.POSITIVE_INFINITY,val;
			for(var key in self.rows){
				if(self.rows[key].fields[fieldName]){
					val=self.rows[key].fields[fieldName].current_val;
					if(val || val===0){
						val=Number(self.rows[key].fields[fieldName].current_val);
						if(val>max){max=val;}
						if(val<min){min=val;}
					}
				}
			}
			if(max==Number.NEGATIVE_INFINITY){return false;}
			return {max:max,min:min}
		};
		this.updateLookUpTableFilter=function(params){
			var fieldName=params.fieldName,values=params.values,lookUpTable=params.lookUpTable,inputPkgContent=params.inputPkgContent;
			var table=main.data.lookUpTables[lookUpTable];
			var filterChksWrap=inputPkgContent.find('.filterChksWrap');
			filterChksWrap.html('');
			/* var options=self.fields[fieldName].options,aliasA,aliasB;
			var optionsLength=Object.keys(options).length;
			values.sort(function(a,b){
				itemA=table.items[a];
				itemB=table.items[b];
				if(itemA!=undefined){aliasA=itemA.alias;
				}else if(optionsLength>0){
					if(options[a]){aliasA=options[a].alias;}else{aliasA='';}}
				if(itemB!=undefined){aliasB=itemB.alias;
				}else if(optionsLength>0){
					if(options[b]){aliasB=options[b].alias;}else{aliasB='';}}
				if(aliasA && aliasB){
					if(typeof aliasA=='string'){aliasA=aliasA.toLowerCase();}
					if(typeof aliasB=='string'){aliasB=aliasB.toLowerCase();}
					if(aliasA>aliasB) return 1;
					if(aliasA<aliasB) return -1;
					return 0;
				}else{
					return -1;
				}
			}); */
			var item,alias='';
			var lookUpTableMap=self.callerProperties.lookup_table_map;
			var cultivarField=null;
			if(self.callerProperties.internal_field_map && self.callerProperties.internal_field_map.cultivar){cultivarField=self.callerProperties.internal_field_map.cultivar;}
			values.sort(function(a,b){
				var aAlias=main.utilities.getSimpleLookUpAlias(a,self.fields[fieldName],lookUpTableMap,cultivarField);
				var bAlias=main.utilities.getSimpleLookUpAlias(b,self.fields[fieldName],lookUpTableMap,cultivarField);
				if(aAlias && typeof aAlias=='string'){aAlias=aAlias.toLowerCase();}
				if(bAlias && typeof bAlias=='string'){bAlias=bAlias.toLowerCase();}
				if(aAlias>bAlias){return 1;}
				if(aAlias<bAlias){return -1;}
				return 0;
			});
			for(var i=0;i<values.length;i++){
				alias=main.utilities.getSimpleLookUpAlias(values[i],self.fields[fieldName],lookUpTableMap,cultivarField);
				filterChksWrap.append(new Checkbox({
					label:alias,
					value:values[i],
					checked:true,
					name:values[i],
					classes:'filterCheck filterInput '+values[i]
				}).html);
			}
		};
		this.updateLookUpTable=function(inputPkgContent,fieldName,lookUpTable){
			var values=self.getLookUpValues(fieldName);
			if(!main.data.lookUpTables[lookUpTable].items){
				main.data.lookUpTables[lookUpTable].waiting.push([self.updateLookUpTableFilter,{fieldName:fieldName,values:values,lookUpTable:lookUpTable,inputPkgContent:inputPkgContent}]);
			}else{
				self.updateLookUpTableFilter({fieldName:fieldName,values:values,lookUpTable:lookUpTable,inputPkgContent:inputPkgContent});
			}
		};
		this.getCheckNullsCheckbox=function(type){
			return new Checkbox({
				label:'Show Nulls',
				value:'showNulls',
				checked:true,
				name:'showNulls',
				classes:'showNullsOption '+type
			}).html;
		};
		this.createFilterPkg=function(field){
			var filterPkg=$('<div>').addClass('filterPkg');
			if(field.input_type=='text' || field.input_type=='hidden' || field.input_type=='textarea'){
				var input=new Input({
					field:field,
					classes:'filterInput',
					typeOverride:'text',
					is_in_filter:true,
					layerName:self.table,
					data_table:self
				});
				input.container.prop('disabled',false);
				var inputPkg=$('<div>').addClass('inputPkg').data('name',field.name)
					.append($('<div>').addClass('filterPkgLabel').html(field.alias))
					.append($('<div>').addClass('inputPkgContent')
						.append(input.container)
						.append($('<div class="filterExtras">').append(self.getCheckNullsCheckbox('txt')))
					);
			}else if(field.input_type=='select' || field.input_type=='checkbox' || field.input_type=='radio'){
				var inputPkgContent=$('<div>').addClass('inputPkgContent');
				var filterChksWrap=$('<div>').addClass('filterChksWrap');
				inputPkgContent.append(filterChksWrap);
				if(field.options && Object.keys(field.options).length>0){
					if(field.lookup_table){
						self.updateLookUpTable(inputPkgContent,field.name,field.lookup_table);
					}else{
						for(var key in field.options){
							filterChksWrap.append(new Checkbox({
								label:field.options[key].alias,
								value:field.options[key].value,
								checked:true,
								name:key,
								classes:'filterCheck filterInput '+key
							}).html);
						}
					}
				}
				var inputPkg=$('<div>').addClass('inputPkg chkPkg').data('name',field.name)
					.append($('<div>').addClass('filterPkgLabel').html(field.alias))
					.append(inputPkgContent
						.append($('<div class="filterExtras">').append(self.getCheckNullsCheckbox('chk')))
					);
			}else if(field.input_type=='number'){
				var extremes=self.getExtremes(field.name);
				if(extremes){
					var id=main.utilities.generateUniqueID();
					var range=new Range({
						id:id,
						classes:'filterInput',
						key:field.name,
						to_fixed:field.to_fixed,
						prepend_to:field.prepend_to,
						append_to:field.append_to,
						step:field.step,
						min:extremes.min,
						max:extremes.max,
						onMove:null,
						onChange:self.filterRangeChange
					});
					self.ranges[id]=range;
					var inputPkg=$('<div>').addClass('inputPkg')
						.append($('<div>').addClass('filterPkgLabel').html(field.alias))
						.append($('<div>').addClass('inputPkgContent')
							.append(range.html)
							.append($('<div class="filterExtras">').append(self.getCheckNullsCheckbox('rng')))
						);
				}else{
					var inputPkg=$('<div>').addClass('inputPkg')
						.append($('<div>').addClass('filterPkgLabel').html(field.alias))
						.append($('<div>').addClass('inputPkgContent')
							.append(self.noFilterValuesMsg)
						);
				}
			}else if(field.input_type=='date'){
				var extremes=self.getExtremes(field.name);
				if(extremes){
					var id=main.utilities.generateUniqueID();
					var isTime=false;
					var isDate=true;
					if(field.sub_data_type=='time'){
						var displayFunction=main.utilities.msToTime;
						isDate=false;
						isTime=true;
					}else{var displayFunction=main.utilities.formatDate;}
					var range=new Range({
						id:id,
						classes:'filterInput',
						key:field.name,
						to_fixed:field.to_fixed,
						prepend_to:field.prepend_to,
						append_to:field.append_to,
						step:field.step,
						min:extremes.min,
						max:extremes.max,
						onMove:null,
						onChange:self.filterRangeChange,
						sticky:false,
						stickyStep:1000*60*60*24,
						displayFunction:displayFunction,
						isDate:isDate,
						isTime:isTime
					});
					self.ranges[id]=range;
					var inputPkg=$('<div>').addClass('inputPkg')
						.append($('<div>').addClass('filterPkgLabel').html(field.alias))
						.append($('<div>').addClass('inputPkgContent')
							.append(range.html)
							.append($('<div class="filterExtras">').append(self.getCheckNullsCheckbox('rng')))
						)
				}else{
					var inputPkg=$('<div>').addClass('inputPkg')
						.append($('<div>').addClass('filterPkgLabel').html(field.alias))
						.append($('<div>').addClass('inputPkgContent')
							.append(self.noFilterValuesMsg)
						);
				}
			}else if(field.input_type=='map_point'){
				var extremes=self.getMapPointExtremes(field.name);
				if(extremes){
					var latId=main.utilities.generateUniqueID();
					var latRange=new Range({
						alias:'Lat',
						id:latId,
						classes:'filterInput lat',
						key:field.name,
						to_fixed:field.to_fixed,
						prepend_to:field.prepend_to,
						append_to:field.append_to,
						step:field.step,
						min:extremes.minLat,
						max:extremes.maxLat,
						to_fixed:10,
						onMove:null,
						onChange:self.filterMapPointChangeLat
					});
					self.ranges[latId]=latRange;
					var lngId=main.utilities.generateUniqueID();
					var lngRange=new Range({
						alias:'Lng',
						id:lngId,
						classes:'filterInput lng',
						key:field.name,
						to_fixed:field.to_fixed,
						prepend_to:field.prepend_to,
						append_to:field.append_to,
						step:field.step,
						min:extremes.minLng,
						max:extremes.maxLng,
						to_fixed:10,
						onMove:null,
						onChange:self.filterMapPointChangeLat
					});
					self.ranges[lngId]=lngRange;
					var inputPkg=$('<div>').addClass('inputPkg')
						.append($('<div>').addClass('filterPkgLabel').html(field.alias))
						.append($('<div>').addClass('inputPkgContent')
							.append(latRange.html)
							.append(lngRange.html)
							.append($('<div class="filterExtras">').append(self.getCheckNullsCheckbox('rng')))
						)
				}else{
					var inputPkg=$('<div>').addClass('inputPkg')
						.append($('<div>').addClass('filterPkgLabel').html(field.alias))
						.append($('<div>').addClass('inputPkgContent')
							.append(self.noFilterValuesMsg)
						);
				}
			}
			return filterPkg.append(inputPkg);
		};
		this.filterCheckChange=function(fieldName,chkPkg){
			var val,values=[];
			chkPkg.find('.filterCheck:checked').each(function(){
				val=$(this).val();
				if(val=='true'){val=true;}
				if(val=='false'){val=false;}
				values.push(val);
			});
			self.filter.currentFilters[fieldName]={
				type:'check',
				fieldName:fieldName,
				values:values,
				html:chkPkg
			}
			self.updateFilter();
		};
		this.filterForCheck=function(fieldName,values,html){
			for(var key in self.rows){
				if(self.filter.currentRows.indexOf(key)>-1){
					if(!self.rows[key].fields[fieldName].current_val && self.rows[key].fields[fieldName].current_val!==0){
						//is nothing
						if(!html.find('.showNullsOption').prop('checked')){
							//dont show nulls
							self.filter.currentRows.splice(self.filter.currentRows.indexOf(key),1);
						}
					}else{
						//is something
						if(values.indexOf(self.rows[key].fields[fieldName].current_val)===-1){
							self.filter.currentRows.splice(self.filter.currentRows.indexOf(key),1);
						}
					}
				}
			}
		};
		this.filterTextChange=function(fieldName,val,html){
			if(!val.trim() && self.filter.currentFilters[fieldName]){
				delete self.filter.currentFilters[fieldName];
			}else{
				self.filter.currentFilters[fieldName]={
					type:'text',
					fieldName:fieldName,
					value:val,
					html:html
				}
			}
			self.updateFilter();
		};
		this.filterForText=function(fieldName,val,html){
			for(var key in self.rows){
				if(self.filter.currentRows.indexOf(key)>-1){
					if(!self.rows[key].fields[fieldName].current_val && self.rows[key].fields[fieldName].current_val!==0){
						if(!html.find('.showNullsOption').prop('checked')){
							//dont show nulls
							self.filter.currentRows.splice(self.filter.currentRows.indexOf(key),1);
						}
					}else{
						//is something
						if(self.rows[key].fields[fieldName].current_val.indexOf(val)===-1){
							self.filter.currentRows.splice(self.filter.currentRows.indexOf(key),1);
						}
					}
				}
			}
		};
		this.filterMapPointChangeLat=function(filterPkg){
			var fieldName=filterPkg.key;
			var latRangePkg=self.ranges[filterPkg.html.closest('.filterPkg').find('.filterInput.lat').attr('id')].getRangePkg();
			var lngRangePkg=self.ranges[filterPkg.html.closest('.filterPkg').find('.filterInput.lng').attr('id')].getRangePkg();
			self.filter.currentFilters[fieldName]={
				type:'map_range',
				fieldName:fieldName,
				maxLat:latRangePkg.currentHi,
				minLat:latRangePkg.currentLow,
				maxLng:lngRangePkg.currentHi,
				minLng:lngRangePkg.currentLow,
				html:filterPkg.html
			}
			self.updateFilter();
		};
		this.filterForMapPoint=function(fieldName,maxLat,minLat,maxLng,minLng,html){
			for(var key in self.rows){
				if(self.filter.currentRows.indexOf(key)>-1){
					var val=self.rows[key].fields[fieldName].current_val;
					if(!val){
						//is nothing
						if(!html.closest('.filterPkg').find('.showNullsOption').prop('checked')){
							//dont show nulls
							self.filter.currentRows.splice(self.filter.currentRows.indexOf(key),1);
						}
					}else{
						//is something
						var points=val.split(','),maxLat,maxLng,go,mainGo=false;
						for(var i=0;i<points.length;i++){
							point=points[i].split(' ');
							lat=Number(point[1]);
							lng=Number(point[0]);
							go=true;
							if(lat>maxLat){go=false;}
							if(lat<minLat){go=false;}
							if(lng>maxLng){go=false;}
							if(lng<minLng){go=false;}
							if(go){mainGo=true;}
						}
						if(!mainGo){
							self.filter.currentRows.splice(self.filter.currentRows.indexOf(key),1);
						}
					}
				}
			}
		};
		this.filterRangeChange=function(filterPkg){
			var fieldName=filterPkg.key;
			if(!self.filter.currentFilters[fieldName]){
				self.filter.currentFilters[fieldName]={
					type:'range',
					fieldName:fieldName,
					max:filterPkg.currentHi,
					min:filterPkg.currentLow,
					html:filterPkg.html
				}
			}
			self.filter.currentFilters[fieldName].max=filterPkg.currentHi;
			self.filter.currentFilters[fieldName].min=filterPkg.currentLow;
			self.updateFilter();
		};
		this.filterForRange=function(fieldName,max,min,html){
			for(var key in self.rows){
				if(self.filter.currentRows.indexOf(key)>-1){
					if(!self.rows[key].fields[fieldName].current_val && self.rows[key].fields[fieldName].current_val!==0){
						//is nothing
						if(!html.closest('.inputPkg').find('.showNullsOption').prop('checked')){
							//dont show nulls
							self.filter.currentRows.splice(self.filter.currentRows.indexOf(key),1);
						}
					}else{
						//is something
						if(Number(self.rows[key].fields[fieldName].current_val)<min || Number(self.rows[key].fields[fieldName].current_val)>max){
							self.filter.currentRows.splice(self.filter.currentRows.indexOf(key),1);
						}
					}
				}
			}
		};
		this.resetFilters=function(){
			self.tableHeaderLabels.find('.filterInput.textInput').val('');
			self.tableHeaderLabels.find('.filterInput.filterCheck').prop('checked',true);
			self.tableHeaderLabels.find('.showNullsOption').prop('checked',true);
			self.tableHeaderLabels.find('.rangeOuterWrap').each(function(){
				self.ranges[$(this).attr('id')].reset();
			});
		};
		this.closeFilters=function(){
			self.tableHeaderLabels.find('.filterContent.block').removeClass('block');
		};
		this.hardClearFilter=function(){
			self.clearFilter();
			self.resetFilters();
			self.closeFilters();
			self.filter.currentFilters={};
		};
		this.clearFilter=function(){
			for(var key in self.rows){
				self.rows[key].htmlRow.removeClass('none filteredOut');
			}
			self.filter.currentRows=Object.keys(self.rows);
		};
		this.updateFilter=function(){
			self.clearFilter();
			var filter;
			for(var key in self.filter.currentFilters){
				filter=self.filter.currentFilters[key];
				if(filter.type=='check'){
					self.filterForCheck(key,filter.values,filter.html);
				}else if(filter.type=='text'){
					self.filterForText(key,filter.value,filter.html);
				}else if(filter.type=='range'){
					self.filterForRange(key,filter.max,filter.min,filter.html);
				}else if(filter.type=='map_range'){
					self.filterForMapPoint(key,filter.maxLat,filter.minLat,filter.maxLng,filter.minLng,filter.html);
				}
			}
			self.updateFilterHTML();
			self.updateHeaderSizes();
		};
		this.updateFilterHTML=function(){
			for(var key in self.rows){
				if(self.filter.currentRows.indexOf(key)===-1){
					self.rows[key].htmlRow.addClass('none filteredOut');
				}
			}
		};
		this.getDTTR=function(pid,stickyTR){
			var classes='';
			if(stickyTR){classes=' dtStickyTR';}
			return $('<tr data-pid="'+pid+'">').addClass('tpdataTableRow dataUnit dtRow_'+pid+classes);
		};
		this.getFirstDTCell=function(pid){
			return $('<td>').addClass('dataTableTd notEditing outerEditingW')
				.append($('<div data-specialfield="editingBox">').addClass('tdLiner dataTableTdLiner editingBox')
					.append($('<div>').addClass('editBoxWrap')
						.append(new Checkbox({
								value:pid,
								name:key,
								classes:'dtRCheckB '+pid,
								wrapClasses:'dTCheckBWrap editBoxEle'
							}).html
						)
						/* .append($('<div>').addClass('deleteDataRowwrap editBoxEle')
							.append(new Button('std','deleteDataRowButton '+pid,'Delete',[['pid',pid]]).html)
						) */
					)
				)
		};
		this.addEditPkg=function(tr){
			if(self.callerProperties[main.account.loggedInUser.user_type+'_data_mng']){
				var pid=tr.data('pid');
				var info=self.lastLoadedResults[pid];
				// if(self.rows[pid]){
					// if(self.rows[pid].htmlRow){
						// self.rows[pid].htmlRow.remove();
						// delete self.rows[pid].htmlRow;
					// }
				// }
				self.rows[pid]={
					fields:{},
					info:info,
					fullyLoaded:false
				};
				self.rows[pid].htmlRow=tr;
				var value;
				for(var i=0;i<self.sortedFieldKeys.length;i++){
					key=self.sortedFieldKeys[i];
					if(self.inTableTest(key,false)){
						value=info[key].val;
						self.rows[pid].fields[key]={
							current_val:value,
							html:value,
							field:self.fields[key]
						};
						main.utilities.getRowThings(self.rows[pid],self.fields[key],pid,{callback:self.updateLookUpTableFields,params:{pid:pid,fieldName:key,value:value}},{callback:self.newSignature},null,null,self.table,false,true);
						if(self.fields[key].input_type=='file' && self.rows[pid].fields[key].input){
							self.rows[pid].fields[key].input.currentPhotos.setRemoveCallBack(self.photoRemoved);
							self.rows[pid].fields[key].input.currentPhotos.setRemoveCallBack(main.utilities.photoELClicked);
						}
						tr.find('.dtField_'+key+' .dataTableCurrent').after(self.rows[pid].fields[key].editPackage);
					}
				}
				tr.addClass('editPkgAdded');
			}
		};
		this.printSubmit=function(params){
			main.print.printRow(params);
		};
		this.printRow=function(pid){
			var mapPoints=[];
			for(var key in self.fields){
				if(self.fields[key].input_type=='map_point' || self.fields[key].input_type=='map_polygon'){
					mapPoints.push(key);
				}
			}
			if(mapPoints.length){
				$.ajax({
					type:'POST',
					url:main.mainPath+'server/db.php',
					data:{
						params:{
							folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
							mapPoints:mapPoints.join(','),
							pid:pid,
							table:self.table
						},
						action:'getMapPoints'
					},
					success:function(response){
						if(response){
							response=JSON.parse(response);
							if(response.status=="OK"){
								var obj={
									type:'FeatureCollection',
									features:[]
								}
								var results=response.results,coords;
								var hasSome=false;
								for(var key in results){
									if(results[key]){
										result=JSON.parse(results[key]);
										type=result.type;
										if(type=='MultiPoint'){
											coords=result.coordinates;
											for(var i=0;i<coords.length;i++){
												obj.features.push({
													geometry:{
														coordinates:coords[i],
														type:'Point'
													},
													properties:{},
													type:'Feature'
												});
											}
										}else if(type=='Polygon'){
											coord=[];
											coords=result.coordinates[0];
											for(var i=0;i<coords.length;i++){
												coord.push(coords[i]);
											}
											obj.features.push({
												geometry:{
													coordinates:[coord],
													type:'Polygon'
												},
												properties:{},
												type:'Feature'
											});
										}else if(type=='MultiPolygon'){
											coords=result.coordinates;
											for(var i=0;i<coords.length;i++){
												coord=[];
												for(var b=0;b<coords[i][0].length;b++){
													coord.push(coords[i][0][b]);
												}
												obj.features.push({
													geometry:{
														coordinates:[coord],
														type:'Polygon'
													},
													properties:{},
													type:'Feature'
												});
											}
										}
										hasSome=true;
									}
								}
								if(hasSome){
									var style=main.utilities.getDefaultStyle();
									var layer=new ol.layer.Vector({
										source:new ol.source.Vector({features: (new ol.format.GeoJSON()).readFeatures(obj)}),
										style:style
									});
									main.map.mapPrint.printMap([layer],{cb:self.printMapSaved,pid:response.params.pid,table:self.table});
								}else{
									self.printSubmit({pid:response.params.pid,table:self.table});
								}
							}else{
								alert('Error');
								console.log(response);
							}
						}
					}
				});
			}else{
				self.printSubmit({pid:pid,table:self.table});
			}
		};
		this.printMapSaved=function(params){
			self.printSubmit(params);
		};
		this.getFieldDTCell=function(fieldname,html,editpkg,special,classes){
			if(special=='print'){
				fieldname='_print';
				var button=new Button('std','dtButton printRowDTButton','Print');
				button.html.on('click',function(){
					self.printRow($(this).closest('.tpdataTableRow').data('pid'));
				});
				return $('<td>').addClass('dataTableTd notEditing outerEditingW dtField_'+fieldname).data('field',fieldname)
					.append($('<div data-field="'+fieldname+'">').addClass('td Liner center editPkgWrap dataTableTdLiner '+fieldname)
						.append(button.html)
					);
			}else if(special=='transferTo'){
				fieldname='_transferCopies';
				return $('<td>').addClass('dataTableTd notEditing outerEditingW dtField_'+fieldname).data('field',fieldname)
					.append($('<div data-field="'+fieldname+'">').addClass('td Liner center editPkgWrap dataTableTdLiner '+fieldname)
						.append('<input type="number" class="transferToCopiesInput" min="0" value="0" disabled="disabled" onchange="'+self.caller.properties.transfer_to[Object.keys(self.caller.properties.transfer_to)[0]].copiesValidate+'(this)"/>')
					);
			}else{
				var center='';
				if(fieldname[0]=='_'){center=' center hauto';}
				editpkg=editpkg || '';
				return $('<td>').addClass('dataTableTd notEditing outerEditingW dtField_'+fieldname).data('field',fieldname)
					.append($('<div data-field="'+fieldname+'">').addClass('tdLiner editPkgWrap dataTableTdLiner '+fieldname)
						.append($('<div>').addClass('dataTableCurrent '+fieldname+center).data('field',fieldname).html(html))
						.append(editpkg)
					);
			}
		};
		this.createDataRow=function(row,shortLoad){
			var pid=row.fields['pid'].current_val;
			var tr=self.getDTTR(pid),html,editpkg;
			var stickyTR=self.getDTTR(pid,true);
			stickyTR.append(self.getFirstDTCell(pid));
			for(var i=0;i<self.sortedFieldKeys.length;i++){
				key=self.sortedFieldKeys[i];
				if(self.inTableTest(key,shortLoad)){
					html=row.fields[key].html;
					if(typeof html=='object' && html!==null && self.fields[key].input_type!='file'){
						html='Click for details';
					}
					editpkg=(self.callerProperties[main.account.loggedInUser.user_type+'_dt_editable'])?row.fields[key].editPackage:'';
					if(!self.stickyColumns || self.stickyColumns.indexOf(key)==-1){
						tr.append(self.getFieldDTCell(key,html,editpkg));
					}else{
						stickyTR.append(self.getFieldDTCell(key,html,editpkg));
					}
				}
			}
			if(self.dbTabs && !shortLoad && self.fieldHolders){
				var key2,lastKey;
				for(var key in self.fieldHolders){
					lastKey=null;
					for(var i=0;i<self.fieldHolders[key].fieldHolder.length;i++){
						key2=self.fieldHolders[key].fieldHolder[i];
						if(tr.find('.dataTableTd.dtField_'+key2).length>0){
							lastKey=key2;
							tr.find('.dataTableTd.dtField_'+key2).appendTo(tr);
						}
					}
					if(lastKey){tr.find('.dataTableTd.dtField_'+lastKey).addClass('dtLastOfTab');}
				}
			}
			if(self.caller.properties.transfer_to && self.caller.properties.transfer_to[Object.keys(self.caller.properties.transfer_to)[0]].copiesText){tr.append(self.getFieldDTCell(null,null,null,'transferTo'));}
			if(self.printable){tr.append(self.getFieldDTCell(null,null,null,'print'));}
			return {
				tr:tr,
				stickyTR:stickyTR
			};
		};
		this.createAliasDataRow=function(pid,row){
			var tr=self.getDTTR(pid),alias,viewTableButton;
			var stickyTR=self.getDTTR(pid,true);
			stickyTR.append(self.getFirstDTCell(pid));
			var hasTable=false;
			for(var i=0;i<self.sortedFieldKeys.length;i++){
				key=self.sortedFieldKeys[i];
				if(self.inTableTest(key)){
					alias=row[key].val;
					if(row[key].alias){alias=row[key].alias;}
					if(self.fields[key].input_type=='file'){
						alias=main.utilities.getPhotosPkg(pid,key,self.table).html;
					}
					if(!self.stickyColumns || self.stickyColumns.indexOf(key)==-1){
						tr.append(self.getFieldDTCell(key,alias));
					}else{
						stickyTR.append(self.getFieldDTCell(key,alias));
					}
				}else{
					if(self.fields[key] && self.fields[key].in_table){
						hasTable=true;
					}
				}
			}
			if(hasTable && self.caller.properties.dt_export_table_function){
				var viewTableButton=new Button('std','dtButton viewTableDTButton','View Table',[['pid',pid]]);
				viewTableButton.html.on('click',function(){
					main.utilities.callFunctionFromName(self.caller.properties.dt_export_table_function,window,{context:self,pid:$(this).data('pid')});
				});
				tr.append(self.getFieldDTCell('_view_table',viewTableButton.html));
			}
			if(self.caller.properties.transfer_to && self.caller.properties.transfer_to[Object.keys(self.caller.properties.transfer_to)[0]].copiesText){tr.append(self.getFieldDTCell(null,null,null,'transferTo'));}
			if(self.printable){tr.append(self.getFieldDTCell(null,null,null,'print'));}
			return {
				tr:tr,
				stickyTR:stickyTR
			};
		};
		this.transferToContinue=function(params,setVals){
			var table=self.caller.properties.transfer_to[params.table].table;
			var transferFields=self.caller.properties.transfer_to[params.table].transferFields?JSON.stringify(self.caller.properties.transfer_to[params.table].transferFields):null;
			// var copiesField=self.caller.properties.transfer_to[params.table].copiesField?self.caller.properties.transfer_to[params.table].copiesField:null;
			var copiesPkg={};
			for(var i=0;i<params.pids.length;i++){
				copiesPkg[params.pids[i]]=(self.dataTablePkgInnerWrapWrap.find('.dtRow_'+params.pids[i]+' .transferToCopiesInput').val() || 1);
				copiesPkg[params.pids[i]]=Math.round(copiesPkg[params.pids[i]]);
			}
			setVals=setVals || null;
			if(!table || !params.pids || !params.pids.length){return;}
			var decrementFieldPkg=null;
			for(var key in main.tables){
				if(main.tables[key].properties.transfer_to && main.tables[key].properties.transfer_to[table] && main.tables[key].properties.transfer_to[table].availableField && main.tables[key].properties.transfer_to[table].sourcePIDField){
					if(!decrementFieldPkg){decrementFieldPkg=[];}
					decrementFieldPkg.push({
						availableField:main.tables[key].properties.transfer_to[table].availableField,
						sourcePIDField:main.tables[key].properties.transfer_to[table].sourcePIDField,
						sourceTable:key,
						thisTable:table
					});
				}
			}
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						transferFields:transferFields,
						fromTable:self.table,
						copiesPkg:copiesPkg,
						setVals:setVals,
						fromPIDs:params.pids.join(','),
						table:table,
						decrementFieldPkg:decrementFieldPkg
					},
					action:'transferTo'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							self.transferToPanel.close();
							self.clearChecked();
							if(main.dataTables[response.table].dataContentWrap.is(':visible')){
								main.dataTables[response.table].sideReset();
							}else{main.dataTables[response.table].needsRefresh=true;}
							if(response.refreshDT){
								if(self.dataContentWrap.is(':visible')){
									self.sideReset();
								}else{self.needsRefresh=true;}
							}
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.turnOffTransferToMap=function(){
			self.placeOnMapStopButton.addClass('none');
			self.transferToMapStartButton.removeClass('none');
			main.invents.selectingForTransferTo=false;
			main.map.mapContainer.removeClass('adding');
			main.map.map.un('click',self.mapClickedForTransferTo);
			self.transferToTable=null;
			self.transferToPIDs=null;
			self.transferToPanel.close();
		};
		this.mapTransferedToSaved=function(){
			if(self.dataContentWrap.is(':visible')){
				self.sideReset();
			}else{self.needsRefresh=true;}
		};
		this.mapTransferedTo=function(params){
			if(self.caller.properties.transfer_to[params.table].sourcePIDField){
				var changeFields=[{field:main.tables[self.table].fields[self.caller.properties.transfer_to[params.table].sourcePIDField],val:params.result['pid']}];
				main.dataEditor.updateData({
					pids:[self.transferToPIDs[0]],
					context:self.caller,
					changeFields:changeFields,
					callback:self.mapTransferedToSaved
				});
				self.turnOffTransferToMap();
			}
		};
		this.mapClickedForTransferTo=function(e){
			if(main.invents.selectingForTransferTo && self.transferToTable){
				e.originalEvent.stopImmediatePropagation();
				var coord=main.map.map.getEventCoordinate(e.originalEvent);
				var transferFields=self.caller.properties.transfer_to[self.transferToTable].transferFields;
				transferFields=transferFields || null;
				var pids=[self.transferToPIDs[0]];
				$.ajax({
					type:'POST',
					url:main.mainPath+'server/db.php',
					data:{
						params:{
							folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
							transferFields:transferFields,
							fromTable:self.table,
							coord:coord,
							pids:pids.join(','),
							table:self.transferToTable
						},
						action:'getBasicResults'
					},
					success:function(response){
						if(response){
							response=JSON.parse(response);
							if(response.status=="OK"){
								if(response.results){
									response.coord[0]=response.coord[0];
									response.coord[1]=response.coord[1];
									main.invents.inventories[self.transferToTable].newItem(response.coord,response.results[0],response.transferFields,null,null,null,null,null,true,self.mapTransferedTo);
								}
							}else{
								alert('Error');
								console.log(response);
							}
						}
					}
				});
			}
		};
		this.transferTo=function(params){
			if(self.transferToPanel){
				self.transferToPanel.close();
				delete self.transferToPanel;
			}
			self.transferToHTML=$('<div class="transferToHtml">');
			var table=self.caller.properties.transfer_to[params.table].table;
			var fields=main.tables[table].fields,field,input,tow;
			self.transferToBottomButtons=$('<div class="transferToBottomButtons">');
			if(self.caller.properties.transfer_to[params.table].askFields){
				var askFields=self.caller.properties.transfer_to[params.table].askFields;
				for(var i=0;i<askFields.length;i++){
					input=new Input({
						field:fields[askFields[i]],
						layerName:table,
						classes:'dtTransferInput',
						sortBy:'abc'
					});
					tow=$('<div class="transferToAskFieldRow cf">')
						.append($('<div class="transferToAskFieldRowEle transferToAskFieldRowLabel">').html(fields[askFields[i]].alias+':'))
						.append($('<div class="transferToAskFieldRowEle transferToAskFieldRowVal">').html(input.container))
					self.transferToHTML.append(tow);
				}
				/* if(self.caller.properties.transfer_to[params.table].copiesField){
					input=new Input({
						field:self.fields[self.caller.properties.transfer_to[params.table].copiesField],
						layerName:self.table,
						classes:'dtTransferCopiesInput'
					});
					tow=$('<div class="transferToAskFieldRow dtTransferCopiesRow cf">')
						.append($('<div class="transferToAskFieldRowEle transferToAskFieldRowLabel">').html(self.fields[self.caller.properties.transfer_to[params.table].copiesField].alias+':'))
						.append($('<div class="transferToAskFieldRowEle transferToAskFieldRowVal">').html(input.container))
					self.transferToHTML.append(tow);
				} */
				self.transferToGoButton=new Button('std','dtButton dtTransferToGoButton','Create',[['params',params]]);
				self.transferToGoButton.html.on('click',function(){
					var setVals=[];
					if(self.caller.properties.transfer_to[params.table].askFields){
						var askFields=self.caller.properties.transfer_to[params.table].askFields;
						for(var i=0;i<askFields.length;i++){
							setVals.push([askFields[i],self.transferToHTML.find('.dtTransferInput.'+askFields[i]).val()]);
						}
					}
					self.transferToContinue($(this).data('params'),setVals);
				});
				self.transferToBottomButtons.append(self.transferToGoButton.html);
			}
			if(self.caller.properties.transfer_to[params.table].placeOnMap){
				self.transferToMapStartButton=new Button('std','transferToMapStartButton transferToMapButton','Select From Map',[['table',table],['pids',params.pids]],null,null,'Select a point from the map.').html.on('click',function(){
					$(this).addClass('none');
					self.placeOnMapStopButton.removeClass('none');
					main.invents.selectingForTransferTo=true;
					self.transferToTable=$(this).data('table');
					self.transferToPIDs=$(this).data('pids');
					if(main.filter_legend && main.filter_legend.active && main.filter_legend.layerName!=self.transferToTable && main.filter_legend.layers[self.transferToTable]){
						main.filter_legend.changeLayer(self.transferToTable,null,null,true);
					}
					main.map.mapContainer.addClass('adding');
					main.map.map.on('click',self.mapClickedForTransferTo);
					self.transferToPanel.positionPanel(null,'bLeft');
				});
				self.placeOnMapStopButton=new Button('std','transferToMapStopButton transferToMapButton none','Stop Selecting',null,null,null,'Stop Selecting').html.on('click',self.turnOffTransferToMap);
				self.transferToBottomButtons
					.append(self.transferToMapStartButton)
					.append(self.placeOnMapStopButton);
			}
			self.transferToHTML.append(self.transferToBottomButtons);
			self.transferToPanel=new Panel({
				content:self.transferToHTML,
				title:'Select values to add.',
				classes:'dtTransferToValuesToAdd',
				centerOnOrientationChange:true
			});
			self.transferToPanel.open();
		};
		this.createTableSort=function(fieldName){
			var tableSort=$('<div data-field="'+fieldName+'">').addClass('tableSortPkg '+fieldName)
				.append($('<div>').addClass('sortableEle tHeadSortWrap')
					.append($('<div>').addClass('tHeadSortArrow tHSAsc').attr('title','Sort Ascending'))
					.append($('<div>').addClass('tHeadSortArrow tHSDesc').attr('title','Sort Descending'))
				)
			return tableSort;
		},
		this.newSignature=function(sig){
			var pid=sig.container.closest('.tpdataTableRow').data('pid');
			var field=sig.container.closest('.dataTableInputWrap').data('field');
			self.rows[pid].fields[field].html=main.utilities.createSigImage(sig.img);
			sig.container.closest('.dataTableTdLiner').find('.dataTableCurrent').html(self.rows[pid].fields[field].html);
		};
		this.getSignatureFields=function(){
			self.signatureFields=[];
			for(var key in self.fields){
				if(self.fields[key].active && self.fields[key].input_type=='signature'){
					self.signatureFields.push(key);
				}
			}
		};
		this.addData=function(formPkg){
			//convert data
			self.convertResultsToObject([formPkg]);
			//row to table
			// self.createNewRow(formPkg['pid']);
			//update header sizes
			self.updateHeaderSizes();
		};
		/* this.updateData=function(args){
			var pids=args.pids;
			var callback=args.callback;
			if(self.callerProperties[main.account.loggedInUser.user_type+'_dt_editable']){
				if(!self.loaded){self.loadFeatures(main.dataEditor.updateData,{pids:pids,callback:callback});return;}
				var recordPkgs={},fields=[],field,row,pid,value;
				for(var i=0;i<pids.length;i++){
					pid=pids[i];
					if(self.rows[pid]){
						row=self.rows[pid];
						recordPkgs[pid]={};
						for(var key in row.fields){
							field=self.fields[key];
							if(field.active && field.in_update && field.in_data_table && !field.in_table){
								// value=row.fields[key].current_val;
								if(self.callerProperties.internal_field_map && self.callerProperties.internal_field_map.last_modified && self.callerProperties.internal_field_map.last_modified==key){
									main.misc.blockUpdateData=true;
									self.tbody.find('.dtRow_'+pid+' .last_modified.formInput').val(main.utilities.formatDate(new Date().getTime())).change();
									main.misc.blockUpdateData=false;
								}
								if(main.tables[self.table].map_layer && main.tables[self.table].map_layer.features && main.tables[self.table].map_layer.features[pid]){
									main.tables[self.table].map_layer.features[pid].properties[key]=row.fields[key].current_val;
									var features=main.tables[self.table].map_layer.layer.source.getFeatures();
									for(var t=0;t<features.length;t++){
										if(features[t].get('pid')==pid){features[t].set(key,row.fields[key].current_val)}
									}
								}
								recordPkgs[pid][key]={
									value:row.fields[key].current_val,
									data_type:field.data_type,
									sub_data_type:field.sub_data_type,
									input_type:field.input_type,
									name:key
								}
								if(field.input_type=='signature' && row.fields[key].input.signature.img){
									recordPkgs[pid][key].sigImg=row.fields[key].input.signature.img;
								}
								if(i===0){fields.push(key)};
							}
						}
					}
				}
				$.ajax({
					type:'POST',
					url:main.mainPath+'server/db.php',
					data:{
						params:{
							folderName:main.folderName,
							userID:main.userID,
							fields:fields,
							recordPkgs:recordPkgs,
							table:self.table
						},
						action:'updateDataEntry'
					},
					success:function(response){
						if(response){
							response=JSON.parse(response);
							if(response.status=="OK"){
								if(callback){callback();}
								if(main.filter_legend && main.filter_legend.active){main.filter_legend.refresh();}
								if(main.dashboard && main.dashboard.active){main.dashboard.refresh();}
							}else{
								alert('Error');
							}
						}
					}
				});
			}
		}; */
		/* this.startEditing=function(dataTD){
			dataTD.removeClass('notEditing').addClass('editing').find('.dataTableEdit').addClass('block');
			dataTD.find('.dataTableCurrent').addClass('none');
		};
		this.stopEditing=function(dataTD){
			dataTD.removeClass('editing').addClass('notEditing').find('.dataTableCurrent').removeClass('none');
			dataTD.find('.dataTableEdit').removeClass('block');
		}; */
		this.remove=function(accordian){
			self.html.remove();
			if(accordian){
				self.dataContentWrap=accordian.removeDataShelf(self);
			}
			delete self;
			delete this;
		}
		this.convertToPanel=function(){
			self.parent=self.html.parent();
			if(self.parent){
				self.top.addClass('none');
				self.panelHTML=$('<div class="panelHTML">')
					.append(self.optionsTabs.html)
					.append(self.dtButtonsTop)
					.append(self.html)
				self.panel=new Panel({
					content:self.panelHTML,
					label:'Data Management',
					title:'Data Management',
					classes:'dataTablePanel'
				});
				self.panel.addContractButton(self.convertFromPanel);
				self.panel.open();
			}
			self.updateHeaderSizes();
			if(self.activeRow){self.scrollDT(self.activeRow);}
		};
		this.convertFromPanel=function(){
			self.html.appendTo(self.parent);
			self.top.removeClass('none')
				.prepend(self.optionsTabs.html)
				.append(self.dtButtonsTop);
			self.panel.remove();
			delete self.panel;
			self.updateHeaderSizes();
			if(self.activeRow){self.scrollDT(self.activeRow);}
		};
		this.clearChecked=function(){
			self.html.find('.dtRCheckB:checked').each(function(i,ele){
				$(ele).prop('checked',false);
			});
		};
		this.scrollDT=function(pid,noCushion){
			if(self.html.find('.dtRow_'+pid).length>0){
				var rowOffset=self.html.find('.dtRow_'+pid).offset();
				var scrollTop=self.html.find('.dtRow_'+pid).closest('.dataTableWrap').scrollTop();
				var offset=self.html.find('.dtRow_'+pid).closest('.dataTableWrap').offset();
				if(offset!==undefined && scrollTop!==undefined && rowOffset!==undefined){
					var dataTableWrapOffset=offset.top;
					var newScrollTop=rowOffset.top-dataTableWrapOffset+scrollTop;
					self.blockScrollE=false;
					var cushion=80;
					if(noCushion){cushion=0;}
					self.html.find('.dtRow_'+pid).closest('.dataTableWrap').scrollTop(newScrollTop-cushion);
				}
			}
		};
		this.activateRow=function(pid){
			if(self.html.find('.dtRow_'+pid).length>0){
				self.html.find('.dtRow_'+pid).addClass('activeRow');
				self.scrollDT(pid);
			}
		};
		this.initEvents=function(){
			this.expandButton.on('click',function(){
				self.convertToPanel();
			});
			if(self.caller.properties.transfer_to/*  && self.caller.properties.transfer_to[Object.keys(self.caller.properties.transfer_to)[0]].copiesText */){
				this.html.on('change','.dtRCheckB',function(){
					$(this).closest('.dataTablePkgInnerWrap').find('.dataTableWrap .dtRow_'+$(this).closest('.tpdataTableRow').data('pid')+' .transferToCopiesInput').prop('disabled',!$(this).prop('checked'));
					if(self.html.find('.dtRCheckB:checked').length){
						self.transferToButton.removeClass('none');
					}else{self.transferToButton.addClass('none');}
				});
			}
			// $('html').on('click',function(e){
				// if($(e.target).closest('.filterContent,.filterButton').length===0){
					// self.closeFilters();
				// }
			// });
			// this.html.on('click','.filterContent ',function(){
				// $('.activeFilter').removeClass('activeFilter');
				// $(this).addClass('activeFilter');
			// });
			// this.html.on('change','.showNullsOption.rng',function(){
				// self.filterRangeChange(self.ranges[$(this).closest('.inputPkg').find('.rangeOuterWrap').attr('id')].getRangePkg());
			// });
			// this.html.on('change','.showNullsOption.chk',function(){
				// self.filterCheckChange($(this).closest('.inputPkg').data('name'),$(this).closest('.chkPkg'));
			// });
			// this.html.on('change','.filterInput.filterCheck',function(){
				// self.filterCheckChange($(this).closest('.inputPkg').data('name'),$(this).closest('.chkPkg'));
			// });
			// this.html.on('change','.showNullsOption.txt',function(){
				// self.filterTextChange($(this).closest('.inputPkg').data('name'),$(this).closest('.inputPkg').find('.textInput').val(),$(this).closest('.inputPkg'));
			// });
			// this.html.on('change','.filterInput.textInput',function(){
				// self.filterTextChange($(this).closest('.inputPkg').data('name'),$(this).val(),$(this).closest('.inputPkg'));
			// });
			// this.html.on('click','.tHeadSortArrow',function(){
				// if($(this).hasClass('tHSAsc')){
					// var sortOrder='ASC';
				// }else{
					// var sortOrder='DESC';
				// }
				// self.tableSort($(this).closest('.tableSortPkg').data('field'),sortOrder);
			// });
			// this.html.on('click','.filterButton',function(){
				// if($(this).closest('.dtFieldHeader').find('.inputPkg').length>0){
					// $('.activeFilter').removeClass('activeFilter');
					// if($(this).closest('.dtFieldHeader').find('.filterContent').is(':hidden')){
						// $(this).closest('.dtFieldHeader').find('.filterContent').addClass('block activeFilter');
						// var of
					// }else{$(this).closest('.dtFieldHeader').find('.filterContent').removeClass('block');}
				// }
			// });
			// this.html.on('click','.dtFieldHeader',function(e){
				// if($(e.target).closest('.tableSortPkg').length===0){
					// var fieldname=$(this).data('fieldname')
				// }
			// });
			this.html.on('change','.dtRCheckAll',function(){
				self.html.find('.dtRCheckB').prop('checked',$(this).prop('checked'));
			});
			this.deleteCheckedButton.html.on('click',function(){
				if(confirm("Are you sure you want to delete these?")){
					var y=confirm("Warning: You are about to permanently delete data. This cannot be undone.");
					if(y){
						var pids=self.getCheckedPIDs();
						if(pids.length>0){
							if(!self.signatureFields){self.getSignatureFields();}
							// var signatures=[];
							// for(var i=0;i<pids.length;i++){
								// for(var t=0;t<self.signatureFields.length;t++){
									// console.log(pids[i]);
									// if(self.rows[pids[i]].fields[self.signatureFields[t]].current_val){
										// signatures.push(self.rows[pids[i]].fields[self.signatureFields[t]].current_val);
									// }
								// }
							// }
							main.utilities.deleteData(pids,self.table,self.signatureFields.join(','));
							self.checkAll.checkbox.prop('checked',false);
						}
					}
				}
			});
			/* this.html.on('click','.deleteDataRowButton',function(){
				if(confirm("Are you sure you want to delete this?")){
					var pid=$(this).data('pid');
					if(pid){
						self.deleteData([pid]);
					}
				}
			}); */
			this.html.on('click','.dataTableEditLink',function(){
				// self.startEditing($(this).closest('.dataTableTd'));
				self.updateHeaderSizes();
				self.updateStickyPos();
				self.setScrollPosBar();
			});
			this.html.on('click','.dataTableDoneEditLink',function(){
				// self.stopEditing($(this).closest('.dataTableTd'));
				self.updateHeaderSizes();
				self.updateStickyPos();
				self.setScrollPosBar();
			});
			this.html.on('click','.tpdataTableRow',function(e){
				if(!self.dragging.mousemoving && !$(e.target).closest('input').length){
					// self.blockScrollE=true;
					var pid=$(this).data('pid');
					if($('.activeRow').find('.dataTableTd.editing').length && self.activeRow!=pid){
						$('.activeRow').find('.dataTableTd.editing .dataTableDoneEditLink').click();
					}
					main.layout.removeAllPopUps();
					$('.activeRow').removeClass('activeRow');
					self.activateRow($(this).data('pid'));
					if(self.zoomToButton){self.zoomToButton.addClass('inlineBlock');}
					self.activeRow=pid;
					if(main.tables[self.table].map_layer && main.tables[self.table].map_layer.layer){
						var layerSource=main.tables[self.table].map_layer.layer.olLayer.getSource();
						var features=layerSource.getFeatures();
						for(var t=0;t<features.length;t++){
							features[t].set('isActive',false);
							if(features[t].get('pid')==pid){
								features[t].set('isActive',true);
								main.mapItems.hasActive=pid;
								main.mapItems.hasActiveLayer=self.table;
								if(main.filter_legend && main.filter_legend.active){main.filter_legend.refresh(true);}
								if(main.dashboard && main.dashboard.active){main.dashboard.refresh();}
							}
						}
						main.utilities.setTopLayer(main.tables[self.table].map_layer.layer.olLayer);
					}
					//add edit pkg
					if(!$(this).hasClass('editPkgAdded')){self.addEditPkg($(this));}
				}
				self.dragging.mousemoved=false;
				self.updateStickyPos();
				self.setScrollPosBar();
			});
			this.tbody.add(this.stickyColumnsTbody).on('change','.formInput',function(){
				self.updateDataTable($(this).closest('.tpdataTableRow').data('pid'),$(this).closest('.editsWrap').data('field'),$(this),true,false);
			});
			this.tbody.on(main.controls.touchBegin,function(e){
				if($(e.target).closest('.editLink,input[type="text"],input[type="number"],textarea').length===0){
					if(!main.browser.isMobile){
						e.stopPropagation();
						self.dragging.mousedown=true;
						self.dragging.initPageX=e.pageX;
						self.dragging.initPageY=e.pageY;
						self.dragging.initScrollX=self.dataTablePkgInnerWrap.scrollLeft();
						self.dragging.initScrollY=self.dataTableWrap.scrollTop();
						self.tbody.addClass('noSelect grabbing').closest('.sidePanel,.panel').addClass('noSelect grabbing');
						$('html').on(main.controls.touchMove,self.mousemoveForDrag);
					}
				}
			});
			$('html').on(main.controls.touchEnd,function(e){
				if(self.dragging.mousedown){
					self.dragging.mousedown=false;
					self.tbody.removeClass('noSelect grabbing').closest('.sidePanel,.panel').removeClass('noSelect grabbing');
					$('html').off(main.controls.touchMove,self.mousemoveForDrag);
					if(self.dragging.mousemoved){
						self.dataTableWrapScroll();
					}
					self.dragging.mousemoving=false;
				}
			});
			self.dataTablePkgInnerWrap.add(self.dataTableWrap).on('scroll',function(e){
				self.updateStickyPos();
				self.setScrollPosBar();
			});
			// self.dataTableWrap.on('scroll',function(e){
				// self.setScrollPosBar();
			// });
		};
		this.mousemoveForDrag=function(e){
			e.preventDefault();
			var thisX=e.pageX;
			var thisY=e.pageY;
			self.dragging.mousemoving=true;
			self.dragging.mousemoved=true;
			var deltaX=self.dragging.initPageX-thisX;
			var deltaY=self.dragging.initPageY-thisY;
			var newScrollLeft=self.dragging.initScrollX+deltaX;
			var newScrollTop=self.dragging.initScrollY+deltaY;
			var dataTableWrapWidth=self.dataTableWrap.outerWidth(true);
			var innerWrapWidth=self.dataTablePkgInnerWrap.width();
			if(newScrollLeft>=(dataTableWrapWidth-innerWrapWidth)){
				newScrollLeft=(dataTableWrapWidth-innerWrapWidth);
				self.dragging.initPageX=thisX;
				self.dragging.initScrollX=newScrollLeft;
			}
			if(newScrollLeft<=0){
				newScrollLeft=0;
				self.dragging.initPageX=thisX;
				self.dragging.initScrollX=newScrollLeft;
			}
			var dataTableTableHeight=self.dataTableTable.outerHeight(true);
			var dataTableWrapHeight=self.dataTableWrap.height();
			if(newScrollTop>=(dataTableTableHeight-dataTableWrapHeight)){
				newScrollTop=(dataTableTableHeight-dataTableWrapHeight);
				self.dragging.initPageY=thisY;
				self.dragging.initScrollY=newScrollTop;
			}
			if(newScrollTop<=0){
				newScrollTop=0;
				self.dragging.initPageY=thisY;
				self.dragging.initScrollY=newScrollTop;
			}
			self.dataTablePkgInnerWrap.scrollLeft(newScrollLeft);
			self.dataTableWrap.scrollTop(newScrollTop);
			self.updateStickyPos();
			self.setScrollPosBar();
		};
		this.updateStickyPos=function(data){
			self.updateStickyColsSizes();
			var headerHeight=self.stickyTableHeader.outerHeight(true);
			self.stickyHeaderWrap.css('left',self.dataTablePkgInnerWrap.scrollLeft()+'px');
			self.dtStickyColsWrap.css('left',self.dataTablePkgInnerWrap.scrollLeft()+'px');
			self.dtStickyColsWrap.css('top',(-self.dataTableWrap.scrollTop()+headerHeight)+'px');
		};
		this.updateCurrentVals=function(data){
			var pid=data.pid,row,field,options,fields,value,html;
			if(self.rows[pid]){
				row=self.rows[pid];
				fields=self.rows[pid].fields;
				for(var fieldName in data){
					// if(fields.field){fields=fields.field}
					if(fields[fieldName]){
						field=fields[fieldName].field;
						if(field.active){
							value=data[fieldName];
							self.rows[pid].fields[fieldName].current_val=value;
						}
					}
				}
				return pid;
			}
		};
		this.updateDataTable=function(pid,fieldName,primaryE,updateDB,updateNoOtherTables,setInputs,val){
			var value,html,changeFields=[];
			if(self.rows[pid]){
				var field=self.rows[pid].fields[fieldName].field;
				if(val){var primaryEVal=val;
				}else{var primaryEVal=primaryE.val();}
				var input=self.html.find('.tpdataTableRow.dtRow_'+pid+' .dataTableTd.dtField_'+fieldName+' .dataTableEdit .formInput.'+fieldName)
				//get db value and visible html
				if(field.input_type=='checkbox'){
					// html='';
					if(!val){
						var chkStuff=main.utilities.getCheckboxValue(primaryE.closest('.checkboxesWrap').find('.checkboxInput'),field);
						value=chkStuff.value;
						html=chkStuff.html;
					}else{
						value=primaryEVal;
						html=main.utilities.getHTMLFromVal();
					}
					// main.utilities.setCheckboxes(input.closest('.checkboxesWrap').find('.checkboxInput'),chkStuff.value.split(';;'),field);
					if(value && typeof value=='string'){
						/* html= */main.utilities.setCheckboxes(input.closest('.checkboxesWrap').find('.checkboxInput'),value.split(';;'),field);
					}else if(field.data_type=='boolean'){
						if(value){
							if(field.options['true'] && field.options['true'].alias){html=field.options['true'].alias;}else{html='True';}
							value=true;
							input.prop('checked',true);
						}else{
							if(field.options['true'] && field.options['true'].offAlias){html=field.options['true'].offAlias;}else{html='False';}
							value=false;
							input.prop('checked',false);
						}
					}
				}else if(field.input_type=='radio'){
					value=primaryEVal;
					html='';
					if(value!==null){
						input.filter('*[value="'+value+'"]').prop('checked',true);
						if((value || value==0) && field.options[value]){html=field.options[value].alias;}
					}
				}else if(field.input_type=='date'){
					input.val(primaryEVal);
					if(field.sub_data_type=='season'){
						value=primaryEVal;
						if(value && !isNaN(value)){
							var seasonStuff=main.utilities.getSeasonFromTimeStamp(Number(value));
							html=main.utilities.formatSeason(seasonStuff);
							input.closest('.seasonDateContainer').find('.seasonSelect').val(seasonStuff.season);
							input.closest('.seasonDateContainer').find('.yearSelect').val(seasonStuff.year);
						}else{
							html='';
							input.closest('.seasonDateContainer').find('.seasonSelect').val('');
							input.closest('.seasonDateContainer').find('.yearSelect').val('');
						}
					}else if(field.sub_data_type=='time'){
						value=primaryEVal;
						var timePkg;
						if(timePkg=main.utilities.msToTime(value)){
							html=timePkg.html
						}else{html='';}
						main.utilities.setTimePkg(timePkg,input);
					}else{
						if(!val){
							value=main.utilities.getTimeStamp(primaryEVal);
							html=primaryEVal;
						}else{
							if(primaryEVal){
								value=primaryEVal;
								html=main.utilities.formatDate(primaryEVal);
							}
						}
					}
				}else if(field.input_type=='file'){
					var fileObjs=main.utilities.getFileValue(primaryE);
					value=JSON.stringify(fileObjs.fileObjs);
					html=fileObjs.filesStr;
				// }else if((field.input_type=="map_point" || field.input_type=="map_polygon")/*  && val && typeof val=='object' */){
						// var geoData=self.getGeometryData(val);
						// obItem.fields[fieldName].html=geoData.html;
						// html='Edit to View';
						// value=geoData.valStr;
					}else{
					input.val(primaryEVal);
					html=primaryEVal;
					value=primaryEVal;
					//check for look up table
					if(field.lookup_table){
						var a;
						if(a=main.utilities.getLookUpTableHTML(field.lookup_table,value,fieldName,self.table,field)){
							html=a;}
						// var a=self.rows[pid].fields[fieldName].input.lookUpTable.getLookUpTableHTML(self.updateLookUpTableFields,{pid:pid,fieldName:fieldName,value:primaryEVal});
						// if(a!==false){html=a;}
						if(self.callerProperties.lookup_table_map && !main.misc.blockUpdateData && !main.misc.blockUpdateData2){
							var luFields=Object.keys(self.callerProperties.lookup_table_map[field.lookup_table]);
							luFields.splice(luFields.indexOf,1);
							for(var m=0;m<luFields.length;m++){
								if(luFields[m]!=fieldName){changeFields.push({field:self.rows[pid].fields[luFields[m]].field,val:value});}
							}
						}
					}else{
						if(field.options){
							if(field.options[primaryEVal]){
								html=field.options[primaryEVal].alias;
							}
						}
					}
				}
				if(msg=main.utilities.validateData(value,field)){
					alert(msg);
					return;
				}
				self.rows[pid].fields[fieldName].html=html || '';
				self.rows[pid].fields[fieldName].current_val=value;
				self.html.find('.tpdataTableRow.dtRow_'+pid+' .dataTableTd.dtField_'+fieldName+' .dataTableCurrent').html(self.rows[pid].fields[fieldName].html);
				//**update database
				// self.updateTable(pid,fieldName,value);
				if(!main.misc.blockUpdateData && !main.misc.blockUpdateData2 && updateDB){
					var inventory=null,form=null,props;
					if(main.invents && main.invents.inventories[self.table]){
						inventory=main.invents.inventories[self.table];
						props=inventory.properties;
					}
					if(main.forms && main.forms[self.table]){
						form=main.forms[self.table];
						props=form.properties;
					}
					// if(props && props.internal_field_map  && props.internal_field_map.last_modified){
						// self.rows[pid].fields[props.internal_field_map.last_modified].current_val=new Date().getTime();
						// changeFields.push(props.internal_field_map.last_modified);
					// }
					var ecoVals=null;
					if(props){var fieldMap=props.internal_field_map;}
					if(fieldMap && fieldMap.dbh && fieldMap.main_species_field){
						var landUseVal='';
						if(self.rows[pid].fields['landuse'] && props.eco_fields_map && props.eco_fields_map[self.rows[pid].fields['landuse'].current_val]){
							landUseVal=props.eco_fields_map[self.rows[pid].fields['landuse'].current_val];
						}
						ecoVals=main.utilities.getEcoFields(fieldMap,self.rows[pid].fields[fieldMap.dbh].current_val,self.rows[pid].fields[fieldMap.main_species_field].current_val,landUseVal);
					}
					changeFields.push({field:self.rows[pid].fields[fieldName].field,val:value});
					main.dataEditor.updateData({
						pids:[pid],
						context:self.caller,
						form:form,
						ecoVals:ecoVals,
						changeFields:changeFields
					});
					// self.updateData({pids:[pid]});
				}
				
				//update filter
				// if(field.input_type=='date' || (main.misc.numberDTs.indexOf(field.data_type)>-1 && !field.options)){
					// var isDate=false;
					// if(field.input_type=='date'){isDate=true;}
					// self.updateRangeFilters(fieldName,isDate);
				// }
				// if(field.lookup_table){
					// self.updateLookUpTable(self.tableHeaderLabels.find('.dtFieldHeader.'+fieldName+' .inputPkgContent'),fieldName,field.lookup_table);
				// }
				
				//update other forms
				if(!updateNoOtherTables && main.tables[self.table].map_layer && main.tables[self.table].map_layer.features && main.tables[self.table].map_layer.features[pid] && main.tables[self.table].map_layer.features[pid].popup){
					main.tables[self.table].map_layer.features[pid].inventory.updateTable(pid,fieldName,primaryE,false,true);
				}
				if(self.callerProperties.special_connections && self.callerProperties.special_connections[fieldName]){
					main.utilities.doSpecialConnection(value,self.callerProperties.special_connections[fieldName],primaryE);
					// main.utilities.doSpecialConnection(value,self.inventory.properties.special_connections[fieldName],self.fields[to_effect],primaryE);
				}
			}
		};
		this.updateRangeFilters=function(fieldName,isDate){
			var extremes=self.getExtremes(fieldName,isDate);
			var range=self.ranges[this.tableHeaderLabels.find('.dtFieldHeader.'+fieldName+' .rangeOuterWrap').attr('id')];
			if(range){
				range.updateExtremes(extremes.max,extremes.min);
				range.reset();
				self.filterRangeChange(range.getRangePkg());
			}
		};
		/* this.updateTable=function(pid,fieldName,value,updateEI){
			var html=value;
			var input_type=self.rows[pid].fields[fieldName].field.input_type;
			if(updateEI){
				if(input_type=='checkbox'){
					if(value){
						self.html.find('.tpdataTableRow.dtRow_'+pid+' .dataTableTd.dtField_'+fieldName+' .dataTableEdit .formInput.'+fieldName).prop('checked',true);
					}else{
						self.html.find('.tpdataTableRow.dtRow_'+pid+' .dataTableTd.dtField_'+fieldName+' .dataTableEdit .formInput.'+fieldName).prop('checked',false);
					}
				}else if(input_type=='radio'){
					if(value){
						self.html.find('.tpdataTableRow.dtRow_'+pid+' .dataTableTd.dtField_'+fieldName+' .dataTableEdit .formInput.'+fieldName).prop('checked',true);
					}
				}else{
					self.html.find('.tpdataTableRow.dtRow_'+pid+' .dataTableTd.dtField_'+fieldName+' .dataTableEdit .formInput.'+fieldName).val(value);
				}
			}
			if(self.rows[pid].fields[fieldName].input && self.rows[pid].fields[fieldName].input.field.options){
				// if(input_type=='checkbox' && !value){value=null;}
				if(self.rows[pid].fields[fieldName].input.field.options[value]){
					html=self.rows[pid].fields[fieldName].input.field.options[value].alias;
				}else if(self.fields[fieldName].default_off_alias){
					html=self.fields[fieldName].default_off_alias;
				}
			}
			if(self.rows[pid].fields[fieldName].field.lookup_table){
				var a=self.rows[pid].fields[fieldName].input.lookUpTable.getLookUpTableHTML(self.updateLookUpTableFields,{pid:pid,fieldName:fieldName,value:value});
				if(a!==false){html=a;}
			}
			if(input_type=='date'){
				value=main.utilities.getTimeStamp(value);
			}
			if(input_type=='file'){
				html='';
				for(var i=0;i<value.length;i++){
					html+=value[i].fileName+',';
				}
				html=html.replace(/,([^,]*)$/,'$1');
				value=JSON.stringify(fileObj);
			}
			self.rows[pid].fields[fieldName].html=html || '';
			self.rows[pid].fields[fieldName].current_val=value;
			if(self.fields[fieldName].input_type=='map_point'){
				self.rows[pid].fields[fieldName].html=main.utilities.convertMapPointInput(value);
			}
			self.html.find('.tpdataTableRow.dtRow_'+pid+' .dataTableTd.dtField_'+fieldName+' .dataTableCurrent').html(self.rows[pid].fields[fieldName].html);
		}; */
		this.updateLookUpTableFields=function(params){
			html=params.lookUpTable.getItemHTML(params.value);
			self.rows[params.pid].fields[params.fieldName].html=html;
			self.htmlRowUpdate(null,{pid:params.pid,fieldName:params.fieldName})
		}
		this.htmlRowCheck=function(params){
			if(self.rows[params.pid]){return true;}else{return false;}
		};
		this.htmlRowUpdate=function(results,params){
			self.rows[params.pid].htmlRow.find('.dataTableTd.dtField_'+params.fieldName+' .dataTableCurrent').html(self.rows[params.pid].fields[params.fieldName].html);
		};
		this.dbPhotoRemoved=function(response){
			// if(main.tables[self.table].map_layer && main.tables[self.table].map_layer.features[pid] && main.tables[self.table].map_layer.features[pid].popup){
				// main.tables[self.table].map_layer.features[pid].inventory.updateTable(pid,fieldName,primaryE,false,true);
			// }
		};
		this.photoRemoved=function(args){
			if(args.params.pid){
				main.utilities.removePhoto(args.name,args.params.field.referencer,args.params.field.name,args.params.pid,self.dbPhotoRemoved);
			}
		};
		this.convertToRow=function(key,value,pid){
			self.rows[pid].fields[key]={
				current_val:value,
				html:value,
				field:self.fields[key]
			};
			main.utilities.getRowThings(self.rows[pid],self.fields[key],pid,{callback:self.updateLookUpTableFields,params:{pid:pid,fieldName:key,value:value}},{callback:self.newSignature},null,null,self.table,false,true);
			if(self.fields[key].input_type=='file' && self.rows[pid].fields[key].input){
				self.rows[pid].fields[key].input.currentPhotos.setRemoveCallBack(self.photoRemoved);
				self.rows[pid].fields[key].input.currentPhotos.setRemoveCallBack(main.utilities.photoELClicked);
			}
		};
		this.convertResultsToObject2=function(results,shortLoad/* ,omitShortLoadConstraint */){
			var value,pid;
			for(var i=0;i<results.length;i++){
				pid=results[i]['pid'];
				if(self.rows[pid]){
					if(self.rows[pid].htmlRow){
						self.rows[pid].htmlRow.remove();
						delete self.rows[pid].htmlRow;
					}
				}
				self.rows[pid]={
					fields:{},
					info:{},
					fullyLoaded:false
				};
				for(var a=0;a<self.sortedFieldKeys.length;a++){
					self.rows[pid].info[self.sortedFieldKeys[a]]=results[i][self.sortedFieldKeys[a]]
					if(self.inTableTest(self.sortedFieldKeys[a],shortLoad)){
						self.convertToRow(self.sortedFieldKeys[a],results[i][self.sortedFieldKeys[a]],pid);
					}
				}
				if(main.tables[self.table].map_layer && main.tables[self.table].map_layer.features && main.tables[self.table].map_layer.features[pid]){main.tables[self.table].map_layer.features[pid].dataTable=self;}
				self.createNewRow(pid,shortLoad);
			}
			self.sortDataKeys=Object.keys(self.rows);
		};
		this.loadDTIt=function(result,dir){
			var pid=result['pid'].val;
			if(main.tables[self.table].map_layer && main.tables[self.table].map_layer.features && main.tables[self.table].map_layer.features[pid]){main.tables[self.table].map_layer.features[pid].dataTable=self;}
			var content=self.createAliasDataRow(pid,result);
			if(dir=='prev'){
				self.tbody.prepend(content.tr);
				self.stickyColumnsTbody.prepend(content.stickyTR);
			}else{
				self.tbody.append(content.tr);
				self.stickyColumnsTbody.append(content.stickyTR);
			}
		}
		this.loadDT=function(results,dir){
			if(dir=='prev'){
				for(var p=results.length-1;p>=0;p--){
					self.loadDTIt(results[p],dir);
				}
				if(self.lastLoadedFirstItemLast){self.scrollDT(self.lastLoadedFirstItemLast,true);}
			}else{
				for(var p=0;p<results.length;p++){
					self.loadDTIt(results[p],dir);
				}
			}
			var scrollOffset=0;
			if(self.dataTableWrap.scrollTop()==0){self.dataTableWrap.scrollTop(scrollOffset);
			}else if(self.dataTableTable.outerHeight(true)-self.dataTableWrap.height()==self.dataTableWrap.scrollTop()){
				self.dataTableWrap.scrollTop(self.dataTableWrap.scrollTop()-scrollOffset);
			}
			// self.sortDataKeys=Object.keys(self.rows);
		};
		this.convertResultsToObject=function(results,shortLoad/* ,omitShortLoadConstraint */){
			var value,pid;
			for(var i=0;i<results.length;i++){
				pid=results[i]['pid'];
				if(self.rows[pid]){
					if(self.rows[pid].htmlRow){
						self.rows[pid].htmlRow.remove();
						delete self.rows[pid].htmlRow;
					}
				}
				self.rows[pid]={
					fields:{},
					info:{},
					fullyLoaded:false
				};
				for(var a=0;a<self.sortedFieldKeys.length;a++){
					self.rows[pid].info[self.sortedFieldKeys[a]]=results[i][self.sortedFieldKeys[a]]
					if(self.inTableTest(self.sortedFieldKeys[a],shortLoad)){
						self.convertToRow(self.sortedFieldKeys[a],results[i][self.sortedFieldKeys[a]],pid);
					}
				}
				if(main.tables[self.table].map_layer && main.tables[self.table].map_layer.features && main.tables[self.table].map_layer.features[pid]){main.tables[self.table].map_layer.features[pid].dataTable=self;}
				self.createNewRow(pid,shortLoad);
			}
			self.sortDataKeys=Object.keys(self.rows);
			
			
			
			/* 	var keys=self.sortDataKeys;
			// }
			for(var i=0;i<keys.length;i++){
				if(main.tables[self.table].map_layer && main.tables[self.table].map_layer.features && main.tables[self.table].map_layer.features[keys[i]]){
					main.tables[self.table].map_layer.features[keys[i]].dataTable=self;}
				self.createNewRow(keys[i]/* ,omitShortLoadConstraint);
			} */
		};
		this.initData=function(shortLoad/* pids *//* ,omitShortLoadConstraint */){
			// self.populateTable(/* pids *//* ,omitShortLoadConstraint */);
			// self.addFilters(shortLoad/* omitShortLoadConstraint */);
			self.updateHeaderSizes();
			// self.initiateSort(shortLoad/* omitShortLoadConstraint */);
			// if(self.callerProperties && self.callerProperties.sort_by){
				// self.sortHTMLTable();
			// }
		};
		this.getPIDs=function(where){
			if(!where){
				where=null;
				if(self.callerProperties && self.callerProperties.dependent_upon && self.callerProperties.internal_field_map && self.callerProperties.internal_field_map.dependent_upon_field && self.caller.inventories.currentFilterPID && !main.account.loggedInUser.dont_filter){
					var dependent_upon_field=self.callerProperties.internal_field_map.dependent_upon_field;
					where=dependent_upon_field+"='"+self.caller.inventories.currentFilterPID+"'";
				}
			}
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						where:where,
						table:self.table
					},
					action:'getPIDs'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							self.lastLoadedPossiblePids=response.pids;
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.retrieveData=function(args){
			self.initiallyLoaded=true;
			if(!args){args={};}
			var fieldPkg={};
			for(var key in self.fields){
				if(self.inTableTest(key,false)){
					fieldPkg[key]={
						data_type:self.fields[key].data_type,
						input_type:self.fields[key].input_type,
						options:self.fields[key].options,
						lookup_table:self.fields[key].lookup_table,
						name:self.fields[key].name,
						referencer:self.fields[key].referencer
					}
				}
			}
			var where=args.where || '';
			if(self.callerProperties && self.callerProperties.dependent_upon && self.callerProperties.internal_field_map && self.callerProperties.internal_field_map.dependent_upon_field && !main.account.loggedInUser.dont_filter){
				if(main.account.loggedInUser && main.account.loggedInUser.org_data && !self.callerProperties[main.account.loggedInUser.user_type+'_download_all']){
					var filterPID=main.account.loggedInUser.org_data.orgId; 
				}else if(self.caller.inventories.currentFilterPID){
					var filterPID=self.caller.inventories.currentFilterPID;
				}
				if(filterPID){
					var dependent_upon_field=self.callerProperties.internal_field_map.dependent_upon_field;
					if(where){where+=' AND ';}
					where=dependent_upon_field+"='"+filterPID+"'";
				}
			}
			var pids=args.pids || [];
			self.advFilterApplied=false;
			if(main.advFilter && main.advFilter.appliesTo['data_table'].apply && main.advFilter.currentLayer && main.advFilter.currentLayer.properties.layer_name==self.table){
				if(main.advFilter.where){
					if(where){where+=' AND ';}
					where+='('+main.advFilter.where+')';
					self.advFilterApplied=true;
				}
				if(main.advFilter.geoFilter && main.advFilter.geoFilter.length){
					if(pids.length){
						for(var i=0;i<pids.length;i++){
							if(main.advFilter.geoFilter.indexOf(Number(pids[i]))==-1){
								pids.splice(i,1);
							}
						}
					}else{pids=main.advFilter.geoFilter;}
					self.advFilterApplied=true;
				}
			}
			if(pids.length==0){pids=null;}
			if(pids){pids=pids.join(',');}
			if(main.filter_legend && main.filter_legend.active && !main.filter_legend.hideOnly && main.filter_legend.filterWhere && main.filter_legend.layerName==self.table){
				if(where){where+=' AND ';}
				where+='('+main.filter_legend.filterWhere+')';
			}
			var searchForOne=args.searchForOne || null;
			var lookup_table_map=null;
			if(self.callerProperties.lookup_table_map){lookup_table_map=self.callerProperties.lookup_table_map;}
			var firstIndex=null;
			if((self.currentStartIndex || self.currentStartIndex==0)){firstIndex=self.currentStartIndex;}
			if(!isNaN(self.limitInput.val())){var limit=Number(self.limitInput.val());
			}else{var limit=self.maxResultsPerPage;}
			var nBefore=0;
			var nAfter=0;
			if(args.nBefore){
				if(args.nBefore=='std'){nBefore=Math.ceil(limit/2);}
				else{nBefore=args.nBefore}
			}
			if(args.nAfter){
				if(args.nAfter=='std'){nAfter=Math.ceil(limit/2);}
				else{nAfter=args.nAfter}
			}
			var dir=args.dir || null;
			var scrollToBOnLoad=args.scrollToBOnLoad || null;
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						firstIndex:firstIndex,
						limit:limit,
						// firstIndex:0,
						// limit:1500,
						pids:pids,
						dir:dir,
						nBefore:nBefore,
						nAfter:nAfter,
						searchForOne:searchForOne,
						sortBy:self.currentSortBy,
						sortBy2:self.currentSortBy2,
						sortBy3:self.currentSortBy3,
						sortBy4:self.currentSortBy4,
						sortOrder:self.currentSortOrder,
						sortOrder2:self.currentSortOrder2,
						sortOrder3:self.currentSortOrder3,
						sortOrder4:self.currentSortOrder4,
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						table:self.table,
						where:where,
						lookup_table_map:lookup_table_map,
						scrollToBOnLoad:scrollToBOnLoad,
						fields:fieldPkg/* ,
						rows0:rows0 */
					},
					action:'retrieveDataAlias'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							// self.currentPIDs=response.returnPids;
							if(response.resultsArray.length>0){
								var dir=null;
								if(response.scrollpids && response.dir){
									dir=response.dir;
									if(dir=='prev'){
										self.lastLoadedFirstItemLast=self.lastLoadedPossiblePids[self.lastLoadedFirstItem];
										for(var b=response.resultsArray.length-1;b>=0;b--){
											self.lastLoadedResults[response.resultsArray[b]['pid'].val]=response.resultsArray[b];
											self.lastLoadedResultsArray.unshift(response.resultsArray[b]);
											self.lastLoadedFirstItem=self.lastLoadedPossiblePids.indexOf(response.resultsArray[b]['pid'].val);
										}
										self.blockScrollE=true;
									}else if(dir='post'){
										for(var b=0;b<response.resultsArray.length;b++){
											self.lastLoadedResults[response.resultsArray[b]['pid'].val]=response.resultsArray[b];
											self.lastLoadedResultsArray.push(response.resultsArray[b]);
											self.lastLoadedLastItem=self.lastLoadedPossiblePids.indexOf(response.resultsArray[b]['pid'].val);
										}
									}
								}else{
									self.lastLoadedResults=response.results;
									self.lastLoadedResultsArray=response.resultsArray;
									self.lastLoadedPossiblePids=response.returnPids;
									self.lastLoadedCurrentIdx=response.idx;
									self.lastLoadedFirstItem=response.firstItem;
									self.lastLoadedLastItem=response.lastItem;
									self.blockScrollE=true;
								}
								// self.convertResultsToObject(response.results,true);
								self.loadDT(response.resultsArray,dir);
								// self.convertResultsToObject2(response.results,true);
								// self.initData(false);
								// if(response.sfo){self.activateRow(response.sfo);}
							// return;
								self.loaded=true;
								self.prevIndex=self.currentStartIndex;
								self.currentStartIndex+=response.resultsArray.length;
								self.actualStartIndex=self.currentStartIndex;
								// self.indexStartInput.val(self.currentStartIndex+1);
								self.pageDetailsCurrentCount.html(self.tbody.children().length);
								self.pageDetailsOutOfCount.html(self.lastLoadedPossiblePids.length);
								
								if(self.callerProperties && self.callerProperties.dependent_upon && self.callerProperties.internal_field_map && self.callerProperties.internal_field_map.dependent_upon_field && self.caller.inventories.currentFilterPID){
									var dependent_upon_field=self.callerProperties.internal_field_map.dependent_upon_field;
									var field=self.fields[dependent_upon_field];
									var lookupTableMap=null;
									if(self.callerProperties.lookup_table_map){lookupTableMap=self.callerProperties.lookup_table_map;}
									self.pageFilterDetailsHTML.html(' Showing where <span class="ul">'+field.alias+'</span> is <span class="ul">'+main.utilities.getHTMLFromVal(self.caller.inventories.currentFilterPID,field,lookupTableMap)+'</span>.');
								}
								/* if(self.currentSortBy=='last_modified'){
									var orderedByHTML=' Sorted by <span class="ul">your most recent edits</span>.';
								}else{ */
									var orderedByHTML=' Sorted by <span class="ul">'+self.fields[self.currentSortBy].alias+'</span> in <span class="ul">'+self.sortOptions[self.currentSortOrder].alias+'</span> order.'
								// }
								self.pageOrderedByDetailsHTML.html(orderedByHTML);
								var advFIsActiveHTML='';
								if(self.advFilterApplied){advFIsActiveHTML='Advanced filter is applied.';}
								self.advFIsActive.html(advFIsActiveHTML);
								self.updateHeaderSizes();
								if(args.callback){args.callback(args);}
								if(response.sfo){self.activateRow(response.sfo);}
							}
							if(response.params.scrollToBOnLoad){self.sidePanelScrollToBottomOfDT();}
							main.layout.loader.loaderOff();
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			}).done(function(){self.loading=false;});
		};
		// this.getPIDs();
		this.createHTML();
		this.initEvents();
		/* if(this.loadData!==false){
			// this.populateLabelRow();
			// this.retrieveData();
		} */
	};
});
