define(function(require){
	return function pg(type,classes,content,data,extras,iconPath,title,toolTipContent){
		var self=this;
		var ToolTip=require('./ToolTip');
		this.classes='';
		this.type='';
		this.content='';
		this.data=[];
		this.extras=extras;
		this.iconPath=iconPath;
		this.title=title;
		this.toolTipContent=toolTipContent;
		
		if(classes) this.classes=classes;
		if(type) this.type=type;
		if(content) this.content=content;
		if(data) this.data=data;
		
		this.changeIcon=function(path){
			self.iconImg.attr('src',path);
		};
		this.createButton=function(){
			if(this.type=='roundedSquare'){
				var content=$('<div>').addClass('iconButtonIWrap')
					.append($('<div>').addClass('iconButtonContent').html(this.content))
				if(this.iconPath){
					self.iconImg=$('<img src="'+this.iconPath+'" class="iconButtonIcon">');
					content.prepend($('<div>').addClass('iconButtonIconWrap').html(self.iconImg));
				}
				this.html=$('<div>').addClass('button cf symButton noSelect '+this.classes)
					.append($('<div>').addClass('buttonContent buttonEle').html(content));
			}else if(this.type=='toolBox'){
				this.html=$('<div>').addClass('button cf toolBoxButton noSelect '+this.classes)
					.append($('<div>').addClass('buttonContent buttonEle').html(this.content));
			}else if(this.type=='std'){
				this.html=$('<div>').addClass('button cf stdButton noSelect '+this.classes)
					.append($('<div>').addClass('buttonContent buttonEle').html(this.content));
			}else if(this.type=='mobileMenu'){
				var mobileMenuItemIcon='';
				if(this.iconPath){
					self.iconImg=$('<img src="'+this.iconPath+'" class="mobileMenuItemIconImg">');
					mobileMenuItemIcon=$('<div class="mobileMenuItemIcon">').append(self.iconImg);
				}
				this.html=$('<div class="mobileMenuItem button cf noSelect '+this.classes+'">')
					.append(mobileMenuItemIcon)
					.append($('<div class="mobileMenuItemLabel">').html(this.content))
			}else if(this.type=='icon'){
				var content=$('<div>').addClass('iconButtonIWrap')
					.append($('<div>').addClass('iconButtonContent').html(this.content))
				if(this.iconPath){
					self.iconImg=$('<img src="'+this.iconPath+'" class="iconButtonIcon">');
					content.prepend($('<div>').addClass('iconButtonIconWrap')
						.append($('<div>').addClass('iconButtonIconInner')
							.append(self.iconImg)
						)
					);
				}
				this.html=$('<div>').addClass('button cf onlyIconButton noSelect '+this.classes)
					.append($('<div>').addClass('buttonContent buttonEle').html(content));
			}
			if(this.title){this.html.attr('title',this.title)}
			if(this.extras){this.appendExtras(this.extras);}
			if(this.data.length>0){
				for(var i=0;i<this.data.length;i++){
					this.html.data(this.data[i][0],this.data[i][1])
				}
			}
			if(this.toolTipContent){
				self.toolTip=new ToolTip({
					content:self.toolTipContent,
					overDoc:true
				});
				self.html.on('mouseenter',function(){
					self.toolTip.activate();
				});
				self.html.on('mouseleave',self.toolTip.deActivate);
			}
		};
		this.appendExtras=function(extras){
			if(extras=='data'){
				this.bExtra=$('<div>').addClass('buttonExtra dataExtra').text('Table').attr('title','Manage Data');
				this.html.append($('<div>').addClass('buttonExtras buttonEle').append(this.bExtra));
			}else if(extras=='upload'){
				this.bExtra=$('<div>').addClass('buttonExtra uploaderExtra').text('Upload').attr('title','Upload Data');
				this.html.append($('<div>').addClass('buttonExtras buttonEle').append(this.bExtra));
			}
			return this.bExtra;
		};
		this.createButton();
	};
});