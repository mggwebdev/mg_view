define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Checkbox=require('./Checkbox');
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.classes=this.classes || '';
		this.elItems={};
		this.addExportButton=function(){
			self.exportButton=new Button('icon','elExportPhotos','',null,null,main.mainPath+'images/download_white_small.png','Export');
			self.buttonsWrap.append(self.exportButton.html);
			if(self.exportCB){
				self.exportButton.html.on('click',self.exportCB);
			}
		};
		this.createEditableList=function(){
			this.list=$('<ul>').addClass('editableList');
			this.buttonsWrap=$('<div>').addClass('editableListButtonsWrap')
			this.container=$('<div>').addClass('editableListWrap')
				.append(this.list)
				.append(this.buttonsWrap);
			if(this.checkable){
				this.checkAll=new Checkbox({
					label:'Toggle All',
					value:'',
					checked:false,
					name:'',
					classes:'elToggleAll'
				});
				this.container.prepend(this.checkAll.html)
			}
		};
		this.setRemoveCallBack=function(cb){
			self.removeCallBack=cb;
		};
		this.setOnClick=function(cb){
			self.onClick=cb;
		};
		this.addItem=function(name,html,params,extras){
			this.elItems[name]={
				container:$('<li data-name="'+name+'">').addClass('editableListItem cf '+self.classes)
					.append($('<div>').addClass('elLabel').html(html)),
				params:params,
				html:html,
				name:name
			}
			if(this.checkable){
				this.elItems[name].container.prepend(new Checkbox({
					value:name,
					name:'layerCheck',
					wrapClasses:'fLeft',
					classes:'checkInput eLCheck fLeft'
				}).html)
			}
			if(this.removeable) this.elItems[name].container.prepend($('<div>').addClass('eLRemove fLeft button').text('x'))
			this.list.append(this.elItems[name].container);
			if(this.exportable && !this.exportButton){this.addExportButton();}
			return this.elItems[name];
		};
		this.removeItem=function(name){
			this.list.find('li[data-name="'+name+'"]').remove();
			// delete this.elItems[name];
			if(!Object.keys(this.elItems).length){
				self.exportButton.html.remove();
				delete self.exportButton;
			}
		};
		this.clear=function(){
			this.list.html('');
		};
		this.editItemHTML=function(eiItem,html){
			eiItem.find('.elLabel').html(html);
		};
		this.initEvents=function(){
			this.list.on('click','.eLRemove',function(){
				var name=$(this).closest('.editableListItem').data('name');
				self.removeItem(name);
				if(self.removeCallBack){self.removeCallBack(self.elItems[name]);}
			});
			this.list.on('click','.editableListItem',function(e){
				// if($(e.target).closest('input[type="checkbox"]').length===0 && ){
					// $(this).find('.eLCheck').click();
				// }
				if($(e.target).closest('.eLRemove').length==0){
					if(self.onClick){self.onClick(self.elItems[$(this).data('name')]);}
				}
			});
			if(this.checkAll){
				this.checkAll.checkbox.on('click',function(e){
					$(this).closest('.editableListWrap').find('.eLCheck').prop('checked',$(this).prop('checked'));
				});
			}
		};
		this.createEditableList();
		this.initEvents();
	};
});