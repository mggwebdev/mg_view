define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var MapTools=require('./MapTools');
		// var Legend=require('./Legend');
		this.settings=main.globalSettings.map;
		this.personal_settings=main.personal_settings.map;
		this.tile_source=main.utilities.cloneObj(main.data.std_settings.tile_source);
		this.tiles=main.utilities.cloneObj(main.data.std_settings.tiles);
		this.tile_source_keys=this.tile_source.sortedKeys;
		this.tiles_keys=this.tiles.sortedKeys;
		this.alias='Map';
		// if(main.browser.isMobile){this.defaultTiles='bing';}else{this.defaultTiles='bing';}
		this.defaultTiles=this.personal_settings.tile_source;
		if(!this.defaultTiles){this.defaultTiles='bing';}
		this.defaultTile=this.personal_settings.tile;
		this.withItem='with_basemap_options';
		this.google={};
		this.currentTiles=null;
		this.currentSource=null;
		this.baseTiles=null;
		this.vectorLayers={};
		this.mapPoints={};
		this.mapPolygons={};
		this.geocoders={};
		this.maxSafeZoom=19;
		this.tilesLoadedCt=0;
		this.maxGSafeZoom=21;
		this.waitingForLocation=false;
		if(this.personal_settings.max_zoom || this.personal_settings.max_zoom===0){
			this.maxZoom=Number(this.personal_settings.max_zoom);
		}else{this.maxZoom=this.maxSafeZoom;}
		this.maxGZoom=this.maxZoom || this.maxGSafeZoom;
		if(this.personal_settings.min_zoom || this.personal_settings.min_zoom===0){
			this.minZoom=Number(this.personal_settings.min_zoom);
		}else{this.minZoom=1;}
		this.minGZoom=this.minZoom;
		// this.legends={};
		this.extraInteractions={};
		this.selecting=false;
		this.shiftDown=false;
		this.currentVectorLayerID=null;
		this.topZIndex=999;
		this.setParams=function(){
			this.params=main.utilities.cloneObj(main.data.std_settings.draw);
		};
		this.initTileOptions=function(){
			this.tileOptions={};
			for(var i=0;i<this.tile_source_keys.length;i++){
				key1=this.tile_source_keys[i];
				this.tileOptions[key1]=this.tile_source[key1];
				this.tileOptions[key1].tiles={};
				for(var t=0;t<this.tiles_keys.length;t++){
					key2=this.tiles_keys[t];
					if(key1==this.tiles[key2].platform){
						this.tileOptions[key1].tiles[key2]=this.tiles[key2];
					}
				}
			}
			if(!this.tileOptions[this.defaultTiles].tiles[this.defaultTile]){this.defaultTile=this.tileOptions[this.defaultTiles].default_map_type;}
		};
		this.setView=function(cood,zoom){
			var view=self.map.getView();
			view.setCenter(cood);
			view.setZoom(zoom);
		};
		this.initGeolocation=function(){
			this.geolocation=new ol.Geolocation({
				projection:self.currentView.getProjection()
			});
			//handle geolocation error
			this.geolocation.on('error',function(error){
				//error.message
				console.log(error);
			});
			
			//set accuracy feature
			this.accuracyFeature=new ol.Feature();
			this.geolocation.on('change:accuracyGeometry',function(){
				var geometry=self.geolocation.getAccuracyGeometry();
				self.accuracyFeature.setGeometry(geometry);
				// self.panTo(geometry.getCoordinates());
			});
			
			//set position feature
			this.positionFeature=new ol.Feature();
			this.positionFeature.setStyle(new ol.style.Style({
				image:new ol.style.Circle({
					radius:6,
					fill:new ol.style.Fill({
						color: '#3399CC'
					}),
					stroke:new ol.style.Stroke({
						color:'#fff',
						width:2
					})
				})
			}));
			
			//inaccurate
			this.geolocation.on('change:position',function(){
				if(self.waitingForLocation){
					var coods=self.geolocation.getPosition();
					self.positionFeature.setGeometry(coods?new ol.geom.Point(coods):null);
					self.panTo(coods);
					self.animateZoom(11);
					self.waitingForLocation=false;
				}
			});
			this.geolocationOverlay=new ol.layer.Vector({
				source:new ol.source.Vector({
					features:[self.accuracyFeature,self.positionFeature]
				})
			});
			self.geolocating=false;
		};
		this.turnGeolocationOn=function(){
			self.waitingForLocation=true;
			self.geolocationOverlay.setMap(self.map);
			self.geolocation.setTracking(true);
			self.geolocating=true;
		};
		this.turnGeolocationOff=function(){
			self.geolocation.setTracking(false);
			self.geolocationOverlay.setMap(null);
			self.geolocating=false;
			self.waitingForLocation=false;
		};
		this.initMapTools=function(){
			self.mapTools=new MapTools({
				map:self.map,
				context:self,
			});
		};
		this.initMapChangeLoadMore=function(){
			self.map.on('moveend',function(e){
				if(main.invents){
					var inventories=main.invents.inventories;
					for(var key in inventories){
						if(inventories[key] && inventories[key].inventory.map_layer.layer && inventories[key].inventory.map_layer.layer.olLayer.getVisible() && (!main.map_layers[key].properties.max_res || main.map_layers[key].properties.max_res>main.map.map.getView().getResolution())){
							if(!main.invents.inventories[key].blockMoveEndLoadOnce && !main.invents.inventories[key].blockMoveEndLoad){
								main.loader.loadMoreMapPoints(key);
							}
							if(main.invents.inventories[key].blockMoveEndLoadOnce){main.invents.inventories[key].blockMoveEndLoadOnce=false;}
						}
					}
				}
				if(main.filter_legend && main.filter_legend.active){
					var layers=main.map_layers,max_res,takenCareOf;
					for(var key in layers){
						takenCareOf=false;
						if(main.map_layers[key].properties.max_res && main.map_layers[key].layer){
							max_res=Number(main.map_layers[key].properties.max_res);
							if(main.filter_legend.otherLayerItems[key]){
								if(main.filter_legend.otherLayerItems[key].checkbox.checkbox.prop('checked')){
									main.filter_legend.toggleLayer(key,true);
									takenCareOf=true;
								}
							}/* else if(key==main.filter_legend.layerName){
								if(max_res<main.map.map.getView().getResolution()){
									main.map_layers[key].layer.olLayer.setVisible(false);
								}else{
									main.map_layers[key].layer.olLayer.setVisible(true);
								}
							} */
						}
						// if(!takenCareOf && main.map_layers[key].layer){
							// var visible=null;
							// if(main.map_layers[key].properties.max_res){
								// if(Number(main.map_layers[key].properties.max_res)<=main.map.map.getView().getResolution()){visible=true;
								// }else{visible=false;}
							// }
							// if(main.map_layers[key].properties.min_res && visible){
								// if(Number(main.map_layers[key].properties.min_res)<main.map.map.getView().getResolution()){
									// visible=false;
								// }
							// }
							// if(visible!==null){main.map_layers[key].layer.setVisible(visible);}
						// } 
					}
				}
			});
		};
		this.initMap=function(){
			if(this.defaultTiles!='google'){
				if(this.defaultTiles=='bing'){ this.baseTiles=this.getBingTiles(this.defaultTile);
				}else if(this.defaultTiles=='map_quest'){ this.baseTiles=this.getMapQuestTiles(this.defaultTile);
				}else if(this.defaultTiles=='osm'){ this.baseTiles=this.getOSMTiles(this.defaultTile);
				}else if(this.defaultTiles=='esri'){ this.baseTiles=this.getESRITiles(this.defaultTile);}
				var layers=[this.baseTiles];
			}else{
				var layers=[];
			}
			this.currentTiles=this.defaultTiles;
			this.tileOptions[this.currentTiles].current_map_type=this.defaultTile;
			this.mapContainer=main.layout.mapContainer;
			this.mapContainerParent=self.mapContainer.get(0).parentNode;
			var extentLimit;
			if((self.personal_settings.bound_extent_maxx || self.personal_settings.bound_extent_maxx===0)
				&& (self.personal_settings.bound_extent_maxy || self.personal_settings.bound_extent_maxy===0)
				&& (self.personal_settings.bound_extent_minx || self.personal_settings.bound_extent_minx===0)
				&& (self.personal_settings.bound_extent_miny || self.personal_settings.bound_extent_miny===0)
			){
				extentLimit=[Number(self.personal_settings.bound_extent_minx),Number(self.personal_settings.bound_extent_miny),Number(self.personal_settings.bound_extent_maxx),Number(self.personal_settings.bound_extent_maxy)];
			} 
			var withResScale=false;
			var zoom=this.personal_settings.zoom;
			var resolution;
			var resolutions;
			if(withResScale){
				//normal multiplier=2
				var multiplier=1.2;
				var minRes=0.29858214173896974;
				var maxRes=156543.03392804097;
				var i=minRes;
				self.resolutions=[];
				self.stdResolution=152.8740565703525;
				while(i<maxRes){
					self.resolutions.push(i);
					i=i*multiplier;
				}
				if(self.resolutions[self.resolutions.length-1]!=maxRes){self.resolutions.push(maxRes);}
				self.resolutions.reverse();
				var zoom=undefined;
				var resolution=self.stdResolution;
				var resolutions=self.resolutions;
			}
			this.mainView=new ol.View({
				center: ol.proj.transform(this.personal_settings.center, 'EPSG:4326', 'EPSG:3857'),
				zoom: zoom,
				maxZoom:this.maxZoom,
				minZoom:this.minZoom,
				resolution: resolution,
				resolutions:resolutions,
				extent:extentLimit
			});
			this.map = new ol.Map({
				interactions:ol.interaction.defaults({altShiftDragRotate:false,pinchRotate:false}).extend([new self.extraInteractions.Drag()]),
				controls:ol.control.defaults({
					attributionOptions:({
						collapsible:false
					}),
					zoom:false
				}).extend([
					// new ol.control.FullScreen(),
					new ol.control.ScaleLine({units:'us'}),
					// new ol.control.ZoomSlider(),
					// new ol.control.MousePosition({
						// coordinateFormat:ol.coordinate.createStringXY
					// })
				]),
				logo:main.mainPath+'images/PiGEO_LOGO_hiRes.jpg',
				target: this.mapContainer.attr('id'),
				layers: layers,
				view: this.mainView
			});
			// this.disableDragRotate();
			// this.disablePinchRotate();
			this.currentView=self.mainView;
			this.createAuxLayers();
			if(this.defaultTiles=='google'){
				// this.changeToGoogleMap(this.tileOptions['google'].default_map_type);
			}
			setTimeout(self.checkForTilesLoaded,5000);
		};
		
		this.checkForTilesLoaded=function(){
			if(!self.tilesLoadedCt){self.changeSource(self.currentTiles);}
		};
		this.initSelectLocation=function(){
			self.vectorLayers['select_location']=new ol.layer.Vector({
				source:new ol.source.Vector()
			});
			self.map.addLayer(self.vectorLayers['select_location']);
		};
		this.initLocatePoint=function(){
			self.vectorLayers['locate_point']=new ol.layer.Vector({
				source:new ol.source.Vector()
			});
			self.map.addLayer(self.vectorLayers['locate_point']);
		};
		this.createAuxLayers=function(){
			this.initSelectLocation();
			this.initLocatePoint();
		},
		this.adjustMapSize=function(){
			var right=0,left=0,sidePanel;
			for(var key in main.layout.sidePanels){
				sidePanel=main.layout.sidePanels[key];
				if((sidePanel.opening || sidePanel.opened) && !sidePanel.closing){
					if(sidePanel.right){
						if(sidePanel.container.hasClass('customSize')){
							right=(sidePanel.container.outerWidth(true)*100/main.layout.bodyContentWrap.width())+'%';
						}else{
							if(sidePanel.container.hasClass('extSidePanel')){
								right='50%';
							}else{
								right=main.layout.defaultSidePanelWidth+'px';
							}
						}
					}else{
						if(sidePanel.container.hasClass('customSize') || sidePanel.container.hasClass('extSidePanel')){
							left=(sidePanel.container.outerWidth(true)*100/main.layout.bodyContentWrap.width())+'%';
						}else{
							left=main.layout.defaultSidePanelWidth+'px';
						}
					}
				}
			}
			main.layout.mapContainerWrap.css('left',left).css('right',right);
			self.map.updateSize();
			if(self.currentTiles=='google'){
				self.adjustGoogleMap();
			}
			if(self.mapTools){self.mapTools.panel.positionPanel();}
			/* main.layout.mapContainerWrap.stop().animate({'left':left,'right':right},main.animations.defaultDuration,main.animations.defaultTransition,function(){
				// $(this).removeClass('block');
				// self.opened=false;
				// self.closing=false;
				self.map.updateSize();
			}); */
			// self.mapContainer.css('left',left).css('right',right);
		};
		this.adjustGoogleMap=function(){
			google.maps.event.trigger(self.google.googleMap,'resize');
			self.google.googleCenterChange();
		};
		this.createHTML=function(){
			this.addShelf();
			main.utilities.initSpectrumPickers(this.params,this.layersInputChange,true,true);
		};
		this.addShelf=function(){
			this.launchButton=new Button('toolBox','',this.alias);
			this.mapForm=this.createMapForm();
			this.shelf=main.layout.toolBox.createShelf({
				name:'mapProps',
				button:this.launchButton.html,
				content:this.mapForm,
				alias:this.alias,
				groups:main.withs[self.withItem].toolbox_groups
			});
		};
		this.createMapForm=function(){
			var mapForm=$('<form>').addClass('MapForm');
			this.tilesSelect=$('<select>').addClass('tilesSelect');
			this.tilesSelect.append($('<option>').val('').text('None'));
			var selected;
			for(var key in this.tileOptions){
				if(!this.tileOptions[key].active){continue;}
				selected='';
				if(this.currentTiles==key) selected=' selected="selected"';
				this.tilesSelect.append($('<option'+selected+'>').val(this.tileOptions[key].name).text(this.tileOptions[key].alias))
			}
			this.baseMapSelect=$('<select>').addClass('baseMapSelect');
			this.changeBasemapSelect(this.baseMapSelect);
			// this.overLaySelect=$('<select>').addClass('overLaySelect');
			// this.changeOverlaySelect(this.overLaySelect);
			var table=$('<table>').addClass('shelfForm')
				.append($('<tr>')
					.append($('<th colspan="2">')
						.append($('<div>').addClass('tdLiner center').text('Map Properties'))
					)
				).append($('<tr>')
					.append($('<td>').append($('<div>').addClass('tdLiner').text('Source')))
					.append($('<td>').append($('<div>').addClass('tdLiner').append(this.tilesSelect)))
				).append($('<tr>')
					.append($('<td>').append($('<div>').addClass('tdLiner').text('Base Tiles')))
					.append($('<td>').append($('<div>').addClass('tdLiner').append(this.baseMapSelect)))
				)/* .append($('<tr>')
					.append($('<td>').append($('<div>').addClass('tdLiner').text('Overlay')))
					.append($('<td>').append($('<div>').addClass('tdLiner').append(this.overLaySelect)))
				) */;
			return mapForm.append(table);
		};
		this.tilesLoaded=function(type){
			self.tilesLoadedCt++;
		};
		this.setCurrentSource=function(type){
			// self.currentSource.on('tileloadstart',function(e){console.log(e);});
			self.currentSource.on('tileloadend',self.tilesLoaded);
			// self.currentSource.on('tileloaderror',function(e){console.log(e);});
		};
		this.getBingTiles=function(type){
			self.currentSource=new ol.source.BingMaps({
				key:this.tileOptions['bing'].key,
				imagerySet:type
			})
			self.setCurrentSource();
			return new ol.layer.Tile({
				source: self.currentSource
			});
		};
		this.getMapQuestTiles=function(type){
			self.currentSource=new ol.source.MapQuest({
				layer:type
			})
			self.setCurrentSource();
			return new ol.layer.Tile({
				source: self.currentSource
			});
		};
		this.getOSMTiles=function(type){
			if(type=='std'){
				self.currentSource=new ol.source.OSM()
				self.setCurrentSource();
				return new ol.layer.Tile({
					source: self.currentSource
				});
			}else{
				var tileParams=this.tileOptions['osm'].tiles[type];
				self.currentSource=new ol.source.OSM({
					attributions: [
						new ol.Attribution({
							html:tileParams.attribution
						}),
						ol.source.OSM.ATTRIBUTION
					],
					crossOrigin: tileParams.crossOrigin,
					url: tileParams.url
				});
				self.setCurrentSource();
				return new ol.layer.Tile({
					source: self.currentSource
				});
			}
		};
		this.getESRITiles=function(type){
			var tileParams=this.tileOptions['esri'].tiles[type];
			var attribution=new ol.Attribution({
				html: 'Tiles &copy; <a href="http://services.arcgisonline.com/ArcGIS/rest/services/'+tileParams.name+'/MapServer">ArcGIS</a>'
			});
			self.currentSource=new ol.source.XYZ({
				attributions: [attribution],
				url: tileParams.url
			});
			self.setCurrentSource();
			return new ol.layer.Tile({
				source: self.currentSource
			});
		};
		this.enableDragPan=function(){
			if(self.dragPan){self.map.addInteraction(self.dragPan);}
			self.dragPan=null;
		};
		this.disableDragPan=function(){
			self.dragPan=null;
			self.map.getInteractions().forEach(function(interaction){
				if(interaction instanceof ol.interaction.DragPan){
					self.dragPan=interaction;
				}
			},this);
			if(self.dragPan){self.map.removeInteraction(self.dragPan);}
		};
		this.enableDragRotate=function(){
			if(self.dragRotate){self.map.addInteraction(self.dragRotate);}
			self.dragRotate=null;
		};
		this.disableDragRotate=function(){
			self.dragRotate=null;
			self.map.getInteractions().forEach(function(interaction){
				if(interaction instanceof ol.interaction.DragRotate){
					self.dragRotate=interaction;
				}
			},this);
			if(self.dragRotate){self.map.removeInteraction(self.dragRotate);}
		};
		this.enablePinchRotate=function(){
			if(self.pinchRotate){self.map.addInteraction(self.pinchRotate);}
			self.pinchRotate=null;
		};
		this.disablePinchRotate=function(){
			self.pinchRotate=null;
			self.map.getInteractions().forEach(function(interaction){
				if(interaction instanceof ol.interaction.PinchRotate){
					self.pinchRotate=interaction;
				}
			},this);
			if(self.pinchRotate){self.map.removeInteraction(self.pinchRotate);}
		};
		this.changeToGoogleMap=function(type){
			main.layout.gmapContainer.removeClass('gmapHidden');
			this.tileOptions[this.currentTiles].current_map_type=type;
			var mainMapCenter=self.mainView.getCenter();
			var center=ol.proj.transform(mainMapCenter,'EPSG:3857','EPSG:4326');
			var zoom=self.mainView.getZoom();
			// var resolution=self.mainView.getResolution();
			self.adjustForGoogle();
			if(!self.google.googleMap){
				if(zoom>21){zoom=21;}
				self.google.googleMap=new google.maps.Map(document.getElementById(main.layout.gmapContainer.attr('id')),{
					disableDefaultUI: true,
					keyboardShortcuts: false,
					draggable: false,
					disableDoubleClickZoom: true,
					scrollwheel: false,
					streetViewControl: false,
					zoom: zoom,
					center: new google.maps.LatLng(center[1], center[0]),
					mapTypeId: google.maps.MapTypeId[type],
					tilt:0
				});
				self.google.googleView=new ol.View({
					maxZoom:self.maxGZoom,
					mixZoom:self.minGZoom,
					center:mainMapCenter,
					// resolution:resolution,
					// resolutions:self.resolutions,
					zoom:zoom
				});
				$(window).on('resize',self.adjustGoogleMap);
			}else{
				self.google.googleView.setCenter(mainMapCenter);
				self.google.googleView.setZoom(zoom);
				self.google.googleCenterChangeHandler=self.google.googleView.on('change:center',self.google.googleCenterChange);
				self.google.googleResoChangeeHandler=self.google.googleView.on('change:resolution',self.google.googleResoChange);
				self.map.setView(self.google.googleView);
				self.currentView=self.google.googleView;
				$(window).on('resize',self.adjustGoogleMap);
				if(self.baseTiles) self.map.removeLayer(self.baseTiles);
				self.google.showing=true;
				return;
			}
			self.google.googleCenterChangeHandler=self.google.googleView.on('change:center',self.google.googleCenterChange);
			self.google.googleResoChangeeHandler=self.google.googleView.on('change:resolution',self.google.googleResoChange);
			self.map.setView(self.google.googleView);
			self.currentView=self.google.googleView;
			// self.mapContainer.remove();
			var mapContainer=self.mapContainer.get(0);
			// mapContainer.parentNode.removeChild(mapContainer);
			this.mapContainerParent.removeChild(mapContainer);
			self.google.googleMap.controls[google.maps.ControlPosition.TOP_LEFT].push(mapContainer);
			self.google.showing=true;
			if(self.baseTiles) self.map.removeLayer(self.baseTiles);
		};
		this.google.googleCenterChange=function(){
			var center=self.google.googleView.getCenter();
			if(center){
				center=ol.proj.transform(center,'EPSG:3857','EPSG:4326');
				// self.google.googleMap.setCenter(new google.maps.LatLng(0,0));
				self.google.googleMap.setCenter(new google.maps.LatLng(center[1],center[0]));
				self.google.googleMap.setZoom(self.google.googleView.getZoom());
			}
		};
		this.google.googleResoChange=function(){
			self.google.googleMap.setZoom(self.google.googleView.getZoom());
		};
		this.removeGoogleMap=function(){
			self.google.showing=false;
			self.google.googleView.unByKey(self.google.googleCenterChangeHandler);
			self.google.googleView.unByKey(self.google.googleResoChangeeHandler);
			self.mainView.setCenter(self.google.googleView.getCenter());
			self.mainView.setZoom(self.google.googleView.getZoom());
			self.adjustForNotGoogle();
			// main.layout.gmapContainer.addClass('gmapHidden2');
			self.map.setView(self.mainView);
			self.currentView=self.mainView;
			self.mapContainerParent.appendChild(self.mapContainer.get(0));
			// self.mapContainer.get(0);
		};
		this.adjustForNotGoogle=function(){
			main.layout.gmapContainer.children('*').children('div:first-child,div:nth-child(2),div.gmnoprint,div:last-child').addClass('none');
			$('.ol-scale-line').css('bottom','');
			$('.ol-attribution').removeClass('googAttAdjust');
			self.mapContainer.removeClass('none');
		};
		this.adjustForGoogle=function(){
			main.layout.gmapContainer.children('*').children('div:first-child,div:nth-child(2),div.gmnoprint,div:last-child').removeClass('none');
			$('.ol-scale-line').css('bottom','28px');
			$('.ol-attribution').addClass('googAttAdjust');
		};
		this.changeBasemap=function(type){
			this.tileOptions[this.currentTiles].current_map_type=type;
			if(this.currentTiles!='google'){
				if(self.google.showing){self.removeGoogleMap();}
				if(this.currentTiles=='bing'){ var newBaseTiles=this.getBingTiles(type);
				}else if(this.currentTiles=='map_quest'){ var newBaseTiles=this.getMapQuestTiles(type);
				}else if(this.currentTiles=='osm'){ var newBaseTiles=this.getOSMTiles(type);
				}else if(this.currentTiles=='esri'){ var newBaseTiles=this.getESRITiles(type);}
				if(this.baseTiles){this.map.removeLayer(this.baseTiles);}
				var mapLayers=this.map.getLayers();
				mapLayers.insertAt(0,newBaseTiles);
				this.baseTiles=newBaseTiles;
			}else{
				self.google.googleMap.setMapTypeId(google.maps.MapTypeId[type]);
				self.google.googleCenterChange();
			}
		};
		this.changeBasemapSelect=function(select){
			select.html('');
			var tiles=this.tileOptions[this.currentTiles].tiles;
			if(!this.tileOptions[this.currentTiles].current_map_type) this.tileOptions[this.currentTiles].current_map_type=this.tileOptions[this.currentTiles].default_map_type;
			for(var key in tiles){
				selected='';
				if(this.tileOptions[this.currentTiles].current_map_type==key) selected=' selected="selected"';
				select.append($('<option'+selected+'>').val(tiles[key].name).text(tiles[key].alias))
			}
		};
		this.changeOverlaySelect=function(select){
			select.html('');
			var overlays=this.tileOptions[this.currentTiles].overlays;
			for(var key in overlays){
				select.append($('<option'+selected+'>').val(overlays[key].name).text(overlays[key].alias))
			}
		};
		this.changeSource=function(type){
			this.currentTiles=type;
			if(type){
				this.changeBasemapSelect(this.baseMapSelect);
				if(type=='google'){
					self.changeToGoogleMap(this.tileOptions[this.currentTiles].current_map_type);
					self.google.googleCenterChange();
				}else{
					$(window).off('resize',self.adjustGoogleMap);
					this.changeBasemap(this.tileOptions[this.currentTiles].current_map_type);
				}
			}else{if(this.baseTiles) this.map.removeLayer(this.baseTiles);}
		};
		this.addOverlay=function(type){
			if(this.currentTiles=='osm'){var overlay=this.getOSMTiles(type);}
			this.map.addLayer(overlay);
		};
		this.updateLayerSource=function(layerName,secondaryLoad){
			if(!secondaryLoad){
				if(main.map_layers[layerName].layerObj){
					main.map_layers[layerName].layer.setSource(main.map_layers[layerName].layerObj);
				}
			}else{
				if(main.map_layers[layerName].secondaryLoad.layerObj){
					main.map_layers[layerName].layer.setSource(main.map_layers[layerName].secondaryLoad.layerObj);
				}
			}
		};
		this.clearOtherPActions=function(){
			for(var key in self.mapPoints){
				if(self.mapPoints[key].selecting){
					self.mapPoints[key].stopSelecting();
				}
			}
			for(var key in self.geocoders){
				if(self.geocoders[key].selecting){
					self.geocoders[key].stopSelecting();
				}
			}
		};
		this.boundMap=function(printLayers,geomField){
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						printLayers:printLayers,
						geomField:geomField,
					},
					action:'getBoundingBox'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							main.utilities.boundTo(response.bounds,self.map,self.boundToMapMove);
						}
					}
				}
			});
		}
		this.getLayerIndex=function(layer){
			var idx=null;
			var layers=main.map.map.getLayers().getArray();
			for(var i=0;i<layers.length;i++){
				if(layers[i]==layer){idx=i;}
			}
			return idx;
		};
		this.insertLayer=function(insertLayer,beforeLayer){
			main.map.map.getLayers().remove(insertLayer);
			var idx=self.getLayerIndex(beforeLayer)-1;
			main.map.map.getLayers().insertAt(idx,main.map_layers.trees.layer.olLayer);
		}
		this.sortLayers=function(layer){
			var layers=main.map.map.getLayers().getArray();
			var layersForSort=[];
			for(var i=0;i<layers.length;i++){
				if(layers[i].get('fromApp')){
					if(layers[i].get('z_index')!==undefined && layers[i].get('z_index')!==null){
						layersForSort.push(layers[i]);
					}
				}
			}
			layersForSort.sort(function(a,b){
				if(Number(a.get('z_index'))>Number(b.get('z_index'))){return -1;}
				if(Number(a.get('z_index'))<Number(b.get('z_index'))){return 1;}
				return 0;
			});
			// if(layersForSort[0]){
				// main.layerrrr=layersForSort[0];
				// console.log(layersForSort[0].getProperties());
				// }
			for(var i=0;i<layersForSort.length;i++){
				main.map.map.getLayers().remove(layersForSort[i]);
				main.map.map.getLayers().insertAt((main.map.map.getLayers().getArray().length-1),layersForSort[i]);
			}
			if(layer && layersForSort.indexOf(layer)==-1){
				main.map.map.getLayers().remove(layer);
				main.map.map.getLayers().insertAt((main.map.map.getLayers().getArray().length-layersForSort.length-1),layer);
			}
		}
		this.boundToMapMove=function(initialCenter,newCenter,initialZoom,newZoom){
			self.map.getView().setCenter(initialCenter);
			self.panTo(newCenter);
			if(newZoom){
				self.map.getView().setZoom(initialZoom);
				if(initialZoom!=newZoom){
					self.animateZoom(newZoom);
				}
			}
		};
		this.panTo=function(coords,callback,mobilePosition){
			var originalCoords=self.map.getView().getCenter();
			var pan=ol.animation.pan({
				duration:300,
				source:originalCoords
			});
			if(mobilePosition){
				self.map.getView().setCenter(coords);
				var x=coords[0];
				var minX=main.map.map.getView().calculateExtent(main.map.map.getSize())[2];
				var newX=minX+((x-minX)*0.4);
				coords[0]=newX;
				var y=coords[1];
				var minY=main.map.map.getView().calculateExtent(main.map.map.getSize())[3];
				var newY=minY+((y-minY)*1.3);
				coords[1]=newY;
				self.map.getView().setCenter(originalCoords);
			}								
			if(callback){
				if(coords[0]==originalCoords[0] && coords[1]==originalCoords[1]){callback();
				}else{self.map.once('moveend',callback);}
			}
			self.map.beforeRender(pan);
			self.map.getView().setCenter(coords);
		};
		this.animateZoom=function(zoom,callback){
			var originalZoom=self.map.getView().getZoom();
			if(self.google.showing){
				if(zoom>self.maxGZoom){zoom=self.maxGZoom;}
				if(zoom<self.minGZoom){zoom=self.minGZoom;}
			}else{
				if(zoom>self.maxZoom){zoom=self.maxZoom;}
				if(zoom<self.minZoom){zoom=self.minZoom;}
			}
			var zoomA=ol.animation.zoom({
				duration:300,
				resolution:self.map.getView().getResolution()
			});		
			if(callback){
				if(zoom==originalZoom){callback();
				}else{self.map.once('moveend',callback);}
			}
			self.map.beforeRender(zoomA);
			self.map.getView().setZoom(zoom);
		};
		this.registerLayer=function(layer){
			if(!main.map.vectorLayers[layer.id] && main.layers){
				main.layers.addLayers([layer]);
			}
		};
		this.onMapClick=function(e){
			if(main.map.selecting || main.flags.adding || main.flags.drawing || main.flags.movingFeature/*  || self.context.groupOrganizer.moving  *//* || self.context.selectingForPhoto *//*  || !self.context.layer.layer.visible */ || (main.measure && main.measure.measuring) || (main.invents && main.invents.selectingForTransferTo) || (main.massUpdater && main.massUpdater.polygonDrawAdded) || (main.filter_legend && main.filter_legend.withCanopyMode)){return;}
			
			var cood=self.map.getEventCoordinate(e.originalEvent);
			// var closestFeature=self.context.layer.layer.source.getClosestFeatureToCoordinate(cood);
			var props,matchedFeature,closestFeature//,matchedLayer=self.context.layer.layer.olLayer;
			var pixel=main.utilities.getPixel(e);
			if(!pixel){return;}
			var features=[];
			var possibleMatches={};
			self.map.forEachFeatureAtPixel(pixel,function(feature,layer){
				if(layer){
					// if(layer.get('id')==self.context.layerName){
						// features.push(feature);
					// }
					// if(layer.get('id')==self.context.layerName && closestFeature==feature){
						// matchedLayer=layer;
						// console.log(22);
						// matchedFeature=feature;
					// }
					if(!possibleMatches[layer.get('id')]){
						closestFeature=layer.getSource().getClosestFeatureToCoordinate(cood);
						possibleMatches[layer.get('id')]={layer:layer,allFeatures:[],closestFeature:closestFeature};
					}
					gemoetry=possibleMatches[layer.get('id')].closestFeature.getGeometry();
					if(!(gemoetry instanceof ol.geom.Point) || !(gemoetry instanceof ol.geom.MultiPoint) || feature==possibleMatches[layer.get('id')].closestFeature){
						possibleMatches[layer.get('id')].matchedFeature=feature;
					}
					possibleMatches[layer.get('id')].allFeatures.push(feature);
				}
				
			});
			//get best match by zIndex
			var layers=self.map.getLayers().getArray();
			var bestMatch=null;
			for(var i=0;i<layers.length;i++){
				if(possibleMatches[layers[i].get('id')] && possibleMatches[layers[i].get('id')].matchedFeature){
					bestMatch=possibleMatches[layers[i].get('id')];
				}
			}
			main.mapItems.unSetActives();
			
			//apply buffer for touchable devices
			if(!bestMatch && (main.browser.isMobile || main.browser.isTouchable)){
				var possibleMatches=[],style,closestFeature,getType,closestFill,closestColor,limit=30,closestFeatureCood,closestFeaturePixel,delta;
				for(var key in main.tables){
					if(main.tables[key].map_layer && main.tables[key].map_layer.layer && main.tables[key].map_layer.layer.olLayer && main.tables[key].map_layer.layer.olLayer.getVisible()){
						style=main.tables[key].map_layer.layer.olLayer.getStyle();
						closestFeature=main.tables[key].map_layer.layer.olLayer.getSource().getClosestFeatureToCoordinate(cood);
						getType={};
						if(getType.toString.call(style)==='[object Function]'){
							closestColor=style(closestFeature)[0].getFill().getColor();
						}else{
							closestFill=style.getFill();
							if(closestFill){
								closestColor=style.getFill().getColor();
							}else{closestColor=null;}
						}
						if((closestColor && closestColor!='rgba(0,0,0,0)') || !closestColor){
							closestFeatureCood=closestFeature.getGeometry().getCoordinates();
							//to do: coordinates for polygons
							// main.details
							// if(closestFeatureCood[0].constructor===Array){closestFeatureCood=main.utilities.getCenterOfFeature(closestFeature);}
							if(closestFeatureCood && closestFeatureCood[0].constructor!==Array){
								closestFeaturePixel=main.map.map.getPixelFromCoordinate(closestFeatureCood);
								delta=Math.sqrt(Math.pow(Math.abs(closestFeaturePixel[0]-pixel[0]),2)+Math.pow(Math.abs(closestFeaturePixel[1]-pixel[1]),2));
								if(delta<limit){
									possibleMatches.push({layer:main.tables[key].map_layer.layer.olLayer,allFeatures:[],closestFeature:closestFeature,matchedFeature:closestFeature,delta:delta});
								}
							}
						}
					}
				}
				possibleMatches=possibleMatches.sort(function(a,b){
					if(a.delta<b.delta){return 1;}
					if(a.delta>b.delta){return -1;}
					return 0;
				});
				bestMatch=possibleMatches[0];
			}
			if(!bestMatch){
				$('.activeRow').removeClass('activeRow');
				main.layout.removeAllPopUps();
				return;
			}
			
			bestMatch.matchedFeature.set('isActive',true);
			var context=main.tables[bestMatch.layer.get('id')].coordModule;
			if(context.groupOrganizer.womSelectInv){
				main.layout.removeAllPopUps();
				context.groupOrganizer.womSelectEditForm.womOnMapClick(bestMatch.matchedFeature,context.groupOrganizer.womSelectInv);
				if(main.filter_legend && main.filter_legend.active && main.filter_legend.layer==context.map_layer){
					main.filter_legend.refreshPoints();
				}
				return;
			}
			if(main.massUpdater && main.massUpdater.selectingFromPoint){
				main.layout.removeAllPopUps();
				main.massUpdater.featureSelectedByPoint(bestMatch.matchedFeature);
				return;
			}
			var pid=bestMatch.matchedFeature.get('pid');
			main.mapItems.hasActive=pid;
			main.mapItems.hasActiveLayer=bestMatch.layer.get('id');
			// main.utilities.setTopLayer(context.layer.layer.olLayer);
			self.sortLayers(context.layer.layer.olLayer);
			if(main.browser.isMobile && context.groupOrganizer.moving){
				main.map.mapContainer.addClass('adding');
				context.movingFeature=pid;
				main.flags.movingFeature=pid;
				context.movingLayer=matchedLayer;
				main.map.map.on('click',context.movingEvent);
			}
			if(!context.groupOrganizer.moving && context.dataTable){
				context.dataTable.activateRow(pid);
			}
			if(main.filter_legend && main.filter_legend.active && main.filter_legend.layer==context.map_layer){
				main.filter_legend.refreshPoints();
			}else{
				context.map_layer.layer.style.setStyleFunction();
			}
			// if(main.filter_legend && main.filter_legend.active){main.filter_legend.refresh(true);}
			// if(main.dashboard && main.dashboard.active){main.dashboard.refresh();}
			if(!context.groupOrganizer.moving && context.currentFocused!=bestMatch.matchedFeature){
				context.lastFocused=context.currentFocused;
				context.currentFocused=bestMatch.matchedFeature;
			}
			// context.map_layer.features[results[i].pid].feature.setProperties(results[i]);
			context.map_layer.features[pid].feature=bestMatch.matchedFeature;
			if(!context.groupOrganizer.moving){context.mapInteractions.showPopup(pid,bestMatch.matchedFeature,features);}
			var coords=main.utilities.getCoords(bestMatch.matchedFeature);
			var mobilePan=false;
			if($('body').width()<=main.dimensions.phoneWidth){mobilePan=true;}
			context.map.panTo(coords,null,mobilePan);
			// main.map.map.getView().setCenter(coords);
			
			
			return;
			if(!main.mapItems.featureMatched){
				var cood=self.context.map.map.getEventCoordinate(e.originalEvent);
				var closestFeature=self.context.layer.layer.source.getClosestFeatureToCoordinate(cood);
				var props,matchedFeature,matchedLayer=self.context.layer.layer.olLayer;
				var pixel=main.utilities.getPixel(e);
				if(!pixel){return;}
				var features=[];
				self.context.map.map.forEachFeatureAtPixel(pixel,function(feature,layer){
					if(layer){
						if(layer.get('id')==self.context.layerName){
							features.push(feature);
						}
						if(layer.get('id')==self.context.layerName && closestFeature==feature){
							matchedLayer=layer;
							console.log(22);
							matchedFeature=feature;
						}
					}
				});
				//correct for zIndex
				return;
				
				if(!matchedFeature && features.length>0){matchedFeature=features[0];}
				main.mapItems.unSetActives();
				//apply buffer for touchable devices
				if(!matchedFeature && (main.browser.isMobile || main.browser.isTouchable) && self.context.layer.layer.olLayer.getVisible()){
					var style=self.context.layer.layer.olLayer.getStyle();
					var getType={};
					if(getType.toString.call(style)==='[object Function]'){
						var closestColor=style(closestFeature)[0].getFill().getColor();
					}else{
						var closestFill=style.getFill();
						if(closestFill){
							var closestColor=style.getFill().getColor();
						}else{var closestColor=null;}
					}
					if((closestColor && closestColor!='rgba(0,0,0,0)') || !closestColor){
						var limit=30;
						var closestFeatureCood=closestFeature.getGeometry().getCoordinates();
						if(closestFeatureCood){
							var closestFeaturePixel=main.map.map.getPixelFromCoordinate(closestFeatureCood);
							var delta=Math.sqrt(Math.pow(Math.abs(closestFeaturePixel[0]-pixel[0]),2)+Math.pow(Math.abs(closestFeaturePixel[1]-pixel[1]),2));
							if(delta<limit){
								matchedFeature=closestFeature;
							}
						}
					}
				}
				if(matchedFeature){
					matchedFeature.set('isActive',true);
					if(self.context.groupOrganizer.womSelectInv){
						main.layout.removeAllPopUps();
						self.context.groupOrganizer.womSelectEditForm.womOnMapClick(matchedFeature,self.context.groupOrganizer.womSelectInv);
						if(main.filter_legend && main.filter_legend.active && main.filter_legend.layer==self.context.map_layer){
							main.filter_legend.refreshPoints();
						}
						return;
					}
					if(main.massUpdater && main.massUpdater.selectingFromPoint){
						main.layout.removeAllPopUps();
						main.massUpdater.featureSelectedByPoint(matchedFeature);
						return;
					}
					var pid=matchedFeature.get('pid');
					main.mapItems.hasActive=pid;
					main.utilities.setTopLayer(self.context.layer.layer.olLayer);
					if(main.browser.isMobile && self.context.groupOrganizer.moving){
						main.map.mapContainer.addClass('adding');
						self.context.movingFeature=pid;
						self.context.movingLayer=matchedLayer;
						main.map.map.on('click',self.context.movingEvent);
					}
					if(!self.context.groupOrganizer.moving && self.context.dataTable){
						self.context.dataTable.activateRow(pid);
					}
					if(main.filter_legend && main.filter_legend.active && main.filter_legend.layer==self.context.map_layer){
						main.filter_legend.refreshPoints();
					}else{
						self.context.map_layer.layer.style.setStyleFunction();
					}
					// if(main.filter_legend && main.filter_legend.active){main.filter_legend.refresh(true);}
					// if(main.dashboard && main.dashboard.active){main.dashboard.refresh();}
					if(!self.context.groupOrganizer.moving && self.context.currentFocused!=matchedFeature){
						self.context.lastFocused=self.context.currentFocused;
						self.context.currentFocused=matchedFeature;
					}
					// self.context.map_layer.features[results[i].pid].feature.setProperties(results[i]);
					self.context.map_layer.features[pid].feature=matchedFeature;
					if(!self.context.groupOrganizer.moving){self.showPopup(pid,matchedFeature,features);}
					var coords=main.utilities.getCoords(matchedFeature);
					var mobilePan=false;
					if($('body').width()<=main.dimensions.phoneWidth){mobilePan=true;}
					self.context.map.panTo(coords,null,mobilePan);
					main.mapItems.featureMatched=true;
					// main.map.map.getView().setCenter(coords);
				}
			}
		};
		this.initEvents=function(){
			this.map.on('click',self.onMapClick);
			this.tilesSelect.on('change',function(e){
				self.changeSource($(this).val());
			});
			this.baseMapSelect.on('change',function(e){
				self.changeBasemap($(this).val());
			});
			
			this.map.on('pointermove',function(e){
				var pixel=main.utilities.getPixel(e);
				if(!pixel){return;}
				var feature=self.map.forEachFeatureAtPixel(pixel,function(feature,layer){
					return feature;
				});
				var cursor=feature?'pointer':'';
				if(!main.map.selecting && (!main.invents || (!main.invents.adding && !main.invents.movingFeature)) && (!main.advFilter || !main.advFilter.drawing) && (!main.measure || !main.measure.measuring) && (!main.draw || !main.draw.drawing)){
					$('#'+self.map.getTarget()).css('cursor',cursor);
				}
				if(main.filter_legend && main.filter_legend.withCanopyMode && main.filter_legend.layer.layer.olLayer.getSource().getFeatures().indexOf(feature)>-1){$('#'+self.map.getTarget()).addClass('cursorDefault');}else{$('#'+self.map.getTarget()).removeClass('cursorDefault');}
			});
			// this.overLaySelect.on('change',function(e){
				// self.addOverlay($(this).val());
			// });
			$(document).keydown(function(e){
				//shift
				if(e.which==16){self.shiftDown=true;}
			});
			$(document).keyup(function(e){
				//shift
				if(e.which==16){self.shiftDown=false;}
			});
			this.map.on('click',function(e){
				var cood=self.map.getEventCoordinate(e.originalEvent);
			});
		};
		self.extraInteractions.Drag = function() {

			ol.interaction.Pointer.call(this, {
				handleDownEvent: self.extraInteractions.Drag.prototype.handleDownEvent,
				handleDragEvent: self.extraInteractions.Drag.prototype.handleDragEvent,
				handleMoveEvent: self.extraInteractions.Drag.prototype.handleMoveEvent,
				handleUpEvent: self.extraInteractions.Drag.prototype.handleUpEvent
			});

			/**
			* @type {ol.Pixel}
			* @private
			*/
			this.coordinate_ = null;

			/**
			* @type {string|undefined}
			* @private
			*/
			this.cursor_ = 'pointer';

			/**
			* @type {ol.Feature}
			* @private
			*/
			this.feature_ = null;

			/**
			* @type {string|undefined}
			* @private
			*/
			this.previousCursor_ = undefined;

		};
		ol.inherits(self.extraInteractions.Drag, ol.interaction.Pointer);


		/**
		 * @param {ol.MapBrowserEvent} evt Map browser event.
		 * @return {boolean} `true` to start the drag sequence.
		 */
		self.extraInteractions.Drag.prototype.handleDownEvent = function(evt) {
			// var map = evt.map;
			var map = self.map;
			var features=map.forEachFeatureAtPixel(evt.pixel,function(feature, layer) {
				return {
					feature:feature,
					layer:layer
				}
			});

			if (features && features.feature && features.layer) {
				if(!features.feature.get('draggable') && !features.layer.get('draggable')){return false;}
				this.startPix = [evt.pixel[0],evt.pixel[1]];
				this.started=false;
				this.coordinate_ = evt.coordinate;
				this.feature_ = features.feature;
				this.layer_ = features.layer;
			}
			return !!(features && features.feature);
		};


		/**
		 * @param {ol.MapBrowserEvent} evt Map browser event.
		 */
		self.extraInteractions.Drag.prototype.handleDragEvent = function(evt) {
			if(!this.coordinate_){return false;}
			// var map = evt.map;
			var map = self.map;

			var feature = map.forEachFeatureAtPixel(evt.pixel,function(feature, layer) {
				return feature;
			});

			var deltaX = evt.coordinate[0] - this.coordinate_[0];
			var deltaY = evt.coordinate[1] - this.coordinate_[1];

			var geometry = /** @type {ol.geom.SimpleGeometry} */
			  (this.feature_.getGeometry());
			geometry.translate(deltaX, deltaY);

			this.coordinate_[0] = evt.coordinate[0];
			this.coordinate_[1] = evt.coordinate[1];
			
			if(!this.started && Math.abs(evt.pixel[0]-this.startPix[0])>1 || Math.abs(evt.pixel[1]-this.startPix[1])>1){
				if(this.feature_.get('startDragCB')){this.feature_.get('startDragCB')(evt,this.feature_);}
				this.started=true;
			}
		  
			if(this.feature_.get('draggingCB')){this.feature_.get('draggingCB')(evt,this.feature_);}
		};


		/**
		 * @param {ol.MapBrowserEvent} evt Event.
		 */
		self.extraInteractions.Drag.prototype.handleMoveEvent = function(evt) {
			if (this.cursor_) {
				// var map = evt.map;
				var map = self.map;
				var feature = map.forEachFeatureAtPixel(evt.pixel,
					function(feature, layer) {
						return feature;
					});
				var element = map.getTargetElement();
				if (feature) {
					if (element.style.cursor != this.cursor_ && (!main.invents || !main.invents.adding)) {
						this.previousCursor_ = element.style.cursor;
						element.style.cursor = this.cursor_;
					}
				} else if (this.previousCursor_ !== undefined) {
					element.style.cursor = this.previousCursor_;
					this.previousCursor_ = undefined;
				}
		    }
		};


		/**
		 * @param {ol.MapBrowserEvent} evt Map browser event.
		 * @return {boolean} `false` to stop the drag sequence.
		 */
		self.extraInteractions.Drag.prototype.handleUpEvent = function(evt) {
			if(!this.feature_){return false;}
			if(this.feature_.get('doneDragCB')){
				if(Math.abs(evt.pixel[0]-this.startPix[0])>1 || Math.abs(evt.pixel[1]-this.startPix[1])>1){
					this.feature_.get('doneDragCB')(evt,this.feature_,this.layer_);
				}
			}else if(this.layer_ && this.layer_.get('doneDragCB')){
				if(Math.abs(evt.pixel[0]-this.startPix[0])>1 || Math.abs(evt.pixel[1]-this.startPix[1])>1){
					this.layer_.get('doneDragCB')(evt,this.feature_,this.layer_);
				}
			}
			this.coordinate_ = null;
			this.feature_ = null;
			return false;
		};
		this.setParams();
		this.initTileOptions();
		this.initMap();
		this.initGeolocation();
		this.createHTML();
		this.initEvents();
		this.initMapTools();
		this.initMapChangeLoadMore();
		// this.turnGeolocationOn();
	};
});