define(function(require){
	return function pg(map){
		var self=this;
		var Button=require('./Button');
		var EditableList=require('./EditableList');
		var Input=require('./Input');
		var ViewMore=require('./ViewMore');
		this.map=map;
		this.keys=main.utilities.cloneObj(main.data.std_settings.measure.sortedKeys);
		this.alias='Measure';
		this.withItem='with_measure';
		this.measuring=false;
		this.removeFeaturesHTML='Remove';
		this.latLngFixed=7;
		this.features={};
		this.formID='measureForm';
		this.setParams=function(){
			this.params=main.utilities.cloneObj(main.data.std_settings.measure);
		};
		this.getStyle=function(){
			return new ol.style.Style({
				fill:new ol.style.Fill({
					color:(self.params.fill_color.current_val)?self.params.fill_color.current_val:self.params.fill_color.default_val
				}),
				stroke:new ol.style.Stroke({
					color:(self.params.stroke_color.current_val)?self.params.stroke_color.current_val:self.params.stroke_color.default_val,
					width:(self.params.stroke_width.current_val)?self.params.stroke_width.current_val:self.params.stroke_width.default_val
				}),
				image:new ol.style.Circle({
					radius:(self.params.point_radius.current_val)?self.params.point_radius.current_val:self.params.point_radius.default_val,
					fill:new ol.style.Fill({
						color:(self.params.point_color.current_val)?self.params.point_color.current_val:self.params.point_color.default_val
					})
				})
			})
		};
		this.startLocating=function(){
			this.locating=true;
			main.map.mapContainer.addClass('adding');
			self.locationsTR.removeClass('none');
		};
		this.stopLocating=function(){
			this.locating=false;
			main.map.mapContainer.removeClass('adding');
			self.locationsTR.addClass('none');
		};
		this.startMeasuring=function(){
			main.draw.stopDrawingFunctions();
			if(this.deleting) this.doneDeletingFeatures();
			if(this.modifying) this.stopModifying();
			this.measuring=true;
			this.startMeasuringButton.addClass('none');
			this.stopMeasuringButton.removeClass('none');
			if(this.currentType=='location'){
				this.startLocating();
				this.addDraw("Point");
			}else{
				this.addDraw(this.params.type.options[this.currentType].geomType);
			}
		};
		this.stopMeasuring=function(){
			this.measuring=false;
			this.startMeasuringButton.removeClass('none');
			this.stopMeasuringButton.addClass('none');
			if(this.locating){this.stopLocating();}
			this.map.map.removeInteraction(this.measure);
		};
		this.startModifying=function(){
			main.draw.stopDrawingFunctions();
			if(this.measuring) this.stopMeasuring();
			if(this.deleting) this.doneDeletingFeatures();
			this.modifying=true;
			this.map.map.addInteraction(this.select);
			this.map.map.addInteraction(this.modify);
			this.startModifyingButton.addClass('none');
			this.stopModifyingButton.removeClass('none');
		};
		this.stopModifying=function(){
			this.modifying=false;
			this.select.getFeatures().clear();
			this.map.map.removeInteraction(this.select);
			this.map.map.removeInteraction(this.modify);
			this.startModifyingButton.removeClass('none');
			this.stopModifyingButton.addClass('none');
		};
		this.beginDeletingFeatures=function(){
			main.draw.stopDrawingFunctions();
			if(this.measuring) this.stopMeasuring();
			if(this.modifying) this.stopModifying();
			this.deleting=true;
			this.map.map.addInteraction(this.dragBox);
			this.deleteFeaturesButton.addClass('none');
			this.doneDeletingFeaturesButton.removeClass('none');
		};
		this.doneDeletingFeatures=function(){
			this.deleting=false;
			this.map.map.removeInteraction(this.dragBox);
			this.doneDeletingFeaturesButton.addClass('none');
			if(this.measureSource.getFeatures().length===0){
				this.deleteFeaturesButton.addClass('none');
				this.deleteAllButton.addClass('none');
			}else{this.deleteFeaturesButton.removeClass('none');}
		};
		this.stopMeasuringFunctions=function(){
			if(this.measuring) this.stopMeasuring();
			if(this.modifying) this.stopModifying();
			if(this.deleting) this.doneDeletingFeatures();
		};
		this.addDraw=function(type){
			this.measure=new ol.interaction.Draw({
				source:this.measureSource,
				type:type
			});
			// if(this.currentType!='location'){
				this.measure.on('drawstart',function(e){
					var id=main.utilities.generateUniqueID();
					var feature=e.feature;
					feature.set('featureID',id);
					if(self.currentType!='location'){self.measurements.addItem(id,'',{'feature':feature});}
					else{
						var coods=ol.proj.transform(e.feature.getGeometry().getCoordinates(),'EPSG:3857','EPSG:4326');
						self.measurements.addItem(id,coods[0].toFixed(self.latLngFixed)+',<br/>'+coods[1].toFixed(self.latLngFixed),{'feature':feature});
					}
					self.features[id]=feature;
					feature.on('change',function(e){
						var output;
						var feature=e.target;
						var geom=e.target.getGeometry();
						if(geom instanceof ol.geom.Polygon){output=self.formatArea(geom);
						}else if(geom instanceof ol.geom.LineString){output=self.formatLength(geom);
						}else if(self.currentType=='location'){
							var coods=ol.proj.transform(feature.getGeometry().getCoordinates(),'EPSG:3857','EPSG:4326');
							output=coods[0].toFixed(self.latLngFixed)+',<br/>'+coods[1].toFixed(self.latLngFixed);
						}
						self.measurements.editItemHTML($('.editableListItem[data-name="'+feature.get('featureID')+'"]'),output);
					});
				});
				this.measure.on('drawend',function(e){
					e.feature.set(main.constants.featureTypeCode,'measure')
					self.deleteFeaturesButton.removeClass('none');
					self.deleteAllButton.removeClass('none');
					self.startModifyingButton.removeClass('none');
				});
			// }else{
				
			// }
			this.map.map.addInteraction(this.measure);
		};
		this.createHTML=function(){
			this.addShelf();
			main.utilities.initSpectrumPickers(this.params,this.measuringInputChange,true,true);
		};
		this.addShelf=function(){
			this.launchButton=new Button('toolBox','',this.alias);
			this.measureForm=this.createMeasureForm();
			this.shelf=main.layout.toolBox.createShelf({
				name:'measure',
				button:this.launchButton.html,
				content:this.measureForm,
				alias:this.alias,
				groups:main.withs[self.withItem].toolbox_groups
			});
		};
		this.createMeasureForm=function(){
			var measureForm=$('<form>').attr('id',this.formID);
			this.measurements=new EditableList(null,this.measurementRemoved,true);
			this.measurements=new EditableList({
				removeCallBack:this.measurementRemoved,
				removeable:true
			});
			this.locations=$('<div>').addClass('locationsContainer');
			this.startMeasuringButton=$('<div>').addClass('button startMeasuring').text('Start Measuring');
			this.stopMeasuringButton=$('<div>').addClass('button stopMeasuring none').text('Stop Measuring');
			this.startModifyingButton=$('<div>').addClass('button startModifying none').text('Modify Measurements');
			this.stopModifyingButton=$('<div>').addClass('button stopModifying none').text('Stop Modifying');
			this.deleteFeaturesButton=$('<div>').addClass('button deleteDrawFeatures none').text('Delete Measurements');
			this.doneDeletingFeaturesButton=$('<div>').addClass('button doneDeletingFeatures none').text('Done Deleting');
			this.deleteAllButton=$('<div>').addClass('button deleteAllButton none').text('Delete All Measurements');
			self.viewMore=new ViewMore({
				prev:'Use this tool to measure length, area, or to determine a location in degrees.',
				content:'<ul><li>Step 1: Select a measure type.</li><li>Step 2: Change the parameters.</li><li>Step 3: Click "Start Measuring".</li><li>To delete individual measurements, click "Delete Measurements" and click the graphics you want to delete.</li><li>To modify individual measurements, click "Modify Measurements", then click a graphic. Click and drag the point or the outline of a line or polygon.</li></ul>'
			});
			var table=$('<table>').addClass('shelfForm')
				.append($('<tr>')
					.append($('<td colspan="2">').append($('<div>').addClass('tdLiner toolBoxDesc').html(self.viewMore.html)))
				)
				.append($('<tr>')
					.append($('<th colspan="2">')
						.append($('<div>').addClass('tdLiner center').text('Measure Properties'))
					)
				);
			var toolFormMakeItems=main.utilities.toolFormMake(this.keys,this.params,this.formID,table);
			this.advancedViewMore=toolFormMakeItems.advancedViewMore;
			this.advancedOptions=toolFormMakeItems.advancedOptions;
			this.typeSelect=toolFormMakeItems.typeSelect;
			this.unitSelect=toolFormMakeItems.unitSelect;
			this.locationsTR=$('<tr class="none">')
				.append($('<td>').append($('<div>').addClass('tdLiner').text('Location')))
				.append($('<td>').append($('<div>').addClass('tdLiner').append(this.locations)))
			table.append($('<tr>')
					.append($('<td>').append($('<div>').addClass('tdLiner').text('Measurements')))
					.append($('<td>').append($('<div>').addClass('tdLiner').append(this.measurements.container)))
				).append(this.locationsTR
				).append($('<tr>')
					.append($('<td>').append($('<div>').addClass('tdLiner').append(this.startMeasuringButton)))
					.append($('<td>').append($('<div>').addClass('tdLiner').append(this.stopMeasuringButton)))
				).append($('<tr>')
					.append($('<td>').append($('<div>').addClass('tdLiner').append(this.startModifyingButton)))
					.append($('<td>').append($('<div>').addClass('tdLiner').append(this.stopModifyingButton)))
				).append($('<tr>')
					.append($('<td>').append($('<div>').addClass('tdLiner').append(this.deleteFeaturesButton)))
					.append($('<td>').append($('<div>').addClass('tdLiner').append(this.doneDeletingFeaturesButton)))
				).append($('<tr>')
					.append($('<td colspan="2">').append($('<div>').addClass('tdLiner').append(this.deleteAllButton)))
				);
			return measureForm.append(table);
		};
		this.formatArea=function(polygon){
			var area=polygon.getArea(),output;
			if(area>10000){
				area=Math.round(area/1000000*100)/100;
				if(self.currentUnits.name=='english'){area=area*0.38610;}
				output=area.toFixed(2)+' '+self.currentUnits.longLengthAlias+'<sup>2</sup>';
			}else{
				area=Math.round(area*100)/100;
				if(self.currentUnits.name=='english'){area=area*10.7639;}
				output=area.toFixed(2)+' '+self.currentUnits.shortLengthAlias+'<sup>2</sup>';
			}
			return output;
		};
		this.formatLength=function(line){
			var length=Math.round(line.getLength()*100)/100, output;
			if(length>1000){
				length=Math.round(length/1000*100)/100;
				if(self.currentUnits.name=='english'){length=length*0.621371;}
				output=length.toFixed(2)+' '+self.currentUnits.longLengthAlias;
			}else{
				length=Math.round(length*100)/100;
				if(self.currentUnits.name=='english'){length=length*3.28084;}
				output=length.toFixed(2)+' '+self.currentUnits.shortLengthAlias;
			}
			return output;
		};
		this.measurementRemoved=function(elItem){
			if(elItem.params){
				if(elItem.params.feature){
					self.measureSource.removeFeature(elItem.params.feature);
				}
			}
		};
		this.measuringInputChange=function(){
			self.updateMeasurementParams();
		};
		this.updateMeasurementParams=function(){
			main.utilities.setCurrentVals(self.measureForm,self.params);
			self.measureVector.setStyle(self.getStyle());
		};
		this.initEvents=function(){
			main.map.map.on('pointermove',function(e){
				if(self.locating){
					var coods=ol.proj.transform(e.coordinate,'EPSG:3857','EPSG:4326');
					self.locations.html(coods[0].toFixed(self.latLngFixed)+',<br/>'+coods[1].toFixed(self.latLngFixed));
				}
			});
			$(main.map.map.getViewport()).on('click',function(e){
				var pixel=[e.originalEvent.layerX,e.originalEvent.layerY];
				var feature=main.map.map.forEachFeatureAtPixel(pixel,function(feature,layer){
					return feature;
				});
				if(feature){
					if(self.deleting && feature.get(main.constants.featureTypeCode)=='measure'){
						self.measureSource.removeFeature(feature);
						if(self.measureSource.getFeatures().length===0){
							self.doneDeletingFeatures();
							self.startModifyingButton.addClass('none');
						}
						self.measurements.removeItem(feature.get('featureID'));
					}
				}
			});
			this.dragBox.on('boxend',function(e){
				var extent=self.dragBox.getGeometry().getExtent();
				self.measureSource.forEachFeatureIntersectingExtent(extent,function(feature){
					self.measureSource.removeFeature(feature);
					if(self.measureSource.getFeatures().length===0){
						self.doneDeletingFeatures();
						self.startModifyingButton.addClass('none');
					}
					self.measurements.removeItem(feature.get('featureID'));
				});
			});
			this.typeSelect.on('change',function(){
				self.currentType=$(this).val();
				self.stopMeasuring();
				self.startMeasuring();
			});
			if(this.unitSelect){
				this.unitSelect.on('change',function(){
					self.currentUnits=self.params.unit.options[$(this).val()];
				});
			}
			this.startMeasuringButton.on('click',function(){
				self.startMeasuring();
			});
			this.stopMeasuringButton.on('click',function(){
				self.stopMeasuring();
			});
			this.startModifyingButton.on('click',function(){
				self.startModifying();
			});
			this.stopModifyingButton.on('click',function(){
				self.stopModifying();
			});
			this.deleteFeaturesButton.on('click',function(){
				self.beginDeletingFeatures();
			});
			this.doneDeletingFeaturesButton.on('click',function(){
				self.doneDeletingFeatures();
			});
;			this.deleteAllButton.on('click',function(){
				self.measureSource.clear();
				self.measurements.clear();
				if(self.deleting){self.doneDeletingFeatures();
				}else{
					self.deleteFeaturesButton.addClass('none');
					self.doneDeletingFeaturesButton.addClass('none');
					self.deleteAllButton.addClass('none');
				}
				if(self.modifying){ self.stopModifying();}
				self.startModifyingButton.addClass('none');
			});
			this.measureForm.on('change','.formInput',function(e){
				e.preventDefault();
				self.measuringInputChange();
			});
		};
		this.setParams();
		this.currentType=this.params.type.default_val;
		if(this.params.unit){this.currentUnits=this.params.unit.options[this.params.unit.default_val];
		}else{this.currentUnits='english';}
		this.measureSource=new ol.source.Vector();
		this.measureVector=new ol.layer.Vector({
			source:this.measureSource,
			style:this.getStyle()
		});
		this.map.map.addLayer(this.measureVector);
		this.select=new ol.interaction.Select({});
		this.modify=new ol.interaction.Modify({
			features:this.select.getFeatures(),
			deleteCondition:function(event){
				return ol.events.condition.shiftKeyOnly(event) && ol.events.condition.singleClick(event);
			}
		});
		this.dragBox=new ol.interaction.DragBox({
			condition:ol.events.condition.shiftKeyOnly,
			style:this.getStyle()
		});
		this.createHTML();
		main.utilities.setCurrentVals(this.measureForm,this.params);
		this.initEvents();
	};
	pg.prototype={
	};
});