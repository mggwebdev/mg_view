define(function(require){
	return function pg(args){
		var self=this;
		this.defaultSortBy='pid';
		this.currentSortBy=this.defaultSortBy;
		this.defaultSortOrder='DESC';
		this.currentSortOrder=this.defaultSortOrder;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.getSortedKeys=function(field,sortOrder){
			var pids=[];
			self.tbody.find('tr').each(function(){
				pids.push([$(this).data('pid'),$(this).find('.sortableCell_'+field).data('sortval'),$(this)]);
			});
			pids=pids.sort(function(a,b){
				if(a[1]>b[1]){return 1;}
				if(a[1]<b[1]){return -1;}
				return 0;
			});
			if(sortOrder=='DESC'){pids.reverse();}
			return pids;
		};
		this.sortTable=function(field,sortOrder,dontFlop){
			sortOrder=sortOrder || self.currentSortOrder;
			if(!dontFlop){
				if(self.currentSortBy==field){
					if(sortOrder=='DESC'){sortOrder='ASC';
					}else{sortOrder='DESC';}
				}else{
					sortOrder=self.defaultSortOrder;
				}
				self.thead.find('.tableSortTH_'+field).data('sortorder',sortOrder);
			}
			self.currentSortBy=field;
			var keys=self.getSortedKeys(field,sortOrder),tr,checked;
			for(var t=0;t<keys.length;t++){
				self.tbody.append(keys[t][2]);
			}
		};
		(this.init=function(){
			self.thead.find('.tableSortTH').data('sortorder',self.defaultSortOrder);
			// self.sortTable();
			self.thead.on('click','.tableSortTH',function(){
				self.sortTable($(this).data('field'),$(this).data('sortorder'));
			});
		})();
		
		/*	
		
			Instructions:
			
			Instantiate like this:
			self.signOffTableMod=new Table({
				thead:self.yourClickableTHead,
				tbody:self.yourSortableTBody
			});
			Add 'tableSortTH' class and 'tableSortTH_'+field to th's that you want sortable
			Add 'sortableCell_'+field class and sortval data attribute with the value to sort by to td's
			
			Sort table like this:
			self.signOffTableMod.sortTable('dbh_exact','DESC',dontFlop)
			or click .tableSortTH 
		
		*/
	};
});