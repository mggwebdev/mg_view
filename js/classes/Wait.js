define(function(require){
	return function pg(criteria,onComplete,criteriaParams,onCompleteParams){
		var self=this;
		this.id=this.id || main.utilities.generateUniqueID();
		this.criteria=criteria;
		this.onComplete=onComplete;
		this.criteriaParams=criteriaParams;
		this.onCompleteParams=onCompleteParams;
		this.interval=200;
		this.maxIts=400;
		this.currentIt=0;
		this.stop=false;
		this.stopIts=function(){
			clearTimeout(self.timeout);
			self.stop=true;
		};
		this.nextIt=function(){
			if(!this.stop){
				var response=self.criteria(self.criteriaParams);
				if(response){
					return self.onComplete(response,self.onCompleteParams);
				}else{
					if(self.currentIt<self.maxIts){
						self.currentIt++;
						self.timeout=setTimeout(function(){self.nextIt()},self.interval);
					}else{
						return false;
					}
				}
			}else{
				clearTimeout(self.timeout);
			}
		};
		if(this.criteria && this.onComplete){
			this.nextIt();
		}else{
			return false;
		}
	}
});