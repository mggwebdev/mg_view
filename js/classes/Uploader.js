define(function(require){
	return function pg(args){
		var self=this;
		this.modifies_layer='';
		this.uploading=false;
		(this.setArgs=function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		(this.createHTML=function(){
			var title='Upload Data';
			self.paramsInput=$('<input type="hidden" name="params" class="formInput hiddenInput"/>');
			var table=$('<table class="uploaderTable">')
				.append($('<thead>')
					.append($('<tr>')
						.append($('<th colspan="2">')
							.append($('<div>').addClass('tdLiner center').text(title))
						)
					)
				)
				.append($('<tbody>')
					.append($('<tr>')
						.append($('<td>')
							.append($('<div>').addClass('tdLiner').html('Upload File'))
						)
						.append($('<td>')
							.append($('<div>').addClass('tdLiner')
								.append('<input type="file" name="uploadFile" class="formInput fileInput" multiple/>')
								.append('<input type="hidden" name="action" value="uploadData" class="formInput hiddenInput"/>')
								.append(self.paramsInput)
							)
						)
					)
					.append($('<tr>')
						.append($('<td colspan="2">')
							.append($('<div>').addClass('tdLiner center formConcButtons').html('<input type="submit" class="button submit formConcButton" value="Upload"/>'))
						)
					)
				)
			self.html=$('<form enctype="multipart/form-data">').addClass('uploaderForm')
				.append(table)
		})();
		this.dataMergedOK=function(){
			alert('Data Updated Successfully.');
		};
		this.dataLoaded=function(args){
			var pids=[];
			var files=args.files;
			for(var i=0;i<files.length;i++){
				var data=files[i].data;
				if(data && Object.keys(data).length>0){
					var fields=self.context.fields;
					for(var key in data){
						var newData=main.utilities.uploadedToVal(data[key],fields,self.table);
						pid=newData.pid;
						if(self.dataTable){
							self.dataTable.updateRow(newData);
							// self.dataTable.updateCurrentVals(newData);
						}
						if(pid){pids.push(pid);}
					}
				}
			}
			if(pids.length>0){
				if(self.dataTable){
					main.dataEditor.updateData({
						pids:pids,
						callback:self.dataMergedOK
					});
					// self.dataTable.updateData({pids:pids,callback:self.dataMergedOK});
				}
			}
		};
		(this.initEvents=function(){
			self.html.on('submit',function(e){
				e.preventDefault();
				main.layout.loader.loaderOn();
				if(!self.uploading){
					self.uploading=true;
					var lookup_table_map='';
					if(self.modifies_layer){
						var inv=main.invents.inventories[self.modifies_layer];
						if(inv){
							lookup_table_map=inv.properties.lookup_table_map;
						}
					}
					var params={
						modifies_layer:self.modifies_layer,
						lookup_table_map:lookup_table_map,
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]
					}
					self.paramsInput.val(JSON.stringify(params));
					var formData=new FormData($(this)[0]);
					var file=$(this).find('*[name="uploadFile"]').get(0);
					for(var i=0,len=file.files.length;i<len;i++){
						formData.append('upload_'+(i+1),file.files[i])
					}
					if(len>0){
						$.ajax({
							type:'POST',
							url:main.mainPath+'server/db.php',
							data:formData,
							async:false,
							cache:false,
							contentType:false,
							processData:false,
							success:function(response){
								self.uploading=false;
								if(response){
									var response=JSON.parse(response);
									if(response.status=='OK'){
										var files=response.files;
										// if(self.dataTable && !self.dataTable.loaded){self.dataTable.loadFeatures({files:files,callback:self.dataLoaded});}
										// else{self.dataLoaded({files:files});}
									}else{
										alert(response.status+': '+(response.message || ''));
									}
								}
								main.layout.loader.loaderOff();
							},
							error:function(response){
								alert('Error');
								console.log(response);
							}
						});
					}else{
						alert('Choose an excel file to upload.');
					}
				}
			});
		})();
	};
});