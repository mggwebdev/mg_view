define(function(require){
	return function pg(default_val,id){
		var self=this;
		this.id=id;
		this.default_val=default_val;
		this.onState=false;
		this.type='single';
		this.alignments={
			'tLeft':{
				name:'tLeft',
				alias:'Top Left'
			},
			'tMiddle':{
				name:'tMiddle',
				alias:'Top Middle'
			},
			'tRight':{
				name:'tRight',
				alias:'Top Right'
			},
			'mLeft':{
				name:'mLeft',
				alias:'Middle Left'
			},
			'mMiddle':{
				name:'mMiddle',
				alias:'Middle Middle'
			},
			'mRight':{
				name:'mRight',
				alias:'Middle Right'
			},
			'bLeft':{
				name:'bLeft',
				alias:'Bottom Left'
			},
			'bMiddle':{
				name:'bMiddle',
				alias:'Bottom Middle'
			},
			'bRight':{
				name:'bRight',
				alias:'Bottom Right'
			}
		};
		this.createAlignment=function(){
			this.input=$('<input name="'+this.id+'" type="hidden" value="'+this.default_val+'">').addClass('alignInput formInput '+this.id).data('name',this.id);
			this.alignmentsWrap=$('<div>').addClass('alignmentsWrap cf');
			this.container=$('<div>').addClass('alignment')
				.append(this.input)
				.append(this.alignmentsWrap);
			var activeClass;
			for(var key in this.alignments){
				activeClass='';
				if(key==this.default_val) activeClass=' activeAlignmentEle';
				this.alignmentsWrap.append($('<div title="'+this.alignments[key].alias+'">').addClass('alignmentEle fLeft '+key+activeClass).data('alignment',key))
			}
		};
		this.initEvents=function(){
			this.alignmentsWrap.on('click','.alignmentEle',function(){
				if(self.type=='single') self.alignmentsWrap.find('.alignmentEle').removeClass('activeAlignmentEle');
				$(this).toggleClass('activeAlignmentEle');
				self.input.val($(this).data('alignment')).change();
			});
		};
		this.createAlignment();
		this.initEvents();
	};
});