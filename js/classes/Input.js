define(function(require){
	return function pg(args){
		var self=this;
		var Alignment=require('./Alignment');
		var AvailabilitySelector=require('./AvailabilitySelector');
		var Checkbox=require('./Checkbox');
		var DateMod=require('./DateMod');
		var EditableList=require('./EditableList');
		var LookUp=require('./LookUp');
		var MapPoint=require('./MapPoint');
		var MapPolygon=require('./MapPolygon');
		var Radio=require('./Radio');
		var Signature=require('./Signature');
		var Time=require('./Time');
		var UniqueSelect=require('./UniqueSelect');
		var Wait=require('./Wait');
		this.dontAddDefault=false;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.classes=this.classes || '';
		this.data=this.data || null;
		this.lookUpTableLoadCheck=function(){
			if(main.data.lookUpTables[self.lookUpTableName].items){return true;}else{return false;}
		};
		this.lookUpTablesLoaded=function(params){
			self.lookUpTable.updateInput(params.value);
		};
		this.createSelect=function(key,options,on_change_cb,disabled,readonly){
			if(self.setToVal || self.setToVal=='' || self.setToVal=='0'){self.value=self.setToVal;}
			disabled=disabled || '';
			readonly=readonly || '';
			key=key || '';
			var name='';
			if(key){name='name="'+key+'"';}
			self.container=$('<select '+name+disabled+readonly+'>').addClass('selectInput formInput uiInput toolBoxInput '+key+' '+self.classes).data('name',key);
			self.container.append($('<option value=""></option>'));
			for(var key2 in options){
				var selected='';
				if(key2==self.value && (self.value!='' || self.value===0)){selected=' selected="selected"';}
				self.container.append($('<option'+selected+' value="'+key2+'">').text(options[key2].alias));
			}
			if(on_change_cb){
				self.container.data('on_change',on_change_cb);
				self.container.on('change',function(){
					main.utilities.callFunctionFromName($(this).data('on_change'),window,{context:self});
				});
			}
		};
		this.exportPhotos=function(){
			main.attachments.export(self.value,'images/uploaded_images/');
		};
		this.createInput=function(){
			if(this.field){
				var param=this.field,id;
				var input_type=param.input_type;
				if(this.typeOverride){input_type=this.typeOverride;}
				var key=param.name || '',options;
				var default_val=param.default_val;
				if(this.dontAddDefault){default_val='';}
				var disabled='';
				if(param.disabled)disabled=' disabled';
				var readonly='';
				if(param.readonly){readonly=' readonly';}
				var maxlength='';
				if(param.character_limit || param.character_limit===0) maxlength=' maxlength="'+param.character_limit+'"';
				if(input_type=='number' || input_type=='text' || input_type=='hidden'){
					self.value=default_val,max='',min='',step='';
					if(!self.value && self.value!==0) self.value='';
					if(param.max || param.max===0) max=' max="'+param.max+'"';
					if(param.min || param.min===0) min=' min="'+param.min+'"';
					if(param.step || param.step===0) step=' step="'+param.step+'"';
					if(this.setToVal || this.setToVal=='')self.value=this.setToVal;
					var inputType=input_type
					// if(param[main.account.loggedInUser.user_type+'_required']=='email'){inputType='email';}
					this.container=$('<input type="'+inputType+'" name="'+key+'" value="'+self.value+'"'+max+min+step+disabled+readonly+maxlength+'>').addClass('textInput formInput uiInput toolBoxInput '+key+' '+this.classes).data('name',key);
					if(param.logic_check){
						this.container.data('logic_check',param.logic_check);
						this.container.on('change',function(e){
							self.container.val(main.utilities.callFunctionFromName($(this).data('logic_check'),window,{context:self}));
						});
					}
					if(param.on_change_cb){
						this.container.data('on_change',param.on_change_cb);
						this.container.on('change',function(){
							main.utilities.callFunctionFromName($(this).data('on_change'),window,{context:self});
						});
					}
					if(input_type=='number'){
						this.container.on('change',function(){
							$(this).data('lastval',$(this).val());
						});
					}
					if(param.placeholder){this.container.attr('placeholder',param.placeholder);}
				}else if(input_type=='textarea'){
					self.value=default_val;
					if(this.setToVal || this.setToVal=='')self.value=this.setToVal;
					this.container=$('<textarea name="'+key+'"'+disabled+readonly+'>').addClass('textareaInput formInput uiInput toolBoxInput '+key+' '+this.classes).data('name',key).html(self.value);
					if(param.on_change_cb){
						this.container.data('on_change',param.on_change_cb);
						this.container.on('change',function(){
							main.utilities.callFunctionFromName($(this).data('on_change'),window,{context:self});
						});
					}
					if(param.placeholder){this.container.attr('placeholder',param.placeholder);}
				}else if(input_type=='select'){
					self.value=default_val;
					options=param.options;
					on_change_cb=param.on_change_cb;
					self.createSelect(key,options,on_change_cb,disabled,readonly);
				}else if(input_type=='checkbox'){
					self.value=default_val;
					if(this.setToVal || this.setToVal=='')self.value=this.setToVal;
					var tempVals,dataCB;
					tempVals=[self.value]
					if(self.value && (typeof self.value=='string' || self.value.constructor===Array)){
						if(typeof self.value=='string'){
							tempVals=self.value.split(',');
						}else if(self.value.constructor===Array){
							tempVals=self.value;
						}
					}else if(param.data_type=='boolean' && self.value){
						tempVals=['true'];
					}
					options=param.options;
					this.container=$('<div>').addClass('checkboxesWrap checksableWrap');
					this.checkboxes={};
					id='checkbox_'+main.utilities.generateUniqueID()+'_'+key;
					if(self.sortBy && self.sortBy=='abc'){
						var optionKeys=main.utilities.sortObjectByAlias(options);
					}else{var optionKeys=Object.keys(options);}
					for(var b=0;b<optionKeys.length;b++){
						key2=optionKeys[b];
						selected=false;
						// if(self.value!=null && options[key2].value.toString()==self.value.toString()) selected=true;
						if(tempVals.indexOf(options[key2].value)>-1 || self.checked){selected=true;}
						var label='';
						if(!self.omitAlias){
							var label=options[key2].alias;
							if(param.omit_alias){var label='';}
						}
						dataCB=[['option',key2]];
						if(param.on_change_cb){dataCB.push(['on_change',param.on_change_cb])}
						this.checkboxes[key2]=new Checkbox({
							label:label,
							value:options[key2].value,
							checked:selected,
							name:id,
							classes:'checkboxInput formInput uiInput toolBoxInput '+key+'  '+id+' '+key2+' '+this.classes,
							data:dataCB,
							disabled:disabled,
							readonly:readonly
						});
						this.container.append(this.checkboxes[key2].html);
						if(param.on_change_cb){
							this.container.find('.checkboxInput').on('change',function(){
								main.utilities.callFunctionFromName($(this).data('on_change'),window,{context:self});
							});
						}
						// $('<div>').addClass('checkboxWrap')
							// .append($('<input type="checkbox" id="'+id+'" name="'+key+'" value="'+options[key2].value+'"'+selected+disabled+readonly+'>').addClass('checkboxInput formInput uiInput toolBoxInput '+key+' '+this.classes).data('name',key))
							// .append($('<label for="'+id+'"><span>'+options[key2].alias+'</span></label>'))
						// );
					}
				}else if(input_type=='radio'){
					self.value=default_val;
					if(this.setToVal || this.setToVal=='')self.value=this.setToVal;
					options=param.options;
					this.container=$('<div>').addClass('radiosWrap checksableWrap');
					this.radios={}
					id='radio_'+main.utilities.generateUniqueID()+'_'+key;
					var dataCB;
					for(var key2 in options){
						selected=false;
						if(self.value!=null && options[key2].value.toString()==self.value.toString() || self.checked) selected=true;
						var label='';
						var label=options[key2].alias;
						if(param.omit_alias){var label='';}
						dataCB=[['option',key2]];
						if(param.on_change_cb){dataCB.push(['on_change',param.on_change_cb])}
						this.radios[key2]=new Radio({
							label:label,
							value:options[key2].value,
							checked:selected,
							name:id,
							classes:'radioInput formInput uiInput toolBoxInput '+key+' '+id+' '+key2+' '+this.classes,
							data:dataCB,
							disabled:disabled,
							readonly:readonly
						});
						this.container.append(this.radios[key2].html);
						if(param.on_change_cb){
							this.container.find('.checkboxInput').on('change',function(){
								main.utilities.callFunctionFromName($(this).data('on_change'),window,{context:self});
							});
						}
						
						// l$('<div>').addClass('radioWrap')
							// .append($('<input type="radio" id="'+id+'" name="'+key+'" value="'+options[key2].value+'"'+selected+disabled+readonly+'>').addClass('radioInput formInput uiInput toolBoxInput '+key+' '+this.classes).data('name',key))
							// .append($('<label for="'+id+'"><span>'+options[key2].alias+'</span></label>'))
						
					}
				}else if(input_type=='date'){
					self.value=default_val;
					if(this.setToVal || this.setToVal=='')self.value=this.setToVal;
					if(param.sub_data_type=='season'){
						self.season=new UniqueSelect({
							type:'season',
							value:self.value,
							fieldName:key
						});
						this.container=self.season.container;
						if(param.on_change_cb){
							self.season.input.data('on_change',param.on_change_cb);
							self.season.input.on('change',function(){
								main.utilities.callFunctionFromName($(this).data('on_change'),window,{context:self});
							});
						}
					}else if(param.sub_data_type=='time'){
						if(self.value && !isNaN(Number(self.value))){
							self.value=Number(self.value);
						}else{self.value='';}
						var cb=param.on_change_cb || null;
						self.time=new Time({
							readonly:param.readonly,
							disabled:param.disabled,
							value:self.value,
							onChange:cb,
							classes:'formInput uiInput toolBoxInput '+key+' '+this.classes
						});
						this.container=self.time.html
						if(param.on_change_cb){
							self.time.valInput.data('on_change',param.on_change_cb);
							self.time.valInput.on('change',function(){
								main.utilities.callFunctionFromName($(this).data('on_change'),window,{context:self});
							});
						}
					}else{
						if(self.value && !isNaN(Number(self.value))){
							// self.value=main.utilities.formatDate(Number(self.value),null,null,true,null);
							self.value=main.utilities.formatDate(Number(self.value));
						}else{self.value='';}
						var dateMod=new DateMod({
							disabled:param.disabled,
							readonly:param.readonly,
							name:key,
							value:self.value,
							classes:'formInput uiInput toolBoxInput '+key+' '+this.classes,
							on_change_cb:param.on_change_cb,
						});
						this.container=dateMod.html;
					}
				}else if(input_type=='file'){
					self.value=default_val;
					if(this.setToVal){
						self.value=this.setToVal;
						if(typeof self.value=='string'){self.value=JSON.parse(self.value);}
					}else{self.value=[];}
					var multiple='',name=key;
					if(param.allow_multiple){
						multiple=' multiple',name+='[]';
					}
					this.currentPhotos=new EditableList(null,self.photoRemoved,true);
					this.currentPhotos=new EditableList({
						removeCallBack:self.photoRemoved,
						removeable:true,
						exportable:true,
						exportCB:self.exportPhotos
					});
					if(self.value){
						for(var v=0;v<self.value.length;v++){
							this.currentPhotos.addItem(self.value[v].servFileName,self.value[v].fileName,{field:param,pid:self.pid});
						}
					}
					this.inputHTML=$('<input type="file" name="'+name+'"'+multiple+disabled+readonly+'>').addClass('fileInput formInput uiInput toolBoxInput '+key+' '+this.classes).data('name',key);
					this.container=$('<div class="filePkgWrap">').data('layerName',self.layerName).data('pid',self.pid)
						.append(this.currentPhotos.container)
						.append(this.inputHTML);
					if(!main.tables[self.layerName].atchs.items[self.pid]){main.tables[self.layerName].atchs.items[self.pid]={fields:{}}}
					if(!main.tables[self.layerName].atchs.items[self.pid].fields[param.name]){main.tables[self.layerName].atchs.items[self.pid].fields[param.name]={};}
					if(self.is_in_edit_form){
						main.tables[self.layerName].atchs.items[self.pid].fields[param.name].etInput=this.container;
						main.tables[self.layerName].atchs.items[self.pid].fields[param.name].etCurrentPhotos=this.currentPhotos;
					}else if(self.is_in_data_table){
						main.tables[self.layerName].atchs.items[self.pid].fields[param.name].dtInput=this.container;
						main.tables[self.layerName].atchs.items[self.pid].fields[param.name].dtCurrentPhotos=this.currentPhotos;
					}else if(self.is_in_popup){
						main.tables[self.layerName].atchs.items[self.pid].fields[param.name].puInput=this.container;
						main.tables[self.layerName].atchs.items[self.pid].fields[param.name].puCurrentPhotos=this.currentPhotos;
					}
					// main.tables[self.layerName].atchs.items[self.pid].input=this.container;
					// main.tables[self.layerName].atchs.items[self.pid].currentPhotos=this.currentPhotos;
						// main.tables[self.layerName].map_layer.features[self.pid].currentPhotos=this.currentPhotos;
					if(param.on_change_cb){
						this.inputHTML.data('on_change',param.on_change_cb);
						this.inputHTML.on('change',function(){
							main.utilities.callFunctionFromName($(this).data('on_change'),window,{context:self});
						});
					}
				}else if(input_type=='html'){
					this.container=$('<div>').addClass('htmlInput '+key+' '+this.classes).data('name',key).html(param.html);
				}else if(input_type=='color'){
					id='spectrum_'+main.utilities.generateUniqueID()+'_'+key;
					this.container=$('<input type="text" name="'+key+'" id="'+id+'">').addClass('textInput formInput uiInput toolBoxInput '+key+' '+this.classes).data('name',key)
					param.spectrumID=id;
					if(param.on_change_cb){
						this.container.data('on_change',param.on_change_cb);
						this.container.on('change',function(){
							main.utilities.callFunctionFromName($(this).data('on_change'),window,{context:self});
						});
					}
				}else if(input_type=='availabilitySelect'){
					self.value=default_val;
					if(this.setToVal || this.setToVal==''){self.value=this.setToVal;}
					self.availabilitySelect=new AvailabilitySelector({
						layerName:self.layerName,
						field:param,
						value:self.value
					});
					this.container=self.availabilitySelect.availSelWrap;
					if(param.on_change_cb){
						this.availabilitySelect.input.data('on_change',param.on_change_cb);
						this.availabilitySelect.input.on('change',function(){
							main.utilities.callFunctionFromName($(this).data('on_change'),window,{context:self});
						});
					}
				}else if(input_type=='align'){
					self.value=default_val;
					if(this.setToVal || this.setToVal=='')self.value=this.setToVal;
					this.align=new Alignment(self.value,key)
					this.container=$('<div>').addClass('alignPkg')
						.append(this.align.container);
					if(param.on_change_cb){
						this.align.input.data('on_change',param.on_change_cb);
						this.align.input.on('change',function(){
							main.utilities.callFunctionFromName($(this).data('on_change'),window,{context:self});
						});
					}
				}else if(input_type=='signature'){
					self.value=default_val;
					if(this.setToVal || this.setToVal=='')self.value=this.setToVal;
					this.signature=new Signature(self.value,key,this.callback);
					this.container=$('<div>').addClass('sigPkg')
						.append(this.signature.container);
					if(param.on_change_cb){
						this.signature.input.data('on_change',param.on_change_cb);
						this.signature.input.on('change',function(){
							main.utilities.callFunctionFromName($(this).data('on_change'),window,{context:self});
						});
					}
				}else if(input_type=='map_point'){
					self.value=default_val;
					if(this.setToVal || this.setToVal=='')self.value=this.setToVal;
					this.mapPoint=new MapPoint({origValue:self.value,key:key,map:main.map,type:'Point'});
					this.container=$('<div>').addClass('mapPointPkg '+this.classes)
						.append(this.mapPoint.container);
					if(param.on_change_cb){
						this.mapPoint.input.data('on_change',param.on_change_cb);
						this.mapPoint.input.on('change',function(){
							main.utilities.callFunctionFromName($(this).data('on_change'),window,{context:self});
						});
					}
				}else if(input_type=='map_polygon'){
					self.value=default_val;
					if(this.setToVal || this.setToVal=='')self.value=this.setToVal;
					this.mapPoint=new MapPoint({origValue:self.value,key:key,map:main.map,type:'Polygon'});
					this.container=$('<div>').addClass('mapPointPkg '+this.classes)
						.append(this.mapPoint.container);
					if(param.on_change_cb){
						this.mapPoint.input.data('on_change',param.on_change_cb);
						this.mapPoint.input.on('change',function(){
							main.utilities.callFunctionFromName($(this).data('on_change'),window,{context:self});
						});
					}
				}
			}else{
				if(self.lookup_table){
					self.createSelect(key,options,on_change_cb,disabled,readonly);
				}
			}
			if((param && param.lookup_table && param.input_type == 'select' && param.with_lu_popup) 
				|| (self.lookup_table && self.lookup_table_field)){
				this.lookupInput=this.container;
				this.container=$('<div class="inputWithLookUpTable">').append(this.container);
				var field0=null;
				var options=null;
				var lookup_table_field=null;
				if(param){
					this.lookUpTableName=param.lookup_table;
					field0=param;
					options=param.options;
					this.omit_lookup_in_form=param.omit_lookup_in_form;
				}else{
					this.lookUpTableName=self.lookup_table;
					lookup_table_field=self.lookup_table_field;
				}
				this.lookUpTable=new LookUp({
					tableName:this.lookUpTableName,
					input:this.lookupInput,
					field:field0,
					lookup_table_field:lookup_table_field,
					layerName:self.layerName
					// extraOptions:options
				});
				// self.tableWait=new Wait(self.lookUpTableLoadCheck,self.lookUpTablesLoaded);
				if(main.data.lookUpTables[self.lookUpTableName].items){
					self.lookUpTablesLoaded({value:self.value});
				}else{
					main.data.lookUpTables[self.lookUpTableName].waiting.push([self.lookUpTablesLoaded,{value:self.value}]);
				}
				this.lookupInput.on('click',function(e){
					e.preventDefault();
					if(!self.omit_lookup_in_form || !self.is_in_form){self.lookUpTable.open();}
				});
			}
			if(this.data){
				for(var a=0;a<this.data.length;a++){
					this.container.data(this.data[a][0],this.data[a][1]);
				}
			}
			if(this.specialField && param){param.specialInput=this.container;}
		};
		this.createInput();
	};
});
