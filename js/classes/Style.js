define(function(){
	return function pg(vectorLayer){
		var self=this;
		this.vectorLayer=vectorLayer;
		this.setParams=function(){
			var params=main.layers.params;
			this.params={};
			for(var key in params){
				this.params[key]={
					current_val:params[key].current_val
				}
			}
		};
		this.createNewStyle=function(feature,resolution,dom){
			return new ol.style.Style({
				fill: new ol.style.Fill({
					color: self.params.fill_color.current_val
				}),
				stroke: new ol.style.Stroke({
					color: self.params.stroke_color.current_val,
					width: self.params.stroke_width.current_val
				})
			})
		};
		this.createNewLabeledStyle=function(feature,resolution,dom){
			var labels=self.vectorLayer.labels;
			var text=labels.createTextStyle(feature,resolution,dom);
			return new ol.style.Style({
				fill: new ol.style.Fill({
					color: this.params.fill_color.current_val
				}),
				stroke: new ol.style.Stroke({
					color: this.params.stroke_color.current_val,
					width: this.params.stroke_width.current_val
				}),
				text:text
			})
		};
		this.setStyleFunction=function(){
			if(this.vectorLayer.labels.withLabels){
				this.vectorLayer.layer.setStyle(this.labelStyleFunction);
			}else{
				this.vectorLayer.layer.setStyle(this.styleFunction);
			}
		};
		this.labelStyleFunction=function(feature,resolution,dom){
			return [self.vectorLayer.style.createNewLabeledStyle(feature,resolution,dom)];
		};
		this.styleFunction=function(feature,resolution,dom){
			return [self.vectorLayer.style.createNewStyle(feature,resolution,dom)];
		};
		this.setLayer=function(layer){
			this.vectorLayer=layer;
		};
		this.updateLayers=function(){
			main.utilities.setCurrentVals(main.layers.layersForm,this.params);
			this.setStyleFunction();
		};
		this.getColor=function(max,min,value){
			if(this.colors.currentColorGradient!='Random'){
				var normalizeTo=510;
			}else{
				var normalizeTo=1530;
			}
			var red=0,green=0,blue=0;
			var number=((value-min)/(max-min))*normalizeTo;
			if(isNaN(number)) number=0
			return this.getColorNumber(number,normalizeTo,max);
		};
		this.getColorNumber=function(number,normalizeTo,colorLength,type){
			var red=0,green=0,blue=0;
			if(type=='Blue-Red'){
				if(number<=255){
					blue=255;
					red=number;
				}else{
					blue=normalizeTo-number;
					red=255;
				}
			}if(type=='Red-Blue'){
				if(number<=255){
					red=255;
					blue=number;
				}else{
					red=normalizeTo-number;
					blue=255;
				}
			}else if(type=='Green-Red'){
				if(number<=255){
					green=255;
					red=number;
				}else{
					green=normalizeTo-number;
					red=255;
				}
			}else if(type=='Red-Green'){
				if(number<=255){
					red=255;
					green=number;
				}else{
					red=normalizeTo-number;
					green=255;
				}
			}else if(type=='Random'){
				number=number*colorLength/(colorLength+1);
				if(number<=255){
					red=255;
					green=number;
				}else if(number<=510){
					green=255; 
					red=510-number;
				}else if(number<=765){
					green=255; 
					blue=number-510;
				}else if(number<=1020){
					blue=255;
					green=1020-number;
				}else if(number<=1275){
					blue=255;
					red=number-1020;
				}else if(number<=1530){
					red=255;
					blue=1530-number;
				}
			}else if(type=='Vivid Random'){
				var array=[0,1,2];
				zeroSelector=Math.floor(Math.random()*2.9999999);
				toZero=array.splice(zeroSelector,1);
				highSelector=Math.floor(Math.random()*1.9999999);
				toHigh=array.splice(highSelector,1);
				var rgb=[Math.random()*255,Math.random()*255,Math.random()*255];
				rgb[toZero]=0;
				rgb[toHigh]=255;
				red=rgb[0],green=rgb[1],blue=rgb[2];
			}
			return [Math.round(red),Math.round(green),Math.round(blue),this.defaultOpacity];
		}
		this.setParams();
	};
});