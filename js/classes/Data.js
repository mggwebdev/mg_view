define(function(require){
	return function pg(args){
		var self=this;
		this.updateData=function(args){
			if(!args.context || !args.changeFields){return false;}
			var pids=args.pids;
			var changeFields=args.changeFields;
			var from=args.from || 'editForm';
			context=args.context;
			if(context.properties[main.account.loggedInUser.user_type+'_dt_editable']){
				if(!args.fileChanged){
					if(context.properties.internal_field_map && context.properties.internal_field_map.last_modified && context.fields[context.properties.internal_field_map.last_modified]){
						changeFields.push({field:context.fields[context.properties.internal_field_map.last_modified],val:new Date().getTime()});
					}
					var recordPkgs={},otherLayerPkg=null,fields=[],field,pid;
					var features,withEcobensUpdate=false,key,value,hasLastMod,time_tracked,updateSignOff;
					var fieldMap=context.properties.internal_field_map || null;
					for(var i=0;i<pids.length;i++){
						pid=pids[i];
						recordPkgs[pid]={};
						for(var b=0;b<changeFields.length;b++){
							key=changeFields[b].field.name;
							field=changeFields[b].field;
							if(field.active && field.in_update && field.input_type!='file' && field.input_type!='html'){
								value=changeFields[b].val;
								hasLastMod=false;
								if(context.properties.internal_field_map && context.properties.internal_field_map.last_modified && context.properties.internal_field_map.last_modified==key && context.fields[key]){
									hasLastMod=true;
								}
								if(main.tables[context.layerName].map_layer && main.tables[context.layerName].map_layer.features[pid]){
									if(context.map_layer.features[pid].editLargeTable && hasLastMod){
										main.misc.blockUpdateData=true;
										context.map_layer.features[pid].editLargeTable.formWrap.find('.formInput.last_modified').val(main.utilities.formatDate(value)).change();
										main.misc.blockUpdateData=false;
									}
									main.tables[context.layerName].map_layer.features[pid].properties[key]=value;
									features=main.tables[context.layerName].map_layer.layer.source.getFeatures();
									for(var t=0;t<features.length;t++){
										if(features[t].get('pid')==pid){features[t].set(key,value);break;}
									}
								}
								updateSignOff=null;
								if(main.withs.with_wom && main.withs.with_wom.value && context.properties.internal_field_map && context.properties.internal_field_map.status && context.properties.internal_field_map.status==key){
									updateSignOff=true;
								}
								time_tracked=null;
								if(field.time_tracked){time_tracked=true;}
								recordPkgs[pid][key]={
									value:value,
									data_type:field.data_type,
									sub_data_type:field.sub_data_type,
									input_type:field.input_type,
									time_tracked:time_tracked,
									updateSignOff:updateSignOff,
									name:key
								}
								//update other layers
								if(context.properties.special_connections && context.properties.special_connections[key]){
									if(context.properties.special_connections[key].type=='update_other'){
										if(!otherLayerPkg){otherLayerPkg={};}
										layer0=context.properties.special_connections[key].other_layer;
										link_field=context.properties.special_connections[key].link_field;
										if(!otherLayerPkg[pid]){otherLayerPkg[pid]={};}
										field0=main.tables[layer0].context.fields[context.properties.special_connections[key].to_effect];
										time_tracked0=null;
										if(field.time_tracked){time_tracked0=true;}
										otherLayerPkg[pid][link_field]={
											value:value,
											data_type:field0.data_type,
											sub_data_type:field0.sub_data_type,
											input_type:field0.input_type,
											time_tracked:time_tracked0,
											trigger_field:key,
											name:field0.name,
											otherLayer:layer0,
											link_field:link_field
										};
									}
								}
								// if(field.input_type=='signature' && row.fields[key].input.signature.img){
									// recordPkgs[pid][key].sigImg=row.fields[key].input.signature.img;
								// }
								if(i===0){fields.push(key)};
							}
						}
						if(!Object.keys(recordPkgs[Object.keys(recordPkgs)[0]]).length){return;}
						if(args.ecoVals && (args.ecoVals.dbh.val || args.ecoVals.dbh.val==0)){
							var specy=null;
							var sp_code=null;
							var is_blank=null;
							if(args.ecoVals.main_species_field.val && main.data.lookUpTables.species && main.data.lookUpTables.species.items[args.ecoVals.main_species_field.val]){
								specy=main.data.lookUpTables.species.items[args.ecoVals.main_species_field.val];
								if(specy && specy.sp_code){
									if(specy.sp_code.toLowerCase().trim()=='n/a' || args.ecoVals.dbh.val==0){is_blank=true;}
									sp_code=specy.sp_code;
								}
							}
							if(!specy){is_blank=true;}
							recordPkgs[pid]['_ecobensInfo']={
								dbh:args.ecoVals.dbh.val,
								land_use:args.ecoVals.land_use.val,
								sp_code:sp_code,
								is_blank:is_blank
							}
							withEcobensUpdate=true;
						}
					}
					var ecoDetails='';
					if(withEcobensUpdate){
						ecoDetails=main.data.std_settings.ecobens;
						if(ecoDetails.sortedKeys){delete ecoDetails.sortedKeys;}
					}
					latlng=args.latlng || null;
					dontUpdateAddress=args.dontUpdateAddress || false;
					// if(otherLayerPkg && !confirm('Do you want to update associated inventories?')){
						otherLayerPkg=null;
					// }
					$.ajax({
						type:'POST',
						url:main.mainPath+'server/db.php',
						data:{
							params:{
								folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
								fields:fields,
								otherLayerPkg:otherLayerPkg,
								recordPkgs:recordPkgs,
								latlng:latlng,
								fieldMap:fieldMap,
								ecoDetails:ecoDetails,
								dontUpdateAddress:dontUpdateAddress,
								table:context.layerName
							},
							action:'updateDataEntry'
						},
						success:function(response){
							if(response){
								response=JSON.parse(response);
								if(response.status=="OK"){
									if(main.dashboard && main.dashboard.active){main.dashboard.refresh();}
									var results=response.results,features;
									for(var i=0;i<results.length;i++){
										features=self.updateInfoPostAjax(results[i],response,context,args,context.layerName);
									}
									if(response.oLPResults){
										var oLPResults=response.oLPResults;
										for(var i=0;i<oLPResults.length;i++){
											self.updateInfoPostAjax(oLPResults[i],response,context,args,oLPResults[i].this_layer);
										}
									}
									var withTimeResponse=false;
									if(response.timeResponse && response.timeResponse.timeResults){
										var timeResults=response.timeResponse.timeResults,item;
										if(context.map_layer){
											for(var key in timeResults){
												if(timeResults[key].completedPIDs.length){
													for(var m=0;m<timeResults[key].completedPIDs.length;m++){
														item=context.map_layer.features[timeResults[key].completedPIDs[m]];
														if(item.editLargeTable){
															item.editLargeTable.formWrap.find('.formInput.'+key).val(timeResults[key].time_sensitive_info.completed_val);
														}
														if(item.editTable){
															item.editTable.table.find('.dataTableCurrent.'+key).html(main.utilities.getHTMLFromVal(timeResults[key].time_sensitive_info.completed_val,context.fields[key]));
														}
													}
												}
												if(timeResults[key].upcomingPIDs.length){
													for(var m=0;m<timeResults[key].upcomingPIDs.length;m++){
														item=context.map_layer.features[timeResults[key].upcomingPIDs[m]];
														if(item.editLargeTable){
															item.editLargeTable.formWrap.find('.formInput.'+key).val(timeResults[key].time_sensitive_info.upcoming_val);
														}
														if(item.editTable){
															item.editTable.table.find('.dataTableCurrent.'+key).html(main.utilities.getHTMLFromVal(timeResults[key].time_sensitive_info.upcoming_val,context.fields[key]));
														}
													}
												}
											}
										}
										withTimeResponse=true;
									}
									if(main.filter_legend && main.filter_legend.active && (response.fields.indexOf(main.filter_legend.current_filter_by)!=-1 || (withTimeResponse && Object.keys(response.timeResponse.timeResults).indexOf(main.filter_legend.current_filter_by)!=-1))){main.filter_legend.refresh(args.dontUpdateCharts);}
									if(args.latlng){
										var item,val;
										for(var i=0;i<results.length;i++){
											main.misc.blockUpdateData2=true;
											if(context.dataTable && from!='dataTable'){context.dataTable.updateRow(results[i]);}
											main.misc.blockUpdateData2=false;
											if(context.map_layer){
												item=context.map_layer.features[results[i].pid];
												if(item.editLargeTable && results[i].lat){
													item.editLargeTable.formWrap.find('.formInput.lat').val(results[i].lat);
													item.editLargeTable.formWrap.find('.formInput.lng').val(results[i].lng);
												}
												if(context.properties.internal_field_map){
													if(context.properties.internal_field_map.address_number && results[i][context.properties.internal_field_map.address_number]){
														val=results[i][context.properties.internal_field_map.address_number];
														context.map_layer.features[results[i].pid].fields[context.properties.internal_field_map.address_number].current_val=val;
														if(features){for(var t=0;t<features.length;t++){if(features[t].get('pid')==results[i].pid){features[t].set(context.properties.internal_field_map.address_number,val)}}}
														if(item.editTable){item.editTable.table.find('.dataTableCurrent.'+context.properties.internal_field_map.address_number).html(val);}
														if(item.editLargeTable){item.editLargeTable.formWrap.find('.formInput.'+context.properties.internal_field_map.address_number).val(val);}
													}
													if(context.properties.internal_field_map.street && results[i][context.properties.internal_field_map.street]){
														val=results[i][context.properties.internal_field_map.street];
														context.map_layer.features[results[i].pid].fields[context.properties.internal_field_map.street].current_val=val;
														if(features){for(var t=0;t<features.length;t++){if(features[t].get('pid')==results[i].pid){features[t].set(context.properties.internal_field_map.street,val)}}}
														if(item.editTable){item.editTable.table.find('.dataTableCurrent.'+context.properties.internal_field_map.street).html(val);}
														if(item.editLargeTable){item.editLargeTable.formWrap.find('.formInput.'+context.properties.internal_field_map.street).val(val);}
													}
													if(context.properties.internal_field_map.street_tree_on && results[i][context.properties.internal_field_map.street_tree_on]){
														val=results[i][context.properties.internal_field_map.street_tree_on];
														context.map_layer.features[results[i].pid].fields[context.properties.internal_field_map.street_tree_on].current_val=val;
														if(features){for(var t=0;t<features.length;t++){if(features[t].get('pid')==results[i].pid){features[t].set(context.properties.internal_field_map.street_tree_on,val)}}}
														if(item.editTable){item.editTable.table.find('.dataTableCurrent.'+context.properties.internal_field_map.street_tree_on).html(val);}
														if(item.editLargeTable){item.editLargeTable.formWrap.find('.formInput.'+context.properties.internal_field_map.street_tree_on).val(val);}
													}
													if(context.properties.internal_field_map.city && results[i][context.properties.internal_field_map.city]){
														val=results[i][context.properties.internal_field_map.city];
														context.map_layer.features[results[i].pid].fields[context.properties.internal_field_map.city].current_val=val;
														if(features){for(var t=0;t<features.length;t++){if(features[t].get('pid')==results[i].pid){features[t].set(context.properties.internal_field_map.city,val)}}}
														if(item.editTable){item.editTable.table.find('.dataTableCurrent.'+context.properties.internal_field_map.city).html(val);}
														if(item.editLargeTable){item.editLargeTable.formWrap.find('.formInput.'+context.properties.internal_field_map.city).val(val);}
													}
													if(context.properties.internal_field_map.zip && results[i][context.properties.internal_field_map.zip]){
														val=results[i][context.properties.internal_field_map.zip];
														context.map_layer.features[results[i].pid].fields[context.properties.internal_field_map.zip].current_val=val;
														if(features){for(var t=0;t<features.length;t++){if(features[t].get('pid')==results[i].pid){features[t].set(context.properties.internal_field_map.zip,val)}}}
														if(item.editTable){item.editTable.table.find('.dataTableCurrent.'+context.properties.internal_field_map.zip).html(val);}
														if(item.editLargeTable){item.editLargeTable.formWrap.find('.formInput.'+context.properties.internal_field_map.zip).val(val);}
													}
													if(context.properties.internal_field_map.address_num_street && results[i][context.properties.internal_field_map.address_num_street]){
														val=results[i][context.properties.internal_field_map.address_num_street];
														context.map_layer.features[results[i].pid].fields[context.properties.internal_field_map.address_num_street].current_val=val;
														if(features){for(var t=0;t<features.length;t++){if(features[t].get('pid')==results[i].pid){features[t].set(context.properties.internal_field_map.address_num_street,val)}}}
														if(item.editTable){item.editTable.table.find('.dataTableCurrent.'+context.properties.internal_field_map.address_num_street).html(val);}
														if(item.editLargeTable){item.editLargeTable.formWrap.find('.formInput.'+context.properties.internal_field_map.address_num_street).val(val);}
													}
												}
											}
										}
									}
									if(args.callback){args.callback(args,response);}
								}else{
									if(response.ERR_CODE==1){main.utilities.loggedOutNotice();}
									else{alert('Error: Data not saved. Please contact Plan-IT GEO');}
									console.log(response);
								}
							}
						}
					});
				}else{
					main.layout.loader.loaderOn();
					var type=null;
					for(var b=0;b<changeFields.length;b++){
						if(changeFields[b].field.name==args.fileChanged){
							type=changeFields[b].field.sub_data_type;
							break;
						}
					}
					if(type===null){return;}
					var params={
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						table:context.layerName,
						pid:pids[0],
						fieldName:args.fileChanged,
						type:type
					};
					var form=args.reference.closest('.itemForm');
					form.append($('<input type="hidden" name="action" value="updateDataEntryForFiles"/>'));
					form.append($('<input type="hidden" name="params"/>').val(JSON.stringify(params)));
					var formData=new FormData(form[0]);
					main.forrm=form;
					$.ajax({
						type:'POST',
						url:main.mainPath+'server/db.php',
						data:formData,
						async:true,
						cache:false,
						contentType:false,
						processData:false,
						success:function(response){
							main.layout.loader.loaderOff();
							if(response){
								response=JSON.parse(response);
								if(response.status=="OK"){
									if(context.map_layer){
										var feature;
										context.map_layer.features[pids[0]].properties[response.params.fieldName]=response.oldFiles;
										for(var i=0;i<main.layout.popups.length;i++){if(main.layout.popups[i].pid==pids[0]){main.layout.popups[i].properties[response.params.fieldName]=response.oldFiles;}}
										features=context.layer.layer.olLayer.getSource().forEachFeature(function(feature){
											if(feature.get('pid')==pids[0]){
												context.map_layer.features[pids[0]].feature=feature;
												context.map_layer.features[pids[0]].feature.set(response.params.fieldName,response.oldFiles);
												if(context.lastFocused && context.lastFocused.get('pid')==pids[0]){context.lastFocused.set(response.params.fieldName,response.oldFiles);}
												if(context.currentFocused && context.currentFocused.get('pid')==pids[0]){context.currentFocused.set(response.params.fieldName,response.oldFiles);}
											}
										});
										main.tables[context.layerName].atchs.items[pids[0]].fields[response.params.fieldName].photoInfo=response.oldFiles;
										for(var m=0;m<response.newFiles.length;m++){
											feature=context.map_layer.features[pids[0]];
											main.tables[context.layerName].atchs.items[pids[0]].fields[response.params.fieldName].etCurrentPhotos.addItem(response.newFiles[m].servFileName,response.newFiles[m].fileName,{field:feature.fields[response.params.fieldName].field,pid:pids[0]});
										}
									}
									if(context.dataTable && from!='dataTable'){
										main.misc.blockUpdateData2=true;
										context.dataTable.updateRow(response.results[0],true);
										main.misc.blockUpdateData2=false;
									}
									main.utilities.setPhotosButtons(context.layerName,pids[0],response.oldFiles.length);
									if(args.callback){args.callback(args,response);}
								}else{
									if(response.ERR_CODE==1){main.utilities.loggedOutNotice();}
									else{alert('Error: '+response.message);}
									console.log(response);
								}
							}
						}
					});
				}
			}
		};
		this.updateInfoPostAjax=function(result,response,context,args,layer){
			var features;
			var inventory0=null;
			if(layer && main.invents.inventories[layer]){
				inventory0=main.invents.inventories[layer];
			}
			if(inventory0 && context.map_layer){
				context.map_layer.features[result.pid].properties=result;
				for(var t=0;t<main.layout.popups.length;t++){if(main.layout.popups[t].pid==result.pid){main.layout.popups[t].properties=result;}}
				features=inventory0.layer.layer.olLayer.getSource().forEachFeature(function(feature){
					if(feature.get('pid')==result.pid){
						context.map_layer.features[result.pid].feature=feature;
						context.map_layer.features[result.pid].feature.setProperties(result);
						if(inventory0.lastFocused && inventory0.lastFocused.get('pid')==result.pid){inventory0.lastFocused.setProperties(result);}
						if(inventory0.currentFocused && inventory0.currentFocused.get('pid')==result.pid){inventory0.currentFocused.setProperties(result);}
					}
				});
				if(context.map_layer.features[result.pid].editForm){
					context.map_layer.features[result.pid].editForm.properties=result;
					if(context.map_layer.features[result.pid].editForm.timeTab){
						context.map_layer.features[result.pid].editForm.refreshTimeTracked();
					}
					if(context.map_layer.features[result.pid].editForm.editFormFieldsPreview){
						context.map_layer.features[result.pid].editForm.updateFieldPreviews(result);
					}
					if(response.signOffUpdated){
						context.map_layer.features[result.pid].editForm.refreshSignOff();
					}
				}
				if(context.map_layer.features[result.pid].popup &&  context.map_layer.features[result.pid].popup.ecobenPanel && context.map_layer.features[result.pid].popup.ecobenPanel.opened){
					context.map_layer.features[result.pid].popup.resetEcoBens();
				}
			}
			if(inventory0 && inventory0.properties.special_connections){
				if(inventory0.properties.special_connections._multiple && inventory0.properties.special_connections._multiple.effected_by){
					var fields=response.fields;
					var match=false;
					for(var o=0;o<fields.length;o++){
						if(inventory0.properties.special_connections._multiple.effected_by.indexOf(fields[o])>-1){match=true;}
					}
					if(match){
						var specialConnection=inventory0.properties.special_connections._multiple;
						var type=specialConnection.type;
						if(type=="sum_of_others"){
							main.utilities.doSpecialConnection(null,specialConnection,args.reference,null,inventory0.layerName,result.pid);
						}
					}
				}
			}
			return features;
		};
	}
});