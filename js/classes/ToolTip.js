define(function(require){
	return function pg(args){
		var self=this;
		this.toolTipActive=false;
		this.offsetX=18;
		this.offsetY=18;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.content=this.content || '';
		this.setContent=function(content){
			self.content=content;
			self.toolTipContent.html(self.content);
		};
		this.activate=function(){
			self.toolTipActive=true;
			self.onEvents();
		};
		this.deActivate=function(){
			self.toolTipActive=false;
			self.container.removeClass('block');
			self.offEvents();
		};
		this.onEvents=function(){
			self.container.add(self.reference).on('mousemove',self.mouseMoveFunction);
			if(!self.overDoc){$('body').on('mousemove',self.overMapCheck);}
		};
		this.offEvents=function(){
			self.container.add(self.reference).off('mousemove',self.mouseMoveFunction);
			if(!self.overDoc){$('body').off('mousemove',self.overMapCheck);}
		};
		this.overMapCheck=function(e){
			if($(e.target).closest('#'+self.reference.attr('id')).length===0){
				self.container.removeClass('block')
			}
		};
		this.mouseMoveFunction=function(e){
			if(self.toolTipActive){
				var x=e.pageX+self.offsetX;
				var y=e.pageY+self.offsetY;
				self.container.addClass('block').css('left',x).css('top',y);
			}
		};
		(this.create=function(){
			self.container=$('<div>').addClass('toolTip');
			self.toolTipContent=$('<div>').addClass('toolTipContent').html(self.content);
			self.container.append(self.toolTipContent);
			main.layout.toolTipsContainer.append(self.container);
			if(self.overDoc){
				self.reference=$('body');
			}else{
				self.reference=main.layout.mapContainer;
			}
		})();
		(this.initEvents=function(){
			// $('body').on('mousemove',self.overMapCheck)
		})();
	};
});