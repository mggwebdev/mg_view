define(function(require){
	return function pg(){
		var self=this;
		var Accordian=require('./Accordian');
		var Button=require('./Button');
		this.alias='Tools';
		this.name='toolBox';
		this.sortItems=function(sort_by){
			if(!sort_by){sort_by='abc';}
			self.accordian.sortShelves(sort_by);
		};
		// this.update=function(){
			// var shelves=self.accordian.shelves;
			// for(var key in shelves){
				// self.accordian.shelfInAppTest(shelves[key]);
			// }
			// self.noneTest();
		// };
		this.noneTest=function(){
			var shelves=self.groups.mainGenForms.shelves,inShelves,t;
			for(var key in shelves){
				inShelves=shelves[key].accordian.shelves;
				t=0;
				for(var key2 in inShelves){
					if(!inShelves[key2].shelf.hasClass('none')){
						t++;
						break;
					}
				}
				if(t>0){shelves[key].shelf.removeClass('none');
				}else{shelves[key].shelf.addClass('none');}
			}
		};
		this.removeShelves=function(){
			for(key in self.accordian.shelves){
				if(self.accordian.shelves[key].isGroup){
					if(self.accordian.shelves[key].shelf.find('.shelf').length>0){
						self.accordian.detachShelf(self.accordian.shelves[key]);
					}else{
						self.accordian.removeShelf(self.accordian.shelves[key]);
					}
				}
			}
		};
		this.recreate=function(){
			self.removeShelves();
			self.setGroups();
		};
		this.setGroups=function(){
			var item,currentGroup,groups,thisGroup,accordian,shelf,alias,setGroup,tooltip,shelfTemp;
			self.groups={};
			self.groups['mainGenForms']={
				accordian:self.accordian,
				shelves:{}
			}
			if(main.personal_settings.app_settings.toolbox_groups){
				var setGroups=main.personal_settings.app_settings.toolbox_groups;
			}else{return;}
			for(var key in self.accordian.shelves){
				item=self.accordian.shelves[key];
				accordian=self.accordian;
				if(item.groups){
					groups=item.groups;
					thisGroup=self.groups['mainGenForms'];
					for(var i=0;i<groups.length;i++){
						groupName=groups[i];
						if(setGroups){setGroup=setGroups[groupName];}
						if(setGroup){alias=setGroup.alias;}else{alias=groups[i].alias;}
						if(setGroup){tooltip=setGroup.tooltip;}else{tooltip=groups[i].tooltip;}
						if(!thisGroup.shelves[groupName]){
							var launchButton=new Button('toolBox','',alias,null,null,null,null,tooltip);
							accordian=new Accordian(groupName);
							shelf=thisGroup.accordian.createShelf({
								name:groupName,
								button:launchButton.html,
								content:accordian.container,
								alias:alias,
								isGroup:true
							});
							thisGroup.shelves[groupName]={
								launchButton:launchButton,
								accordian:accordian,
								shelf:shelf,
								shelves:{}
							}
						}else{
							accordian=thisGroup.shelves[groupName].accordian;
						}
						thisGroup=thisGroup.shelves[groupName];
					}
				}
				item.subaccordian=accordian;
				// accordian.container.append(item.shelf);
				shelf=accordian.receiveShelf(item);
			}
			if(setGroups){
				var keys=Object.keys(setGroups);
				keys=keys.sort(function(a,b){
					if(setGroups[a].place_in_order>setGroups[b].place_in_order){return 1;}
					if(setGroups[a].place_in_order<setGroups[b].place_in_order){return -1;}
					return 0;
				});
				var shelf,accordian;
				for(var i=0;i<keys.length;i++){
					shelf=self.accordian.container.find('.shelf.'+keys[i]);
					accordian=shelf.closest('.accordian');
					accordian.append(shelf);
					main.utilities.sortShelves(shelf.children('.shelfContentWrap').children('.accordian'));
				}
			}
			self.noneTest();
		};
		(this.preHTMLAdd=function(){
			main.utilities.addLayoutItems(self.name,self.alias,self,main.mainPath+'images/wrench.png');
		})();
	};
});