define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		// var Panel=require('./Panel');
		this.withItem='with_updater';
		this.alias='Data Updater';
		this.name='dataupdater';
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.addShelf=function(){
			self.launchButton=new Button('toolBox','',self.alias);
			self.shelf=main.layout.toolBox.createShelf({
				name:self.name,
				button:self.launchButton.html,
				content:self.html,
				context:self,
				alias:self.alias,
				dontAppendTo:true,
				groups:main.withs[self.withItem].toolbox_groups
			});
			main.layout.toolBox.shelfInAppTest(main.layout.toolBox.shelves[self.name]);
		};
		// this.layerSelectUpdate=function(){
			// self.layerSelect.html('');
			// self.layerSelect.append('<option value=""></option>');
			// if(main.invents){
				// var inv;
				// for(var key in main.invents.inventories){
					// if(main.invents.inventories[key].properties.upload_update){
						// self.layerSelect.append('<option value="'+key+'">'+main.invents.inventories[key].properties.alias+'</option>');
					// }
				// }
			// }
		// };
		(this.createHTML=function(){
			// self.layerSelect=$('<select class="exporterLayerSelect">');
			// self.layerSelectUpdate();
			self.paramsInput=$('<input type="hidden" name="params" class="formInput hiddenInput"/>');
			var table=$('<table class="uploaderTable">')
				.append($('<thead>')
					.append($('<tr>')
						.append($('<th colspan="2">')
							.append($('<div>').addClass('tdLiner center').text(self.alias))
						)
					)
				)
				.append($('<tbody>')
					// .append($('<tr>')
						// .append($('<td>')
							// .append($('<div>').addClass('tdLiner').html('Select Data'))
						// )
						// .append($('<td>')
							// .append($('<div>').addClass('tdLiner')
								// .append(self.layerSelect)
							// )
						// )
					// )
					.append($('<tr>')
						.append($('<td>')
							.append($('<div>').addClass('tdLiner').html('Upload File'))
						)
						.append($('<td>')
							.append($('<div>').addClass('tdLiner')
								.append('<input type="file" name="uploadFile" class="formInput fileInput" multiple/>')
								.append('<input type="hidden" name="action" value="uploadData" class="formInput hiddenInput"/>')
								.append(self.paramsInput)
							)
						)
					)
					.append($('<tr>')
						.append($('<td colspan="2">')
							.append($('<div>').addClass('tdLiner center formConcButtons').html('<input type="submit" class="button submit formConcButton" value="Upload"/>'))
						)
					)
				)
			self.html=$('<form enctype="multipart/form-data">').addClass('uploaderForm')
				.append(table);
			self.addShelf();
		})();
		(this.initEvents=function(){
			self.html.on('submit',function(e){
				e.preventDefault();
				if(!self.uploading){
					main.layout.loader.loaderOn();
					self.uploading=true;
					var params={
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]
					}
					self.paramsInput.val(JSON.stringify(params));
					var formData=new FormData($(this)[0]);
					var file=$(this).find('*[name="uploadFile"]').get(0);
					for(var i=0,len=file.files.length;i<len;i++){
						formData.append('upload_'+(i+1),file.files[i])
					}
					if(len>0){
						$.ajax({
							type:'POST',
							url:main.mainPath+'server/db.php',
							data:formData,
							async:false,
							cache:false,
							contentType:false,
							processData:false,
							success:function(response){
								self.uploading=false;
								if(response){
									var response=JSON.parse(response);
									if(response.status=='OK'){
										var files=response.files;
										if(response.layerName && main.dataTables[response.layerName]){
											if(main.dataTables[response.layerName].dataContentWrap.is(':visible')){main.dataTables[response.layerName].sideReset();
											}else{main.dataTables[response.layerName].needsRefresh=true;}
										}
										self.html.find('*[name="uploadFile"]').val('');
										alert('Data Uploaded Successfully.');
									}else{
										alert(response.status+': '+(response.message || ''));
									}
								}
								main.layout.loader.loaderOff();
							},
							error:function(response){
								alert('Error');
								console.log(response);
							}
						});
					}else{
						alert('Choose an excel file to upload.');
					}
				}
			});
		})();
	};
});