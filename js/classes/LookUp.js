define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Checkbox=require('./Checkbox');
		var Panel=require('./Panel');
		var Wait=require('./Wait');
		this.id=this.id || main.utilities.generateUniqueID();
		this.openOnUpdate=false;
		this.listLoaded=false;
		this.topLimit=5;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.table=main.data.lookUpTables[this.tableName];
		this.currentFiltered=[];
		if(this.field){
			// this.extraOptions=this.field.options;
			this.referencer=this.field.referencer;
			this.lookup_table_map=main.tables[this.referencer].properties.lookup_table_map[this.tableName];
			this.column=this.lookup_table_map[this.field.name];
		}else{
			this.column=this.lookup_table_field;
		}
		this.generateUniqueList=function(){
			self.uniqueListCheck=[];
			self.uniqueList=[];
			for(var i=0;i<self.keys.length;i++){
				if(self.table.items[self.keys[i]][self.column] && self.uniqueListCheck.indexOf(self.table.items[self.keys[i]][self.column])==-1){
					self.uniqueListCheck.push(self.table.items[self.keys[i]][self.column]);
					self.uniqueList.push(self.keys[i]);
				}
			}
		};
		this.setKeys=function(){
			self.keys=self.sort(Object.keys(self.table.items));
			self.generateUniqueList();
		};
		this.getValueFromAlias=function(alias){
			var val=null;
			for(var key in self.table.items){
				if(self.table.items[key][self.column]==alias){
					val=key;break;}
			}
			// if(!val){
				// for(var key in self.extraOptions){
					// if(self.extraOptions[key]==alias){
						// val=key;
						// break;}
				// }
			// }
			return val;
		};
		this.sort=function(keys){
			keys.sort(function(a,b){
				var itemA=self.table.items[a][self.column]
				var itemB=self.table.items[b][self.column];
				if(itemA){itemA=itemA.toLowerCase();}else{return -1;}
				if(itemB){itemB=itemB.toLowerCase();}else{return 1;}
				if(itemA>itemB){return 1;}
				if(itemA<itemB){return -1;}
				return 0;
			});
			return keys;
		};
		this.isItems=function(){
			if(self.table.items){return true;}else{return false;}
		};
		this.refreshUniqueList=function(){
			if(self.table.items){
				self.setKeys();
				self.updateList();
			}
		};
		(this.init=function(){
			if(self.table.items){self.setKeys();}else{
				// self.waitToOpen=new Wait(self.isItems,self.setKeys);
			}
		})();
		this.panelClose=function(){
			if(!self.field || !self.field.disabled && !self.forFilter){
				self.input.prop('disabled',false);
			}
		};
		(this.createHTML=function(){
			self.lookupFormButtons=$('<div class="lookupFormButtons cf">');
			if(main.luEditor && main.data.lookUpTables[self.tableName].properties.editable){
				self.editorButton=new Button('std','lookupTableEditorButton lookupFormButton','Edit Table');
				self.lookupFormButtons.append(self.editorButton.html);
				self.editorButton.html.on('click',function(){
					// main.utilities.openForm('lookup_editor','data',true);
					main.luEditor.newEditPanel(self.tableName);
					self.close();
				});
			}
			if(self.table.properties.with_add && !self.forFilter){
				self.lookupTableSearchAdd=new Button('std','lookupTableSearchAdd lookupFormButton','Add');
				self.lookupFormButtons.append(self.lookupTableSearchAdd.html);
				self.lookupTableSearchAdd.html.on('click',function(){
					if(self.lookupTableSearch.val()){
						var item=self.addNewItem(self.lookupTableSearch.val());
						if(item){
							self.itemSelected(item,true);
							self.input.change();
						}
					}
				});
			}
			self.html=$('<div>').addClass('lookUpPkg')
			if(self.table.properties.with_search){
				self.clearSearchAdd=new Button('std','clearSearchAdd lookupFormButton none','Clear Search');
				self.lookupFormButtons.append(self.clearSearchAdd.html);
				self.clearSearchAdd.html.on('click',function(){
					self.lookupTableSearch.val('');
					self.clearSearchAdd.html.addClass('none');
					self.updateList()
				});
				self.lookupTableSearchWrap=$('<div class="lookupTableSearchWrap lookUpListEle">');
				if(self.table.properties.with_add || self.table.properties.with_search){
					self.lookupTableSearch=$('<input type="text" class="lookupTableSearch" placeholder="Search/Add"/>');
					self.lookupTableSearchWrap.append(self.lookupTableSearch)
				}
				self.lookupTableSearchWrap.append(self.lookupFormButtons);
			}
			self.list=$('<ul class="lookUpListUL lookUpListEle">');
			var extraClasses='';
			self.topListLabel=$('<div class="topListLabel">').text('Most Common');
			self.topList=$('<ul class="lookUpListUL lookUpTops">');
			self.topListWrap=$('<div class="lookUpListUL lookUpTops lookUpListEle">')
				.append(self.topListLabel)
				.append(self.topList);
			// self.extrasList=$('<ul class="lookUpListUL lookupExtras lookUpListEle">');
			self.html
				.append(self.topListWrap);
				// .append(self.extrasList)
			if(self.forFilter){
				self.showFilteredValuesUL=$('<ul class="showFilteredValuesUL none">');
				self.showFilteredValues=$('<div class="luShowFilteredValues">')
				self.selectFilterItemsButton=new Button('std','selectFilterItemsButton','Select '+self.field.alias);
				self.selectFilterItemsButton.html.on('click',function(){
					self.open();
				});
				self.luFilterPkg=$('<div class="luFilterPkg" data-id="'+self.id+'">')
					.append(self.showFilteredValues.append(self.showFilteredValuesUL))
					.append(self.selectFilterItemsButton.html);
				extraClasses=' luForFilterPanel';
				
				self.clearSelectedButton=new Button('std','clearSelectedButton lookupFormButton none','Clear Selections');
				self.lookupFormButtons.append(self.clearSelectedButton.html);
				self.clearSelectedButton.html.on('click',function(){
					self.clearLUPkg();
				});
				self.doneSelectingButton=new Button('std','clearSelectedButton lookupFormButton','Done Selecting');
				self.lookupFormButtons.append(self.doneSelectingButton.html);
				self.doneSelectingButton.html.on('click',function(){
					self.close();
				});
			}
			self.html.append(self.list);
			self.panel=new Panel({
				content:self.html,
				topContent:self.lookupTableSearchWrap,
				title:self.table.properties.alias,
				//relativeTo:main.layout.mapContainerWrap,
				hideOnOutClick:true,
				hideOnOutClickExceptions:['.inputWithLookUpTable'],
				classes:'lookUpTablePanel'+extraClasses,
				onClose:self.panelClose,
				scrollToTop:true,
				withOutMin:true
			});
		})();
		this.clearLUPkg=function(val){
			self.currentFiltered=[];
			self.showFilteredValuesUL.find('.showFilteredValuesLI').addClass('none').remove();
			self.list.find('.lUFilterCheck').prop('checked',false);
			self.showFilteredValuesUL.addClass('none');
			self.clearSelectedButton.html.addClass('none');
		};
		this.addNewItem=function(val){
			//check for duplicate
			var lowerCaseVal=val.toLowerCase();
			for(var i=0;i<self.uniqueList.length;i++){
				if(self.table.items[self.uniqueList[i]][self.column].toLowerCase().indexOf(lowerCaseVal)>-1){return self.uniqueList[i];}
			}
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
						val:val,
						column:self.column,
						tableName:self.tableName
					},
					action:'addNewLookUpListItem'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							var record=response.results[0];
							var pid=record.pid;
							main.data.lookUpTables[self.tableName].items[pid]=record;
							self.setKeys();
							self.updateList();
							self.retrieveTops();
							self.itemSelected(pid,true);
							self.input.change();
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.updateTopList=function(tops){
			self.topList.html('');
			for(var i=0;i<tops.length;i++){
				if(self.table.items[tops[i]['val']]){
					self.topList.append(self.addListItem(self.table.items[tops[i]['val']][self.column],tops[i]['val']));
				}
			}
			if(self.topList.find('.lookUpItem').length){
				self.topListWrap.removeClass('none');
			}else{
				self.topListWrap.addClass('none');
			}
		};
		this.retrieveTops=function(){
			var invColumn;
			if(main.invents && main.invents.inventories[self.layerName] && main.invents.inventories[self.layerName].properties.lookup_table_map){
				var lUMap=main.invents.inventories[self.layerName].properties.lookup_table_map[self.tableName];
				for(var key in lUMap){
					if(self.column==lUMap[key]){invColumn=key;}
				}
			}
			if(!invColumn){return;}
			
			var where='';
			var props=main.invents.inventories[self.layerName].properties;
			if(main.invents.currentFilterLayer && main.inventories[main.invents.currentFilterLayer].is_depended_upon_by==self.layerName && main.invents.currentFilterPID && props.internal_field_map && props.internal_field_map.dependent_upon_field && !main.account.loggedInUser.dont_filter){
				where=props.internal_field_map.dependent_upon_field+"='"+main.invents.currentFilterPID+"'";
			}
			// var pids=null;
			// if(main.advFilter && main.advFilter.appliesTo['map'].apply && main.advFilter.currentLayer && main.advFilter.currentLayer.properties.layer_name==layerName){
				// if(main.advFilter.where){
					// if(where){where+=' AND ';}
					// where+='('+main.advFilter.where+')';
				// }
				// if(main.advFilter.geoFilter){pids=main.advFilter.geoFilter;}
			// }
			// if(pids && pids.length==0){
				// self.noResults();
				// return;
			// }
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						column:invColumn,
						limit:self.topLimit,
						where:where,
						layerName:self.layerName
					},
					action:'getLookUpTableMostCommon'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							if(response.results.length){
								self.updateTopList(response.results);
							}else{
								//todo hide top list
							}
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.sortShowFilteredValuesUL=function(val){
			var items=[];
			self.showFilteredValuesUL.find('.showFilteredValuesLI').each(function(){
				items.push([$(this).data('item'),$(this).html()]);
			});
			items=items.sort(function(a,b){
				itemA=a[1].toLowerCase();
				itemB=b[1].toLowerCase();
				if(itemA>itemB){return 1;}
				if(itemA<itemB){return -1;}
				return 0;
			});
			for(var i=0;i<items.length;i++){
				self.showFilteredValuesUL.append(self.showFilteredValuesUL.find('.showFilteredValuesLI_'+items[i][0]));
			}
		}
		this.filterCheckChanged=function(val,checked,alias){
			if(checked){
				if(self.currentFiltered.indexOf(val)==-1){
					self.currentFiltered.push(val);
					self.showFilteredValuesUL.append($('<li class="showFilteredValuesLI showFilteredValuesLI_'+val+'" data-item="'+val+'">'+alias+'</li>'));
				}
			}else{
				if(self.currentFiltered.indexOf(val)!=-1){
					self.currentFiltered.splice(self.currentFiltered.indexOf(val),1);
					self.showFilteredValuesUL.find('.showFilteredValuesLI_'+val).remove();
				}
			}
			if(self.showFilteredValuesUL.find('.showFilteredValuesLI').length>0){
				self.showFilteredValuesUL.removeClass('none');
				self.clearSelectedButton.html.removeClass('none');
			}else{
				self.showFilteredValuesUL.addClass('none');
				self.clearSelectedButton.html.addClass('none');
			}
			self.topList.find('.luItem_'+val+' .lUFilterCheck.checkbox').prop('checked',checked);
			self.list.find('.luItem_'+val+' .lUFilterCheck.checkbox').prop('checked',checked);
			self.sortShowFilteredValuesUL();
		};
		this.addListItem=function(alias,pid){
			var filterStuff='';
			var filterClass='';
			if(self.forFilter){
				filterClass='lookUpItemFilterLI';
				var checked=false;
				if(self.currentFiltered.indexOf(pid)>-1){checked=true;}
				var filterCheck=new Checkbox({
					value:pid,
					checked:checked,
					classes:'lUFilterCheck lUFilterCheck_'+pid[i],
					data:[['alias',alias]]
				});
				filterStuff=$('<div class="lookUpItemFilterCheckWrap lookUpItemEle">').append(filterCheck.html);
				filterCheck.checkbox.on('change',function(){
					self.filterCheckChanged($(this).val(),$(this).prop('checked'),$(this).data('alias'));
				});
			}
			var liContent=$('<div class="lookUpItemLabel lookUpItemEle">'+alias+'</div>');
			// liRemove=$('<div class="lookUpItemRemove lookUpItemEle" title="Remove Item">-</div>');
			return $('<li class="lookUpItem cf luItem_'+pid+filterClass+'" data-item="'+pid+'">')
				.append(filterStuff)
				.append(liContent);
				// .append(liRemove);
		};
		this.updateList=function(filterStr){
			self.list/* .add(self.extrasList) */.html('');
			var val,li,liContent,liRemove;
			if(filterStr){filterStr=filterStr.toLowerCase();}else{filterStr='';}
			for(var i=0;i<self.uniqueList.length;i++){
				val=self.table.items[self.uniqueList[i]][self.column];
				if(val && (!filterStr || val.toLowerCase().indexOf(filterStr)>-1)){
					self.list.append(self.addListItem(self.table.items[self.uniqueList[i]][self.column],self.uniqueList[i]));
				}
			}
			// if(self.extraOptions  && !self.lookup_table_map){
				// var keys=self.sort(Object.keys(self.extraOptions));
				// for(var i=0;i<keys.length;i++){
					// self.extrasList.append('<li class="lookUpItem" data-item="'+keys[i]+'">'+self.extraOptions[keys[i]].alias+'</li>');
				// }
			// }
			if(self.openOnUpdate){self.panel.open();self.openOnUpdate=false;}
			self.listLoaded=true;
			if(self.forFilter){
				for(var i=0;i<self.currentFiltered.length;i++){
					self.list.find('.lookUpItem.luItem_'+self.currentFiltered[i]+' .lUFilterCheck.checkbox').prop('checked',true);
				}
			}
		};
		/* this.lookUpTableLoadCheck=function(){
			if(main.data.lookUpTables[self.tableName].items){return true;}else{return false;}
		}; */
		this.lookUpTablesLoaded=function(params){
			self.table=main.data.lookUpTables[params.tableName];
			self.setKeys();
			self.updateList();
			self.retrieveTops();
		};
		this.open=function(){
			if(!self.forFilter){self.input.prop('disabled',true);}
			if(self.table.items){
				self.refreshUniqueList();
				self.retrieveTops();
				self.panel.open();
			}else{
				self.openOnUpdate=true;
				main.data.lookUpTables[self.tableName].waiting.push([self.lookUpTablesLoaded,{tableName:self.tableName}]);
				// self.tableWait=new Wait(self.lookUpTableLoadCheck,self.lookUpTablesLoaded);
			}
		};
		this.updateInput=function(itemVal){
			if(!self.table.items){self.table=main.data.lookUpTables[self.tableName]};
			self.itemSelected(itemVal);
		};
		this.close=function(){
			if(self.panel){self.panel.close();}
		};
		this.inputSet=function(input,item,column){
			var val=item[column];
			if(!val){val='';}
			if(input.get(0).nodeName.toLowerCase()=='select'){
				if(input.find('option[value="'+item.pid+'"]').length===0){
					input.html('<option value="'+item.pid+'">'+val+'</option>');
				}
				input.val(item.pid)/* .change() */;
			}else{
				input.val(val)/* .change() */;
			}
			return input;
		};
		this.itemSelected=function(itemVal,updateRelations,relations){
			if(itemVal){
				var item;
				if(self.table && self.table.items[itemVal]){
					item=self.table.items[itemVal];
					self.inputSet(self.input,item,self.column);
				}/* else if(self.extraOptions && self.extraOptions[itemVal]){
					self.input.html('<option value="'+itemVal+'">'+self.extraOptions[itemVal].alias+'</option>');
					self.input.val(itemVal)*//* .change() *//* ;
				} */
				var parent=self.input.closest('.dataUnit'),input;
				if(self.field && item && !updateRelations && self.field && self.field.disabled){
					for(var key in self.lookup_table_map){
						if(key!=self.field.name){
							if(item[self.lookup_table_map[key]]){
								input=parent.find('.formInput.'+key);
								if(self.table.items[input.val()]){
									if(self.table.items[input.val()].is_added_from_app && self.field.override_disabled_when_new){
										input.prop('disabled',false);
									}else{
										input.prop('disabled',true);
									}
								}
							}
						}
					}
				}
				if(item && updateRelations && self.field && !self.field.dont_update_lookup_fields){
					main.misc.blockUpdateData=true;
					var relatedField;
					for(var key in self.lookup_table_map){
						if(key!=self.field.name){
							input=parent.find('.formInput.'+key);
							// if(!input.val()){
								// if(item[self.lookup_table_map[key]]){
									val=item[self.lookup_table_map[key]];
									if(!val){val='';}
									if(parent.find('.formInput.'+key).length>0){
										self.inputSet(input,item,self.lookup_table_map[key]);
										if(relations && relations[key]){
											relations[key].current_val=item.pid;
											input.closest('.editPkgWrap').find('.dataTableCurrent').html(val);
										}else{
											input.change();
										}
									}
								// }else{
									//do something for no value
								// }
								relatedField=main.tables[self.layerName].fields[key];
								if(relatedField.disabled){
									if(item.is_added_from_app && relatedField.override_disabled_when_new){
										input.prop('disabled',false);
									}else{
										input.prop('disabled',true);
									}
								}
							// }
						}
					}
					main.misc.blockUpdateData=false;
				}
			}
			if(self.panel.opened){
				self.close();
			}
		};
		this.getItemHTML=function(itemVal){
			if(itemVal && self.table){
				if(self.table.items[itemVal]){
					return self.table.items[itemVal][self.column];
				}/* else if(self.extraOptions && self.extraOptions[itemVal]){
					return self.extraOptions[itemVal].alias;
				} */else{
					return '';
				}
			}
		};
		this.getLookUpTableHTML=function(callback,params){
			if(!self.table.items){
				if(callback){
					params.lookUpTable=self;
					main.data.lookUpTables[self.tableName].waiting.push([callback,params]);
				}
				return false;
			}else{
				return self.getItemHTML(params.value);
			}
		};
		(this.initEvents=function(){
			if(!self.forFilter){
				self.html.on('click','.lookUpItemRemove',function(e){
					if(confirm('Caution: This will permanently remove this item from this look up list along with all associated items. Are you sure you want to continue?')){
					}
				});
			}
			self.html.on('click','.lookUpItem',function(e){
				if($(e.target).closest('.lookUpItemRemove, .lookUpItemFilterCheckWrap .lUFilterCheck').length==0){
					if(!self.forFilter){
						self.itemSelected($(this).data('item'),true);
						self.input.change();
					}else{
						var chk=$(this).find('.lUFilterCheck');
						chk.prop('checked',!chk.prop('checked')).change();
						// self.filterCheckChanged(chk.val(),chk.prop('checked'),chk.data('alias'));
					}
				}
			});
			self.lookupTableSearch.on('keyup',function(){
				self.updateList($(this).val())
				if($(this).val().length>0){
					self.clearSearchAdd.html.removeClass('none');
				}else{
					self.clearSearchAdd.html.addClass('none');
				}
			});
		})();
	}
});
