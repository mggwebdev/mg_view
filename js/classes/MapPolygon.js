define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var ToolTip=require('./ToolTip');
		this.id=main.utilities.generateUniqueID();
		this.layerAdded=false;
		this.selecting=false;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		(this.create=function(){
			self.startButton=new Button('std','mapPolygonStart mapPolygonButton fullWidth','Draw',null,null,null,'Draw a polygon on the map. To finish, double click or click the first point. To freehand draw, hold down shift.');
			self.stopButton=new Button('std','mapPolygonStop mapPolygonButton fullWidth','Stop Drawing',null,null,null,'Stop drawing.');
			self.showSelectedButton=new Button('std','showMapPolygons mapPolygonButton fullWidth','Show',null,null,null,'Show');
			self.removeSelectedButton=new Button('std','removeMapPolygons mapPolygonButton fullWidth','Hide',null,null,null,'Hide');
			self.input=$('<input name="'+self.id+'" type="hidden">').addClass('mapPolygonInput formInput '+self.id+' '+self.key).data('name',self.id);
			self.container=$('<div>').addClass('mapPolygon')
				.append(self.input)
				.append($('<div>').addClass('mapPolygonButtons cf')
					.append(self.startButton.html)
					.append(self.stopButton.html)
					.append(self.showSelectedButton.html)
					.append(self.removeSelectedButton.html)
				);
			self.toolTip=new ToolTip({
				content:'To finish, double click or click the first point. To freehand draw, hold down shift.'
			});
			self.startButton.html.on('click',function(e){
				self.startSelecting();
			});
			self.stopButton.html.on('click',function(e){
				self.stopSelecting();
			});
			self.showSelectedButton.html.on('click',function(e){
				self.showFeatures();
			});
			self.removeSelectedButton.html.on('click',function(e){
				self.hideFeatures();
			});
			self.source = new ol.source.Vector({wrapX: false});
			if(self.origValue){
				self.source.addFeatures((new ol.format.GeoJSON()).readFeatures(self.origValue));
				// self.source.addFeatures((new ol.format.GeoJSON()).readFeatures(self.origValue.obj));
			}
			self.features=self.source.getFeatures();
			self.vector = new ol.layer.Vector({
				source: self.source,
				style: new ol.style.Style({
					fill: new ol.style.Fill({
						color: 'rgba(255, 255, 255, 0.2)'
					}),
					stroke: new ol.style.Stroke({
						color: '#ffcc33',
						width: 2
					}),
					image: new ol.style.Circle({
						radius: 7,
						fill: new ol.style.Fill({
							color: '#ffcc33'
						})
					})
				})
			});
			self.draw=new ol.interaction.Draw({
				source: self.source,
				type: 'Polygon'
			});
			self.geojson=new ol.format.GeoJSON();
			self.source.on('addfeature',function(e,f){
				self.features=self.source.getFeatures();
				self.updateInputVal();
			});
		})();
		this.updateInputVal=function(){
			self.featureGJson=self.geojson.writeFeatures(self.features);
			self.input.val(self.featureGJson).change();
		};
		this.startSelecting=function(){
			self.map.clearOtherPActions();
			self.startButton.html.addClass('none');
			self.stopButton.html.addClass('inlineBlock');
			self.selecting=true;
			self.map.selecting=true;
			self.toolTip.activate();
			self.addDraw();
		};
		this.stopSelecting=function(){
			self.stopButton.html.removeClass('inlineBlock');
			self.startButton.html.removeClass('none');
			self.selecting=false;
			self.map.selecting=false;
			self.toolTip.deActivate();
			self.removeDraw();
		};
		this.showFeatures=function(){
			if(!self.layerAdded){self.map.map.addLayer(self.vector);}
			self.layerAdded=true;
			self.showSelectedButton.html.addClass('none');
			self.removeSelectedButton.html.addClass('block');
		};
		this.hideFeatures=function(){
			if(self.layerAdded){self.map.map.removeLayer(self.vector);}
			self.layerAdded=false;
			self.showSelectedButton.html.removeClass('none');
			self.removeSelectedButton.html.removeClass('block');
		};
		this.addDraw=function(){
			if(!self.layerAdded){self.map.map.addLayer(self.vector);}
			self.layerAdded=true;
			if(!self.drawAdded){self.map.map.addInteraction(self.draw);}
			self.drawAdded=true;
		};
		this.removeDraw=function(){
			if(self.layerAdded){self.map.map.removeLayer(self.vector);}
			self.layerAdded=false;
			if(self.drawAdded){self.map.map.removeInteraction(self.draw);}
			self.drawAdded=false;
		};
		
		
		
		
		
		
		
		
		
		
		
		
		/* this.default_val=default_val;
		this.map=map;
		this.pid=pid;
		this.selecting=false;
		this.iconFeatures={};
		this.values={};
		this.style=new Style2();
		this.create=function(){
			var origVal=this.default_val;
			value='';
			if(origVal){
				if(typeof origVal=='object'){
					var pointData=main.utilities.getGeometryData(value);
					if(pointData){
						for(var i=0;i<pointData.coords.length;i++){
							self.addFeature(pointData.coords[i],false,false);
						}
						value=pointData.valStr;
					}
				}else if(typeof origVal=="string"){
					var valSplit=origVal.split(','),coordSplit;
					for(var i=0;i<valSplit.length;i++){
						coordSplit=valSplit[i].split(' ');
						self.addFeature(ol.proj.transform([Number(coordSplit[0]),Number(coordSplit[1])], 'EPSG:4326', 'EPSG:3857'),false,false);
					}
					value=origVal;
				}
			}
			this.startButton=new Button('std','mapPolygonStart mapPolygonButton fullWidth','Select From Map',null,null,null,'Select a point from the map. To delete a point, hold down shift and click the corresponding icon.');
			this.stopButton=new Button('std','mapPolygonStop mapPolygonButton fullWidth','Stop Selecting',null,null,null,'Stop Selecting');
			// this.editButton=new Button('std','mapPolygonStop mapPolygonButton fullWidth','Move Selecting',null,null,null,'Stop Selecting');
			this.showSelectedButton=new Button('std','showmapPolygons mapPolygonButton fullWidth','Show Selected',null,null,null,'Show Selected');
			this.removeSelectedButton=new Button('std','removemapPolygons mapPolygonButton fullWidth','Hide',null,null,null,'Hide');
			this.input=$('<input name="'+this.id+'" type="hidden" value="'+value+'">').addClass('mapPolygonInput formInput '+this.id).data('name',this.id);
			this.container=$('<div>').addClass('mapPolygon')
				.append(this.input)
				.append($('<div>').addClass('mapPolygonButtons cf')
					.append(this.startButton.html)
					.append(this.stopButton.html)
					.append(this.showSelectedButton.html)
					.append(this.removeSelectedButton.html)
				);
			this.toolTip=new ToolTip({
				content:'Select a point from the map.'
			});
		};
		this.startSelecting=function(){
			self.map.clearOtherPActions();
			self.startButton.html.addClass('none');
			self.stopButton.html.addClass('inlineBlock');
			self.selecting=true;
			self.map.selecting=true;
			self.toolTip.activate();
			self.addDraw();
		};
		this.stopSelecting=function(){
			self.stopButton.html.removeClass('inlineBlock');
			self.startButton.html.removeClass('none');
			self.selecting=false;
			self.map.selecting=false;
			self.toolTip.deActivate();
		};
		this.addDraw=function(deleteToo){
			this.drawSource=new ol.source.Vector();
			this.drawVector=new ol.layer.Vector({
				source:this.drawSource,
				style:this.style.createNewStyle()
			});
			this.draw=new ol.interaction.Draw({
				source:this.drawSource,
				type:type
			});
			this.draw.on('drawend',function(e){
				e.feature.set(main.constants.featureTypeCode,'mapPolygon');
				// self.deleteFeaturesButton.removeClass('none');
				// self.deleteAllButton.removeClass('none');
				// self.startModifyingButton.removeClass('none');
			});
			this.map.map.addInteraction(this.draw);
		};
		this.removeAllFeatures=function(deleteToo){
			for(var key in self.values){
				self.layerSource.removeFeature(self.values[key].feature);
				self.values[key].iconOnMap=false;
				if(deleteToo){delete self.values[key];}
			}
			self.removeSelectedButton.html.removeClass('inlineBlock');
			self.showSelectedButton.html.removeClass('none');
		};
		this.removeFeature=function(key,deleteToo){
			self.layerSource.removeFeature(self.values[key].feature);
			if(deleteToo){delete self.values[key];}
		};
		this.updateInputVal=function(){
			var value='',coods,latlng;
			for(var key in self.values){
				coods=self.values[key].feature.getGeometry().getCoordinates();
				latlng=ol.proj.transform(coods,'EPSG:3857','EPSG:4326');
				value+=latlng[0]+' '+latlng[1]+',';
			}
			value=value.replace(/,([^,]*)$/,'');
			self.input.val(value).change();
		};
		this.showSelected=function(){
			if(Object.keys(self.values).length>0){
				for(var key in self.values){
					if(!self.values[key].iconOnMap){
						self.layerSource.addFeature(self.values[key].feature);
						self.values[key].iconOnMap=true;
					}
				}
			}
		};
		this.doneDragging=function(){
			self.updateInputVal();
		};
		this.initEvents=function(){
			this.startButton.html.on('click',function(e){
				self.toolTip.activate();
				self.startSelecting();
			});
			this.stopButton.html.on('click',function(e){
				self.stopSelecting();
			});
			this.showSelectedButton.html.on('click',function(e){
				self.showSelected();
				self.removeSelectedButton.html.addClass('inlineBlock');
				self.showSelectedButton.html.addClass('none');
			});
			this.removeSelectedButton.html.on('click',function(e){
				self.removeAllFeatures();
				self.removeSelectedButton.html.removeClass('inlineBlock');
				self.showSelectedButton.html.removeClass('none');
			});
		};
		this.create();
		this.initEvents(); */
		self.map.mapPolygons[self.id]=self;
	};
});