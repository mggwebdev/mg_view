define(function(require){
	return function pg(){
		var self=this;
		var Button=require('./Button');
		this.defaultHeaderHeight=54;
		this.users_settings=main.personal_settings.app_settings;
		this.setHeader=function(){
			var classes='header';
			this.headerLeft=this.createHeaderLeft();
			this.headerRight=this.createHeaderRight();
			if(this.users_settings.title_image){this.headImg='<img src="'+main.localImgs+this.users_settings.title_image+'" class="headerImg"/>';}else{this.headImg='';}
			if(this.users_settings.title_image_2){this.headImg2='<img src="'+main.localImgs+this.users_settings.title_image_2+'" class="headerImg"/>';}else{this.headImg2='';}
			this.title=this.users_settings.title || '';
			// this.headerCenter=$('<div>').addClass('headerCenter headerSection')
				// .append($('<div>').addClass('headerTitle').html('<div class="headerContentWrap">'/* +'<a href="#" class="discreteLink">' */+'<div class="headerContEle icon">'+this.headImg+'</div><div class="headerContEle text">'+this.title+'</div>'/* +'</a>' */+'</div>'));
			this.headerCenter=$('<div>').addClass('headerCenter headerSection');
			headerTitle=$('<div>').addClass('headerTitle');
			headerWrap=$('<div>').addClass('headerContentWrap');
			headerImg=$('<div>').addClass('headerContEle icon').html(this.headImg);
			if(this.headImg2){
				headerImg2=$('<div>').addClass('headerContEle icon').html(this.headImg2);
			}else{
				headerImg2='';
			}
			headerText=$('<div>').addClass('headerContEle text').html(this.title);
			if (main.withs.with_welcome.value){
				headerText.add(headerImg).on('click',function(){
				main.welcome.panel.toggleMin();});
			}
			headerWrap.append(headerImg);	
			headerWrap.append(headerText);
			headerWrap.append(headerImg2);
			headerTitle.append(headerWrap);
			this.headerCenter.append(headerTitle);
			this.mobileHeaderInfo=this.headerCenter.clone().addClass('mobileHeaderInfo');
			this.mobileMenuButton=new Button('roundedSquare','headerButton mobileMenuButton','Menu',null,null,main.mainPath+'images/menu.png','Menu');
			this.mobileMenuButton.html.on('click',function(){
				self.mobileMenu.slideToggle(200);
				main.layout.closeAllSidePanels();
				main.map.adjustMapSize();
			});
			this.mobileMenu=$('<div class="mobileMenu">');
			$('html').on('click',function(e){
				if(!$(e.target).closest('.mobileMenu,.mobileMenuButton').length){
					self.mobileMenu.slideUp(200);
				}
			});
			this.mobileMenuWrap=$('<div class="mobileMenuWrap">').append(this.mobileMenu);
			this.mobileHeader=$('<div class="mobileHeader cf">')
				.append(this.mobileHeaderInfo)
				.append(this.mobileMenuButton.html)
				.append(this.mobileMenuWrap);
			this.nonmobileHeader=$('<div class="nonMobileHeader">')
				.append(this.headerLeft)
				.append(this.headerCenter)
				.append(this.headerRight);
			this.container=$('<div>').addClass(classes)/* .height(this.defaultHeaderHeight) */
				.append(this.mobileHeader)
				.append(this.nonmobileHeader);
			this.container.on('click',function(){
				self.container.css('z-index',++main.misc.maxZIndex);
			});
		};
		this.createMobileMenuItem=function(classes,content,data,imgPath,title){
			return new Button('mobileMenu',classes,content,data,null,imgPath,title);
		};
		this.createHeaderRight=function(){
			return $('<div>').addClass('headerRight headerSection');
		};
		this.createHeaderLeft=function(){
			return $('<div>').addClass('headerLeft headerSection')
		};
		this.createHeaderButton=function(classes,content,data,imgPath,title){
			return new Button('roundedSquare','headerButton '+classes,content,data,null,imgPath,title);
		};
		this.setHeader();
	};
});