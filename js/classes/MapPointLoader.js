define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		this.cb=null;
		this.cbParams=null;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		if(main.data.map_layers[self.layerName]){
			self.maxFeatures=self.maxMapLayerFeatures;
		}
		self.mapPoints={};
		this.mapPoints.stopLoading=function(){
			if(self.mapPoints.retrieveMapPointsStartAjax){self.mapPoints.retrieveMapPointsStartAjax.abort();}
			if(self.mapPoints.retrieveMapLayersAjax){self.mapPoints.retrieveMapLayersAjax.abort();}
			self.mapPoints.loading=false;
		};
		this.mapPoints.resetMapPointParams=function(){
			self.mapPoints.its=0;
			self.mapPoints.thisIt=0;
			self.mapPoints.resultsPerIt=self.maxFeatures;
			// if(main.browser.isMobile){self.mapPoints.resultsPerIt=300;}
			self.mapPoints.pids=[];
			self.mapPoints.currentSlice=[];
			self.mapPoints.loading=false;
			self.mapPoints.doneLoading=false;
			self.mapPoints.active=false;
			self.mapPoints.retrieveMapPointsStartAjax=null;
			self.mapPoints.retrieveMapLayersAjax=null;
		};
		this.mapPoints.retrieveMapPoints=function(layerName,mustTurnOn){
			if(self.mapPoints.currentSlice.length>0){
				mustTurnOn
				self.mapPoints.retrieveMapLayersAjax=$.ajax({
					type:'POST',
					url:main.mainPath+'server/db.php',
					data:{
						params:{
							funcToCall:'retrieveLayers',
							args:{
								folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
								table:layerName,
								mustTurnOn:mustTurnOn,
								// pidst:JSON.stringify(self.currentSlice),
								pids:self.mapPoints.currentSlice.join(',')
							}
						},
						action:'callFunction'
					},
					success:function(response){
						// main.layout.loader.loaderOff();
						if(response){
							response=JSON.parse(response);
							if(response.status=="OK"){
								var layerName=response.args.table;
								var map_layer=main.map_layers[layerName];
								if(map_layer){
									var features=response.results[layerName].features;
									var layerObj=JSON.parse(response.results[layerName].geojson);
									if(!map_layer.secondaryLoad.features){main.map_layers[layerName].secondaryLoad.features={};}
									if(!main.map_layers[layerName].features){main.map_layers[layerName].features={};}
									//probelm below, main.map_layers[layerName].features undefined
									for(var key in features){
									// layerObj.features.forEach(function(f, idx) {
                                        // var key = f.properties.pid;
										if(!main.map_layers[layerName].features[key]){
											main.map_layers[layerName].features[key]={
												// properties: f.properties
												properties:features[key]
											}
										}
									// });
									}
									var gjFeatures=layerObj.features;
									var remainingFeaturesToAdd=self.maxFeatures-main.map_layers[layerName].secondaryLoad.layerObj.features.length;
									if(remainingFeaturesToAdd>0){
										var featuresToAdd=gjFeatures.slice(0,remainingFeaturesToAdd);
										Array.prototype.push.apply(main.map_layers[layerName].secondaryLoad.layerObj.features,featuresToAdd);
										if(map_layer.layer){main.map.updateLayerSource(layerName,true);}
										if(remainingFeaturesToAdd<=gjFeatures.length){self.mapPoints.doneLoading=true;}
									}
									self.mapPoints.thisIt++;
									var mustTurnOn=response.results.mustTurnOn?true:null;
									if(mustTurnOn && map_layer.layer && !map_layer.layer.olLayer.getVisible()){map_layer.layer.olLayer.setVisible(true)}
									self.mapPoints.nextLayerRetIt(layerName);
								}
							}else{
								alert('Error');
								console.log(response);
							}
						}
					}
				});
			}
		};
		this.mapPoints.nextLayerRetIt=function(layerName,mustTurnOn){
			if(self.mapPoints.thisIt<self.mapPoints.its && !self.mapPoints.doneLoading){
				var min=self.mapPoints.thisIt*self.mapPoints.resultsPerIt;
				var max=min+self.mapPoints.resultsPerIt;
				self.mapPoints.currentSlice=self.mapPoints.pids.slice(min,max);
				self.mapPoints.retrieveMapPoints(layerName,mustTurnOn);
			}else{
				main.layout.loader.loaderOff();
				self.mapPoints.loading=false;
				if(main.mapItems.hasActive && main.mapItems.hasActiveLayer==layerName){
					var pid=main.mapItems.hasActive;
					if(main.map_layers[layerName].layer){
						var features=main.map_layers[layerName].layer.olLayer.getSource().getFeatures();
						for(var i=0;i<features.length;i++){
							if(features[i].get('pid')==pid){
								features[i].set('isActive',true);
								if(main.invents && main.invents.inventories[layerName] && main.invents.inventories[layerName].hasPopUp && pid==main.invents.inventories[layerName].hasPopUp){
									var invent=main.invents.inventories[layerName];
									var popUpZ=null;
									if(invent.popUpZ){popUpZ=invent.popUpZ;}
									invent.mapInteractions.showPopup(pid,features[i],invent.hasPopUpOtherFeatures,null,true,popUpZ,invent.hasPopUpCurrentIndex);
								}
							}
						}
					}
				}
				if(main.labels && main.tables[layerName].map_layer.layer && main.tables[layerName].map_layer.layer.labels.withLabels){
					main.labels.refresh();
				}
				if(main.filter_legend && main.filter_legend.active && main.filter_legend.layerName==layerName){main.loader.loadMoreMapPoints(layerName,null,true);}
				if(self.cb){self.cb(self.cbParams);}
			}
		};
		this.getCurrentPids=function(layerName){
			var features=main.tables[layerName].map_layer.layer.olLayer.getSource().getFeatures();
			var pids='';
			for(var i=0;i<features.length;i++){
				pids+=features[i].get('pid')+',';
			}
			return pids.replace(/,([^,]*)$/,'$1');
		};
		this.mapPoints.initMapPointsStartResults=function(layerName,count,pids,mustTurnOn){
			main.map_layers[layerName].hasLoaded=true;
			main.map_layers[layerName].secondaryLoad={};
			main.map_layers[layerName].secondaryLoad.layerObj={
				type:'FeatureCollection',
				features:[]
			}
			if(pids.length>0){
				self.mapPoints.pids=pids;
				self.mapPoints.its=Math.ceil(self.mapPoints.pids.length/self.mapPoints.resultsPerIt);
				var mustTurnOn=mustTurnOn?true:null;
				self.mapPoints.nextLayerRetIt(layerName,mustTurnOn);
			}else{
				var layerObj=JSON.parse('{"type":"FeatureCollection","features":[]}');
				main.map_layers[layerName].secondaryLoad.layerObj=layerObj;
				var map_layer=main.map_layers[layerName];
				if(map_layer.layer){main.map.updateLayerSource(layerName,true);}
				main.map_layers[layerName].secondaryLoad.features={};
				self.mapPoints.loading=false;
				if(main.filter_legend && main.filter_legend.active && main.filter_legend.layerName==layerName){main.loader.loadMoreMapPoints(layerName,null,true);}
				main.layout.loader.loaderOff();
			}
		}
		this.mapPoints.retrieveMapPointsStart=function(layerName,where,extent,geom_field,mustTurnOn,pids,cb,cbParams){
			// main.layout.loader.loaderOn();
			if(cb){self.cb=cb;}
			if(cbParams){self.cbParams=cbParams;}
			if(!geom_field){return;}
			self.mapPoints.resetMapPointParams();
			self.mapPoints.active=true;
			self.mapPoints.loading=true;
			pids=pids || null;
			if(pids){
				if(pids.length==0){
					self.mapPoints.initMapPointsStartResults(layerName,0,pids);
					return;
				}
				pids=pids.join(',');
			}
			// var currentPIDs=self.getCurrentPids();
			self.mapPoints.retrieveMapPointsStartAjax=$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						funcToCall:'retrieveMapPointsStart',
						args:{
							folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
							layerName:layerName,
							extent:extent,
							mustTurnOn:mustTurnOn,
							geom_field:geom_field,
							pids:pids,
							// currentPIDs:currentPIDs
							// maxFeatures:main.maxFeatures,
							where:where
						}
					},
					action:'callFunction'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							self.mapPoints.initMapPointsStartResults(response.results.params.layerName,response.results.total.count,response.results.pids,response.results.mustTurnOn);
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
	};
});
