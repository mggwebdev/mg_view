define(function(require){
	return function pg(map){
		var self=this;
		var Button=require('./Button');
		var EditableList=require('./EditableList');
		var Input=require('./Input');
		var Switch=require('./Switch');
		var ViewMore=require('./ViewMore');
		this.canopyDefaultFill= 'rgba(0, 0, 0, 0)';
		this.canopyDefaultOutline= 'rgba(0, 0, 0, 1)';
		this.map=map;
		this.keys=main.utilities.cloneObj(main.data.std_settings.layers.sortedKeys);
		this.formID='layersForm';
		this.withItem='with_layers';
		this.setParams=function(){
			this.params=main.utilities.cloneObj(main.data.std_settings.layers);
		};
		this.maxZindex=0;
		this.alias='Layers';
		this.createHTML=function(){
			this.addShelf();
			main.utilities.initSpectrumPickers(this.params,this.layersInputChange,true,true);
		};
		this.addShelf=function(){
			this.launchButton=new Button('toolBox','',this.alias);
			this.layersForm=this.createLayersForm();
			this.shelf=main.layout.toolBox.createShelf({
				name:'layers',
				button:this.launchButton.html,
				content:this.layersForm,
				alias:this.alias,
				groups:main.withs[self.withItem].toolbox_groups
			});
		};
		this.addLayers=function(layers){
			// self.currentLoadedLayer=0;
			self.loadedLayers=layers.length;
			for(var i=0;i<layers.length;i++){
				self.map.vectorLayers[layers[i].id]=layers[i];
				self.map.currentVectorLayerID=layers[i].id;
				// self.map.map.addLayer(layers[i].layer);
				main.labels.updateHTMLElements(layers[i]);
				self.updateHTMLElements(layers[i]);
				/* layers[i].layer.once('postcompose',function(e){
					// self.currentLoadedLayer++;
					// if(self.currentLoadedLayer==self.loadedLayers){
						// main.server.addLayers(layers);
					// }
				}); */
			}
		};
		this.updateHTMLElements=function(layer){
			// this.layerList.addItem(layer.id,layer.alias,{'layer':layer});
			if(/* this.layerFormSwitch &&  */self.mainLayersLayerSelect){
				// if(layer.olLayer.getVisible()){this.layerFormSwitch.turnSwitchOn(false);
				// }else{this.layerFormSwitch.turnSwitchOff(false);}
				self.mainLayersLayerSelect.append('<option value="'+layer.id+'">'+layer.alias+'</option>');
			}
		};
		this.revertToOriginalStyle=function(layer){
			if(layer){layer.layer.setDefaultStyle();}
		};
		this.turnOffFilterLegend=function(){
			if(main.filter_legend && main.filter_legend.active){
				if(!main.filter_legend.disabled && main.filter_legend.layerName==self.primaryLayerName){main.filter_legend.disable();}
				if(main.filter_legend.otherLayerItems && main.filter_legend.otherLayerItems[self.primaryLayerName]){main.filter_legend.otherLayerItems[self.primaryLayerName].checkbox.checkbox.prop('checked',true);}
			}
		};
		this.createLayersForm=function(){
			var layersForm=$('<form>').attr('id',this.formID);
			// this.layerList=new EditableList(null,null,false,true,true);
			this.formBotButtons=$('<div class="formBotButtons">');
			this.revertToOriginal=new Button('std','revertToOriginal','Original',null,null,null,'Revert to Original Styling');
			this.revertToOriginal.html.on('click',function(){
				self.revertToOriginalStyle(self.primaryLayer);
			});
			// this.updateButton=new Button('std','updateLayersButton','Update',null,null,null,'Update Layer on Map');
			// this.updateButton.html.on('click',function(){
				// self.updateLayers();
			// });
			this.formBotButtons
				.append(this.revertToOriginal.html);
				// .append(this.updateButton.html);
			this.mainLayersLayerSelect=$('<select class="mainLayersLayerSelect">')
				.append('<option value="">None</option>');
			this.mainLayersLayerSelect.on('change',function(){
				if(self.primaryLayer){
					self.turnOffLayer();
				}
				self.primaryLayerName=$(this).val();
				if($(this).val()){
					
					// if(self.mainLabelFieldSelect.val()){
						self.turnOffFilterLegend();
						main.tables[self.primaryLayerName].map_layer.layer.setVisible(true);
						main.utilities.setTopLayer(main.tables[self.primaryLayerName].map_layer.layer.olLayer);
						self.primaryLayer=main.tables[self.primaryLayerName].map_layer;
						main.utilities.setTopLayer(main.tables[self.primaryLayerName].map_layer.layer.olLayer);
						
					// }
					if(main.canopy && main.canopy_layers[self.primaryLayerName]){
					
						$("#layersForm").find(".fill_color").spectrum("set",self.canopyDefaultFill).change();
						$("#layersForm").find(".stroke_color").spectrum("set",self.canopyDefaultOutline).change();
						$(".tbFormParamRow_shape").addClass("none");
					}else{
						
						$("#layersForm").find(".fill_color").spectrum("set",self.params.fill_color.default_val).change();
						$("#layersForm").find(".stroke_color").spectrum("set",self.params.stroke_color.default_val).change();
						$(".tbFormParamRow_shape").removeClass("none");
					}
				}
				self.layersInputChange();
			});
			self.viewMore=new ViewMore({
				prev:'Change the appearance of the selected layer',
				content:'<ul><li>Step 1: Select a layer to modify.</li><li>Step 2: Change the parameters.</li></ul>'
			});
			var table=$('<table>').addClass('shelfForm')
				.append($('<tr>')
					.append($('<td colspan="2">').append($('<div>').addClass('tdLiner toolBoxDesc').html(self.viewMore.html)))
				).append($('<tr>')
					.append($('<td>').append($('<div>').addClass('tdLiner').text('Layer')))
					.append($('<td>').append($('<div>').addClass('tdLiner').html(self.mainLayersLayerSelect)))
					// .append($('<td>').append($('<div>').addClass('tdLiner checkWrap').append(this.layerList.container)))
				).append($('<tr>')
					.append($('<th colspan="2">')
						.append($('<div>').addClass('tdLiner center').text('Layer Properties'))
					)
				);
			var toolFormMakeItems=main.utilities.toolFormMake(this.keys,this.params,this.formID,table);
			this.advancedViewMore=toolFormMakeItems.advancedViewMore;
			this.advancedOptions=toolFormMakeItems.advancedOptions;
			// this.layerFormSwitch=new Switch(this.turnOnLayer,this.turnOffLayer);
			// this.updateFormButton=$('<div>').addClass('button updateForm').text('Update Layers');
			table.append($('<tr>')
					.append($('<td colspan="999">')
						.append($('<div>').addClass('tdLiner submitRowEle stdMrgTop cf')
							// .append(this.updateFormButton)
							.append(this.formBotButtons)
						)
					)/* .append($('<td>')
						.append($('<div>').addClass('tdLiner submitRowEle stdMrgTop cf')
							.append($('<div>').addClass('submitRowSwitchWrap').append(this.layerFormSwitch.container))
						)
					) */
				)/* .append($('<tr>')
					.append($('<td clospan="99999">')
						.append($('<div>').addClass('tdLiner stdMrgTop cf')
							.append(this.formBotButtons)
						)
					)
				) */;
			return layersForm.append(table);
		};
		this.turnOnLayer=function(){
			// var layerIDs=[];
			// self.layerList.container.find('.eLCheck').each(function(){if($(this).prop('checked')) layerIDs.push($(this).val());});
			// for(var i=0;i<layerIDs.length;i++) self.map.vectorLayers[layerIDs[i]].layer.setVisible(true);
			self.primaryLayer.layer.olLayer.setVisible(true);
		};
		this.turnOffLayer=function(){
			// var layerIDs=[];
			// self.layerList.container.find('.eLCheck').each(function(){if($(this).prop('checked')) layerIDs.push($(this).val());});
			// for(var i=0;i<layerIDs.length;i++) self.map.vectorLayers[layerIDs[i]].layer.setVisible(false);
			self.primaryLayer.layer.olLayer.setVisible(false);
		};
		this.removeLayers=function(){
			// var layerIDs=[];
			// this.layerList.container.find('.eLCheck').each(function(){
				// if($(this).prop('checked')) layerIDs.push($(this).val());
			// });
			// for(var i=0;i<layerIDs.length;i++) this.map.vectorLayers[layerIDs[i]].layer.setVisible(false);
		};
		this.updateLayers=function(){
			main.utilities.setCurrentVals(self.layersForm,self.params);
			// self.params.circle_fill=self.params.fill_color;
			// var layerIDs=[],layer;
			// self.layerList.container.find('.eLCheck').each(function(){if($(this).prop('checked')) layerIDs.push($(this).val());});
			/* for(var i=0;i<layerIDs.length;i++){
				layer=self.map.vectorLayers[layerIDs[i]];
				layer.style.updateLayers(self.params);
				if(self.layerFormSwitch.onState && !layer.olLayer.getVisible()) layer.olLayer.setVisible(true);
				if(!self.layerFormSwitch.onState && layer.olLayer.getVisible()) layer.olLayer.setVisible(false);
			} */
			if(self.primaryLayer){self.primaryLayer.layer.style.updateLayers(self.params);}
		};
		/* this.updateForm=function(){
			var layerIDs=[];
			// this.layerList.container.find('.eLCheck').each(function(){if($(this).prop('checked')) layerIDs.push($(this).val());});
			if(layerIDs.length===1){
				var layerID=layerIDs[0], layerParams=self.params, layer=self.map.vectorLayers[layerID];
				layerParams.fill_color.input.spectrum('set',layer.params.style.fill_color.current_val);
				layerParams.stroke_color.input.spectrum('set',layer.params.style.stroke_color.current_val);
				layerParams.stroke_width.input.val(layer.params.style.stroke_width.current_val);
				if(layer.olLayer.getVisible()){self.layerFormSwitch.turnSwitchOn(true);
				}else{self.layerFormSwitch.turnSwitchOff(true);}
			}
		}; */
		this.refreshLayers=function(vectorLayers){
			for(var i=0;i<vectorLayers.length;i++) vectorLayers[i].style.setStyleFunction();
		};
		this.layersInputChange=function(){
			self.turnOffFilterLegend();
			self.updateLayers();
		};
		this.initEvents=function(){
			/* this.updateFormButton.on('click',function(e){
				self.updateLayers();
			}); */
			/* this.layerList.container.on('click','.eLCheck',function(e){
				self.layerChange=true;
				if(self.layerList.container.find('.eLCheck:checked').length===1){
					// self.updateForm(self);
				}
			}); */
			this.layersForm.on('change','.formInput',function(e){
				e.preventDefault();
				// if(!self.layerChange){
					
					self.layersInputChange();

					self.layerChange=false;
				// }
			});
		}
		this.setParams();
		if(main.withs.with_layers.value){
			this.createHTML();
			main.utilities.setCurrentVals(this.layersForm,self.params);
			this.initEvents();
		}
	};
});