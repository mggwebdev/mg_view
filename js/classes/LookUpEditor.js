define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Panel=require('./Panel');
		this.withItem='with_lookup_editor';
		this.alias='Look Up Table Editor';
		this.name='lookup_editor';
		this.defaultSortOrder='ASC';
		this.isReset=false;
		this.itemsArray=[];
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.addShelf=function(){
			self.launchButton=new Button('toolBox','',self.alias);
			self.shelf=main.layout.toolBox.createShelf({
				name:self.name,
				button:self.launchButton.html,
				content:self.html,
				context:self,
				alias:self.alias,
				dontAppendTo:true,
				groups:main.withs[self.withItem].toolbox_groups
			});
			main.layout.toolBox.shelfInAppTest(main.layout.toolBox.shelves[self.name]);
		};
		this.createListTR=function(item,luListMap){
			var checked='';
			if(self.itemsArray.indexOf(item.pid.toString())>-1){checked=' checked="checked"';}
			var tr=$('<tr class="luEditListTR luEditListTRPID_'+item.pid+'" data-item="'+item.pid+'">').append($('<td>').append($('<div class="tdLiner luListEditEditCell">').append('<input type="checkbox" class="luListEditCB"'+checked+'/>')));
			for(var key in luListMap){
				tr.append($('<td class="luEditUnitTD" data-itemunit="'+luListMap[key]+'" data-itemunitkey="'+key+'">').append($('<div class="tdLiner luListEditUnit">').append($('<div class="luEditCurrent">').html(item[luListMap[key]]))));
			}
			return tr;
		};
		(this.createHTML=function(){
			self.listUL=$('<ul class="luEUL">');
			var luLists=main.data.lookUpTables;
			if(luLists && Object.keys(luLists).length>0){
				var luKeys=Object.keys(luLists).sort(function(a,b){
					var itemA=luLists[a].properties.alias.toLowerCase();
					var itemB=luLists[b].properties.alias.toLowerCase();
					if(itemA>itemB){return 1;}
					if(itemA<itemB){return -1;}
					return 0;
				});
				for(var i=0;i<luKeys.length;i++){
					if(luLists[luKeys[i]].properties.editable){
						self.listUL.append('<li class="luELI" data-lulist="'+luKeys[i]+'">'+luLists[luKeys[i]].properties.alias+'</li>');
					}
				}
			}
			self.html=$('<div class="lookUpEditorPanelWrap">')
				.append(self.listUL);
			self.addShelf();
		})();
		this.panelClose=function(){
			self.panel.remove(true);
			delete self.panel;
		};
		this.addNewItem=function(){
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
						listName:self.listName,
						layerName:self.layerName
					},
					action:'addBlankLUItem'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							var pid=response.result.pid;
							main.data.lookUpTables[response.params.listName].items[pid]=response.result;
							var luListMap=main.tables[response.params.layerName].properties.lookup_table_map[response.params.listName];
							var items=main.data.lookUpTables[response.params.listName].items;
							tr=self.createListTR(response.result,luListMap);
							self.editorTbody.append(tr);
							self.panel.body.scrollTop(self.panel.panelContent.outerHeight(true)-self.panel.body.height());
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
			
			
			
			// var keys=self.getSortedKeys(invent,field,listname,sortOrder),tr,checked;
			// var luListMap=main.invents.inventories[invent].properties.lookup_table_map[listname];
			// var items=main.data.lookUpTables[listname].items;
			// for(var t=0;t<keys.length;t++){
				// key0=keys[t].pid;
				// tr=self.createListTR(items[key0],luListMap);
		};
		this.createEditorHTML=function(list){
			self.itemsArray=[];
			self.topContent=$('<div class="luListEditorTopContent">');
			self.itemEditorList=$('<table class="itemEditorList" border="1">');
			var items=list.items;
			var luListMap=null;
			var refFields=null;
			var table=null;
			for(var key in main.tables){
				if(main.tables[key] && main.tables[key].properties.lookup_table_map && main.tables[key].properties.lookup_table_map[list.properties.name]){
					luListMap=main.tables[key].properties.lookup_table_map[list.properties.name];
					refFields=main.tables[key].fields;
					table=key;
				}
			}
			self.listName=list.properties.name;
			self.layerName=table;
			self.itemEditorWrap=$('<div class="itemEditorWrap" data-listname="'+list.properties.name+'" data-table="'+table+'">');
			self.topButtons=$('<div class="lookupEditorButtons cf">');
			self.topContent.append(self.topButtons);
			self.deleteCheckedB=new Button('std','deleteCheckedLUEB lookUpEditorButton','Delete Checked');
			self.topButtons.append(self.deleteCheckedB.html);
			self.deleteCheckedB.html.on('click',function(){
				if(!self.itemsArray.length || !self.listName){return;}
				if(!confirm('Are you sure you want to delete the checked options?')){return;}
				var items=self.itemEditorWrap.find('.luListEditCB:checked');
				for(var n=0;n<self.itemsArray.length;n++){
					self.editorTbody.find('.luEditListTRPID_'+self.itemsArray[n]).remove();
				}
				$.ajax({
					type:'POST',
					url:main.mainPath+'server/db.php',
					data:{
						params:{
							folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
							itemsArray:self.itemsArray,
							listname:self.listName
						},
						action:'deleteLUListItems'
					},
					success:function(response){
						if(response){
							response=JSON.parse(response);
							if(response.status=="OK"){
								if(response.params.itemsArray){
									for(var t=0;t<response.params.itemsArray.length;t++){
										if(main.data.lookUpTables[response.params.listname].items[response.params.itemsArray[t]]){
											delete main.data.lookUpTables[response.params.listname].items[response.params.itemsArray[t]];
										}
									}
								}
								self.itemEditorWrap.find('.luListEditToggleAll').prop('checked',false);
							}else{
								alert('Error');
								console.log(response);
							}
						}
					}
				});
			});
			self.luEMoreInfo=$('<div class="luEMoreInfo someMoreInfo postScript">').html('*Edits are saved automatically');
			self.topContent.append(self.luEMoreInfo);
			// if(items && Object.keys(items).length>0 && luListMap){
				self.tHeader=$('<tr>').append('<th><input type="checkbox" class="luListEditToggleAll"/></th>');
				self.editorTbody=$('<tbody>');
				for(var key in luListMap){
					self.tHeader.append('<th class="luEditorTH luEditorTHField_'+key+'" data-sortorder="'+self.defaultSortOrder+'" data-field="'+key+'" data-table="'+table+'">'+refFields[key].alias+'</th>');
				}
				self.itemEditorList.append($('<thead>').append(self.tHeader));
				self.itemEditorList.append(self.editorTbody);
				self.itemEditorWrap.append(self.itemEditorList);
				
				self.sortTable(Object.keys(luListMap)[0],self.defaultSortOrder);
			// }
			self.itemAdd=new Button('std','luEditItemAdd lookUpEditorButton','Add');
			self.topButtons.append(self.itemAdd.html);
			self.itemAdd.html.on('click',function(){
				if(self.layerName){
					self.addNewItem();
				}
			});
			self.itemEditorWrap.on('click','.luEditUnitTD',function(){
				if($(this).find('.luEditInputWrap').length==0){
					self.createEditCell($(this).find('.luListEditUnit'));
				}
			});
			self.itemEditorWrap.on('change','.luEditInput',function(){
				self.luEditInputChanged($(this));
			});
			self.itemEditorWrap.on('blur','.luEditInput',function(){
				self.resetCell($(this).closest('.luListEditUnit'));
				self.isReset=true;
			});
			self.itemEditorWrap.on('keypress','.luEditInput',function(e){
				if(e.which==13){$(this).blur();}
			});
			self.itemEditorWrap.on('change','.luListEditToggleAll',function(){
				self.itemEditorWrap.find('.luListEditCB').prop('checked',$(this).prop('checked'));
				self.registerChecked();
			});
			self.itemEditorWrap.on('change','.luListEditCB',function(){
				self.registerChecked();
			});
			self.itemEditorWrap.on('click','.luEditorTH',function(){
				self.sortTable($(this).data('field'),$(this).data('sortorder'));
			});
		};
		this.registerChecked=function(){
			var items=self.itemEditorWrap.find('.luListEditCB:checked');
			self.itemsArray=[];
			items.each(function(){
				self.itemsArray.push($(this).closest('.luEditListTR').data('item').toString());
			});
		};
		this.sortTable=function(field,sortOrder){
			self.editorTbody.html('');
			if(self.currentSortBy==field){
				if(sortOrder=='DESC'){sortOrder='ASC';
				}else{sortOrder='DESC';}
			}else{
				sortOrder=self.defaultSortOrder;
			}
			self.currentSortBy=field;
			var keys=self.getSortedKeys(self.layerName,field,self.listName,sortOrder),tr,checked;
			var luListMap=main.tables[self.layerName].properties.lookup_table_map[self.listName];
			var items=main.data.lookUpTables[self.listName].items;
			for(var t=0;t<keys.length;t++){
				key0=keys[t].pid;
				tr=self.createListTR(items[key0],luListMap);
				self.editorTbody.append(tr);
			}
		};
		this.getSortedKeys=function(table,field,listname,sortOrder){
			var luFieldName=main.tables[table].properties.lookup_table_map[listname][field];
			var items=main.data.lookUpTables[listname].items;
			var aliases=[];
			for(var key0 in items){aliases.push({pid:key0,alias:items[key0][luFieldName]});}
			aliases=aliases.sort(function(a,b){
				var aAlias=(a.alias?a.alias.toLowerCase():'');
				var bAlias=(b.alias?b.alias.toLowerCase():'');
				if(!aAlias && bAlias){return 1;}
				if(!bAlias && aAlias){return -1;}
				if(aAlias>bAlias){return 1;}
				if(aAlias<bAlias){return -1;}
				return 0;
			});
			if(sortOrder=='DESC'){aliases.reverse();}
			return aliases;
		};
		this.createEditCell=function(luListEditUnit){
			var item=luListEditUnit.closest('.luEditListTR').data('item');
			var itemunit=luListEditUnit.closest('.luEditUnitTD').data('itemunit');
			var itemunitkey=luListEditUnit.closest('.luEditUnitTD').data('itemunitkey');
			var listname=self.listName;
			var list=main.data.lookUpTables[listname];
			var val=list.items[item][itemunit] || '';
			var input=$('<input type="text" class="luEditInput" data-item="'+item+'" data-itemunit="'+itemunit+'" data-itemunitkey="'+itemunitkey+'" value="'+val+'">');
			var inputDiv=$('<div class="luEditInputWrap">').append(input);
			luListEditUnit.find('.luEditCurrent').addClass('none');
			luListEditUnit.append(inputDiv);
			input.focus();
		};
		this.resetCell=function(luListEditUnit){
			luListEditUnit.find('.luEditCurrent').removeClass('none');
			if(luListEditUnit.find('.luEditInputWrap').length){luListEditUnit.find('.luEditInputWrap').first().remove();}
		};
		this.luEditInputChanged=function(luEditInput){
			var newVal=luEditInput.val();
			var field=luEditInput.data('itemunit');
			var item=luEditInput.data('item');
			var listname=self.listName;
			luEditInput.closest('.luListEditUnit').find('.luEditCurrent').html(newVal);
			if(!self.isReset){
				self.resetCell(luEditInput.closest('.luListEditUnit'));
			}
			self.isReset=false;
			main.data.lookUpTables[listname].items[item][field]=newVal;
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
						newVal:newVal,
						field:field,
						item:item,
						listname:listname
					},
					action:'updateLUList'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.newEditPanel=function(lulist){
			if(self.panel){
				self.panel.remove();
				delete self.panel;
			}
			var list=main.data.lookUpTables[lulist];
			self.createEditorHTML(list);
			self.panel=new Panel({
				content:self.itemEditorWrap,
				topContent:self.topContent,
				title:self.alias+' - '+list.properties.alias,
				classes:'luEditor',
				hideOnOutClick:true,
				scrollToTop:true,
				onClose:self.panelClose
			});
			self.panel.open();
			if($('body').width()<=main.dimensions.phoneWidth){
				main.layout.sidePanels[self.shelf.closest('.sidePanel').data('button')].close();
				main.map.adjustMapSize();
			}
		};
		(initEvents=function(){
			self.html.on('click','.luELI',function(){
				self.newEditPanel($(this).data('lulist'));
			});
		})();
	};
});