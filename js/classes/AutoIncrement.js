define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Input=require('./Input');
		var ViewMore=require('./ViewMore');
		this.alias='Auto Increment';
		this.name='autoIncrement';
		this.withItem='with_auto_increment';
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.currentLayer=null;
		this.currentField=null;
		this.currentFieldToIncrement=null;
		this.updateAllChecked=null;
		this.fields=null;
		this.addShelf=function(){
			self.launchButton=new Button('toolBox','',self.alias);
			// var link='<a href="'+window.location.href+'exporter/'+'">Click here to Export.</a>';
			self.accordian=main.layout.toolBox;
			self.shelf=main.layout.toolBox.createShelf({
				name:self.name,
				button:self.launchButton.html,
				content:self.html,
				context:self,
				alias:self.alias,
				dontAppendTo:true,
				groups:main.withs[self.withItem].toolbox_groups
			});
			main.layout.toolBox.shelfInAppTest(main.layout.toolBox.shelves[self.name]);
		};
		this.layerSelectUpdate=function(){
			var layers=main.invents.inventories;
			self.layerSelect.html('').append('<option value=""></option>')
			for(var key in layers){
				self.layerSelect.append('<option value="'+key+'">'+layers[key].properties.alias+'</option>')
			}
			main.utilities.sortOptions(self.layerSelect);
		};
		this.layerSelectChange=function(){
			self.currentLayer=$(this).val();
			self.fieldSelectUpdate();
		};
		this.fieldToIncrementSelectChange=function(){
			self.currentFieldToIncrement=$(this).val();
		};
		this.updateAllCheckChange=function(){
			self.updateAllChecked=$(this).prop('checked');
		};
		this.fieldSelectChange=function(){
			self.currentField=$(this).val();
			self.input=new Input({
				field:self.fields[self.currentField],
				layerName:self.currentLayer,
				classes:'autoIncrementInput',
				sortBy:'abc',
				dontAddDefault:true
			});
			self.valueWrap.html(self.input.container);
		};
		this.fieldSelectUpdate=function(){
			self.fields=main.invents.inventories[self.currentLayer].fields;
			self.fieldSelect.html('').append('<option value=""></option>')
			self.fieldToIncrementSelect.html('').append('<option value=""></option>')
			for(var key in self.fields){
				if(self.fields[key].active){
					self.fieldSelect.append('<option value="'+key+'">'+self.fields[key].alias+'</option>');
					if(/* main.misc.numberDTs.indexOf(self.fields[key].data_type)>-1 &&  */key!='pid'){
						self.fieldToIncrementSelect.append('<option value="'+key+'">'+self.fields[key].alias+'</option>');
					}
				}
			}
			main.utilities.sortOptions(self.fieldSelect);
			main.utilities.sortOptions(self.fieldToIncrementSelect);
		};
		this.go=function(){
			if(!self.currentLayer || (!self.currentField && !self.updateAllChecked) || !self.currentFieldToIncrement){alert('Select the required information.');return;}
			var layer=self.currentLayer;
			var field=self.fields[self.currentField];
			if(self.input){var val=main.utilities.getValFromInput(field,self.input.container.find('.formInput').andSelf().filter('.formInput').eq(0));
			}else{var val=null}
			if(!val && !self.updateAllChecked){alert('Select a value or select the "No Field to Compare" check box.');return;}
			var fieldToIncrement=self.fields[self.currentFieldToIncrement];
			if(!confirm('Are you sure you want to overwrite '+fieldToIncrement.alias+' with new values?')){return;}
			var fieldPkg={
				value:val,
				data_type:field.data_type,
				sub_data_type:field.sub_data_type,
				input_type:field.input_type,
				name:field.name
			};
			var fieldToIncrementPkg={
				data_type:fieldToIncrement.data_type,
				sub_data_type:fieldToIncrement.sub_data_type,
				input_type:fieldToIncrement.input_type,
				name:fieldToIncrement.name
			};
			//address_street=Kahala Dr
			// var where=field+"="+val;
			var updateAll=self.updateAllChecked || null;
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
						layer:layer,
						updateAll:updateAll,
						fieldToIncrementPkg:fieldToIncrement,
						fieldPkg:fieldPkg,
						val:val
					},
					action:'autoIncrement'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		(this.createHTML=function(){
			self.layerSelect=$('<select class="autoIncrementLayerSelect">').on('change',self.layerSelectChange);
			self.fieldToIncrementSelect=$('<select class="autoIncrementFieldSelect">').on('change',self.fieldToIncrementSelectChange);
			self.fieldSelect=$('<select class="autoIncrementFieldSelect">').on('change',self.fieldSelectChange);
			self.layerSelectUpdate();
			self.valueWrap=$('<div class="tdLiner">')
			self.updateAllCheck=$('<input type="checkbox" class="autoIncrementUpdateAllCheck"/>').on('change',self.updateAllCheckChange);
			self.submitButton=new Button('std','autoIncrementGoButton','Go').html.on('click',function(){
				self.go();
			});
			self.table=$('<table class="autoIncrementTable" border="1">')
				.append($('<tbody>')
					.append($('<tr>')
						.append($('<td>')
							.append($('<div class="tdLiner">').html('Layer'))
						)
						.append($('<td>')
							.append($('<div class="tdLiner">').html(self.layerSelect))
						)
					)
					.append($('<tr>')
						.append($('<td>')
							.append($('<div class="tdLiner">').html('Field To Increment'))
						)
						.append($('<td>')
							.append($('<div class="tdLiner">').html(self.fieldToIncrementSelect))
						)
					)
					.append($('<tr>')
						.append($('<td>')
							.append($('<div class="tdLiner">').html('Field To Compare'))
						)
						.append($('<td>')
							.append($('<div class="tdLiner">').html(self.fieldSelect))
						)
					)
					.append($('<tr>')
						.append($('<td>')
							.append($('<div class="tdLiner">').html('Value To Compare'))
						)
						.append($('<td>')
							.append(self.valueWrap)
						)
					)
					.append($('<tr>')
						.append($('<td>')
							.append($('<div class="tdLiner">').html('No Field to Compare (Increment all records)'))
						)
						.append($('<td>')
							.append(self.updateAllCheck)
						)
					)
					.append($('<tr>')
						.append($('<td colspan="2">')
							.append($('<div class="tdLiner center">').html(self.submitButton))
						)
					)
				)
			self.infoViewMore=new ViewMore({
				prev:'Use this tool to auto-increment an ID field based on another field value.',
				content:'<ul><li>Step 1: Select a layer.</li><li>Step 2: Select a field to increment by. This is usually an ID field.</li><li>Step 3: If you want to only increment records that have a certain value for a certain field, select a "Field to Compare". Then, set a value to compare this field to in the "Value To Compare" section.</li><li>Step 4: If you want to update all records with the new increment, check the "No Field to Compare" check box.</li><li>Step 5: Click "Go".</li></ul>',
				hidePrevOnShow:true
			});
			self.html=$('<div class="autoIncrementWrap">')
				.append(self.infoViewMore.html)
				.append(self.table);
			self.addShelf();
		})();
	}
});
