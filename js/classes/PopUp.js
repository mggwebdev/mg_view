define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var EditForm=require('./EditForm');
		var Panel=require('./Panel');
		this.id=main.utilities.generateUniqueID();
		this.showOnCreate=true;
		this.visible=false;
		this.active=false;
		this.iconOffset=[26,-123];
		this.withImageView=true;
		this.map=main.map;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		if(this.context && !this.omitDetails && (!this.context.properties || !this.context.properties.without_details)){this.withDetails=true;
		}else{this.withDetails=false;}
		this.withDTSC=true;
		this.classes=this.classes || '';
		this.pid=this.pid || (this.properties && this.properties.pid) || null;
		if(this.otherFeatures && this.otherFeatures.length>1){
			this.hasOtherFeatures=true;
		}
		/* this.arrowVisTest=function(){
			self.othFeatures.togLeft.removeClass('none');
			self.othFeatures.togRight.removeClass('none');
			if(self.currentIdx==0){
				self.othFeatures.togLeft.addClass('none');
			}else if(self.currentIdx==self.otherFeatures.length-1){
				self.othFeatures.togRight.addClass('none');
			}
		}; */
		this.createEditForm=function(linkTo){
			if(!self.editForm){
				self.editForm=new EditForm({
					inventory:self.context,
					// content:self.inventory.inventory.map_layer.features[self.pid].editTable.table.clone(true),
					properties:self.properties,
					isNew:self.isNew,
					linkTo:linkTo
				});
				main.layout.editForms.push(self.editForm);
			}
		};
		this.setCurrentIDX=function(){
			self.currentIdx=null;
			for(var i=0;i<self.otherFeatures.length;i++){
				if(self.pid==self.otherFeatures[i].get('pid')){
					self.currentIdx=i;
					// break;
				}
			}
		};
		this.setProperties=function(properties){
			self.properties=properties;
		};
		this.resetEcoBensHTML=function(){
			var ecobenCats=main.dashboard.ecobensCategories;
			var ecobenCatsKeys=main.dashboard.ecobensKeys;
			var ecobens=main.data.std_settings.ecobens;
			
			self.ecobensUL=$('<ul class="ecobensUL">');
			var noEcoBens=true,ecoKey,val,prepend_to,append_to,ecoCatSubLI,img,ecoCatSubUL,ecoCatLabel,ecoCat;
			for(var t=0;t<ecobenCatsKeys.length;t++){
				ecoKey=ecobenCatsKeys[t];
				ecoCatSubUL=$('<ul class="ecoCatSubUL">');
				for(var key in ecobens){
					if(ecobens[key].category==ecoKey && self.properties[key]){
						val=Number(self.properties[key]);
						prepend_to='',append_to='';
						if(ecobens[key].prepend_to){prepend_to=ecobens[key].prepend_to;}
						if(ecobens[key].append_to){append_to=ecobens[key].append_to;}
						if(val<5){val=val.toFixed(2);
						}else{val=val.toFixed(0);}
						val=prepend_to+main.utilities.addCommas(val)+append_to;
						ecoCatSubLI=$('<li class="ecoCatSubLI">')
							.append($('<div class="ecoCatSubLIEle ecoCatSubLILabel">').html(ecobens[key].alias))
							.append($('<div class="ecoCatSubLIEle ecoCatSubLIDetails">')
								.append($('<div class="ecoCatSubLIDetailsInner">').html(val)));
						ecoCatSubUL.append(ecoCatSubLI);
						noEcoBens=false;
					}
				}
				ecoCatLabel=$('<div class="ecoCatLabel">').html(ecobenCats[ecoKey].alias);
				img=ecobenCats[ecoKey].img || '';
				if(img){img=$('<img class="containerImg" src="'+main.mainPath+'images/'+img+'">');}
				ecoCat=$('<li class="ecobensCatLI">')
					.append(ecoCatLabel)
					.append(img)
					.append(ecoCatSubUL);
				self.ecobensUL.append(ecoCat);
			}
			self.ecobensContentWrap=$('<div class="ecobensContentWrap">');
			if(!noEcoBens){self.ecobensContentWrap.append(self.ecobensUL);
			}else{self.ecobensContentWrap.append($('<div class="inNoEcoBens">').html('No eco-benefits data for this '+self.context.singular_alias.toLowerCase()+'.'));}
			self.ecobenPanel.setContent(self.ecobensContentWrap);
		};
		this.resetEcoBens=function(){
			self.setProperties(self.context.map_layer.features[self.pid].properties);
			self.resetEcoBensHTML();
			if(!self.ecobenPanel.opened){self.ecobenPanel.open();}
		};
		this.editFormInitiate=function(){
			if(main.account.loggedInUser.user_type_data.with_editform && self.context.properties[main.account.loggedInUser.user_type+'_editable']){
				if(!self.editForm){
					self.createEditForm();
					self.editForm.open();
				}else{
					self.editForm.open();
				}
				return true;
			}else{
				alert('This requires a log in. Please log in.');
				// open login
				main.utilities.openForm('logIn','account',true);
			}
			return false;
		};
		this.openWOM=function(){
			if(self.editFormInitiate()){
				if(self.context.properties.is_wom){	
					self.editForm.tabs.tabsWrap.find('.linkInvTab').click();
				}else if(main.withs.with_wom && main.withs.with_wom.value && !self.context.inventory.properties.is_wom){
					self.editForm.tabs.tabsWrap.find('.womTab').click();
				}
			}
		};
		this.getCountByRestrictorForLoadAdd=function(response,pids){
			if(Number(response.results[Object.keys(response.results)[0]])){
				self.addLoadButton();
			}
		};
		this.addLoadButton=function(){
			self.loadInvtButton=new Button('std','loadInvtButton','Load '+main.invents.inventories[self.context.inventory.is_depended_upon_by].properties.plural_alias);
			self.loadInvtButton.html.attr('title','Click to Load Features');
			self.puBotButtons.prepend(self.loadInvtButton.html);
			self.loadInvtButton.html.on('click',function(){
				self.context.blockMoveEndLoadOnce=true;
				self.context.loadFeatures({pid:self.pid,context:self,zoomTo:true});
			});
		};
		(this.createPopupHTML=function(){
			self.content=$('<div>').addClass('popupContent').html(self.html);
			self.puBotButtons=$('<div class="puBotButtons cf">');
			if(self.context && self.context.inventory && self.context.inventory.is_depended_upon_by && !self.properties.bulk_count/*  && (!main.inventories[self.context.inventory.is_depended_upon_by].currentLoad || self.pid!=main.inventories[self.context.inventory.is_depended_upon_by].currentLoad) */){
				if(main.invents.inventories[self.context.inventory.is_depended_upon_by].properties[main.account.loggedInUser.user_type+'_editable']){
					self.addLoadButton();
				}else{
					main.utilities.getCountByRestrictor(self.context.inventory.is_depended_upon_by,self.layerName,main.invents.inventories[self.context.inventory.is_depended_upon_by].properties.internal_field_map.dependent_upon_field,[self.pid],self.getCountByRestrictorForLoadAdd);
				}
				if(main.account.loggedInUser.username=='user1'){
					self.reIndexButton=new Button('std','reIndex','ReIndex');
					// self.puBotButtons.append(self.reIndexButton.html);
					self.reIndexButton.html.on('click',function(){
						self.context.reIndex(self.pid,self.context.inventory.is_depended_upon_by);
					});
				}
			}
			if(self.withDetails && (self.context.properties && (self.context.properties[main.account.loggedInUser.user_type+'_editable'] || self.context.properties[main.account.loggedInUser.user_type+'_visible'])) && (!self.context.properties || !self.context.properties.min_popup_buttons || !main.account.loggedInUser.base_user)){
				self.puDetailsButton=new Button('std','puDetailsButton','Details');
				self.puBotButtons.append(self.puDetailsButton.html);
				self.puDetailsButton.html.on('click',function(){
					self.editFormInitiate();
				});
				if(self.context.properties.photo_fields){
					var photoFields=self.context.properties.photo_fields.split(',');
					for(var u=0;u<photoFields.length;u++){
						var value=null;
						if(self.properties[photoFields[u]]){
							value=self.properties[photoFields[u]];
						}
						if(self.context.fields[photoFields[u]].sub_data_type=='photo'){
							self.showImages=main.utilities.getPhotosPkg(self.pid,photoFields[u],self.context.layerName,value);
						}else{
							main.utilities.getPhotosPkg(self.pid,photoFields[u],self.context.layerName,value);
						}
					}
					self.viewPhotosButton=self.showImages.viewPhotosButton;
					self.viewPhotosButton.html.on('click',main.utilities.viewPhotosClicked);
					if(!main.tables[self.context.layerName].atchs || !main.tables[self.context.layerName].atchs.items[self.pid] || !main.tables[self.context.layerName].atchs.items[self.pid].fields || !main.tables[self.context.layerName].atchs.items[self.pid].fields.photos || !main.tables[self.context.layerName].atchs.items[self.pid].fields.photos.puCurrentPhotos || Object.keys(main.tables[self.context.layerName].atchs.items[self.pid].fields.photos.puCurrentPhotos.elItems).length==0){self.showImages.html.addClass('none');}
					self.puBotButtons.append(self.showImages.html);
				}
			}
			if(self.context && self.context.properties && self.context.properties.popup_buttons){
				var buttons=self.context.properties.popup_buttons,button;
				self.extra_popup_buttons=[];
				for(var y=0;y<buttons.length;y++){
					var go=true;
					if(buttons[y][3]=='time'){
						go=false;
						var fields=self.context.fields;
						for(var key in fields){
							if(fields[key].active && fields[key].time_sensitive_info && fields[key].time_sensitive_info.type=='occurred_or_not'){
								var tsi=fields[key].time_sensitive_info;
								if(self.properties[key] && self.properties[key]!=tsi.completed_val && self.properties[key]!=tsi.cancelled_val){
									go=true;
								}
							}
						}
					}
					if(go){
						button=new Button('std','puDetailsButton',buttons[y][2],[['shelf',buttons[y][0]],['accordian',buttons[y][1]]]);
						button.html.on('click',function(){
							main.utilities.openForm($(this).data('shelf'),$(this).data('accordian'),true);
						});
						self.puBotButtons.append(button.html);
						self.extra_popup_buttons.push(button);
					}
				}
			}
			if(self.context && self.context.properties){
				if(self.context.properties.is_wom || (main.withs.with_wom && main.withs.with_wom.value)){
						self.womButton=new Button('std','puWomButton','WOM');
						self.puBotButtons.append(self.womButton.html);
						self.womButton.html.on('click',function(){
							self.openWOM();
						});
				}
				if(main.withs.with_ecobens.value && self.context.properties.with_ecobens){
					self.ecobenPanel=new Panel({
						// content:self.ecobensContentWrap,
						content:'',
						title:'Eco-Benefits',
						matchHeight:true,
						classes:'indEcobensPanel',
						relativeTo:main.layout.mapContainerWrap,
						centerOnOrientationChange:true
					});
					self.ecoPupOpenButton=new Button('std','ecoPupOpenButton','Eco-Benefits');
					self.puBotButtons.append(self.ecoPupOpenButton.html);
					self.ecoPupOpenButton.html.on('click',function(){
						self.resetEcoBens();
					});
				}
				if(self.withDTSC && self.context.is_inventory && (!self.context.properties.min_popup_buttons || !main.account.loggedInUser.base_user)/*  && self.context.dataTable */){
					self.puDTButton=new Button('std','puDTButton','Data Table');
					/* if(self.context.properties[main.account.loggedInUser.user_type+'_dt_visible']){ */
					self.puBotButtons.append(self.puDTButton.html);
					/* } */
					self.puDTButton.html.on('click',function(){
						if(self.context.properties[main.account.loggedInUser.user_type+'_dt_visible']){
							if(self.context.dataTable){
								var shelf=self.context.dataTable.html.closest('.shelf');
								var name=shelf.data('name');
								var id=null;
								if(self.context.groupOrganizer.accordian && self.context.groupOrganizer.accordian.shelves[name] && self.context.groupOrganizer.accordian.shelves[name].dataTablePresent){
									id=self.context.groupOrganizer.accordian.id;
								}else if(self.context.groupOrganizer.womAccordian && self.context.groupOrganizer.womAccordian.shelves[name] && self.context.groupOrganizer.womAccordian.shelves[name].dataTablePresent){
									id=self.context.groupOrganizer.womAccordian.id;
								}
								if(id){
									self.context.dataTable.dontLoadOnOpen=true;
									main.utilities.openForm(name,id,true,true);
									self.context.dataTable.setDTForSingleItem(self.pid);
								}
							}
						}else{
							alert('This requires a log in. Please log in.');
							// open login
							main.utilities.openForm('logIn','account',true);
						}
					});
					// if(self.inventory.properties.photo_fields){
						// var photoFields=self.inventory.properties.photo_fields.split(',');
						// self.showImages=main.utilities.getPhotosPkg(self.pid,photoFields[0],self.inventory.layerName);
						// self.viewPhotosButton=self.showImages.viewPhotosButton;
						// self.viewPhotosButton.html.on('click',main.utilities.viewPhotosClicked);
						// self.puBotButtons.append(self.showImages.html);
					// }
				}
			}
			self.popupTop=$('<div>').addClass('popupTop cf');
			self.topLabel=$('<div>').addClass('popupTopLabel');
			if(self.topLabelContent){self.topLabel.html(self.topLabelContent)}
			self.popupTop.append(self.topLabel)
			self.closer=$('<div>').addClass('popupButton closePopup noSelect').text('x');
			self.popupButtons=$('<div>').addClass('popupButtons cf')
				.append(self.closer);
			self.popupTop.append(self.popupButtons)
			if(self.hasOtherFeatures && self.pid){
				if(!self.currentIdx && self.currentIdx!==0){self.setCurrentIDX();}
				self.othFeatures={};
				self.othFeatures.otherFeaturesDesc=$('<div class="otherFeaturesDesc otherFeaturesWrapEle">').html((self.currentIdx+1)+' of '+self.otherFeatures.length);
				self.othFeatures.buttons=$('<div class="otherFeaturesButtons otherFeaturesWrapEle cf">');
				self.othFeatures.togLeft=$('<div class="togLeft otherFeatsTog" title="Next Feature: '+self.otherFeatures.length+' features">');
				self.othFeatures.togRight=$('<div class="togRight otherFeatsTog" title="Previous Feature: '+self.otherFeatures.length+' features">');
				self.othFeatures.otherFeatures=self.otherFeatures;
				self.othFeatures.togLeftWrap=$('<div class="togLeft otherFeatsTogWrap">').append(self.othFeatures.togLeft);
				self.othFeatures.togRightWrap=$('<div class="togRight otherFeatsTogWrap">').append(self.othFeatures.togRight);
				self.othFeatures.html=$('<div class="otherFeaturesWrap cf">')
					.append(self.othFeatures.otherFeaturesDesc)
					.append(self.othFeatures.buttons
						.append(self.othFeatures.togLeftWrap)
						.append(self.othFeatures.togRightWrap)
					);
				// self.arrowVisTest();
				self.othFeatures.togLeft.on('click',function(){
					self.prevFeature();
				});
				self.othFeatures.togRight.on('click',function(){
					self.nextFeature();
				});
				self.popupTop.append(self.othFeatures.html);
			}
			self.container=$('<div>').addClass('popup '+self.classes+' '+(self.pid || '')).data('pid',self.pid)
				.append($('<div>').addClass('pointerWrap pointerLeft')
					.append($('<div>').addClass('arrowLeft')
						.append($('<div>').addClass('arrowP'))
						.append($('<div>').addClass('arrowCov'))
					)
				)
				.append(self.popupTop)
				.append(self.content)
				.append(self.puBotButtons);
			main.layout.popupsContainer.append(self.container);
		})();
		(this.createOverlay=function(){
			self.overlay=new ol.Overlay({
				element:self.container
			});
			self.map.map.addOverlay(self.overlay);
		})();
		this.nextFeature=function(){
			self.currentIdx++;
			if(self.currentIdx==self.otherFeatures.length){self.currentIdx=0;}
			self.context.lastFocused=self.context.currentFocused;
			self.context.currentFocused=self.otherFeatures[self.currentIdx];
			main.mapItems.unSetActives();
			self.otherFeatures[self.currentIdx].set('isActive',true);
			main.mapItems.hasActive=self.otherFeatures[self.currentIdx].get('pid');
			main.mapItems.hasActiveLayer=self.layerName;
			self.genNewTable();
		};
		this.prevFeature=function(){
			self.currentIdx--;
			if(self.currentIdx==-1){self.currentIdx=self.otherFeatures.length-1;}
			self.context.lastFocused=self.context.currentFocused;
			self.context.currentFocused=self.otherFeatures[self.currentIdx];
			self.genNewTable();
		};
		this.genNewTable=function(){
			var feature=self.otherFeatures[self.currentIdx];
			self.context.mapInteractions.showPopup(feature.get('pid'),feature,self.otherFeatures);
		};
		this.setMarker=function(marker){
			self.marker=marker;
		};
		this.makeActive=function(){
			self.active=true;
			// $('.activePanel').removeClass('activePanel');
			// self.container.addClass('activePanel');
			self.container.css('z-index',++main.misc.maxZIndex);
		};
		this.setCood=function(cood){
			if(cood){self.position=cood;}
		};
		this.setPosition=function(cood){
			if(cood){self.position=cood;}
			self.overlay.setPosition(self.position);
			self.overlay.setOffset(self.iconOffset);
		};
		this.setContentHTML=function(html){
			if(html){self.contentHTML=html;}
			self.content.html(self.contentHTML);
		};
		this.refreshEditForm=function(){
			var coords=null;
			var zIndex=null;
			var tab=null;
			if(self.editForm){
				if(self.editForm.panel.opened){
					coords=[parseInt(self.editForm.panel.html.css('left')),parseInt(self.editForm.panel.html.css('top'))];
					zIndex=self.editForm.panel.html.css('z-index');
					if(self.editForm.tabs && self.editForm.tabs.activeTab){tab=self.editForm.tabs.activeTab.name;}
				}
				self.editForm.panel.html.remove();
				for(var i=0;i<main.layout.editForms.length;i++){
					if(main.layout.editForms[i].id==self.editForm.id){
						main.layout.editForms.splice(i,1);
						break;
					}
				}
				delete self.editForm;
				self.createEditForm();
			}
			return {
				coords:coords,
				zIndex:zIndex,
				tab:tab
			}
		};
		this.refresh=function(beingCreated,dontMakeActive,popUpZ/* ,otherFeatures */){
			self.context.mapInteractions.createFeatureHTML(self.properties);
			self.html.remove();
			self.html=self.context.map_layer.features[self.pid].editTable.table;
			self.content.html(self.html);
			
			if(self.editForm && self.editForm.panel.opened){
				var location=self.refreshEditForm();
				self.editForm.open();
				if(location && location.coords){
					self.editForm.panel.setPosition(location.coords);
					self.editForm.panel.setZIndex(location.zIndex);
					if(location && location.tab){
						self.editForm.tabs.setActiveTabByName(location.tab);
						self.editForm.tabChanged();
					}
				}
			}
		};
		this.showPopup=function(beingCreated,dontMakeActive,popUpZ/* ,otherFeatures */){
			self.setPosition(self.position);
			self.visible=true;
			self.wasOn=true;
			// self.makeActive();
			if(!dontMakeActive){self.makeActive();}
			if(popUpZ){self.container.css('z-index',popUpZ);}
			// if(otherFeatures && otherFeatures.length>0){
				// self.hasOtherFeatures=true;
				// self.otherFeatures=otherFeatures;
				// self.othFeatures.otherFeatures=self.otherFeatures;
				// self.setCurrentIDX();
			// }else{
				if(self.hasOtherFeatures && self.pid && !beingCreated){
					self.setCurrentIDX();
				}/* else{
					self.hasOtherFeatures=false;
					self.otherFeatures=[];
					self.othFeatures.otherFeatures=self.otherFeatures;
				}
			} */
		};
		this.hide=function(wasOn){
			self.overlay.setPosition(undefined);
			self.closer.blur();
			self.visible=false;
			if(wasOn){self.wasOn=true;}else{
			self.wasOn=false;}
			if(self.onClose){self.onClose(self);}
		};
		this.remove=function(removeContainer){
			self.container.remove();
			if(self.onClose){self.onClose(self);}
			delete self;
		};
		(this.initEvents=function(){
			self.closer.on('click',function(){
				if(self.removeMarkerOnPopUpClose && self.marker){
					if(self.marker.layer){
						self.marker.layer.removeFeature(self.marker.id,true);
					}else if(self.context && self.context.removeFeature){
						self.context.removeFeature(self.marker.id,true);
					}
					self.remove();
				}else{
					self.hide();
				}
				if(self.onClickClose){self.onClickClose(self);}
				return false;
			});
			self.container.on('mousedown',function(e){
				if($(e.target).closest('.closePopup,.puDetailsButton').length===0){
					self.makeActive();
				}
			});
		})();
		if(self.layer){
			self.layer.registerPopUp(self);
		}
		if(self.showOnCreate){
			self.showPopup(true,self.dontMakeActive,self.popUpZ);
		}
		self.dontMakeActive=false;
	};
});