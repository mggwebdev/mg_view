define(function(require){
	return function pg(args){
		var self=this;
		var Checkbox=require('./Checkbox');
		this.id=this.id || main.utilities.generateUniqueID();
		this.type="season";
		this.yearMin=1998;
		this.defaultYearsTilMax=3;
		this.yearMax=new Date().getFullYear()+this.defaultYearsTilMax;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.getTimeStampFromSeason=function(){
			if(self.season && self.year){
				var month=main.misc.seasons[self.season].months[0][0]+1;
				return new Date(month+'-1-'+self.year).getTime();
			}else{return null;}
		};
		(this.init=function(){
			if(self.type=='season'){
				self.seasonSelect=$('<select class="seasonSelect">');
				self.seasonSelect.append('<option value="">Select Season</option>');
				for(var key in main.misc.seasons){
					self.seasonSelect.append('<option value="'+key+'">'+main.misc.seasons[key].alias+'</option>');
				}
				self.yearSelect=$('<select class="yearSelect">');
				self.yearSelect.append('<option value="">Select Year</option>');
				for(var i=self.yearMin;i<=self.yearMax;i++){
					self.yearSelect.append('<option value="'+i+'">'+i+'</option>');
				}
				self.input=$('<input type="hidden" name="'+self.fieldName+'" value="'+self.value+'">').addClass('seasonInput formInput uiInput toolBoxInput '+self.fieldName+' '+this.classes).data('name',self.fieldName);
				self.container=$('<div class="seasonDateContainer uniqueSelectContainer">')
					.append(self.input)
					.append(self.seasonSelect)
					.append('<br/>')
					.append(self.yearSelect);
				if(self.value && !isNaN(self.value)){
					self.value=Number(self.value);
					var seasonStuff=main.utilities.getSeasonFromTimeStamp(self.value);
					self.season=seasonStuff.season;
					self.year=seasonStuff.year;
					self.input.val(self.value);
				}
				if(self.season){self.seasonSelect.val(self.season);}else{self.season=null;}
				if(self.year){self.yearSelect.val(self.year);}else{self.year=null;}
				self.timeStamp=self.getTimeStampFromSeason();
				self.seasonSelect.on('change',function(){
					self.season=$(this).val();
					if(self.season && self.year){
						self.timeStamp=self.getTimeStampFromSeason();
						self.input.val(self.timeStamp).change();
					}else{self.timeStamp=null}
				});
				self.yearSelect.on('change',function(){
					self.year=$(this).val();
					if(self.season && self.year){
						self.timeStamp=self.getTimeStampFromSeason();
						self.input.val(self.timeStamp).change();
					}else{self.timeStamp=null}
				});
			}
		})();
	}
});