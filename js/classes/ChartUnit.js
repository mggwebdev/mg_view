define(function(require){
	return function pg(args){
		var Button=require('./Button');
		var Panel=require('./Panel');
		var Tabs=require('./Tabs');
		var self=this;
		this.id=main.utilities.generateUniqueID();
		this.editable=true;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.alias=self.inventory.singular_alias+' Details';
		this.convertToEditing=function(pid){
			self.inventory.inventory.map_layer.features[pid].editLargeTable.table.find('.outerEditingW').removeClass('editing').addClass('notEditing').find('.dataTableEdit').addClass('block');
			self.inventory.inventory.map_layer.features[pid].editLargeTable.table.find('.dataTableCurrent,.dataTableEditLink,.dataTableDoneEditLink').addClass('none');
		};
		(this.createHTML=function(name){
			self.html=$('<div class="editForm dataUnit">')
			var fields=self.inventory.fields,field,value,key;
			var pid=self.properties.pid;
			self.inventory.inventory.map_layer.features[pid].fields={};
			self.inventory.inventory.map_layer.features[pid].editLargeTable={};
			self.inventory.inventory.map_layer.features[pid].editLargeTable.tbody=$('<tbody>');
			if(!self.sortedFieldKeys){self.sortedFieldKeys=main.utilities.sortFields(fields,'place_in_order');}
			for(var i=0;i<self.sortedFieldKeys.length;i++){
				key=self.sortedFieldKeys[i];
				field=fields[key];
				value=self.properties[key];
				if(field.data_type=="geometry"){value=self.inventory.inventory.map_layer.features[pid][key];}
				self.inventory.inventory.map_layer.features[pid].fields[key]={
					current_val:value,
					html:value,
					field:field
				};
				main.utilities.getRowThings(self.inventory.inventory.map_layer.features[pid],field,pid,{params:{pid:pid,fieldName:key,value:value}},{callback:self.newSignature},null,true);
				if(field.in_popup && field.input_type!='hidden'){
					self.inventory.inventory.map_layer.features[pid].editLargeTable.tbody.append($('<tr class="popUpRow '+key+'" data-field="'+key+'">')
						.append($('<td>')
							.append($('<div class="tdLiner popupRowLabel">').text(field.alias))
						)
						.append($('<td class="outerEditingW notEditing">')
							.append($('<div class="tdLiner">')
								.append(
									$('<div data-field="'+key+'">').addClass('tdLiner popUpEditPkg '+key)
										.append($('<div>').addClass('dataTableCurrent '+key).data('field',key).html(self.inventory.inventory.map_layer.features[pid].fields[key].html))
										.append(self.inventory.inventory.map_layer.features[pid].fields[key].largeEditPackage)
								)
							)
						)
					)
				}
			}
			self.inventory.inventory.map_layer.features[pid].editLargeTable.table=$('<table class="popupTable dataUnit" border="1" data-pid="'+pid+'">')
				.append($('<thead>')
					.append($('<tr>')
						.append($('<th colspan="2">')
							.append($('<div class="tdLiner">').text(self.inventory.singular_alias))
						)
					)
				)
				.append(self.inventory.inventory.map_layer.features[pid].editLargeTable.tbody);
			if(!self.editable){main.utilities.tableToUneditable(self.inventory.inventory.map_layer.features[pid].editLargeTable.table)}
			self.inventory.inventory.map_layer.features[pid].editLargeTable.tbody.on('change','.formInput',function(){
				self.inventory.inputChanged($(this),true);
			});
			self.convertToEditing(pid);
			// self.label=$('<div class="editFormLabel">').text(self.alias);
			self.content=$('<div class="editFormContent">');
			self.html
				.append(self.content.append(self.inventory.inventory.map_layer.features[pid].editLargeTable.table));
			self.tabs=new Tabs();
			var activeTab=self.tabs.addTab('Tree',$('<div class="editFormTab">')
				.append(self.html)
			);
			self.tabs.addTab('Site',$('<div class="editFormTab">'));
			self.tabs.setActiveTab(activeTab.tabid);
			self.panel=new Panel(self.tabs.html,'',self.alias,'editFormPanel');
			self.panel.open();
		})();
	};
});