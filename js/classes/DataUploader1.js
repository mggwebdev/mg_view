define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		this.alias='Uploader';
		this.name='uploader';
		this.withItem='with_uploader';
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.addShelf=function(){
			self.launchButton=new Button('toolBox','',self.alias);
			var link='<a href="'+window.location.href+'uploader/'+'">Click here to Upload.</a>';
			self.shelf=main.layout.toolBox.createShelf({
				name:self.name,
				button:self.launchButton.html,
				content:link,
				context:self,
				alias:self.alias,
				dontAppendTo:true,
				groups:main.withs[self.withItem].toolbox_groups
			});
			main.layout.toolBox.shelfInAppTest(main.layout.toolBox.shelves[self.name]);
		};
		(this.createHTML=function(){
			self.addShelf();
		})();
	}
});