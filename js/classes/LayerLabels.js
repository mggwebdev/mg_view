define(function(){
	return function pg(vectorLayer){
		var self=this;
		this.map=vectorLayer.map;
		this.vectorLayer=vectorLayer;
		this.withLabels=false;
		this.createTextStyle=function(feature,resolution,dom){
			var params=self.params;
			return new ol.style.Text({
				textAlign:params.align.current_val,
				textBaseline:params.baseline.current_val,
				font:params.weight.current_val+' '+params.size.current_val+' '+params.font.current_val,
				text:self.getText(feature,resolution,dom,params),
				fill:new ol.style.Fill({color:params.fill_color.current_val}),
				stroke:new ol.style.Stroke({color:params.outline_color.current_val,width:params.outline_width.current_val}),
				offsetX:params.offset_x.current_val,
				offsetY:params.offset_y.current_val,
				rotation:params.rotation.current_val
			});
		},
		this.setParams=function(){
			if(main.labels){
				this.params=main.labels.params;
			}
		};
		this.getText=function(feature,resolution,dom,params){
			var type=params.text.current_val;
			var maxResolution=params.maxreso.current_val;
			var value=feature.get(self.labelField) || '';
			var field=main.map_layers[self.vectorLayer.properties.layer_name].fields[self.labelField];
			var text=main.utilities.getHTMLFromVal(value,field,self.vectorLayer.properties.lookup_table_map,null,0);
			if(resolution>maxResolution){
				text='';
			}else if(type=='hide'){
				text='';
			}else if(type=='shorten'){
				text=text.trunc(32);
			}else if(type=='wrap'){
				text=main.utilities.stringDivider(text,16,'\n');
			}
			return text;
		};
		this.removeLabels=function(){
			this.vectorLayer.style.setStyleFunction();
		};
		this.addLabels=function(labelField){
			self.labelField=labelField;
			main.utilities.setCurrentVals(main.labels.labelForm,self.params);
			this.vectorLayer.style.setStyleFunction();
		}
		this.setParams();
	};
});