define(function(require){
	return function pg(args){
		var self=this;
		var Accordian=require('./Accordian');
		var Button=require('./Button');
		var Checkbox=require('./Checkbox');
		var Geocoder=require('./Geocoder');
		var Input=require('./Input');
		// this.reference=reference;
		this.formProblems={problems:{}};
		this.currentParams=[];
		// this.currentExtras={};
		this.fieldCurrentVals={};
		this.tables={};
		this.dontSendEmail=false;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.layerName=this.properties.name;
		this.submit_text=this.submit_text || 'Submit';
		// (function(params){
			// for(var key in params){
				// self[key]=params[key];
			// }
		// })(params);
		// this.setFields=function(){
			// for(var key in params){
				// this[key]=params[key];
			// }
		// };
		// if(params){this.setParams();}
		this.sortTableEles=function(object){
			var keys=Object.keys(object);
			keys.sort(function(a,b){
				if(object[a].place_in_order>object[b].place_in_order){return 1;}
				if(object[a].place_in_order<object[b].place_in_order){return -1;}
				return 0;
			});
			return keys;
		}
		this.printPreview=function(){
		};
		this.createForm=function(){
			accordian=self.forms.accordian;
			if(self.properties.groups){
				groups=self.properties.groups;
				thisGroup=self.forms.groups['mainGenForms'];
				if(main.personal_settings.forms_settings.groups){
					var setGroups=main.personal_settings.forms_settings.groups;
				}
				for(var i=0;i<groups.length;i++){
					groupName=groups[i];
					if(setGroups){setGroup=setGroups[groupName];}
					if(setGroup){alias=setGroup.alias;}else{alias=groups[i];}
					if(setGroup){tooltip=setGroup.tooltip;}else{tooltip=groups[i];}
					if(!thisGroup.shelves[groupName]){
						var launchButton=new Button('toolBox','',alias,null,null,null,null,tooltip);
						accordian=new Accordian(groupName);
						shelf=thisGroup.accordian.createShelf({
							name:groupName,
							button:launchButton.html,
							content:accordian.container,
							alias:alias,
							place_in_order:self.properties.place_in_order
						});
						thisGroup.shelves[groupName]={
							launchButton:launchButton,
							accordian:accordian,
							shelf:shelf,
							shelves:{}
						}
					}else{
						accordian=thisGroup.shelves[groupName].accordian;
					}
					thisGroup=thisGroup.shelves[groupName];
				}
			}
			self.accordian=accordian;
			self.sidePanel=self.forms.sidePanel;

			
			
			
			this.html=$('<form class="form dataUnit">').data('id',self.properties.name);
			var table=$('<table>').addClass('formTable');
			table.append(this.header=$('<thead>'));
			table.append(this.tbody=$('<tbody>'));
			this.clearFormButton=new Button('std','clearForm formTopButton','Reset');
			this.header.append($('<tr>')
				.append($('<th colspan="2">')
					.append($('<div>').addClass('tdLiner formTopButtons')
						.append(this.clearFormButton.html)
					)
				)
			)
			if(this.properties.title){
				this.header.append($('<tr>')
					.append($('<th colspan="2">')
						.append($('<div>').addClass('tdLiner formTitle').html(this.properties.title))
					)
				)
			}
			if(this.properties.pre_form){
				this.tbody.append($('<tr>')
					.append($('<td colspan="2">')
						.append($('<div>').addClass('tdLiner formDesc').html(this.properties.pre_form))
					)
				)
			}
			if(this.properties.form_when || this.properties.form_where || this.properties.form_why || this.properties.form_cost || this.properties.form_your_job || this.properties.form_next_step || this.properties.form_questions){
				var detailsTBody=$('<tbody>');
				var detailsTable=$('<table>').append(detailsTBody);
				this.tbody.append($('<tr>')
					.append($('<td colspan="2">')
						.append($('<div>').addClass('tdLiner formDetailsTable').html(detailsTable))
					)
				)
				if(this.properties.form_when){
					detailsTBody.append($('<tr>')
						.append($('<td>')
							.append($('<div>').addClass('tdLiner formDetailsLabel').html('When:'))
						)
						.append($('<td>')
							.append($('<div>').addClass('tdLiner formDetails').html(this.properties.form_when))
						)
					)
				}
				if(this.properties.form_where){
					detailsTBody.append($('<tr>')
						.append($('<td>')
							.append($('<div>').addClass('tdLiner formDetailsLabel').html('Where:'))
						)
						.append($('<td>')
							.append($('<div>').addClass('tdLiner formDetails').html(this.properties.form_where))
						)
					)
				}
				if(this.properties.form_why){
					detailsTBody.append($('<tr>')
						.append($('<td>')
							.append($('<div>').addClass('tdLiner formDetailsLabel').html('Why:'))
						)
						.append($('<td>')
							.append($('<div>').addClass('tdLiner formDetails').html(this.properties.form_why))
						)
					)
				}
				if(this.properties.form_cost){
					detailsTBody.append($('<tr>')
						.append($('<td>')
							.append($('<div>').addClass('tdLiner formDetailsLabel').html('Cost:'))
						)
						.append($('<td>')
							.append($('<div>').addClass('tdLiner formDetails').html(this.properties.form_cost))
						)
					)
				}
				if(this.properties.form_your_job){
					detailsTBody.append($('<tr>')
						.append($('<td>')
							.append($('<div>').addClass('tdLiner formDetailsLabel').html('Your Job:'))
						)
						.append($('<td>')
							.append($('<div>').addClass('tdLiner formDetails').html(this.properties.form_your_job))
						)
					)
				}
				if(this.properties.form_next_step){
					detailsTBody.append($('<tr>')
						.append($('<td>')
							.append($('<div>').addClass('tdLiner formDetailsLabel').html('Next Step:'))
						)
						.append($('<td>')
							.append($('<div>').addClass('tdLiner formDetails').html(this.properties.form_next_step))
						)
					)
				}
				if(this.properties.form_questions){
					detailsTBody.append($('<tr>')
						.append($('<td>')
							.append($('<div>').addClass('tdLiner formDetailsLabel').html('Questions:'))
						)
						.append($('<td>')
							.append($('<div>').addClass('tdLiner formDetails').html(this.properties.form_questions))
						)
					)
				}
			}
			if(this.id)this.html.attr('id','app_form_'+this.id);
			if(this.fields && Object.keys(this.fields).length>0){
				var field,labelTD,inputTD;
				this.sortedFieldKeys=main.utilities.sortFields(this.fields,'place_in_order');
				for(var i=0;i<this.sortedFieldKeys.length;i++){
					field=this.fields[this.sortedFieldKeys[i]];
					self.fieldCurrentVals[this.sortedFieldKeys[i]]={};
					if(field.active){
						if((!main.account || field[main.account.loggedInUser.user_type+'_editable']) && field.in_form){
							var setToVal=null;
							if(this.reference && self.fieldMap && this.reference[self.fieldMap[field.name]]){
								setToVal=this.reference[self.fieldMap[field.name]].current_val;
							}
							self.fieldCurrentVals[this.sortedFieldKeys[i]].current_val=setToVal;
							field.input=new Input({
								field:field,
								setToVal:setToVal,
								is_in_form:true,
								layerName:self.properties.name
							});
							if(field.start_table){
								var keys=self.sortTableEles(field.table_headers);
								self.currentTableID=main.utilities.generateUniqueID();
								var tableID=field.table_id || self.currentTableID;
								self.tables[self.currentTableID]={
									table:$('<table id="'+tableID+'" class="inFormTable" data-id="'+self.currentTableID+'">'),
									info:field.table_headers,
									keys:self.sortTableEles(field.table_headers),
									subSections:{},
									id:self.currentTableID,
									sums:{}
								};
								var visibleColumns=[];
								for(var t=0;t<self.tables[self.currentTableID].keys.length;t++){
									if(main.utilities.eleToBool(self.tables[self.currentTableID].info[self.tables[self.currentTableID].keys[t]][main.account.loggedInUser.user_type+'_editable'])!==false){
										visibleColumns.push(self.tables[self.currentTableID].keys[t]);
									}
								}
								self.tables[self.currentTableID].visibleColumns=visibleColumns;
								if(field.start_sub_sec){
									self.tables[self.currentTableID].tbody=$('<tbody>');
								}else{
									self.tables[self.currentTableID]=$('<tbody>');
								}
								if(field.title){
									self.tables[self.currentTableID].headerTitle=$('<thead>')
										.append('<tr class="inFormTitle"><th colspan="99999">'+field.title+'</th></tr>');
										self.tables[self.currentTableID].table.append(self.tables[self.currentTableID].headerTitle);
								}
								self.tables[self.currentTableID].header=$('<thead>');
								self.tables[self.currentTableID].table.append(self.tables[self.currentTableID].header);
								if(field.table_title){
									self.tables[self.currentTableID].header.append($('<tr>')
										.append($('<th colspan="2">').html(field.table_title)));
								}
								if(field.table_headers){
									self.tables[self.currentTableID].headerRow=$('<tr>');
									for(var key in field.table_headers){
										if(main.utilities.eleToBool(self.tables[self.currentTableID].info[key][main.account.loggedInUser.user_type+'_editable'])!==false){
											self.tables[self.currentTableID].headerRow.append($('<th class="tableHeader">').html(field.table_headers[key].alias).data('column',key))
										}
									}
									self.tables[self.currentTableID].header.append(self.tables[self.currentTableID].headerRow);
								}
								self.tables[self.currentTableID].table.append(self.tables[self.currentTableID].tbody);
							}
							if(!field.omit_alias){
								var desc='',req='';
								if(field.description){desc=field.description;}
								if(!main.account || field[main.account.loggedInUser.user_type+'_required']){
									req='<span class="reqSpan" title="Required">*</span>'
								}
								if(field.input_type=='signature'){
									labelTD='';
									inputTD=$('<td colspan="2" class="inputTD '+field.name+'">')
										.append($('<div>').addClass('tdLiner fullRowLabel').html(field.alias+req))
										.append($('<div>').addClass('formFieldDesc').html(desc))
										.append($('<div>').addClass('tdLiner').append(field.input.container))
								}else{
									labelTD=$('<td>').append(
										$('<div>').addClass('tdLiner')
											.append($('<div>').addClass('formFieldAlias').html(field.alias+req))
											.append($('<div>').addClass('formFieldDesc').html(desc))
									);
									inputTD=$('<td class="inputTD '+field.name+'">').append($('<div>').addClass('tdLiner').append(field.input.container))
								}
							}else{
								labelTD='';
								if(field.input_type=='checkbox'){
									inputTD=$('<td colspan="2" class="inputTD '+field.name+'">')
										.append($('<div>').addClass('tdLiner inline').append(field.input.container))
										.append($('<div>').addClass('tdLiner fullRowLabel inline').html(field.alias))
								}else{
									inputTD=$('<td colspan="2" class="inputTD '+field.name+'">')
										.append($('<div>').addClass('tdLiner inline').append(field.input.container))
								}
							}
							if(field.title && !field.in_table){
								this.tbody.append($('<tr class="field_title_row '+field.name+'">')
									.append($('<td colspan="2">')
										.append($('<div>').addClass('tdLiner fieldTitle').html(field.title))
									)
								);
							}
							if(field.pre_field && !field.in_table){
								this.tbody.append($('<tr class="inFormPreField '+field.name+'">')
									.append($('<td colspan="2">')
										.append($('<div>').addClass('tdLiner fieldPreField').html(field.pre_field))
									)
								);
							}
							field.errorContainer=$('<div>').addClass('inputErrorContainer');
							inputTD.find('.tdLiner').append(field.errorContainer);
							if(field.in_table && self.tables[self.currentTableID]){
								if(field.table_column==self.tables[self.currentTableID].keys[0]){
									if(field.start_sub_sec){
										self.currentSubSectionID=main.utilities.generateUniqueID();
										self.tables[self.currentTableID].subSections[self.currentSubSectionID]={
											id:self.currentSubSectionID,
											tbody:$('<tbody class="inFormTableSubSec" data-id="'+self.currentSubSectionID+'">'),
											sums:{}
										}
										self.tables[self.currentTableID].currentSubSec=self.tables[self.currentTableID].subSections[self.currentSubSectionID];
										if(field.table_sub_header){
											self.tables[self.currentTableID].subSections[self.currentSubSectionID].subHeader=$('<thead>').append($('<th class="subSecHeader" colspan="99999">').html(field.table_sub_header));
											self.tables[self.currentTableID].table.append(self.tables[self.currentTableID].subSections[self.currentSubSectionID].subHeader);
										}
										self.tables[self.currentTableID].table.append(self.tables[self.currentTableID].subSections[self.currentSubSectionID].tbody);
									}
									self.tables[self.currentTableID].currentTR=$('<tr class="inFormTableTR">');
									if(self.tables[self.currentTableID].currentSubSec){
										self.tables[self.currentTableID].currentSubSec.tbody.append(self.tables[self.currentTableID].currentTR);
									}else{
										self.tables[self.currentTableID].tbody.append(self.tables[self.currentTableID].currentTR);
									}
								}
								if(main.utilities.eleToBool(self.tables[self.currentTableID].info[field.table_column][main.account.loggedInUser.user_type+'_editable'])!==false){
									if(field.is_table_label){inputTD.addClass('formLabel')}
									self.tables[self.currentTableID].currentTR.append(inputTD.addClass(field.table_column).data('column',field.table_column));
								}
							}else{
								var hideClass='';
								if(field.input_type=='hidden'){hideClass=' class="none"';}
								this.tbody.append($('<tr'+hideClass+'>')
									.append(labelTD).append(inputTD)
								);
							}
							if(field.post_field){
								this.tbody.append($('<tr class="post_field_row '+field.name+'">')
									.append($('<td colspan="999999">')
										.append($('<div>').addClass('tdLiner post_field').html(field.post_field))
									)
								);
							}
							if(field.end_sub_sec){
								if(field.sum_columns){
									var sumRow=$('<tr class="sumRow">');
									var sumRowTbody=$('<tbody>');
									var column,index,blanks,curIndex=0;
									var subSecTbody=self.tables[self.currentTableID].currentSubSec.tbody;
									if(field.sub_sec_total_column_location){
										sumRow.append($('<td colspan="3" class="sumRowTD sumRowTitle '+field.sub_sec_total_column_location+'" data-column="'+field.sub_sec_total_column_location+'">').append($('<div class="tdLiner">').html(field.sub_sec_total_column_title)));
										curIndex=curIndex+3;
									}
									for(var t=0;t<field.sum_columns.length;t++){
										column=field.sum_columns[t];
										if(self.tables[self.currentTableID].visibleColumns.indexOf(column)>-1){
											index=self.tables[self.currentTableID].visibleColumns.indexOf(column);
											var sum=self.updateTableTotals(subSecTbody,column,self.currentTableID);
											self.tables[self.currentTableID].subSections[self.currentSubSectionID].sums[column]=sum;
											var blanks=index-curIndex;
											for(var v=0;v<blanks;v++){
												sumRow.append($('<td class="sumRowTD _empty" data-column="_empty">').append($('<div class="tdLiner">').html('&nbsp;')));
												curIndex++;
											}
											sumRow.append($('<td class="sumRowTD '+column+'" data-column="'+column+'">').append($('<div class="tdLiner">').html(sum.text)));
											curIndex++;
										}
									}
									/* for(var t=0;t<self.tables[self.currentTableID].keys.length;t++){
									// self.tables[self.currentTableID].headerRow.find('th').each(function(){
										column=self.tables[self.currentTableID].keys[t];
										if(field.sum_columns.indexOf(column)>-1){
											sum=0;
											self.tables[self.currentTableID].tbody.find('.'+column+' .formInput').each(function(){
												sum+=Number($(this).val());
											});
											if(self.tables[self.currentTableID].info[column].prepend_to){sum=self.tables[self.currentTableID].info[column].prepend_to+sum;}
											if(self.tables[self.currentTableID].info[column].append_to){sum+=self.tables[self.currentTableID].info[column].append_to;}
											text=sum;
										}else{
											text='';
										}
										self.tables[self.currentTableID].sumRow
											.append($('<td class="sumRowTD">').append($('<div class="tdLiner">').text(text)));
									} */
									self.tables[self.currentTableID].table.append(sumRowTbody);
									if(self.currentSubSectionID){
										self.tables[self.currentTableID].subSections[self.currentSubSectionID].sumRow=sumRow;
										self.tables[self.currentTableID].subSections[self.currentSubSectionID].sumRowTbody=sumRowTbody
											.append(self.tables[self.currentTableID].subSections[self.currentSubSectionID].sumRow);
									}
								}
								self.currentSubSectionID=null;
							}
							if(field.total_table_columns){
								self.tables[self.currentTableID].sumRow=$('<tr class="sumRow tableTotals">');
								self.tables[self.currentTableID].sumRowTbody=$('<tbody>')
									.append(self.tables[self.currentTableID].sumRow);
								self.tables[self.currentTableID].table.append(self.tables[self.currentTableID].sumRowTbody);
								var subSections=self.tables[self.currentTableID].subSections;
								var column,index,blanks,curIndex=0;
								if(field.total_table_column_location){
									self.tables[self.currentTableID].sumRow.append($('<td colspan="3" class="sumRowTD sumRowTitle '+field.total_table_column_location+'" data-column="'+field.total_table_column_location+'">').append($('<div class="tdLiner">').html(field.total_table_column_title)));
									curIndex=curIndex+3;
								}
								for(var t=0;t<field.total_table_columns.length;t++){
									column=field.total_table_columns[t];
									if(self.tables[self.currentTableID].visibleColumns.indexOf(column)>-1){
										index=self.tables[self.currentTableID].visibleColumns.indexOf(column);
										var sum=self.updateTotalTableTotals(subSections,column,self.currentTableID);
										self.tables[self.currentTableID].sums[column]=sum;
										var blanks=index-curIndex;
										for(var v=0;v<blanks;v++){
											self.tables[self.currentTableID].sumRow.append($('<td class="sumRowTD _empty" data-column="_empty">').append($('<div class="tdLiner">').html('&nbsp;')));
											curIndex++;
										}
										self.tables[self.currentTableID].sumRow.append($('<td class="sumRowTD '+column+'" data-column="'+column+'">').append($('<div class="tdLiner">').html(sum.text)));
										curIndex++;
									}
								}
							}
							if(field.end_table){
								self.tables[self.currentTableID].tableWrap=$('<tr class="inFormTableWrap">').append(
									$('<td colspan="2">').append($('<div class="tdLiner">').append(self.tables[self.currentTableID].table)));
								self.tbody.append(self.tables[self.currentTableID].tableWrap);
								// self.tables[self.currentTableID]=null;
							}
						}
					}
				}
				if(this.post_form){
					this.tbody.append($('<tr>')
						.append($('<td colspan="2">')
							.append($('<div>').addClass('tdLiner').html(this.post_form))
						)
					);
				}
				var formConcExtras=$('<div>').addClass('tdLiner formConcExtras');
				if(self.properties.send_email_prompt){
					this.send_email_prompt_check=new Checkbox({
						label:'Send E-Mail On Submit',
						value:'true',
						checked:true,
						name:'send_email_prompt',
						classes:'elToggleAll'
					});
					formConcExtras.append(this.send_email_prompt_check.html);
				}
				var formConcButtons=$('<div>').addClass('tdLiner center formConcButtons');
				var formConcWrap=$('<div>').addClass('tdLiner formConcWrap')
					.append(formConcExtras)
					.append(formConcButtons);
				if(self.properties.prev_pdf){
					this.printPDFPreviewButton=new Button('std','printPreview formConcButton','Preview PDF');
					formConcButtons.append(this.printPDFPreviewButton.html);
				}
				if(self.properties.print_preview){
					self.printAfterSubmitCheck=new Checkbox({
						value:'true',
						label:'Save/Print after Submit',
						classes:'formPrintAfterSubmitCheck',
						wrapClasses:'formPrintAfterSubmitCheckWrap'
					});
					var formConcChecks=$('<div>').addClass('tdLiner formConcChecks');
					formConcChecks.append(self.printAfterSubmitCheck.html);
					formConcWrap.prepend(formConcChecks);
				}
				if(self.properties.save_pdf){
					this.saveAsPDFButton=new Button('std','saveFormAsPDF formConcButton','Save as PDF');
					formConcButtons.append(this.saveAsPDFButton.html);
				}
				if(self.properties.save_excel){
					this.saveAsXLSButton=new Button('std','saveFormAsExcel formConcButton','Save XLSX File');
					formConcButtons.append(this.saveAsXLSButton.html);
				}
				
				formConcButtons.append($('<input type="submit" class="button submit formConcButton" value="'+this.submit_text+'">'));
				this.tbody.append($('<tr>')
					.append($('<td colspan="2">')
						.append(formConcWrap)
					)
				);
				//setfields
				// (this.setFields=function(){
				self.specialFields={};
				if(self.properties.internal_field_map){
					if(self.properties.internal_field_map.street_num_field_1 && self.fields[self.properties.internal_field_map.street_num_field_1]){
						self.specialFields.street_num=self.fields[self.properties.internal_field_map.street_num_field_1].input.container.filter('.formInput').find('.formInput').andSelf();
					}
					if(self.properties.internal_field_map.address_field_1 && self.fields[self.properties.internal_field_map.address_field_1]){
						self.specialFields.address_1=self.fields[self.properties.internal_field_map.address_field_1].input.container.filter('.formInput').find('.formInput').andSelf();
					}
					if(self.properties.internal_field_map.zip_field_1 && self.fields[self.properties.internal_field_map.zip_field_1]){
						self.specialFields.zip=self.fields[self.properties.internal_field_map.zip_field_1].input.container.filter('.formInput').find('.formInput').andSelf();
					}
					if(self.properties.internal_field_map.geometry_field && self.fields[self.properties.internal_field_map.geometry_field]){
						self.specialFields.geometry=self.fields[self.properties.internal_field_map.geometry_field].input.container.filter('.formInput').find('.formInput').andSelf();
					}
				}
				// })();
				if(self.properties.internal_field_map && self.properties.internal_field_map.locator_field_1){
					var callback=null;
					if(self.properties.geocoder_1_callback){
						callback=self.properties.geocoder_1_callback;
					}
					self.properties.geocoder_1=new Geocoder({
						map:main.map,
						context:self,
						callback:callback
					});
					this.tbody.find('.formInput.'+self.properties.internal_field_map.locator_field_1).closest('.tdLiner').append(self.properties.geocoder_1.html);
				}
				if(this.extended)this.tbody.prepend($('<col width="280">'));
				this.html.append(table);
			}
		};
		this.updateTotalTableTotals=function(subSections,column,tableID){
			var sum=0;
			for(var key in subSections){
				sum+=subSections[key].sums[column].number;
			}
			var number=sum;
			if(self.tables[tableID].info[column].to_fixed){
				sum=sum.toFixed(Number(self.tables[tableID].info[column].to_fixed));
			}
			if(self.tables[tableID].info[column].prepend_to){sum=self.tables[tableID].info[column].prepend_to+sum;}
			if(self.tables[tableID].info[column].append_to){sum+=self.tables[tableID].info[column].append_to;}
			return {
				text:sum,
				number:number
			};
		};
		this.updateTableTotals=function(subSecTbody,column,tableID){
			var sum=0;
			subSecTbody.find('.inFormTableTR').each(function(){
				sum+=Number($(this).find('.inputTD.'+column+' .formInput').val());
			});
			var number=sum;
			if(self.tables[tableID].info[column].to_fixed){
				sum=sum.toFixed(Number(self.tables[tableID].info[column].to_fixed));
			}
			if(self.tables[tableID].info[column].prepend_to){sum=self.tables[tableID].info[column].prepend_to+sum;}
			if(self.tables[tableID].info[column].append_to){sum+=self.tables[tableID].info[column].append_to;}
			return {
				text:sum,
				number:number
			};
		};
		this.generateExcel=function(saveMethod,callback){
			var dataPkg=main.utilities.createFilePKG(self.fields);
			var title="";
			if(this.file_title){title=this.file_title;
			}else if(this.title){title=this.title;}
			if(saveMethod=='saveToServer' && self.properties.generate_excel_action){
				$.ajax({
					type:'POST',
					url:main.mainPath+'server/excel.php',
					data:{
						dataPkg:JSON.stringify(dataPkg),
						keys:JSON.stringify(self.sortedFieldKeys),
						title:title,
						headerImg:self.properties.header_img,
						saveMethod:saveMethod,
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						action:self.properties.generate_excel_action
					},
					success:function(response){
						if(response){
							response=JSON.parse(response);
							if(response.status=="OK"){
								self.formsLoaded++;
								if(self.properties.email_callback){
									main.utilities.callFunctionFromName(self.properties.email_callback,window,{dataPkg:dataPkg,context:self});
								}
								self.currentParams.push({title:title,path:response.path,type:'xlsx'});
								if(callback){callback();}
							}else{
								if(self.areFCreatedTO){clearTimeout(self.areFCreatedTO);}
								alert('Error');
								console.log(response);
							}
						}
					}
				});
			}else{
				var form=$('<form>',{
					action:main.mainPath+'server/excel.php',
					method:'POST'
				});
				var pkg=$('<input type="hidden" name="dataPkg"/>').val(JSON.stringify(dataPkg));
				var keys=$('<input type="hidden" name="keys"/>').val(JSON.stringify(self.sortedFieldKeys));
				form.append('<input type="hidden" name="title" value="'+title+'"/>');
				if(saveMethod=='saveToLocal'){form.append('<input type="hidden" name="fileName" value="'+title.replace(/ /g,'_').replace(/\W+/g,"")+'.xlsx"/>');}
				if(this.header_img){
					form.append('<input type="hidden" name="headerImg" value="'+this.header_img+'"/>');
				}
				form.append('<input type="hidden" name="saveMethod" value="'+saveMethod+'"/>');
				form.append('<input type="hidden" name="action" value="'+self.properties.generate_excel_action+'"/>');
				form.append(pkg);
				form.append(keys);
				$('body').append(form);
				form.get(0).submit();
				form.remove();
			}
		};
		this.generatePDF=function(saveMethod,callback){
			var dataPkg=main.utilities.createFilePKG(self.fields);
			if(this.file_title){title=this.file_title;
			}else if(this.title){title=this.title;}
			if(saveMethod=='saveToServer' && self.properties.generate_pdf_action){
				$.ajax({
					type:'POST',
					url:main.mainPath+'server/pdf.php',
					data:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						dataPkg:dataPkg,
						keys:JSON.stringify(self.sortedFieldKeys),
						title:title,
						headerImg:self.properties.header_img,
						saveMethod:saveMethod,
						action:self.properties.generate_pdf_action
					},
					success:function(response){
						if(response){
							response=JSON.parse(response);
							if(response.status=="OK"){
								self.formsLoaded++;
								if(self.properties.email_callback){
									main.utilities.callFunctionFromName(self.properties.email_callback,window,{data:dataPkg,context:self});
								}
								self.currentParams.push({title:title,path:response.path,type:'pdf'});
								if(callback){callback();}
							}else{
								if(self.areFCreatedTO){clearTimeout(self.areFCreatedTO);}
								alert('Error');
								console.log(response);
							}
						}
					}
				});
			}else{
				var form=$('<form>',{
					action:main.mainPath+'server/pdf.php',
					method:'POST'
				});
				var pkg=$('<input type="hidden" name="dataPkg"/>').val(JSON.stringify(dataPkg));
				var keys=$('<input type="hidden" name="keys"/>').val(JSON.stringify(self.sortedFieldKeys));
				form.append('<input type="hidden" name="title" value="'+title+'"/>');
				if(saveMethod=='saveToLocal'){form.append('<input type="hidden" name="fileName" value="'+title.replace(/ /g,'_').replace(/\W+/g,"")+'.pdf"/>');
				}else{form.attr('target','_blank');}
				if(this.header_img){
					form.append('<input type="hidden" name="headerImg" value="'+this.header_img+'"/>');
				}
				form.append('<input type="hidden" name="saveMethod" value="'+saveMethod+'"/>');
				form.append('<input type="hidden" name="action" value="'+self.properties.generate_pdf_action+'"/>');
				form.append(pkg);
				form.append(keys);
				$('body').append(form);
				form.get(0).submit();
				form.remove();
			}
		};
		this.saveFormAsPDF=function(){
			self.generatePDF('saveToLocal');
		};
		this.showPrintPDFPreview=function(){
			self.generatePDF();
		};
		this.saveFormAsExcel=function(){
			self.generateExcel('saveToLocal');
		};
		this.populateAutoLoadLasts=function(){
			for(var key in main.formsMod.auto_load_last_fields){
				main.utilities.setValToInput(self.html.find('.formInput.'+key),main.formsMod.auto_load_last_fields[key],self.fields[key]);
				self.html.find('.formInput.'+key).change();
			}
		};
		this.clearForm=function(){
			for(var key in self.fields){
				if(self.fields[key].active && self.fields[key].input){
					if(self.fields[key].input_type=='signature'){
						self.fields[key].input.signature.clearCanvas();
					}
					if(self.fields[key].input_type=='map_point' || self.fields[key].input_type=='map_polygon'){
						self.fields[key].input.mapPoint.clear();
					}
					if(self.fields[key].input_type=='checkbox' || self.fields[key].input_type=='radio'){
						self.fields[key].input.container.find('input[type="checkbox"],input[type="radio"]').andSelf().prop('checked',false);
						self.fields[key].input.container.find('input[type="checkbox"],input[type="radio"]').andSelf().filter('input[value="'+self.fields[key].default_val+'"]').prop('checked',true);
					}else if(self.fields[key].input_type=='availabilitySelect'){
						self.fields[key].input.availabilitySelect.refresh();
						self.fields[key].input.container.find('.formInput').andSelf().filter('.formInput').val(self.fields[key].default_val);
					}else{
						self.fields[key].input.container.find('.formInput').andSelf().filter('.formInput').val(self.fields[key].default_val);
					}
					if(self.properties.internal_field_map && self.properties.internal_field_map.locator_field_1==key){
						self.properties.geocoder_1.clear();
					}
				}
			}
			if(self.printAfterSubmitCheck){self.printAfterSubmitCheck.checkbox.prop('checked',false);}
			// self.fields[key].input.container.find('.formInput').andSelf().filter('.formInput').change();
		};
		this.areFormsCreated=function(){
			if(self.formsLoaded==self.formsToLoad){
				self.currentCallBack()
			}else{
				self.areFCreatedTO=setTimeout(function(){
					self.areFormsCreated();
				},100);
			}
		},
		this.formSubmit=function(){
			if(main.account){
				var status=self.formValidate();
				if(status!='OK'){alert('Please correct the errors highlighted in red before proceeding.'); return;}
			}
			if(self.properties.send_conf_email || (self.properties.send_email_prompt && self.properties.send_email_prompt_check.checkbox.prop('checked'))){
				self.formsLoaded=0;
				self.formsToLoad=0;
				self.currentCallBack=self.submitForm;
				self.currentParams=[];
				if(self.properties.email_pdf){
					self.generatePDF('saveToServer',self.areFormsCreated);
					self.formsToLoad++;
				}
				if(self.properties.email_excel){
					self.generateExcel('saveToServer',self.areFormsCreated);
					self.formsToLoad++;
				}
				if(self.formsToLoad==0){self.submitForm();}
			}else{
				self.submitForm();
			}
		};
		this.updatePID=function(inv_pid,form_pid){
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						inv_pid:inv_pid,
						form_pid:form_pid,
						pid_name:self.properties.internal_field_map.inv_pid,
						name:self.properties.name
					},
					action:'updateInvID'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							var data={pid:form_pid};
							data[self.properties.internal_field_map.inv_pid]=inv_pid;
							if(self.dataTable && self.dataTable.loaded){self.dataTable.updateRow(data);}
						}
					}
				}
			});
		};
		this.submitForm=function(){
			var formPkg=main.utilities.getFormPkg(self.fields,true);
			if(self.properties.auto_load_last_fields){
				main.formsMod.auto_load_last_fields={};
				for(var i=0;i<self.properties.auto_load_last_fields.length;i++){
					main.formsMod.auto_load_last_fields[self.properties.auto_load_last_fields[i]]=formPkg[self.properties.auto_load_last_fields[i]].value;
				}
			}
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						fields:formPkg,
						name:self.properties.name
					},
					action:'submitForm'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							if(response.availSelUpdated && main.invEditor){
								main.invEditor.refresh();
							}
							if(response.results[0]){
								if(self.printAfterSubmitCheck && self.printAfterSubmitCheck.checkbox.prop('checked')){main.print.printRow({pid:response.results[0]['pid'],table:self.properties.name});}
								if(self.dataTable && self.dataTable.loaded){
									self.dataTable.sideReset();
								}
								if(self.properties.field_map){
									if(self.properties.new_inv_item && main.invents.inventories[self.properties.new_inv_item]){
										//main.invents.inventories[self.properties.new_inv_item].addItemFromForm(response.results[0],self.properties.field_map[self.properties.new_inv_item].fields,self);
									}
									if(self.properties.update_on_submit){
										var pid,newFormPkg,tempFields,newFields,fieldsMap,fieldsList,recordPkgs;
										for(var key in self.properties.update_on_submit){
											if(self.properties.field_map[key]){
												main.utilities.updateTable(key,self.properties.field_map[key].fields,self.properties.update_on_submit[key].link_column_from,formPkg);
												/* tempFields={};
												newFields=main.tables[key].fields;
												fieldsMap=self.field_map[key].fields;
												fieldsList=[];
												for(var key2 in fieldsMap){
													if(newFields[key2]){
														tempFields[key2]=newFields[key2];
														tempFields[key2].current_val=formPkg[fieldsMap[key2]].value;
														fieldsList.push(key2);
													}
												}
												newFormPkg=main.utilities.getFormPkg(tempFields);
												pid=formPkg[self.update_on_submit[key].link_column_from].value;
												recordPkgs={};
												recordPkgs[pid]=newFormPkg
												$.ajax({
													type:'POST',
													url:main.mainPath+'server/db.php',
													data:{
														params:{
															fields:fieldsList,
															recordPkgs:recordPkgs,
															table:key
														},
														action:'updateDataEntry'
													},
													success:function(response){
														if(response){
															response=JSON.parse(response);
															if(response.status=="OK"){
																var table=response.params.table;
																if(main.tables[table].map_layer.features[pid] && main.tables[table].map_layer.features[pid].dataTable){console.log(recordPkgs[pid]);
																	// main.tables[table].map_layer.features[pid].dataTable.updateDataTable(pid,fieldName,primaryE,false,true,true);
																	var data=recordPkgs[pid];
																	var tempData={};
																	for(var key in data){tempData[key]=data[key].value;}
																	tempData.pid=pid;
																	main.tables[table].map_layer.features[pid].dataTable.updateRow(tempData);
																	// self.dataTable.updateData([pid]);
																}
																if(main.filter_legend && main.filter_legend.active){main.filter_legend.refresh();}
															}else{
																alert('Error');
																console.log(response);
															}
														}
													}
												}); */
											}
										}
									}
								}
							}
							if((!self.dontSendEmail && self.properties.send_conf_email) || (self.properties.send_email_prompt && self.send_email_prompt_check.checkbox.prop('checked'))){self.sendConfirmationEmail(formPkg);}
							if(self.properties.submission_response){alert(self.properties.submission_response);}
							else{alert('Form submitted successfully.');}
							self.clearForm();
							self.populateAutoLoadLasts();
							if(self.properties.close_after_submit){
								if(self.sidePanel){self.sidePanel.close();}
								if(self.accordian){self.accordian.close(self.shelf.data('name'));}
							}
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.formInputChanged=function(fieldName,val){
			main.utilities.setCurrentVals(self.html,self.fields);
			if(self.properties.special_connections && self.properties.special_connections[fieldName]){
				if(self.properties.special_connections[fieldName].type=='multiply_by'){
					var sp=self.properties.special_connections[fieldName];
					self.html.find('.formInput.'+sp.to_effect).val(Number(sp.mult_by)*Number(val)).change();
				}
			}
		};
		this.initEvents=function(){
			this.html.on('change','.formInput',function(){
				self.formInputChanged($(this).data('name'),$(this).val());
			});
			this.html.on('change','.inFormTable .formInput',function(){
				var tbody=$(this).closest('tbody');
				var column=$(this).closest('.inputTD').data('column');
				var tableID=$(this).closest('.inFormTable').data('id');
				var tbodyID=tbody.data('id');
				var subSections=self.tables[tableID].subSections;
				if(self.tables[tableID].subSections[tbodyID].sums[column]){
					var sum=self.updateTableTotals(tbody,column,tableID);
					self.tables[tableID].subSections[tbodyID].sums[column]=sum;
					subSections[tbodyID].sumRow.find('.sumRowTD.'+column+' .tdLiner').html(sum.text);
				}
				if(self.tables[tableID].sums[column]){
					var totalSum=self.updateTotalTableTotals(subSections,column,tableID);
					self.tables[tableID].sums[column]=totalSum;
					self.tables[tableID].sumRow.find('.sumRowTD.'+column+' .tdLiner').html(totalSum.text);
				}
			});
			if(this.saveAsPDFButton) {
                this.saveAsPDFButton.html.on('click',function(e){
                    self.saveFormAsPDF();
                });
            }
            if(this.printPDFPreviewButton) {
                this.printPDFPreviewButton.html.on('click',function(e){
                    self.showPrintPDFPreview();
                });
            }
			if(this.saveAsXLSButton) {
                this.saveAsXLSButton.html.on('click',function(e){
                    self.saveFormAsExcel();
                });
            }
            if(this.clearFormButton) {
                this.clearFormButton.html.on('click',function(e){
                    self.clearForm();
                });
            }
			this.html.on('submit',function(e){
				e.preventDefault();
				self.formSubmit();
			});
		};
		this.sendConfirmationEmail=function(formPkg){
			if(self.properties.confirm_email_action){
				var emails=[];
				if(self.properties.internal_field_map && self.properties.internal_field_map.email_1 && self.fields[self.properties.internal_field_map.email_1].current_val){emails.push(self.fields[self.properties.internal_field_map.email_1].current_val);
				}
				if(self.properties.default_conf_email){emails.push(self.properties.default_conf_email);}
				// if(!emails.length){ return;}
				var title=this.file_title || this.title || '';
				$.ajax({
					type:'POST',
					url:main.insFolderName+'server/custom.php',
					data:{
						params:{
							folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
							atchs:self.currentParams,
							email:emails,
							title:title,
							body:self.properties.email_body,
							email_variation_1:self.properties.email_variation_1,
							fields:formPkg
						},
						action:self.properties.confirm_email_action
					},
					success:function(response){
						if(response){
							response=JSON.parse(response);
							if(response.status=="OK"){
								// alert(self.properties.submission_response);
							}else{
								// alert('Error');
								console.log(response);
							}
						}
					}
				});
			}else{
				// var title=this.file_title || this.title || '';
				var email_body=self.properties.email_body;
				$.ajax({
					type:'POST',
					url:main.mainPath+'server/db.php',
					data:{
						params:{
							folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
							atchs:null,
							title:null,
							body:email_body
						},
						action:'sendConfEmail'
					},
					success:function(response){
						if(response){
							response=JSON.parse(response);
							if(response.status=="OK"){
								// alert(self.properties.submission_response);
							}else{
								// alert('Error');
								console.log(response);
							}
						}
					}
				});
			}
		};
		this.formValidate=function(){
			self.formProblems.problems={};
			$('.inputErrorContainer').removeClass('block');
			var status='OK';
			for(var key in self.fields){
				field=self.fields[key];
				if(field.active && field[main.account.loggedInUser.user_type+'_editable']){
					if(field[main.account.loggedInUser.user_type+'_required']=='value'){
						if(!field.current_val && field.current_val!==0){
							self.newProblem(field);
							status='ERROR';
						}
					}else if(field[main.account.loggedInUser.user_type+'_required']=='number'){
						if(isNaN(field.current_val)){
							self.newProblem(field);
							status='ERROR';
						}
					}else if(field[main.account.loggedInUser.user_type+'_required']=='email'){
						if(!field.current_val || field.current_val.indexOf('@')==-1){
							self.newProblem(field);
							status='ERROR';
						}
					}
				}
			}
			return status
		};
		this.newProblem=function(field){
			if(field.errorContainer){
				var errorMsg=field.required_msg;
				if(!errorMsg)errorMsg='This field is required. Please supply your input.';
				field.errorContainer.html(errorMsg).addClass('block');
				self.formProblems.problems[field.name]={
					name:field.name,
					msg:errorMsg
				};
			}
		};
		this.createForm();
		this.initEvents();
		(this.moreForm=function(){
			main.layout.forms[self.layerName]=self;
			if(self.accordian){
				self.launchButton=new Button('toolBox','',self.properties.alias);
				self.shelf=self.accordian.createShelf({
					name:self.properties.name,
					button:self.launchButton.html,
					content:self.html,
					alias:self.properties.alias,
					extended:self.properties.extended,
					withoutContent:!self.properties.with_form,
					place_in_order:self.properties.place_in_order
				});
				self.inApp=true;
				if(main.account && self.properties[main.account.loggedInUser.user_type+'_data_mng']){
					main.utilities.addDataTable(self);
				}
				if(main.account && self.properties[main.account.loggedInUser.user_type+'_uploader']){
					main.utilities.addUploader(self);
				}
			}
			if(self.properties.shortcut){
				var shortcut_name=self.properties.shortcut_name || '';
				var shortcut_attr_title=self.properties.shortcut_attr_title || '';
				var shortcut_image=self.properties.shortcut_image || '';
				main.layout.shortcuts[self.properties.name]=main.layout.header.createHeaderButton('iconButton shortCutButton '+self.properties.name,shortcut_name,[['button',self.sidePanel.container.data('button')],['accordian',self.accordian.id],['form',self.properties.name]],'images/'+shortcut_image,shortcut_attr_title);
				self.shortcut=main.layout.shortcuts[self.properties.name];
				main.layout.header[self.properties.shortcut].append(main.layout.shortcuts[self.properties.name].html);
				main.layout.shortcuts[self.properties.name].html.click(function(){
					main.utilities.openForm($(this).data('form'),$(this).data('accordian'),true);
				});
			}
			// if(self.properties.extended){
				// self.html.closest('.shelf').addClass('extended')
			// }
		})();
		// if( self.curent_variation ) {
            // this.updateVariation();
		// }
		main.utilities.setCurrentVals(self.html,self.fields);
	};
});