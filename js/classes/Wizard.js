define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Panel=require('./Panel');
		var Range=require('./Range');
		var Input=require('./Input');
		var Wait=require('./Wait');
		//var Utilities = require('./Utilities');
		this.setToApp=false;
		this.alias='Wizard';
		this.addAccShelf=true;
		this.items={};
		this.wizard=main.data.wizard;
		this.inSettings=true;
		this.minOnOutClick=true;
		this.id=main.utilities.generateUniqueID();
		this.activePanel='SelectType';
		
		this.defaultRangeMin=0;
		this.defaultRangeMax=50;
		this.defaultWeight=6.66;
		this.defaultWeightMin=0;
		this.defaultWeightMax=9.99;
		this.defaultWeightStep=3.33;
		this.defaultWeightStdDev=3;
		this.defaultWeightColorGrad='Green-Red';
		this.relativeValues=['Low Priority','Medium Priority','High Priority'];
		this.weightLabelAlias='Site Suitability';
		
		this.mainPanel={};
		this.panelIntro={};
		this.panelType={};
		this.panelScale={};
		this.panelData={};
		this.panelLabel={};
		this.panelPrint={};
		this.panelData.fields={};
		this.panelData.plan_ranges={};
		this.panelData.view_ranges={};
		this.panelData.weight_ranges={};
		
		defaultType='';
		defaultScale='';
		defaultData='';

		defaultLabel='';
		defaultLabelFill='rgba(255, 255, 255, 0.9)';
		defaultLabelOutline='rgba(0, 0, 0, 0.9)';
		defaultLabelFont='14';
		defaultLabelWeight='Normal';
		defaultlabelYes=false;
		
		defaultFileType='JPG';
		defaultTitle='';
		defaultLegendYes=true;
		defaultScaleYes=true;
		defaultFileName='Plotter_Map';
		
		currentType=defaultType;
		currentScale=defaultScale;
		currentData=defaultData;
		
		currentLabel=defaultLabel;
		currentLabelFill=defaultLabelFill;
		currentLabelOutline=defaultLabelOutline;
		currentLabelFont=defaultLabelFont;
		currentLabelWeight=defaultLabelWeight;
		currentlabelYes=defaultlabelYes;
		
		currentFileType=defaultFileType;
		currentTitle=defaultTitle;
		currentLegendYes=defaultLegendYes;
		currentScaleYes=defaultScaleYes;
		currentFileName=defaultFileName;
		
		this.labelSettings=main.utilities.cloneObj(main.data.std_settings.labels);
		
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.createShelfContent=function(){
			self.showWizardButton=new Button('std','showWizard showButton','Show');
			self.hideWizardButton=new Button('std','hideWizard hideButton','Hide');
			var table=$('<table>')
				.append($('<tbody>')
					.append($('<tr>')
						.append($('<th>')
							.append($('<td>')
								.append($('<div>').addClass('tdLiner')
									.append(self.showWizardButton.html)
									.append(self.hideWizardButton.html)
								)
							)
						)
					)
				);
			return table
		};
		canopyLayerLoaded=function(){
			changePanel("Type");
		};
		this.show=function(){
			self.panel.open();
			main.canopy.blockDeactivate=true;
			if($(".wizardWindow ").find("#wizardRestartLink").length>0){ // the restart link is already present
				//do Nothing
			}
			else{
				$(".wizardWindow ").find(".panelBody").append(self.mainPanel.restartLink);
			}
			// self.updateButtonsShow();
		};
		this.hide=function(){
			self.panel.minimizePanel();
			main.canopy.blockDeactivate=false;
			// self.updateButtonsHide();
		};
		panelOpened=function(){
			if(self.panel.opened){return true;}
			else{return false;}
		};
		addRefresh=function(){
			if($(".wizardWindow ").find("#wizardRestartLink").length>0){ // the restart link is already present
				//do Nothing
			}
			else{
				$(".wizardWindow ").find(".panelBody").append(self.mainPanel.restartLink);
			}
		};
		if(self.addAccShelf){
			this.updateButtonsShow=function(){
				self.showWizardButton.html.removeClass('inlineBlock');
				self.hideWizardButton.html.removeClass('none');
				main.canopy.blockDeactivate=true;
				if(!main.canopy.view.hidecheckBox.checkbox.prop('checked')){
					main.canopy.view.hidecheckBox.checkbox.click();
				}
				var waitForRefresh=new Wait(panelOpened,addRefresh);
				
				
				//self.show();
			};
			this.updateButtonsHide=function(){
				self.showWizardButton.html.addClass('inlineBlock');
				self.hideWizardButton.html.addClass('none');
				main.canopy.blockDeactivate=false;
				if(main.canopy.view && main.canopy.view.hidecheckBox && main.canopy.view.hidecheckBox.checkbox.prop('checked')){
					main.canopy.view.hidecheckBox.checkbox.click();
				}

				//self.hide();
			}; 
		};

		
		//Dropdown option from Panel Type was selected
		selectType=function(type){
			id="."+type+"Tab";
			$(id).click() 	
			currentType=type;
			resetLabels();
		};
		//Dropdown option from Panel Scale was selected
		selectArea=function(area){
			if(currentType=='view'){
				$(".ViewSelectLayer").val(area).change();
			}
			else{
				$(".planSelectLayer").val(area).change();
			}
			resetLabels();
			currentScale=area;
		};
		
		//Dropdown Option from Panel Data was selected
		selectData=function(val){
			if(currentType=='view'){
				if(val==main.canopy.currentField){
					//already selected Do Nothing 
				}
				else{
					if(main.canopy.currentLayer.fields[val].groups== null){
						//Not in a group just need to select
						$("."+val).click();
					}
					else{
						//in a group need to check if current visible
						if($(".rangePkgGroupSelect").val()==val){
							//currently visible
							$("."+val).click();
						}
						else{
							$(".rangePkgGroupSelect").val(val).change();
						}
					}
				}
			}
			else{
				//Do Nothing
			}
			resetLabels();
			currentData=val;
		}
		toggleLables=function(){
			
			main.labels.udpateLabels();
				
		};
		//Dropdown Option from Panel Label was selected
		selectLabel=function(val){
			$(".mainLabelFieldSelect").val(val).change();
			currentLabel=val;
			toggleLables();
		};
		//Dropdown Option from Panel Print was selected
		selectPrint=function(val){
			$(".file_type").val(val).change();
			currentFileType=val;
		};
		//fill or outline color selected
		selectColor=function(){
			if($("#fill_color").val()){
				fill=$("#fill_color").val();
				outline=$("#outline_color").val();
				
				currentLabelFill=fill;
				currentLabelOutline=outline;
			}
			else{
				fill=currentLabelFill;
				outline=currentLabelOutline;
			}
			$("#labelForm").find(".fill_color").spectrum("set",fill).change();
			$("#labelForm").find(".outline_color").spectrum("set",outline).change();
			
			toggleLables();
		};
		selectYesNoLabel=function(val){
			if(val=='yes'){
				currentlabelYes=true;
				if($("#labelForm").find(".switchWrap").hasClass("switchOff")){
					$("#labelForm").find(".switchWrap").click();
				}
			}
			else{
				currentlabelYes=false;
				if($("#labelForm").find(".switchWrap").hasClass("switchOn")){
					$("#labelForm").find(".switchWrap").click();
				}
			}
			changePanel("Label");//refresh label panel
		};
		
		resetLabels=function(){
			currentLabel=defaultLabel;
			currentLabelFill=defaultLabelFill;
			currentLabelOutline=defaultLabelOutline;
			currentLabelFont=defaultLabelFont;
			currentLabelWeight=defaultLabelWeight;
			currentlabelYes=defaultlabelYes;
			if($("#labelForm").find(".switchWrap").hasClass("switchOn")){
					$("#labelForm").find(".switchWrap").click();
				}
			};
			
		resetPrint=function(){
			currentFileType=defaultFileType;
			currentTitle=defaultTitle;
			currentLegendYes=defaultLegendYes;
			currentScaleYes=defaultScaleYes;
			currentFileName=defaultFileName;
		};
		
		this.restart=function(){
			currentType=defaultType;
			currentScale=defaultScale;
			currentData=defaultData;
			
			currentLabel=defaultLabel;
			currentLabelFill=defaultLabelFill;
			currentLabelOutline=defaultLabelOutline;
			currentLabelFont=defaultLabelFont;
			currentLabelWeight=defaultLabelWeight;
			currentlabelYes=defaultlabelYes;
			if($("#labelForm").find(".switchWrap").hasClass("switchOn")){
					$("#labelForm").find(".switchWrap").click();
				}
			currentFileType=defaultFileType;
			currentTitle=defaultTitle;
			currentLegendYes=defaultLegendYes;
			currentScaleYes=defaultScaleYes;
			currentFileName=defaultFileName;
			changePanel("Intro");
			
			
		};
		
		//populate dropdown from Panel Type
		updateTypeOptions=function(){
			self.panelType.valSelect.empty();
			
			if (currentType=='view'){					
				self.panelType.valSelect.append('<input type=radio value="view" name=wizardTypeOpt class=wizardRadio checked>Existing Canopy or Planting Map</input><br>');
				if(!main.withs.with_canopy_plan || main.withs.with_canopy_plan.value){
					self.panelType.valSelect.append('<input type=radio value="plan" name=wizardTypeOpt class=wizardRadio>Priority Planting Map</input><br>');	
				}
			}
			else if(currentType=='plan'){
				if(!main.withs.with_canopy_view || main.withs.with_canopy_view.value){
					self.panelType.valSelect.append('<input type=radio value="view" name=wizardTypeOpt class=wizardRadio>Existing Canopy or Planting Map</input><br>');
				}
				self.panelType.valSelect.append('<input type=radio value="plan" name=wizardTypeOpt class=wizardRadio checked>Priority Planting Map</input><br>');	
			}
			else{
				if(!main.withs.with_canopy_view || main.withs.with_canopy_view.value){
					self.panelType.valSelect.append('<input type=radio value="view" name=wizardTypeOpt class=wizardRadio>Existing Canopy or Planting Map</input><br>');
				}
				if(!main.withs.with_canopy_plan || main.withs.with_canopy_plan.value){
					self.panelType.valSelect.append('<input type=radio value="plan" name=wizardTypeOpt class=wizardRadio>Priority Planting Map</input><br>');	
				}
			}
		};
		//populate dropdown from Panel Scale
		updateScaleOptions=function(){
			self.panelScale.valSelect.empty();
			for (var key in main.canopy.layers){
					if((currentType=='view' && main.canopy.layers[key].properties.in_view_tools)||(currentType=='plan' && main.canopy.layers[key].properties.in_plan_tools)){		
						if(key==currentScale){
							self.panelScale.valSelect.append('<input type=radio value="'+key+'" name=wizardScaleOpt class=wizardRadio checked>'+main.canopy.layers[key].properties.alias+'</input><br>');
						}
						else{
							self.panelScale.valSelect.append('<input type=radio value="'+key+'" name=wizardScaleOpt class=wizardRadio>'+main.canopy.layers[key].properties.alias+'</input><br>');
						}
					}
			}
				
		};
		//populate dropdown from Panel Data
		updateDataOptions=function(){
			self.panelData.valSelect.empty();
			
			for(var t=0;t<main.canopy.currentLayerSortedFieldKeys.length;t++){
					key=main.canopy.currentLayerSortedFieldKeys[t];
					if(main.canopy.currentLayer.fields[key].in_canopy_tools && main.canopy.currentLayer.fields[key].in_view_tools){ // only view has options 
						if(key==currentData){
							self.panelData.valSelect.append('<input type=radio value="'+key+'" name=wizardDataOpt class=wizardRadio checked>'+main.canopy.currentLayer.fields[key].alias+'</input><br>');
							updateSliders(key);
						}
						else{
							self.panelData.valSelect.append('<input type=radio value="'+key+'" name=wizardDataOpt class=wizardRadio>'+main.canopy.currentLayer.fields[key].alias+'</input><br>');
						}
					}
			}
			
		};
		//populate dropdown from Panel Label
		updateLabelOptions=function(){
			self.panelLabel.fieldSelect.empty();
			
			
			fields=main.tables[currentScale].fields;
			var keys=main.utilities.sortFields(fields,'abc');
			if(currentLabel==''||currentLabel==null){
				if(currentType=='plan'){
					currentLabel='last_plan_rank';
				}
				else{
					currentLabel=main.canopy.currentLayer.properties.internal_field_map.alias;
				}
				
			};
			for(var i=0;i<keys.length;i++){
				key=keys[i];
				if(fields[key].active && fields[key].input_type!=='html' && fields[key].input_type!='hidden' && fields[key][main.account.loggedInUser.user_type+'_visible']&&fields[key].canopy_label){
					if(key==currentLabel){
						self.panelLabel.fieldSelect.append('<option type=radio value="'+key+'" Selected>'+main.canopy.currentLayer.fields[key].alias+'</option>');
						selectLabel(key);
					}
					else{
						self.panelLabel.fieldSelect.append('<option value="'+key+'">'+main.canopy.currentLayer.fields[key].alias+'</option>');
					}
				}
			}
				
		};	
		//populate dropdown from Panel Print
		updatePrintOptions=function(){
			self.panelPrint.fileSelect.empty();
			
			self.panelPrint.fileSelect.append('<option value="" >Please Select One</option>');
			
			var keys=["JPG","PNG","PDF"];
			for(var i=0;i<keys.length;i++){
				key=keys[i];
				if(key==currentFileType){
					self.panelPrint.fileSelect.append('<option value="'+key+'" Selected>'+key+'</option>');
					selectPrint(key);
				}
				else{
					self.panelPrint.fileSelect.append('<option value="'+key+'">'+key+'</option>');
				}
			}
			if(currentLegendYes){
				self.panelPrint.LegendBox.prop( "checked", true );
			}
			if(currentScaleYes){
			self.panelPrint.ScaleBox.prop( "checked", true );
			}
		};
		changePanel=function(newPanel){
			$(".wizardWindow .wizardContent").remove();
			switch(newPanel) {
				case "Type":
					$(".wizardWindow .PanelContent").html(main.wizard.createPanelTypeContent());
					break;
				case "Scale":
					$(".wizardWindow .PanelContent").html(main.wizard.createPanelScaleContent());
					break;
				case "Data":
					$(".wizardWindow .PanelContent").html(main.wizard.createPanelDataContent());
					break;
				case "Label":
					$(".wizardWindow .PanelContent").html(main.wizard.createPanelLabelContent());
					main.utilities.initSpectrumPickers(self.labelSettings,selectColor,true,false,false);
					selectColor();
					break;
				case "Print":
					$(".wizardWindow .PanelContent").html(main.wizard.createPanelPrintContent());
					break;
				default:
					$(".wizardWindow .PanelContent").html(main.wizard.createPanelIntroContent());
			}
							
			
		};
		//Begin Slider Creators
		createRangePkgHTML=function(range,activateButton,type){
			var activateButton=activateButton || '',fieldPkgLeft='',noFieldPkgLeft=' noFieldPkgLeft';
			if(activateButton){
				fieldPkgLeft=$('<div class="fieldPkgLeft  fieldPkgEle cf">').append(activateButton);
				noFieldPkgLeft='';
			}
			return $('<div class="fieldPkg wizardfieldPkg canopy view cf">')
				.append(fieldPkgLeft)
				.append($('<div class="fieldPkgRight fieldPkgEle cf'+noFieldPkgLeft+'">')
					// .append($('<div class="fieldPkgLabel">').html(field.alias))
					// .append(self[tools].fields[fieldName].slider.html)
					.append(range.html)
				);
		};

		createWeightFilterPkg=function(){
		
			self.panelData.weight_ranges.plan_weight=new Range({
				alias:'Filter by '+self.weightLabelAlias,
				classes:'canopyRange fieldPkgRightEle',
				noOutputs:true,
				to_fixed:0,
				min:main.canopy.currentLayer.canopy.plan.weightedScore.minWeightedScore,
				max:main.canopy.currentLayer.canopy.plan.weightedScore.maxWeightedScore,
				
				
				onChange:selectWeightRange,
				onMove:selectWeightRange,
				
				moreInfo:false,
				handlesLength:1
			});
			main.canopy.currentLayer.canopy.plan.weightedScore.weightFilter.currentHi=main.canopy.currentLayer.canopy.plan.weightedScore.maxWeightedScore;
			main.canopy.currentLayer.canopy.plan.weightedScore.weightFilter.currentLow=main.canopy.currentLayer.canopy.plan.weightedScore.minWeightedScore;
			return (createRangePkgHTML(self.panelData.weight_ranges.plan_weight));
		};	
		createRangePkg=function(field,onRangeChange,onRangeMove){
			if(field.input_type=='date'){
				var isTime=false;
				var isDate=true;
				if(field.sub_data_type=='time'){
					var displayFunction=main.utilities.msToTime;
					isDate=false;
					isTime=true;
				}else{var displayFunction=main.utilities.formatDate}
			}else{var displayFunction=null;}
			// var fixed=Math.min(self.maxToFixed,field.to_fixed);
			var fixed=self.maxToFixed;
			var moreInfo=false;
			var moreInfoContent=null;
			var moreInfoTitle=null;
			
			self.panelData.view_ranges[field.name]=new Range({
				alias:field.alias,
				classes:'wizardRange fieldPkgRightEle',
				key:field.name,
				to_fixed:0,
				prepend_to:field.prepend_to,
				append_to:field.append_to,
				step:field.step,
				min:self.defaultRangeMin,
				max:self.defaultRangeMax,
				withScale:true,
				onChange:onRangeChange,
				onMove:onRangeMove,
				withAvg:true,
				withAvgLbl:'City Wide Avg',
				displayFunction:displayFunction,
				moreInfo:moreInfo,
				moreInfoContent:moreInfoContent,
				moreInfoTitle:moreInfoTitle,
				isDate:isDate,
				isTime:isTime
			});
			
			self.panelData.view_ranges[field.name].activateButton=$('<div>');
			return createRangePkgHTML(self.panelData.view_ranges[field.name],self.panelData.view_ranges[field.name].activateButton); 
			};
			
			createSliderPkg=function(field,onRangeChange,onRangeMove){
			var moreInfo=false;
			var moreInfoContent=null;
			var moreInfoTitle=null;
			
			self.panelData.plan_ranges[field.name]=new Range({
				alias:field.alias,
				classes:'wizardRange fieldPkgRightEle weightSlider',
				key:field.name,
				to_fixed:0,
				value:[self.defaultWeight],
				step:self.defaultWeightStep,
				max:self.defaultWeightMax,
				min:self.defaultWeightMin,
				displayFunction:weightDisplay,
				onChange:onRangeChange,
				withScale:true,
				discreteScale:true,
				onMove:onRangeMove,
				handlesLength:1,
				round:1,
				moreInfo:moreInfo,
				moreInfoContent:moreInfoContent,
				moreInfoTitle:moreInfoTitle
			});
			
			return $('<div class="wizardfieldPkg canopy plan cf">')
					.append($('<div class="fieldPkgEle cf">')
					.append(self.panelData.plan_ranges[field.name].html)
				);
		};
		
		weightDisplay=function(val){
			var html='';
			if(val==0){html='None';
			}else if(val==3.33){html=self.relativeValues[0];
			}else if(val==6.66){html=self.relativeValues[1];
			}else if(val==9.99){html=self.relativeValues[2];}
			return html;
		};
		//End Slider Creators
		
		//Call the create slider functions for Panel Data and add to the panel
		updateSliders=function(field){
			self.panelData.sliders.empty();
			if(currentType=='plan'){
				for(var t=0;t<main.canopy.currentLayerSortedFieldKeys.length;t++){
					key=main.canopy.currentLayerSortedFieldKeys[t];
					if(main.canopy.currentLayer.fields[key].in_canopy_tools && main.canopy.currentLayer.fields[key].in_plan_tools){
						self.panelData[key]=main.utilities.cloneObj(main.canopy.currentLayer.fields[key]);
						self.panelData.sliders.append(createSliderPkg(self.panelData[key],selectSlider,selectSlider));
					}
				}
			}
			else{
				if(main.canopy.currentLayer.fields[field].in_canopy_tools && main.canopy.currentLayer.fields[field].in_view_tools){
					self.panelData.fields[field]=main.utilities.cloneObj(main.canopy.currentLayer.fields[field]);
					
					self.panelData.sliders.append(createRangePkg(self.panelData.fields[field],selectSlider,selectSlider));
					self.panelData.view_ranges[field].updateExtremes(main.canopy.currentLayer.canopy.stats.fields[field].max,main.canopy.currentLayer.canopy.stats.fields[field].min,true);
					self.panelData.view_ranges[field].setAvg(main.canopy.currentLayer.canopy.stats.fields[field].avg);
				}
			}
				
		};
		
		//Panel Data slider has been moved, Update the cooresponding slider in canopy
		selectSlider=function(currentRange){
			if(currentType=='view'){
				key=currentRange.key;
				low=currentRange.currentLow;
				hi=currentRange.currentHi;
				main.canopy.view.fields[key].range.currentLow=low;
				main.canopy.view.fields[key].range.currentHi=hi;
				main.canopy.view.fields[key].range.updateAll();
				main.canopy.view.rangeChange(main.canopy.view.fields[key].range.getRangePkg());
			}
			else{
				key=currentRange.key;
				low=currentRange.currentLow;
				hi=currentRange.currentHi;
				main.canopy.plan.fields[key].slider.currentLow=low;
				main.canopy.plan.fields[key].slider.currentHi=hi;
				main.canopy.plan.fields[key].slider.updateAll();
				main.canopy.plan.rangeChange(main.canopy.plan.fields[key].slider.getRangePkg());
			}
		};
		selectWeightRange=function(currentRange){
			
				key=currentRange.key;
				low=currentRange.currentLow;
				hi=currentRange.currentHi;
				
				var args={};
				args.currentHi=hi;
				args.currentLow=low;
				main.canopy.plan.weightFilterPkgRange.currentLow=low;
				main.canopy.plan.weightFilterPkgRange.currentHi=hi;
				main.canopy.plan.weightFilterPkgRange.updateAll();
				main.canopy.plan.weightFilterChange(main.canopy.plan.weightFilterPkgRange);
			
		};
		//Start Create Panel Functions 
		self.createPanelIntroContent=function(){
			self.panelIntro.content=$('<div>').addClass('wizardContent panelIntro');
 			self.panelIntro.titleHTML=$('<div>').addClass('welcomeTitle welcomeRibbon');
				$('<p>').addClass('subHeader center').html(PanelIntroTitle).appendTo(self.panelIntro.titleHTML);		
			self.panelIntro.wizardTopRow=$('<div>').addClass('row wizardTopRow cf');
			self.panelIntro.wizardTopRow.html(self.panelIntro.titleHTML);
			self.panelIntro.content.append(self.panelIntro.wizardTopRow);
			self.panelIntro.wizardCenterRow=$('<div>').addClass('row wizardCenterRow cf');
			self.panelIntro.instruction=$('<p>').addClass('center').html("<p>This step-by-step wizard walks you through how to make full <br> use of the mapping tools to create great maps.<br> Each step is listed below.<br></br><b>In Step 1</b>, choose the type of map you want to make</br></br><b>In Step 2</b>, choose the layer you want to include in your map</br></br><b>In Step 3</b>, choose your data fields or priorities </br></br><b>In Step 4</b>, choose your map labels </br></br><b>In Step 5</b>, choose print options and print your map!</br></br>Note: Any settings that have been set in canopy planner will be <br>changed when using the wizard.</p>");
			self.panelIntro.nextButton=$('<button>').addClass("stdButton button cf wizardButtonBegin").html('Begin');
			self.panelIntro.nextButton.on('click',function(){
							currentType=defaultType;
							currentScale=defaultScale;
							currentData=defaultData;
							
							currentLabel=defaultLabel;
							currentLabelFill=defaultLabelFill;
							currentLabelOutline=defaultLabelOutline;
							currentLabelFont=defaultLabelFont;
							currentLabelWeight=defaultLabelWeight;
							currentlabelYes=defaultlabelYes;
							
							currentFileType=defaultFileType;
							currentTitle=defaultTitle;
							currentLegendYes=defaultLegendYes;
							currentScaleYes=defaultScaleYes;
							currentFileName=defaultFileName;
							main.layout.sidePanels.canopy.open();
							self.waitToOpen=new Wait(main.canopy.checkForCurrentLayer,canopyLayerLoaded);
							
						});
			
			self.panelIntro.wizardCenterRow.append(self.panelIntro.instruction);
			self.panelIntro.wizardCenterRow.append(self.panelIntro.nextButton);
			self.panelIntro.content.append(self.panelIntro.wizardCenterRow);	
			return self.panelIntro.content;
		};
		
		self.createPanelTypeContent=function(){
			
			self.panelType.content=$('<div>').addClass('wizardContent panelType');
 			self.panelType.titleHTML=$('<div>').addClass('welcomeTitle welcomeRibbon');
				$('<p>').addClass('subHeader center').html(PanelTypeTitle).appendTo(self.panelType.titleHTML);		
			self.panelType.wizardTopRow=$('<div>').addClass('row wizardTopRow cf');
			self.panelType.wizardTopRow.html(self.panelType.titleHTML);
			self.panelType.content.append(self.panelType.wizardTopRow);
			self.panelType.wizardCenterRow=$('<div>').addClass('row wizardCenterRow cf');
			self.panelType.instruction=$('<p>').addClass('center').html("Step 1: What type of map do you want to make?");
			self.panelType.valSelect=$('<div id="panelTypeSelect" class="wizardValSelect">');
			
			self.panelType.valSelect.on('change',function(){
							if($('input[name=wizardTypeOpt]:checked').val()){
								val=$('input[name=wizardTypeOpt]:checked').val();
								selectType(val);
								self.panelType.nextButton.prop("disabled",false);
							}
						});
			
			updateTypeOptions();
			self.panelType.nextButton=$('<button>').addClass("stdButton button cf wizardButtonNext").html('Next');
			self.panelType.nextButton.on('click',function(){
							
							changePanel("Scale");
							
						});
			self.panelType.prevButton=$('<button>').addClass("stdButton button cf wizardButtonPrev").html('Previous');
			self.panelType.prevButton.on('click',function(){
							changePanel("Intro");
						});
			if(currentType==''){self.panelType.nextButton.prop("disabled",true);}			
			
			self.panelType.wizardCenterRow.append(self.panelType.instruction);
			self.panelType.wizardCenterRow.append(self.panelType.valSelect);
			self.panelType.wizardCenterRow.append(self.panelType.prev);
			self.panelType.wizardCenterRow.append(self.panelType.nextButton);
			self.panelType.content.append(self.panelType.wizardCenterRow);	
			return self.panelType.content;
		};
		self.createPanelScaleContent=function(){
			main.layout.sidePanels.canopy.open();
			self.panelScale.content=$('<div>').addClass('wizardContent panelScale');
 			self.panelScale.titleHTML=$('<div>').addClass('welcomeTitle welcomeRibbon');
				$('<p>').addClass('subHeader center').html(PanelScaleTitle).appendTo(self.panelScale.titleHTML);		
			self.panelScale.wizardTopRow=$('<div>').addClass('row wizardTopRow cf');
			self.panelScale.wizardTopRow.html(self.panelScale.titleHTML);
			self.panelScale.content.append(self.panelScale.wizardTopRow);
			self.panelScale.wizardCenterRow=$('<div>').addClass('row wizardCenterRow cf');
			self.panelScale.instruction=$('<p>').addClass('center').html("Step 2: What is your Geographic Scale of interest?");
			self.panelScale.valSelect=$('<div id="panelScaleSelect" class="wizardValSelect">');
			self.panelScale.valSelect.on('change',function(){
							if($('input[name=wizardScaleOpt]:checked').val()){
								val=$('input[name=wizardScaleOpt]:checked').val();
								selectArea(val);
								self.panelScale.nextButton.prop("disabled",false);
							}
						});
			updateScaleOptions();
			
			self.panelScale.nextButton=$('<button>').addClass("stdButton button cf wizardButtonNext").html('Next');
 			self.panelScale.nextButton.on('click',function(){
							changePanel("Data");
						}); 

			self.panelScale.prevButton=$('<button>').addClass("stdButton button cf wizardButtonPrev").html('Previous');
			self.panelScale.prevButton.on('click',function(){
							changePanel("Type");
						});
			if(currentScale==''){self.panelScale.nextButton.prop("disabled",true);}					
			self.panelScale.wizardCenterRow.append(self.panelScale.instruction);
			self.panelScale.wizardCenterRow.append(self.panelScale.valSelect);
			self.panelScale.wizardCenterRow.append(self.panelScale.prevButton);
			self.panelScale.wizardCenterRow.append(self.panelScale.nextButton);
			self.panelScale.content.append(self.panelScale.wizardCenterRow);
			return self.panelScale.content;
		};
		
		self.createPanelDataContent=function(){
			main.layout.sidePanels.canopy.open();
			self.panelData.content=$('<div>').addClass('wizardContent panelData');
 			self.panelData.titleHTML=$('<div>').addClass('welcomeTitle welcomeRibbon');
				$('<p>').addClass('subHeader center').html(PanelDataTitle).appendTo(self.panelData.titleHTML);		
			self.panelData.wizardTopRow=$('<div>').addClass('row wizardTopRow cf');
			self.panelData.wizardTopRow.html(self.panelData.titleHTML);
			self.panelData.content.append(self.panelData.wizardTopRow);
			self.panelData.wizardCenterRow=$('<div>').addClass('row wizardCenterRow cf');
			self.panelData.options=$('<div>').addClass('row wizardOptions cf');
			if(currentType=='view'){
				self.panelData.instruction=$('<p>').addClass('center').html("Step 3: What data would you like to see?");
			}
			else{
				self.panelData.instruction=$('<p>').addClass('center').html("Step 3: What are your priorities?");
			}
			self.panelData.slidersWrap=$('<div>').addClass('row wizardSliders cf');
			self.panelData.sliders=$('<div>').addClass('row wizardSliders cf');
			self.panelData.sliders.weight=$('<div>').addClass('row wizardSliders weight cf');
			self.panelData.sliders.weightInstruction=$('<p>').addClass('center').html("Do you want to filter the map to show the highest priority areas? (optional)");
			if(currentType=='plan'){
				updateSliders('na');
				self.panelData.sliders.weight.append(createWeightFilterPkg());
				self.panelData.slidersWrap.append(self.panelData.sliders);
				self.panelData.slidersWrap.append(self.panelData.sliders.weightInstruction);
				self.panelData.slidersWrap.append(self.panelData.sliders.weight);
			}
			else{
				self.panelData.valSelect=$('<div id="panelDataSelect" class="wizardValSelect">');
				self.panelData.valSelect.on('change',function(){
						if($('input[name=wizardDataOpt]:checked').val()){
								val=$('input[name=wizardDataOpt]:checked').val();
								selectData(val);
								updateSliders(val);
								self.panelData.slidersWrap.append(self.panelData.sliders);
								self.panelData.nextButton.prop("disabled",false);
							}
						});
				updateDataOptions();
				self.panelData.options.append(self.panelData.valSelect);
				self.panelData.slidersWrap.append(self.panelData.sliders);
			}

			self.panelData.nextButton=$('<button>').addClass("stdButton button cf wizardButtonNext").html('Next');
 			self.panelData.nextButton.on('click',function(){
							changePanel("Label");
						}); 

			self.panelData.prevButton=$('<button>').addClass("stdButton button cf wizardButtonPrev").html('Previous');
			self.panelData.prevButton.on('click',function(){
							changePanel("Scale");
						});
			if(currentData=='' && currentType=='view'){self.panelData.nextButton.prop("disabled",true);}				
			self.panelData.wizardCenterRow.append(self.panelData.instruction);
			self.panelData.wizardCenterRow.append(self.panelData.options);
			self.panelData.wizardCenterRow.append('<p></p>'); // add some whiteSpace between elements
			self.panelData.wizardCenterRow.append(self.panelData.slidersWrap);
			self.panelData.wizardCenterRow.append(self.panelData.prevButton);
			self.panelData.wizardCenterRow.append(self.panelData.nextButton);
			self.panelData.content.append(self.panelData.wizardCenterRow);
			return self.panelData.content;
		};
		self.createPanelLabelContent=function(){
			//main.canopy.blockDeactivate=true;
			main.utilities.openForm("labels","toolBox",true);
			main.canopy.refreshFeatures();
			//main.canopy.blockDeactivate=false;
			$(".mainLabelLayerSelect").val(currentScale).change();
			
			
			self.panelLabel.content=$('<div>').addClass('wizardContent panelLabelcontent');
 			self.panelLabel.titleHTML=$('<div>').addClass('welcomeTitle welcomeRibbon');
				$('<p>').addClass('subHeader center').html(PanelLabelTitle).appendTo(self.panelLabel.titleHTML);		
			self.panelLabel.wizardTopRow=$('<div>').addClass('row wizardTopRow cf');
			self.panelLabel.wizardTopRow.html(self.panelLabel.titleHTML);
			self.panelLabel.content.append(self.panelLabel.wizardTopRow);
			self.panelLabel.wizardCenterRow=$('<div>').addClass('row wizardCenterRow cf');
			self.panelLabel.options=$('<div>').addClass('row wizardOptions cf');
			
			self.panelLabel.instruction=$('<p>').addClass('center').html("Step 4: Do you Want Labels?");
			self.panelLabel.YesNoSelect=$('<div id="LabelYesNoSelect" class="wizardValSelect">');
				self.panelLabel.YesNoSelect.on('change',function(){
						if($('input[name=wizardyesnoOpt]:checked').val()){
								val=$('input[name=wizardyesnoOpt]:checked').val();
								selectYesNoLabel(val);
							}
						});
			if(currentlabelYes){			
				self.panelLabel.YesNoSelect.append('<input type=radio value="yes" name=wizardyesnoOpt class=wizardRadio checked>Yes </input><br>');
				self.panelLabel.YesNoSelect.append('<input type=radio value="no" name=wizardyesnoOpt class=wizardRadio >No </input><br>');
				//selectYesNoLabel("yes");
				if($("#labelForm").find(".switchWrap").hasClass("switchOff")){
					$("#labelForm").find(".switchWrap").click();
				}
			}
			else{
				self.panelLabel.YesNoSelect.append('<input type=radio value="yes" name=wizardyesnoOpt class=wizardRadio>Yes </input><br>');
				self.panelLabel.YesNoSelect.append('<input type=radio value="no" name=wizardyesnoOpt class=wizardRadio checked>No </input><br>');
			}
			self.panelLabel.YesInstrunctions=$('<p>').addClass('center').html("Select Your Label Options");
			self.panelLabel.fieldInstruction=$('<div>').html("Choose label field");
			self.panelLabel.fieldSelect=$('<select id="panelLabelSelect" class="wizardValSelect">');
				self.panelLabel.fieldSelect.on('change',function(){
						if($(this).val()){
								val=$(this).val();
								selectLabel(val);
							}
						});
			
			self.panelLabel.fillInstruction=$('<div>').html("Choose fill color");
			self.panelLabel.fillSpectrum=$('<input = "text" id="fill_color">');
			//self.labelSettings.fill_color.input=new Input({field:self.labelSettings.fill_color,specialField:false,omitAlias:true,checked:true,classes:"test"});
			self.panelLabel.outlineInstruction=$('<div>').html("Choose outline color");
			self.panelLabel.OutlineSpectrum=$('<input = "text" id="outline_color">');
			self.panelLabel.sizeInstruction=$('<div>').html("Choose font size");
			self.labelSettings.size.input=new Input({field:self.labelSettings.size,specialField:false,omitAlias:true,checked:true,classes:"test"});
			self.labelSettings.size.input.container.on('change',function(){
								$('.tbFormParamRow_size').find('.size').val($(this).val());
								toggleLables();
							});
			self.panelLabel.weightInstruction=$('<div>').html("Choose label weight");
			self.labelSettings.weight.input=new Input({field:self.labelSettings.weight,specialField:false,omitAlias:true,checked:true,classes:"test"});
				self.labelSettings.weight.input.container.on('change',function(){
								$('.tbFormParamRow_weight').find('.weight').val($(this).val());
								toggleLables();
							});
			self.labelSettings.fill_color.spectrumID="fill_color";
			self.labelSettings.outline_color.spectrumID="outline_color";
			self.labelSettings.fill_color.default_val=currentLabelFill;
			self.labelSettings.outline_color.default_val=currentLabelOutline;
			
			updateLabelOptions();
			self.panelLabel.options.append(self.panelLabel.YesInstrunctions);
			self.panelLabel.options.append(self.panelLabel.fieldInstruction);
			self.panelLabel.options.append(self.panelLabel.fieldSelect);
			self.panelLabel.options.append('<p></p>'); // add some whiteSpace between elements
			self.panelLabel.options.append(self.panelLabel.fillInstruction);
			self.panelLabel.options.append(self.panelLabel.fillSpectrum);
			self.panelLabel.options.append('<p></p>'); // add some whiteSpace between elements
			self.panelLabel.options.append(self.panelLabel.outlineInstruction);
			self.panelLabel.options.append(self.panelLabel.OutlineSpectrum);
			self.panelLabel.options.append('<p></p>'); // add some whiteSpace between elements
			self.panelLabel.options.append(self.panelLabel.sizeInstruction);
			self.panelLabel.options.append(self.labelSettings.size.input.container);
			self.panelLabel.options.append('<p></p>'); // add some whiteSpace between elements
			self.panelLabel.options.append(self.panelLabel.weightInstruction);
			self.panelLabel.options.append(self.labelSettings.weight.input.container);
			
			
			self.panelLabel.nextButton=$('<button>').addClass("stdButton button cf wizardButtonNext").html('Next');
			self.panelLabel.nextButton.on('click',function(){
							changePanel("Print");
							
						});  

			self.panelLabel.prevButton=$('<button>').addClass("stdButton button cf wizardButtonPrev").html('Previous');
			self.panelLabel.prevButton.on('click',function(){
							changePanel("Data");
							
						});
						
			self.panelLabel.wizardCenterRow.append(self.panelLabel.instruction);
			self.panelLabel.wizardCenterRow.append(self.panelLabel.YesNoSelect);
			if(currentlabelYes){
			self.panelLabel.wizardCenterRow.append(self.panelLabel.options);
			}
			self.panelLabel.wizardCenterRow.append(self.panelLabel.prevButton);
			self.panelLabel.wizardCenterRow.append(self.panelLabel.nextButton);
			self.panelLabel.content.append(self.panelLabel.wizardCenterRow);
			
			main.canopy.refreshFeatures();
			
			return self.panelLabel.content;
		};
		self.createPanelPrintContent=function(){
			//main.canopy.blockDeactivate=true;
			main.utilities.openForm("print","toolBox",true);
			//main.canopy.blockDeactivate=false;
			self.panelPrint.content=$('<div>').addClass('wizardContent panelPrint');
 			self.panelPrint.titleHTML=$('<div>').addClass('welcomeTitle welcomeRibbon');
				$('<p>').addClass('subHeader center').html(PanelPrintTitle).appendTo(self.panelPrint.titleHTML);		
			self.panelPrint.wizardTopRow=$('<div>').addClass('row wizardTopRow cf');
			self.panelPrint.wizardTopRow.html(self.panelPrint.titleHTML);
			self.panelPrint.content.append(self.panelPrint.wizardTopRow);
			self.panelPrint.wizardCenterRow=$('<div>').addClass('row wizardCenterRow cf');
			self.panelPrint.options=$('<div>').addClass('row wizardOptions cf');
			
			self.panelPrint.instruction=$('<p>').addClass('center').html("Step 5: Select Your Print Options");
			
			self.panelPrint.fileInstruction=$('<div>').html("Choose a file type");
			self.panelPrint.fileSelect=$('<select id="panelPrintSelect" class="wizardValSelect">');
				self.panelPrint.fileSelect.on('change',function(){
						if($(this).val()){
								val=$(this).val();
								selectPrint(val);
							}
						});
			
			self.panelPrint.TitleInstruction=$('<div>').html("Choose a title");
			self.panelPrint.TitleBox=$('<input type="text">').val($('.tbFormParamRow_title').find('.title').val());
				self.panelPrint.TitleBox.on('change',function(){
							val=$(this).val();
							$('.tbFormParamRow_title').find('.title').val(val).change();
							currentTitle=val;
						});
			self.panelPrint.LegendInstruction=$('<div>').html("Do you want a legend?");
			self.panelPrint.LegendBox=$('<input type="checkbox" id="printPanelLegend">');
				self.panelPrint.LegendBox.on('click',function(){
							$('.tbFormParamRow_with_legend').find('.checkbox').click();
							if(currentLegendYes){currentLegendYes==false;}else{currentLegendYes==true;}
						});
			self.panelPrint.ScaleInstruction=$('<div>').html("Do you want a scale bar?");
			self.panelPrint.ScaleBox=$('<input type="checkbox" id="printPanelScale">');
				self.panelPrint.ScaleBox.on('click',function(){
							$('.tbFormParamRow_with_scale').find('.checkbox').click();
							if(currentScaleYes){currentScaleYes==false;}else{currentScaleYes==true;}
						});
			self.panelPrint.NameInstruction=$('<div>').html("Choose a filename");
			self.panelPrint.NameBox=$('<input type="text">').val($('.tbFormParamRow_file_name').find('.file_name').val());
				self.panelPrint.NameBox.on('change',function(){
								val=$(this).val();
								$('.tbFormParamRow_file_name').find('.file_name').val(val);
								currentFileName=val;
							});
			self.panelPrint.PrintButton=$('<button>').addClass("stdButton button cf wizardButtonPrint").html("Print");
				self.panelPrint.PrintButton.on('click',function(){
							$('.startPrinting').click();
							self.hide();
						});
			
			updatePrintOptions();
			self.panelPrint.options.append(self.panelPrint.fileInstruction);
			self.panelPrint.options.append(self.panelPrint.fileSelect);
			self.panelPrint.options.append('<p></p>'); // add some whiteSpace between elements
			self.panelPrint.options.append(self.panelPrint.TitleInstruction);
			self.panelPrint.options.append(self.panelPrint.TitleBox);
			self.panelPrint.options.append('<p></p>'); // add some whiteSpace between elements
			self.panelPrint.options.append(self.panelPrint.LegendInstruction);
			self.panelPrint.options.append(self.panelPrint.LegendBox);
			self.panelPrint.options.append('<p></p>'); // add some whiteSpace between elements
			self.panelPrint.options.append(self.panelPrint.ScaleInstruction);
			self.panelPrint.options.append(self.panelPrint.ScaleBox);
			self.panelPrint.options.append('<p></p>'); // add some whiteSpace between elements
			self.panelPrint.options.append(self.panelPrint.NameInstruction);
			self.panelPrint.options.append(self.panelPrint.NameBox);
			self.panelPrint.options.append('<p></p>'); // add some whiteSpace between elements
			self.panelPrint.options.append(self.panelPrint.PrintButton);
			
			
			
/* 			self.panelPrint.nextButton=$('<button>').html('Next');
			self.panelPrint.nextButton.on('click',function(){
							$(".wizardWindow .wizardContent").remove();
							$(".wizardWindow .PanelContent").html(main.wizard.createPanelPrintContent());
						});   */

			self.panelPrint.prevButton=$('<button>').addClass("stdButton button cf wizardButtonPrev").html('Previous');
			self.panelPrint.prevButton.on('click',function(){
							changePanel("Label");
							
						});
						
			self.panelPrint.wizardCenterRow.append(self.panelPrint.instruction);
			self.panelPrint.wizardCenterRow.append(self.panelPrint.options);
			
			self.panelPrint.wizardCenterRow.append(self.panelPrint.prevButton);
			//self.panelPrint.wizardCenterRow.append(self.panelType.nextButton);
			self.panelPrint.content.append(self.panelPrint.wizardCenterRow);
			return self.panelPrint.content;
		};
		
		//End Panel Creation Functions 
		
		(this.createHTML=function(){
			if(!self.inSettings){
				//shelf
				if(self.addAccShelf){
					self.launchButton=new Button('toolBox','',self.alias);
					self.shelfContent=self.createShelfContent();
					self.shelf=main.layout.toolBox.createShelf({
						name:'wizard',
						button:self.launchButton.html,
						content:self.shelfContent,
						alias:self.alias
					});
				}
			}else{
				var settingsStuff=main.settings.addSetting('Wizard Window');
				self.showWizardButton=settingsStuff.showButton;
				self.hideWizardButton=settingsStuff.hideButton;
			}
			PanelIntroTitle='<b>Welcome to the Wizard</b>';
			PanelTypeTitle='<b>Step 1</b> <br> Type of Map';
			PanelScaleTitle='<b>Step 2</b> <br> Geographic Scale';
			PanelDataTitle='<b>Step 3 </b> <br> Data Selection';
			PanelLabelTitle='<b> Step 4 </b> <br> Label Options';
			PanelPrintTitle='<b> Step 5 </b> <br> Print Options';

			self.mainPanel.content=self.createPanelIntroContent();
			
			self.panel=new Panel({
				content:self.mainPanel.content,
				title:'Wizard',
				classes:'wizardWindow',
				position:'mMiddle',
				matchHeight:true,
				relativeTo:main.layout.mapContainerWrap,
				minOnOutClick:self.minOnOutClick,
				hideOnOutClickExceptions:['.headerContEle.icon','.headerContEle.text'],
				onClose:self.updateButtonsHide,
				onMin:self.updateButtonsHide,
				onShow:self.updateButtonsShow,
				withOutClose:false
			});
			self.mainPanel.restartLink=$("<a href='#' id='wizardRestartLink'>").text("Restart Wizard");
			self.mainPanel.restartLink.on('click',function(){
					self.restart();
				});
			
		})();
		(this.initEvents=function(){
			self.panel.html.on('change','.WizardLayerCheckWrap',function(){
				var WizardID=$(this).data('Wizardid');
				var visible=$(this).prop('checked');
			});

			if(self.addAccShelf){
				self.hideWizardButton.html.on('click',function(){
					self.hide();
				});
				self.showWizardButton.html.on('click',function(){
					self.show();
					
				});
			}
			
		})();
		if(this.setToApp){this.show();}else{this.updateButtonsHide()}
	};
});