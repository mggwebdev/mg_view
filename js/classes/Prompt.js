define(function(require){
	return function pg(type,content,buttons,visible){
		this.visible=visible;
		this.createInContextPrompt=function(content,buttons){
			var classes='';
			if(this.visible) classes='block';
			promptButtons=$('<div>').addClass('promptButtons cf '+classes);
			this.container=$('<div>').addClass('prompt inContext')
				.append($('<div>').addClass('promptContent stdContent').html(content))
				.append(promptButtons);
			for(var i=0;i<buttons.length;i++){
				promptButtons.append($('<div>').addClass('promptButton button '+buttons[i][0]).html(buttons[i][1]))
			}
			this.container;
		};
		if(type=='inContext'){
			this.createInContextPrompt(content,buttons);
		}
	};
});