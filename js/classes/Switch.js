define(function(require){
	return function pg(onCallBack,offCallBack,defaultOn){
		var self=this;
		this.onCallBack=onCallBack;
		this.offCallBack=offCallBack;
		this.defaultOn=defaultOn;
		this.onState=false;
		this.turningOn=false;
		this.turningOff=false;
		this.createSwitch=function(){
			this.switchButton=$('<div>').addClass('switchButton');
			this.container=$('<div>').addClass('switchWrap switchOff noSelect centerMargin')
				.append(this.switchButton)
				.append($('<div>').addClass('switchBackground cf')
					.append($('<div>').addClass('switchLeft switchContent').text('On'))
					.append($('<div>').addClass('switchRight switchContent').text('Off'))
				)
			if(this.defaultOn){
				this.onState=true;
				this.setButton();
			}
		};
		this.animateSwitch=function(onState,dontDoWork,callCB){
			if(parseInt(this.switchButton.css('right'))===0){
				this.switchButton.css('right','').css('left',this.container.width()-this.switchButton.outerWidth(true))
			}
			if(this.switchButton.is(':animated')){
				self.turningOn=false;
				self.turningOff=false;
				this.switchButton.stop(true,true)
			}
			this.switchButton.animate({left:this.moveTo},main.animations.defaultDuration,main.animations.defaultTransition,function(){
				self.onState=onState;
				self.switchClass();
				if(!dontDoWork){
					self.turningOff=false;
					self.turningOn=false;
					if(onState && self.onCallBack){
						self.onCallBack();
					}else{
						self.offCallBack();
					}
				}
				if(callCB){
					if(self.onCallBack){ self.onCallBack();}
				}
			});
		};
		this.setButton=function(){
			this.switchClass();
			this.setButtonPosition();
		};
		this.setButtonPosition=function(){
			if(this.onState){this.switchButton.css('left','auto').css('right',0);
			}else{this.switchButton.css('left',0);}
		};
		this.switchClass=function(){
			if(this.onState){this.container.removeClass('switchOff').addClass('switchOn');
			}else{this.container.removeClass('switchOn').addClass('switchOff');}
		};
		this.turnSwitchOn=function(dontDoWork,callCB){
			if(!this.onState && !this.turningOn){
				if(this.container.is(':hidden')){
					this.onState=true;
					this.setButton();
					if(callCB){
						if(self.onCallBack){self.onCallBack();}
					}
				}else{
					this.moveTo=this.container.width()-this.switchButton.outerWidth(true);
					this.turningOn=true;
					this.animateSwitch(true,dontDoWork,callCB);
				}
			}
		};
		this.turnSwitchOff=function(dontDoWork,callCB){
			if(this.onState && !this.turningOff){
				if(this.container.is(':hidden')){
					this.onState=false;
					this.setButton();
					if(callCB){
						if(self.onCallBack){ self.onCallBack();}
					}
				}else{
					this.moveTo=0;
					this.turningOff=true;
					this.animateSwitch(false,dontDoWork,callCB);
				}
			}
		};
		this.initEvents=function(){
			var self=this;
			this.container.on('click',function(){
				if(self.onState){
					self.turnSwitchOff(false);
				}else{
					self.turnSwitchOn(false);
				}
			});
		};
		this.createSwitch();
		this.initEvents();
	};
});