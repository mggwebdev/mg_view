define(function(require){
	return function pg(args){
		var self=this;
		var Header=require('./Header');
		var LoaderBox=require('./LoaderBox');
		this.shortcuts={};
		this.accordians={};
		this.sidePanels={};
		this.forms={};
		this.shelves={};
		this.panels={};
		this.popups=[];
		this.editForms=[];
		this.defaultSidePanelWidth=340;
		this.stdOffset=10;
		this.closeAllSidePanels=function(context,exceptions){
			for(var key in self.sidePanels){
				if((!exceptions || exceptions.indexOf(key)==-1) && (self.sidePanels[key].opened || self.sidePanels[key].opening) && !self.sidePanels[key].closing) self.sidePanels[key].close(context);
			}
		};
		this.checkAccordianWidths=function(){
			for(var key in self.accordians){
				self.accordians[key].extSidePanelTest2();
			}
		};
		this.closeAllEditForms=function(){
			for(var i=0;i<self.editForms.length;i++){
				if((self.editForms[i].panel.opened || self.editForms[i].panel.opening) && !self.editForms[i].panel.minimized){ self.editForms[i].panel.close();}
			}
		};
		this.removeAllPopUps=function(){
			for(var i=0;i<main.layout.popups.length;i++){
				main.layout.popups[i].hide();
			}
		};
		this.refreshPopups=function(){
			for(var i=0;i<main.layout.popups.length;i++){
				main.layout.popups[i].refresh();
			}
		};
		this.initMainLayout=function(){
			//header
			this.header=new Header();
			//map
			this.mapContainerWrap=$('<div>').attr('id','mapContainerWrap').addClass('mapWrap');
			this.mapContainer=$('<div>').attr('id','mainMap').addClass('map');
			this.gmapContainer=$('<div>').attr('id','gmap').addClass('map gmapHidden');
			this.bodyContentWrap=$('<div>').addClass('bodyContentWrap');
			$('body').prepend(this.header.container);
			this.bodyContentWrap
				.append(this.mapContainerWrap.append(this.mapContainer).append(this.gmapContainer));
			$('body').append(this.bodyContentWrap);
			this.setDimensions(this);
			this.initEvents();
			this.initAccountBox();
			this.initPanels();
			this.initImageViewer();
			this.initToolTips();
			this.initInfoTips();
			this.initPopups();
			this.initGrads();
			this.initMoreInfos();
			this.initShade();
			this.initLoader();
		};
		this.initAccountBox=function(){
			self.accountBoxHolder=$('<div>').addClass('accountBoxHolder');
			self.mapContainerWrap.append(self.accountBoxHolder);
		};
		this.initPanels=function(){
			self.panelsContainer=$('<div>').addClass('panelsContainer');
			self.mapContainerWrap.append(self.panelsContainer);
		};
		this.initImageViewer=function(){
			self.imageViewerContainer=$('<div class="fixedContainer" id="imageViewerContainer">');
			$('body').append(self.imageViewerContainer);
		};
		this.initToolTips=function(){
			self.toolTipsContainer=$('<div class="fixedContainer" id="toolTipsContainer">');
			$('body').append(self.toolTipsContainer);
		};
		this.initInfoTips=function(){
			self.infoTipsContainer=$('<div class="fixedContainer" id="infoTipsContainer">');
			$('body').append(self.infoTipsContainer);
		};
		this.initPopups=function(){
			self.popupsContainer=$('<div class="fixedContainer" id="popupsContainer">');
			$('body').append(self.popupsContainer);
		};
		this.initGrads=function(){
			self.grdsContainer=$('<div class="fixedContainer" id="grdsContainer">');
			$('body').append(self.grdsContainer);
			self.grds={};
		};
		this.initMoreInfos=function(){
			self.moreInfosContainer=$('<div class="fixedContainer" id="moreInfosContainer">');
			$('body').append(self.moreInfosContainer);
		};
		this.initShade=function(){
			self.shade=$('<div id="shade">');
			$('body').append(self.shade);
		};
		this.initEvents=function(){
			$(window).resize(function(){
				self.setDimensions(self)
			});
		};
		this.setDimensions=function(self){
			headerHeight=self.header.container.outerHeight(true);
			windowHeight=$(window).height();
			bodyContentWrapHeight=windowHeight-headerHeight;
			self.bodyContentWrap.height(bodyContentWrapHeight);
			if(main.map){main.map.adjustMapSize();}
		};
		this.initLoader=function(message){
			self.loader=new LoaderBox({message:'Loading'});
			$('body').append(self.loader.html);
		}
	};
});