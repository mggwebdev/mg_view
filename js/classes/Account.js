define(function(require){
	return function pg(args){
		var Accordian=require('./Accordian');
		var Button=require('./Button');
		var MoreInfo=require('./MoreInfo');
		var self=this;
		this.alias='Account';
		this.name='account';
		this.withItem='with_account';
		this.users_settings=main.personal_settings.app_settings;
		this.accountSettings=main.personal_settings.account;
		this.default_user_type='public_1';
		this.default_logged_in_user=this.accountSettings.default_logged_in_user;
		this.loggedInUser=null;
		this.loggedInUserType=null;
		this.inToolBox=true;
		this.addItShelves={};
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		
		this.addManageOrgShelf=function(){
			self.manageOrgs={};
			self.manageOrgs.launchButton=new Button('toolBox','','Organizations');
			self.manageOrgs.manageAccountsHTML=$('<div class="accountManageActs Extended">').html('<p>Fill out the form to add a new organization or double click on a record to edit that organization. Click Submit. </p><div class="modal fade" id="addOrgModal"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 id="add-edit-org-title" class="modal-title">Add/Edit Organization</h4></div><div class="modal-body"><form class="form-addOrg" role="form" id="addOrgForm"><input type="text" name="orgId" placeholder="ID"id="orgId" disabled="true" /><br><input type="text" name="orgname" placeholder="Name"id="orgname" required/><br>Organization Type:<br><select id="select_orgtype" required><option></option></select><br><button id="addMapPoint" type="button" class="button btn btn-default">Add Map Point</button><br><input type="text" name="geom" placeholder="Point Geometry"id="geom"disabled="true" required/><br><input type="text" name="zoom" placeholder="Zoom Level"id="zoom"disabled="true" required/><br></form></div><div class="modal-footer"><button id="addOrgCancel" type="button" class="button btn btn-default" data-dismiss="modal">Cancel</button><button class="button btn btn-primary" type="submit" id="submit" form="addOrgForm">Save Changes</button></div></div><!-- /.modal-content --></div><!-- /.modal-dialog --></div><!-- /.modal --><table id="tblOrgs" class="display" width="80%"><thead><tr><th>ID</th><th>Organization</th><th>Type</th><th class="editTH">&nbsp;</th></tr></thead></table>');
			
			self.manageOrgs.manageAccountsShelf=self.toolBoxAccordian.createShelf({
				name:'manageOrgs',
				button:self.manageOrgs.launchButton.html,
				content:self.manageOrgs.manageAccountsHTML,
				alias:'Organizations'
			});
			self.manageOrgs.shelf=self.toolBoxAccordian.shelves[self.manageOrgs.manageAccountsShelf.data('name')];
			self.manageOrgs.manageAccountsShelf.addClass('extended');
			self.addItShelves['manageOrgs']=self.manageOrgs;
			self.manageOrgsJS();
			return self.manageOrgs.manageAccountsShelf;
		};
		
		this.manageOrgsJS=function(){
			$("#addMapPoint").on( 'click', function( ) {
							
						 //$("#geom").val(main.map.mainView.getZoom());

					$(main.map.map.getViewport()).on('click',function(e){
						var coord=main.map.map.getEventCoordinate(e.originalEvent);
						var zlevel=main.map.mainView.getZoom();
						//var latlng=ol.proj.transform(coord, 'EPSG:3857', 'EPSG:4326');
						if(coord){$("#geom").val(coord);
								$("#zoom").val(zlevel);}
					
					});
					
					//$(main.map.map.getViewport()).un('click');
				});
				
				//reset org id on org modal
				//$('#addOrgModal').data('orgId', null);
				
				ufcLoginApp.hideAdminButtons();
				ufcLoginApp.getLoggedInUserData();
						
				//$( "#logoutSpan" ).tooltip({'placement':'left'});
				//$( "#logoutSpan" ).on('click', function() { location.href='logout.php' } );
			
				$("a.navLink").click(function(evt) {
					//ufcLoginApp.loginVerify(evt.target.id);
				});
				
				//delete org
				$('body').on('click','.deleteOrg',function(e) {
					e.preventDefault();
					e.stopImmediatePropagation();
					if(confirm("Please contact PlanitGeo to delete an organization")==true){
						
						return;
						
						var id=$(this).closest('tr').find('.tableRowId').data('id');
						var params={
							ids : [id],
							folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2]
						}
						$.post(main.loginPath+'dbOrgs.php',{
								action : 'deleteOrgs',
								params : params
							}).done(function(response){
								if(response){
									response=JSON.parse(response);
									if(response.status=="OK"){
										if(main.filter_legend.layername=='organization'){
											main.filter_legend.refresh();
											main.dashboard.refresh();
										}
									}else{
										alert("Error");
									}
								}
							});
					}
				});

				//init empty dt
				$("#tblOrgs").DataTable( {
						data: {},
						paging: false
					});	
				
				var params = {folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2]};
				$.post(main.loginPath+'dbOrgs.php', {
						action : 'getOrgs',
						params : params
					}).done(function(data) {

						var jData = JSON.parse(data);
						
						//add delete button
						for(var i=0;i<jData.length;i++){
							jData[i][0]='<div class="tableRowId" data-id='+jData[i][0]+'>'+jData[i][0]+'</div>';
							jData[i].push('<a href="#" class="deleteOrg">Delete</a>');
						}
						initDataTableOrg( jData );
					});
				function getOrgs(){
				//get data to populate existing orgs table
				$.post(main.loginPath+'dbOrgs.php', {
						action : 'getOrgs',
						params : params
					}).done(function(data) {
					
						var jData = JSON.parse(data);
						var table = $("#tblOrgs").DataTable();
						table.clear();
						//add delete button
						for(var i=0;i<jData.length;i++){
							jData[i][0]='<div class="tableRowId" data-id='+jData[i][0]+'>'+jData[i][0]+'</div>';
							jData[i].push('<a href="#" class="deleteOrg">Delete</a>');
						}
						initDataTableOrg( jData );
					});
				}
				//initialize datatable with received json data
				function initDataTableOrg( jData ) {

					$("#tblOrgs").dataTable().fnAddData(jData);
					
					//rowclick event - capture org id
					$("#tblOrgs").dataTable().on( 'dblclick', 'tr', function() {
						
						//get org data from table cells - presently by position
						var orgId = $(this).find('td').eq(0).text();
						var orgname = $(this).find('td').eq(1).text();
						var orgtype = $(this).find('td').eq(2).text();
						//var orgextent = $(this).find('td').eq(3).text();
						
						//add org id to modal before opening for edi
						$('#addOrgModal').data('orgId',orgId);
						$('#addOrgModal').data('orgname',orgname);
						$('#addOrgModal').data('orgtype',orgtype);
						//$('#addOrgModal').data('orgextent',orgextent);
						
						//$('#addOrgModal').modal('show');
						var orgId = $('#addOrgModal').data('orgId');
						if( !orgId ) {
							//add mode
							$("#add-edit-org-title", '#addOrgModal').text('Add Organization');

						} else {
							//edit org mode
							$("#add-edit-org-title", '#addOrgModal').text('Edit Organization');
							$("#orgId", '#addOrgModal').val( $('#addOrgModal').data('orgId') );
							$("#orgname", '#addOrgModal').val( $('#addOrgModal').data('orgname') );
							$("#select_orgtype", '#addOrgModal').val( $('#addOrgModal').data('orgtype') );
							//$("#orgextent", this).val( $(this).data('orgextent') );
							
						}				
					});
				}
				
				$("#addOrgForm").submit(function(event) {
					event.preventDefault();
					//turn off the the add map point
					$(main.map.map.getViewport()).off('click');
					var params = {folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2]};
					var action = 'addOrg';
					var orgId = $('#addOrgModal').data('orgId');
					if( orgId ) {
						action = 'updateOrg';
						params.orgId = orgId;
					}
					params.orgname = $("#orgname").val();
					//params.orgextent = $("#orgextent").val();
					params.orgtype = $("#select_orgtype").val();
					params.geom = $("#geom").val();
					params.zoom = $("#zoom").val();
					//either add or update org
					$.post(main.loginPath+'dbOrgs.php', {
							action : action,
							params : params
						}).done(function(data) {
							var jData = JSON.parse(data);
							//$('#addOrgModal').modal('hide');
							getOrgs();
							if(main.filter_legend ){
								if(main.filter_legend.layerName=='organization'){
									main.filter_legend.refresh();
									main.loader.loadMoreMapPoints('organization');
								}
							}
							//location.reload(true);
							var orgId = $('#addOrgModal').data('orgId');
						if( !orgId ) {
							//add mode
							$("#add-edit-org-title", '#addOrgModal').text('Add Organization');

						} else {
							//edit org mode
							$("#add-edit-org-title", '#addOrgModal').text('Edit Organization');
							$("#orgId", '#addOrgModal').val( $('#addOrgModal').data('orgId') );
							$("#orgname", '#addOrgModal').val( $('#addOrgModal').data('orgname') );
							$("#select_orgtype", '#addOrgModal').val( $('#addOrgModal').data('orgtype') );
							//$("#orgextent", this).val( $(this).data('orgextent') );
							
						}			
						});

				});

/* 				$('#addOrgModal').on('shown.bs.modal', function (e) {
				
						var orgId = $(this).data('orgId');
						if( !orgId ) {
							//add mode
							$("#add-edit-org-title", this).text('Add Organization');

						} else {
							//edit org mode
							$("#add-edit-org-title", this).text('Edit Organization');
							
							$("#orgname", this).val( $(this).data('orgname') );
							$("#select_orgtype", this).val( $(this).data('orgtype') );
							//$("#orgextent", this).val( $(this).data('orgextent') );
							
						}				
					});
					 */
				$('#addOrgCancel').on('click', function (e) {

						$(this).data('orgId', null);
							
						$("#orgname", this).val( '' );
						$("#select_orgtype", this).val( '' );
						$("#add-edit-org-title", '#addOrgModal').text('');
							$("#orgId", '#addOrgModal').val( '' );
							$("#orgname", '#addOrgModal').val( '' );
							$("#select_orgtype", '#addOrgModal').val( '' );
						//$("#orgextent", this).val( '' );
							
					});
									
				
				
				//get data to populate org types drop-down
				$.post(main.loginPath+'dbOrgs.php', {
						action : 'getOrgTypes',
						params : {folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2]}
					}).done(function(data) {

						var jData = JSON.parse(data);
						jData = jData.orgTypes;
						var selectEl = document.getElementById("select_orgtype");
						
						//update org drop-down
						for (var i = 0; i < jData.length; i++) {
							var optionEl = document.createElement("option");
							optionEl.value = optionEl.text= jData[i];
							
							selectEl.appendChild( optionEl );
						};
					});
			};
		this.addManageAllUsersShelf=function(){
			self.manageAllUsers={};
			self.manageAllUsers.launchButton=new Button('toolBox','','All Users');
			self.manageAllUsers.manageAccountsHTML=$('<div class="accountManageActs Extended">').append('<p>Fill out the information to add a new user or double click on a record to edit that user. Click Submit</p><div class="modal fade" id="addAllUserModal"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 id="add-edit-AllUser-title" class="modal-title">Add/Edit User</h4></div><div class="modal-body"><form class="form-addAllUser" role="form" id="addAllUserForm"><input type="text" name="Allid" placeholder="ID" id="AllUserId" disabled="true" /><br><input type="text" name="AllUsername" placeholder="Username" id="AllUsername" required/><br><input type="email" name="email" placeholder="E-Mail" id="email3" required/><br><input type="password" name="password" placeholder="Password" id="password3" /><br>Role:<br><select id="Allselect_role" required><option></option></select><br>Organization:<br><select id="Allselect_org" required><option></option></select></form></div><div class="modal-footer"><button id="addAllUserCancel" type="button" class="button btn btn-default" >Cancel</button><button id="addAllUserSubmit" class="button btn btn-primary" type="submit" id="submit" form="addAllUserForm">Save Changes</button></div></div><!-- /.modal-content --></div><!-- /.modal-dialog --></div><table id="tblAllUsers" class="display" width="80%"><thead><tr><th>ID</th><th>Username</th><th>Email</th><th>Role</th><th>Organization</th><th class="editTH">&nbsp;</th></tr></thead></table>');
			
			self.manageAllUsers.manageAccountsShelf=self.toolBoxAccordian.createShelf({
				name:'manageAllUsers',
				button:self.manageAllUsers.launchButton.html,
				content:self.manageAllUsers.manageAccountsHTML,
				alias:'All Users'
			});
			self.manageAllUsers.shelf=self.toolBoxAccordian.shelves[self.manageAllUsers.manageAccountsShelf.data('name')];
			self.manageAllUsers.manageAccountsShelf.addClass('extended');
			self.addItShelves['manageAllUsers']=self.manageAllUsers;
			self.manageAllUsersJS();
			return self.manageAllUsers.manageAccountsShelf;
		};
		
		this.manageAllUsersJS=function(){
				//reset AllUser id on AllUser modal
				$('#addAllUserModal').data('AllUserId', null);
			
				ufcLoginApp.hideAdminButtons();
				ufcLoginApp.getLoggedInUserData();
										
				//$( "#logoutSpan" ).tooltip({'placement':'left'});
				//$( "#logoutSpan" ).on('click', function() { location.href='logout.php' } );
						
				$("a.navLink").click(function(evt) {
					//ufcLoginApp.loginVerify(evt.target.id);
				});
				
				//delete AllUser
				$('body').on('click','.deleteAllUser',function(e) {
					e.preventDefault();
					e.stopImmediatePropagation();
					if(confirm("Are you sure you want to delete this user?")==true){
						var id=$(this).closest('tr').find('.tableRowId').data('id');
						var params={
							ids : [id],
							folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]
						}
							$.post(main.loginPath+'dbUsers.php',{
									action : 'deleteUsers',
									params : params
								}).done(function(response){
									if(response){
										response=JSON.parse(response);
										if(response.status=="OK"){
											//location.reload(true);
										$.post(main.loginPath+'dbUsers.php', {
													action : 'getOrgId',
													params : params
												}).done(function(data) {
											
											var jData = JSON.parse(data);
											params.orgId = jData.orgId;
											getAllUsersByOrg( params );
										});
										}else{
											alert("Error");
										}
									}
								});
					}
				});

				//init empty dt
				$("#tblAllUsers").DataTable( {
						data: {},
						paging: false
					});	
	
				$("#addAllUserForm").submit(function(event) {
					event.preventDefault();

					var params = {folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]};
					var action = 'addUser';
					var AllUserId = $('#addAllUserModal').data('AllUserId');
					if( AllUserId ) {
						action = 'updateUserAndPassword';
						params.userId = AllUserId;
						//disable password
						 if($("#password3").val()==""){
							 action = 'updateUser';
						 }
					}
					params.username = $("#AllUsername").val();
					params.email = $("#email3").val();
					params.password = $("#password3").val();
					params.orgname = $("#Allselect_org").val();
					params.rolename = $("#Allselect_role").val();
					//console.log("Password: "&params[password]);
					//either add or update AllUser
					$.post(main.loginPath+'dbUsers.php', {
								action : 'getOrgId',
								params : params
							}).done(function(data) {
						
						var jData = JSON.parse(data);
						params.orgId = jData.orgId;
					$.post(main.loginPath+'dbUsers.php', {
							action : action,
							params : params
						}).done(function(data) {
							var jData = JSON.parse(data);
							
							
						
						getAllUsersByOrg( params );
					
					});
							//getAllUsersByOrg(params);
							//$('#addAllUserModal').modal('hide');
							
							//location.reload(true);
							
						});

				});
				
				//get data to populate orgs drop-down on add AllUser form
				$.post(main.loginPath+'dbUsers.php', {
						action : 'getOrganizations',
						params : {folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]}
					}).done(function(data) {

						//orgs as id-orgname
						var jData = JSON.parse(data);
						var selectEl = document.getElementById("Allselect_org");
						
						//update org drop-down
						for (var i = 0; i < jData.length; i++) {
							var optionEl = document.createElement("option");
							optionEl.setAttribute("data-org-id", jData[i].id);
							optionEl.value = optionEl.text= jData[i].orgname;

							selectEl.appendChild( optionEl );
						};

					});
				
				//get data to populate roles drop-down on add AllUser form
				$.post(main.loginPath+'dbUsers.php', {
						action : 'getRoles',
						params : {folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]}
					}).done(function(data) {

						//roles as id-rolename
						var jData = JSON.parse(data);
						var selectEl = document.getElementById("Allselect_role");
						
						//update org drop-down
						for (var i = 0; i < jData.length; i++) {
							var optionEl = document.createElement("option");
							optionEl.setAttribute("data-role-id", jData[i].pid);
							optionEl.value = optionEl.text= jData[i].alias;
							
							selectEl.appendChild( optionEl );
						};
					});


				//get org, use it to get AllUsers
				$.post(main.loginPath+'dbUsers.php', {
					action : 'getOrgId',
					params : {folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]}
				}).done(function(data) {
					var jData = JSON.parse(data);
					if(jData.status=="OK"){
						var params = {folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]};
						params.orgId = jData.orgId;
						getAllUsersByOrg( params );
					}else{
						if(jData.ERR_CODE==1){main.utilities.loggedOutNotice();}
					}
				});
				
				//get data to populate existing AllUsers table 
				// expects params.orgId
				function getAllUsersByOrg( params ) {	
					$.post(main.loginPath+'dbUsers.php', {
							action : 'getExistingUsers',
							params : params
						}).done(function(data) {
						
							//existing AllUsers as json data
							var jData = JSON.parse(data);
							var table = $("#tblAllUsers").DataTable();
							table.clear();
							//add delete button
							for(var i=0;i<jData.length;i++){
								jData[i][0]='<div class="tableRowId" data-id='+jData[i][0]+'>'+jData[i][0]+'</div>';
								jData[i].push('<a href="#" class="deleteAllUser">Delete</a>');
							}
							
							//initialize AllUsers table as a dataTable
                            if( !jQuery.isEmptyObject( jData ) ) {
                                initDataTableAllUsers( jData );
                            }
					});
				}
					
				//initialize datatable with received json data
				function initDataTableAllUsers( jData ) {
				
					$("#tblAllUsers").dataTable().fnAddData(jData);
					
					//rowclick event - capture AllUser id
					$dataTable = $("#tblAllUsers").DataTable();
					$("#tblAllUsers").dataTable().on( 'dblclick', 'tr', function() {
						
						//get AllUser data from table cells - presently by position
						var AllUserId = $(this).find('td').eq(0).text();
						var name = $(this).find('td').eq(1).text();
						var email = $(this).find('td').eq(2).text();
						var role = $(this).find('td').eq(3).text();
						var organization = $(this).find('td').eq(4).text();
						
						//add AllUser id to modal before opening for edit
						$('#addAllUserModal').data('AllUserId',AllUserId);
						$('#addAllUserModal').data('name',name);
						$('#addAllUserModal').data('email',email);
						$('#addAllUserModal').data('role',role);
						$('#addAllUserModal').data('organization',organization);
						
						//$('#addAllUserModal').modal('show');
						
						var AllUserId = $('#addAllUserModal').data('AllUserId');
						if( !AllUserId ) {
							//add mode
							$("#add-edit-AllUser-title", '#addAllUserModal').text('Add User');

							$('#AllUsername').attr('disabled', false);
							
						} else {
							//edit AllUser mode
							$("#add-edit-AllUser-title", '#addAllUserModal').text('Edit User');
							$('#password3').attr('disabled', true);
							//$('#AllUsername').attr('disabled', true);
							$("#AllUserId", '#addAllUserModal').val( $('#addAllUserModal').data('AllUserId') );
							$("#AllUsername", '#addAllUserModal').val( $('#addAllUserModal').data('name') );
							$("#email3", '#addAllUserModal').val( $('#addAllUserModal').data('email') );
							$("#Allselect_role", '#addAllUserModal').val($('#addAllUserModal').data('role'));
							$("#Allselect_org", '#addAllUserModal').val( $('#addAllUserModal').data('organization') );
						}
					});
				}
				
/* 				$('#addAllUserModal').on('shown.bs.modal', function (e) {
				
						var AllUserId = $(this).data('AllUserId');
						if( !AllUserId ) {
							//add mode
							$("#add-edit-AllUser-title", this).text('Add User');

							$('#AllUsername').attr('disabled', false);
							
						} else {
							//edit AllUser mode
							$("#add-edit-AllUser-title", this).text('Edit User');
							
							//$('#AllUsername').attr('disabled', true);
							$("#AllUserId", this).val( $(this).data('AllUserId') );
							$("#AllUsername", this).val( $(this).data('name') );
							$("#email3", this).val( $(this).data('email') );
							$("#Allselect_role", this).val($(this).data('role'));
							$("#Allselect_org", this).val( $(this).data('organization') );
							
						}				
					}); */
					
				$('#addAllUserCancel').on('click', function (e) {

						$(this).data('AllUserId', null);
							
						$("#AllUsername", this).val( '' );
						$("#email", this).val( '' );
						$("#select_role", this).val( '' );
						$("#select_org", this).val( '' );
							$("#AllUserId", this).val( '' );
							$("#AllUsername", this).val( '' );
							$("#email3", this).val( '' );
							$("#Allselect_role", this).val('' );
							$("#Allselect_org", this).val('' );
					});
		};
		this.addManageSelfRegUsersShelf=function(){
			self.manageSelfRegUsers={};
			self.manageSelfRegUsers.launchButton=new Button('toolBox','','Self Registered Users');
			self.manageSelfRegUsers.manageAccountsHTML=$('<div class="accountManageActs Extended">').append('<p>Double click on a record to edit the self registered user. Click submit.</p><div class="modal fade" id="addSelfRegUserModal"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 id="add-edit-SelfRegUser-title" class="modal-title">Edit Self Registered User</h4></div><div class="modal-body"><form class="form-addSelfRegUser" role="form" id="addSelfRegUserForm"><input type="text" name="SelfRegUserId" placeholder="ID" id="SelfRegUserId" disabled="true" required/><br><input type="text" name="SelfRegUsername" placeholder="Username" id="SelfRegUsername" required/><br><input type="email" name="email" placeholder="E-Mail" id="email4" required/><br><input type="password" name="password4" placeholder="Password" id="password5" disabled="true" /><br>Role:<br><select id="select_role4" required><option></option></select><br>Organization:<br><select id="select_org4" required><option></option></select></form></div><div class="modal-footer"><button id="addSelfRegUserCancel" type="button" class="button btn btn-default" data-dismiss="modal">Cancel</button><button id="addSelfRegUserSubmit" class="button btn btn-primary" type="submit" id="submit" form="addSelfRegUserForm">Save Changes</button></div></div><!-- /.modal-content --></div><!-- /.modal-dialog --></div><table id="tblSelfRegUsers" class="display" width="80%"><thead><tr><th>ID</th><th>Username</th><th>Email</th><th>Role</th><th>Organization</th><th>Comment</th><th class="editTH">&nbsp;</th><th class="editTH">&nbsp;</th></tr></thead></table>');
			
			self.manageSelfRegUsers.manageAccountsShelf=self.toolBoxAccordian.createShelf({
				name:'manageSelfRegUsers',
				button:self.manageSelfRegUsers.launchButton.html,
				content:self.manageSelfRegUsers.manageAccountsHTML,
				alias:'Self Registered Users'
			});
			self.manageSelfRegUsers.shelf=self.toolBoxAccordian.shelves[self.manageSelfRegUsers.manageAccountsShelf.data('name')];
			self.manageSelfRegUsers.manageAccountsShelf.addClass('extended');
			self.addItShelves['manageSelfRegUsers']=self.manageSelfRegUsers;
			self.manageSelfRegUsersJS();
			return self.manageSelfRegUsers.manageAccountsShelf;
		};
		
		this.manageSelfRegUsersJS=function(){
				//reset SelfRegUser id on SelfRegUser modal
				$('#addSelfRegUserModal').data('SelfRegUserId', null);
			
				ufcLoginApp.hideAdminButtons();
				ufcLoginApp.getLoggedInUserData();
										
				//$( "#logoutSpan" ).tooltip({'placement':'left'});
				//$( "#logoutSpan" ).on('click', function() { location.href='logout.php' } );
						
				$("a.navLink").click(function(evt) {
					//ufcLoginApp.loginVerify(evt.target.id);
				});
				
				//delete SelfRegUser
				$('body').on('click','.rejectSelfRegUser',function(e) {
					e.preventDefault();
					
						var id=$(this).closest('tr').find('.tableRowId').data('id');
						var params={
							ids : id,
							title:self.users_settings.title,
							from:main.personal_settings.account['from_email'],
							folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]
						}
						$.post(main.loginPath+'dbUsers.php',{
								action : 'rejectSelfRegisteredUser',
								params : params
							}).done(function(response){
								if(response){
									response=JSON.parse(response);
									if(response.status=="OK"){
										//location.reload(true);
										var table = $("#tblSelfRegUsers").DataTable();
										table.clear();
										getSelfRegUsersByOrg(params);
									}else{
										alert("Error");
									}
								}
							});
					
				});
				//approve SelfRegUser
				$('body').on('click','.approveSelfRegUser',function(e) {
					e.preventDefault();
					
						var id=$(this).closest('tr').find('.tableRowId').data('id');
						var params={
							ids : id,
							title:self.users_settings.title,
							from:main.personal_settings.account['from_email'],
							default_logged_in_user:self.default_logged_in_user,
							folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]
						}
						$.post(main.loginPath+'dbUsers.php',{
								action : 'approveSelfRegisteredUser',
								
								params : params
							}).done(function(response){
								if(response){
									response=JSON.parse(response);
									if(response.success==true){
										//location.reload(true);
										var table = $("#tblSelfRegUsers").DataTable();
										table.clear();
										getSelfRegUsersByOrg(params);
									}else{
										alert("Error");
									}
								}
							});
					
				});

				//init empty dt
				$("#tblSelfRegUsers").DataTable( {
						data: {},
						paging: false
					});	
	
				$("#addSelfRegUserForm").submit(function(event) {
					event.preventDefault();

					var params = {folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]};
					var action = 'addUser';
					var SelfRegUserId = $('#addSelfRegUserModal').data('SelfRegUserId');
					if( SelfRegUserId ) {
						action = 'updateUserAndPassword';
						params.SelfRegUserId = SelfRegUserId;
					}
					params.username = $("#SelfRegUsername").val();
					params.email = $("#email4").val();
					params.password = $("#password5").val();
					params.orgname = $("#select_org4").val();
					params.rolename = $("#select_role4").val();
					//console.log("Password: "&params[password]);
					//either add or update SelfRegUser
					
					if( action == 'updateUserAndPassword'){
					$.post(main.loginPath+'dbUsers.php', {
							action : action,
							params : params
						}).done(function(data) {
							var jData = JSON.parse(data);
							var table = $("#tblSelfRegUsers").DataTable();
							table.clear();
							getSelfRegUsersByOrg(params);
							//$('#addSelfRegUserModal').modal('hide');
							
						//	location.reload(true);
							
						});
					}
					else{
						alert('Use this funcitonality to edit self registered users. Use All Users to add new users.');
					}
				});
				
				//get data to populate orgs drop-down on add SelfRegUser form
				$.post(main.loginPath+'dbUsers.php', {
						action : 'getOrganizations',
						params : {folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]}
					}).done(function(data) {

						//orgs as id-orgname
						var jData = JSON.parse(data);
						var selectEl = document.getElementById("select_org4");
						
						//update org drop-down
						for (var i = 0; i < jData.length; i++) {
							var optionEl = document.createElement("option");
							optionEl.setAttribute("data-org-id", jData[i].id);
							optionEl.value = optionEl.text= jData[i].orgname;

							selectEl.appendChild( optionEl );
						};

					});
				
				//get data to populate roles drop-down on add SelfRegUser form
				$.post(main.loginPath+'dbUsers.php', {
						action : 'getRoles',
						params : {folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]}
					}).done(function(data) {

						//roles as id-rolename
						var jData = JSON.parse(data);
						var selectEl = document.getElementById("select_role4");
						
						//update org drop-down
						for (var i = 0; i < jData.length; i++) {
							var optionEl = document.createElement("option");
							optionEl.setAttribute("data-role-id", jData[i].pid);
							optionEl.value = optionEl.text= jData[i].alias;
							
							selectEl.appendChild( optionEl );
						};
					});


				//get org, use it to get SelfRegUsers
				$.post(main.loginPath+'dbUsers.php', {
						action : 'getOrgId',
						params :{folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]}
					}).done(function(data) {
						
						var jData = JSON.parse(data);
						var params ={folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]};
						params.orgId = jData.orgId;
						
						getSelfRegUsersByOrg( params );
					
					});
				
				//get data to populate existing SelfRegUsers table 
				// expects params.orgId
				function getSelfRegUsersByOrg( params ) {	
					$.post(main.loginPath+'dbUsers.php', {
							action : 'getSelfRegisteredUsers',
							params : params
						}).done(function(data) {

							//existing SelfRegUsers as json data
							var jData = JSON.parse(data);
							var table = $("#tblSelfRegUsers").DataTable();
							table.clear();
							//add delete button
							for(var i=0;i<jData.length;i++){
								jData[i][0]='<div class="tableRowId" data-id='+jData[i][0]+'>'+jData[i][0]+'</div>';
								jData[i].push('<a href="#" class="approveSelfRegUser">Approve</a>');
								jData[i].push('<a href="#" class="rejectSelfRegUser">Reject</a>');
							}
							
							//initialize SelfRegUsers table as a dataTable
                            if( !jQuery.isEmptyObject( jData ) ) {
                                initDataTableSelfRegUsers( jData );
                            }
					});
				}
					
				//initialize datatable with received json data
				function initDataTableSelfRegUsers( jData ) {
				
					$("#tblSelfRegUsers").dataTable().fnAddData(jData);
					
					//rowclick event - capture SelfRegUser id
					$dataTable = $("#tblSelfRegUsers").DataTable();
					$("#tblSelfRegUsers").dataTable().on( 'dblclick', 'tr', function() {
						
						//get SelfRegUser data from table cells - presently by position
						var SelfRegUserId = $(this).find('td').eq(0).text();
						var name = $(this).find('td').eq(1).text();
						var email = $(this).find('td').eq(2).text();
						var role = $(this).find('td').eq(3).text();
						var organization = $(this).find('td').eq(4).text();
						
						//add SelfRegUser id to modal before opening for edit
						$('#addSelfRegUserModal').data('SelfRegUserId',SelfRegUserId);
						$('#addSelfRegUserModal').data('name',name);
						$('#addSelfRegUserModal').data('email',email);
						$('#addSelfRegUserModal').data('role',role);
						$('#addSelfRegUserModal').data('organization',organization);
						
						//$('#addSelfRegUserModal').modal('show');
						var SelfRegUserId = $('#addSelfRegUserModal').data('SelfRegUserId');
						if( !SelfRegUserId ) {
							//add mode
							//$("#add-edit-SelfRegUser-title", '#addSelfRegUserModal').text('Add SelfRegUser');

							$('#SelfRegUsername').attr('disabled', false);
							
						} else {
							//edit SelfRegUser mode
							//$("#add-edit-SelfRegUser-title", '#addSelfRegUserModal').text('Edit SelfRegUser');
							
							$('#SelfRegUsername').attr('disabled', true);
							$("#SelfRegUserId", '#addSelfRegUserModal').val( $('#addSelfRegUserModal').data('SelfRegUserId') );
							$("#SelfRegUsername", '#addSelfRegUserModal').val( $('#addSelfRegUserModal').data('name') );
							$("#email4", '#addSelfRegUserModal').val( $('#addSelfRegUserModal').data('email') );
							$("#select_role4", '#addSelfRegUserModal').val( $('#addSelfRegUserModal').data('role') );
							$("#select_org4", '#addSelfRegUserModal').val( $('#addSelfRegUserModal').data('organization') );
							
						}				
					});
				}
				
/* 				$('#addSelfRegUserModal').on('shown.bs.modal', function (e) {
				
						var SelfRegUserId = $(this).data('SelfRegUserId');
						if( !SelfRegUserId ) {
							//add mode
							$("#add-edit-SelfRegUser-title", this).text('Add SelfRegUser');

							$('#SelfRegUsername').attr('disabled', false);
							
						} else {
							//edit SelfRegUser mode
							$("#add-edit-SelfRegUser-title", this).text('Edit SelfRegUser');
							
							$('#SelfRegUsername').attr('disabled', true);
							
							$("#SelfRegUsername", this).val( $(this).data('name') );
							$("#email", this).val( $(this).data('email') );
							$("#select_role", this).val( $(this).data('role') );
							$("#select_org", this).val( $(this).data('organization') );
							
						}				
					});
					 */
				$('#addSelfRegUserCancel').on('click', function (e) {

						$(this).data('SelfRegUserId', null);
							
						$("#SelfRegUsername", this).val( '' );
						$("#email", this).val( '' );
						$("#select_role", this).val( '' );
						$("#select_org", this).val( '' );
							$("#SelfRegUserId", '#addSelfRegUserModal').val( '' );
							$("#SelfRegUsername", '#addSelfRegUserModal').val( '' );
							$("#email4", '#addSelfRegUserModal').val( '' );
							$("#select_role4", '#addSelfRegUserModal').val( '' );
							$("#select_org4", '#addSelfRegUserModal').val( '' );
							$('#SelfRegUsername').attr('disabled', false);
							$("#SelfRegUserId", '#addSelfRegUserModal').val( '');
							$("#SelfRegUsername", '#addSelfRegUserModal').val( '' );
							$("#email4", '#addSelfRegUserModal').val( '' );
							$("#select_role4", '#addSelfRegUserModal').val( '' );
							$("#select_org4", '#addSelfRegUserModal').val( '' );
					});
		};
		this.addManageMyActShelf=function(){self.manageMyAct={};self.manageMyAct.launchButton=new Button('toolBox','','Change Password');
		self.manageMyAct.manageAccountsHTML=$('<div class="accountManageActs">').html('<form class="form-loginSystem" role="form" id="profileUpdateForm"><div id="pageErrorMessageManage" class="errorText"></div><p>Change your email address or password - leave a field blank if you do not want to change it.</p><input type="email" class="form-control" placeholder="E-Mail" id="email_update"><br><input type="password" class="form-control" placeholder="Password" id="password_update"><input type="password" class="form-control" placeholder="Password Again" id="password2"><br><button class="button btn btn-lg btn-primary btn-block" type="submit" id="submit">Submit</button></form><script type="text/javascript">$(function() {$("#pageErrorMessageManage").css( "display", "none");function displayFail( txt ) {$("#pageErrorMessageManage").css( "display", "block");$("#pageErrorMessageManage").css( "color", "rgb(200,0,0)");$("#pageErrorMessageManage").empty();$("#pageErrorMessageManage").append("<p>"+txt+"</p>");}function displaySuccess( txt ) {$("#pageErrorMessageManage").css( "display", "block");	$("#pageErrorMessageManage").css( "color", "rgb(0,0,0)");$("#pageErrorMessageManage").empty();$("#pageErrorMessageManage").append("<p>"+txt+"</p>");}$("#profileUpdateForm").submit( function( event ) {event.preventDefault();params = {folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]};if( $("#password_update").val() === "" && $("#email_update").val() === "" ) {return;}if( $("#password_update").val() != "" ) {	if( $("#password_update").val() != $("#password2").val() ) {alert("The passwords have to match.");return;} else {params.password = $("#password_update").val();}}params.email = $("#email_update").val();params.userID = main.account.loggedInUser.pid;var action = "updateUserProfile";$.post( main.loginPath+"dbUsers.php", {params: params, action: action} ).done( function(data){var jData = JSON.parse(data);if( jData.success ) {displaySuccess( "User Updated" );} else {displayFail( jData.error );}});});});</script>');
			self.manageMyAct.manageAccountsShelf=self.toolBoxAccordian.createShelf({
				name:'manageMyAct',
				button:self.manageMyAct.launchButton.html,
				content:self.manageMyAct.manageAccountsHTML,
				alias:'Change Password'
			});
			self.manageMyAct.shelf=self.toolBoxAccordian.shelves[self.manageMyAct.manageAccountsShelf.data('name')];
			self.addItShelves['manageMyAcct']=self.manageMyAct;
			return self.manageMyAct.manageAccountsShelf;
		};
		this.addRegShelf=function(){
			self.selfReg={};
			self.selfReg.launchButton=new Button('toolBox','','Request an Account');
			var tooltip='';
			if(main.personal_settings.app_settings.request_account_hover_tip_content){
				self.selfReg.moreInfo=new MoreInfo({
					content:main.personal_settings.app_settings.request_account_hover_tip_content
				});
				tooltip=self.selfReg.moreInfo.html;
			}
			var h2='<h2 class="form-loginSystem-heading">Enter your information</h2>';
			var request_account_instr;
			if(self.accountSettings.request_account_instr){
				h2='';
				request_account_instr='<div class="stdSection">'+self.accountSettings.request_account_instr+'</div>';
			}
			var form=$('<form class="form-loginSystem" role="form" id="registerform">')
				.append(h2)
				.append(request_account_instr)
				.append('<div id="pageErrorMessageRegister" class="errorText"></div>')
				.append(tooltip)
				.append('<input type="text" class="form-control" placeholder="Username" id="username_register" required autofocus>'+
						'<input type="email" class="form-control" placeholder="E-Mail" id="email_register" required>'+
						'<input type="password" class="form-control" placeholder="Password" id="password_register" required>'+
						'<input type="password" class="form-control" placeholder="Re-Type Password" id="password2_register" required>'+
						'<br>'+
						'<span data-toggle="tooltip" data-placement="left" title="Do not see the organization you are a member of?  Request a new organization be created using the comment form below.">Select an Organization:</span>'+
						'<select id="select_org_register" class="form-control"  required>'+
							'<option></option>'+
						'</select>'+
						'<br>'+
						'<textarea class="form-control" placeholder="Enter comments here - for example, you can request that a new organization be created" id="comment_register" maxlength="200" rows="5"></textarea>'+
						'<br>'+
						'<button class="button btn btn-lg btn-primary btn-block" type="submit" id="submit">Register</button>');
			
			self.selfReg.manageAccountsHTML=$('<div class="accountRequest">').append(form);
			
			self.selfReg.manageAccountsShelf=self.toolBoxAccordian.createShelf({
				name:'selfReg',
				button:self.selfReg.launchButton.html,
				content:self.selfReg.manageAccountsHTML,
				alias:'Request an Account'
			});
			self.selfReg.shelf=self.toolBoxAccordian.shelves[self.selfReg.manageAccountsShelf.data('name')];
			self.addItShelves['selfReg']=self.selfReg;
			self.registerJS();
		};
		this.registerJS=function(){
		$("#pageErrorMessageRegister").css( "display", "none");

		function displayRegisterFail( txt ) {
			$("#pageErrorMessageRegister").css( "display", "block");
			$("#pageErrorMessageRegister").css( "color", "rgb(200,0,0)");
			
			$("#pageErrorMessageRegister").empty();
			$("#pageErrorMessageRegister").append('<p>'+txt+'</p>');
		}

		$("#registerform").submit( function( event ) {
			event.preventDefault();
			
			var params = {folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]};
			params.username = $("#username_register").val();
			params.password = $("#password_register").val();
			params.email = $("#email_register").val();
			params.comment = $("#comment_register").val();
			params.orgId = $("#select_org_register option:selected").val();
			params.Toemail= main.personal_settings.account['to_email'];
			params.title=self.users_settings.title;
			
			if( $("#password_register").val() != "" ) {			
				if( $("#password_register").val() != $("#password2_register").val() ) {
					alert("The passwords have to match.");
					return;
				} else {
					params.password = $("#password_register").val();
				}
			}
			
			$.post( main.loginPath+'selfRegister.php', { action: 'selfRegister',params: params } )
				.done( function(data){
					var jData = JSON.parse(data);
					if( jData.success) {
						displayRegisterFail("You successfully registered, and will be emailed once your login is approved.");
					} else {
						displayRegisterFail(jData.error);			
					}
				});
		});
		
		//get data to populate orgs drop-down
		$.post(main.loginPath+'selfRegister.php', {
				action : 'getOrgs',
				params : {folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]}
			}).done(function(data) {

				//orgs as id-orgname
				var jData = JSON.parse(data);
				var selectEl = document.getElementById("select_org_register");
				
				//update org drop-down
				for (var i = 0; i < jData.length; i++) {
					var optionEl = document.createElement("option");
					optionEl.text = jData[i][1];
					optionEl.value  = jData[i][0];

					selectEl.appendChild( optionEl );
				};

			});
		};
		this.addForgotUserShelf=function(){
			self.forgotUser={};
			self.forgotUser.launchButton=new Button('toolBox','','Forgot User Name');
			self.forgotUser.manageAccountsHTML=$('<div class="accountforgotUserName">').html(
			  '<form class="form-loginSystem" role="form" id="userResetForm">'+
				'<h3 class="form-loginSystem-heading">Request your User Name</h3>'+
				'<div id="pageErrorMessageForgotUsername" class="errorText">'+
				'</div>'+
				'<div id="requestname"><p>Enter your email to request reminder</p>'+
				'<input type="text" class="form-control" placeholder="E-Mail" id="email1" required autofocus>'+
				'<button class="button btn btn-lg btn-primary btn-block" type="button" id="sendName">Send reminder</button>'+
				
				'</div>'+
			'</form>'+
			'<br>'+
			  '<span>');
			self.forgotUser.manageAccountsShelf=self.toolBoxAccordian.createShelf({
				name:'forgotUser',
				button:self.forgotUser.launchButton.html,
				content:self.forgotUser.manageAccountsHTML,
				alias:'Forgot User Name'
			});
			self.forgotUser.shelf=self.toolBoxAccordian.shelves[self.forgotUser.manageAccountsShelf.data('name')];
			self.addItShelves['manageName']=self.forgotUser;
			self.manageUserJS();
		};
		this.manageUserJS=function(){
					$("#pageErrorMessageForgotUsername").css( "display", "none");

				function displayLoginFail( txt ) {
					$("#pageErrorMessageForgotUsername").css( "display", "block");
					$("#pageErrorMessageForgotUsername").css( "color", "rgb(200,0,0)");
					
					$("#pageErrorMessageForgotUsername").empty();
					$("#pageErrorMessageForgotUsername").append('<p>'+txt+'</p>');
				}
					
					$("#request").css( "display", "block");
					
				$("#sendName").on( 'click', function( ) {
							
						var name = $("#email1").val();
						
						if( name === "" ) {
							alert("Please enter an email.");
							return;
						}
						
						var action = 'emailUserName';
						var params = {email:name,
						url:window.location.href,
						title:self.users_settings.title+" Requested Username",
						from:main.personal_settings.account['from_email'],
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2]
						};

						$.post( main.loginPath+'pw_reminder.php', {action:action, params:params})
							.done( function(data){
								var jData = JSON.parse(data);
								
									if( jData.success ) {
								$("#pageErrorMessageForgotUsername").css( "display", "block");
								$("#pageErrorMessageForgotUsername").css( "color", "rgb(0,0,0)");
					
								$("#pageErrorMessageForgotUsername").empty();
								$("#pageErrorMessageForgotUsername").append('<p>UserName Sent </p>');
									
								} else {
									displayLoginFail(jData.error);
								}
							
							});
				});
		};
		this.addForgotShelf=function(){
			self.forgotPword={};
			self.forgotPword.launchButton=new Button('toolBox','','Forgot Password');
			//self.forgotPword.forgotPwordBtn=new Button('std','forgotBtn','Forgot Password');
			//self.forgotPword.forgotPwordBtn.html.click(function(){window.open('../login/login.html');});
			self.forgotPword.manageAccountsHTML=$('<div class="accountforgotPassword">').html(
			  '<form class="form-loginSystem" role="form" id="pwResetForm">'+
				'<h3 class="form-loginSystem-heading">Change your password</h3>'+
				'<div id="pageErrorMessageForgot" class="errorText">'+
				'</div>'+
				'<div id="request"><p>Enter your username and request a secret code.</p>'+
				'<input type="text" class="form-control" placeholder="Username" id="username1" autofocus>'+
				'<button class="button btn btn-lg btn-primary btn-block" type="button" id="sendCode">Send the Secret Code</button>'+
				'<button class="button btn btn-lg btn-primary btn-block" type="button" id="haveCode">Already have a Secret Code</button>'+
				'</div>'+
				'<div id="reset">'+
				'<p>Enter your username, new password, and the secret code emailed to you.</p>'+
				'<input type="text" class="form-control" placeholder="Username" id="username" required autofocus>'+
				'<br><input type="password" class="form-control" placeholder="Password" id="password" required>'+
				'<input type="password" class="form-control" placeholder="Password Again" id="password2" required>'+
				'<input type="secretCode" class="form-control" placeholder="Secret Code" id="secretCode" required>'+
				'<br>'+
				'<button class="button btn btn-lg btn-primary btn-block" type="submit" id="submit">Submit</button>'+
				'<button class="button btn btn-lg btn-primary btn-block" type="button" id="needCode">Need a Secret Code</button>'+
				'</div>'+
			'</form>'+
			'<br>'+
			  '<span>');
			self.forgotPword.manageAccountsShelf=self.toolBoxAccordian.createShelf({
				name:'forgotPassword',
				button:self.forgotPword.launchButton.html,
				content:self.forgotPword.manageAccountsHTML,
				alias:'Forgot Password'
			});
			self.forgotPword.shelf=self.toolBoxAccordian.shelves[self.forgotPword.manageAccountsShelf.data('name')];
			self.addItShelves['managePword']=self.forgotPword;
			self.managePwordJS();
		};
		this.managePwordJS=function(){
					$("#pageErrorMessageForgot").css( "display", "none");

				function displayLoginFail( txt ) {
					$("#pageErrorMessageForgot").css( "display", "block");
					$("#pageErrorMessageForgot").css( "color", "rgb(200,0,0)");
					
					$("#pageErrorMessageForgot").empty();
					$("#pageErrorMessageForgot").append('<p>'+txt+'</p>');
				}
					$("#reset").css( "display", "none");
					$("#request").css( "display", "block");
					
				$("#sendCode").on( 'click', function( ) {
							
						var name = $("#username1").val();
						
						if( name === "" ) {
							alert("Please enter a username.");
							return;
						}
						
						var action = 'emailPasswordResetLink';
						var params = {name:name,
						url:window.location.href,
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						title:self.users_settings.title+" Password Reset Link",
						from:main.personal_settings.account['from_email']
						};

						$.post( main.loginPath+'pw_reminder.php', {action:action, params:params})
							.done( function(data){
								var jData = JSON.parse(data);
								if( jData.success) {
									alert('Please check your email to reset your password.');
									$("#reset").css( "display", "block");
									$("#request").css( "display", "none");
								} else {
									displayLoginFail(jData.error);
								}
						});
				});
				$("#haveCode").on( 'click', function( ) {
					$("#reset").css( "display", "block");
					$("#request").css( "display", "none");

				});
				$("#needCode").on( 'click', function( ) {
					$("#reset").css( "display", "none");
					$("#request").css( "display", "block");

				});
				$("#pwResetForm").submit( function( event ) {
					event.preventDefault();
					
					if( $("#password").val() != $("#password2").val() ) {
						alert("The passwords have to match.");
					}
					
					params = {};
					params.name = $("#username").val();
					params.password = $("#password").val();
					params.secretCode = $("#secretCode").val();
					params.folder=window.location.pathname.split('/')[window.location.pathname.split('/').length-2];
					
					$.post( main.loginPath+'pw_reset.php', params )
						.done( function(data){
							var jData = JSON.parse(data);
							if( jData.success ) {
								$("#pageErrorMessageForgot").css( "display", "block");
								$("#pageErrorMessageForgot").css( "color", "rgb(0,0,0)");
					
								$("#pageErrorMessageForgot").empty();
								$("#pageErrorMessageForgot").append('<p>Password Reset</p>');
							} else {
								displayLoginFail( jData.error );			
							}
						});
				});	
		};
		this.addManageShelf=function(){
			self.manageActs={};
			
			self.manageActs.launchButton=new Button('toolBox','','Manage Account');
			//self.manageActs.manageActsBtn=new Button('std','manageActsBtn','Manage Account');
			//self.manageActs.manageActsBtn.html.click(function(){window.open('../login/login.html');});
			self.manageActs.manageAccountsHTML=$('<div class="accountManageActs">').append(this.addManageMyActShelf());
			if(main.personal_settings.account[self.loggedInUser.user_type+'_manage_org']){
				self.manageActs.manageAccountsHTML.append(this.addManageOrgShelf());
			}
			if(main.personal_settings.account['request_accounts']&& main.personal_settings.account[self.loggedInUser.user_type+'_manage_users']){ 
					self.manageUsers={};
					self.manageUsers.manageUsersHTML=$('<div class="accountManageUsers">');
					self.manageUsers.launchButton=new Button('toolBox','','Users');
				if(main.personal_settings.account[self.loggedInUser.user_type+'_manage_users']){
					self.manageUsers.manageUsersHTML.append(this.addManageAllUsersShelf());
				}
					self.manageUsers.manageUsersHTML.append(this.addManageSelfRegUsersShelf());
					self.manageUsers.manageUsersShelf=self.toolBoxAccordian.createShelf({
					name:'manageUsers',
					button:self.manageUsers.launchButton.html,
					content:self.manageUsers.manageUsersHTML,
					alias:'Users'});
					self.manageActs.manageAccountsHTML.append(self.manageUsers.manageUsersShelf);
					self.manageUsers.shelf=self.toolBoxAccordian.shelves[self.manageUsers.manageUsersShelf.data('name')];
					self.addItShelves['manageUsers']=self.manageUsers;
			}
			else{ // no self reg users no need for sub user shelf
				if(main.personal_settings.account[self.loggedInUser.user_type+'_manage_users']){
					self.manageActs.manageAccountsHTML.append(this.addManageAllUsersShelf());
				}
			}
			self.manageActs.manageAccountsShelf=self.toolBoxAccordian.createShelf({
				name:'manageAccounts',
				button:self.manageActs.launchButton.html,
				content:self.manageActs.manageAccountsHTML,
				alias:'Manage Account'
			});
			self.manageActs.shelf=self.toolBoxAccordian.shelves[self.manageActs.manageAccountsShelf.data('name')];
			self.addItShelves['manageAccounts']=self.manageActs;
		};
		this.preHTMLAdd=function(){
			var buttonAlias=main.withs.with_account.alias || self.alias;
			if(main.withs.with_account.icon_path){var buttonImg='images/'+main.withs.with_account.icon_path}
			else{var buttonImg=main.mainPath+'images/key.png';}
			if(!self.inToolBox){
				main.utilities.addLayoutItems(self.name,buttonAlias,self,buttonImg,'');
			}else{
				//shelf
				self.launchButton=new Button('toolBox','',self.alias);
				self.toolBoxAccordian=new Accordian(self.name);
				self.shelfContent=self.toolBoxAccordian.container;
				self.shelf=main.layout.toolBox.createShelf({
					name:'account',
					button:self.launchButton.html,
					content:self.shelfContent,
					alias:self.alias,
					groups:main.withs[self.withItem].toolbox_groups
				});
				main.toolBox.sortItems('abc');
			}
		};
		this.removeShelves=function(){
			for(var key in self.addItShelves){
				self.addItShelves[key].shelf.shelf.remove();
				delete self.addItShelves[key];
			}
		};
		this.addShelves=function(){
			if(self.loggedInUser.user_type=='public_1'){
				self.addForgotShelf();
				self.addForgotUserShelf();
				if(main.personal_settings.account['request_accounts']){
					self.addRegShelf();
				}
			}
			if(self.loggedInUserType!='public_1'){
			// if(field[self.loggedInUser.user_type+'_visible']){
				self.addManageShelf();
			}
		};
		this.createHTML=function(){
			this.accountBoxContent=$('<div>').addClass('accountBoxContent')
			this.accountBoxEmail=$('<div>').addClass('accountBoxInfoValue accountBoxInfoEle')
			this.accountBoxLogout=$('<div>').addClass('accountLogOut accountBoxButton').text('Log Out')
			this.accountBox=$('<div>').addClass('accountBox')
				.append($('<div>').addClass('accountBoxHeader')/* .text('Account') */)
				.append(this.accountBoxContent
					.append($('<div>').addClass('accountBoxEmail accountBoxInfo cf')
						.append($('<div>').addClass('accountBoxInfoLabel accountBoxInfoEle').html('Welcome,'))
						.append(this.accountBoxEmail)
					)
					.append($('<div>').addClass('accountBoxButtons')
						.append(this.accountBoxLogout)
					)
				);
			$('.accountBoxHolder').append(this.accountBox);
		};
		this.showAccountBox=function(){
			self.accountBox.slideDown(main.animations.defaultDuration).css('display','').addClass('block');
		};
		this.hideAccountBox=function(){
			self.accountBox.slideUp(main.animations.defaultDuration).css('display','').removeClass('block');
		};
		this.updateAccountBox=function(){
			if(self.loggedInUser.username){
				var name=self.loggedInUser.username;
				self.accountBoxEmail.html(name);
			}
		};
		this.initAccountBox=function(){
			self.updateAccountBox();
			self.showAccountBox();
		};
		this.initEvents=function(){
			this.accountBoxLogout.on('click',function(){
				self.login.logOut();
			});
		};
		this.checkForSession=function(){
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2]
					},
					action:'sessionCheck'
				},
				timeout:10000,
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							if(response.logThemOut && !self.login.recentLogOut){
								self.login.logOut();
								// alert('You have been logged out due to inactivity. To continue, please log back in.');
							}
						}else{
							alert('Error: '+response.message);
							console.log(response);
						}
					}
				}
			});
		};
		this.offSessionCheck=function(){
			clearInterval(self.sessionCheckInterval);
			self.sessionCheckInterval=null;
		};
		this.initSessionCheck=function(){
			self.sessionCheckInterval=setInterval(self.checkForSession,30000);
		};
		this.init=function(){
			this.preHTMLAdd();
			this.createHTML();
			var LogIn=require('./LogIn');
			this.login=new LogIn(this);
			this.addShelves();
			this.initEvents();
		};
		this.init();
	};
});