define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Panel=require('./Panel');
		var Tabs=require('./Tabs');
		this.withItem='with_time_compare';
		this.alias='Time Comparison';
		this.name='time_compare';
		this.defaultSortOrder='ASC';
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.addShelf=function(){
			self.launchButton=new Button('toolBox','',self.alias);
			self.shelf=main.layout.toolBox.createShelf({
				name:self.name,
				button:self.launchButton.html,
				content:self.html,
				context:self,
				alias:self.alias,
				dontAppendTo:true,
				groups:main.withs[self.withItem].toolbox_groups
			});
			main.layout.toolBox.shelfInAppTest(main.layout.toolBox.shelves[self.name]);
		};
		this.createListTR=function(item,luListMap){
			var checked='';
			if(self.itemsArray.indexOf(item.pid.toString())>-1){checked=' checked="checked"';}
			var tr=$('<tr class="luEditListTR luEditListTRPID_'+item.pid+'" data-item="'+item.pid+'">').append($('<td>').append($('<div class="tdLiner luListEditEditCell">').append('<input type="checkbox" class="luListEditCB"'+checked+'/>')));
			for(var key in luListMap){
				tr.append($('<td class="luEditUnitTD" data-itemunit="'+luListMap[key]+'" data-itemunitkey="'+key+'">').append($('<div class="tdLiner luListEditUnit">').append($('<div class="luEditCurrent">').html(item[luListMap[key]]))));
			}
			return tr;
		};
		this.clear=function(){
			self.listHolder.find('.activeAvailSelect').removeClass('activeAvailSelect');
			self.input.val('');
		};
		this.clearInput=function(){
			self.input.val('');
		};
		this.refresh=function(){
			if(main.tables[self.layerName].fields[self.field.name]){
				var field=main.tables[self.layerName].fields[self.field.name];
				$.ajax({
					type:'POST',
					url:main.mainPath+'server/db.php',
					data:{
						params:{
							folder:window.location.pathname.split('/')[window.location.pathname.split('/').length-2],
							field:{
								stored_inventory:field.stored_inventory,
								name:field.name
							},
							layerName:self.layerName
						},
						action:'getAvailSelect'
					},
					success:function(response){
						if(response){
							response=JSON.parse(response);
							if(response.status=="OK"){
								if(response.results.length){
									if(!main.data.lookUpTables[response.properties.lookup_table].items){
										main.data.lookUpTables[response.properties.lookup_table].waiting.push([self.lookUpLoaded,{response:response}]);
									}else{
										self.lookUpLoaded({response:response});
									}
								}else{
									self.lookUpLoaded({response:response});
								}
							}else{
								alert('Error');
								console.log(response);
							}
						}
					}
				});
			}
		};
		this.updateValuesForm=function(){
			if(self.value){
				var values=self.value.split(','),valSplit;
				for(var i=0;i<values.length;i++){
					valSplit=values[i].split(':');
					self.listHolder.find('.availSelTR_'+valSplit[0]).addClass('activeAvailSelect').find('.availSelQuantInput').val(valSplit[1]);
				}
			}
		};
		(this.createHTML=function(){
			self.input=$('<input type="hidden" name="'+self.field.name+'" class="availSelInput formInput '+self.field.name+'">');
			self.availSelInputWrap=$('<div class="availSelInputWrap">').append(self.input);
			self.availSelWrap=$('<div class="availSelWrap">')
			if(!self.field.avail_select_format){
				self.listHolder=$('<tbody class="availSelTBody">');
				self.primaryTH=$('<th>Species</th>');
				self.tableHead=$('<thead>')
					.append('<th>Request</th><th>Remains</th>')
					.append(self.primaryTH)
				self.table=$('<table class="availSelTable" border="1">')
					.append(self.tableHead)
					.append(self.listHolder);
				// self.availSelShowVal=$('<div class="availSelShowVal">');
				if(self.value){
					self.input.val(self.value);
					// self.availSelShowVal.html(main.utilities.getHTMLFromVal(self.value,main.tables[self.layerName].fields[self.field.name],lookUpTableMap));
				}
				var content=self.table;
			}else if(self.field.avail_select_format=='size'){
				self.smallGroupList=$('<div class="availSelGroupList availSelSmallGroupList">')
				self.smallGroup=$('<div class="availSelGroup availSelSmallGroup">')
					.append($('<div class="availSelGroupLabel">').html('<b>Small</b> (under 30\')'))
					.append(self.smallGroupList);
				self.medGroupList=$('<div class="availSelGroupList availSelMedGroupList">')
				self.medGroup=$('<div class="availSelGroup availSelMedGroup">')
					.append($('<div class="availSelGroupLabel">').html('<b>Medium</b> (30 to 50\' - possibly taller in ideal conditions)'))
					.append(self.medGroupList);
				self.largeGroupList=$('<div class="availSelGroupList availSelLargeGroupList">')
				self.largeGroup=$('<div class="availSelGroup availSelLargeGroup">')
					.append($('<div class="availSelGroupLabel">').html('<b>Large</b> (over 50\')'))
					.append(self.largeGroupList);
				self.undesignatedGroupList=$('<div class="availSelGroupList availSelUndesGroupList">')
				self.undesignatedGroup=$('<div class="availSelGroup availSelUndesGroup">')
					.append($('<div class="availSelGroupLabel">').html('<b>Undesignated Height</b>'))
					.append(self.undesignatedGroupList);
				self.listHolder=$('<div class="availSelGroupsWrap">')
					.append(self.smallGroup)
					.append(self.medGroup)
					.append(self.largeGroup)
					.append(self.undesignatedGroup);
				var content=self.listHolder;
				self.availSelWrap.addClass('asSizeFormat');
			}
			self.noItems=$('<div class="availSelNoItems none">No Items Available</div>');
			self.availSelWrap
				.append(content)
				.append(self.availSelInputWrap)
				.append(self.noItems);
			self.refresh();
		})();
		this.toggleGroups=function(){
			self.listHolder.find('.availSelGroup').each(function(){
				if($(this).find('.availSelTR').length){$(this).removeClass('none');
				}else{$(this).addClass('none');}
			});
		};
		this.lookUpLoaded=function(params){
			var results=params.response.results.sort(function(a,b){
				aAlias=(a.value)?main.utilities.getHTMLFromVal(a.value,main.tables[self.layerName].fields[self.field.name],main.tables[self.layerName].properties.lookup_table_map,null,null,params.response.properties.lookup_table,params.response.properties.primary_field).toLowerCase():'';
				bAlias=(b.value)?main.utilities.getHTMLFromVal(b.value,main.tables[self.layerName].fields[self.field.name],main.tables[self.layerName].properties.lookup_table_map,null,null,params.response.properties.lookup_table,params.response.properties.primary_field).toLowerCase():'';
				if(aAlias>bAlias){return 1;}
				if(aAlias<bAlias){return -1;}
				return 0
			});
			if(results.length){self.noItems.addClass('none');
			}else{self.noItems.removeClass('none');}
			if(!self.field.avail_select_format){
				self.listHolder.html('');
				// self.properties=main.tables[self.layerName].fields[params.response.properties.primary_field];
				// self.primaryTH.append();
				for(var i=0;i<results.length;i++){
					self.listHolder.append($('<tr class="availSelTR availSelTR_'+results[i].value+'">').data('val',results[i].value)
						.append($('<td>').append($('<div class="tdLiner availSelQuantInputWrap">').html('<input type="number" class="availSelQuantInput" min="0"/>')))
						.append($('<td>').append($('<div class="tdLiner availSelCount" data-remains="'+results[i].count+'">').html(results[i].count)))
						.append($('<td>').append($('<div class="tdLiner availSelAlias">').html(main.utilities.getHTMLFromVal(results[i].value,main.tables[self.layerName].fields[self.field.name],main.tables[self.layerName].properties.lookup_table_map,null,null,params.response.properties.lookup_table,params.response.properties.primary_field))))
					);
				}
			}else if(self.field.avail_select_format=='size'){
				self.listHolder.find('.availSelGroupList').html('');
				var size_class,group;
				for(var i=0;i<results.length;i++){
					size_class=results[i].size_class;
					if(size_class=='s'){group=self.smallGroupList;
					}else if(size_class=='m'){group=self.medGroupList;
					}else if(size_class=='l'){group=self.largeGroupList;
					}else{group=self.undesignatedGroupList;}
						group.append($('<div class="availSelTR availSelTR_'+results[i].value+' cf">').data('val',results[i].value)
							.append($('<div class="asNonTableCell availSelQuantInputWrap">').html('<input type="number" class="availSelQuantInput" min="0"/>'))
							// .append($('<div class="asNonTableCell availSelCount" data-remains="'+results[i].count+'">').html(results[i].count))
							.append($('<div class="asNonTableCell availSelAlias">').html(main.utilities.getHTMLFromVal(results[i].value,main.tables[self.layerName].fields[self.field.name],main.tables[self.layerName].properties.lookup_table_map,null,null,params.response.properties.lookup_table,params.response.properties.primary_field)))
						);
				}
				self.toggleGroups();
			}
			self.updateValuesForm();
		};
		this.inputItemChanged=function(row){
			var val='';
			self.listHolder.find('.activeAvailSelect').each(function(){
				if($(this).find('.availSelQuantInput').val().trim() && $(this).find('.availSelQuantInput').val().trim()!=0){
					val+=$(this).data('val')+':'+$(this).find('.availSelQuantInput').val()+',';
				}
			});
			val=val.replace(/,([^,]*)$/,'$1');
			self.input.val(val).change();
		};
		(this.initEvents=function(){
			self.availSelWrap.on('change','.availSelQuantInput',function(){
				var val=$(this).val().trim();
				if(val && val!=0){
					$(this).closest('.availSelTR').addClass('activeAvailSelect');
					if(!self.field.avail_select_format){
						if(Number(val)>Number($(this).closest('.availSelTR').find('.availSelCount').data('remains'))){
							$(this).val($(this).closest('.availSelTR').find('.availSelCount').data('remains'));
							self.inputItemChanged($(this).closest('.availSelTR'));
						}
					}
				}else{$(this).closest('.availSelTR').removeClass('activeAvailSelect');}
			});
			// self.availSelWrap.on('click','.availSelTR',function(e){
				// if(!$(e.target).closest('.availSelQuantInput').length){
					// $(this).toggleClass('activeAvailSelect');
					// self.inputItemChanged($(this));
				// }
			// });
		})();
	};
});