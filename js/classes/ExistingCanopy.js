define(function(require){
	return function pg(args){
		var self=this;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		self.layersa={};
		(this.addallExistingCanopy=function(){
			for (var templayer in main.data.layers){
				var layer=main.utilities.cloneObj(main.data.layers[templayer]);
				var geojson=layer.geojson.row_to_json;
				var layerObj=JSON.parse(geojson);
				var id=main.utilities.generateUniqueID();
				// var source=new ol.source.GeoJSON({
					// extractStyles: false,
					// object: layerObj
				// });
				var source=new ol.source.Vector({
					features: (new ol.format.GeoJSON()).readFeatures(layerObj)
				});
				self.layers[id]=new Layer2({
					id:id,
					source:source,
					visible:false,
					markerSource:new ol.source.Vector(),
					map:main.map
				});
				self.layers[id].setProperties(layer.properties);
				self.layers[id].setIconStyle(main.utilities.getMarkerStyle());
				self.layers.templayer=self.layers[id];
			}
		})();
	};
});