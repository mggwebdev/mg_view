define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Checkbox=require('./Checkbox');
		var LoaderBox=require('./LoaderBox');
		var Panel=require('./Panel');
		var Radio=require('./Radio');
		var Range=require('./Range');
		var Tabs=require('./Tabs');
		var Wait=require('./Wait');
		this.defaultColorGrad='Green-Red';
		this.inSettings=true;
		this.setToApp=true;
		this.withOtherLayers=true;
		this.showThisLayerWithOthers=false;
		this.showTop=false;
		this.alias='Interactive Legend';
		this.polygonAlpha=0.3;
		this.canvasSize=298;
		this.chartItemLimit=13;
		this.hideBackgroundFeaturesOnWOMShow=false;
		this.defaultColor='rgba(0,255,0,1)';
		this.backgroundFeatureColor='rgba(255,255,255,0.34)';
		this.defaultcharacterSortType='abc';
		this.characterSortType=this.defaultcharacterSortType;
		this.relativeTo=main.layout.mapContainerWrap;
		this.withSearch=true;
		this.with_load_more=true;
		this.hideOnly=true;
		this.withCanopyMode=false;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.id=this.id || main.utilities.generateUniqueID();
		this.currentFilters={};
		this.filterWhere=null;
		this.filterStr=null;
		this.default_number_breaks=5;
		this.default_to_fixed=1;
		this.layers=main.data.filter_legend;
		this.primaryLayer=null;
		for(var key in this.layers){
			if(!main.inventories[key]){
				delete this.layers[key];
				continue;
			}
			if(this.layers[key].primary_layer && (!main.tables[key].properties.dependent_upon || main.tables[key].overRideDependentOn)){
				this.primaryLayer=this.layers[key];
			}
		}
		if(!this.primaryLayer){
			for(var key in this.layers){
				if(!main.tables[key].properties.dependent_upon || main.tables[key].overRideDependentOn){
					this.primaryLayer=this.layers[key];
					break;
				}
			}
		}
		if(!this.primaryLayer){this.primaryLayer=main.invents.default_inventory;}
		this.layerName=this.primaryLayer.layer_name;
		this.inventory=main.inventories[this.layerName];
		this.layer=this.inventory.map_layer;
		// if(!this.layers || this.layers.length===0){this.layers=[this.layerName]}
		this.currentFilterBys=this.primaryLayer.filter_bys;
		this.default_filter_by=this.primaryLayer.default_filter_by;
		this.current_filter_by=this.default_filter_by;
		this.fields=this.layer.fields;
		this.normal_groups={};
		this.defaultRadius=5;
		this.defaultFillColor='rgba(255, 255, 255, 0.2)';
		this.defaultStrokeColor='rgba(0,0,0,1)';
		this.defaultStrokeWidth=1;
		this.defaultZIndex=100;
		this.active=false;
		this.initialResortInited=false;
		this.minimized=false;
		this.chartsSettings={
			doughnut:{
				name:'doughnut',
				alias:'Pie',
				chartType:'doughnut',
				animation:true,
			},
			bar:{
				name:'bar',
				alias:'Bar',
				chartType:'bar',
				animation:true,
			}
		};
		this.getStyle=function(color,strokeColor,strokeWidth,zIndex,text){
			if(!color){color=self.defaultFillColor;}
			if(!strokeColor){strokeColor=self.defaultStrokeColor;}
			if(!strokeWidth){strokeWidth=self.defaultStrokeWidth;}
			if(zIndex===null){
				if(self.layer.properties.z_index!==null){zIndex=Number(self.layer.properties.z_index);
				}else{zIndex=self.defaultZIndex;}
			}
			if(!text){
				return new ol.style.Style({
					fill: new ol.style.Fill({
						color: color
					}),
					stroke: new ol.style.Stroke({
						color: strokeColor,
						width:strokeWidth
					}),
					zIndex: zIndex,
					image: new ol.style.Circle({
						radius: self.layer.layer.style.default_circle_radius,
						fill: new ol.style.Fill({
							color: color
						}),
						stroke: new ol.style.Stroke({
							color: strokeColor,
							width:strokeWidth
						})
					})
				});
			}else{
				return new ol.style.Style({
					fill: new ol.style.Fill({
						color: color
					}),
					stroke: new ol.style.Stroke({
						color: strokeColor,
						width:strokeWidth
					}),
					zIndex: zIndex,
					text: text,
					image: new ol.style.Circle({
						radius: self.layer.layer.style.default_circle_radius,
						fill: new ol.style.Fill({
							color: color
						}),
						stroke: new ol.style.Stroke({
							color: strokeColor,
							width:strokeWidth
						})
					})
				});
			}
		}
		this.normalStyle=function(feature,text,dom){
			if(main.tables[main.filter_legend.layerName].map_layer.layer.labels.withLabels && typeof text!='object' && text!==null){return main.tables[main.filter_legend.layerName].map_layer.layer.style.labelStyleFunction(feature,text,dom)}
			if(!self.normal_groups[self.current_filter_by]){return false;}
			var val=feature.getProperties()[self.current_filter_by],color,hidden,groupMatch;
			if(main.misc.numberDTs.indexOf(self.cur_filt_field.data_type)>-1 && !self.cur_filt_field.options && self.cur_filt_field.sub_data_type!='season'){
				if(val || val=='0'){
					var groups=self.normal_groups[self.current_filter_by].groups,color='rgba(0,0,0,0)',groupsLength=Object.keys(groups);
					if(self.currentFilterBys[self.current_filter_by].type=='by_year'){
						val=new Date(val).getFullYear();
						if(groups[val].hide){
							hidden=true;
						}else if(groups[val]){
							color=groups[val].color;
						}
					}else{
						for(var key in groups){
							if((val>=groups[key].group.min && val<groups[key].group.max) || (Number(self.normal_groups[self.current_filter_by].props.max)==val && Number(groups[key].group.max)==Number(self.normal_groups[self.current_filter_by].props.max))){
								color=groups[key].color;
								groupMatch=key;
								break;
							}
						}
						if(self.normal_groups[self.current_filter_by].groups[groupMatch].hide){
							hidden=true;
						}
					}
				}else{
					if(self.normal_groups[self.current_filter_by].nothing_group && !self.normal_groups[self.current_filter_by].nothing_group.hide){
						color=self.normal_groups[self.current_filter_by].nothing_group.color;
					}else{hidden=true;}
				}
			}else if(self.cur_filt_field.data_type=='character varying' || self.cur_filt_field.data_type=='text' || self.cur_filt_field.data_type=='boolean' /* || self.cur_filt_field.data_type=='character varying'  */|| (main.misc.numberDTs.indexOf(self.cur_filt_field.data_type)>-1 && self.cur_filt_field.options) || self.cur_filt_field.sub_data_type=='season'){
				if(val || val=='0'){
					var isNothing=false;
					if(self.cur_filt_field.input_type=='checkbox'){
						if(typeof val=='string'){vals=val.split(',');}else{
							vals=[val.toString()];
						}
						cmatch=false;
						for(var e=0;e<vals.length;e++){
							if(self.normal_groups[self.current_filter_by].groups[vals[e]]){
								cmatch=true;
								if(!self.normal_groups[self.current_filter_by].groups[vals[e]].hide){
									if(self.normal_groups[self.current_filter_by].groups[vals[e]]){
										color=self.normal_groups[self.current_filter_by].groups[vals[e]].color;
									}else{
										// hidden=true;
										// color='rgba(0,0,0,0)';
										isNothing=true;
									}
								}else{
									hidden=true;
								}
							}
						}
						if(!cmatch){hidden=true;}
					}else{
						if(self.normal_groups[self.current_filter_by].isGrouped){
							if(self.normal_groups[self.current_filter_by].valDetails[val]){
								var group=self.normal_groups[self.current_filter_by].valDetails[val].group;
								if(group!='_empty'){
									if(!self.normal_groups[self.current_filter_by].groups[group].hide){
										color=self.normal_groups[self.current_filter_by].groups[group].color;
									}else{hidden=true;}
								}else{isNothing=true;}
							}else{isNothing=true;}
						}else{
							if(self.normal_groups[self.current_filter_by].groups[val]){
								if(!self.normal_groups[self.current_filter_by].groups[val].hide){
									if(self.currentFilterBys[self.current_filter_by].colors && self.currentFilterBys[self.current_filter_by].colors[val]){
										color=self.currentFilterBys[self.current_filter_by].colors[val];
									}else{
										color=self.normal_groups[self.current_filter_by].groups[val].color;
									}
								}else{hidden=true;}
							}else{isNothing=true;}
						}
					}
				}else{
					isNothing=true;
				}
				if(isNothing){
					if(self.normal_groups[self.current_filter_by].nothing_group && !self.normal_groups[self.current_filter_by].nothing_group.hide){
						color=self.normal_groups[self.current_filter_by].nothing_group.color;
					}else{hidden=true;}
				}
			}
			var zIndex=null;
			if(main.invents && main.invents.currentHighlightFilter && main.invents.currentHighlightFilterLayer==self.layerName && main.invents.currentHighlightFilter.length){
				if(main.invents.currentHighlightFilter.indexOf(Number(feature.get('pid')))===-1){
					if(self.hideBackgroundFeaturesOnWOMShow){
						hidden=true;
					}else{
						color=self.backgroundFeatureColor;
						var strokeColor='rgba(0,0,0,1)';
						var strokeWidth=1;
					}
				}else{
					zIndex=main.showGroupStyle.zIndex;
				}
			}
			if(!hidden){
				if(feature.get('isActive')){
					var strokeColor=main.activeStyle.strokeColor;
					var strokeWidth=main.activeStyle.strokeWidth;
					zIndex=main.activeStyle.zIndex;
				}else{
					if(feature.getGeometry() instanceof ol.geom.MultiLineString){
						var strokeColor=color;
						if(!self.layer.properties.stroke_width){var strokeWidth=1;
						}else{var strokeWidth=self.layer.properties.stroke_width;}
					}else if(feature.getGeometry() instanceof ol.geom.MultiPolygon || feature.getGeometry() instanceof ol.geom.Polygon){
						var strokeColor='rgba(0,0,0,1)';
						var strokeWidth=1;
						if(color){color=color.substring(0,color.split(',',3).join(',').length+1)+self.polygonAlpha+color.substring(color.indexOf(")"));}
					}else{
						var strokeColor='rgba(0,0,0,1)';
						var strokeWidth=1;
					}
				}
			}else{
				color='rgba(0,0,0,0)';
				var strokeColor='rgba(0,0,0,0)';
				var strokeWidth=0;
			}
			if(!color){color=self.defaultColor;}
			if(typeof text!='object' || text==null || color=='rgba(0,0,0,0)'){text=null;}
			var style=self.getStyle(color,strokeColor,strokeWidth,zIndex,text);
			return [style];
		};
		this.panelShow=function(){
			self.minimized=false;
			self.updateButtonsShow();
			self.setHeightParams();
			self.active=true;
			if(self.disabled){self.enable();}
		};
		this.setHeightParams=function(){
			if(self.relativeTo){
				self.panel.html.height('');
				self.filterGroupsHTMLWrapInner.css('max-height','').css('min-height','');
				var t=self.relativeTo.height();
				var height=self.relativeTo.height();
				var params=self.panel.getParams();
				var yMargin=params.panelMarginY;
				var topLegend=self.filterLOptionsWrap.outerHeight(true)+self.flLayerSearchWrap.outerHeight(true)+self.panel.top.outerHeight(true)+parseInt(self.panel.panelContent.css('margin-top'))+parseInt(self.panel.panelContent.css('padding-top'));
				var actualPanelHeight=self.panel.html.outerHeight(true);
				var panelOuterHeight=height-2*yMargin;
				var filterGroupsCharts=0;
				if(self.filterGroupsCharts && self.filterGroupsCharts.is(':visible')){
					filterGroupsCharts=self.filterGroupsCharts.outerHeight(true);
					var minFilterGroupsHTML=200;
				}else{
					var minFilterGroupsHTML=614;
				}
				if(actualPanelHeight>panelOuterHeight){
					self.filterGroupsHTMLWrapInner.css('max-height',minFilterGroupsHTML+'px');
					actualPanelHeight=self.panel.html.outerHeight(true);
					if(actualPanelHeight>panelOuterHeight){
						var panelHeight=height-2*yMargin-2*parseInt(self.panel.html.css('border-top-width'))-2*parseInt(self.panel.html.css('padding-top'));
						self.panel.html.height(panelHeight);
					}
				}
			}
		};
		this.panelClose=function(){
			self.active=false;
			self.updateButtonsHide();
		};
		this.panelMin=function(){
			self.minimized=true;
			self.updateButtonsHide();
		};
		// this.numRangeChange=function(args){
			// self.normal_groups[self.current_filter_by].props.inputMax=args.max;
			// self.normal_groups[self.current_filter_by].props.inputMin=arg.min;
		// };
		this.refreshGetGroupsCB=function(args){
			self.updateFilterBySelect();
			self.setHTMLGroups(args.dontUpdateCharts,true);
			self.currentStyle=self.normalStyle;
			if(!self.layers[self.layerName].dont_render_on_map && !self.currentFilterBys[self.current_filter_by].dont_render_on_map && self.layer.layer && main.invents.currentInvent==self.layerName){
				self.layer.layer.olLayer.setStyle(self.currentStyle);
			}
		};
		this.refresh=function(dontUpdateCharts){
			self.getGroups(self.refreshGetGroupsCB,dontUpdateCharts);
		};
		this.refreshPoints=function(){
			if(!self.layers[self.layerName].dont_render_on_map && !self.currentFilterBys[self.current_filter_by].dont_render_on_map && main.invents.currentInvent==self.layerName){
				self.layer.layer.olLayer.setStyle(self.currentStyle);
			}
		};
		this.chartDataReset=function(){
			var doughnutData=[],barData={labels:[],datasets:[]},barData1={data:[]},barLabel,barDataOrg=[],o=0,chartItemLimit=self.chartItemLimit;
			if(self.normal_groups[self.current_filter_by].nothing_group && !self.normal_groups[self.current_filter_by].nothing_group.hide && self.showNotSpecified.checkbox.prop('checked')){chartItemLimit--;}
			for(var u=0;u<self.normal_groups[self.current_filter_by].countSortedKeys.length;u++){
				var key=self.normal_groups[self.current_filter_by].countSortedKeys[u];
				if(!self.currentFilters[self.current_filter_by] || self.currentFilters[self.current_filter_by].value.indexOf(key)>=-1){
					if(self.normal_groups[self.current_filter_by].groups[key].short_alias){barLabel=self.normal_groups[self.current_filter_by].groups[key].short_alias;}
					barLabel=(self.normal_groups[self.current_filter_by].groups[key].alias || self.normal_groups[self.current_filter_by].groups[key].value);
					if(o<chartItemLimit && !self.normal_groups[self.current_filter_by].groups[key].hide){
						if(self.currentFilterBys[self.current_filter_by].colors && self.currentFilterBys[self.current_filter_by].colors[key]){
							var color=self.currentFilterBys[self.current_filter_by].colors[key];
						}else{var color=self.normal_groups[self.current_filter_by].groups[key].color;}
						barDataOrg.push({
							label:barLabel/* .substr(0,20) */,
							count:self.normal_groups[self.current_filter_by].groups[key].count,
							color:color
						});
						o++;
					}
				}
			}
			if(self.normal_groups[self.current_filter_by].nothing_group && !self.normal_groups[self.current_filter_by].nothing_group.hide && self.showNotSpecified.checkbox.prop('checked')){
				barDataOrg.push({
					label:self.normal_groups[self.current_filter_by].nothing_group.html,
					count:self.normal_groups[self.current_filter_by].nothing_group.count,
					color:'rgba(255,255,255,1)'
				});
			}
			barData1.label="dataset1";
			barData1.fillColor="rgba(151,187,205,0.5)";
			barData1.strokeColor="rgba(151,187,205,0.5)";
			barData1.highlightFill="rgba(151,187,205,0.5)";
			barData1.highlightStroke="rgba(151,187,205,0.5)";
			
			// var i=0;
			// barDataOrg=barDataOrg.sort(function(a,b){
				// if(a.count>b.count){return -1;}
				// if(a.count<b.count){return 1;}
				// return 0;
				// i++;
			// });
			for(var i=0;i<barDataOrg.length;i++){
				barData.labels.push(barDataOrg[i].label);
				barData1.data.push(barDataOrg[i].count);
				doughnutData.push({
					value:barDataOrg[i].count,
					color:barDataOrg[i].color,
					highlight:'rgb(202, 202, 255)',
					label:barDataOrg[i].label
				});
			}
			barData.datasets.push(barData1);
			//charts
			for(var key in self.chartsSettings){
				if(self.chartsSettings[key].chartType=='doughnut'){
					self.chartsSettings[key].data=doughnutData;
					var ct=0;
					for(var u=0;u<doughnutData.length;u++){
						ct+=doughnutData[u].value;
					}
					self.chartsSettings[key].localCount=ct;
				}else if(self.chartsSettings[key].chartType=='bar'){
					self.chartsSettings[key].data=barData;
					self.chartsSettings[key].barDataOrg=barDataOrg;
					var ct=0;
					for(var u=0;u<barData.datasets[0].data.length;u++){
						ct+=barData.datasets[0].data[u];
					}
					self.chartsSettings[key].localCount=ct;
				}
			}
		};
		this.fullChartReset=function(dontAnimate){
			self.chartDataReset();
			self.setNewChart(self.chartTabs.getActiveTab().name,dontAnimate);
		};
		this.setDependedUponLoadableResponse=function(response){
			var results=response.results;
			for(var key in results){
				// if(Number(results[key])){
					self.setDependedUponLoadable(null,key,Number(results[key]));
				// }
			}
		};
		this.setLoadables=function(group,withDependants){
			group.filterGroupUnit.on('click',function(){
				if(!$(this).hasClass('fLNotLoadable')){
					var pid=$(this).data('pid');
					main.invents.inventories[self.layerName].blockMoveEndLoadOnce=true;
					main.invents.inventories[self.layerName].loadFeatures({pid:pid,zoomTo:true});
				}else{
					alert('No Data Added to this '+main.invents.inventories[self.layer.properties.name].singular_alias);
				}
			});
			if(!withDependants){
				group.filterGroupUnit.addClass('fLNotLoadable');
			}else{
				group.flGroupLabel.attr('title','Click to Load Features');
			}
			group.filterGroupUnit.addClass('isFLClickable');
		};
		this.setDependedUponLoadable=function(key,pid,withDependants){
			if(key){
				self.setLoadables(self.normal_groups[self.current_filter_by].groups[key],withDependants);
			}else if(pid){
				var groups=self.normal_groups[self.current_filter_by].groups;
				for(var key2 in groups){
					if(groups[key2].pid==pid){
						self.setLoadables(self.normal_groups[self.current_filter_by].groups[key2],withDependants);
					}
				}
			}
		};
		this.refreshNoWheres=function(){
			var vals,group,field,val,showNulls,nullZeroOr,tempWhere,where='';
			for(var key in self.currentFilters){
				tempWhere=''
				vals=self.currentFilters[key].value;
				field=self.fields[key];
				showNulls=!self.currentFilters[key].hideNulls;
				if(self.currentFilters[key].type=='check'){
					if(field.input_type=='checkbox'){
						if(main.misc.textDTs.indexOf(field.data_type)>-1){
							if(vals.length){
								tempWhere+="(";
								for(var i=0;i<vals.length;i++){
									group=self.normal_groups[key].groups[vals[i]].group;
									tempWhere+="'"+group.val+"'!=ANY("+key+") AND ";
									// tempWhere+="CONCAT(';;',"+key+",';;') NOT ILIKE '%;;"+group.val+";;%' AND ";
								}
								tempWhere=tempWhere.replace(/ AND ([^ AND ]*)$/,'$1')+") AND ";
							}
						}else if(field.data_type=='boolean'){
							for(var i=0;i<vals.length;i++){
								group=self.normal_groups[key].groups[vals[i]];
								if(group.value==true || group.value=='true' || group.value=='t'){
									tempWhere+="("+key+"=FALSE OR "+key+"=NULL) AND ";
								}else{
									tempWhere+=key+"=TRUE AND ";
								}
							}
						}
					}else{
						if(vals.length){
							tempWhere+="(";
							for(var i=0;i<vals.length;i++){
								group=self.normal_groups[key].groups[vals[i]];
								val='';
								if(group.value){
									val=group.value;
								}else if(group.values){
									val=group.values[0];
								}
								tempWhere+="("+key+"!='"+val+"' OR "+key+" IS NULL) AND ";
							}
							tempWhere=tempWhere.replace(/ AND ([^ AND ]*)$/,'$1')+") AND ";
						}
					}
				}else if(self.currentFilters[key].type=="text"){
					for(var i=0;i<vals.length;i++){
						group=self.normal_groups[key].groups[vals[i]].group;
						tempWhere+="("+key+"!='"+group.val+"' OR "+key+" IS NULL) AND ";
					}
				}/* else if(self.currentFilters[key].type=="withFile"){
					if(cF.value){
						tempWhere+=cF.fieldName+" IS NOT NULL AND "+cF.fieldName+"!='' AND "+cF.fieldName+"!='[]' AND ";
					}else{
						tempWhere+="("+field.alias+" IS NULL OR "+cF.fieldName+"='' OR "+cF.fieldName+"='[]') AND ";
					}
				} */else if(self.currentFilters[key].type=='range'){
					for(var i=0;i<vals.length;i++){
						group=self.normal_groups[key].groups[vals[i]].group;
						// minAlias=group.min;
						// maxAlias=group.max;
						// if(field.to_fixed){minAlias=minAlias.toFixed(field.to_fixed);}
						// if(field.to_fixed){maxAlias=maxAlias.toFixed(field.to_fixed);}
						tempWhere+="("+key+"<="+Number(group.min)+" OR "+key+">="+Number(group.max)+") AND ";
					}
				}
				if(showNulls){
					if(tempWhere){
						tempWhere=tempWhere.replace(/ AND ([^ AND ]*)$/,'$1')+" OR ";
					}
					tempWhere+=key+" IS NULL AND ";
				}else{
					nullZeroOr='';
					nullBlankOr='';
					if(((field.options && !field.options['0']) || field.lookup_table) && main.misc.numberDTs.indexOf(field.data_type)!=-1 ){nullZeroOr=" AND "+key+"!='0'";}
					if(main.misc.textDTs.indexOf(field.data_type)!=-1){nullBlankOr=" AND "+key+"!=''";}
					tempWhere+=key+" IS NOT NULL"+nullZeroOr+nullBlankOr+" AND ";
				}
				tempWhere=tempWhere.replace(/ AND ([^ AND ]*)$/,'$1');
				where+="("+tempWhere+") AND ";
			}
			where=where.replace(/ AND ([^ AND ]*)$/,'$1');
			return {
				where:where
			}
		};
		this.updateCurrentFilters=function(field){
			var vals=[];
			self.filterGroupsHTML.find('.flGroupTogCheck:not(:checked):not(.flNothingGroup)').each(function(){
				vals.push($(this).data('group'));
			});
			if(self.filterGroupsHTML.find('.flGroupTogCheck.flNothingGroup').length){
				if(self.filterGroupsHTML.find('.flGroupTogCheck.flNothingGroup').prop('checked')){var hideNulls=false;
				}else{var hideNulls=true;}
			}else{var hideNulls=false;}
			if(!vals.length && !hideNulls){
				delete self.currentFilters[field.name];
				self.filterWhere=null;
			}else{
				if(field.input_type=='text' || (field.input_type=='hidden' && main.misc.textDTs.indexOf(field.data_type)!=-1) || field.input_type=='textarea'){
					self.currentFilters[field.name]={
						type:'text',
						fieldName:field.name,
						value:vals,
						hideNulls:hideNulls
					}
				}else if(field.input_type=='number' || (field.input_type=='hidden' && main.misc.numberDTs.indexOf(field.data_type)!=-1)){
				if(main.misc.numberDTs.indexOf(field.data_type)>-1 && !field.options){
					self.currentFilters[field.name]={
						type:'range',
						fieldName:field.name,
						value:vals,
						hideNulls:hideNulls
					}
				}
				}/* else if(field.input_type=='file'){
					self.currentFilters[field.name]={
						type:'withFile',
						fieldName:field.name,
						value:'t',
						hideNulls:hideNulls
					}
				} */else if((field.input_type=='select' || field.input_type=='checkbox' || field.input_type=='radio') && !field.lookup_table){
					self.currentFilters[field.name]={
						type:'check',
						fieldName:field.name,
						value:vals,
						hideNulls:hideNulls
					}
				}else if(field.lookup_table){
					self.currentFilters[field.name]={
						type:'check',
						fieldName:field.name,
						value:vals,
						hideNulls:hideNulls
					}
				}else if(field.input_type=='number' || (field.input_type=='hidden' && main.misc.numberDTs.indexOf(field.data_type)!=-1)){
					self.currentFilters[field.name]={
						type:'range',
						fieldName:field.name,
						value:vals,
						hideNulls:hideNulls
					}
				}else if(field.input_type=='date'){
					self.currentFilters[field.name]={
						type:'range',
						fieldName:field.name,
						value:vals,
						hideNulls:hideNulls
					}
				}
				var wheres=self.refreshNoWheres();
				self.filterWhere=wheres.where;
			}
			main.loader.loadMoreMapPoints(self.layerName);
		};
		this.addHTMLRow=function(key,inTop){
			html=self.normal_groups[self.current_filter_by].groups[key].alias;
			if(!html || (self.filterStr && html.toLowerCase().indexOf(self.filterStr)==-1)){return false;}
			
			if(self.currentFilterBys[self.current_filter_by].colors && self.currentFilterBys[self.current_filter_by].colors[key]){
				var color0=self.currentFilterBys[self.current_filter_by].colors[key]
			}else{var color0=self.normal_groups[self.current_filter_by].groups[key].color;}
			self.normal_groups[self.current_filter_by].groups[key].svg='<svg height="16" width="16"><circle cx="8" cy="8" r="7" stroke="rgb(0,0,0)" stroke-width="1" fill="'+color0+'"/></svg>';
			var checked=true;
			if(self.currentFilters[self.current_filter_by] && self.currentFilters[self.current_filter_by].value.indexOf(key)!=-1){checked=false;}
			self.normal_groups[self.current_filter_by].groups[key].checkToggle=new Checkbox({
				value:'true',
				name:'flGroupTogCheck',
				checked:checked,
				data:[['group',key],['filter_by',self.current_filter_by]],
				classes:'flGroupTogCheck flGroupTogCheck_'+key
			});
			self.normal_groups[self.current_filter_by].groups[key].checkToggle.checkbox.on('change',function(){
				var group=$(this).data('group');
				var filter_by=$(this).data('filter_by');
				if(!$(this).prop('checked')){var hide=true;
				}else{var hide=false;}
				if(self.hideOnly){
					self.normal_groups[filter_by].groups[group].hide=hide;
					self.currentStyle=self.normalStyle;
					if(!self.layers[self.layerName].dont_render_on_map && !self.currentFilterBys[self.current_filter_by].dont_render_on_map && main.invents.currentInvent==self.layerName){
						self.layer.layer.olLayer.setStyle(self.currentStyle);
					}
					if(!self.layers[self.layerName].dont_show_graphs && self.layers[self.layerName].with_charts && !self.withCanopyMode){
						self.fullChartReset(true);
						if(self.inTopList && $.contains(document,self.inTopList[0])){
							self.filterGroupsHTMLWrapInner.find('.filterGroupUnit[data-group="'+group+'"] .flGroupTogCheck ').prop('checked',$(this).prop('checked'));
						}
					}
				}else{
					self.updateCurrentFilters(self.fields[filter_by]);
					if(!self.layers[self.layerName].dont_show_graphs && self.layers[self.layerName].with_charts){
						self.fullChartReset(true);
					}
				}
				self.updateShowingDisplay(null,true);
			});
				// val=self.normal_groups[self.current_filter_by].groups[key].value;
				// if(self.normal_groups[self.current_filter_by].groups[key].alias){
					// html=self.normal_groups[self.current_filter_by].groups[key].alias;
				// }else{
					// html=val;
					// if(field.options && field.options[val] && !field.lookup_table){html=field.options[val].alias;}
				// if(field.lookup_table && main.data.lookUpTables[field.lookup_table]){
					// var lookUpVal=self.getLookUpTableHTML(field.lookup_table,val,self.current_filter_by,self.layerName);
					// if(lookUpVal!==null){html=lookUpVal;}
				// }
			// }
			self.normal_groups[self.current_filter_by].groups[key].flGroupLabel=$('<div class="flGroupLabel flGUEle">');
			self.normal_groups[self.current_filter_by].groups[key].flGroupLabelCount=$('<span class="flGroupLabelCount">');
			var groupLabel=$('<div class="flGroupLabelIn">')
				.append($('<span class="flGroupLabelHTML">').html(html));
				
			if(!self.currentFilterBys[self.current_filter_by].no_count){
				groupLabel.append(self.normal_groups[self.current_filter_by].groups[key].flGroupLabelCount);
			}
			if(!self.layers[self.layerName].dont_count){
				self.normal_groups[self.current_filter_by].groups[key].flGroupLabelCount.html(' ('+self.normal_groups[self.current_filter_by].groups[key].count+')' );
			}
			self.normal_groups[self.current_filter_by].groups[key].HTMLGroupLabel=groupLabel;
			self.normal_groups[self.current_filter_by].groups[key].filterGroupUnit=$('<div class="filterGroupUnit cf" data-current_filter_by="'+self.current_filter_by+'" data-group="'+key+'">');
			if(self.normal_groups[self.current_filter_by].groups[key].pid){
				self.normal_groups[self.current_filter_by].groups[key].filterGroupUnit.data('pid',self.normal_groups[self.current_filter_by].groups[key].pid)}
			if(main.inventories[self.layerName].is_depended_upon_by && (self.layers[self.layerName].dont_render_on_map || self.currentFilterBys[self.current_filter_by].dont_render_on_map)){
				var go=true;
				if(main.inventories[self.layerName].is_depended_upon_by){
					if(!main.invents.inventories[main.inventories[self.layerName].is_depended_upon_by].properties[main.account.loggedInUser.user_type+'_editable']){
						if(self.normal_groups[self.current_filter_by].groups[key].pid){
							self.checkForLoadables.push(self.normal_groups[self.current_filter_by].groups[key].pid);
							go=false;
						}
					}
				}
				if(go){self.setDependedUponLoadable(key,null,true);}
			}
			if(!inTop){
				self.filterGroupsHTML.append(self.normal_groups[self.current_filter_by].groups[key].filterGroupUnit);
			}else{
				self.inTopListList.append(self.normal_groups[self.current_filter_by].groups[key].filterGroupUnit);
			}
			if(!self.layers[self.layerName].dont_render_on_map && !self.currentFilterBys[self.current_filter_by].dont_render_on_map){
				self.normal_groups[self.current_filter_by].groups[key].filterGroupUnit
					.append($('<div class="flGroupToggle flGUEle">').append(self.normal_groups[self.current_filter_by].groups[key].checkToggle.html))
					.append($('<div class="flGroupSymbol flGUEle">').html(self.normal_groups[self.current_filter_by].groups[key].svg))
			}
			self.normal_groups[self.current_filter_by].groups[key].filterGroupUnit.append(self.normal_groups[self.current_filter_by].groups[key].flGroupLabel.html(self.normal_groups[self.current_filter_by].groups[key].HTMLGroupLabel))
			// if(u<self.chartItemLimit-1 && !self.normal_groups[self.current_filter_by].groups[key].hide){
				// barDataOrg.push({
					// label:barLabel.substr(0,20),
					// count:self.normal_groups[self.current_filter_by].groups[key].count,
					// color:self.normal_groups[self.current_filter_by].groups[key].color
				// });
			// }
			// barData.labels.push(barLabel);
			// barData1.data.push(self.normal_groups[self.current_filter_by].groups[key].count);
			// barColors.push(self.normal_groups[self.current_filter_by].groups[key].color);
			// doughnutData.push({
				// value:self.normal_groups[self.current_filter_by].groups[key].count,
				// color:self.normal_groups[self.current_filter_by].groups[key].color,
				// highlight:'rgb(202, 202, 255)',
				// label:html
			// });
		};
		this.flOptionsSearchWrapVisCheck=function(){
			if(/* main.inventories[self.layerName].is_depended_upon_by &&  */self.normal_groups[self.current_filter_by].htmlSortedKeys.length>8 && (main.misc.textDTs.indexOf(self.fields[self.current_filter_by].data_type)>-1 || self.fields[self.current_filter_by].lookup_table || self.fields[self.current_filter_by].options)){
				self.flOptionsSearchWrap.addClass('block');
			}else{
				self.flOptionsSearchWrap.removeClass('block');
			}
		};
		this.setHTMLGroups=function(dontUpdateCharts,dontAnimate){
			self.flToggleAll.checkbox.prop('checked',true);
			self.filterGroupsHTML.html('');
			self.filterGroupsHTML.data('current_filter_by',self.current_filter_by)
			if(self.inTopListList){self.inTopListList.html('');}
			var val,html;
			if(self.with_load_more){
				if(main.inventories[self.layerName].is_depended_upon_by){self.loadMoreWrap.removeClass('block');
				}else{
					// self.clearShowingDisplay();
					self.loadMoreWrap.addClass('block');
				}
			}
			// var inTop=[];
			if(self.currentFilterBys[self.current_filter_by].showTop && self.showTop){
				if(!self.inTopListList){
					self.inTopListList=$('<div class="inTopListList">');
					self.inTopListLabel=$('<div class="filterGroupLabel">');
					self.filterGroupsHTMLLabel=$('<div class="filterGroupLabel">').text(self.currentFilterBys[self.current_filter_by].alias);
					self.inTopList=$('<div class="inTopList flList">');
					self.inTopList
						.append(self.inTopListLabel)
						.append(self.inTopListList);
				}
				self.filterGroupsHTMLWrapO.prepend(self.filterGroupsHTMLLabel);
				self.filterGroupsHTMLWrapO.before(self.inTopList);
				var showTop=Number(self.currentFilterBys[self.current_filter_by].showTop);
				var inList=0;
				for(var u=0;u<self.normal_groups[self.current_filter_by].countSortedKeys.length;u++){
					if(inList<showTop){
						var key=self.normal_groups[self.current_filter_by].countSortedKeys[u];
						if(!key){break;}
						if(self.addHTMLRow(key,true)===false){continue;}else{inList++;}
						// inTop.push(key);
					}else{break;}
				}
				self.inTopListLabel.text('Top '+(inList));
			}else{
				if(self.inTopList){
					self.inTopList.detach();
					self.filterGroupsHTMLLabel.detach();
				}
			}
			self.checkForLoadables=[];
			for(var u=0;u<self.normal_groups[self.current_filter_by].htmlSortedKeys.length;u++){
			// for(var key in self.normal_groups[self.current_filter_by].groups){
				var key=self.normal_groups[self.current_filter_by].htmlSortedKeys[u];
				// if(inTop.indexOf(key)==-1){
					if(self.addHTMLRow(key)===false){continue;}
				// }
			}
			if(self.checkForLoadables.length){
				main.utilities.getCountByRestrictor(main.inventories[self.layerName].is_depended_upon_by,self.layerName,main.invents.inventories[main.inventories[self.layerName].is_depended_upon_by].properties.internal_field_map.dependent_upon_field,self.checkForLoadables,self.setDependedUponLoadableResponse);
			}
			if(self.normal_groups[self.current_filter_by].nothing_group && !self.layers[self.layerName].dont_render_on_map && !self.currentFilterBys[self.current_filter_by].dont_render_on_map && !self.layers[self.layerName].dont_show_graphs){
				self.normal_groups[self.current_filter_by].nothing_group.svg='<svg height="16" width="16"><circle cx="8" cy="8" r="7" stroke="rgb(0,0,0)" stroke-width="1" fill="'+self.normal_groups[self.current_filter_by].nothing_group.color+'"/></svg>';
				self.normal_groups[self.current_filter_by].nothing_group.html='Not Specified';
				var checked=true;
				if(self.currentFilters[self.current_filter_by] && self.currentFilters[self.current_filter_by].hideNulls){checked=false;}
				self.normal_groups[self.current_filter_by].nothing_group.checkToggle=new Checkbox({
						value:'true',
						name:'flGroupTogCheck',
						checked:checked,
						data:[['group','flNothingGroup'],['filter_by',self.current_filter_by]],
						classes:'flGroupTogCheck flNothingGroup'
					});
				self.normal_groups[self.current_filter_by].nothing_group.checkToggle.checkbox.on('change',function(){
					var filter_by=$(this).data('filter_by');
					if(!$(this).prop('checked')){var hide=true;
					}else{var hide=false;}
					if(self.hideOnly){
						self.normal_groups[filter_by].nothing_group.hide=hide;
						self.currentStyle=self.normalStyle;
						if(!self.layers[self.layerName].dont_render_on_map && !self.currentFilterBys[self.current_filter_by].dont_render_on_map && main.invents.currentInvent==self.layerName){
							self.layer.layer.olLayer.setStyle(self.currentStyle);
						}
						if(!self.layers[self.layerName].dont_show_graphs && self.layers[self.layerName].with_charts && !self.withCanopyMode){
							self.fullChartReset(true);
						}
					}else{
						self.updateCurrentFilters(self.fields[filter_by]);
						if(!self.layers[self.layerName].dont_show_graphs && self.layers[self.layerName].with_charts){
							self.fullChartReset(true);
						}
					}
				});
				var nothingGroupLabel=self.normal_groups[self.current_filter_by].nothing_group.html;
				if(!self.layers[self.layerName].dont_count){nothingGroupLabel+=' ('+self.normal_groups[self.current_filter_by].nothing_group.count+')';}
				self.normal_groups[self.current_filter_by].nothing_group.filterGroupUnit=$('<div class="filterGroupUnit flNothingGroup cf">');
				self.filterGroupsHTML.append(self.normal_groups[self.current_filter_by].nothing_group.filterGroupUnit
					.append($('<div class="flGroupToggle flGUEle">').append(self.normal_groups[self.current_filter_by].nothing_group.checkToggle.html)))
				if(!self.layers[self.layerName].dont_render_on_map && !self.currentFilterBys[self.current_filter_by].dont_render_on_map){
					self.normal_groups[self.current_filter_by].nothing_group.filterGroupUnit.append($('<div class="flGroupSymbol flGUEle">').html(self.normal_groups[self.current_filter_by].nothing_group.svg))
				}
				self.normal_groups[self.current_filter_by].nothing_group.filterGroupUnit.append($('<div class="flGroupLabel flGUEle">').html(nothingGroupLabel))
				
				// if(!self.normal_groups[self.current_filter_by].nothing_group.hide){
					// barDataOrg.push({
						// label:self.normal_groups[self.current_filter_by].nothing_group.html,
						// count:self.normal_groups[self.current_filter_by].nothing_group.count,
						// color:'rgba(255,255,255,1)'
					// });
				// }
				// barData.labels.push(self.normal_groups[self.current_filter_by].nothing_group.html);
				// barData1.data.push(self.normal_groups[self.current_filter_by].nothing_group.count);
				// barColors.push('rgba(255,255,255,1)');
				// doughnutData.push({
					// value:self.normal_groups[self.current_filter_by].nothing_group.count,
					// color:'rgba(255,255,255,1)',
					// highlight:'rgb(202, 202, 255)',
					// label:self.normal_groups[self.current_filter_by].nothing_group.html
				// });
			}
			if(self.withSearch){self.flOptionsSearchWrapVisCheck();}
			if(self.currentFilterBys[self.current_filter_by].no_charts){
				self.hideCharts(true);
			}else{
				if(!dontUpdateCharts && !self.layers[self.layerName].dont_show_graphs){
					if(self.layers[self.layerName].with_charts){self.showCharts();}
					if(self.filterGroupsCharts.is(':visible')){
						if(self.layers[self.layerName].with_charts){
							self.fullChartReset(dontAnimate);
						}else{
							self.hideCharts();
						}
					}
				}
			}
			self.setHeightParams();
		};
		this.hideCharts=function(na){
			self.filterGroupsCharts.detach();
			// if(na){self.flInvNormWrap.append(self.naCharts);}
		};
		this.showCharts=function(na){
			// if(na){self.naCharts.detach();}
			if(!self.layers[self.layerName].dont_show_graphs && self.layers[self.layerName].with_charts){
				self.flInvNormWrap.append(self.filterGroupsCharts);
			}
		};
		this.someResults=function(){
			self.flNoResultsWrap.addClass('none');
			self.filterGroupsHTMLWrap.removeClass('none');
			self.loadMoreWrap.addClass('block');
			self.filterGroupsCharts.removeClass('none');
			self.flOptionsSearchWrapVisCheck();
			self.filterBySelectHideTest();
		};
		this.noResults=function(){
			self.flNoResultsWrap.html('No '+main.invents.inventories[self.layer.properties.name].plural_alias+'. Start Adding.').removeClass('none');
			self.filterGroupsHTML.html('');
			self.filterGroupsHTMLWrap.addClass('none');
			self.loadMoreWrap.removeClass('block');
			self.filterGroupsCharts.addClass('none');
			self.flOptionsSearchWrap.removeClass('block');
			self.filterBySelectHideTest();
		};
		this.getGroupsAjax=function(fieldName,field,features,layerName,colorGrad,characterSortType,numberBreaks,to_fixed,limit,cb,dontUpdateCharts,dontHideOtherLayers,changeToField){
			self.loader.loaderOn();
			var lookup_table_map=null;
			var props=main.invents.inventories[layerName].properties;
			if(props.lookup_table_map){lookup_table_map=props.lookup_table_map;}
			var getPids=null;
			if(self.layers[layerName].dont_render_on_map || self.currentFilterBys[self.current_filter_by].dont_render_on_map){getPids=true;}
			
			var where='';
			if(main.invents.currentFilterLayer && main.inventories[main.invents.currentFilterLayer].is_depended_upon_by==layerName && main.invents.currentFilterPID && props.internal_field_map && props.internal_field_map.dependent_upon_field && !main.account.loggedInUser.dont_filter){
				where=props.internal_field_map.dependent_upon_field+"='"+main.invents.currentFilterPID+"'";
			}
			var pids=null;
			if(main.advFilter && main.advFilter.appliesTo['map'].apply && main.advFilter.currentLayer && main.advFilter.currentLayer.properties.layer_name==layerName){
				if(main.advFilter.where){
					if(where){where+=' AND ';}
					where+='('+main.advFilter.where+')';
				}
				if(main.advFilter.geoFilter){pids=main.advFilter.geoFilter;}
			}
			if(pids){
				if(pids.length==0){
					self.noResults();
					return;
				}
				pids=pids.join(',');
			}
			var placeholders=null;
			if(self.currentFilterBys[fieldName].place_holders){
				placeholders=self.currentFilterBys[fieldName].place_holders;
			}
			var type=null;
			if(self.currentFilterBys[fieldName].type){
				type=self.currentFilterBys[fieldName].type;
			}
			self.filterGroupsHTML.html('');
			changeToField=changeToField || null;
			self.getGroupsByCountAjax=$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						fieldName:fieldName,
						field:field,
						where:where,
						pids:pids,
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						getPids:getPids,
						layerName:layerName,
						colorGrad:colorGrad,
						characterSortType:characterSortType,
						lookup_table_map:lookup_table_map,
						numberBreaks:numberBreaks,
						to_fixed:to_fixed,
						placeholders:placeholders,
						type:type,
						changeToField:changeToField,
						dontUpdateCharts:dontUpdateCharts,
						dontHideOtherLayers:dontHideOtherLayers,
						limit:limit
					},
					action:'getGroupsByCount'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							var groups=response.normalGroup;
							if(groups.totalCount && groups.totalCount>0){
								if(typeof groups!='string'){
									self.normal_groups[response.fieldName]=groups;
									self.someResults();
									// return self.normal_groups;
									if(cb){
										var cbPkg={
											layerName:response.layerName,
											dontUpdateCharts:response.dontUpdateCharts,
											dontHideOtherLayers:response.dontHideOtherLayers,
											changeToField:response.changeToField
										};
										cb(cbPkg);
									}
									if(self.layer.layer && !self.layer.layer.olLayer.getVisible()){self.layer.layer.olLayer.setVisible(true);}
								}else{
									main.data.lookUpTables[groups].waiting.push([self.refresh,{}]);
									// return false;
								}
								main.filter_legend.checkLoginChange();
							}else{
								self.noResults();
							}
							self.setHeightParams();
							self.setDontRenderOnMap(self.layerName);
						}else{
							alert('Error');
							console.log(response);
						}
						self.loader.loaderOff();
					}
				}
			});
		};
		this.endCurrentGetGroupsAjax=function(){
			if(self.getGroupsByCountAjax){
				self.getGroupsByCountAjax.abort();
				self.getGroupsByCountAjax=null;
				self.loader.loaderOff();
			}
		};
		this.getGroups=function(cb,dontUpdateCharts,dontHideOtherLayers,changeToField){
			self.endCurrentGetGroupsAjax();
			var field=self.fields[self.current_filter_by];
			self.cur_filt_field=field;
			var colorGrad=self.currentFilterBys[self.current_filter_by].colorGrad || self.defaultColorGrad;
			if(self.currentFilterBys[self.current_filter_by].invert_colors){colorGrad=main.utilities.getInvertedColorType(colorGrad);}
			var sortType=self.characterSortType;
			if((field.options && !isNaN(Object.keys(field.options)[0])) || field.sub_data_type=='season'){sortType='val';}
			var to_fixed=self.default_to_fixed;
			if(field.to_fixed || field.to_fixed===0){to_fixed=field.to_fixed;}
			// var groups=main.utilities.getGroupsByCount(self.current_filter_by,field,self.layer.features,self.layerName,colorGrad,sortType,self.default_number_breaks,to_fixed);
			self.getGroupsAjax(self.current_filter_by,field,self.layer.features,self.layerName,colorGrad,sortType,self.default_number_breaks,to_fixed,null,cb,dontUpdateCharts,dontHideOtherLayers,changeToField);
			
			// if(typeof groups!='string'){
				// self.normal_groups[self.current_filter_by]=groups;
				// return self.normal_groups;
			// }else{
				// main.data.lookUpTables[groups].waiting.push([self.refresh,{}]);
				// return false;
			// }
		};
		this.updateButtonsShow=function(){
			self.showButton.html.removeClass('inlineBlock');
			self.hideButton.html.removeClass('none');
		};
		this.updateButtonsHide=function(){
			self.showButton.html.addClass('inlineBlock');
			self.hideButton.html.addClass('none');
		}; 
		this.layerLoaded=function(layerName,updateFilterLegend){
			if(!updateFilterLegend){self.refresh();}
			if(!self.initialResortInited){
				main.utilities.setTopLayer(self.layer.layer.olLayer);
				self.initialResortInited=true;
			}
			if(layerName && self.flLayerSelect.find('option[value="'+layerName+'"]').length===0){
				self.flLayerSelect.append('<option value="'+self.layers[layerName].layer_name+'"'+selected+'>'+main.tables[self.layers[layerName].layer_name].properties.alias+'</option>');
			}
		};
		this.turnOn=function(){
			self.show();
			self.active=true;
			self.currentStyle=self.normalStyle;
			if(!self.layers[self.layerName].dont_render_on_map && !self.currentFilterBys[self.current_filter_by].dont_render_on_map && main.invents.currentInvent==self.layerName){
				self.layer.layer.olLayer.setStyle(self.currentStyle);
			}
		}
		this.enable=function(){
			if(main.invents.currentInvent==self.layerName){
				self.layer.layer.olLayer.setStyle(self.currentStyle);
				main.utilities.setTopLayer(self.layer.layer.olLayer);
				self.disabled=false;
			}
		}
		this.disable=function(){
			self.panel.minimizePanel();
			self.disabled=true;
			self.setDefaultStyling(self.layer);
		};
		this.turnOff=function(){
			self.hide();
			self.active=false;
			self.setDefaultStyling(self.layer);
		}
		this.show=function(){
			self.panel.open();
		};
		this.hide=function(){
			self.panel.close();
		};
		this.setNewChart=function(key,dontAnimate){
			self.chartsSettings[key].canvas=$('<canvas class="fLChart" width="'+self.canvasSize+'" height="'+self.canvasSize+'">');
			self.chartsSettings[key].ctx=self.chartsSettings[key].canvas.get(0).getContext('2d');
			self.chartsSettings[key].container.html(self.chartsSettings[key].canvas);
			var animation=true;
			if(dontAnimate || main.browser.isMobile){animation=false;}
			if(self.chartsSettings[key].chartType=='doughnut'){
				self.chartsSettings[key].currentChart=new Chart(self.chartsSettings[key].ctx).Doughnut(self.chartsSettings[key].data,{
					animation:animation,
					animateScale:true,
					segmentShowStroke :true,
					segmentStrokeColor  :'rgb(224, 224, 224)',
					tooltipFontSize: 9,
					tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= '   '+value+'   :   '+(value*100/"+self.normal_groups[self.current_filter_by].totalCount+").toFixed(1)+'%' %>",
					// customTooltips: false,
					// responsive: true
				});
			}else if(self.chartsSettings[key].chartType=='bar'){
				self.chartsSettings[key].currentChart=new Chart(self.chartsSettings[key].ctx).Bar(self.chartsSettings[key].data,{
					animation:animation,
					barShowStroke :true,
					tooltipFontSize: 9,
					tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= '   '+value+'   :   '+(value*100/"+self.chartsSettings[key].localCount+").toFixed(1)+'%' %>",
					// responsive: true
				});
				for(var t=0;t<self.chartsSettings[key].barDataOrg.length;t++){
					self.chartsSettings[key].currentChart.datasets[0].bars[t].fillColor=self.chartsSettings[key].barDataOrg[t].color;
					self.chartsSettings[key].currentChart.datasets[0].bars[t]._saved.fillColor=self.chartsSettings[key].barDataOrg[t].color;
				}
			}
		};
		this.removeChart=function(key){
			self.chartsSettings[key].canvas.remove();
			self.chartsSettings[key].ctx=null;
			self.chartsSettings[key].currentChart=null;
		};
		this.newTabOpened=function(tab,prevtab){
			if(!self.layers[self.layerName].dont_show_graphs && self.layers[self.layerName].with_charts){
				self.removeChart(prevtab.name);
				self.setNewChart(tab.name);
			}
		};
		this.toggleChecks=function(){
			var checked=$(this).prop('checked');
			self.filterGroupsHTML.find('.filterGroupUnit:not(.flNothingGroup) .flGroupTogCheck').prop('checked',checked);
			if(self.hideOnly){	
				var hide=!checked;
				self.currentStyle=self.normalStyle;
				self.filterGroupsHTML.find('.filterGroupUnit:not(.flNothingGroup) .flGroupTogCheck').each(function(){
					var group=$(this).data('group');
					var filter_by=$(this).data('filter_by');
					self.normal_groups[filter_by].groups[group].hide=hide;
				});
				if(self.filterGroupsHTML.find('.filterGroupUnit.flNothingGroup').length>0){
					self.normal_groups[self.filterGroupsHTML.find('.filterGroupUnit.flNothingGroup .flGroupTogCheck').prop('checked',checked).data('filter_by')].nothing_group.hide=hide;
				}
				if(!self.layers[self.layerName].dont_render_on_map && !self.currentFilterBys[self.current_filter_by].dont_render_on_map && main.invents.currentInvent==self.layerName){
					self.layer.layer.olLayer.setStyle(self.currentStyle);
				}
				if(!self.layers[self.layerName].dont_show_graphs && self.layers[self.layerName].with_charts){
					self.fullChartReset();
				}
			}else{
				self.updateCurrentFilters(self.fields[self.filterGroupsHTML.data('current_filter_by')]);
				if(!self.layers[self.layerName].dont_show_graphs && self.layers[self.layerName].with_charts){
					self.fullChartReset(true);
				}
			}
			self.updateShowingDisplay(null,true);
		};
		this.addNewOtherLayer=function(layer){
			if(self.otherLayers && (self.showThisLayerWithOthers || (!self.showThisLayerWithOthers && layer.id!=self.layerName))){
				self.otherLayers[layer.id]=layer;
				self.addOtherLayer(layer);
			}
		};
		this.turnOffAllLayers=function(){
			for(var key in self.otherLayerItems){
				self.otherLayerItems[key].layer.setVisible(false);
			}
		};
		this.toggleLayer=function(layerID,visible){
			if(self.otherLayerItems[layerID]){
				if(visible){
					if(main.map_layers[layerID].properties.max_res){
						if(Number(main.map_layers[layerID].properties.max_res)<=main.map.map.getView().getResolution()){visible=false;}
					}
				}
				if(visible/*  && !main.map_layers[layerID].hasLoaded */){
					main.loader.loadMoreMapPoints(layerID);
				}
				self.otherLayerItems[layerID].layer.setVisible(visible);
			}
			if(main.canopy && Object.keys(main.canopy.layers).indexOf(layerID)>-1){
				main.canopy.setCurrentLayer(main.canopy.layers[layerID]);
				main.canopy.setStats(main.canopy.layers[layerID],true);
				main.canopy.infoTip.updateCurrentLayer(main.canopy.layers[layerID]);
			}
		};
		this.sortLegendItems=function(type){
			if(type=='abc'){
				var keys=Object.keys(self.otherLayerItems).sort(function(a,b){
					if(self.otherLayerItems[a].layer.alias>self.otherLayerItems[b].layer.alias){return 1;}
					if(self.otherLayerItems[a].layer.alias<self.otherLayerItems[b].layer.alias){return -1;}
					return 0;
				});
				for(var i=0;i<keys.length;i++){
					self.otherLayerItems[keys[i]].legendRow.appendTo(self.otherLayersLayers);
				}
			}
		};
		this.outOfRangeLayersCheck=function(){
			var res=main.map.map.getView().getResolution();
			for(var key in self.otherLayerItems){
				if(main.map_layers[key].properties.max_res || main.map_layers[key].properties.min_res){
					if(main.map_layers[key].properties.max_res && Number(main.map_layers[key].properties.max_res)<=res){
						self.otherLayerItems[key].outOfRangeOut.removeClass('none');
						self.otherLayerItems[key].checkbox.checkbox.prop('disabled',true);
						if(self.otherLayerItems[key].checkbox.checkbox.prop('checked')){
							self.otherLayerItems[key].checkbox.checkbox.prop('checked',false).change();
							self.otherLayerItems[key].checkbox.checkbox.data('was_checked',true);
						}
					}else if(main.map_layers[key].properties.min_res && Number(main.map_layers[key].properties.min_res)>=res){
						self.otherLayerItems[key].outOfRangeIn.removeClass('none');
						self.otherLayerItems[key].checkbox.checkbox.prop('disabled',true);
						if(self.otherLayerItems[key].checkbox.checkbox.prop('checked')){
							self.otherLayerItems[key].checkbox.checkbox.prop('checked',false).change();
							self.otherLayerItems[key].checkbox.checkbox.data('was_checked',true);
						}
					}else{
						self.otherLayerItems[key].outOfRangeOut.addClass('none');
						self.otherLayerItems[key].outOfRangeIn.addClass('none');
						self.otherLayerItems[key].checkbox.checkbox.prop('disabled',false);
						if(self.otherLayerItems[key].checkbox.checkbox.data('was_checked')){
							self.otherLayerItems[key].checkbox.checkbox.prop('checked',true).change();
						}
					}
				}
			}
		};
		this.addOtherLayer=function(layer){
			var id=layer.properties.layer_name;
			if(layer.properties.geom_type=='MultiLineString'){
				var fillColor='rgba(0,0,0,0)';
				var strokeColor=layer.layer.style.params.stroke_color;
				var lineClass=' isLineStringLayerPrev'
			}else{
				var fillColor=layer.layer.style.params.fill_color;
				var strokeColor=layer.layer.style.params.stroke_color;
				var lineClass=''
			}
			self.otherLayerItems[id]={
				id:id,
				map_layer:layer,
				layer:layer.layer,
				legendRow:$('<div class="legendRow otherItem cf">'),
				layerPreview:$('<div class="layerPreview otherItem legendRowEle">'),
				layerPreviewInner:$('<div class="layerPreviewInner otherItem">'),
				layerPreviewContent:$('<div class="layerPreviewContent fill'+lineClass+'" style="background-color:'+fillColor+';border-color:'+strokeColor+'"></div>'),
				layerLabelWrap:$('<div class="layerLabelWrap legendRowEle otherItem cf">'),
				layerLabel:$('<div class="layerLabel">').html(layer.layer.alias),
				outOfRangeOut:$('<div class="flOutOfRangeNotice outOfRangeOut none">').html('(Zoom In To View)'),
				outOfRangeIn:$('<div class="flOutOfRangeNotice outOfRangeIn none">').html('(Zoom Out To View)'),
				// radio:new Radio({
				checkbox:new Checkbox({
					value:id,
					checked:layer.layer.olLayer.getVisible(),
					name:'legendLayer',
					classes:'legendLayerCheckWrap legendLayerCheckWrap_'+id,
					data:[['layerid',id]],
					wrapClasses:'legendRowEle',
					// turnOffable:true
				})
			};
			var item=self.otherLayerItems[id];
			item.layerLabelWrap
				.append(item.layerLabel)
				.append(item.outOfRangeOut)
				.append(item.outOfRangeIn);
			item.layerPreviewInner.append(item.layerPreviewContent);
			item.checkbox.checkbox.on('change',function(){
				// self.turnOffAllLayers();
				$(this).data('was_checked',$(this).prop('checked'))
				self.toggleLayer($(this).data('layerid'),$(this).prop('checked'));
			});
			self.otherLayersLayers.append(item.legendRow
				.append(item.checkbox.html)
				.append(item.layerPreview.append(item.layerPreviewInner))
				.append(item.layerLabelWrap)
			);
			self.outOfRangeLayersCheck();
			self.sortLegendItems('abc');
			// if(id!=self.layerName){main.map.mainLegend.updateLayerPreviews(item);}
			self.setHeightParams();
		};
		this.toggleAllLegend=function(){
			self.otherLayersLayers.find('.legendLayerCheckWrap').prop('checked',$(this).prop('checked')).change();
		};
		this.setDefaultStyling=function(layer){
			if(layer && layer.layer){
				layer.layer.setDefaultStyle();
			}
		};
		this.updateFilterBySelect=function(){
			self.filterBySelect.html('');
			var cFBSorted=Object.keys(self.currentFilterBys).sort(function(a,b){
				if(self.currentFilterBys[a].alias.toLowerCase()>self.currentFilterBys[b].alias.toLowerCase()){return 1;}
				if(self.currentFilterBys[a].alias.toLowerCase()<self.currentFilterBys[b].alias.toLowerCase()){return -1;}
				return 0;
			});
			var key;
			for(var i=0;i<cFBSorted.length;i++){
				key=cFBSorted[i];
				if(self.layer.fields[key] && self.layer.fields[key].active && self.layer.fields[key][main.account.loggedInUser.user_type+'_visible']/*  && self.layer.fields[key].input_type!='checkbox' */){
					selected='';
					if(self.current_filter_by && key==self.current_filter_by){selected=' selected="selected"';}
					self.filterBySelect.append('<option value="'+key+'"'+selected+'>'+self.currentFilterBys[key].alias+'</option>');
				}
			}
			self.filterBySelectHideTest();
		};
		this.addOtherLayers=function(layersToLoad){
			// self.otherLayers=main.map.mainLegend.items;
			self.otherLayerItems={};
			self.otherLayersLabel=$('<div class="otherLayersLabel">').text('Layers');
			self.otherLayersWrap=$('<div class="otherLayersWrap">');
			// self.flLegendToggleAll=new Checkbox({
				// value:'true',
				// name:'flLegendToggleAll',
				// checked:true,
				// label:"Toggle All",
				// classes:'flLegendToggleAll'
			// });
			// self.flLegendToggleAll.checkbox.on('change',self.toggleAllLegend);
			// self.otherLayersWrapTop=$('<div class="otherLayersWrapTop">')
				// .append(self.flLegendToggleAll.html);
			self.otherLayersLayers=$('<div class="otherLayersLayers">');
			self.otherLayersWrap
				// .append(self.otherLayersWrapTop)
				.append(self.otherLayersLayers);
			self.otherLayersWrapWrap=$('<div class="otherLayersWrapWrap">')
				.append(self.otherLayersLabel)
				.append(self.otherLayersWrap)
			self.otherLayers={};
			if(Object.keys(main.data.map_layers).length){
				var map_layers=main.data.map_layers;
				for(var key in map_layers){
					if(main.map_layers[key]){
						self.otherLayers[key]=main.map_layers[key];
						if(main.map_layers[key].layer){
							self.addOtherLayer(main.map_layers[key]);
						}else{
							var params={layerName:key};
							self.otherLayers[key].wait=new Wait(self.checkForLayer,self.otherLayerLoaded,params,params);
						}
					}
				}
			}
			if(main.personal_settings.app_settings.with_invs_fl_toggle || layersToLoad){
				var inventLayers=main.map_layers;
				for(var key in inventLayers){
					if(inventLayers[key].inventory && (!layersToLoad || layersToLoad.indexOf(key)>-1)){
						self.otherLayers[key]=inventLayers[key];
						if(inventLayers[key].layer){
							self.addOtherLayer(inventLayers[key]);
						}else{
							var params={layerName:key};
							self.otherLayers[key].wait=new Wait(self.checkForLayer,self.otherLayerLoaded,params,params);
						}
					}
				}
			}
			main.map.map.on('moveend',function(e){
				self.outOfRangeLayersCheck();
			});
			// for(var key in self.otherLayers){
				// if(self.showThisLayerWithOthers || (!self.showThisLayerWithOthers && self.otherLayers[key].id!=self.layerName)){
					// self.addOtherLayer(self.otherLayers[key]);
				// }
			// }
			/* if(Object.keys(self.otherLayers).lengh>0){ */self.flInvWrap.append(self.otherLayersWrapWrap);/* } */
		};
		this.checkForLayer=function(params){
			if(main.map_layers[params.layerName].layer){return true;}
		};
		this.otherLayerLoaded=function(response,params){
			self.addOtherLayer(main.map_layers[params.layerName]);
		};
		this.toggleOtherLayer=function(layerName,turnOn){
			if(self.otherLayerItems[layerName]){
				var checked;
				if(turnOn){checked=true;
				}else{checked=false;}
				self.otherLayerItems[layerName].checkbox.checkbox.prop('checked',true).change();
			}
		};
		this.hideOthers=function(dontHideOtherLayers){
			if(self.otherLayerItems){var otherLayerItemsKeys=Object.keys(self.otherLayerItems);}
			for(var key in self.layers){
				if(key!=self.layerName && main.tables[self.layerName].map_layer && main.tables[key].map_layer.layer){
					if(!self.otherLayerItems || !dontHideOtherLayers || otherLayerItemsKeys.indexOf(key)==-1 || !self.otherLayerItems[key].map_layer.properties[main.account.loggedInUser.user_type+'_visible']){
						main.tables[key].map_layer.layer.olLayer.setVisible(false);
						// if(main.invents.inventories[key] && main.invents.inventories[key].inventory.map_layer){
							
						// }
					}
				}else{
					if(main.tables[key].map_layer.layer){main.tables[key].map_layer.layer.olLayer.setVisible(true);}
				}
			}
		};
		this.setDontRenderOnMap =function(layerName){
			if(self.layer.layer && !self.layers[layerName].dont_render_on_map && !self.currentFilterBys[self.current_filter_by].dont_render_on_map && main.invents.currentInvent==self.layerName){
				self.filterGroupsHTMLWrap.removeClass('dont_render_on_map');
			}else{
				self.filterGroupsHTMLWrap.addClass('dont_render_on_map');
			}
		};
		this.changeLayerGetGroupsCB=function(args){
			self.setHTMLGroups();
			self.currentStyle=self.normalStyle;
			if(self.layer.layer && !self.layers[args.layerName].dont_render_on_map && !self.currentFilterBys[self.current_filter_by].dont_render_on_map && main.invents.currentInvent==self.layerName){
				self.layer.layer.olLayer.setStyle(self.currentStyle);
			}else{
				main.loader.loadMoreMapPoints(args.layerName);
			}
			self.setDontRenderOnMap(self.layerName);
			if(self.layers[args.layerName].hide_others_on_map){
				self.hideOthers(args.dontHideOtherLayers);
			}
			if(main.inventories[args.layerName].is_depended_upon_by){
				main.invents.clearCurrents();
			}
			if(main.dashboard){main.dashboard.refresh();}
			if(args.changeToField){
				self.changeField(args.changeToField);
			}
		};
		this.changeLayer=function(layerName,dontHideOtherLayers,changeToField,showAllPossible){
			if(showAllPossible && main.tables[layerName].properties.dependent_upon && self.layerName==main.tables[layerName].properties.dependent_upon){
				if(self.layer.inventory.is_depended_upon_by && !main.account.accountSettings[main.account.loggedInUser.user_type+'_restrict_to_own_data']){
					main.invents.inventories[main.invents.depened_upons[0]].loadFeatures({showAll:true,zoomTo:true,refresh:true});
					self.checkLoginChange();
				}
			}else{
				if(self.layer.layer && self.layer.layer.olLayer.getVisible()){self.layer.layer.olLayer.setVisible(false);}
				if(main.invents.inventories[self.layerName]){
					main.layout.removeAllPopUps();
					main.invents.checkForOtherActions();
					main.invents.currentInvent=layerName;
				}
				if(main.layout.editForms.length){
					for(var i=0;i<main.layout.editForms.length;i++){
						if(main.layout.editForms[i].inventory.inventory.properties.is_wom){
							main.layout.editForms[i].setDefaultButtons(layerName);
						}
					}
				}
				self.layerName=layerName;
				self.flLayerSelect.val(layerName);
				self.setDefaultStyling(self.layer);
				self.layer=main.inventories[self.layerName].map_layer;
				if(self.layer.layer){
					main.utilities.setTopLayer(self.layer.layer.olLayer);
					main.map.sortLayers();
				}
				self.currentFilterBys=self.layers[self.layerName].filter_bys;
				self.default_filter_by=self.layers[self.layerName].default_filter_by;
				self.current_filter_by=self.default_filter_by;
				self.fields=self.layer.fields;
				self.updateFilterBySelect();
				self.filterGroupsCharts.detach();
				self.showCharts();
				if(main.inventories[layerName].is_depended_upon_by){
					self.removedOption=self.flLayerSelect.find('option[value="'+main.inventories[layerName].is_depended_upon_by+'"]').detach();
					if(main.personal_settings.map.center){main.map.panTo(ol.proj.transform(main.personal_settings.map.center, 'EPSG:4326', 'EPSG:3857'));}
					if(main.personal_settings.map.zoom){main.map.animateZoom(main.personal_settings.map.zoom);}
				}
				self.getGroups(self.changeLayerGetGroupsCB,null,dontHideOtherLayers,changeToField);
				main.invents.unOrgFilteredAndLoaded=false;
				main.loader.loadMoreMapPoints(layerName);
			}
		};
		this.filterBySelectHideTest=function(){
			var visibles=0;
			for(var key in self.currentFilterBys){
				if(self.layer.fields[key] && self.layer.fields[key].active && self.layer.fields[key][main.account.loggedInUser.user_type+'_visible'] && self.layer.fields[key].input_type!='checkbox'){
					visibles++;
				}
			}
			if(visibles<=1){self.filterBySelectWrap.addClass('none');}
			else{self.filterBySelectWrap.removeClass('none')}
		};
		this.checkLoginChange=function(){
			if(self.layer.inventory.is_depended_upon_by && !main.account.accountSettings[main.account.loggedInUser.user_type+'_restrict_to_own_data']){
				if(!$.contains(document,self.flShowAllButton.html[0])){self.flTopButtons.append(self.flShowAllButton.html);}
			}else{self.flShowAllButton.html.detach();}
			self.flTopButtonsVisCheck();
			if(main.personal_settings.app_settings.with_filter_legend_other_layers){
				if(self.otherLayerItems){
					self.chechOtherLayerButtonsOff();
				}
			}
		};
		this.chechOtherLayerButtonsOff=function(){
			for(var key in main.filter_legend.otherLayerItems){
				if(main.filter_legend.otherLayerItems[key].layer){
					self.otherLayersLayers.find('.legendLayerCheckWrap_'+key).prop('checked',main.filter_legend.otherLayerItems[key].layer.olLayer.getVisible());
				}
			}
		};
		this.flTopButtonsVisCheck=function(){
			if(self.flTopButtons.find('.button:not(.none)').length){
				self.flTopButtons.removeClass('none');
			}else{
				self.flTopButtons.addClass('none');
			}
		};
		this.clearShowingDisplay=function(){
			self.loadMoreShowing.html('0');
			self.loadMoreTotal.html('0');
			self.loadMoreItems.html(main.invents.inventories[self.layerName].plural_alias.toLowerCase());
		};
		this.updateShowingDisplay=function(outOf,onlyShowing){
			var features=main.tables[self.layerName].map_layer.layer.olLayer.getSource().getFeatures(),style,ct=0;
			for(var i=0;i<features.length;i++){
				style=self.normalStyle(features[i])[0];
				if(!style || style.getFill().getColor()!='rgba(0,0,0,0)'){ct++;}
			}
			self.loadMoreShowing.html(main.utilities.addCommas(ct.toString()));
			if(!onlyShowing){
				outOf=outOf || 0;
				self.loadMoreTotal.html(main.utilities.addCommas(outOf.toString()));
				var inventory=main.invents.inventories[self.layerName];
				var alias=inventory.plural_alias;
				if(outOf==1){alias=inventory.singular_alias;}
				self.loadMoreItems.html(alias.toLowerCase());
			}
		};
		this.filterBySelectGetGroupsCB=function(){
			self.setHTMLGroups();
			self.currentStyle=self.normalStyle;
			if(!self.layers[self.layerName].dont_render_on_map && !self.currentFilterBys[self.current_filter_by].dont_render_on_map){
				if(main.invents.currentInvent==self.layerName){
					self.layer.layer.olLayer.setStyle(self.currentStyle);
				}
			}else{
				self.setDefaultStyling(self.layer);
			}
		};
		this.updateList=function(filterStr){
			if(filterStr){self.filterStr=filterStr.toLowerCase();}else{self.filterStr=null;}
			self.setHTMLGroups(true);
		};
		this.convertToWithCanopy=function(){
			self.withCanopyMode=true;
			var canopy_with_invents=main.personal_settings.app_settings.canopy_with_invents;
			var firstLayer=Object.keys(canopy_with_invents)[0];
			if(firstLayer!=self.layerName){
				self.changeLayer(this.primaryLayer.layer_name,null,canopy_with_invents[firstLayer].defaultField);
			}else if(canopy_with_invents[firstLayer].defaultField!=self.current_filter_by){
				self.changeField(canopy_with_invents[firstLayer].defaultField);
			}
			if(!self.flCanopyWrap){
				self.flCanopyWrap=$('<div class="flCanopyWrap">');
				self.flCanopyCanopyLabel=$('<div class="flCanopyLabel flCanopyCanopyLabel">Canopy Assess</div>');
				self.flCanopyCanopyWrapWrap=$('<div class="flCanopyCanopyWrapWrap flCanopyWrapWrap">')
					.append(self.flCanopyCanopyLabel)
					.append(self.flCanopyWrap);
				self.flCanopyInvLabel=$('<div class="flCanopyInvLabel flCanopyLabel">'+main.invents.inventories[firstLayer].plural_alias+'</div>');
				self.flCanopyInvWrapWrap=$('<div class="flCanopyInvWrapWrap flCanopyWrapWrap">')
					.append(self.flCanopyInvLabel);
			}
			// if(!self.tabs){
				// self.tabs=new Tabs();
				// self.invTab=self.tabs.addTab('Inventories',self.flInvWrap);
				// self.canopyTab=self.tabs.addTab('Canopy',self.flCanopyWrap);
			// }else{
				// self.tabs.setTabContent(self.flInvWrap,self.invTab.tabid);
			// }
			// self.tabs.setActiveTab(self.invTab.tabid);
			// self.fLFLWrap.append(self.tabs.html);
			
			self.panel.html.addClass('canopyFLPanel');
			self.flInvNormWrap.addClass('none');
			self.fLFLWrap.prepend(self.flCanopyCanopyWrapWrap);
			self.fLFLWrap.append(self.flCanopyInvWrapWrap);
			self.flCanopyInvWrapWrap.append(self.filterGroupsHTMLWrap);
			// if(!self.otherLayersWrapWrap){self.addOtherLayers(Object.keys(main.personal_settings.app_settings.canopy_with_invents));
			// }else{self.flInvWrap.append(self.otherLayersWrapWrap);}
			self.panel.positionPanel();
		};
		this.convertFromWithCanopy=function(){
			// self.tabs.html.detach();
			// self.fLFLWrap.append(self.flInvWrap);
			self.panel.html.removeClass('canopyFLPanel');
			self.flInvNormWrap.removeClass('none');
			self.flCanopyCanopyWrapWrap.detach();
			self.flCanopyInvWrapWrap.detach();
			self.flNoResultsWrap.after(self.filterGroupsHTMLWrap);
			// self.otherLayersWrapWrap.detach();
			self.panel.positionPanel();
			self.changeField(self.default_filter_by);
			self.withCanopyMode=false;
		};
		this.changeField=function(val){
			self.current_filter_by=val;
			self.getGroups(self.filterBySelectGetGroupsCB);
		};
		(this.createHTML=function(){
			var selected;
			self.filterBySelect=$('<select class="filterBySelect flOptionsSelect fLeft">');
			self.filterBySelectWrap=$('<div class="filterBySelectWrap filterLOptionsWrapEle cf">')
				.append($('<div class="flOptionsLabel fLeft">').html('Display by:&nbsp;'))
				.append(self.filterBySelect)
			self.filterLOptionsWrap=$('<div class="filterLOptionsWrap">')
				.append(self.filterBySelectWrap);
			// if(Object.keys(self.layers).length>1){
				self.flLayerSelect=$('<select class="flLayerSelect flOptionsSelect fLeft">');
				/* var layersKeys=Object.keys(self.layers).sort(function(a,b){
					if(main.tables[self.layers[a].layer_name].properties.alias.toLowerCase()>main.tables[self.layers[b].layer_name].properties.alias.toLowerCase()){return 1;}
					if(main.tables[self.layers[a].layer_name].properties.alias.toLowerCase()<main.tables[self.layers[b].layer_name].properties.alias.toLowerCase()){return -1;}
					return 0;
				}); */
				var layersKeys=Object.keys(self.layers).sort(function(a,b){
					if(!self.layers[b].place_in_order){return -1}
					if(self.layers[a].place_in_order>self.layers[b].place_in_order){return 1;}
					if(self.layers[a].place_in_order<self.layers[b].place_in_order){return -1;}
					return 0;
				});
				var key;
				for(var i=0;i<layersKeys.length;i++){
					key=layersKeys[i];
					if(!main.tables[key].properties.dependent_upon || main.tables[key].overRideDependentOn){
						selected='';
						if(self.layerName && self.layers[key].layer_name==self.layerName){selected=' selected="selected"';}
						self.flLayerSelect.append('<option value="'+self.layers[key].layer_name+'"'+selected+'>'+main.tables[self.layers[key].layer_name].properties.alias+'</option>');
					}
				}
				self.flLayerSearch=$('<input type="text" class="flLayerSearch">');
				self.flLayerSearch.on('keyup',function(){
					self.updateList($(this).val())
				});
				self.flLayerSearchWrap=$('<div class="flLayerSearchWrap filterLOptionsWrapEle cf">')
					.append($('<div class="flOptionsLabel fLeft">').html('Search:&nbsp;'))
					.append(self.flLayerSearch);
				self.flOptionsSearchWrap=$('<div class="flOptionsSearchWrap filterLOptionsWrap ">')
					.append(self.flLayerSearchWrap);
				if(main.personal_settings.app_settings.legend_layer_text){
					var layerText=main.personal_settings.app_settings.legend_layer_text;
				}else{var layerText='Layer';}
				self.flLayerSelectWrap=$('<div class="flLayerSelectWrap filterLOptionsWrapEle cf">')
					.append($('<div class="flOptionsLabel fLeft">').html(layerText+':&nbsp;'))
					.append(self.flLayerSelect);
				self.filterLOptionsWrap.prepend(self.flLayerSelectWrap);
				self.flLayerSelect.on('change',function(){
					if(main.invents && main.invents.currentHighlightFilterLayer){main.invents.resetHighlightFilter();}
					self.changeLayer($(this).val());
				});
			// }
			self.updateFilterBySelect();
			if(self.layer && self.layer.layer){
				main.utilities.setTopLayer(self.layer.layer.olLayer);
			}
			self.filterBySelect.on('change',function(){
				self.changeField($(this).val());
			});
			self.content=$('<div class="filterLegend">');
			/* if(self.primary_group_by && self.settings.view_all){
				var field=self.fields[self.primary_group_by];
				self.viewAllGroups={};
				if(field.filter_l_options){
					self.viewAllGroups.options=field.options;
					self.viewAllGroups.ogroups=self.viewAllGroups.options;
				}else{
					self.viewAllGroups.natural_groups=self.getGroups(field);
					self.viewAllGroups.ogroups=self.viewAllGroups.natural_groups;
				}
				self.viewAllHTMLShadow=$('<div class="viewAllHTMLShadow">');
				self.viewAllHTML=$('<div class="viewAllHTML">').append(self.viewAllHTMLShadow);
				self.viewAllHTMLToggle=new Checkbox({
						label:'View All',
						value:'true',
						name:'viewAll',
						classes:'viewAllHTMLToggle',
						checked:true
					});
				self.viewAllHTMLToggleWrap=$('<div class="viewAllHTMLToggleWrap">')
					.append(self.viewAllHTMLToggle.html);
				self.viewAllHTMLToggle.checkbox.on('change',function(){
					if($(this).prop('checked')){
						self.turnViewAllOn();
					}else{
						self.turnViewAllOff();
					}
				});
				self.viewAllHTMLContent=$('<div class="viewAllHTMLContent">');
				var groupCount=Object.keys(self.viewAllGroups.ogroups).length,i=0,color,/* stroke,width,strokeColorSVG,strokeWidthSVG;
				self.viewAllGroups.groups={};
				for(var key in self.viewAllGroups.ogroups){
					self.viewAllGroups.groups[key]={
						group:self.viewAllGroups.ogroups[key],
						color:main.utilities.getColor(groupCount-1,0,i,self.defaultColorGrad)
					}
					self.viewAllGroups.groups[key].rgba='rgba('+self.viewAllGroups.groups[key].color[0]+','+self.viewAllGroups.groups[key].color[1]+','+self.viewAllGroups.groups[key].color[2]+','+self.viewAllGroups.groups[key].color[3]+')';
					self.viewAllGroups.groups[key].style=self.getStyle(self.viewAllGroups.groups[key].rgba);
					// stroke=self.viewAllGroups.groups[key].style.getStroke();
					// strokeWidthSVG='';
					// strokeColorSVG='';
					// if(stroke){
						// color=stroke.getColor();
						// width=stroke.getWidth();
						// if(width){
							// strokeWidthSVG=' stroke-width="'+width+'"';
						// }
						// if(width){
							// strokeColorSVG=' stroke="'+color+'"';
						// }
					// }
					// self.viewAllGroups.groups[key].svg='<svg height="16" width="16"><circle cx="8" cy="8" r="7"'+strokeColorSVG+strokeWidthSVG+' fill="'+self.viewAllGroups.groups[key].rgba+'"/></svg>';
					self.viewAllGroups.groups[key].svg='<svg height="16" width="16"><circle cx="8" cy="8" r="7" stroke="rgb(0,0,0)" stroke-width="1" fill="'+self.viewAllGroups.groups[key].rgba+'"/></svg>';
					self.viewAllHTMLContent.append(
						$('<div class="naturalBreakUnit cf">')
							.append($('<div class="nbSymbol fLeft">').html(self.viewAllGroups.groups[key].svg))
							.append($('<div class="nbLabel fLeft">').html(self.viewAllGroups.groups[key].group.alias))
					);
					i++;
				}
				self.content
					.append(self.viewAllHTML.append(self.viewAllHTMLToggleWrap).append(self.viewAllHTMLContent));
			} */
			self.filterGroupsHTML=$('<div class="filterGroupsHTML">');
			self.filterGroupsHTMLWrapO=$('<div class="filterGroupsHTMLWrapO flList">').append(self.filterGroupsHTML);
			self.filterGroupsHTMLWrapInner=$('<div class="filterGroupsHTMLWrapInner">').append(self.filterGroupsHTMLWrapO);
			self.flToggleAll=new Checkbox({
				value:'true',
				name:'flToggleAll',
				checked:true,
				label:"Toggle All",
				classes:'flToggleAll'
			});
			self.flToggleAll.checkbox.change(self.toggleChecks);
			self.filterGroupsHTMLTop=$('<div class="filterGroupsHTMLTop">')
				.append(self.flToggleAll.html);
			self.flNoResultsWrap=$('<div class="flNoResultsWrap none">').text('No Results. Start Adding.')
			self.filterGroupsHTMLWrap=$('<div class="filterGroupsHTMLWrap">')
				.append(self.filterGroupsHTMLTop)
				.append(self.filterGroupsHTMLWrapInner);
			// if(self.layers[self.layerName].dont_render_on_map || self.currentFilterBys[self.current_filter_by].dont_render_on_map){self.filterGroupsHTMLWrap.addClass('dont_render_on_map');}
			self.setDontRenderOnMap(self.layerName);
			// self.showOtherInvents=$('<div class="filterLShowOtherInvents">');
			self.naCharts=$('<div class="filterLNACharts">').text('N/A');
			self.filterGroupsCharts=$('<div class="filterGroupsCharts">');
			self.filterGroupsChartsExtras=$('<div class="filterGroupsChartsExtras">');
			self.openFilterGroupsChartsExtras=new Button('std','openFilterGroupsChartsExtras','Options');
			self.openFilterGroupsChartsExtras.html.on('click',function(){
				self.filterGroupsChartsExtrasWrap.slideToggle(200);
			});
			self.filterGroupsChartsExtrasTop=$('<div class="filterGroupsChartsExtrasTop">')
				.append(self.openFilterGroupsChartsExtras.html);
			self.filterGroupsChartsExtrasWrap=$('<div class="filterGroupsChartsExtrasWrap">');
			self.showNotSpecified=new Checkbox({
				value:'true',
				name:'showNotSpecified',
				label:"Show 'Not Specified'",
				classes:'showNotSpecified'
			});
			self.showNotSpecified.checkbox.on('change',function(){
				if(!self.layers[self.layerName].dont_show_graphs && self.layers[self.layerName].with_charts){
					self.fullChartReset();
				}
			});
			self.filterGroupsChartsExtras
				.append(self.filterGroupsChartsExtrasTop)
				.append(self.filterGroupsChartsExtrasWrap
					.append(self.showNotSpecified.html));
			self.chartTabs=new Tabs();
			var tab,chart,activeTab,i=0;
			for(var key in self.chartsSettings){
				chart=self.chartsSettings[key];
				self.chartsSettings[key].container=$('<div class="flChartContainer '+key+'" data-chart="'+key+'">')
				tab=self.chartTabs.addTab(chart.alias,$('<div class="flChartTab">')
					.append(self.chartsSettings[key].container),key,self.newTabOpened);
				if(i==0){activeTab=tab;i++;}
			}
			self.chartTabs.setActiveTab(activeTab.tabid);
			// self.loadMoreButton=new Button('std','loadMoreButton','Load More');
			// self.loadMoreButton.html.addClass('lMDEle').on('click',function(){
				// main.loader.loadMoreMapPoints(self.layerName);
			// });
			self.loadMoreShowing=$('<span class="loadMoreShowing">');
			self.loadMoreItems=$('<span class="loadMoreItems">');
			self.loadMoreTotal=$('<span class="loadMoreTotal">');
			self.loadMoreDetails=$('<div class="loadMoreDetails">')
				.append('Showing ')
				.append(self.loadMoreShowing)
				.append(' of ')
				.append(self.loadMoreTotal)
				.append(' ')
				.append(self.loadMoreItems)
				.append('.');
			self.loadMoreDetailsWrap=$('<div class="loadMoreDetailsWrap lMDEle">')
				.append(self.loadMoreDetails);
			self.loadMoreWrap=$('<div class="flLoadMoreWrap cf">')
				// .append(self.loadMoreButton.html)
				.append(self.loadMoreDetailsWrap);
			self.flInvNormWrap=$('<div class="flInvNormWrap">');
			self.flInvWrap=$('<div class="flInvWrap">').append(self.flInvNormWrap);
			self.fLFLWrap=$('<div class="fLFLWrap">')
				.append(self.flInvWrap);
			self.flInvNormWrap.append(self.filterLOptionsWrap);
			self.flTopButtons=$('<div class="flTopButtons cf">')
			if(self.with_load_more){self.flInvNormWrap.append(self.loadMoreWrap);}
			if(self.withSearch){self.flInvNormWrap.append(self.flOptionsSearchWrap)}
			self.flShowAllButton=new Button('std','flTopButtonsButton','Show All');
			self.flShowAllButton.html.on('click',function(){
				main.invents.inventories[main.invents.depened_upons[0]].loadFeatures({showAll:true,zoomTo:true,refresh:true});
				self.checkLoginChange();
			});
			self.checkLoginChange();
			self.flInvNormWrap
				.append(self.flTopButtons)
				.append(self.flNoResultsWrap)
				.append(self.filterGroupsHTMLWrap)
				
			self.filterGroupsCharts
				.append(self.chartTabs.html)
				.append(self.filterGroupsChartsExtras);
			self.showCharts();
			self.content.append(self.fLFLWrap);
			self.content.append(self.loadBox);
			
			self.loader=new LoaderBox({message:'Loading'});
			self.loader.html.addClass('flLoader');
			self.content.append(self.loader.html);
			
			//add to settings
			if(self.inSettings){
				var settingsStuff=main.settings.addSetting(self.alias);
				self.showButton=settingsStuff.showButton;
				self.showButton.html.html('On');
				self.hideButton=settingsStuff.hideButton;
				self.hideButton.html.html('Off');
				self.showButton.html.on('click',function(){
					self.turnOn();
				});
				self.hideButton.html.on('click',function(){
					self.turnOff();
				});
			}
			self.panel=new Panel({
				content:self.content,
				title:self.alias,
				classes:'filterLegendPanel',
				position:'tRight',
				relativeTo:main.layout.mapContainerWrap,
				onClose:self.panelClose,
				onMin:self.panelMin,
				onShow:self.panelShow,
				withOutClose:true,
				withoutResize:true
			});
			// self.panel.open();
			if($('body').width()>main.dimensions.phoneWidth){
				if(self.setToApp){self.show();}else{self.updateButtonsHide()}
			}else{
				self.show();
				self.panel.minimizePanel();
			}
			if(self.layerName && main.invents.inventories[self.layerName] && main.invents.inventories[self.layerName].layerLoaded){
				self.layerLoaded();
			}
			//other layers
			// if(main.personal_settings.app_settings.with_filter_legend_other_layers && main.map.mainLegend){
				// self.addOtherLayers();
			// }
			if(main.personal_settings.app_settings.with_filter_legend_other_layers && (Object.keys(main.data.map_layers).length || main.personal_settings.app_settings.with_invs_fl_toggle)){
				self.addOtherLayers();
			}
		})();
		(this.initEvents=function(){
			$(window).resize(function(){
				self.setHeightParams();
			});
		})();
	}
});