define(function(require){
	return function pg(map){
		var self=this;
		var Accordian=require('./Accordian');
		var Button=require('./Button');
		var Form=require('./Form');
		this.alias='Admin',
		this.formList=[
			'tree_tenders_groups'
		],
		this.forms={},
		this.createHTML=function(){
			this.addSubAccordian();
			this.addShelf();
			this.addForms();
		},
		this.formDataTransfer=function(){
			var form;
			for(var i=0;i<this.formList.length;i++){
				this.forms[this.formList[i]]=main.data.forms[this.formList[i]];
				form=this.forms[this.formList[i]];
				form.appForm=new Form(form);
				main.layout.forms[this.formList[i]]=form;
			}
		},
		this.addShelf=function(){
			this.launchButton=new Button('toolBox','',this.alias);
			this.shelf=main.layout.admin.createShelf('admin',this.launchButton.html,this.subContent.subAccordian.container);
			main.layout.admin.container.append(this.shelf);
		},
		this.addSubAccordian=function(){
			//add forms accordian
			this.formsAccordian=new Accordian('adminForms');
			this.subContent={forms:{}};
			this.subContent.subAccordian=new Accordian('adminSubAcc');
			this.subContent.forms.launchButton=new Button('toolBox','','Admin Forms');
			this.subContent.forms.shelf=this.subContent.subAccordian.createShelf('adminFormsShelf',this.subContent.forms.launchButton.html,this.formsAccordian.container);
			this.subContent.subAccordian.container.append(this.subContent.forms.shelf);
		},
		this.addForms=function(){
			self=this;
			//add forms to forms accordian
			for(var key in this.forms){
				if(this.forms[key].position=='side_panel'){
					this.forms[key].launchButton=new Button('toolBox','',this.forms[key].alias);
					this.forms[key].shelf=this.formsAccordian.createShelf(key,this.forms[key].launchButton.html,this.forms[key].appForm.html);
					this.formsAccordian.container.append(this.forms[key].shelf);
				}
				if(this.forms[key].shortcut){
					main.layout.shortcuts[this.forms[key].name]=main.layout.header.createHeaderButton('iconButton','Admn Grps',[['button','admin']]);
					main.layout.header[this.forms[key].shortcut].append(main.layout.shortcuts[this.forms[key].name].html);
					main.layout.shortcuts[this.forms[key].name].html.click(function(){
						self.formsAccordian.openAllParents($(self.forms[key].target));
						main.layout.sidePanels[$(this).data('button')].open();
					});
				}
				if(this.forms[key].extended){
					this.forms[key].appForm.html.closest('.shelf').addClass('extended')
				}
			}
		}
		this.formDataTransfer();
		this.createHTML();
	};
});