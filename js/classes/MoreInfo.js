define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		this.offsetX=0;
		this.offsetY=-5;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.setContainerPos=function(){
			var offset=self.activateButton.offset();
			var left=offset.left+self.activateButton.outerWidth()+self.offsetX;
			var top=offset.top+self.offsetY;
			var bodyHeight=$('body').height();
			var mIContainerHeight=self.mIContainer.outerHeight(true);
			if((top+mIContainerHeight)>bodyHeight){
				top=bodyHeight-mIContainerHeight;
			}
			if(top<0){top=0;}
			self.mIContainer.css('left',left);
			self.mIContainer.css('top',top);
		};
		(this.create=function(){
			self.activateButton=$('<div class="mIActivate">').text('?');
			self.contentContainer=$('<div class="mIContentContainer">');
			if(self.content){
				self.contentContainer.html(self.content);
			}
			self.mIContainer=$('<div class="mIContainer">')
				.append(self.contentContainer);
			if(self.title){
				self.titleWrap=$('<div class="mITitle">').html(self.title);
				self.mIContainer.prepend(self.titleWrap);
			}
			self.html=$('<div class="moreInfoWrap">')
				.append(self.activateButton)
				.append(self.mIContainer);
			self.activateButton.add(self.mIContainer).on('mouseenter',function(){
				self.mIContainer.addClass('moreInfoActive');
				main.layout.moreInfosContainer.append(self.mIContainer);
				self.setContainerPos();
			}).on('mouseleave',function(){
				self.mIContainer.removeClass('moreInfoActive');
				self.html.append(self.mIContainer);
			});
		})();
	};
});