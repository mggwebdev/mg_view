define(function(require){
	return function pg(args){
		var self=this;
		var Accordian=require('./Accordian');
		var Button=require('./Button');
		var DataTable=require('./DataTable');
		var Form=require('./Form');
		var Input=require('./Input');
		var SidePanel=require('./SidePanel');
		var Uploader=require('./Uploader');
		var ViewMore=require('./ViewMore');
		String.prototype.trunc=String.prototype.trunc ||
		function(n){
			return this.length>n?this.substr(0,n-1)+'...':this.substr(0);
		};
		this.generateUniqueID=function(){
			function gen4(){
				return Math.floor((1+Math.random())*0x10000)
					.toString(16)
					.substring(1);
			}
			return gen4()+gen4()+gen4()+gen4();
		};
		this.addLayoutItems=function(name,alias,context,imgPath,omitAccordian,onSPOpen,headerRight,onSPClose){
			var headerItems=self.addToHeader(name,alias,imgPath,headerRight);
			var sidePanelItems=self.addSidePanel(name,alias,omitAccordian,onSPOpen,onSPClose,'place_in_order');
			context.headerButton=headerItems.headerButton;
			for(var key in sidePanelItems){
				context[key]=sidePanelItems[key];
			}
		};
		this.deleteFile=function(paths,onSuccess){
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						paths:paths
					},
					action:'deleteFile'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							// if(onSuccess){self.onSuccess(response.path);}
						}else{
							if(response.ERR_CODE==1){main.utilities.loggedOutNotice();}
							else{alert('Error');}
							console.log(response);
						}
						main.layout.loader.loaderOff();
					}
				}
			});
		};
		this.addToHeader=function(name,alias,imgPath,headerRight){
			var header=main.layout.header;
			if(name=='forms' && main.personal_settings.app_settings.forms_button_text){alias=main.personal_settings.app_settings.forms_button_text;}
			header[name+'_button']=header.createHeaderButton(name+' iconButton',alias,[['button',name]],imgPath,alias);
			if(headerRight){header.headerRight.append(header[name+'_button'].html);
			}else{header.headerLeft.append(header[name+'_button'].html);}
			header[name+'_mobile_button']=header.createMobileMenuItem(name,alias,[['button',name]],imgPath,alias);
			header.mobileMenu.append(header[name+'_mobile_button'].html);
			return {
				headerButton:header[name+'_button']
			}
		};
		this.addSidePanel=function(name,alias,omitAccordian,onOpen,onClose,sortOrder){
			var toReturn={};
			if(!omitAccordian){
				sortOrder=sortOrder || 'abc';
				main.layout[name]=new Accordian(name,sortOrder);
				var content=main.layout[name].container;
				toReturn.accordian=main.layout[name];
			}else{
				var content='';
			}
			main.layout.sidePanels[name]=new SidePanel({
				openButtons:main.layout.header[name+'_button'].html,
				mobileMenuButtons:main.layout.header[name+'_mobile_button'].html,
				initialContent:content,
				onOpen:onOpen,
				onClose:onClose,
				name:name
			});
			main.layout.bodyContentWrap.append(main.layout.sidePanels[name].container);
			toReturn.sidePanel=main.layout.sidePanels[name];
			return toReturn;
		};
		this.getTimeStamp=function(dateStr){
			dateStr=dateStr.replace(/-/g,'/')
			return new Date(dateStr).getTime();
		};
		this.addLeadingZero=function(value){
			if(typeof value!='string'){value=value.toString();}
			if(value.length===1){value='0'+value}
			return value;
		};
		this.getSimpleLookUpAliasNoField=function(val,lookup_table,primary_field/* ,cultivarField */){
			var html=val,lutValue;
			var lut=main.data.lookUpTables[lookup_table].items;
			if(lut){
				var aliasName=primary_field || 'alias';
				var lutValue;
				if(lut[val]){lutValue=lut[val][aliasName];}
				if(!lutValue && field.options){lutValue=field.options[val];}
				if(!lutValue){lutValue='';}
				if(typeof lutValue=='object' && lutValue.alias){lutValue=lutValue.alias}
				// if(lutValue && lookup_table=='species' && cultivarField && lookupTableMap[lookup_table][cultivarField]){
					// var cultivar=lut[val][lookupTableMap[lookup_table][cultivarField]];
					// if(cultivar){lutValue+=' ('+cultivar+')';}
				// }
				html=lutValue;
			}else{
				html=val;
			}
			return html;
		};
		this.getSimpleLookUpAlias=function(val,field,lookupTableMap,cultivarField){
			var html=val,lutValue;
			var lut=main.data.lookUpTables[field.lookup_table].items;
			if(lut){
				var aliasName=lookupTableMap[field.lookup_table][field.name] || 'alias';
				var lutValue;
				if(lut[val]){lutValue=lut[val][aliasName];}
				if(!lutValue && field.options){lutValue=field.options[val];}
				if(!lutValue){lutValue='';}
				if(typeof lutValue=='object' && lutValue.alias){lutValue=lutValue.alias}
				if(lutValue && field.lookup_table=='species' && cultivarField && lookupTableMap[field.lookup_table][cultivarField]){
					var cultivar=lut[val][lookupTableMap[field.lookup_table][cultivarField]];
					if(cultivar){lutValue+=' ('+cultivar+')';}
				}
				html=lutValue;
			}else{
				html=val;
			}
			return html;
		};
		this.getLookUpTableHTML=function(lookupTable,val,fieldName,layerName,field){
			var table=main.data.lookUpTables[lookupTable];
			if(table.items[val]){
				if(main.tables[layerName].properties.lookup_table_map){
					return table.items[val][main.tables[layerName].properties.lookup_table_map[lookupTable][fieldName]]
				}
			}else if(field.options && field.options[val]){
				return field.options[val].alias;
			}else{
				return '';
			}
			return null;
		};
		this.setValToInput=function(input,val,field){
			if(field.input_type=='text' || field.input_type=='number' || field.input_type=='textarea' || field.input_type=='select' || field.input_type=='radio'){
				input.val(val);
			}else if(field.input_type=='checkbox'){
				var vals=val.split(',');
				input.find('.checkbox').prop('checked',false);
				for(var i=0;i<vals.length;i++){
					input.find('.checkbox[data="'+vals[i]+'"]').prop('checked',true);
				}
			}
		};
		this.callFunctionFromName=function(functionName,context,args){
			var args=[].slice.call(arguments).splice(2);
			var namespaces=functionName.split(".");
			var func=namespaces.pop();
			for(var i=0;i<namespaces.length;i++){
				context=context[namespaces[i]];
			}
			return context[func].apply(this,args);
		};
		this.updateFilterLegend=function(inv){
			if(main.filter_legend){
				if(inv){
					if(main.filter_legend.layers[inv]){
						if(main.filter_legend.layerName!=inv){
							main.filter_legend.changeLayer(inv);
						}else{
							main.filter_legend.refreshPoints();
						}
					}
				}else{
					main.filter_legend.refreshPoints();
				}
			}
		};
		this.getSeasonFromMonth=function(month){
			var season=null;
			for(var key in main.misc.seasons){
				months=main.misc.seasons[key].months;
				for(var i=0;i<months.length;i++){
					if(month>=months[i][0] && month<=months[i][1]){
						season=main.misc.seasons[key];
					}
				}
			}
			return season;
		};
		this.formatSeason=function(seasonStuff){
			return main.misc.seasons[seasonStuff.season].alias+' '+seasonStuff.year;
		};
		this.getSeasonHTMLFromTimeStamp=function(val){
			if(val && !isNaN(val)){
				val=Number(val);
				var date=new Date(val);
				return self.getSeasonFromMonth(date.getMonth()).alias+' '+date.getFullYear();
			}
		};
		this.getSeasonFromTimeStamp=function(val){
			var date=new Date(val);
			var month=date.getMonth();
			return {
				season:self.getSeasonFromMonth(month).name,
				year:date.getFullYear()
			}
		};
		this.timeToMS=function(time){
			if(time){
				var offset=19*60*60*1000;
				var date=new Date('12/31/1969 '+time).getTime();
				var ms=date+offset;
				return ms;
			}else{
				return null;
			}
		};
		this.msToTime=function(ms){
			var html='';
			if(!ms && ms!=0){ms='';}
			if(ms || ms==0){
				//conver milliseconds into timestamp
				var offset=19*60*60*1000;
				value=ms-offset;
				var date=new Date(value);
				var hours=date.getHours();
				var hoursData=self.getHours(hours);
				hours=hoursData.hours;
				var mins=date.getMinutes();
				var secs=date.getSeconds();
				var ampm=hoursData.ampm;
				return {
					hours:hours,
					mins:mins,
					secs:secs,
					ampm:ampm,
					html:hours+':'+self.addLeadingZero(mins)+' '+ampm
				};
			}else{
				return null;
			}
		};
		this.getHours=function(hours){
			var formattedHours=hours;
			var ampm='AM';
			if(hours>12){formattedHours-=12;
			}else if(hours===0){formattedHours=12;}
			if(hours>=12){ampm='PM';}
			return {hours:formattedHours,ampm:ampm};
		};
		this.formatDate=function(timestamp,withTime,omitDate,forInput,noLeadingZeros,forSeason){
			var returnDate='';
			if((timestamp || timestamp==0) && !isNaN(timestamp)){
				timestamp=Number(timestamp);
				var date=new Date(timestamp);
				if(!forSeason){
					if(!omitDate){
						if(!noLeadingZeros){
							var month=self.addLeadingZero((date.getMonth()+1));
							var day=self.addLeadingZero(date.getDate());
						}else{
							var month=date.getMonth()+1;
							var day=date.getDate().toString();
						}
						if(!forInput){
							returnDate=month+'/'+day+'/'+(date.getFullYear())
						}else{
							returnDate=date.getFullYear()+'-'+month+'-'+day;
						}
					}
					if(withTime){
						var hours=date.getHours();
						var formattedHours=hours;
						var ampm='AM';
						if(hours>12){formattedHours-=12;
						}else if(hours===0){formattedHours=12;}
						if(hours>=12){ampm='PM';}
						returnDate+=' '+formattedHours+':'+self.addLeadingZero(date.getMinutes())+':'+self.addLeadingZero(date.getSeconds())+' '+ampm;
					}
				}else{
					var month=date.getMonth(),season;
					if(month>=11 || month<=1){
						season='Winter';
					}else if(month>=2 && month<=4){
						season='Spring';
					}else if(month>=5 && month<=7){
						season='Summer';
					}else if(month>=8 && month<=10){
						season='Fall';
					}
					return {
						season:season,
						year:date.getFullYear()
					}
				}
			}
			return returnDate;
		};
		this.getAvg=function(numbers){
			var total=0;
			$.each(numbers,function(){
				total+=this;
			});
			return total/numbers.length;
		};
		this.getStdDev=function(numbers){
			var total=0;
			$.each(numbers,function(){
				total+=this;
			});
			var mean=total/numbers.length;
			var variCalc=0;
			$.each(numbers,function(){
				variCalc+=Math.pow(this-mean,2);
			});
			var variance=variCalc/(numbers.length-1);
			return {
				stdDev:Math.sqrt(variance),
				mean:mean
			}
		};
		this.invertObject=function(obj){
			var newObj={};
			for(var key in obj){
				newObj[obj[key]]=key;
			}
			return newObj;
		};
		this.getFormPkg=function(fields,inCreateTest,fieldLev2,fieldCurrentVals){
			var formPkg={},field;
			for(var key in fields){
				if(!fieldLev2){field=fields[key];
				}else{field=fields[key].field;}
				if(field.active && field.input_type!='html' && (!inCreateTest || field.in_create)){
					if(!fieldCurrentVals){var value=field.current_val;}
					else{var value=fieldCurrentVals[key].current_val;}
					if(value===undefined){value=null;}
					formPkg[key]={
						value:value,
						stored_inventory:field.stored_inventory,
						data_type:field.data_type,
						sub_data_type:field.sub_data_type,
						input_type:field.input_type,
						name:key
					}
					if(field.input_type=='signature'){
						formPkg[key].sigImg=field.input.signature.img;
					}
				}
			}
			for(var key in fields){
				if(fields[key].val_function){
					formPkg=main.utilities.callFunctionFromName(fields[key].val_function,window,{formPkg:formPkg});
				}
			}
			return formPkg;
		};
		this.submitMultipleForms=function(rows,table,alertMsg,openInfo,callback){
			var formPkgs=[],formPkg,fields,field;
			for(var i=0;i<rows.length;i++){
				formPkgs.push(JSON.stringify(main.utilities.getFormPkg(rows[i].fields,true,false,rows[i].fieldCurrentVals)));
			}
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						formPkgs:formPkgs,
						table:table
					},
					action:'submitForms'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							if(response.results[0]){
								for(var i=0;i<response.results.length;i++){
									if(main.formsMod.forms0[table] && main.formsMod.forms0[table].dataTable && main.formsMod.forms0[table].dataTable.loaded){
										main.formsMod.forms0[table].dataTable.addData(response.results[i]);
									}
								}
								if(alertMsg){alert(alertMsg);}
								if(openInfo){self.openForm(openInfo[0],openInfo[1]);}
							}
						}else{
							alert('Error');
							console.log(response);
						}
					}
					if(callback){callback();}
				}
			});
		};
		this.getPolygonData=function(value){
			// if(value.type=="MultiPoint"){
				// return main.utilities.getMultiPointData(value);
			// }else if(value.type=="Point"){
				// return main.utilities.getPointData(value);
			// }
			return {
				valStr:JSON.stringify(value),
				html:'Click for Details',
				obj:value
			}
		};
		this.getMultiPointData=function(value){
			var valStr='';
			var htmlStr='';
			var coods=[];
			for(var i=0;i<value.coordinates.length;i++){
				valStr+=value.coordinates[i][0]+' '+value.coordinates[i][1]+',';
				htmlStr+='['+value.coordinates[i][1]+','+value.coordinates[i][0]+'],';
				coods.push(ol.proj.transform([Number(value.coordinates[i][0]),Number(value.coordinates[i][1])], 'EPSG:4326', 'EPSG:3857'));
			}
			return {
				valStr:valStr.replace(/,([^,]*)$/,''),
				html:htmlStr.replace(/,([^,]*)$/,''),
				coords:coods
			}
		};
		this.getGeometryData=function(value){
			return {
				valStr:JSON.stringify(value),
				html:'Click for Details',
				obj:value
			}
			// if(value.type=="MultiPoint"){
				// return main.utilities.getMultiPointData(value);
			// }else if(value.type=="Point"){
				// return main.utilities.getPointData(value);
			// }
		};
		this.getPointData=function(value){
			return {
				valStr:value.coordinates[0]+' '+value.coordinates[1],
				html:'['+value.coordinates[1]+','+value.coordinates[0]+']',
				coords:[ol.proj.transform([Number(value.coordinates[0]),Number(value.coordinates[1])], 'EPSG:4326', 'EPSG:3857')]
			}
		};
		this.convertMapPointInput=function(value){
			var pointsSplit=value.split(','),str='',pointSplit;
			if(pointsSplit[0]){
				for(var i=0;i<pointsSplit.length;i++){
					pointSplit=pointsSplit[i].split(' ');
					str+='['+pointSplit[1]+','+pointSplit[0]+'],';
				}
			}
			return str.replace(/,([^,]*)$/,'');
		};
		this.eleToBool=function(val,blankIsFalse){
			if(val=='false' || val=='f' ||(blankIsFalse && !val && val!=0)){val=false;
			}else if(val=='true' || val=='t'){val=true;}
			return val;
		};
		this.stringDivider=function(str,width,spaceReplacer){
			if(str.length>width){
				var p=width;
				for(;p>0 && (str[p]!=' ' && str[p] != '-');p--){}
				if(p>0){
					var left;
					if(str.substring(p,p+1)=='-'){
						left=str.substring(0,p+1);
					}else{
						left=str.substring(0,p);
					}
					var right=str.substring(p+1);
					return left+spaceReplacer+stringDivider(right,width,spaceReplacer);
				}	
			}
			return str;
		};
		this.cloneArray=function(array){
			var newArray=[];
			for(var i=0;i<array.length;i++){
				newArray.push(array[i]);
			}
			return newArray;
		};
		this.cloneObj=function(obj){
			if(obj == null || typeof(obj)!='object'){return obj;}
			var temp=obj.constructor();
			for(var key in obj){
				if(obj.hasOwnProperty(key)){
					temp[key]=self.cloneObj(obj[key]);
				}
			}
			return temp;
		};
		this.getFieldsObj=function(obj){
			var t={};
			for(var key in obj){
				t[key]={};
				for(var key2 in obj[key]){
					t[key][key2]=obj[key][key2];
				}
			}
			return t;
		};
		this.setCurrentVals=function(form,params){
			var val,input,param,input_type,data_type;
			for(var key in params){
				param=params[key];
				input_type=param.input_type;
				data_type=param.data_type;
				input=form.find('.formInput.'+key);
				val=input.val();
				if(val && val.trim()){
					if(input_type=='checkbox'){
						val='';
						input.closest('.checkboxesWrap').find('.checkboxInput').each(function(){
							if($(this).prop('checked')){val+=$(this).val()+',';}
						});
						val=val.replace(/,([^,]*)$/,'$1');
					}else if(input_type=='radio'){
						val='';
						if(input.closest('.radioWrap').find('.formInput:checked').length>0){val=input.closest('.radioWrap').find('.formInput:checked').val()}
					}else if(input_type=='date'){
						// if(param.sub_data_type=='time'){
							// var timePkg;
							// if(timePkg=main.utilities.msToTime(val)){
								// val=null;
							// }
						// }else{
							if(val){val=main.utilities.getTimeStamp(val)
							}else{val=null;}
						// }
					}
					if(main.misc.numberDTs.indexOf(param.input_type)>-1){val=Number(val);}
					if(param.to_append){val+=param.to_append;}
					param.current_val=val;
				}else{
					param.current_val=param.default_val;
				}
			}
		};
		this.createCheckRow=function(value,name,label,type,classes){
			return $('<div>').addClass('checkRow cf')
				.append($('<div>').addClass('checkInputWrap fLeft')
					.append($('<input type="'+type+'" name="'+name+'" value="'+value+'">').addClass('checkInput '+classes))
				)
				.append($('<div>').addClass('checkLabel fLeft').text(label))
		};
		this.getSpectrumColors=function(){
			var colors=[],subArray=[],perRow=3;
			for(var i=0;i<main.colors.colors.length;i++){
				subArray.push('rgb('+main.colors.colors[i][0]+','+main.colors.colors[i][1]+','+main.colors.colors[i][2]+')');
				if((i+1) % perRow==0 || (i+1)==main.colors.colors.length){
					colors.push(subArray);
					subArray=[];
				}
			}
			colors.push(['rgba(0,0,0,0)']);
			return colors;
		};
		this.initSpectrumPickers=function(params,callback,showAlpha,showPalette,showButtons){
			var param,formattedColor,colors;
			showPalette=showPalette || false;
			for(var key in params){
				if(params[key].spectrumID){
					colors=null;
					if(showPalette){
						colors=self.getSpectrumColors();
					}
					param=params[key];
					$('#'+param.spectrumID).val(param.default_val).spectrum({
						color:param.default_val,
						showInput:true,
						hideAfterPaletteSelect:true,
						preferredFormat:'rgb',
						clickoutFiresChange:true,
						showAlpha:showAlpha,
						showPalette:showPalette,
						showButtons:showButtons,
						palette:colors,
						move:function(color){
							$(this).val(color);
							callback();
						}
					});
				}
			}
		};
		this.removeForm=function(key){
			if(main.formsMod.forms0[key]){main.formsMod.forms0[key].html.remove();}
			if(main.formsMod.forms0[key].shelf){main.formsMod.forms0[key].shelf.remove();}
			if(main.formsMod.forms0[key].shortcut){main.formsMod.forms0[key].shortcut.html.remove();}
			delete main.formsMod.forms0[key];
		};
		this.getMarkerStyle=function(form){
			return new ol.style.Style({
				image:new ol.style.Icon(({
					anchor:[0.5,1],
					anchorXUnits:'fraction',
					anchorYUnits:'fraction',
					opacity:0.75,
					zIndex:99999999999,
					src:main.mainPath+'images/marker_red.png'
				}))
			})
		};
		this.toolFormMake=function(keys,params,formID,table){
			var key,row,typeSelect,unitSelect;
			var advancedOptions=$('<table>');
			for(var i=0;i<keys.length;i++){
				key=keys[i];
				param=params[key];
				param.formID=formID;
				if(param.name=='type' || param.name=='unit'){var specialField=true;}else{var specialField=false;}
				param.input=new Input({field:param,specialField:specialField,omitAlias:true,checked:true});
				if(param.name=='type'){typeSelect=param.specialInput;}
				if(param.name=='unit'){unitSelect=param.specialInput;}
				row=$('<tr class="tbFormParamRow_'+param.name+'">')
					.append($('<td>').append($('<div>').addClass('tdLiner').data('name',key).text(param.alias)))
					.append($('<td>').append($('<div>').addClass('tdLiner').append(param.input.container)))
				if(!param.advanced){
					table.append(row);
				}else{advancedOptions.append(row);}
			}
			if(advancedOptions.children().length>0){
				var advancedViewMore=new ViewMore({
					openHTML:'Advanced',
					content:advancedOptions
				});
				table.append($('<tr>')
					.append($('<td colspan="2">')
						.append($('<div class="tdLiner">').append(advancedViewMore.html))
					)
				);
			}
			return {
				advancedViewMore:advancedViewMore,
				advancedOptions:advancedOptions,
				table:table,
				unitSelect:unitSelect,
				typeSelect:typeSelect
			}
		};
		this.openForm=function(shelf,accordian,onlyOpen,openDT){
			if(!main.layout.accordians[accordian] || !main.layout.accordians[accordian].shelves[shelf]){return;}
			var shelfHTML=main.layout.accordians[accordian].shelves[shelf].shelf;
			var sidePanelName=shelfHTML.closest('.sidePanel').data('button');
			main.layout.closeAllSidePanels(null,[sidePanelName]);
			var sidePanelHTML=main.layout.sidePanels[sidePanelName];
			if(!onlyOpen){sidePanelHTML.togglePanel();}
			else{sidePanelHTML.open();}
			sidePanelHTML.container.scrollTop(shelfHTML.offset().top-sidePanelHTML.container.offset().top);
			if(main.layout.accordians[accordian].shelves[shelf].dataContentWrap){
				main.layout.accordians[accordian].close(shelf,main.layout.accordians[accordian].shelves[shelf].dataContentWrap);
				if(openDT){
					main.layout.accordians[accordian].close(shelf,main.layout.accordians[accordian].shelves[shelf].shelfContentWrap);
					main.layout.accordians[accordian].toggleDataShelf(shelf);
				}
			}
			var tgt=$('.shelfContentWrap.'+shelf);
			if(openDT){tgt=$('.dataContentWrap.'+shelf);}
			main.layout.accordians[accordian].openAllParents(tgt);
		};
		this.getNextColor=function(){
			var color=main.colors.colors[main.colors.colorIdx];
			main.colors.colorIdx++;
			return color;
		};
		this.getEditPkg=function(fieldName,editInput,obItem){
			return $('<div>').addClass('dataTableEditPkg')
				.append($('<div data-field="'+fieldName+'">').addClass('dataTableEdit editsWrap '+fieldName)
					.append(editInput)
				)
				.append(obItem.fields[fieldName].dataTableEditLink)
				.append(obItem.fields[fieldName].dataTableDoneEditLink);
		};
		this.tableToUneditable=function(table){
			table.find('.dataTableEditPkg').addClass('none');
		};
		this.getFileStr=function(fileObjs){
			if(typeof fileObjs=='object'){
				var filesStr='';
				for(var i=0;i<fileObjs.length;i++){
					filesStr+=fileObjs[i].fileName+', ';
				}
				return filesStr.replace(/, ([^, ]*)$/,'$1');
			}
			return '';
		};
		this.getFileValue=function(input){
			var fileObjs=[];
			var filesStr='';
			var files=input[0].files,servFileName,ext,fileName,fileObj;
			for(var i=0;i<files.length;i++){
				fileName=files[i].name;
				ext=fileName.split('.').pop();
				servFileName=main.utilities.generateUniqueID()+ext;
				fileObj={
					fileName:fileName,
					ext:ext,
					dateUploaded:new Date().getTime(),
					servFileName:servFileName,
				}
				fileObjs.push(fileObj);
				filesStr+=fileName+', ';
			}
			return {
				fileObjs:fileObjs,
				filesStr:filesStr.replace(/, ([^, ]*)$/,'$1'),
			};
		};
		this.addCommas=function(value){
			var str=value.toString().split('.');
			str[0]=str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
			return str.join('.');
		};
		this.getCheckboxValue=function(chx,field){
			var value='',html='',val;
			chx.each(function(){
				if($(this).prop('checked')){
					val=$(this).val();
					value+=val+',';
					if(field.options && field.options[val]){html+=field.options[val].alias+', '}
					else{html+=val+', '}
				}
			});
			return {
				value:value.replace(/,([^,]*)$/,'$1'),
				html:html.replace(/, ([^, ]*)$/,'$1')
			}
		};
		this.setTimePkg=function(timePkg,input){
			if(timePkg){
				input.closest('.timeInputWrap').find('.timeHourInput').val(timePkg.hours);
				input.closest('.timeInputWrap').find('.timeMinInput').val(main.utilities.addLeadingZero(timePkg.mins));
				input.closest('.timeInputWrap').find('.timeSecInput').val(main.utilities.addLeadingZero(timePkg.secs));
				input.closest('.timeInputWrap').find('.timeAMPMSelect').val(timePkg.ampm);
			}else{
				input.closest('.timeInputWrap').find('.timeHourInput').val('');
				input.closest('.timeInputWrap').find('.timeMinInput').val('');
				input.closest('.timeInputWrap').find('.timeSecInput').val('');
				input.closest('.timeInputWrap').find('.timeAMPMSelect').val('');
			}
		};
		this.setCheckboxes=function(chx,values,field,pid){
			var html='',val;
			values=values || [];
			chx.each(function(){
				val=$(this).attr('value');
				if(values.indexOf(val)>-1){
					$(this).prop('checked',true);
					if(field.options && field.options[val]){html+=field.options[val].alias+', '}
					else{html+=val+', '}
					html=html.replace(/, ([^, ]*)$/,'$1')
				}else{$(this).prop('checked',false);}
			});
			return html;
		};
		this.setUnit=function(field,input,value,pid,lUCB,inputObject,fields,layerName,fromDT,fromEF){
			var html='';
			if(field.options && (!field.lookup_table || !field.with_lu_popup)){
				if(field.input_type=='checkbox'){
					if(value && typeof value=='string'){
						html=main.utilities.setCheckboxes(input.closest('.checkboxesWrap').find('.checkboxInput'),value.split(','),field);
					}else if(field.data_type=='text_array'){
                        html=main.utilities.setCheckboxes(input.closest('.checkboxesWrap').find('.checkboxInput'),value,field);
					}else if(field.data_type=='boolean'){
						if(value){
							html=field.options['true'].alias;
							input.prop('checked',true);
						}else{
							if(field.options['true'].offAlias){html=field.options['true'].offAlias;}
							input.prop('checked',false);
						}
					}
				}else if(field.input_type=='radio'){
					html='';
					if(value!==null){
						input.filter('*[value="'+value+'"]').prop('checked',true);
						if((value || value==0) && field.options[value]){html=field.options[value].alias;}
					}
				}else{
					if((value || value==0) && field.options[value]){
						html=field.options[value].alias;
						input.val(value);
					}else{
						html='';
						input.val('');}
				}
			}else{
				if(field.input_type=='date'){
					if(field.sub_data_type=='season'){
						if(value && !isNaN(value)){
							var seasonStuff=main.utilities.getSeasonFromTimeStamp(Number(value));
							html=main.utilities.formatSeason(seasonStuff);
							input.closest('.seasonDateContainer').find('.seasonSelect').val(seasonStuff.season);
							input.closest('.seasonDateContainer').find('.yearSelect').val(seasonStuff.year);
						}else{
							html='';
							input.closest('.seasonDateContainer').find('.seasonSelect').val('');
							input.closest('.seasonDateContainer').find('.yearSelect').val('');
						}
					}else if(field.sub_data_type=='time'){
						var timePkg;
						if(timePkg=main.utilities.msToTime(value)){
							html=timePkg.html
						}else{html='';}
						self.setTimePkg(timePkg,input);
						input.val(html);
					}else{
						html=main.utilities.formatDate(value);
						input.val(html);
					}
				}else if(field.input_type=='file'){
					if(main.tables[layerName].atchs.items[pid] && main.tables[layerName].atchs.items[pid].fields[field.name].dtCurrentPhotos && value){
						for(var i=0;i<value.length;i++){
							if(!main.tables[layerName].atchs.items[pid].fields[field.name].dtCurrentPhotos.elItems[value[i].servFileName]){
								if(fromDT){
									main.tables[layerName].atchs.items[pid].fields[field.name].dtCurrentPhotos.addItem(value[i].servFileName,value[i].fileName,{field:field,pid:pid});
								}else if(fromEF){
									main.tables[layerName].atchs.items[pid].fields[field.name].etCurrentPhotos.addItem(value[i].servFileName,value[i].fileName,{field:field,pid:pid});
								}
								// html=false;
							}
						}
					}
					// html=main.utilities.getFileStr(value);
				}else{
					//check for look up table
					if(field.lookup_table && field.input_type == 'select' && /* ( */inputObject.lookUpTable/*  || !field.with_lu_popup) */){
						// if(field.with_lu_popup){
							inputObject.lookUpTable.itemSelected(value,true,fields);
							var a=inputObject.lookUpTable.getLookUpTableHTML(lUCB,{pid:pid,fieldName:field.name,value:value});
							// var lookUpVal=main.utilities.getLookUpTableHTML(field.lookup_table,value,field.name,layerName,field);
							// if(lookUpVal!==null){alias=lookUpVal;}
							if(a!==false){html=a;}
						// }else{
							// html=self.getLookUpTableHTML(field.lookup_table,value,field.name,layerName,field);
						// }
					}else{
						input.val(value);
						html=value;
					}
				}
			}
			return {
				html:html,
				val:value
			};
		};
		this.postDelete=function(pids,table,callback,response){
			if(main.dataTables[table]){
				var layerSource,features,feature;
				if(main.tables[table].map_layer && main.tables[table].map_layer.layer){
					layerSource=main.tables[table].map_layer.layer.olLayer.getSource();
					features=layerSource.getFeatures();
				}
				for(var i=0;i<pids.length;i++){
					if(main.dataTables[table].tbody){
						main.dataTables[table].tbody.add(main.dataTables[table].stickyColumnsTbody).find('.dtRow_'+pids[i]).remove();
					}
					if(layerSource){
						for(var t=0;t<features.length;t++){
							if(features[t].get('pid')==pids[i]){
								layerSource.removeFeature(features[t]);
							}
						}
					}
					if(main.tables[table].map_layer && main.tables[table].map_layer.features && main.tables[table].map_layer.features[pids[i]]){
						var feature=main.tables[table].map_layer.features[pids[i]];
						if(feature.popup){
							feature.popup.remove();
						}
						if(feature.editForm){
							feature.editForm.panel.remove();
							delete feature.editForm;
						}
					}
				}
			}
			main.dataTables[table].updateHeaderSizes();
			if(callback){callback(pids);}
			if(response.updateDTs){
				for(var i=0;i<response.updateDTs.length;i++){
					if(main.dataTables[response.updateDTs[i]].dataContentWrap.is(':visible')){
						main.dataTables[response.updateDTs[i]].sideReset();
					}else{
						main.dataTables[response.updateDTs[i]].needsRefresh=true;
					}
				}
			}
		};
		this.deleteAllData=function(table,fieldName,value){
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						fieldName:fieldName,
						value:value,
						table:table
					},
					action:'deleteAllData'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							self.postDelete(response.results,table,null,response);
						}else{
							if(response.ERR_CODE==1){main.utilities.loggedOutNotice();}
							else{alert('Error');}
							console.log(response);
						}
					}
				}
			});
		};
		this.deleteData=function(pids,table,signatures,callback){
			var incrementFieldPkg=null;
			for(var key in main.tables){
				if(main.tables[key].properties.transfer_to && main.tables[key].properties.transfer_to[table] && main.tables[key].properties.transfer_to[table].availableField && main.tables[key].properties.transfer_to[table].sourcePIDField){
					if(!incrementFieldPkg){incrementFieldPkg=[];}
					incrementFieldPkg.push({
						availableField:main.tables[key].properties.transfer_to[table].availableField,
						sourcePIDField:main.tables[key].properties.transfer_to[table].sourcePIDField,
						sourceTable:key,
						thisTable:table
					});
				}
			}
			//check for transfer to changes
			var transferToPkg=null;
			for(var key in main.tables){
				if(main.tables[key].properties.transfer_to && main.tables[key].properties.transfer_to[table] && main.tables[key].properties.transfer_to[table].sourcePIDFieldReturnToNullOnDelete && main.tables[key].properties.transfer_to[table].sourcePIDField){
					if(!transferToPkg){transferToPkg={};}
					transferToPkg[key]={
						sourcePIDField:main.tables[key].properties.transfer_to[table].sourcePIDField,
						table:key
					}
				}
			}
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						pids:pids,
						table:table,
						transferToPkg:transferToPkg,
						incrementFieldPkg:incrementFieldPkg,
						signatures:signatures
					},
					action:'deleteData'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						console.log(response);
						if(response.status=="OK"){
							self.postDelete(pids,table,callback,response);
							if(main.filter_legend && main.filter_legend.active){main.filter_legend.refresh();}
						}else{
							if(response.ERR_CODE==1){main.utilities.loggedOutNotice();}
							else{alert('Error');}
							console.log(response);
						}
					}
				}
			});
		};
		this.mapDoubleClickWhenDrawing=function(){
			if(main.invents.blockDoubleClick){
				main.invents.blockDoubleClick=false;
				return false;
			}
		};
		this.setPhotosButtons=function(table,pid,count){
			if(main.invents.inventories[table].inventory.map_layer.features[pid].popup){
				if(count>0){
					if(main.invents.inventories[table].inventory.map_layer.features[pid].popup.viewPhotosButton){
						main.invents.inventories[table].inventory.map_layer.features[pid].popup.viewPhotosButton.html.removeClass('none')
						main.invents.inventories[table].inventory.map_layer.features[pid].popup.showImages.html.removeClass('none')
					}
					if(main.invents.inventories[table].inventory.map_layer.features[pid].popup.editForm && main.invents.inventories[table].inventory.map_layer.features[pid].popup.editForm.viewPhotosButton){
						main.invents.inventories[table].inventory.map_layer.features[pid].popup.editForm.viewPhotosButton.html.removeClass('none')
						main.invents.inventories[table].inventory.map_layer.features[pid].popup.editForm.showImages.html.removeClass('none')
					}
				}else{
					if(main.invents.inventories[table].inventory.map_layer.features[pid].popup.viewPhotosButton){
						main.invents.inventories[table].inventory.map_layer.features[pid].popup.viewPhotosButton.html.addClass('none');
						main.invents.inventories[table].inventory.map_layer.features[pid].popup.showImages.html.addClass('none');
					}
					if(main.invents.inventories[table].inventory.map_layer.features[pid].popup.editForm && main.invents.inventories[table].inventory.map_layer.features[pid].popup.editForm.viewPhotosButton){
						main.invents.inventories[table].inventory.map_layer.features[pid].popup.editForm.viewPhotosButton.html.addClass('none')
						main.invents.inventories[table].inventory.map_layer.features[pid].popup.editForm.showImages.html.addClass('none')
					}
				}
			}
		};
		this.removePhoto=function(name,table,fieldName,pid,cb){
			var photos=main.tables[table].atchs.items[pid].fields[fieldName].photoInfo;
			// var photos=main.tables[table].map_layer.features[pid].photoInfo;
			var array=[];
			var toDelete=[];
			for(var k=0;k<photos.length;k++){
				if(photos[k].servFileName!=name){array.push(photos[k]);}
				else{toDelete.push(photos[k].servFileName);}
			}
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						photos:array,
						toDelete:toDelete,
						fieldName:fieldName,
						pid:pid,
						table:table
					},
					action:'removePhoto'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status="OK"){
							main.tables[table].map_layer.features[pid].properties[fieldName]=array;
							main.tables[table].atchs.items[pid].fields[fieldName].photoInfo=array;
							// main.tables[table].map_layer.features[pid].photoInfo=array;
							if(main.tables[table].map_layer.features[pid].dataTable){main.tables[table].map_layer.features[pid].dataTable.updateRow(response.results[0]);}
							self.setPhotosButtons(table,pid,array.length);
						}else{
							if(response.ERR_CODE==1){main.utilities.loggedOutNotice();}
							else{alert('Error');}
							console.log(response);
						}
						if(cb){cb(response);}
					}
				}
			});
		};
		this.getCenterOfFeature=function(feature){
			// var coords=feature.getGeometry().getCoordinates();
			// if(coords[0].constructor===Array){
				// coords=coords[0];
				// closestFeatureCood=main.utilities.getCenterOfFeature(closestFeature);
			// }
			// return coords;
		};
		this.setTopLayer=function(layer){
			if(layer){
				var mapLayers=main.map.map.getLayers().getArray();
				var thisLayerIdx=null
				for(var i=0;i<mapLayers.length;i++){
					if(layer==mapLayers[i]){
						thisLayerIdx=i;
					}
				}
				if(thisLayerIdx!=null){
					var mapLayersLength=mapLayers.length
					var oldLayer=main.map.map.getLayers().removeAt(thisLayerIdx);
					main.map.map.getLayers().insertAt(mapLayers.length-2,oldLayer);
				}
			}
			// mapLayers.insertAt(0,newBaseTiles);
		};
		this.photoELClicked=function(args){
			var layerName=args.container.closest('.filePkgWrap').data('layerName');
			var pid=args.container.closest('.filePkgWrap').data('pid');
			
			var info=main.tables[layerName].atchs.items[pid].fields[args.params.field.name].photoInfo;
			// var info=main.tables[layerName].map_layer.features[pid].photoInfo;
			if(info){
				if(args.params.field.sub_data_type=='photo'){
					// info=JSON.parse(info);
					// if(typeof info=='string'){info=JSON.parse(info);}
					main.imageView.loadImages(info,args.name);
					main.imageView.panel.open();
				}else if(args.params.field.sub_data_type=='atch'){
					window.open(location.href+'atch/'+args.name);
				}
			}
		};
		this.getPhotosViewButton=function(pid,fieldName,layerName){
			return new Button('std','viewPhotos','Photos',[['pid',pid],['fieldname',fieldName],['layerName',layerName]]);
		};
		this.getPhotosPkg=function(pid,fieldName,layerName,value){
			if(!main.tables[layerName].atchs.items[pid]){main.tables[layerName].atchs.items[pid]={fields:{}};}
			if(!main.tables[layerName].atchs.items[pid].fields[fieldName]){main.tables[layerName].atchs.items[pid].fields[fieldName]={};}
			if(!main.tables[layerName].atchs.items[pid].fields[fieldName].photoInfo && value){
				var photoVal;
				if(value){
					if(typeof value=='string'){photoVal=JSON.parse(value);}
					else{photoVal=value;}
				}else{photoVal=null;}
				main.tables[layerName].atchs.items[pid].fields[fieldName].photoInfo=photoVal;
			}
			var viewPhotos=self.getPhotosViewButton(pid,fieldName,layerName);
			var photoHTMLPkg=$('<div class="photoHTMLPkg">')
				.append(viewPhotos.html);
			viewPhotos.html.on('click',self.viewPhotosClicked);
			var photos=main.tables[layerName].atchs.items[pid].fields[fieldName].photoInfo;
			if(!photos || photos.length==0){
				viewPhotos.html.addClass('none');
				photoHTMLPkg.addClass('none');
			}
			return {
				viewPhotosButton:viewPhotos,
				html:photoHTMLPkg
			}
		};
		this.getLookUpValueFromAlias=function(alias,field,tableName,layerName){
			var val=null;
			var table=main.data.lookUpTables[tableName];
			var lookup_table_map=main.tables[layerName].properties.lookup_table_map;
			if(table && lookup_table_map){
				var items=table.items;
				var column=lookup_table_map[tableName];
				for(var key in items){
					if(items[key][column]==alias){
						val=key;
						break;
					}
				}
				if(!val){
					for(var key in field.options){
						if(field.options[key]==alias){
							val=key;
							break;
						}
						
					}
				}
			}
			return val;
		};
		this.uploadedToVal=function(data,fields,tableName){
			var newData={},value,newValue,field;
			for(var key in data){
				value=data[key];
				newValue=value;
				field=fields[key];
				if(field.active){
					if(field.options){
						if(field.input_type=='checkbox'){
							newValue=null;
							if(value && field.data_type!='boolean'){
								newValue=value.replace(/, /g,',');
							}else if(field.data_type=='boolean'){
								if(field.options['true'].alias==value){newValue=true;
								}else if(field.options['true'].offAlias==value){newValue=false;}
							}
						}else{
							if(value){
								var match=false;
								for(var key2 in field.options){
									if(field.options[key2].alias==value){newValue=key2;match=true;}
								}
								if(!match){newValue=null;}
							}
						}
					}
					if(field.input_type=="date" && (value || value==0)){
						if(field.sub_data_type=='time'){
							newValue=main.utilities.timeToMS(value);
						}else{
							var delimiter;
							if(value.indexOf('/')!==-1){delimiter='/';
							}else if(value.indexOf('-')!==-1){delimiter='-';}
							if(delimiter && typeof value=='string' && value.split(delimiter).length==3){
								newValue=self.getTimeStamp(value);
							}else{newValue=null;}
						}
					}
					if(field.lookup_table && (value || value==0)){
						var a=self.getLookUpValueFromAlias(value,field,field.lookup_table,field.lookup_table,tableName);
						if(a!==null){newValue=a;}
					}
					if(msg=main.utilities.validateData(value,field)){
						// alert(msg);
						newData[key]=null;
					}
					newData[key]=newValue;
				}
			}
			return newData;
		};
		this.hexToRGB=function(val){
			var result=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
			return result ? {
				r:parseInt(result[1],16),
				g:parseInt(result[2],16),
				b:parseInt(result[3],16)
			} : null;
		};
		this.componentToHex=function(c){
			if(!c){return null;}
			var hex=c.toString(16);
			return hex.length==1?"0"+hex:hex;
		};
		this.rGBToHex=function(r,g,b){
			return "#"+self.componentToHex(r)+self.componentToHex(g)+self.componentToHex(b);
		};
		this.logError=function(msg){
			return;
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						msg:msg,
						date:new Date().getTime()
					},
					action:'logError'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
						}else{
							// alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.getPixel=function(e){
			var pixel;
			if(!e.originalEvent.touches){
				var x=e.originalEvent.clientX-$(e.originalEvent.target).offset().left;
				var y=e.originalEvent.clientY-$(e.originalEvent.target).offset().top;
				var n=1;
			}else{
				// pixel=main.map.map.getEventPixel(e.originalEvent);
				var x=e.originalEvent.touches[0].clientX-$(e.originalEvent.target).offset().left;
				var y=e.originalEvent.touches[0].clientY-$(e.originalEvent.target).offset().top;
				main.he=e;
				var n=2;
			}
			pixel=[x,y];
			if(!pixel[0]){
				return false;
			}
			return pixel;
		};
		this.getColorArray=function(val){
			var tempVal,rSplit;
			if(val){
				if(val.indexOf('rgba')>-1){
					tempVal=val.replace('rgba(','');
					tempVal=tempVal.replace(')','');
					rSplit=tempVal.split(",");
				}else if(val.indexOf('rgb')>-1 || val.indexOf('#')>-1){
					if(val.indexOf('#')>-1){val=self.hexToRGB(val);}
					tempVal=val.replace('rgb(','');
					tempVal=tempVal.replace(')','');
					rSplit=tempVal.split(",");
					rSplit[3]=1;
				}
			}
			return rSplit ? [Number(rSplit[0]),Number(rSplit[1]),Number(rSplit[2]),Number(rSplit[3])] : null;
		};
		this.getCountByRestrictor=function(count,layerName,field,pids,cb,cbParams){
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						count:count,
						layerName:layerName,
						field:field,
						pids:pids.join(',')
					},
					action:'getCountByRestrictor'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							if(cb){cb(response,cbParams);}
						}else{
							console.log(response);
						}
					}
				}
			});
		};
		this.addLookUpValsToChangedFields=function(value,field,fields,lookup_table_map){
			var changeFields=[];
			if(field.lookup_table && lookup_table_map && lookup_table_map[field.lookup_table]){
				var lookupmap=lookup_table_map[field.lookup_table];
				for(var key in lookupmap){
					if(key!=field.name){
						changeFields.push({field:fields[key],val:value});
					}
				}
			}
			return changeFields;
		};
		this.getHTMLFromVal=function(value,field,lookupTableMap,cultivarField,to_fixed,lookup_table,primary_field){
			if(main.misc.numberDTs.indexOf(field.data_type)>-1 && !field.options && (field.to_fixed || field.to_fixed==0)){
				value=Number(value).toFixed(field.to_fixed);
			}
			if(main.misc.numberDTs.indexOf(field.data_type)>-1 && !field.options &&(to_fixed || to_fixed==0)){
				value=Number(value).toFixed(to_fixed);
			}
			var html=value;
			var fieldName=field.name;
			if(lookupTableMap && (field.lookup_table || field.stored_inventory)){
				if(field.lookup_table){
					html=self.getSimpleLookUpAlias(value,field,lookupTableMap,cultivarField);
				}else if(field.stored_inventory){
					html=self.getSimpleLookUpAliasNoField(value,lookup_table,primary_field);
				}
			}else if(field.options){
				if(field.input_type=='checkbox'){
					if(value && typeof value=='string'){
						var tempVals=value.split(','),string='';
						for(var i=0;i<tempVals.length;i++){
							string+=field.options[tempVals[i]].alias+', ';
						}
						string=string.replace(/,([^,]*)$/,'$1'),
						html=string;
					}else if(field.data_type=='boolean'){
						if(value){
							html=field.options['true'].alias;
						}else{
							if(field.options['true'].offAlias){
								html=field.options['true'].offAlias;
							}
						}
					}
				}else{
					if(value || value==0){
						if(field.options[value]){
							html=field.options[value].alias;
						}else{
							html='';
						}
					}
				}
			}
			/* if(field.input_type=="file"){
				var photoStuff=main.utilities.getPhotosPkg(value,pid,fieldName);
				obItem.fields[fieldName].viewPhotosButton=photoStuff.viewPhotosButton;
				obItem.fields[fieldName].html=photoStuff.html;
				obItem.fields[fieldName].fileInfo=value;
				obItem.fields[fieldName].viewPhotosButton.html.on('click',function(){
					var pid=$(this).data('pid');
					var fieldName=$(this).data('fieldName');
					var info=$(this).data('info');
					if(info && info!='""' && info!="''"){
						info=JSON.parse(info);
						if(typeof info=='string'){info=JSON.parse(info);}
						main.imageView.loadImages(info);
						main.imageView.panel.open();
					}
				});
			} */
			if(field.input_type=="date" && value && !isNaN(Number(value))){
				if(field.sub_data_type=='season'){
					html=main.utilities.formatSeason(main.utilities.getSeasonFromTimeStamp(Number(value)));
				}else if(field.sub_data_type=='time'){
					var timePkg;
					if(timePkg=main.utilities.msToTime(value)){
						html=timePkg.html
					}else{
						html='';
					}
				}else{
					html=self.formatDate(Number(value));
				}
			}
			if((field.input_type=="map_point" || field.input_type=="map_polygon") && value && typeof value=='object'){
				// var geoData=self.getGeometryData(value);
				// html=geoData.html;
				html='Edit to View';
			}
			if(html && typeof html =='number' && field.to_fixed){html=html.toFixed(field.to_fixed);}
			if(html && typeof html !='string'){html=html.toString();}
			if(html && field.append_to){html=html+field.append_to;}
			if(html && field.prepend_to){html=html+field.prepend_to;}
			/* if(field.input_type=='signature'){
				if(obItem.fields[fieldName].html && obItem.fields[fieldName].html!='true'){
					obItem.fields[fieldName].html=self.createSigImage(main.mainPath+'images/signatures/'+obItem.fields[fieldName].html);
				}
			} */
			return html;
		};
		this.validateData=function(value,field){
			if(field[main.account.loggedInUser.user_type+'_required'] && (typeof field[main.account.loggedInUser.user_type+'_required']!='string' || field[main.account.loggedInUser.user_type+'_required'].trim())){
				var requiredToBeA=field[main.account.loggedInUser.user_type+'_required'],msg=null;
				if(requiredToBeA=='email'){
					if(!value || value.indexOf('@')==-1){
						msg=field.required_msg || 'This field: "'+field.alias+'" is an email field and is required.';
					}
				}else if(requiredToBeA=='number'){
					if(isNaN(value)){
						msg=field.required_msg || 'This field: "'+field.alias+'" must be a number';
					}
				}else{
					if(!value && value!==0){
						msg=field.required_msg || 'This field: "'+field.alias+'" is required.';
					}
				}
			}
			return msg;
		};
		this.viewPhotosClicked=function(){
			var pid=$(this).data('pid');
			var layerName=$(this).data('layerName');
			var fieldname=$(this).data('fieldname');
			var photos=main.tables[layerName].atchs.items[pid].fields[fieldname].photoInfo;
			if(photos){
				main.imageView.loadImages(photos);
				main.imageView.panel.open();
			}
		};
		this.sortTabFields=function(keys,fields){
			keys.sort(function(a,b){
				if(!fields[a].place_in_order && fields[a].place_in_order!==0){return b-a;
				}else{return fields[a].place_in_order-fields[b].place_in_order;}
			});
			return keys;
		};
		this.getDependWhere=function(layerName){
			var where='';
			if(main.invents && main.invents.currentFilterLayer && main.inventories[main.invents.currentFilterLayer].is_depended_upon_by==layerName && main.invents.currentFilterPID && main.invents.inventories[layerName].properties.internal_field_map && main.invents.inventories[layerName].properties.internal_field_map.dependent_upon_field){
				if(!main.account.loggedInUser.dont_filter){
					where=main.invents.inventories[layerName].properties.internal_field_map.dependent_upon_field+"='"+main.invents.currentFilterPID+"'";
				}
			}
			return where;
		};
		this.getAdvFilterWhere=function(layerName){
			var pids=null;
			var where='';
			if(main.advFilter && main.advFilter.appliesTo['map'].apply && main.advFilter.currentLayer && main.advFilter.currentLayer.properties.layer_name==layerName){
				if(main.advFilter.where){
					where+='('+main.advFilter.where+')';
				}
				if(main.advFilter.geoFilter){
					pids=main.advFilter.geoFilter;
				}
			}
			return {
				pids:pids,
				where:where
			}
		};
		this.getFilterLegendWhere=function(layerName){
			var where='';
			if(main.filter_legend && main.filter_legend.active && !main.filter_legend.hideOnly && main.filter_legend.filterWhere && main.filter_legend.layerName==layerName){
				if(where){where+=' AND ';}
				where+='('+main.filter_legend.filterWhere+')';
			}
			return where;
		};
		this.getWhere=function(layerName){
			var where='';
			where+=self.getDependWhere(layerName);
			var advWheres=self.getAdvFilterWhere(layerName);
			if(where && advWheres.where){where+=' AND ';}
			where+=advWheres.where;
			pids=advWheres.pids;
			var filterLegendWhere=self.getFilterLegendWhere(layerName);
			if(where && filterLegendWhere){where+=' AND ';}
			where+=filterLegendWhere;
			return {
				where:where,
				pids:pids
			}
		};
		this.getEcoFields=function(fieldMap,dbhVal,main_species_fieldVal,land_useVal){
			var ecoVals={
				dbh:{
					fieldName:fieldMap.dbh,
					val:dbhVal
				},
				main_species_field:{
					fieldName:fieldMap.main_species_field,
					val:main_species_fieldVal
				},
				land_use:{
					fieldName:'landuse',
					val:land_useVal
				}
			}
			return ecoVals;
		};
		this.getValFromInput=function(field,reference){
			var value=null;
			if(field.input_type=='checkbox'){
				var chkStuff=main.utilities.getCheckboxValue(reference.closest('.checkboxesWrap').find('.checkboxInput'),field);
				value=chkStuff.value;
				if(field.data_type=='boolean'){
					if(value){value=true;
					}else{value=false;}
				}
			}else if(field.input_type=='radio'){
				value=reference.closest('.radiosWrap').find('input[type="radio"]:checked').val();
			}else if(field.input_type=='date'){
				value=main.utilities.getTimeStamp(primaryEVal);
			}else if(field.input_type=='file'){
			}else{
				value=reference.val();
			}
			return value;
		};
		this.getRowThings=function(obItem,field,pid,lookUpParams,sigParams,data,isLargeForm,layerName,omitEditing,is_in_data_table,is_in_edit_form,is_in_popup,sortBy){
			var fieldName=field.name,input0;
			var value=obItem.fields[fieldName].current_val;
			if(field.options){
				if(field.input_type=='checkbox'){
					if((value || value=='0') && (typeof value=='string' || value.constructor===Array)){
						var string='';
						if(typeof value=='string'){
							var tempVals=value.split(',');
						}else if(value.constructor===Array){
							var tempVals=value;
						}
						for(var i=0;i<tempVals.length;i++){
							if(field.options[tempVals[i]]){
								string+=field.options[tempVals[i]].alias+', ';
							}
						}
						string=string.replace(/,([^,]*)$/,'$1');
						obItem.fields[fieldName].html=string;
					}else if(field.data_type=='boolean'){
						if(value){
							obItem.fields[fieldName].html=field.options['true'].alias;
						}else{
							if(field.options['true'].offAlias){
								obItem.fields[fieldName].html=field.options['true'].offAlias;
							}
						}
					}
				}else{
					if(value || value=='0'){
						if(field.options[value]){
							obItem.fields[fieldName].html=field.options[value].alias;
						}else/*  if(field.default_off_alias) */{
							obItem.fields[fieldName].html='';
							// obItem.fields[fieldName].html=value;
						}
					}else{
						// obItem.fields[fieldName].html=field.default_off_alias;
						obItem.fields[fieldName].html='';
					}
				}
			}
			if(field.input_type=="date" && (value || value=='0') && !isNaN(Number(value))){
				if(field.sub_data_type=='season'){
					obItem.fields[fieldName].html=main.utilities.formatSeason(main.utilities.getSeasonFromTimeStamp(Number(value)));
				}else if(field.sub_data_type=='time'){
					var timePkg;
					if(timePkg=main.utilities.msToTime(value)){
						obItem.fields[fieldName].html=timePkg.html
					}else{
						obItem.fields[fieldName].html='';
					}
				}else{
					obItem.fields[fieldName].html=self.formatDate(Number(value));
				}
			}
			if((field.input_type=="map_point" || field.input_type=="map_polygon") && value && typeof value=='object'){
				var geoData=self.getGeometryData(value);
				// obItem.fields[fieldName].html=geoData.html;
				obItem.fields[fieldName].html='Edit to View';
				obItem.fields[fieldName].current_val=geoData.valStr;
			}
			// if(field.input_type=="map_polygon" && value && typeof value=='object'){
				// var value=self.getPolygonData(value);
				// obItem.fields[fieldName].html=value.html;
				// obItem.fields[fieldName].current_val=value.valStr;
			// }
			obItem.fields[fieldName].editPackage='';
			// if(!field.readonly && !field.disabled){
			// if(field[main.account.loggedInUser.user_type+'_editable']){
				if(field.input_type=='signature'){
					input0=new Input({
						field:field,
						setToVal:value,
						callback:sigParams.callback,
						layerName:layerName,
						is_in_data_table:is_in_data_table,
						is_in_edit_form:is_in_edit_form,
						is_in_popup:is_in_popup,
						// data_table:self
					});
					if(!omitEditing){
						obItem.fields[fieldName].input=input0;
					}
					var image=new Image();
					image.src='images/signatures/'+obItem.fields[fieldName].html;
					var id=self.generateUniqueID();
					image.dataset.id=fieldName;
					obItem.fields[fieldName].canvasSignature={
						image:image,
						ctx:obItem.fields[fieldName].input.signature.ctx
					}
					image.onload=function(){
						obItem.fields[this.dataset.id].canvasSignature.ctx.drawImage(obItem.fields[this.dataset.id].canvasSignature.image,0,0);
					};
				}else{
					// main.r=obItem;
					// if(typeof value=='object' && field.input_type!="map_polygon" && field.input_type!="map_point"){
						// value='';
					// }
					input0=new Input({
						field:field,
						setToVal:value,
						layerName:layerName,
						is_in_data_table:is_in_data_table,
						is_in_edit_form:is_in_edit_form,
						is_in_popup:is_in_popup,
						// data_table:self,
						pid:pid,
						sortBy:sortBy
					});
					if(!omitEditing){
						obItem.fields[fieldName].input=input0;
					}
				}
				if(field.input_type=="file"){
					var photoStuff=self.getPhotosPkg(pid,fieldName,layerName);
					// obItem.fields[fieldName].viewPhotosButton=photoStuff.viewPhotosButton;
					// obItem.fields[fieldName].viewPhotosButton.html.on('click',self.viewPhotosClicked);
					obItem.fields[fieldName].html=photoStuff.html;
				}
				if(!omitEditing){
					var editInput=$('<div>').addClass('dataTableInputWrap '+fieldName).data('field',fieldName)
						.append(obItem.fields[fieldName].input.container);
					obItem.fields[fieldName].dataTableEditLink=$('<div>').addClass('dataTableEditLink editLink').attr('title','Start Editing').data('field',fieldName).append('<img src="'+main.mainPath+'images/pencil.png" class="editorImg">');
					obItem.fields[fieldName].dataTableDoneEditLink=$('<div>').addClass('dataTableDoneEditLink editLink').attr('title','Done Editing').data('field',fieldName).append('<img src="'+main.mainPath+'images/no_pencil.png" class="editorImg">');
					obItem.fields[fieldName].dataTableEditLink.on('click',function(){
						self.startEditing($(this).closest('.outerEditingW'));
					});
					obItem.fields[fieldName].dataTableDoneEditLink.on('click',function(){
						self.stopEditing($(this).closest('.outerEditingW'));
					});
					var eP=self.getEditPkg(fieldName,editInput,obItem);
					if(!isLargeForm){
						obItem.fields[fieldName].editPackage=eP;
					}else{
						obItem.fields[fieldName].largeEditPackage=eP;
					}
				}
			// }
			// }
			if(field.lookup_table && field.input_type == 'select' && input0.lookUpTable){
				var a=input0.lookUpTable.getLookUpTableHTML(lookUpParams.callback,lookUpParams.params);
				if(a!==false){obItem.fields[fieldName].html=a;}
			}
			if(field.input_type=='signature'){
				if(obItem.fields[fieldName].html && obItem.fields[fieldName].html!='true'){
					obItem.fields[fieldName].html=self.createSigImage('images/signatures/'+obItem.fields[fieldName].html);
				}
			}
			return input0;
		};
		this.updateTable0=function(table,fieldsMap,link_column,reference,callback){
			var pid,newFormPkg,tempFields,newFields,fieldsList,recordPkgs;
			tempFields={};
			newFields=main.tables[table].fields;
			fieldsList=[];
			for(var key2 in fieldsMap){
				if(newFields[key2]){
					tempFields[key2]=newFields[key2];
					tempFields[key2].current_val=reference[fieldsMap[key2]].value || reference[fieldsMap[key2]].current_val;
					fieldsList.push(key2);
				}
			}
			newFormPkg=self.getFormPkg(tempFields);
			pid=reference[link_column].value || reference[link_column].current_val;
			recordPkgs={};
			recordPkgs[pid]=newFormPkg
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						fields:fieldsList,
						recordPkgs:recordPkgs,
						table:table
					},
					action:'updateDataEntry'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							var table=response.params.table;
							if(main.tables[table].map_layer.features[pid] && main.tables[table].map_layer.features[pid].dataTable){
								// main.tables[table].map_layer.features[pid].dataTable.updateDataTable(pid,fieldName,primaryE,false,true,true);
								var data=recordPkgs[pid];
								var tempData={};
								for(var key in data){tempData[key]=data[key].value;}
								tempData.pid=pid;
								main.tables[table].map_layer.features[pid].dataTable.updateRow(tempData);
								// self.dataTable.updateData([pid]);
							}
							if(main.filter_legend && main.filter_legend.active){main.filter_legend.refresh();}
							if(main.dashboard && main.dashboard.active){main.dashboard.refresh();}
							if(callback){callback();}
						}else{
							if(response.ERR_CODE==1){main.utilities.loggedOutNotice();}
							else{alert('Error');}
							console.log(response);
						}
					}
				}
			});
		};
		this.updateTable=function(table,fieldsMap,link_column,reference,callback){
			var pid,newFormPkg,tempFields,newFields,fieldsList,recordPkgs;
			tempFields={};
			newFields=main.tables[table].fields;
			fieldsList=[];
			for(var key2 in fieldsMap){
				if(newFields[key2]){
					tempFields[key2]=newFields[key2];
					if(reference[fieldsMap[key2]]){
						tempFields[key2].current_val=reference[fieldsMap[key2]].value || reference[fieldsMap[key2]].current_val;
						fieldsList.push(key2);
					}
				}
			}
			newFormPkg=self.getFormPkg(tempFields);
			pid=reference[link_column].value || reference[link_column].current_val;
			recordPkgs={};
			recordPkgs[pid]=newFormPkg
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						fields:fieldsList,
						recordPkgs:recordPkgs,
						table:table
					},
					action:'updateDataEntry'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							var table=response.params.table;
							if(main.tables[table].map_layer.features[pid] && main.tables[table].map_layer.features[pid].dataTable){
								// main.tables[table].map_layer.features[pid].dataTable.updateDataTable(pid,fieldName,primaryE,false,true,true);
								var data=recordPkgs[pid];
								var tempData={};
								for(var key in data){tempData[key]=data[key].value;}
								tempData.pid=pid;
								main.tables[table].map_layer.features[pid].dataTable.updateRow(tempData);
								// self.dataTable.updateData([pid]);
							}
							if(main.filter_legend && main.filter_legend.active){main.filter_legend.refresh();}
							if(main.dashboard && main.dashboard.active){main.dashboard.refresh();}
							if(callback){callback();}
						}else{
							if(response.ERR_CODE==1){main.utilities.loggedOutNotice();}
							else{alert('Error');}
							console.log(response);
						}
					}
				}
			});
		};
		this.startEditing=function(dataTD){
			dataTD.removeClass('notEditing').addClass('editing').find('.dataTableEdit').addClass('block');
			dataTD.find('.dataTableCurrent').addClass('none');
		};
		this.stopEditing=function(dataTD){
			dataTD.removeClass('editing').addClass('notEditing').find('.dataTableCurrent').removeClass('none');
			dataTD.find('.dataTableEdit').removeClass('block');
		};
		this.createSigImage=function(img){
			return '<img src="'+img+'" class="dataTableSigImg" onerror="$(this).hide();"/>';
		};
		this.addDataTable=function(form){
			form.dataLaunch=form.launchButton.appendExtras('data');
			form.dataTable=new DataTable({
				title:'',
				table:form.properties.name,
				caller:form,
				callerProperties:form.properties,
				launchButton:form.dataLaunch
			});
			main.dataTables[form.properties.name]=form.dataTable;
			form.dataContentWrap=form.accordian.createDataShelf(form.dataTable);
		};
		this.deg2rad=function(deg){
			return deg*(Math.PI/180);
		};
		this.latLngDelta=function(lat1,lng1,lat2,lng2){
			//km
			var R=6371;
			var dLat=self.deg2rad(lat2-lat1);
			var dLng=self.deg2rad(lng2-lng1);
			var a=Math.sin(dLat/2)*Math.sin(dLat/2)+Math.cos(self.deg2rad(lat1))*Math.cos(self.deg2rad(lat2))*Math.sin(dLng/2)*Math.sin(dLng/2);
			var c=2*Math.atan2(Math.sqrt(a),Math.sqrt(1-a));
			return R*c;
		};
		this.addUploader=function(form){
			form.upoloaderLaunch=form.launchButton.appendExtras('upload');
			form.uploader=new Uploader({
				table:form.properties.name,
				modifies_layer:form.properties.modifies_layer,
				dataTable:form.dataTable,
				context:form,
				launchButton:form.upoloaderLaunch
			});
			form.uploaderContentWrap=form.accordian.createUploaderShelf(form.uploader);
		};
		this.getAlias=function(val,field,layerName){
			var alias;
			if(field.options && field.options[val] && !field.lookup_table){alias=field.options[val].alias;}
			if(field.lookup_table && main.data.lookUpTables[field.lookup_table]){
				if(main.data.lookUpTables[field.lookup_table].items){
					var lookUpVal=main.utilities.getLookUpTableHTML(field.lookup_table,val,field.name,layerName,field);
					if(lookUpVal!==null){alias=lookUpVal;}
				}else{return field.lookup_table;}
			}
			if(field.data_type=='boolean'){
				if(!val && !alias){alias='False';}
			}
			return alias;
		};
		this.getGroupsByCount=function(fieldName,field,features,layerName,colorGrad,characterSortType,numberBreaks,to_fixed,limit){
			var val;
			var normalGroup={
				field:field
			};
			//Number breaks
			if(main.misc.numberDTs.indexOf(field.data_type)>-1 && !field.options){
				//get extremes
				normalGroup.props={
					max:-Infinity,
					min:Infinity
				};
				for(var key in features){
					val=Number(features[key].properties[fieldName]);
					if(!isNaN(val)){
						if(val>normalGroup.props.max){normalGroup.props.max=val;}
						if(val<normalGroup.props.min){normalGroup.props.min=val;}
					}
				}
				// normalGroup.props.inputMax=normalGroup.props.max;
				// normalGroup.props.inputMin=normalGroup.props.min;
				normalGroup.groups={};
				if(field.groups){
					for(var key in field.groups){
						if(!normalGroup.groups[key]){
							field.groups[key].max=Number(field.groups[key].max);
							normalGroup.groups[key]={
								key:key,
								field:field,
								group:field.groups[key],
								alias:field.groups[key].alias,
								count:0
							}
						}
					}
				}else{
					//make breaks
					if(normalGroup.props.max!=-Infinity){					
						normalGroup.props.incriment=(normalGroup.props.max-normalGroup.props.min)/numberBreaks;
						var u=1,max,key,alias,minAlias,maxAlias;
						for(var t=normalGroup.props.min;t<normalGroup.props.max;t=t+normalGroup.props.incriment){
							max=t+normalGroup.props.incriment;
							if(u==numberBreaks){max=normalGroup.props.max;}
							min=t.toFixed(to_fixed),max=max.toFixed(to_fixed)
							key=u-1;
							minAlias=min,maxAlias=max;
							if(field.display_func){
								minAlias=main.utilities.callFunctionFromName(field.display_func,window,minAlias);
								maxAlias=main.utilities.callFunctionFromName(field.display_func,window,maxAlias);
							}else if(field.input_type=='date'){
								minAlias=main.utilities.formatDate(minAlias);
								maxAlias=main.utilities.formatDate(maxAlias);
							}
							alias=minAlias.toString()+' - '+maxAlias.toString();
							normalGroup.groups[key]={
								key:key,
								field:field,
								group:{
									alias:alias,
									min:Number(min),
									max:Number(max)
								},
								alias:alias,
								count:0
							}
							u++;
						}
					}
				}
				//get count
				var groups=normalGroup.groups;
				var max,u,groupsLength=Object.keys(groups).length;
				for(var key in features){
					val=features[key].properties[fieldName];
					if(val || val=='0'){
						val=Number(val);
						if(!isNaN(val)){
							for(var key2 in groups){
								if((val>=groups[key2].group.min && val<groups[key2].group.max) || (normalGroup.props.max==val && normalGroup.props.max==groups[key2].group.max)){normalGroup.groups[key2].count++;break;}
							}
						}
					}else{
						if(!normalGroup.nothing_group){normalGroup.nothing_group={count:1};
						}else{normalGroup.nothing_group.count++;}
					}
				}
				//set colors
				/* var groupsLength=Object.keys(normalGroup.groups).length,u=0;
				if(normalGroup.nothing_group){normalGroup.nothing_group.color='rgba(255,255,255,1)';}
				for(var key in normalGroup.groups){
					var colorA=main.utilities.getColor(groupsLength-1,0,u,colorGrad);
					normalGroup.groups[key].color='rgba('+colorA[0]+','+colorA[1]+','+colorA[2]+','+colorA[3]+')';
					u++;
				} */
				//sort
				normalGroup.htmlSortedKeys=Object.keys(normalGroup.groups);
				normalGroup.htmlSortedKeys=normalGroup.htmlSortedKeys.sort(function(a,b){
					if(groups[a].min>groups[b].min){return 1;}
					if(groups[a].min<groups[b].min){return -1;}
					return 0;
				});
				/* //make color gradient
				var canvas=document.createElement('canvas');
				canvas.width=30;
				canvas.height=200;
				normalGroup.props.linGrad={
					canvas:canvas
				};
				var ctx=canvas.getContext('2d');
				ctx.save();
				ctx.beginPath();
				var grd = ctx.createLinearGradient(0, 0, 30, 200);
				grd=main.utilities.createGradient(grd,self.defaultColorGrad);
				ctx.fillStyle = grd;
				ctx.fillRect(0, 0, 30, 200); */
			}else if(field.data_type=='character varying' || field.data_type=='text' || field.data_type=='boolean' || (main.misc.numberDTs.indexOf(field.data_type)>-1 && field.options)){		
				//String breaks
				normalGroup.groups={};
				var alias,isNothing;
				if(!field.filter_l_options){				
					for(var key in features){
						val=features[key].properties[fieldName];
						pid=features[key].properties.pid;
						isNothing=false;
						if(val || val=='0' || field.data_type=='boolean'){
							if(field.input_type=='checkbox'){
								if(typeof val=='string'){vals=val.split(',');}else{
									if(field.data_type=='boolean'){
										if(val){
											vals=['true'];
										}else{
											if(field.options && field.options['true'] && field.options['true'].offAlias){
												alias=field.options['true'].offAlias;
											}else{alias="False";}
											vals=['false'];
										}
									}else{
										if(field.options && field.options[val]==undefined){isNothing=true;}
										vals=[val.toString()];
									}
								}
								if(!isNothing){
									for(var e=0;e<vals.length;e++){
										if(!normalGroup.groups[vals[e]]){
											if(!alias && !(alias=self.getAlias(vals[e],field,layerName))){alias=vals[e];}
											normalGroup.groups[vals[e]]={
												value:vals[e],
												alias:alias,
												field:field,
												count:1,
												pid:pid
											}
										}else{
											normalGroup.groups[vals[e]].count++;
										}
									}
								}
							}else{
								if(!normalGroup.groups[val]){
									//get alias
									if(field.options && field.options[val]==undefined){
										isNothing=true;
									}else{
										if(!(alias=self.getAlias(val,field,layerName))){alias=val;}
										normalGroup.groups[val]={
											value:val,
											alias:alias,
											field:field,
											count:1,
											pid:pid
										}
									}
								}else{
									normalGroup.groups[val].count++;
								}
							}
						}else{isNothing=true;}
						if(isNothing){
							if(!normalGroup.nothing_group){normalGroup.nothing_group={count:1};
							}else{normalGroup.nothing_group.count++;}
						}
					}
				}else{
					isNothing=false;
					normalGroup.groups=self.cloneObj(field.options);
					var groups=normalGroup.groups;
					for(var key in groups){normalGroup.groups[key].count=0;}	
					for(var key in features){
						val=features[key].properties[fieldName];
						if(val || val=='0'){
							if(field.options && field.options[val]==undefined){isNothing=true;}
							if(normalGroup.groups[val]){normalGroup.groups[val].count++;}
						}else{isNothing=true;}
						if(isNothing){
							if(!normalGroup.nothing_group){normalGroup.nothing_group={count:1};
							}else{normalGroup.nothing_group.count++;}
						}
					}
				}
				//check for duplicate alias'
				/* for(var key0 in normalGroup.groups){
					
				} */
				//sort
				var groups=normalGroup.groups;
				normalGroup.htmlSortedKeys=Object.keys(groups);
				if(characterSortType=='abc'){
					normalGroup.htmlSortedKeys=normalGroup.htmlSortedKeys.sort(function(a,b){
						if(groups[a].alias>groups[b].alias){return 1;}
						if(groups[a].alias<groups[b].alias){return -1;}
						return 0;
					});
				}else if(characterSortType=='val'){
					normalGroup.htmlSortedKeys=normalGroup.htmlSortedKeys.sort(function(a,b){
						if(groups[a].value>groups[b].value){return 1;}
						if(groups[a].value<groups[b].value){return -1;}
						return 0;
					});
				}
			}
			var groups=normalGroup.groups;
			normalGroup.countSortedKeys=Object.keys(groups).sort(function(a,b){
				if(groups[a].count>groups[b].count){return -1;}
				if(groups[a].count<groups[b].count){return 1;}
				return 0;
			});
			//set colors
			var colorCodeType='val';
			if(colorCodeType=='val'){
				var colorKeys=normalGroup.htmlSortedKeys;
			}else if(colorCodeType=='count'){
				var colorKeys=normalGroup.countSortedKeys;
			}
			var groupsLength=Object.keys(normalGroup.groups).length,u=0;
			if(limit){
				if(colorKeys.length<limit){limit=colorKeys.length;}
				groupsLength=limit;
			}
			if(normalGroup.nothing_group){normalGroup.nothing_group.color='rgba(255,255,255,1)';}
			for(var o=0;o<colorKeys.length;o++){
				var colorA=main.utilities.getColor(groupsLength-1,0,u,colorGrad);
				normalGroup.groups[colorKeys[o]].color='rgba('+colorA[0]+','+colorA[1]+','+colorA[2]+','+colorA[3]+')';
				if(o<groupsLength-1){u++;}
			}
			return normalGroup;
		};
		this.boundTo=function(bounds,map,cb,cbParams){
			var initialCenter=map.getView().getCenter();
			var initialZoom=map.getView().getZoom();
			var newZoom;
			var newCenter=[(Number(bounds.max_x)+Number(bounds.min_x))/2,(Number(bounds.max_y)+Number(bounds.min_y))/2];
			map.getView().setCenter(newCenter);
			
			var mapExtent=map.getView().calculateExtent(map.getSize());
			//determine direction
			if(bounds.min_x<=mapExtent[0] || bounds.min_y<=mapExtent[1] || bounds.max_x>=mapExtent[2] || bounds.max_y>=mapExtent[3]){
				//too small
				var zoom=initialZoom;
				while(bounds.min_x<=mapExtent[0] || bounds.min_y<=mapExtent[1] || bounds.max_x>=mapExtent[2] || bounds.max_y>=mapExtent[3]){
					zoom--;
					map.getView().setZoom(zoom);
					mapExtent=map.getView().calculateExtent(map.getSize());
				}
				newZoom=zoom;
			}else{
				//too big
				var zoom=initialZoom;
				var lastZoom;
				var update=false;
				while(bounds.min_x>mapExtent[0] && bounds.min_y>mapExtent[1] && bounds.max_x<mapExtent[2] && bounds.max_y<mapExtent[3]){
					lastZoom=zoom;
					zoom++;
					map.getView().setZoom(zoom);
					mapExtent=map.getView().calculateExtent(map.getSize());
					update=true;
				}
				if(update){newZoom=lastZoom;}
			}
			if(cb){
				cb(initialCenter,newCenter,initialZoom,newZoom,cbParams);
			}
		};
		this.getDefaultStyle=function(){
			return new ol.style.Style({
				fill: new ol.style.Fill({
					color: main.defaultStyle.fillColor
				}),
				stroke: new ol.style.Stroke({
					color: main.defaultStyle.strokeColor,
					width: main.defaultStyle.strokeWidth
				}),
				image: new ol.style.Circle({
					radius: main.defaultStyle.pointRadius,
					fill: new ol.style.Fill({
						color: main.defaultStyle.pointFill
					})
				})
			})
		};
		this.loggedOutNotice=function(){
			alert('You are not logged in. Please log back in.');
			main.account.login.logOut();
		};
		this.isIntersecting=function(f){
			var coords=f.getGeometry().getCoordinates()[0];
			var segments=[],x,y,m,b,deltaX,deltaY,point2,point1;
			for(var i=0;i<coords.length-1;i++){
				point1=coords[i];
				point2=coords[i+1];
				deltaX=point2[0]-point1[0];
				deltaY=point2[1]-point1[1];
				m=deltaY/deltaX;
				b=point2[1]-m*point2[0];
				segments.push({
					m:m,
					b:b,
					x1:point1[0],
					y1:point1[1],
					x2:point2[0],
					y2:point2[1]
				});
			}
			var intersects=false;
			for(var i=0;i<segments.length;i++){
				seg1=segments[i];
				for(var t=0;t<segments.length;t++){
					seg2=segments[t];
					omitNext=i+1;
					omitPrev=i-1;
					if(omitNext==segments.length){
						omitNext=0;
					}
					if(omitPrev<0){
						omitPrev=segments.length-1;
					}
					if(t!=i && t!=omitNext && t!=omitPrev && seg2.m!=seg1.m){
						x=(seg1.b-seg2.b)/(seg2.m-seg1.m);
						y=seg1.m*x+seg1.b;
						if(x<=Math.max(seg1.x1,seg1.x2) && x>=Math.min(seg1.x1,seg1.x2) && x<=Math.max(seg2.x1,seg2.x2) && x>=Math.min(seg2.x1,seg2.x2)){
							return true;
						}
					}
				}
			}
			return false;
		};
		this.getCoords=function(feature){
			var coords=null;
			if(feature.getGeometry() instanceof ol.geom.MultiPolygon || feature.getGeometry() instanceof ol.geom.Polygon || feature.getGeometry() instanceof ol.geom.MultiLineString){
			main.fff=feature;
				var ext=feature.getGeometry().getExtent();
				var x=(ext[2]+ext[0])/2;
				var y=(ext[3]+ext[1])/2;
				coords=[x,y];
			}else{
				coords=feature.getGeometry().getCoordinates();
			}
			return coords;
		};
		this.getGeo=function(table,geomField,pid,callback,getProperties){
			Object.keys(main.tables[table].fields)
			var testForZoomHome=null;
			getProperties=getProperties || null;
			if(main.tables[table].fields.default_zoom_home){testForZoomHome=true;}
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						table:table,
						testForZoomHome:testForZoomHome,
						geomField:geomField,
						getProperties:getProperties,
						getExtent:'true',
						pid:pid
					},
					action:'getGeo'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							if(callback){callback(response);}
						}
					}
				}
			});
		};
		this.getGroupsValTop=function(groups,value){
			var group;
			var keys=Object.keys(groups);
			var firstGroup=keys[0];
			var max,min;
			for(var key in groups){
				max=groups[key].max;
				min=groups[key].min;
				if(max=='Infinity'){max=Infinity;}
				if(min=='-Infinity'){min=-Infinity;}
				if(firstGroup!=key){
					if(value>min && value<=max){
						group=groups[key];}
				}else{
					if(value>=min && value<=max){
						group=groups[key];}
				}
			}
			if(group){
				return group.optionsVal;
			}else{return null;}
		};
		this.getGroupsVal=function(groups,value){
			var group;
			var keys=Object.keys(groups);
			var lastGroup=keys[keys.length-1];
			var max,min;
			for(var key in groups){
				max=groups[key].max;
				min=groups[key].min;
				if(max=='Infinity'){max=Infinity;}
				if(min=='-Infinity'){min=-Infinity;}
				if(lastGroup!=key){
					if(value>=min && value<max){
						group=groups[key];}
				}else{
					if(value>=min && value<=max){
						group=groups[key];}
				}
			}
			if(group){
				return group.optionsVal;
			}else{return null;}
		};
		this.doSpecialConnection=function(value,specialConnection,primaryE,bulkCountCheck,layerName,pid,e){
			// var specialConnection=self.inventory.properties.special_connections[field.name];
			var to_effect=specialConnection.to_effect;
			var type=specialConnection.type;
			if(type=='place_in_range'){
				if(!isNaN(value)){
					value=Number(value);
					var to_effect_field=main.invents.inventories[layerName].fields[to_effect];
					var to_effect_input=primaryE.closest('.dataUnit').find('.formInput.'+to_effect);
					var groups=to_effect_field.groups;
					if(groups){
						var groupVal=self.getGroupsVal(groups,value);
						if(groupVal!==null){to_effect_input.val(groupVal).change();}
					}
				}
			}else if(type=='count'){
				if(main.invents.currentFilterPID){
					if(bulkCountCheck){
						var features=main.invents.inventories[specialConnection.to_effect_layer].inventory.map_layer.features;
						if(features && (features[main.invents.currentFilterPID].properties[specialConnection.dont_count_if]!==false && features[main.invents.currentFilterPID].properties[specialConnection.dont_count_if]!==null)){
							return true;
						}
					}else{
						$.ajax({
							type:'POST',
							url:main.mainPath+'server/db.php',
							data:{
								params:{
									folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
									count_field:specialConnection.count_field,
									value:main.invents.currentFilterPID,
									to_effect_layer:specialConnection.to_effect_layer,
									to_effect:specialConnection.to_effect,
									table:layerName
								},
								action:'getCount'
							},
							success:function(response){
								if(response){
									response=JSON.parse(response);
									if(response.status=="OK"){
										var count=response.results.count;
										var to_effect_layer=response.params.to_effect_layer;
										var to_effect=response.params.to_effect;
										var value=response.params.value;
										if(main.invents.inventories[to_effect_layer].inventory.map_layer.features[value].editLargeTable){
											main.invents.inventories[to_effect_layer].inventory.map_layer.features[value].editLargeTable.table.find('.formInput.'+to_effect).eq(0).val(count).change();
										}/* else if(self.inventories.inventories[to_effect_layer].inventory.map_layer.features[value].editTable){
											self.inventories.inventories[to_effect_layer].inventory.map_layer.features[value].popup.createEditForm();
											self.inventories.inventories[to_effect_layer].inventory.map_layer.features[value].editLargeTable.table.find('.formInput.'+to_effect).eq(0).val(count).change();
										} */else{
											main.invents.inventories[to_effect_layer].inventory.map_layer.features[value].fields[to_effect].current_val=count;
											// self.inventories.inventories[to_effect_layer].updateData([value]);
											main.dataEditor.updateData({
												pids:[value],
												context:main.invents.inventories[to_effect_layer],
												changeFields:[{field:main.invents.inventories[to_effect_layer].fields[to_effect],val:count}]
											});
										}
									}
								}
							}
						});
					}
				}
			}else if(type=='disable'){
				var to_effect=specialConnection.to_effect;
				var checked=primaryE.prop('checked');
				if(checked){
					var confirmed=true;
					var ct=Number(primaryE.closest('.dataUnit').find('input.'+to_effect).val());
					if(ct){
						if(confirm('Are you sure you want to count data as bulk? All currently plotted data of this '+main.invents.inventories[layerName].singular_alias+' will be deleted.')){
							if(confirm('This cannot be undone. All currently plotted data of this '+main.invents.inventories[layerName].singular_alias+' will be deleted. Are you sure you want to continue?')){
								if(specialConnection.remove_depended_upon_by){
									var depended_upon_by=main.invents.inventories[layerName].inventory.is_depended_upon_by;
									var remove_field=specialConnection.remove_field;
									main.utilities.deleteAllData(depended_upon_by,remove_field,pid);
								}
							}else{
								confirmed=false;
							}
						}else{
							confirmed=false;
						}
					}
					if(!confirmed){
						primaryE.prop('checked',false);
						return false;
					}else{
						primaryE.closest('.dataUnit').find('input.'+to_effect).prop('disabled',false);
					}
				}else{
					primaryE.closest('.dataUnit').find('input.'+to_effect).prop('disabled',true);
					if(specialConnection.clear_val){primaryE.closest('.dataUnit').find('input.'+to_effect).val('').change();}
				}
			}else if(type=='sum_of_others'){
				var effected_by=specialConnection.effected_by;
				$.ajax({
					type:'POST',
					url:main.mainPath+'server/db.php',
					data:{
						params:{
							folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
							pid:pid,
							effected_by:effected_by,
							layerName:layerName,
							to_effect:to_effect
						},
						action:'getColSum'
					},
					success:function(response){
						if(response){
							response=JSON.parse(response);
							if(response.status=="OK"){
								var to_effect_field=main.invents.inventories[response.params.layerName].fields[response.params.to_effect]; 
								if(to_effect_field.groups){
									var groupVal=self.getGroupsValTop(to_effect_field.groups,Number(response.sum));
									if(groupVal!==null){primaryE.closest('.dataUnit').find('.formInput.'+response.params.to_effect).val(groupVal).change();}
								}
							}else{
								alert('Error');
								console.log(response);
							}
						}
					}
				});
			}/* else if(type=='count'){
					var layer=specialConnection.to_effect_layer;
					var selfFeatures=self.inventory.map_layer.features;
					var ct=0;
					for(var key in selfFeatures){
						if(selfFeatures[key].properties[specialConnection.count_field]==featureItems.properties[specialConnection.count_field]){
							ct+=Number(selfFeatures[key].properties[field.name]):0;
						}
					}
					if(main.tables[layer].map_layer.features){
						var features=main.tables.event.map_layer.features;
						for(var key in features){
							// features[key].properties[to_effect]=ct;
						}
					{"count_trees":{"to_effect":"num_trees_added","to_effect_layer":"event","type":"count","count_field":"event_name"}}
					{"count_trees":{"to_effect":"num_trees_added","to_effect_layer":"event","type":"count","count_field":"event_name"},"bulk_count":{"to_effect":"num_trees_added","type":"disable"}}
					{"count_trees":{"to_effect":"num_trees_added","to_effect_layer":"event","type":"count","count_field":"event_name"},"bulk_count":{"to_disable":"num_trees_added","type":"disable"}}
					{"bulk_count":{"to_effect":"num_trees_added","type":"disable","remove_depended_upon_by":"true"}}
					{"bulk_count":{"to_effect":"num_trees_added","type":"disable","remove_depended_upon_by":"true","remove_field":"event_name"}}
					
					{"count_trees":{"to_effect":"num_trees_added","to_effect_layer":"event","type":"count","count_field":"event_name","dont_count_if":"bulk_count"}}
					}
				} */
		};
		this.sortOptions=function(select){
			var keys=[];
			select.find('option').each(function(){
				keys.push([$(this).val(),$(this).html()]);
			});
			keys=keys.sort(function(a,b){
				if(!a[1]){return -1;}
				if(!b[1]){return 1;}
				if(a[1].toLowerCase()>b[1].toLowerCase()){return 1;}
				if(a[1].toLowerCase()<b[1].toLowerCase()){return -1;}
				return 0;
			});
			for(var i=0;i<keys.length;i++){
				select.find('option[value="'+keys[i][0]+'"]').appendTo(select);
			}
		};
		this.sortShelves=function(accordian,sortBy){
			sortBy=sortBy || 'abc';
			var shelves=accordian.children('.shelf'),keys=[];
			if(sortBy=='abc'){
				shelves.each(function(){
					keys.push($(this).data('name'));
				});
				var keys2=keys.sort(function(a,b){
					aliasA=shelves.filter('.'+a).find('.shelfButtonWrap .buttonContent').html().toLowerCase();
					aliasB=shelves.filter('.'+b).find('.shelfButtonWrap .buttonContent').html().toLowerCase();
					if(aliasA>aliasB){return 1;}
					if(aliasA<aliasB){return -1;}
					return 0;
				});
				for(var i=0;i<keys2.length;i++){shelves.filter('.'+keys2[i]).appendTo(accordian);}
			}
		};
		this.sortDTFields=function(fields){
			var keys=[];
			if(fields){
				var itemA,itemB;
				keys=Object.keys(fields);
				keys.sort(function(a,b){
					aPIO=fields[a].place_in_order;
					bPIO=fields[b].place_in_order;
					if(fields[a].dt_place_in_order || fields[a].dt_place_in_order==0){aPIO=fields[a].dt_place_in_order;}
					if(fields[b].dt_place_in_order || fields[b].dt_place_in_order==0){aPIO=fields[b].dt_place_in_order;}
					if(!aPIO && aPIO!==0){return -1;
					}else{return aPIO-bPIO;}
				});
			}
			return keys;
		};
		this.sortFields=function(fields,sortBy){
			var keys=[];
			if(fields){
				var itemA,itemB;
				keys=Object.keys(fields)
				if(sortBy=='abc'){
					keys.sort(function(a,b){
						if(fields[a].field){
							itemA=fields[a].field.alias;
							itemB=fields[b].field.alias;
							if(itemA){itemA=itemA.toLowerCase();}
							if(itemB){itemB=itemB.toLowerCase();}
							if(itemA>itemB) return 1;
							if(itemA<itemB) return -1;
							return 0;
						}else{
							itemA=fields[a].alias;
							itemB=fields[b].alias;
							if(itemA){itemA=itemA.toLowerCase();}
							if(itemB){itemB=itemB.toLowerCase();}
							if(itemA>itemB) return 1;
							if(itemA<itemB) return -1;
							return 0;
						}
					});
				}else{
					keys.sort(function(a,b){
						if(!fields[a].place_in_order && fields[a].place_in_order!==0){return -1;
						}else{return fields[a].place_in_order-fields[b].place_in_order;}
					});
				}
			}
			return keys;
		};
		// this.convertToBool=function(val){
			// if(val=='true'){val=true;
			// }else if(val=='false'){val=false;}
			// return val;
		// };
		this.sortObjectByAlias=function(obj){
			var keys=Object.keys(obj);
			keys.sort(function(a,b){
				if(obj[a].alias>obj[b].alias){return 1;}
				if(obj[a].alias<obj[b].alias){return -1;}
				return 0;
			});
			return keys;
		};
		this.createFilePKG=function(fields,overRideExcluded,lookUpAsAlias,context){
			var pkg={},field,aField,bField,key;
			var keys=Object.keys(fields);
			keys.sort(function(a,b){
				aField=fields[a];
				if(aField.field){aField=aField.field;}
				bField=fields[b];
				if(bField.field){bField=bField.field;}
				if(aField.place_in_order>bField.place_in_order){return 1;}
				if(aField.place_in_order<bField.place_in_order){return -1;}
				return 0;
			});
			for(var r=0;r<keys.length;r++){
				key=keys[r];
				field=fields[key];
				value=field.current_val;
				if(field.field){field=field.field;}
				if(field.active && (field.in_atc_1 && (overRideExcluded || (main.misc.excludeFileDTs.indexOf(field.data_type)===-1 && main.misc.excludeFileITs.indexOf(field.input_type)===-1))) && field[main.account.loggedInUser.user_type+'_visible']){
					var lookupTableMap=null;
					if(context && lookUpAsAlias && context.callerContext.callerProperties.lookup_table_map){lookupTableMap=context.callerContext.callerProperties.lookup_table_map;}
					var cultivarField=null;
					if(context.callerContext.callerProperties.internal_field_map && context.callerContext.callerProperties.internal_field_map.cultivar){cultivarField=context.callerContext.callerProperties.internal_field_map.cultivar;}
					value=self.getHTMLFromVal(value,field,lookupTableMap,cultivarField);
					pkg[key]={
						value:value,
						data_type:field.data_type,
						sub_data_type:field.sub_data_type,
						input_type:field.input_type,
						alias:field.alias,
						append_to:field.append_to,
						prepend_to:field.prepend_to,
						lookup_table:field.lookup_table,
						html:field.html,
						key:key,
						pid:field.pid,
						atc_protected:field.atc_protected,
						/* atc_hidden:field.atc_hidden, */
						options:field.options
					}
					if(field.input_type=='signature'){
						pkg[key].sigImg=field.input.signature.img;
					}
				}
			}
			return pkg;
		};
		this.getColor=function(max,min,value,colorGrad){
			if(colorGrad!='Random'){
				var normalizeTo=510;
			}else{
				var normalizeTo=1530;
			}
			var red=0,green=0,blue=0;
			var number=((value-min)/(max-min))*normalizeTo;
			if(isNaN(number)) number=0
			return this.getColorNumber(number,normalizeTo,max,colorGrad);
		};
		this.getInvertedColorType=function(type){
			var newType='Red-Green';
			if(type=='Red-Green'){newType='Green-Red';}
			if(type=='Green-Red'){newType='Red-Green';}
			if(type=='Blue-Red'){newType='Red-Blue';}
			if(type=='Red-Blue'){newType='Blue-Red';}
			return newType;
		};
		this.getColorNumber=function(number,normalizeTo,colorLength,type){
			var red=0,green=0,blue=0;
			type=type || 'Red-Green';
			if(type=='Blue-Red'){
				if(number<=255){
					blue=255;
					red=number;
				}else{
					blue=normalizeTo-number;
					red=255;
				}
			}if(type=='Red-Blue'){
				if(number<=255){
					red=255;
					blue=number;
				}else{
					red=normalizeTo-number;
					blue=255;
				}
			}else if(type=='Green-Red'){
				if(number<=255){
					green=255;
					red=number;
				}else{
					green=normalizeTo-number;
					red=255;
				}
			}else if(type=='Red-Green'){
				if(number<=255){
					red=255;
					green=number;
				}else{
					red=normalizeTo-number;
					green=255;
				}
			}else if(type=='Random'){
				number=number*colorLength/(colorLength+1);
				if(number<=255){
					red=255;
					green=number;
				}else if(number<=510){
					green=255; 
					red=510-number;
				}else if(number<=765){
					green=255; 
					blue=number-510;
				}else if(number<=1020){
					blue=255;
					green=1020-number;
				}else if(number<=1275){
					blue=255;
					red=number-1020;
				}else if(number<=1530){
					red=255;
					blue=1530-number;
				}
			}else if(type=='Vivid Random'){
				var array=[0,1,2];
				zeroSelector=Math.floor(Math.random()*2.9999999);
				toZero=array.splice(zeroSelector,1);
				highSelector=Math.floor(Math.random()*1.9999999);
				toHigh=array.splice(highSelector,1);
				var rgb=[Math.random()*255,Math.random()*255,Math.random()*255];
				rgb[toZero]=0;
				rgb[toHigh]=255;
				red=rgb[0],green=rgb[1],blue=rgb[2];
			}
			return [Math.round(red),Math.round(green),Math.round(blue),1];
		};
		this.rgbToHex=function(r,g,b){
			if(r>255 || g>255 || b>255){
				throw "Invalid color component";}
			return ((r<<16) | (g<<8) | b).toString(16);
		};
		this.getColorFromGradient=function(max,min,val,colorGrd){
			var name=colorGrd[0]+','+colorGrd[1];
			var width=250;
			var height=3;
			if(!main.layout.grds[name]){
				var canvas=document.createElement('canvas');
				canvas.width=width;
				canvas.height=height;
				var ctx=canvas.getContext('2d');
				var grd = ctx.createLinearGradient(0, 0, width, 0);
				grd.addColorStop(0,colorGrd[0]);
				grd.addColorStop(1,colorGrd[1]);
				ctx.fillStyle = grd;
				ctx.fillRect(0, 0, width, height);
				var canvasHTML=$(canvas).addClass('colorGrdCnvs').appendTo(main.layout.grdsContainer);
				main.layout.grds[name]={
					canvasHTML:canvasHTML
				}
			}else{
				var canvasHTML=main.layout.grds[name].canvasHTML;
			}
			// canvasHTML.addClass('block');
			var ctx=canvasHTML[0].getContext('2d');
			var left=canvasHTML.offset().left;
			var top=canvasHTML.offset().top;
			var xOffset=Math.round((val-min)*(width-1)/(max-min));
			var x=xOffset;
			var y=1;
			var p=ctx.getImageData(x,y,1,1).data;
			var color=[p[0],p[1],p[2],1];
			// var hex='#'+('000000'+self.rgbToHex(p[0],p[1],p[2])).slice(-6);
			return color;
		};
		this.createCanvGradRef=function(grd,colorGrd){
			if(colorGrd=='Green-Tan'){
				grd.addColorStop(0,'#033701');
				grd.addColorStop(1,'#F8F1CB');
			}else if(colorGrd=='Tan-Green'){
				grd.addColorStop(0,'#F8F1CB');
				grd.addColorStop(1,'#033701');
			}else if(colorGrd=='Blue-Red'){
				grd.addColorStop(0,'#0000FF');
				grd.addColorStop(1,'#FF0000');
			}else if(colorGrd=='Red-Blue'){
				grd.addColorStop(0,'#FF0000');
				grd.addColorStop(1,'#0000FF');
			}else if(colorGrd=='Green-Red'){
				grd.addColorStop(0,'#008000');
				grd.addColorStop(1,'#FF0000');
			}else if(colorGrd=='Red-Green'){
				grd.addColorStop(0,'#FF0000');
				grd.addColorStop(1,'#008000');
			} 
			return grd;
		};
		this.createGradient=function(grd,colorGrd){
			if(colorGrd=='Blue-Red'){
				var red;
				for(var i=0;i<255;i++){
					grd.addColorStop(i/510, 'rgb(255,0,'+i+')');
				}
				for(var i=0;i<255;i++){
					red=255-i;
					grd.addColorStop((i+255)/510, 'rgb('+red+',0,255)');
				}
			}else if(colorGrd=='Red-Blue'){
				var red;
				for(var i=0;i<255;i++){
					grd.addColorStop(i/510, 'rgb('+i+',0,255)');
				}
				for(var i=0;i<255;i++){
					red=255-i;
					grd.addColorStop((i+255)/510, 'rgb(255,0,'+red+')');
				}
			}else if(colorGrd=='Green-Red'){
				var red;
				for(var i=0;i<255;i++){
					grd.addColorStop(i/510, 'rgb(255,'+i+',0)');
				}
				for(var i=0;i<255;i++){
					red=255-i;
					grd.addColorStop((i+255)/510, 'rgb('+red+',255,0)');
				}
			}else if(colorGrd=='Red-Green'){
				var red;
				for(var i=0;i<255;i++){
					grd.addColorStop(i/510, 'rgb('+i+',255,0)');
				}
				for(var i=0;i<255;i++){
					red=255-i;
					grd.addColorStop((i+255)/510, 'rgb(255,'+red+',0)');
				}
			} 
			return grd;
		};
		// this.initEvents=function(){
		// };
		// this.initEvents();
	};
});
