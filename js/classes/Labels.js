define(function(require){
	return function pg(map){
		var self=this;
		var Button=require('./Button');
		var EditableList=require('./EditableList');
		var Input=require('./Input');
		var Switch=require('./Switch');
		var ViewMore=require('./ViewMore');
		this.map=map;
		this.keys=main.utilities.cloneObj(main.data.std_settings.labels.sortedKeys);
		this.formID='labelForm';
		this.withItem='with_labels';
		this.layers={};
		this.primaryLayer=null;
		this.primaryField=null;
		var defaultCanopyFill='rgba(255, 255, 255, 0.9)';
		var defaultCanopyOutline='rgba(0, 0, 0, 0.9)';
		this.setParams=function(){
			this.params=main.utilities.cloneObj(main.data.std_settings.labels);
			
		};
		this.alias='Labels';
		this.createHTML=function(){
			this.addShelf();
			main.utilities.initSpectrumPickers(this.params,this.udpateLabels,true,false);
		};
		this.addShelf=function(){
			this.launchButton=new Button('toolBox','',this.alias);
			this.labelForm=this.createLabelForm();
			this.shelf=main.layout.toolBox.createShelf({
				name:'labels',
				button:this.launchButton.html,
				content:this.labelForm,
				alias:this.alias,
				groups:main.withs[self.withItem].toolbox_groups
			});
		};
		this.updateHTMLElements=function(layer){
			self.layers[layer.id]=layer;
	
			if(self.mainLabelLayerSelect){
				self.mainLabelLayerSelect.append('<option value="'+layer.id+'">'+layer.alias+'</option>');
				main.utilities.sortOptions(self.mainLabelLayerSelect);
			}
		};
		this.updateMainFields=function(fields){
			self.mainLabelFieldSelect.html('<option value=""></option>');
			var keys=main.utilities.sortFields(fields,'abc'),key;
			for(var i=0;i<keys.length;i++){
				key=keys[i];
				var labelfield=true;
				if(main.canopy_layers && main.canopy_layers[self.primaryLayer]){
					labelfield=fields[key].canopy_label;
				}
				if(fields[key].active && fields[key].input_type!=='html' && fields[key].input_type!='hidden' && fields[key][main.account.loggedInUser.user_type+'_visible'] && labelfield){
					self.mainLabelFieldSelect.append('<option value="'+key+'">'+fields[key].alias+'</option>');
				}
			}
		};
		this.createLabelForm=function(){
			var labelForm=$('<form>').attr('id',this.formID);
			this.labelsList=new EditableList({
				removeable:false,
				reorderable:false,
				checkable:false
			});
			self.mainLabelLayerSelect=$('<select class="mainLabelLayerSelect">')
				.append('<option value=""></option>');
			self.mainLabelLayerSelect.on('change',function(){
				self.primaryLayer=$(this).val();
				if($(this).val()){
					// if(self.mainLabelFieldSelect.val()){
						if(main.canopy && main.canopy_layers[self.primaryLayer]){
							$("#labelForm").find('.tbFormParamRow_offset_x').find(".offset_x").val(0);
							$("#labelForm").find('.tbFormParamRow_offset_y').find(".offset_y").val(0);
							$("#labelForm").find(".fill_color").spectrum("set",defaultCanopyFill);
							$("#labelForm").find(".outline_color").spectrum("set",defaultCanopyOutline);
							//$("#labelForm").find(".fill_color").val(defaultCanopyFill).spectrum();
							//$("#labelForm").find(".outline_color").val(defaultCanopyOutline).spectrum();
						}
						var fields=main.tables[self.primaryLayer].fields;
						self.updateMainFields(fields);
					// }
				}
			});
			self.mainLabelFieldSelect=$('<select class="mainLabelFieldSelect">')
				.append('<option value=""></option>');
			self.mainLabelFieldSelect.on('change',function(){
				self.primaryField=$(this).val();
				if($(this).val()){
					self.udpateLabels();
				}
			});
			self.viewMore=new ViewMore({
				prev:'Use this tool to toggle on/off/change the appearance of labels in the map.',
				content:'<ul><li>Step 1: Select a layer to add labels to.</li><li>Step 2: Select a field to add labels to.</li><li>Step 3: Change the parameters.</li><li>Step 4: Click the On/Off switch.</li></ul>'
			});
			var table=$('<table>').addClass('shelfForm')
				.append($('<tr>')
					.append($('<td colspan="2">').append($('<div>').addClass('tdLiner toolBoxDesc').html(self.viewMore.html)))
				)
				.append($('<tr>')
					.append($('<td>').append($('<div>').addClass('tdLiner').text('Layer')))
					.append($('<td>').append($('<div>').addClass('tdLiner').html(self.mainLabelLayerSelect)))
					// .append($('<td>').append($('<div>').addClass('tdLiner checkWrap').append(this.labelsList.container)))
				)
				.append($('<tr>')
					.append($('<td>').append($('<div>').addClass('tdLiner').text('Fields')))
					.append($('<td>').append($('<div>').addClass('tdLiner').html(self.mainLabelFieldSelect)))
				).append($('<tr>')
					.append($('<th colspan="2">')
						.append($('<div>').addClass('tdLiner center').text('Label Properties'))
					)
				);
			var toolFormMakeItems=main.utilities.toolFormMake(this.keys,this.params,this.formID,table);
			this.advancedViewMore=toolFormMakeItems.advancedViewMore;
			this.advancedOptions=toolFormMakeItems.advancedOptions;
			this.labelFormSwitch=new Switch(this.addLabels,this.removeLabels);
			this.updateFormButton=$('<div>').addClass('button updateForm').text('Update Layers');
			table.append($('<tr>')
					.append($('<td>')
						.append($('<div>').addClass('tdLiner submitRowEle stdMrgTop cf')
							// .append(this.updateFormButton)
						)
					).append($('<td colspan="2">')
						.append($('<div>').addClass('tdLiner submitRowEle stdMrgTop cf')
							.append($('<div>').addClass('submitRowEle submitRowSwitchWrap').append(this.labelFormSwitch.container))
						)
					)
				)
			return labelForm.append(table);
		};
		this.refresh=function(){
			self.addLabels();
		};
		this.turnOn=function(){
			self.labelFormSwitch.turnSwitchOn(null,true);
		};
		this.removeLabels=function(){
			var layerIDs=[];
			// self.labelsList.container.find('.eLCheck').each(function(){if($(this).prop('checked')) layerIDs.push($(this).val());});
			if(self.primaryLayer){
				if(self.primaryField){
					layerIDs=[self.primaryLayer];
					for(var i=0;i<layerIDs.length;i++){
						main.tables[layerIDs[i]].map_layer.layer.labels.withLabels=false;
						main.tables[layerIDs[i]].map_layer.layer.style.setStyleFunction();
						//main.tables[layerIDs[i]].map_layer.layer.labels.removeLabels();
						
					}
					if(main.canopy && main.canopy.currentLayer.map_layer.layer.visible){
						main.canopy.refreshFeatures();
					}
					
				}else{
					self.labelFormSwitch.turnSwitchOff(true);
					alert('Please select a field.');
				}
			}else{
				self.labelFormSwitch.turnSwitchOff(true);
				alert('Please select a layer.');
			}
		};
		this.addLabels=function(){
			var layerIDs=[],layer;
			// self.labelsList.container.find('.eLCheck').each(function(){if($(this).prop('checked')) layerIDs.push($(this).val());});
			if(self.primaryLayer){
				if(self.primaryField){
					layerIDs=[self.primaryLayer];
					for(var i=0;i<layerIDs.length;i++){
						main.tables[layerIDs[i]].map_layer.layer.labels.withLabels=true;
						// if(self.map.vectorLayers[layerIDs[i]].labelFieldSelect){
							// self.map.vectorLayers[layerIDs[i]].labels.addLabels(self.map.vectorLayers[layerIDs[i]].labelFieldSelect.val());
							main.tables[layerIDs[i]].map_layer.layer.labels.addLabels(self.primaryField);
							if(main.canopy && main.canopy.currentLayer.map_layer.layer.visible){
								main.canopy.refreshFeatures();
							} 
						// }
					}
				}else{
					self.labelFormSwitch.turnSwitchOff(true);
					alert('Please select a field.');
				}
			}else{
				self.labelFormSwitch.turnSwitchOff(true);
				alert('Please select a layer.');
			}
		};
		this.udpateLabels=function(){
			var layerIDs=[],layer;
			// self.labelsList.container.find('.eLCheck').each(function(){if($(this).prop('checked')) layerIDs.push($(this).val());});
			if(self.primaryLayer){
				if(self.primaryField){

					layerIDs=[self.primaryLayer];
					for(var i=0;i<layerIDs.length;i++){
						// layer=self.map.vectorLayers[layerIDs[i]];
						layer=main.tables[layerIDs[i]].map_layer.layer;
						if(self.labelFormSwitch.onState && !layer.labels.withLabels) layer.labels.withLabels=true;
						if(!self.labelFormSwitch.onState && layer.labels.withLabels) layer.labels.withLabels=false;
						// if(layer.labelFieldSelect){
							// layer.labels.addLabels(layer.labelFieldSelect.val());
							layer.labels.addLabels(self.primaryField);
							if(main.canopy && main.canopy.currentLayer.map_layer.layer.visible){
								main.canopy.refreshFeatures();
							} 
						// }
					}
				}else{
					alert('Please select a field.');
				}
			}else{
				alert('Please select a layer.');
			}
		};
		this.labelsInputChange=function(){
			self.udpateLabels();
		};
		this.updateForm=function(){
			var layerIDs=[];
			// self.labelsList.container.find('.eLCheck').each(function(){if($(this).prop('checked')) layerIDs.push($(this).val());});
			if(self.primaryLayer){
				if(self.primaryField){
					layerIDs=[self.primaryLayer];
					for(var i=0;i<layerIDs.length;i++){
						var labelParams=self.params,layerLabels=self.map.vectorLayers[layerIDs[i]].labels;
						for(var key in labelParams){
							if(labelParams[key].input_type=='color'){labelParams[key].input.spectrum('set',layerLabels.params[key].current_val);
							}else if(labelParams[key].input_type=='number'){labelParams[key].input.val(parseInt(layerLabels.params[key].current_val));
							}else{labelParams[key].input.val(layerLabels.params[key].current_val);}
						}
						if(layerLabels.withLabels){self.labelFormSwitch.turnSwitchOn(true);
						}else{self.labelFormSwitch.turnSwitchOff(true);}
					}
				}else{
					alert('Please select a field.');
				}
			}else{
				alert('Please select a layer.');
			}
		};
		this.initEvents=function(){
			this.updateFormButton.on('click',function(e){
				self.udpateLabels();
			});
			this.labelsList.container.on('click','.eLCheck',function(e){
				// self.updateForm();
			});
			this.labelForm.on('change','.formInput',function(e){
				self.labelsInputChange();
			});
		};
		this.setParams();
		if(main.withs.with_labels.value){
			this.createHTML();
			main.utilities.setCurrentVals(this.labelForm,this.params);
			this.initEvents();
		}
	};
});