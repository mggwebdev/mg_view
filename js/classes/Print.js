define(function(require){
	return function pg(map){
		var self=this;
		var Button=require('./Button');
		var ViewMore=require('./ViewMore');
		this.map=map;
		this.keys=main.utilities.cloneObj(main.data.std_settings.print.sortedKeys);
		this.alias='Print';
		this.printing=false,
		this.formID='printForm';
		this.withItem='with_print';
		this.printParams={
			itemsOffSetX:10,
			itemsOffSetY:10
		};
		this.setParams=function(){
			this.params=main.utilities.cloneObj(main.data.std_settings.print);
		};
		this.createHTML=function(){
			this.addShelf();
		};
		this.addShelf=function(){
			this.launchButton=new Button('toolBox','',this.alias);
			this.printForm=this.createPrintForm();
			this.shelf=main.layout.toolBox.createShelf({
				name:'print',
				button:this.launchButton.html,
				content:this.printForm,
				alias:this.alias,
				groups:main.withs[this.withItem].toolbox_groups
			});
		};
		this.createPrintForm=function(){
			var printForm=$('<form>').attr('id',this.formID);
			var selected,options;
			this.startPrintingButton=$('<div>').addClass('button startPrinting').text('Print');
			self.viewMore=new ViewMore({
				prev:'Export current extent to a printable image file.',
				content:'<ul><li>Step 1: Select a file type.</li><li>Step 2: Add a title and select an alignent, this is where the title will show up in the print.</li><li>Step 3: If you want a legend, select the "Legend" checkbox and select its alignment.</li><li>Step 4: If you want a scale, select the "Scale" checkbox and select its alignment.</li><li>Step 4: If you want a custom file name, alter the "File Name" input.</li></ul>'
			});
			var table=$('<table>').addClass('shelfForm')
				.append($('<tr>')
					.append($('<td colspan="2">').append($('<div>').addClass('tdLiner toolBoxDesc').html(self.viewMore.html)))
				)
				.append($('<tr>')
					.append($('<th colspan="2">')
						.append($('<div>').addClass('tdLiner center').text('Print Properties'))
					)
				);
			var toolFormMakeItems=main.utilities.toolFormMake(this.keys,this.params,this.formID,table);
			this.advancedViewMore=toolFormMakeItems.advancedViewMore;
			this.advancedOptions=toolFormMakeItems.advancedOptions;
			this.typeSelect=toolFormMakeItems.typeSelect;
			table.append($('<tr>')
				.append($('<td>').append($('<div>').addClass('tdLiner').append(this.startPrintingButton)))
			);
			return printForm.append(table);
		};
		this.printingInputChange=function(){
			self.updatePrintingParams();
		};
		this.updatePrintingParams=function(){
			main.utilities.setCurrentVals(self.printForm,self.params);
		};
		this.getAlignedOffsetY=function(canvas,height,item){
			var offset_y;
			if(this.params[item].current_val.charAt(0)=='t'){offset_y=this.printParams.itemsOffSetY;}
			else if(this.params[item].current_val.charAt(0)=='m'){offset_y=(canvas.height-this.printParams.itemsOffSetY*2)/2+this.printParams.itemsOffSetY-height/2}
			else{offset_y=canvas.height-this.printParams.itemsOffSetY-height}
			return offset_y;
		};
		this.getAlignedOffsetX=function(canvas,width,item){
			var offset_x;
			if(this.params[item].current_val.indexOf('Left')>-1){offset_x=this.printParams.itemsOffSetX;}
			else if(this.params[item].current_val.indexOf('Middle')>-1){offset_x=(canvas.width-this.printParams.itemsOffSetX*2)/2+this.printParams.itemsOffSetX-width/2}
			else{offset_x=canvas.width-this.printParams.itemsOffSetX-width}
			return offset_x;
		};
		this.createPrintTitle=function(ctx,canvas){
			ctx.save();
			var fontSize=34;
			var titlePadding=10;
			var center=canvas.width/2;
			var title=this.params.title.current_val;
			ctx.font="900 "+fontSize+"px Lucida Grande";
			var titleWidth=ctx.measureText(title).width;
			var titleBoxWidth=titleWidth+2*titlePadding;
			var titleBoxHeight=fontSize+titlePadding*2;
			var offset_x=this.getAlignedOffsetX(canvas,titleBoxWidth,'title_align');
			var offset_y=this.getAlignedOffsetY(canvas,titleBoxHeight,'title_align');
			ctx.fillStyle='rgba(255,255,255,0.8)';
			ctx.fillRect(offset_x,offset_y,titleBoxWidth,fontSize+titlePadding);
			ctx.fillStyle='#000000';
			ctx.fillText(title,offset_x+titlePadding,offset_y+fontSize);
			ctx.restore();
			return ctx;
		};
		this.createPrintLegend=function(ctx,canvas){
			var legendPadding=7;
			var legendWidth=180;
			var legendHeight=40;
			var legendItemWidth=10;
			var fontSize=14;
			var font='Lucida Grande';
			var titleFont=16;
			var strokeWidth=1;
			var legendItemHeight=legendItemWidth;
			var outerWidth=legendWidth+2*legendPadding;
			var outerHeight=legendWidth+2*legendPadding;
			var offset_x=this.getAlignedOffsetX(canvas,outerWidth,'legend_align');
			var offset_y=this.getAlignedOffsetY(canvas,outerHeight,'legend_align');
			var xStart=offset_x+legendPadding;
			var yPosStart=offset_y+legendPadding;
			var yPos=yPosStart;
			
			//account for title
			yPos+=titleFont+5;
			
			ctx.font="500 "+fontSize+"px "+font;
			ctx.textAlign="left";
			ctx.strokeStyle='#000000';
			if(!main.canopy || !main.canopy.active){
				//Layers
				var layers=main.map_layers,layer,colorSplit,tempLayerObj={},accountedFor;
				for(var key in layers){
					accountedFor=false;
					layer=layers[key];
					if(layer.layer && layer.layer.olLayer.getVisible()){
						tempLayerObj[key]={};
						if(layer.layer.iconSrc){
							tempLayerObj[key].isImg=true;
							accountedFor=true;
						}else{
							if(layer.layer.olLayer){
								var feature0=layer.layer.olLayer.getSource().getFeatures()[0];
								if(feature0){
									var fillColor=layer.layer.style.params.fill_color;
									var geometry=feature0.getGeometry();
									if((geometry instanceof ol.geom.Polygon) || (geometry instanceof ol.geom.MultiPolygon) || (geometry instanceof ol.geom.Point) || (geometry instanceof ol.geom.MultiPoint)){
										var color=main.utilities.getColorArray(fillColor);
										tempLayerObj[key].fillStyle='rgb('+color[0]+','+color[1]+','+color[2]+')';
										tempLayerObj[key].yStart=yPos;
										tempLayerObj[key].alias=layer.properties.alias;
										if((geometry instanceof ol.geom.Polygon) || (geometry instanceof ol.geom.MultiPolygon)){
											tempLayerObj[key].isPolygon=true;
											accountedFor=true;
										}else if((geometry instanceof ol.geom.Point) || (geometry instanceof ol.geom.MultiPoint)){
											tempLayerObj[key].isPoint=true;
											accountedFor=true;
										}
									}
								}
							}
						}
					}
					if(accountedFor){
						yPos+=fontSize;
					}
				}
				
				//background
				ctx.fillStyle='rgba(255,255,255,0.8)';
				ctx.fillRect(offset_x,offset_y,outerWidth,yPos-offset_y);
				
				//Layers
				ctx.lineWidth=strokeWidth;
				for(var key in tempLayerObj){
					tempLayer=tempLayerObj[key];
					layer=layers[key];
					if(main.filter_legend && main.filter_legend.active && !main.filter_legend.disabled && main.filter_legend.layerName==key){
						var centerX=xStart+legendItemWidth/2;
						var centerY=tempLayer.yStart-legendItemHeight+fontSize/2;
						ctx.arc(centerX,centerY,legendItemWidth/2,0,2*Math.PI,false);
						ctx.fillStyle='rgba(0,0,0,0)';
						ctx.fill();
						ctx.stroke();
					}else{
						if(tempLayer.isPolygon){
							ctx.fillStyle=tempLayer.fillStyle;
							ctx.fillRect(xStart,tempLayer.yStart-legendItemHeight,legendItemWidth,legendItemHeight);
							ctx.stroke();
						}else if(tempLayer.isPoint){
							var centerX=xStart+legendItemWidth/2;
							var centerY=tempLayer.yStart-legendItemHeight+fontSize/2;
							ctx.arc(centerX,centerY,legendItemWidth/2,0,2*Math.PI,false);
							ctx.fillStyle=tempLayer.fillStyle;
							ctx.fill();
							ctx.stroke();
						}else if(tempLayer.isImg){
						}
					}
					ctx.fillStyle="#000000";
					ctx.fillText(tempLayer.alias,xStart+legendItemWidth+4,tempLayer.yStart);
				}
				yPos=yPos-offset_y;
			}else{
				var layer=main.canopy.currentLayer;
				var field=main.canopy.currentField;
				var gradientWidth=26;
				var gradientHeight=180;
				var gradStartY=yPos+fontSize+10;
				ctx.save();
				ctx.beginPath();
				var grd = ctx.createLinearGradient(0, gradStartY, 0, gradStartY+gradientHeight);
				if(main.canopy.toolType=='plan' || !field.color_grad){
					grd=main.utilities.createGradient(grd,main.canopy.currentWeightColorGrad.name);
					var rV=main.canopy.relativeValues;
					var max=rV[2],min=rV[0],middle=rV[1],onEnd='',fieldTitle=main.canopy.weightLabelAlias,toFixed=main.canopy.defaultWeightToFixed;
				}else{
					grd.addColorStop(1,field.color_grad[0]);
					grd.addColorStop(0,field.color_grad[1]);
				}
				
				//background
				ctx.fillStyle='rgba(255,255,255,0.8)';
				ctx.fillRect(offset_x,offset_y,outerWidth,gradStartY+gradientHeight);
				
				var gradStart=offset_x+legendWidth-legendPadding-gradientWidth;
				ctx.fillStyle = grd;
				ctx.fillRect(gradStart, gradStartY, gradientWidth, gradientHeight);
				ctx.stroke();
				if(main.canopy.toolType=='plan'){
					var rV=main.canopy.relativeValues;
					var max=rV[2],min=rV[0],middle=rV[1],onEnd='',fieldTitle=main.canopy.weightLabelAlias,toFixed=main.canopy.defaultWeightToFixed;
				}else{
					var stats=main.canopy.currentLayer.canopy.stats.fields[field.name];
					var toFixed=Math.min(main.canopy.maxToFixed,field.to_fixed);
					onEnd=field.append_to,fieldTitle=field.alias;
					if(main.canopy.toolType=='grow'){
						var max=(Math.max(stats.max/* ,main.canopy.canopyMax */));
						var min=(Math.min(stats.min/* ,main.canopy.canopyMin */));
						var fieldTitle='Percent Urban Tree Canopy';
					}else{
						var max=stats.max;
						var min=stats.min;
					}
					middle=((max+min)/2).toFixed(toFixed)+onEnd;
					max=max.toFixed(toFixed)+onEnd;
					min=min.toFixed(toFixed)+onEnd;
				}
				ctx.fillStyle="#000000";
				ctx.textAlign="right";
				ctx.font="500 "+fontSize+"px "+font;
				yPos+=fontSize;
				ctx.fillText(fieldTitle,offset_x+legendWidth+legendPadding,yPos);
				yPos=gradStartY+gradientHeight+offset_y;
			}
			ctx.textAlign="right";
			ctx.fillStyle="#000000";
			var textStart=gradStart-5;
			ctx.fillText(max,textStart,gradStartY+fontSize);
			ctx.fillText(middle,textStart,gradStartY+gradientHeight/2+fontSize/2);
			ctx.fillText(min,textStart,gradStartY+gradientHeight);
				
			//Title
			ctx.font="900 "+titleFont+"px "+font;
			ctx.textAlign="center";
			// ctx.fillText('Legend',offset_x+legendWidth/2,yPosStart+titleFont);
			
			//Extra Info
			if(main.canopy && main.canopy.active){
				ctx.save();
				var extraSpace=110;
				var maxAliasChars=28;
				var maxValChars=16;
				//ctx.translate(offset_x-extraSpace,yPos);
				var lineHeight=14;
				var paramsHeight=0;
				if(main.canopy.toolType=='plan'){
					var keys=Object.keys(main.canopy.currentLayer.canopy.plan.fields);
					paramsHeight+=lineHeight;
					for(var u=0;u<keys.length;u++){
						if(main.canopy.currentLayer.fields[keys[u]].in_plan_tools){paramsHeight+=lineHeight;}
					}
				}
				if(main.canopy.toolType=='view'){
					var keys=Object.keys(main.canopy.currentLayer.canopy.stats.fields);
					paramsHeight+=lineHeight;
					for(var u=0;u<keys.length;u++){
						if(main.canopy.currentLayer.fields[keys[u]].in_view_tools){paramsHeight+=lineHeight;}
					}
				}
				var offset_x=this.getAlignedOffsetX(canvas,legendWidth+extraSpace+2*legendPadding,'layer_params_align');
			   var offset_y=this.getAlignedOffsetY(canvas,paramsHeight+titleFont+2*legendPadding,'layer_params_align');
				
				ctx.fillStyle='rgba(255,255,255,0.8)';
				ctx.fillRect(offset_x,offset_y,legendWidth+extraSpace+2*legendPadding,paramsHeight+titleFont+2*legendPadding);
				//ctx.fillRect(0,0,legendWidth+extraSpace+2*legendPadding,paramsHeight+titleFont+2*legendPadding);
				
				ctx.fillStyle='#000000';
				var paramsHeight=0;
				if(main.canopy.toolType=='plan'){
					var title='Weight Values';
					ctx.font="900 "+titleFont+"px Lucida Grande";
					ctx.textAlign="center";
					ctx.translate((legendWidth+extraSpace)/2,0);
					//ctx.fillText(title,legendPadding,paramsHeight+titleFont+legendPadding);
					ctx.fillText(title,offset_x+legendPadding,offset_y+paramsHeight/* +titleFont */+legendPadding);
					paramsHeight+=titleFont+legendPadding+5;
					ctx.font="500 "+fontSize+"px Lucida Grande";
					ctx.textAlign="left";
					ctx.translate(-(legendWidth+extraSpace)/2,0);
					var keys=Object.keys(main.canopy.currentLayer.canopy.plan.fields);
					for(var u=0;u<keys.length;u++){
						if(main.canopy.currentLayer.fields[keys[u]].in_plan_tools){
							var alias=main.canopy.currentLayer.fields[keys[u]].alias;
							var val=main.canopy.weightDisplay(main.canopy.currentLayer.canopy.plan.fields[keys[u]].currentWeight);
							val=val.toString();
							if(alias.length>maxAliasChars){alias=alias.substr(0,maxAliasChars)}
							if(val.length>maxValChars){val=val.substr(0,maxValChars)}
							var text=alias+': '+val;
							paramsHeight+=lineHeight;
							//ctx.fillText(text,legendPadding,paramsHeight);
							ctx.fillText(text,offset_x+legendPadding,offset_y+paramsHeight);
						}
					}
				}				
				if(main.canopy.toolType=='view'){
					var title='Filter Values';
					ctx.font="900 "+titleFont+"px Lucida Grande";
					ctx.textAlign="center";
					ctx.translate((legendWidth+extraSpace)/2,0);
					//ctx.fillText(title,legendPadding,paramsHeight+titleFont+legendPadding);
					ctx.fillText(title,offset_x+legendPadding,offset_y+paramsHeight/* +titleFont */+legendPadding);
					paramsHeight+=titleFont+legendPadding+5;
					ctx.font="500 "+fontSize+"px Lucida Grande";
					ctx.textAlign="left";
					ctx.translate(-(legendWidth+extraSpace)/2,0);
					var keys=Object.keys(main.canopy.currentLayer.canopy.stats.fields),field,append_to,prepend_to;
					for(var u=0;u<keys.length;u++){
						if(main.canopy.currentLayer.fields[keys[u]].in_view_tools){
							field=main.canopy.currentLayer.fields[keys[u]];
							append_to='',prepend_to='';
							if(field.append_to){append_to=field.append_to;}
							if(field.prepend_to){prepend_to=field.prepend_to;}
							var alias=field.alias;
							var val=prepend_to+main.canopy.currentLayer.canopy.stats.fields[keys[u]].currentLow.toFixed(main.canopy.maxToFixed)+append_to+' - '+prepend_to+main.canopy.currentLayer.canopy.stats.fields[keys[u]].currentHi.toFixed(main.canopy.maxToFixed)+append_to;
							val=val.toString();
							if(alias.length>maxAliasChars){alias=alias.substr(0,maxAliasChars)}
							if(val.length>maxValChars){val=val.substr(0,maxValChars)}
							var text=alias+': '+val;
							paramsHeight+=lineHeight;
							//ctx.fillText(text,legendPadding,paramsHeight);
							ctx.fillText(text,offset_x+legendPadding,offset_y+paramsHeight);
						}
					}
				}
			}
			ctx.restore();
			
			return ctx;
		};
		this.createScale=function(ctx,canvas){
			ctx.save();
			var origScaleWidth=$('.ol-scale-line-inner').outerWidth(true);
			var unitString=$('.ol-scale-line-inner').html();
			var scaleColor=$('.ol-scale-line').css('background-color');
			var scaleHeight=22;
			var fontSize=12;
			var textPad=4;
			var scaleWidth=origScaleWidth;
			ctx.beginPath();
			ctx.fillStyle=scaleColor;
			var offset_x=this.getAlignedOffsetX(canvas,scaleWidth,'scale_align');
			var offset_y=this.getAlignedOffsetY(canvas,scaleHeight,'scale_align');
			ctx.fillRect(offset_x,offset_y,scaleWidth,scaleHeight);
			ctx.moveTo(offset_x,offset_y);
			ctx.lineTo(offset_x,offset_y+scaleHeight);
			ctx.lineTo(offset_x+scaleWidth,offset_y+scaleHeight);
			ctx.lineTo(offset_x+scaleWidth,offset_y);
			ctx.strokeStyle="rgba(255,255,255,1)";
			ctx.stroke();
			ctx.font=fontSize+'px Lucida Grande';
			ctx.fillStyle="#FFFFFF";
			var textWidth=ctx.measureText(unitString).width;
			var textOffsetX=(offset_x+(scaleWidth-textWidth)/2);
			ctx.fillText(unitString,textOffsetX,offset_y+fontSize+textPad);
			ctx.restore();
			return ctx;
		};
		this.printPDF=function(imgData,canvas,fileName){
			var width=canvas.width;
			var height=canvas.height;
			var whRatio=height/width;
			var pageWidth=210;
			var pageHeight=297;
			var pageMargin=10;
			var pageMarginWidth;
			var supposedPageWidth=pageWidth-2*pageMargin;
			var supposedPageHeight=pageHeight-2*pageMargin;
			if(width>supposedPageWidth && height>supposedPageHeight){
				width=pageWidth-2*pageMargin;
				height=width*whRatio;
				if(height>supposedPageHeight){
					height=pageHeight-2*pageMargin;
					width=height/whRatio;
				}
			}
			if(width>supposedPageWidth && height<=supposedPageHeight){
				width=pageWidth-2*pageMargin;
				height=width*whRatio;
			}
			if(width<=supposedPageWidth && height>supposedPageHeight){
				height=pageHeight-2*pageMargin;
				width=height/whRatio;
			}
			pageMarginWidth=(pageWidth-width)/2;
			var doc=new jsPDF();
			//doc.setFontSize(40);
			//doc.text(35,25,"Test");
			doc.addImage(imgData,'JPEG',pageMarginWidth,pageMargin,width,height);
			doc.save(fileName);
		};
		this.printImg=function(fileType,canvas,fileName,saveLoc,cb,cbParams){
			saveLoc=saveLoc || 'local';
			if(fileType.name=='PDF'){
				var imgData=canvas.toDataURL('image/jpeg');
				self.printPDF(imgData,canvas,fileName);
			}else{
				var imgData=canvas.toDataURL(fileType.dataType);
				if(saveLoc=='local'){
					// canvas.setAttribute('crossOrigin','anonymous');
										// window.open(canvas.toDataURL(fileType.dataType));
					fileName=fileName+'.'+fileType.ext;
					if(!main.browser.isIE){
						var form=$('<form>',{
							action:main.mainPath+'server/createJPG.php',
							method:'POST'
						});
						form.append("<input type='hidden' name='data' value='"+imgData+"'/>");
						form.append('<input type="hidden" name="outputName" value="'+fileName+'"/>');
						$('body').append(form);
						form.get(0).submit();
						form.remove();
					}else{
						// window.open(imgData);
						var test=$('<a download="'+fileName+'" href="'+imgData+'">');
						// test[0].click();
					}
				}else if(saveLoc=='server'){
					fileName=main.utilities.generateUniqueID()+'.'+fileType.ext;
					main.layout.loader.loaderOn();
					$.ajax({
						type:'POST',
						url:main.mainPath+'server/db.php',
						data:{
							params:{
								outputName:fileName,
								data:imgData
							},
							action:'saveImgToServer'
						},
						success:function(response){
							if(response){
								response=JSON.parse(response);
									if(cb){cb(response,cbParams);}
								if(response.status=="OK"){
								}else{
									alert('Error');
									console.log(response);
								}
							}
							main.layout.loader.loaderOff();
						}
					});
				}
			}
		};
		this.printChartImg=function(fileType,img,fileName,saveLoc){
			saveLoc=saveLoc || 'local';
			if(fileType.name=='PDF'){
				var imgData=img;
				self.printPDF(imgData,canvas,fileName);
			}else{
				var imgData=img;
				fileName=fileName+'.'+fileType.ext;
				if(!main.browser.isIE){
					var form=$('<form>',{
						action:main.mainPath+'server/createJPG.php',
						method:'POST'
					});
					form.append("<input type='hidden' name='data' value='"+imgData+"'/>");
					form.append('<input type="hidden" name="outputName" value="'+fileName+'"/>');
					form.append('<input type="hidden" name="saveLoc" value="'+saveLoc+'"/>');
					$('body').append(form);
					form.get(0).submit();
					form.remove();
				}else{
					var test=$('<a download="'+fileName+'" href="'+imgData+'">');
					test[0].click();
				}
			}
		};
		this.printRow=function(params){
			var form=$('<form>',{
				action:main.mainPath+'server/printRow.php',
				method:'POST',
				target:'_blank'
			});
			if(main.formsMod && main.formsMod.forms0[params.table]){
				var group='forms';
			}else if(main.invents && main.invents.inventories[params.table]){
				var group='inventories';
			}
			if(!group){alert('Error');return;}
			var title=main.tables[params.table].properties.plural_alias+' #'+params.pid;
			if(main.tables[params.table].properties.plural_alias){var title=main.tables[params.table].properties.alias+' #'+params.pid;}
			else{var title=main.tables[params.table].properties.alias+' #'+params.pid;}
			form.append($('<input type="hidden" name="folder"/>').val(window.location.pathname.split("/")[window.location.pathname.split("/").length-2]));
			var mapPath=params.mapPath || '';
			form.append($('<input type="hidden" name="description"/>').val(''));
			form.append($('<input type="hidden" name="title"/>').val(title));
			form.append($('<input type="hidden" name="mapPath"/>').val(mapPath));
			form.append($('<input type="hidden" name="pid"/>').val(params.pid));
			form.append($('<input type="hidden" name="table"/>').val(params.table));
			form.append($('<input type="hidden" name="group"/>').val(group));
			$('body').append(form);
			form.get(0).submit();
			form.remove();
		};
		this.print=function(map,saveLoc,cb,cbParams){
			map.once('postcompose',function(event){
				var canvas=event.context.canvas;
				var ctx=canvas.getContext('2d');
				var canvasBG=document.createElement('canvas');
				var ctx=canvasBG.getContext('2d');
				canvasBG.width=canvas.width;
				canvasBG.height=canvas.height;
				ctx.drawImage(canvas, 0, 0);
				if(self.params.title.current_val) ctx=self.createPrintTitle(ctx,canvasBG);
				if(self.params.with_legend.current_val=='true') ctx=self.createPrintLegend(ctx,canvasBG);
				if(self.params.with_scale.current_val=='true') ctx=self.createScale(ctx,canvasBG);
				var fileType=self.params.file_type.options[self.params.file_type.current_val];
				if(self.params.file_name.current_val){
					var fileName=self.params.file_name.current_val;
					if(fileName.lastIndexOf('.')>-1){
						fileName=fileName.substring(0,fileName.lastIndexOf('.'));
					}
					fileName=fileName.replace(/\W+/g,'_');
					if(fileName.length===0) fileName=self.params.file_name.default_val;
				}else{
					fileName=self.params.file_name.default_val;
				}
				self.printImg(fileType,canvasBG,fileName,saveLoc,cb,cbParams);
			});
			map.renderSync();
		};
		this.initEvents=function(){
			this.startPrintingButton.on('click',function(){
				if(main.data.std_settings.tile_source[self.map.currentTiles].printable){self.print(self.map.map);}
				else{alert("These map tiles aren't printable due to restrictions. Please choose a different set of map tiles.");}
			});
			this.printForm.on('change','.formInput',function(){
				self.printingInputChange();
			});
		};
		this.setParams();
		this.createHTML();
		main.utilities.setCurrentVals(this.printForm,this.params);
		this.initEvents();
	};
});
