define(function(require){
	return function pg(args){
		var self=this;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		(this.addlandcover = function(){
			for (var templayer in main.data.rasters){
				for (var temptile in main.data.rasters[templayer].tiles){
					var tile = main.utilities.cloneObj(main.data.rasters[templayer].tiles[temptile]);
					var id = main.utilities.generateUniqueID();
 					var lctile = new ol.layer.Image({
						source: new ol.source.ImageStatic({
							url: 'images/rasters/'+tile.tile_name+'.png',
							imageExtent: [tile.extent_left,tile.extent_bottom,tile.extent_right, tile.extent_top],
						}) ,
						// opacity: 0.7, 
					}); 
					// lctile.setMinResolution(parseInt(tile.minRes));
					// lctile.setMaxResolution(parseInt(tile.maxRes));
					main.map.map.addLayer(lctile);
				}
			}
		})();
	};
});