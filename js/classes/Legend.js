define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var Checkbox=require('./Checkbox');
		var Panel=require('./Panel');
		this.alias='Legend';
		this.addAccShelf=false;
		this.showOnStart=true;
		this.items={};
		this.inSettings=true;
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.layers=this.layers || {};
		this.createShelfContent=function(){
			self.showLegendButton=new Button('std','showLegend showButton','Show');
			self.hideLegendButton=new Button('std','hideLegend hideButton','Hide');
			var table=$('<table>')
				.append($('<tbody>')
					.append($('<tr>')
						.append($('<th>')
							.append($('<td>')
								.append($('<div>').addClass('tdLiner')
									.append(self.showLegendButton.html)
									.append(self.hideLegendButton.html)
								)
							)
						)
					)
				);
			return table
		};
		this.show=function(){
			self.panel.open();
			// self.updateButtonsShow();
		};
		this.hide=function(){
			self.panel.close();
			// self.updateButtonsHide();
		};
		this.updateButtonsShow=function(){
			if(self.showLegendButton){self.showLegendButton.html.removeClass('inlineBlock');}
			if(self.hideLegendButton){self.hideLegendButton.html.removeClass('none');}
		};
		this.updateButtonsHide=function(){
			if(self.showLegendButton){self.showLegendButton.html.addClass('inlineBlock');}
			if(self.hideLegendButton){self.hideLegendButton.html.addClass('none');}
		};
		this.updateLayerPreviews=function(layerItem){
			if(layerItem.layer.iconSrc){
				layerItem.layerPreviewInner.html('<img src="'+layerItem.layer.iconSrc+'" class="layerPreviewContent img"/>');
			}else if(layerItem.layer.style.params.fill_color){
				layerItem.layerPreviewInner.html('<div class="layerPreviewContent fill" style="background-color:'+layerItem.layer.style.params.fill_color+'"></div>');
			}
		};
		this.sortLegendItems=function(layersWrap){
			layersWrap.find('.checkboxWrap').each(function(){
				if($(this).find('.legendLayerCheckWrap').prop('checked')){
					$(this).closest('.legendRow').prependTo(layersWrap);
				}else{
					$(this).closest('.legendRow').appendTo(layersWrap);
				}
			});
		};
		this.addItem=function(layer){
			var id=layer.id;
			self.noLayersWrap.removeClass('block');
			self.items[id]={
				id:id,
				layer:layer,
				legendRow:$('<div class="legendRow cf">'),
				layerPreview:$('<div class="layerPreview legendRowEle">'),
				layerPreviewInner:$('<div class="layerPreviewInner">'),
				layerLabel:$('<div class="layerLabel legendRowEle">').html(layer.alias),
				cb:new Checkbox({
					value:layer.id,
					checked:layer.visible,
					name:'legendLayer',
					classes:'legendLayerCheckWrap '+id,
					data:[['layerid',id]],
					wrapClasses:'legendRowEle'
				})
			};
			self.layers[id]=layer;
			var item=self.items[id];
			item.cb.checkbox.on('change',function(){
				var layerID=$(this).data('layerid');
				var visible=$(this).prop('checked');
				if(self.layers[layerID]){
					self.layers[layerID].layer.setVisible(visible);
				}
			});
			self.layersWrap.append(item.legendRow
				.append(item.cb.html)
				.append(item.layerPreview.append(item.layerPreviewInner))
				.append(item.layerLabel)
			)
			self.sortLegendItems(self.layersWrap);
			self.updateLayerPreviews(item);
			// if(main.filter_legend && main.filter_legend.withOtherLayers){main.filter_legend.addNewOtherLayer(self.layers[id]);}
		};
		(this.createHTML=function(){
			if(!main.withs.with_filter_legend.value){
				if(!self.inSettings){
					//shelf
					if(self.addAccShelf){
						self.launchButton=new Button('toolBox','',self.alias);
						self.shelfContent=self.createShelfContent();
						self.shelf=main.layout.toolBox.createShelf({
							name:'legend',
							button:self.launchButton.html,
							content:self.shelfContent,
							alias:self.alias
						});
					}
				}else{
					var settingsStuff=main.settings.addSetting('Legend');
					self.showLegendButton=settingsStuff.showButton;
					self.hideLegendButton=settingsStuff.hideButton;
				}
				self.hideLegendButton.html.on('click',function(){
					self.hide();
				});
				self.showLegendButton.html.on('click',function(){
					self.show();
				});
			}
			self.titleHTML=$('<div>').addClass('legendTitle');
			self.layersWrap=$('<div>').addClass('layersWrap');
			self.noLayersWrap=$('<div>').addClass('noLayersWrap').text('No layers loaded yet.');
			if(self.title){self.titleHTML.html(self.title)}
			for(var key in self.layers){
				self.addItem(self.layers[key]);
			}
			if(Object.keys(self.layers).length===0){
				self.noLayersWrap.addClass('block');
			}
			self.content=$('<div>').addClass('legendContent')
				.append(self.titleHTML)
				.append(self.layersWrap)
				.append(self.noLayersWrap)
			self.panel=new Panel({
				content:self.content,
				classes:'legend',
				position:'tRight',
				relativeTo:main.layout.mapContainerWrap,
				onClose:self.updateButtonsHide,
				onMin:self.updateButtonsHide,
				onShow:self.updateButtonsShow
			});
		})();
		(this.initEvents=function(){
		})();
		if(self.showOnStart){self.show();}else{self.updateButtonsHide()}
	};
});