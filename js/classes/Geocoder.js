define(function(require){
	return function pg(args){
		var self=this;
		var Button=require('./Button');
		var PopUp=require('./PopUp');
		var ToolTip=require('./ToolTip');
		(function(args){
			for(var key in args){
				self[key]=args[key];
			}
		})(args);
		this.uniqueID=main.utilities.generateUniqueID();
		this.layer=main.map.vectorLayers['locate_point'];
		this.layerSource=this.layer.getSource();
		this.selecting=false;
		this.values={};
		this.popups={};
			// self.map.removePopUp('locate_point');
		this.iconStyle=main.utilities.getMarkerStyle();
		this.create=function(){
			this.centerToAddButton=new Button('icon','centerToAdd fullWidth geocoderBtn','',null,null,main.mainPath+'images/crosshairs.png','Center Map to Address');
			this.addFromMapButton=new Button('icon','addFromMap fullWidth geocoderBtn','',null,null,main.mainPath+'images/select_from_map.png','Select Address From Map');
			this.geoLocInput=$('<input type="hidden" class="geocoderGeoLoc"/>');
			this.html=$('<div>').addClass('geocoderPkg cf')
				.append(this.centerToAddButton.html)
				.append(this.addFromMapButton.html)
				.append(this.geoLocInput);
			this.toolTip=new ToolTip({
				content:'Select a point from the map.'
			});
		};
		(this.setFields=function(){
			self.specialFields=self.context.specialFields;
		})();
		this.removeAllFeatures=function(deleteToo){
			for(var key in self.values){
				self.removeFeature(key,deleteToo);
			}
		};
		this.removeFeature=function(key,deleteToo){
			self.layerSource.removeFeature(self.values[key].feature);
			if(self.values[key].popup){self.values[key].popup.remove();}
			if(deleteToo){delete self.values[key];}
		};
		this.newAddress=function(results,formattedAddress,cood){
			self.removeAllFeatures(true);
			if(self.specialFields.address_1 && !self.specialFields.street_num && results.street_num && results.street_name){
				if(!self.specialFields.zip && results.zip.short_name){
					self.specialFields.address_1.val(results.street_num.long_name+' '+results.street_name.long_name+' '+results.zip.short_name);
				}else{
					self.specialFields.address_1.val(results.street_num.long_name+' '+results.street_name.long_name);
				}
			}else if(self.specialFields.address_1 && self.specialFields.street_num && results.street_num && results.street_name){
				self.specialFields.address_1.val(results.street_name.long_name);
				self.specialFields.street_num.val(results.street_num.long_name);
			}else if(!self.specialFields.address_1 && self.specialFields.street_num && results.street_num){
				self.specialFields.street_num.val(results.street_num.long_name);
			}
			if(self.specialFields.zip && results.zip.short_name){
				self.specialFields.zip.val(results.zip.short_name);
			}
			if(cood){
				self.geoLocInput.val(cood[0]+','+cood[1]);
			}
			if(self.specialFields.geometry){
				var field=self.context.fields[self.specialFields.geometry.data('name')];
				var value='';
				if(field.data_type=='geometry' && results.coods){
					if(field.sub_data_type && field.sub_data_type.toLowerCase()=='point'){
						var value=JSON.stringify({
							type:'Point',
							coordinates:results.coods
						});
					}
				}
				self.specialFields.geometry.val(value).change();
			}
			self.centerMap(results.coods,formattedAddress);
			self.stopSelecting();
			if(self.callback){
				var callback=eval(self.callback);
				callback(self)
			}
		};
		this.clear=function(){
			if(self.layerSource){
				self.removeAllFeatures(true);
			}
		};
		this.getAddFromMap=function(cood,callback){
			cood4326=ol.proj.transform(cood, 'EPSG:3857', 'EPSG:4326');
			$.ajax({
				type:'POST',
				url:main.mainPath+'server/db.php',
				data:{
					params:{
						folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
						cood3857:cood,
						cood:cood4326[1]+','+cood4326[0],
					},
					action:'getAddFromMap'
				},
				success:function(response){
					if(response){
						response=JSON.parse(response);
						if(response.status=="OK"){
							var results=response.results;
							if(callback)callback(results,self.getFormattedAddress(results),response.params.cood3857);
						}else{
							alert('Error');
							console.log(response);
						}
					}
				}
			});
		};
		this.getCoodsFromAddress=function(callback,address){
			if(!address){
				address='';
				if(self.specialFields.street_num && self.specialFields.street_num.val()) address+=self.specialFields.street_num.val();
				if(self.specialFields.address_1 && self.specialFields.address_1.val()) address+=self.specialFields.address_1.val();
				if(self.specialFields.zip && self.specialFields.zip.val()) address+=' '+self.specialFields.zip.val();
			}
			if(address){
				$.ajax({
					type:'POST',
					url:main.mainPath+'server/db.php',
					data:{
						params:{
							folder:window.location.pathname.split("/")[window.location.pathname.split("/").length-2],
							address:address
						},
						action:'getPointFromAddress'
					},
					success:function(response){
						if(response){
							response=JSON.parse(response);
							if(response.status=="OK"){
								var results=response.results;
								if(callback)callback(results.coods,self.getFormattedAddress(results));
							}else{
								var message='';
								if(response.message){message=':'+response.message;}
								alert('Error'+message);
								console.log(response);
							}
						}
					}
				});
			}
		};
		this.getFormattedAddress=function(results){
			var formattedAddress='';
			var top=false;
			if(results.street_num && results.street_num.long_name){
				formattedAddress+=results.street_num.long_name;
				top=true;
			}
			if(results.street_name && results.street_name.long_name){
				if(top){formattedAddress+=' ';}
				formattedAddress+=results.street_name.long_name+'<br/>';
				top=true;
			}else{
				if(top){formattedAddress+='<br/>';}
			}
			if(results.city && results.city.long_name){
				formattedAddress+=results.city.long_name;
			}
			if(results.state && results.state.short_name){
				formattedAddress+=' '+results.state.short_name;
			}
			if(results.zip && results.zip.short_name){
				formattedAddress+=' '+results.zip.short_name;
			}
			return formattedAddress;
		};
		this.startDragCB=function(e,feature){
			var id=feature.get('id');
			self.values[id].popup.remove();
		};
		this.doneDragging=function(e,feature){
			self.getAddFromMap(feature.getGeometry().getCoordinates(),self.newAddress);
		};
		this.addMarker=function(cood,formattedAddress,removeMarkerOnPopUpClose){
			var geo=new ol.geom.Point(cood);
			var id='icon_'+main.utilities.generateUniqueID();
			var iconFeature=new ol.Feature({
				geometry: geo,
				draggable:true,
				startDragCB:self.startDragCB,
				doneDragCB:self.doneDragging,
				id:id
			});
			iconFeature.setProperties({'type':'locate_point','geocoder':self.uniqueID,'id':id,'formattedAddress':formattedAddress});
			iconFeature.setStyle(self.iconStyle);
			self.layerSource.addFeature(iconFeature);
			main.utilities.setTopLayer(self.layer);
			self.values[id]={
				feature:iconFeature,
				id:id,
				formattedAddress:formattedAddress,
				type:'locate_point',
				geocoder:self.uniqueID
			};
			self.values[id].popup=new PopUp({
				position:cood,
				html:formattedAddress,
				map:self.map,
				marker:self.values[id],
				context:self.context,
				classes:'geocoded',
				removeMarkerOnPopUpClose:removeMarkerOnPopUpClose,
				iconOffset:[26,-48]
			});
			return self.values[id];
		};
		this.mapClickedForSelect=function(e){
			e.originalEvent.stopImmediatePropagation();
			self.specialFields=self.context.specialFields;
			self.getAddFromMap(self.map.map.getEventCoordinate(e.originalEvent),self.newAddress);
		};
		this.startSelecting=function(){
			self.map.clearOtherPActions();
			self.toolTip.activate();
			self.selecting=true;
			self.map.selecting=true;
			main.map.map.on('click',self.mapClickedForSelect);
			// $(self.map.map.getTargetElement()).on('click',self.mapClickedForSelect);
		};
		this.stopSelecting=function(){
			self.toolTip.deActivate();
			// self.map.deActivateToolTip();
			self.selecting=false;
			self.map.selecting=false;
			main.map.map.un('click',self.mapClickedForSelect);
		};
		this.centerMap=function(coods,formattedAddress){
			var cood=ol.proj.transform(coods, 'EPSG:4326', 'EPSG:3857');
			self.map.setView(cood,main.map.settings.house_zoom.default_val);
			self.addMarker(cood,formattedAddress);
		};
		this.initEvents=function(){
			this.centerToAddButton.html.click(function(){
				self.getCoodsFromAddress(self.centerMap);
			});
			this.addFromMapButton.html.click(function(){
				if(!self.selecting){
					self.startSelecting();
				}else{
					self.stopSelecting();
				}
			});
			main.map.map.on('click',function(e){
				var pixel=main.utilities.getPixel(e);
				if(!pixel){return;}
				var feature=self.map.map.forEachFeatureAtPixel(pixel,function(feature,layer){
					return feature;
				});
				if(feature){
					var props=feature.getProperties();
					if(props.type=='locate_point' && props.geocoder==self.uniqueID){
						e.originalEvent.stopImmediatePropagation();
						if(self.shiftDown){
							self.removeFeature(props.id,true);
						}else{
							if(self.values[props.id].popup){
								self.values[props.id].popup.showPopup();
							}
						}
					}
				}
			});
		};
		this.create();
		this.initEvents();
		self.map.geocoders[this.uniqueID]=this;
	};
});