requirejs.config({
	baseUrl:main.mainPath+'js',
	paths:{
		mainClasses:'classes',
		instToMainClasses:'../js/classes'
	},
	waitSeconds:200
});
var array=['require'];
for(var i=0;i<main.extraClasses.length;i++){
	array.push(main.insFolderName+'js/classes/subClasses/'+main.extraClasses[i]+'.js');
}
define(array,function(){
	requirejs(['./Main']);
});