<?php
	// header('Cache-Control: no-cache');
	// header('Pragma: no-cache');
	// print_r($_SERVER);
	// print_r($_SESSION);
	// echo "Tree Plotter is temporarily undergoing updates, we'll be back soon.";
	// die;
	if(!isset($userID)){echo "You're Lost";}
	require('server/functions.php');
	$_SESSION['lastActivity']=time();
	$path= explode("/",$_SERVER['REQUEST_URI']);
	$folder=$path[count($path)-2];
    $_SESSION['uploaderClient'] = $folder;
	$data=retrieveData(array('userID'=>$userID,'folder'=>$folder));
	$extraClasses=scandir('js/classes/subClasses');
?>
<html>
<head>
	<title><?php echo $title;?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="shortcut icon" href="images/<?php echo $data->personal_settings->app_settings['small_icon'];?>" type="image/x icon" />
	<script src="<?php echo $mainPath;?>js/lib/jquery.min.js"></script>
	<!--<script src="js/lib/jspdf.min.js"></script>-->
	<script src="<?php echo $mainPath;?>js/lib/jspdf.js"></script>
	<script src="<?php echo $mainPath;?>js/lib/jquery-ui-1.11.4/jquery-ui.min.js"></script>
	<script src="<?php echo $mainPath;?>js/lib/Chart.js"></script>
	
	<!-- <script src="<?php //echo $loginPath;?>js/bootstrap.js"></script>
	<link rel="stylesheet" href="<?php //echo $loginPath;?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php //echo $loginPath;?>css/bootstrap-theme.min.css">-->
	<link rel="stylesheet" href="<?php echo $loginPath;?>DataTables-1.10.2/media/css/jquery.dataTables.min.css"> 
	
	<script src="<?php echo $mainPath;?>js/lib/jqplot/jquery.jqplot.min.js"></script>
	<script src="<?php echo $mainPath;?>js/lib/jqplot/jqplot.pieRenderer.min.js"></script>
	<script src="<?php echo $mainPath;?>js/lib/jqplot/jqplot.donutRenderer.min.js"></script>
	<script src="<?php echo $mainPath;?>js/lib/jqplot/jqplot.meterGaugeRenderer.min.js"></script>
	<script src="<?php echo $mainPath;?>js/lib/jqplot/jqplot.categoryAxisRenderer.min.js"></script>
	<script src="<?php echo $mainPath;?>js/lib/jqplot/jqplot.barRenderer.min.js"></script>
	<script src="<?php echo $mainPath;?>js/lib/jqplot/jqplot.highlighter.min.js"></script>
	<script src="<?php echo $mainPath;?>js/lib/jqplot/jqplot.canvasAxisTickRenderer.min.js"></script>
	<script src="<?php echo $mainPath;?>js/lib/jqplot/jqplot.canvasTextRenderer.min.js"></script>

	<link rel="stylesheet" href="<?php echo $mainPath;?>js/lib/jqplot/jquery.jqplot.min.css">
	
	
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
	<script src="<?php echo $mainPath;?>js/lib/ol.js" type="text/javascript"></script>
	<script src="../login/DataTables-1.10.2/media/js/jquery.dataTables.js"></script>
	<!--<script src="../login/js/bootstrap.min.js"></script>-->
	<script src="../login/js/ufcLoginApp.js"></script>
	<link rel="stylesheet" href="<?php echo $mainPath;?>css/main.css" type="text/css">
	<!--<link rel="stylesheet" href="<?php //echo $mainPath;?>css/dashboard.css" type="text/css">-->
	<script>
		var main={
			mainPath:'<?php echo $mainPath;?>',
			loginPath:'<?php echo $loginPath;?>',
			insDir:'<?php echo '..'.$_SERVER['REQUEST_URI'];?>',
			insFolderName:'<?php echo $_SERVER['REQUEST_URI'];?>',
			folderName:'<?php echo str_replace('/','',$_SERVER['REQUEST_URI']);?>',
			userID:'<?php echo $userID;?>',
			startClass:'Starter',
			extraClasses:[],
			localImgs:'images/',
			maxFeatures:1500,
			retrieveDBLayers:{}
		};
		(main.getBrowserParams=function(){
			main.browser={
				isMobile:false,
				isTouchable:false,
				isIE:false
			};
			var ua=window.navigator.userAgent;
			//test for ie
			var msie=ua.indexOf("MSIE ");
			if(ua.indexOf("MSIE ")>0 || ua.indexOf("Edge")>0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)){ 
				//get version
				var undef,v=3,div=document.createElement('div'),all=div.getElementsByTagName('i');
				while(div.innerHTML='<!--[if gt ID '+(++v)+']><i></i><![endif]-->', all[0]);
				var ie=(v>4?v:undef);
				if(ie<=9){alert('Please update your IE else unwanted results may occur.');}
				main.browser.isIE=true;
			}
			//test for mobile browser
			if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
			|| /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))){ main.browser.isMobile = true;}
			//test for touchable
			if('ontouchstart' in document.documentElement){main.browser.isTouchable=true;}
		})();
		(main.initControls=function(){
			if(main.browser.isMobile){
				main.controls={
					touchEnd:'touchend',
					touchBegin:'touchstart',
					touchMove:'touchmove'
				};
			}else{
				main.controls={
					touchEnd:'mouseup',
					touchBegin:'mousedown',
					touchMove:'mousemove'
				};
			}
		})();
		<?php
			if(count($extraClasses)){
				$remove=array('.','..');
				foreach($extraClasses as $key=>$class){
					if(in_array($class,$remove)){unset($extraClasses[$key]);}else{
					echo "main.extraClasses.push('".str_replace('.js','',$class)."');";}
				}
			}
		?>
		(main.logInUser=function(){
			main.temp={account:{loggedInUser:{}}};
			<?php
				$params=$_SESSION['users'][$folder];
				foreach($params as $key=>$param){
					if(is_array($param) || is_object($param)){
						echo "main.temp.account.loggedInUser['".$key."']={};";
						foreach($param as $key2=>$ele){
							if(!is_array($ele) && !is_object($ele)){
								echo "main.temp.account.loggedInUser['".$key."']['".$key2."']='".$ele."';";
							}else{
								// $ele=trim(json_encode($ele),'"');
								echo "main.temp.account.loggedInUser['".$key."']['".$key2."']=".json_encode($ele).";";
							}
						}
					}else{
						echo "main.temp.account.loggedInUser['".$key."']='".$param."';";
					}
				}
				echo "main.temp.account.loggedInUser.org_data={};";
				if(isset($_SESSION['users'][$folder]['organization'])){echo "main.temp.account.loggedInUser.org_data['organization']='".$_SESSION['users'][$folder]['organization']."';";}
				if(isset($_SESSION['users'][$folder]['orgId'])){echo "main.temp.account.loggedInUser.org_data['orgId']='".$_SESSION['users'][$folder]['orgId']."';";}
				if(isset($_SESSION['users'][$folder]['orgType'])){echo "main.temp.account.loggedInUser.org_data['orgType']='".$_SESSION['users'][$folder]['orgType']."';";}
				if(isset($_SESSION['users'][$folder]['userId'])){echo "main.temp.account.loggedInUser.org_data['userId']='".$_SESSION['users'][$folder]['userId']."';";}
				if(isset($_SESSION['users'][$folder]['roleName'])){echo "main.temp.account.loggedInUser.org_data['roleName']='".$_SESSION['users'][$folder]['roleName']."';";}
				if(isset($_SESSION['users'][$folder]['roleId'])){echo "main.temp.account.loggedInUser.org_data['roleId']='".$_SESSION['users'][$folder]['roleId']."';";}
			?>
		})();
		main.sortData=function (){
			var std_settings=main.data.std_settings,currSettings,sortedKeys;
			for(var key in std_settings){
				currSettings=std_settings[key];
				sortedKeys=Object.keys(currSettings);
				sortedKeys.sort(function(a,b){
					return currSettings[a].place_in_order-currSettings[b].place_in_order;
				});
				std_settings[key].sortedKeys=sortedKeys;
			}
		};
		<?php
			if($data){
				echo "var data='".addslashes(json_encode($data))."';";
				echo "main.data=JSON.parse(data);";
				echo "main.sortData();";
			}
		?> 
		if(main.data){
			main.tables={};
			main.withs=main.data.personal_settings.withs;
			main.map_layers={};
			for(var key in main.data.map_layers){
				if(main.data.map_layers[key].active){
					main.map_layers[key]={
						isShowLayer:true,
						hasLoaded:false,
						properties:main.data.map_layers[key],
						fields:main.data.fields[key]
					};
					main.tables[key]=main.map_layers[key];
					main.tables[key].atchs={items:{}};
					main.tables[key].map_layer=main.map_layers[key];
				}
			}
			
			/* main.groups={
				groups:main.data.groups,
				hierarchy:{}
			}; */
			/* var waiting=[];
			for(var key in main.groups.groups){
				if(!main.groups.groups[key].child_of){
					main.groups.hierarchy[key]={
						item:main.groups.groups[key],
						offspring:{}
					}
				}
			} */
			// main.groups=main.data.groups;
			// var groups,lastGroup;
			if(main.withs.with_forms && main.withs.with_forms.value){
				main.forms={};
				for(var key in main.data.forms){
					main.forms[key]={
						properties:main.data.forms[key],
						fields:main.data.fields[key]
					};
					main.tables[key]=main.forms[key];
					main.tables[key].atchs={items:{}};
					// if(main.data.forms[key].group){
						// groups=main.data.forms[key].group.split(',');
						// for(var i=0;i<groups.length;i++){
							// for(var t=0;t<i+1;t++){
								// main.groups[groups[t]]
								// lastGroup=groups[groups[i]];
								// if(main.groups[groups[i]]){}
							// }
						// }
					// }
				}
			}
			if(main.withs.with_inventories.value){
				main.inventories={};
				for(var key in main.data.inventories){
					if(main.data.inventories[key].active){
						main.inventories[key]={
							properties:main.data.inventories[key],
							fields:main.data.fields[key]
						};
						if(!main.map_layers[main.data.inventories[key].layer_name]){
							main.map_layers[key]={
								hasLoaded:false,
								properties:main.inventories[key].properties,
								fields:main.data.fields[key]
							};
						}
						main.map_layers[main.data.inventories[key].layer_name].inventory=main.inventories[key];
						main.inventories[key].map_layer=main.map_layers[main.data.inventories[key].layer_name];
						main.tables[key]=main.inventories[key];
						main.tables[key].atchs={items:{}};
					}
				}
			}
			if(main.withs.with_canopy.value){
				main.canopy_layers={};
				for(var key in main.data.canopy_layers){
					if(main.data.canopy_layers[key].active){
						main.canopy_layers[key]={
							properties:main.data.canopy_layers[key],
							fields:main.data.fields[key]
						};
						if(!main.map_layers[main.data.canopy_layers[key].layer_name]){
							main.map_layers[key]={
								isCanopy:true,
								hasLoaded:false,
								properties:main.canopy_layers[key].properties,
								fields:main.data.fields[key]
							};
						}
						main.map_layers[main.data.canopy_layers[key].layer_name].inventory=main.canopy_layers[key];
						main.canopy_layers[key].map_layer=main.map_layers[main.data.canopy_layers[key].layer_name];
						main.tables[key]=main.canopy_layers[key];
						main.tables[key].atchs={items:{}};
					}
				}
			}
			main.map_layers_sorted_keys=Object.keys(main.map_layers).sort(function(a,b){
				if(Number(main.map_layers[a].properties.place_in_order)>Number(main.map_layers[b].properties.place_in_order)){return 1;}
				if(Number(main.map_layers[a].properties.place_in_order)<Number(main.map_layers[b].properties.place_in_order)){return -1;}
				return 0;
			});
		}
		main.addMobileCSS=function(){
			var link=document.createElement('link');			
			link.rel='stylesheet';			
			link.type='text/css';			
			link.href='../main/css/mobile.css';			
			link.media='all';
			document.getElementsByTagName('head')[0].appendChild(link);
		};
		if(main.browser.isMobile){main.addMobileCSS();}
	</script>
	<script src="<?php echo $mainPath;?>js/lib/spectrum.js"></script>
	<!--<script src="<?php echo $mainPath;?>js/lib/jquery-sortable-min.js"></script>-->
	<!--<script src="<?php echo $mainPath;?>js/lib/jquery.mjs.nestedSortable.js"></script>-->
	<script src="<?php echo $mainPath;?>js/lib/requirejs.js" data-main="<?php echo $mainPath;?>js/rconfig"></script>
	<!--<link rel="stylesheet" href="http://openlayers.org/en/v3.0.0/css/ol.css" type="text/css">-->
	<link rel="stylesheet" href="<?php echo $mainPath;?>css/ol.css" type="text/css">
	<link rel="stylesheet" href="<?php echo $mainPath;?>js/lib/jquery-ui-1.11.4/jquery-ui.theme.min.css" type="text/css">
	<link rel="stylesheet" href="<?php echo $mainPath;?>js/lib/jquery-ui-1.11.4/jquery-ui.structure.min.css" type="text/css">
	<link rel="stylesheet" href="<?php echo $mainPath;?>js/lib/jquery-ui-1.11.4/jquery-ui.min.css" type="text/css">
	<link rel="stylesheet" href="css/custom.css" type="text/css">
	<link rel="stylesheet" href="<?php echo $mainPath;?>css/spectrum.css" type="text/css">
</head>
<!--<script>
</script>-->
<body class="cf"></body>
</html>
